﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Async.aspx.cs" Inherits="SelectionPage" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Campervans Availability Selection Page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/js/cbox.js"></script>
    <!--<script type="text/javascript" src="/js/isotope.js"></script>-->
    <script type="text/javascript" src="/js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="/js/jstorage.min.js"></script>
    <script type="text/javascript" src="/js/booking.js?ver=1"></script>
    <script type="text/javascript">

        var isSecure = <%= IsSecureMode.ToString().ToLower()  %> ;

        if (window.location.href.indexOf('http:') > -1 && isSecure){
            document.location = window.location.href.replace('http:', 'https:');
        }

        var sortBy = '', filterBy = '', isAsc = true;
        var rqstParams = <%= RequestJSON %>;
        var ctrlParams = <%= ctrlParams %>;
        var domain = <%= DomainJSON %>;
            
        var rqst = null;
        var initParams = null;
       
    </script>
    <style>
        .vprice .forex
        {
            float: none !important;
        }
    </style>
    <link rel="stylesheet" href="<%= CSSLink %>" />
    <%= AnalyticsCode %>
    <%= OpenTag %>
</head>
<body class="<%= bodyClass %>">
    <form id="form1" runat="server">
        <div id="header">
        </div>
        <div id="container">
            <div class="headerContent">
                <div id="brandedHeader">
                    <%= BrandedHeader %>
                </div>
                <div id="genericHeader">
                    <%--<a href="/">--%>
                    <a href="<%= ImageURL %>">
                        <%-- Changed by Nimesh to pull images from server rather than local machine--%>
                        <img src="<%= AssetsURL %>/images/<%= BrandChar %>/icons/logo.png" alt="logo" />
                        <%-- End Changed by Nimesh to pull images from server rather than local machine--%>
                    </a>
                    <ul class="steps">
                        <li class="selected">
                            <span class="tab">1.
                                <label>Select</label></span>
                        </li>
                        <li>
                            <span class="tab">2.
                                <label>Extras</label></span>
                        </li>
                        <li>
                            <span class="tab">3.
                                <label>Payment</label></span>
                        </li>
                    </ul>
                    <a class="contactUs" href="<%= ContactUsURL %>" target="_blank"><span>Contact us <i class="icon-chevron-down"></i></span></a>
                    <a class="contactUs contactUsMobile" href="<%= ContactUsURL %>" target="_blank"><span><i class="icon-envelope"></i></span></a>
                </div>
            </div>
            <div id="currencyPanel">
                <h1>Select Your Vehicle</h1>
                <span id="vehicleSelector">
                    <span class="camper">Campervan</span>
                    <a href="/Selection.aspx" class="car">Car</a>
                </span>
            </div>
            <div class="clearfix"></div>
            <!-- booking control starts -->
            <div id="bookingControl">
                <div class="head">
                    <div id="countrySelector">
                        <label>Country of Travel</label>
                        <select id="countryOfTravel" class="combo">
                            <option value="au">Australia</option>
                            <option value="nz">New Zealand</option>
                        </select>
                    </div>
                    <div class="period cell">
                        <label>Travelling</label>
                        <span id="travelPeriod">&nbsp;</span>
                    </div>
                    <div class="pickup cell">
                        <label>Arriving</label>
                        <span id="pickUpTxt">&nbsp;</span>
                    </div>
                    <div class="dropoff cell">
                        <label>Departing</label>
                        <span id="dropOffTxt">&nbsp;</span>
                    </div>
                    <div class="close cell">
                        <span><small>Close Search</small><i class="icon-minus"></i></span>
                    </div>
                </div>
                <div class="body">
                    <div class="left">
                        <span class="brandRow row">
                            <label for="dlSelector">Driver's Licence:</label>
                            <select id="dlSelector" class="combo">
                                <option value="-1" selected="selected">Please select</option>
                                <option value="au">Australia</option>
                                <option value="ca">Canada</option>
                                <option value="cn">China</option>
                                <option value="dk">Denmark</option>
                                <option value="fr">France</option>
                                <option value="de">Germany</option>
                                <option value="nl">Netherlands</option>
                                <option value="nz">New Zealand</option>
                                <option value="se">Sweden</option>
                                <option value="ch">Switzerland</option>
                                <option value="uk">United Kingdom</option>
                                <option value="us">United States Of America</option>
                                <option value="-" disabled="disabled">--------------------</option>
                                <option value="af">Afghanistan</option>
                                <option value="al">Albania</option>
                                <option value="dz">Algeria</option>
                                <option value="as">American Samoa</option>
                                <option value="ad">Andora</option>
                                <option value="ao">Angola</option>
                                <option value="ai">Anguilla</option>
                                <option value="aq">Antarctica</option>
                                <option value="ag">Antigua & Barbados</option>
                                <option value="ar">Argentina</option>
                                <option value="am">Armenia</option>
                                <option value="aw">Aruba</option>
                                <option value="at">Austria</option>
                                <option value="az">Azerbaijan</option>
                                <option value="bs">Bahamas</option>
                                <option value="bh">Bahrain</option>
                                <option value="bd">Bangladesh</option>
                                <option value="bb">Barbados</option>
                                <option value="by">Belarus</option>
                                <option value="be">Belgium</option>
                                <option value="bz">Belize</option>
                                <option value="bj">Benin</option>
                                <option value="bm">Bermuda</option>
                                <option value="bo">Bolivia</option>
                                <option value="ba">Bosnia and Herzegovina</option>
                                <option value="bw">Botswana</option>
                                <option value="bv">Bouvet Island</option>
                                <option value="br">Brazil</option>
                                <option value="iq">British Indian Ocean Territory</option>
                                <option value="bn">Brunei</option>
                                <option value="bg">Bulgaria</option>
                                <option value="bf">Burkina Faso</option>
                                <option value="bi">Burundi</option>
                                <option value="bt">Bhutan</option>
                                <option value="kh">Cambodia</option>
                                <option value="cm">Cameroon</option>
                                <option value="cv">Cape Verde</option>
                                <option value="ky">Cayman Islands</option>
                                <option value="cf">Cental African Republic</option>
                                <option value="td">Chad</option>
                                <option value="cl">Chile</option>
                                <option value="cx">Christmas Island</option>
                                <option value="cc">Cocos (Keeling) Island</option>
                                <option value="co">Colombia</option>
                                <option value="km">Comoros</option>
                                <option value="cg">Congo</option>
                                <option value="ck">Cook Island</option>
                                <option value="cr">Costa Rica</option>
                                <option value="ci">Cote D'ivoire</option>
                                <option value="hr">Croatia</option>
                                <option value="cu">Cuba</option>
                                <option value="cy">Cyprus</option>
                                <option value="cz">Czech Republic</option>
                                <option value="dj">DjiboutiI</option>
                                <option value="dm">Dominica</option>
                                <option value="do">Dominican Republic</option>
                                <option value="tp">East Timor</option>
                                <option value="ec">Ecuador</option>
                                <option value="eg">Egypt</option>
                                <option value="sv">El Salvador</option>
                                <option value="gq">Equatorial Guinea</option>
                                <option value="er">Eritrea</option>
                                <option value="ee">Estonia</option>
                                <option value="et">Ethiopia</option>
                                <option value="fk">Falkland Islands</option>
                                <option value="fo">Faroe Islands</option>
                                <option value="fj">Fiji</option>
                                <option value="fi">Finland</option>
                                <option value="fx">France, Metropolitan</option>
                                <option value="gf">French Guiana</option>
                                <option value="pf">French Polynesia</option>
                                <option value="tf">French Souther Territories</option>
                                <option value="ga">Gabon</option>
                                <option value="gm">Gambia</option>
                                <option value="ge">Georgia</option>
                                <option value="gh">Ghana</option>
                                <option value="gi">Gibraltar</option>
                                <option value="gr">Greece</option>
                                <option value="gl">Greenland</option>
                                <option value="gd">Grenada</option>
                                <option value="gp">Guadeloupe</option>
                                <option value="gu">Guam</option>
                                <option value="gt">Guatemala</option>
                                <option value="gn">Guinea</option>
                                <option value="gw">Guinea-Bissau</option>
                                <option value="gy">Guyana</option>
                                <option value="ht">Haiti</option>
                                <option value="hm">Heard and McDonald Islands</option>
                                <option value="hn">Honduras</option>
                                <option value="hk">Hong Kong</option>
                                <option value="hu">Hungary</option>
                                <option value="is">Iceland</option>
                                <option value="in">India</option>
                                <option value="id">Indonesia</option>
                                <option value="ir">Iran</option>
                                <option value="iq">Iraq</option>
                                <option value="ie">Ireland</option>
                                <option value="il">Israel</option>
                                <option value="it">Italy</option>
                                <option value="jm">Jamaica</option>
                                <option value="jp">Japan</option>
                                <option value="jo">Jordan</option>
                                <option value="kz">Kazakhstan</option>
                                <option value="ke">Kenya</option>
                                <option value="ki">Kiribati</option>
                                <option value="kw">Kuwait</option>
                                <option value="kg">Kyrgyzstan</option>
                                <option value="la">Lao Peoples Democratic Republic</option>
                                <option value="lv">Latvia</option>
                                <option value="lb">Lebanon</option>
                                <option value="ls">Lesotho</option>
                                <option value="lr">Liberia</option>
                                <option value="ly">Libya</option>
                                <option value="li">Liechtenstein</option>
                                <option value="lt">Lithuania</option>
                                <option value="lu">Luxembourg</option>
                                <option value="mo">Macau</option>
                                <option value="mk">Macedonia</option>
                                <option value="mg">Madagascar</option>
                                <option value="mw">Malawi</option>
                                <option value="my">Malaysia</option>
                                <option value="mv">Maldives</option>
                                <option value="ml">Mali</option>
                                <option value="mt">Malta</option>
                                <option value="mh">Marshall Islands</option>
                                <option value="mq">Martinique</option>
                                <option value="mr">Mauritania</option>
                                <option value="mu">Mauritius</option>
                                <option value="yt">Mayotte</option>
                                <option value="mx">Mexico</option>
                                <option value="fm">Micronesia, Federated States of</option>
                                <option value="md">Moldova</option>
                                <option value="mc">Monaco</option>
                                <option value="mn">Mongolia</option>
                                <option value="ms">Montserrat</option>
                                <option value="ma">Morocco</option>
                                <option value="mz">Mozambique</option>
                                <option value="nm">Myanamar</option>
                                <option value="na">Namibia</option>
                                <option value="nr">Nauru</option>
                                <option value="np">Nepal</option>
                                <option value="an">Netherlands Antilles</option>
                                <option value="nc">New Caledonia</option>
                                <option value="ni">Nicaragua</option>
                                <option value="ne">Niger</option>
                                <option value="ng">Nigeria</option>
                                <option value="nu">Niue</option>
                                <option value="nf">Norfolk Island</option>
                                <option value="kp">North Korea</option>
                                <option value="mp">Northern Mariana Islands</option>
                                <option value="no">Norway</option>
                                <option value="om">Oman</option>
                                <option value="pk">Pakistan</option>
                                <option value="pw">Palau</option>
                                <option value="pa">Panama</option>
                                <option value="pg">Papua New Guinea</option>
                                <option value="py">Paraguay</option>
                                <option value="pe">Peru</option>
                                <option value="ph">Philippines</option>
                                <option value="pn">Pitcairn Islands</option>
                                <option value="pl">Poland</option>
                                <option value="pt">Portugal</option>
                                <option value="pr">Puerto Rico</option>
                                <option value="qa">Qatar</option>
                                <option value="re">Reunion</option>
                                <option value="RO">Romania</option>
                                <option value="ru">Russian Federation</option>
                                <option value="rw">Rwanda</option>
                                <option value="sh">Saint Helena</option>
                                <option value="kn">Saint Kitts and Nevis</option>
                                <option value="lc">Saint Lucia</option>
                                <option value="pm">Saint Pierre and Miquelon</option>
                                <option value="vc">Saint Vincent and The Grenadines</option>
                                <option value="ws">Samoa</option>
                                <option value="sm">San Marino</option>
                                <option value="st">Sao Tome and Principe</option>
                                <option value="sa">Saudi Arabia</option>
                                <option value="sn">Senegal</option>
                                <option value="sc">Seychelles</option>
                                <option value="sg">Singapore</option>
                                <option value="sk">Slovakia</option>
                                <option value="si">Slovenia</option>
                                <option value="sb">Solomon Islands</option>
                                <option value="so">Somalia</option>
                                <option value="za">South Africa</option>
                                <option value="gs">South Georgia</option>
                                <option value="kr">South Korea</option>
                                <option value="es">Spain</option>
                                <option value="sl">Sri Lanka</option>
                                <option value="sd">Sudan</option>
                                <option value="sr">Suriname</option>
                                <option value="sj">Svalbardand Jan Mayen Islands</option>
                                <option value="sy">Syria</option>
                                <option value="ti">Tahiti</option>
                                <option value="tw">Taiwan</option>
                                <option value="tj">Tajikistan</option>
                                <option value="tz">Tanzania</option>
                                <option value="th">Thailand</option>
                                <option value="tg">Togo</option>
                                <option value="tk">Tokelau</option>
                                <option value="to">Tonga</option>
                                <option value="tt">Trinidad and Tobago</option>
                                <option value="tn">Tunisia</option>
                                <option value="tr">Turkey</option>
                                <option value="tm">Turkmenistan</option>
                                <option value="tc">Turks and Caicos Islands</option>
                                <option value="tv">Tuvalu</option>
                                <option value="ug">Uganda</option>
                                <option value="ua">Ukraine</option>
                                <option value="ae">United Arab Emirates</option>
                                <option value="uy">Uruguay</option>
                                <option value="um">US Minor Outlying Islands</option>
                                <option value="uz">Uzbekistan</option>
                                <option value="vu">Vanuatu</option>
                                <option value="va">Vatican City</option>
                                <option value="ve">Venezuala</option>
                                <option value="vn">Vietnam</option>
                                <option value="vg">Virgin Islands (GB)</option>
                                <option value="vi">Virgin Islands (US)</option>
                                <option value="wf">Wallis and Futuna Islands</option>
                                <option value="eh">Western Sahara</option>
                                <option value="ye">Yemen</option>
                                <option value="yu">Yugoslavia</option>
                                <option value="zr">Zaire</option>
                                <option value="zm">Zambia</option>
                                <option value="zw">Zimbabwe</option>
                            </select>
                        </span>
                        <span class="paxRow row">
                            <label for="adultSelector">Travellers:</label>
                            <select id="adultSelector" class="combo">
                                <option value="1" selected="selected">1 Adult</option>
                                <option value="2">2 Adults</option>
                                <option value="3">3 Adults</option>
                                <option value="4">4 Adults</option>
                                <option value="5">5 Adults</option>
                                <option value="6">6 Adults</option>
                            </select>
                            <select id="childSelector" class="combo">
                                <option value="0" selected="selected">0 Children</option>
                                <option value="1">1 Child</option>
                                <option value="2">2 Children</option>
                                <option value="3">3 Children</option>
                                <option value="4">4 Children</option>
                                <option value="5">5 Children</option>
                            </select>
                        </span>
                    </div>
                    <div class="center">
                        <span class="pickupRow row">
                            <label for="pickUpSelector">Picking up from:</label>
                            <select id="pickUpSelector" class="combo location">
                            </select>
                        </span>
                        <span class="dropOffRow row">
                            <label for="dropOffSelector">Dropping off at:</label>
                            <select id="dropOffSelector" class="combo location">
                            </select>
                        </span>
                    </div>
                    <div class="right">
                        <span class="dateTimeRow row">
                            <label for="pickUpTimeTxt">Pick up Date/Time:</label>
                            <span class="dateTimeRowOptions">
                                <input type="text" readonly="readonly" id="pickUpTimeTxt" class="datePicker" value="Mar 01, 2013" style="padding-top: 5px; font-size: .7em;" />
                                <i class="icon-calendar" id="pickCal"></i>
                                <select id="pickTimeSelector" class="combo timeSelector">
                                    <option value="1000" selected="selected">10:00am</option>
                                    <option value="1100">11:00am</option>
                                </select>
                            </span>
                        </span>
                        <span class="dateTimeRow row">
                            <label for="dropOffTimeTxt">Drop off Date/Time:</label>
                            <span class="dateTimeRowOptions">
                                <input type="text" readonly="readonly" id="dropOffTimeTxt" class="datePicker" value="Mar 12, 2013" style="padding-top: 5px; font-size: .7em; color: #555555;" />
                                <i class="icon-calendar"></i>
                                <select id="dropTimeSelector" class="combo timeSelector">
                                    <option value="1100" selected="selected">11:00am</option>
                                    <option value="1200">12:00am</option>
                                </select>
                            </span>
                        </span>
                    </div>


                    <div class="center">
                        <span class="pickupRow row">
                            <%--<label for="pickUpTimeTxt">Pick up Date/Time:</label>
                            <span class="dateTimeRowOptions">
                                <input type="text" readonly="readonly" id="Text1" class="datePicker" value="Mar 01, 2013" style="padding-top: 5px; font-size: .7em;" />
                                <i class="icon-calendar" id="I1"></i>
                                <select id="Select1" class="combo timeSelector">
                                    <option value="1000" selected="selected">10:00am</option>
                                    <option value="1100">11:00am</option>
                                </select>
                            </span>--%>
                        </span>
                        <span class="pickupRow row">
                            <label for="promoCodeTxt">Promo Code:</label>
                            <span class="dateTimeRowOptions">
                                <input type="text" id="promoCodeTxt" style="padding-top: 5px; font-size: .7em; color: #555555;width:250px" />
                            </span>
                        </span>
                    </div>
                </div>

                <div class="footer">
                    <span id="crossSell">
                        <span>Search</span>
                        <select id="csellSelect">
                            <option value="current"></option>
                            <option value="all">All campervan brands</option>
                        </select>
                    </span>
                    <span id="updateSearch" onclick="submitSearch()">Update Search<i class="icon-search"></i></span>
                </div>
            </div>


            <!-- booking control ends -->
        <div class="clearfix"></div>
        <!-- Filter/Sort panel starts -->
        <div id="sortPanel">
            <div class="header">
                <div class="left">
                    <h2>Filter &amp; Sort Vehicles</h2>
                    <small>
                        <em>Hide</em>
                        <i class="icon-chevron-up icon-white"></i>
                    </small>
                </div>
                <div class="right">
                    <span class="alternative-currency">
                        <label>Alternative Currency</label>
                        <%= CurrencySelector %>
                    </span>
                    <label id="infoLbl" for="vehicleInfo">Vehicle Info/Images:</label>
                    <ul id="vehicleInfoSelector">
                        <li><span class="hideshow hide"><i class="icon-eye-close"></i>Hide</span></li>
                        <li><span class="hideshow active show"><i class="icon-eye-open"></i>Show</span></li>
                    </ul>
                </div>
            </div>
            <div class="body">
                <div class="filterPanel">
                    <span class="lbl">Filter by:</span>
                    <!-- wrap filters -->
                    <!-- Changed by Nimesh on 23rd Aug 2014 to filter on multiple criteria rather then just one -->
                    <span class="filterOptions" data-filter-group="sleeps">
                        <span class="selector" id="filter_sleeps">
                            <small>sleeps</small>
                            <span class="btns">
                                <label class="selected" data-filter=".s2, .s3, .s4, .s5, .s6"><span>All</span></label>
                                <label data-filter=".s2"><span>2</span></label>
                                <label data-filter=".s3"><span>3</span></label>
                                <label data-filter=".s4"><span>4</span></label>
                                <label data-filter=".s5"><span>5</span></label>
                                <label data-filter=".s6"><span>6</span></label>
                            </span>
                        </span>
                    </span>
                    <span class="filterOptions" data-filter-group="shower">
                        <span class="selector" id="filter_toilet">
                            <small>Toilet/Shower</small>
                            <span class="btns">
                                <label class="selected" data-filter=".tlt, .notlt"><span>All</span></label>
                                <label data-filter=".tlt"><span>Yes</span></label>
                                <label data-filter=".notlt"><span>No</span></label>
                            </span>
                        </span>
                    </span>
                    <span class="filterOptions" data-filter-group="wheel">
                        <span class="selector" id="filter_wheel">
                            <small>2WD or 4WD</small>
                            <span class="btns">
                                <label class="selected" data-filter=".wna, .w2wd, .w4wd"><span>All</span></label>
                                <label data-filter=".w2wd"><span>2WD</span></label>
                                <label data-filter=".w4wd"><span>4WD</span></label>
                            </span>
                        </span>
                    </span>
                    <span class="filterOptions" data-filter-group="availability">
                        <span class="selector" id="filter_availability">
                            <small>Vehicle Availability</small>
                            <span class="btns">
                                <label data-filter=""><span>All</span></label>
                                <label data-filter=".avail" class="selected"><span>Available</span></label>
                                <label data-filter=".noavail"><span>Unavailable</span></label>
                            </span>
                        </span>
                    </span>
                    <!-- Original code before Changed by Nimesh on 23rd Aug 2014 to filter on multiple criteria rather then just one -->
                    <%--<span class="filterOptions">
                                    <span class="selector" id="filter_sleeps">
                                        <small>sleeps</small>                        
                                        <span class="btns">    
                                            <label class="selected" data-filter=".s2, .s3, .s4, .s5, .s6"><span>All</span></label>
                                            <label data-filter=".s2"><span>2</span></label>
                                            <label data-filter=".s3"><span>3</span></label>
                                            <label data-filter=".s4"><span>4</span></label>
                                            <label data-filter=".s5"><span>5</span></label>
                                            <label data-filter=".s6"><span>6</span></label>
                                        </span>
                                    </span>
                                    <span class="selector" id="filter_toilet">
                                        <small>Toilet/Shower</small>                        
                                        <span class="btns">                           
                                            <label data-filter=".tlt, .notlt"><span>All</span></label>
                                            <label data-filter=".tlt"><span>Yes</span></label>
                                            <label data-filter=".notlt"><span>No</span></label>
                                        </span>
                                    </span>
                                    <span class="selector" id="filter_wheel">
                                        <small>2WD or 4WD</small>                        
                                        <span class="btns">                           
                                            <label data-filter=".wna, .w2wd, .w4wd"><span>All</span></label>
                                            <label data-filter=".w2wd"><span>2WD</span></label>
                                            <label data-filter=".w4wd"><span>4WD</span></label>
                                        </span>
                                    </span>
                                    <span class="selector" id="filter_availability">
                                        <small>Vehicle Availability</small>                        
                                        <span class="btns">                           
                                            <label data-filter=""><span>All</span></label>
                                            <label data-filter=".avail"><span>Available</span></label>
                                            <label data-filter=".noavail"><span>Unavailable</span></label>
                                        </span>
                                    </span>
                            </span>--%>
                    <!-- End Original code before Changed by Nimesh on 23rd Aug 2014 to filter on multiple criteria rather then just one -->
                    <!-- End Changed by Nimesh on 23rd Aug 2014 to filter on multiple criteria rather then just one -->
                    <!-- /END wrap filters -->
                </div>
                <div class="sortPanel">
                    <span class="lbl">Sort by:</span>
                    <!-- wrap sorting -->
                    <span class="filterOptions">
                        <!--
                                    <span class="selector" id="sort_price">
                                        <small>price $</small>                        
                                        <span class="btns">    
                                            <label data-filter="desc"><span>High to Low</span></label>
                                            <label data-filter="asc"><span>Low to High</span></label>                            
                                        </span>
                                    </span>                          
                                    <span class="selector" id="sort_length">
                                        <small>vehicle length</small>                        
                                        <span class="btns">    
                                            <label data-filter="desc"><span>Long to Short</span></label>
                                            <label data-filter="asc"><span>Short to Long</span></label>                            
                                        </span>
                                    </span>
                                    -->
                        <span id="selectorPanel">
                            <small>Sleeps, Price & Length</small>
                            <select id="sortSelector" class="combo">
                                <option value="price_asc">Price - Low to High</option>
                                <option value="price_desc">Price - High to Low</option>
                                <option value="sleeps_asc">Sleeps - Low to High</option>
                                <option value="sleeps_desc">Sleeps - High to Low</option>
                                <option value="length_asc">Length - Short to Long</option>
                                <option value="length_desc">Length - Long to Short</option>
                            </select>
                        </span>
                    </span>
                    <!-- /END wrap sorting -->
                </div>
            </div>
        </div>
        <!-- wrap logos & vehicles -->

        </div>
        
        <div id="vehicleCatalogAll">
            <div id="vehicleCatalogHead">
                <div class="logos">
                    <ul>
                        <li><span rel="kea" class="kea"></span></li>
                        <li><span rel="maui" class="maui"></span></li>
                        <li><span rel="united" class="united"></span></li>
                        <li><span rel="britz" class="britz"></span></li>
                        <li><span rel="alpha" class="alpha"></span></li>
                        <li><span rel="mighty" class="mighty"></span></li>
                        <li><span rel="econo" class="econo"></span></li>
                    </ul>
                </div>
                <div class="arrow">
                    <span class="left"></span>
                    <span class="line"></span>
                    <span class="right"></span>
                </div>
                <div class="prem-econo">
                    <span class="premium">Premium</span>
                    <span class="econo">Economy</span>
                </div>
            </div>
            <div id="vehicleCatalogContainer">
                <div id="vehicleCatalog">
                    <%= VehicleCatalog %>
                </div>
            </div>
        </div>
        <div id="promoContainer">
            <%= AgentPromo %>
        </div>
        <!-- /END wrap logos & vehicles -->
        <div id="footer">
            <div class="top">
                <div id="disclaimer">
                    <span><strong>Please note:</strong> Rentals prices quoted above are only vaild at the time of booking.</span>
                    <span>Alternative currency is based on approximate current exchange rates. All payments will be processed in <%= Currency %>.</span>
                </div>
                <div id="footerlogo">
                    <a href="<%= ImageURL %>">
                        <%--<a href="#">--%>
                        <!-- Changed by Nimesh on 23rd Aug 2014 to pick up images from the server rather than local -->
                        <img src="<%= AssetsURL %>/images/<%= BrandChar %>/icons/foot_logo.png" alt="Worldwide Motorhomes Rentals" /></a>
                    <!-- End Changed by Nimesh on 23rd Aug 2014 to pick up images from the server rather than local -->
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        </div>
        <div id="footerPre"></div>
        <div id="footerWrap">
            <!-- virgin start -->
            <div id="brandedFooter" class="body">
                <%= BrandedFooter %>
            </div>
            <!-- virgin end -->
            <div class="thl body">
                <ul>
                    <li>
                        <a class="thl-logo" href="http://www.thlonline.com">
                            <span class="thl"></span>
                        </a>
                    </li>
                    <li>
                        <a class="thllogos" href="http://www.keacampers.com"><span class="kea"></span></a>
                    </li>
                    <li>
                        <a class="thllogos" href="http://www.maui-rentals.com"><span class="maui"></span></a>
                    </li>
                    <li>
                        <a class="thllogos" href="http://www.britz.com"><span class="britz"></span></a>
                    </li>
                    <li>
                        <a class="thllogos" href="http://www.unitedcampervans.com"><span class="united"></span></a>
                    </li>
                    <li>
                        <a class="thllogos" href="http://www.alphacampers.com"><span class="alpha"></span></a>
                    </li>
                    <li>
                        <a class="thllogos" href="http://www.mightycampers.com"><span class="mighty"></span></a>
                    </li>
                </ul>
                <span class="privacy">
                    <b>&copy; Tourism Holidings Limited</b><br />
                    &nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Privacy Policy</a>
                </span>
            </div>
        </div>
        <%= DNAIMGTag %>
    </form>
    <%= RemarketingCode %>
    <script type="text/javascript" lang="javascript">
        jQuery(document).ready(function () {
            
            //debugger;
            initParams = updateRequest(<%= InitJSON %>);
            rqst = new B2CRequest(initParams);
            initCS({'request': rqst});    
            filterbranches();            
        });
    </script>
</body>
</html>
