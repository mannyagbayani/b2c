﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;


namespace THL.Booking
{
    public partial class BookingControlPanel : System.Web.UI.Page
    {
        public string CountryCode;
        public string VehicleTypeStr, VCodeStr;
        public VehicleType requiredVType;
        public string VehicleCode;
        public string CountryOfResidence;
        public string PickUpDD, DropOfDD, VehicleDD, PickUpTimeDD, DropOffTimeDD, numAdultDD, numChildrenDD;
        public string BaseURL;
        public string ContentURL;
        public string MaxTravellers;
        public string PanelTitle;
        public string IsDisabled = string.Empty;
        public string Brand;

        private CacheManager cm = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            ContentURL = System.Web.Configuration.WebConfigurationManager.AppSettings["BookingControlContentURL"];


            if (Request.Params["brand"] != null) {
                Brand = Request.Params["brand"];
            }

            if (Request.Params["cc"] != null)
            {//TODO: validate input
                CountryCode = Request.Params["cc"].ToUpper();
            }

            if (Request.Params["cr"] != null)
            {//TODO: validate input 
                CountryOfResidence = Request.Params["cr"].ToLower();
            }

            if (Request.Params["vt"] != null)
            {//TODO: validate input 
                VehicleTypeStr = Request.Params["vt"].ToLower();
            }

            if (Request.Params["vh"] != null)
            {//TODO: validate input
                VehicleCode = Request.Params["vh"].ToLower();
            }

            string dropOffLocation = string.Empty, pickUpLocation = string.Empty;
            if (Request.Params["dol"] != null)
            {//TODO: validate input
                dropOffLocation = Request.Params["dol"];
            }
            if (Request.Params["pul"] != null)
            {//TODO: validate input
                pickUpLocation = Request.Params["pul"];
            }


            VCodeStr = (VehicleCode != null && VehicleCode != string.Empty ? "class='VehicleModel'" : "");//add type class

            //init the App Cache
            bool disableCache = false;
            bool.TryParse(Request.Params["disableCache"], out disableCache);

            if (Application["BookingCacheManager"] == null || (disableCache))
            {
                //string CacheDataPath = System.Web.Configuration.WebConfigurationManager.AppSettings["BrandsDataPath"] + "/xml/bp" + ".xml";
                string HTTPCacheDataPath = System.Web.Configuration.WebConfigurationManager.AppSettings["RemoteBookingControlContentURL"] + "/xml/bp" + ".xml";
                string LocalCacheDataPath = System.Web.Configuration.WebConfigurationManager.AppSettings["LocalDataPath"];
                
                bool useLocalResource = true;//TODO: get From Config to define where data is coming from

                string CacheDataPath = (useLocalResource ? LocalCacheDataPath : HTTPCacheDataPath);
                cm = new CacheManager(CacheDataPath, useLocalResource);
                Application.Lock();
                Application["BookingCacheManager"] = cm;
                Application.UnLock();
            }
            else
            {
                cm = (CacheManager)(Application["BookingCacheManager"]);
            }

            requiredVType = (VehicleTypeStr == "rv" ? VehicleType.Campervan : VehicleType.Car);//TODO: define default landing..
            PanelTitle = (requiredVType == VehicleType.Campervan ? "Campervan" : "Car");

            if (CountryCode != null && CountryCode != string.Empty)
            {
                BranchCountryData bcd = cm.GetCountryData(CountryCode.ToUpper(), "bp");
                MaxTravellers = (requiredVType == VehicleType.Campervan ? bcd.MaxPassengersCamper : bcd.MaxPassengersCar).ToString();//TODO: verify
                string defaultTime = (requiredVType == VehicleType.Campervan ? bcd.CampersDefaultHour : bcd.CarsDefaultHour); 
                BranchDisplayer bd = new BranchDisplayer(cm.GetBranchesForBrand(THLBrand.GetBrandForString(Brand)));
                PickUpDD = bd.GetBranchDDForBrand(CountryCode, VehicleTypeStr, "ctrlSearchBox_dropPickUpLocation", BranchType.PickUp, pickUpLocation );
                DropOfDD = bd.GetBranchDDForBrand(CountryCode, VehicleTypeStr, "ctrlSearchBox_dropDropOffLocation", BranchType.DropOff, dropOffLocation);
                PickUpTimeDD = bd.GetOpeningHoursDD(bcd, requiredVType, defaultTime, "ctrlSearchBox_dropPickUpTime");
                DropOffTimeDD = bd.GetOpeningHoursDD(bcd, requiredVType, defaultTime, "ctrlSearchBox_dropDropOffTime");
                VehicleDD = bd.GetVehiclesDDForBranch(requiredVType, cm.GetBrand(THLBrand.GetBrandForString(Brand)), VehicleCode, CountryCode);
                numAdultDD = bd.GetPassengerDD(PassengerType.Adult, (requiredVType == VehicleType.Campervan ? bcd.MaxAdultsCamper : bcd.MaxAdultsCar), "ctrlSearchBox_dropAdults");
                numChildrenDD = bd.GetPassengerDD(PassengerType.Child, (requiredVType == VehicleType.Campervan ? bcd.MaxChildrenCamper : bcd.MaxChildrenCar), "ctrlSearchBox_dropChildren");
            }
            else
            {
                PickUpDD = "<select disabled='disabled' class='SearchBox_FullListBox' id='ctrlSearchBox_dropPickUpLocation' name='ctrlSearchBox_dropPickUpLocation'><option>Select pick up location</option></select>";
                DropOfDD = "<select disabled='disabled' class='SearchBox_FullListBox' id='ctrlSearchBox_dropPickUpLocation' name='ctrlSearchBox_dropPickUpLocation'><option>Select drop off location</option></select>";
                PickUpTimeDD = "<select disabled='disabled' name='ctrlSearchBox_dropPickUpTime' id='ctrlSearchBox_dropPickUpTime' class='SearchBox_TimeListBox'><option value='10:00'>10:00am</option></select>";
                DropOffTimeDD = "<select disabled='disabled' name='ctrlSearchBox_dropPickUpTime' id='ctrlSearchBox_dropPickUpTime' class='SearchBox_TimeListBox'><option value='10:00'>10:00am</option></select>";
                VehicleDD = "<select disabled='disabled' class='SearchBox_FullListBox' id='ctrlSearchBox_dropVehicleType' name='ctrlSearchBox_dropVehicleType'><option value='-1'>Search all</option></select>";
                numAdultDD = "<select disabled='disabled' class='SearchBox_AdultsListBox' id='ctrlSearchBox_dropAdults' name='ctrlSearchBox_dropAdults'><option value='0' selected='selected'>Adults</option></select>";
                numChildrenDD = "<select disabled='disabled' class='SearchBox_AdultsListBox' id='ctrlSearchBox_dropChildren' name='ctrlSearchBox_dropChildren'><option value='0' selected='selected'>Children</option></select>";
                IsDisabled = "disabled='disabled'";
            }
        }
    }
}