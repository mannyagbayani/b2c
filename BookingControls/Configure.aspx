﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Configure.aspx.cs" Inherits="Configure" %>
<%@ Register Src="HorizontalControl.ascx" TagName="HorizontalControl" TagPrefix="thl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Configure Vehicle Page</title>    
    <link href="<%= AssetsURL %>/css/Base.css?v=1.7" type="text/css" rel="stylesheet" />     
    <link href="<%= CSSLink %>" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" media="all" href="<%= AssetsURL %>/css/<%= brandStr %>/horizontal.css" />
    <script type="text/javascript" src="<%= ContentURL %>js/jquery-1.3.1.min.js"></script>   
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.dimensions.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.tooltip.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/date.js"></script>     
    <script type="text/javascript" src="<%= ContentURL %>js/hCtrl.js?v=1.7"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/selection.js?v=1.7"></script>   
    <script src="<%= ContentURL %>js/jquery-ui.min.js" type="text/javascript"></script>    
    <link href="<%= AssetsURL %>/css/jquery.ui.dialog.css" rel="stylesheet" type="text/css"/>      
    <link href="<%= AssetsURL %>/css/<%= brandStr %>/jcal.css" type="text/css" rel="stylesheet" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <%= AnalyticsCode %>  
    <%= OpenTag %>    
</head>
<body>
    <form id="form1" runat="server" onsubmit="return serializeBookingInfo()">
        <%= DNAIMGTag %>
        <script type="text/javascript">
            var ContentbaseUrl = '<%= ContentURL %>';
            var currencyStr = '<%=  CurrencyStr %>';
            var ctrlParams = <%= HorizontalControl1.RequestParams.ToJSON() %>;
            var vehicleCharges = <%= VehicleJSON %>;
            var extraHireItems = <%= ExtraHireItemsJSON %>;
            var insuranceProducts = <%= InsuranceProductJSON %>;
            var ferryItems = <%= FerryJSON %>;
            var availabilityItem = <%= AvailabilityJSON %>;            
            var allInclusive = <%= AllInclusiveJSON %>;
            var oneWayFeeWaiver = <%= OneWayFee %>;
            var $j = jQuery.noConflict();
            var requestParams = <%= AvailRequestJSON %>;
            jQuery(document).ready(function() { 
                initConfigurePage(); 
                $j("#bottomRight a").click(function() {
                    _gaq.push(['_trackEvent','UIEvent','ConfigureBookingControlSubmit']);                
                });
                
                $j('#insurancePanel h4').text('Accident Liability & Security Deposit:');
                $j('#insurancePanel #insOptionsTable tr td.price').each(function(){ if($j(this).html() == "Included") $j(this).html("");});
            
            });
        </script>
        <div id="vehicleConfigureContainer">
            <div id="catalogHeader">
                <span class="Image">
                    <img src="<%= AssetsURL %>/images/<%= brandStr %>/logo.jpg" alt="logo" />
                </span>
                <a class="priceMatch" href="javascript:openPriceMatchWindow();">Price match guarantee</a>
                <span class="ContactInfo">
                    <%= ContactHeaderText %> or <a href="<%= ParentSiteURL %>/contactus" target="_blank">contact us</a>
                </span>
            </div>
            <div id="bookingControlContainer">
                <div id="sslSeal">
                    <script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=<%= SSLSeal %>&amp;size=L&amp;lang=en"></script>                    
                </div>
                <thl:HorizontalControl ID="HorizontalControl1" VehicleType="rv" CountryOfResidence="" CountryCode="" VehicleCode=""  runat="server" Target="Selection.aspx" />
            </div>
            <div id="breadCrumbs">
                <span class="Crumbs">
                    <a href="<%= ParentSiteURL %>" class="HomeLnk">Home</a>&nbsp;&gt;&nbsp;<a href="javascript:history.back();" class="HomeLnk">Select your vehicle</a>&nbsp;&gt;&nbsp;Configure your vehicle
                </span>
                <ul id="stepsDisplayer">
                    <li>
                        <span id="PaymentStep">3.Payment</span>
                    </li>
                    <li>
                        <span id="ConfigureStep">2.Configure</span>
                    </li>
                    <li>
                        <span id="SelectStep">1.Select</span>
                    </li>
                </ul>
            </div>
            <div id="vehicleCatalogContainer">
            <h2>2. Configure your vehicle</h2>
            <div id="configurePanel" >                
                <div id="promptTextArea" class='<%= HasMsg ? "" : "Hide" %>'>
                    <span><%= VehicleDetailsText %></span>
                </div>
                <div id="topHeader">
                    <span class="CurrencyPanel">
                        <small>Alternative Currency:</small>
                        <%= CurrencySelector %> 
                    </span>                  
                </div>
                <div id="vehicleTopPanel">                
                    <div class="VehicleHeader">
                        <h3>
                            <a class='VehicleTitle PopUp' href='http://<%= VehicleContentLink %>'>
                                <%= BrandTitle %> <%= SelectedVehicle.Name %>
                            </a>
                        </h3>
                        <h4>Travel details:&nbsp;</h4>
                        <div class="Locations">
                            <span><b>Pick up:</b> <%= PickUpLocationStr %>, <%= PickUpDateStr %></span>
                            <span><b>Drop off:</b> <%= DropOffLocationStr%>, <%= DropOffDateStr %></span>                            
                        </div>
                    </div>
                    <div class="VehicleImages">                    
                        <div class="Left">
                            <img src="<%= VehicleInclusionPath %>-InclusionIcons.gif" alt="<%= SelectedVehicle.Name %> inclusions" title="<%= SelectedVehicle.Name %> inclusions"/>                    
                        </div>
                        <div class="Right">
                            <img src="<%= VehicleImagePath %>-ClearCut-144.jpg" title="<%= SelectedVehicle.Code %>" alt="<%= SelectedVehicle.Code %>" />
                        </div>                                      
                    </div>                    
                </div>
                <div id="vehicleChargeDetails" class="">
                    <h4>Vehicle charges:&nbsp;</h4>
                    <a href="http://<%= VehicleChargeContentURL %>" class="Quest PopUp">?</a>
                    <span id="ChargeHeader">
                        <span class="Title">                            
                            <small>Vehicle Charge</small>
                            <a href="#details" class="ShowPriceList">Close Details</a>
                        </span>
                        <span class="Total">
                            <small><%= VehiclePriceStr %></small>
                        </span>
                    </span>                    
                    <%= PriceDetailsDiv %>                    
                    <%= AllInclusivePanel %>                     
                </div>
                <%= InusranceOptionsStr %>
                <div id="hireItemsPanel">                    
                    <%= ExtraHiresTable %>                
                </div>
                <div id="ferryPanel">                
                    <%= FerryPanel %>
                    <input type="hidden" id="ferryInfo" name="ferryInfo" />                                    
                </div>
                <div id="configureUpdatePanel">
                    <span class="Total">
                        <label>Current total:</label>
                        <span class="Price">                            
                            <small id="totalAmount">0.00</small>
                            <small id="alternateCurrency">0.0</small>
                        </span>
                    </span>
                    <span class="TaxMsg <%= (AdminFee > 0 && !hasBonusPack ? "HasAdminFee" : "") %>">
                        <small>Total includes all taxes (GST)</small>
                        <big>Includes <%= AdminFee %>% Admin Fee</big>
                    </span>
                    <span class="Submit">
                        <a class="BackBtn" href="javascript:history.back();">Back</a>                        
                        <input type="submit" value="Payment Details"/>
                        <a id="saveQuoteLnk" href="javascript:saveQuote();" title="Save your quote">Save Quote</a>                                                                       
                        <input type="hidden" id="bookingSubmitted" name="bookingSubmitted" value="0" />
                    </span>
                </div>              
            </div>            
        </div>                  
            <div id="disclaimer">
                <span>Please note: Rental prices quoted above are only valid at the time of booking.</span>
            </div>
            <!-- Debug Code Starts -->
            <%= debugHTML %>
            <!-- Debug Code Ends -->
            <input type="hidden" id="vehicleBrand" name="vehicleBrand" value="<%= VehicleBrand %>" />
            <input type="hidden" id="ctrlCOR" name="ctrlCOR" value="<%= CTRLCoR %>" />
        </div>
    </form>
    <%= RemarketingCode %>           
</body>
</html>