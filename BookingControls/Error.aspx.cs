﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Error : System.Web.UI.Page
{
    public string TopTLD;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        TopTLD = Request.Url.Host;
    }
}
