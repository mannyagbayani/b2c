﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class Configure : System.Web.UI.Page
{
    public string brandStr, vehicleBrandStr, CurrencyStr, VehicleImagePath, VehicleInclusionPath;
    public string ContentURL, AssetsURL;

    public string AnalyticsCode;

    public string FerryPanel;

    public string PriceDetailsDiv;
    
    public string VehicleDetailsText;//Top Message
    public bool HasMsg = true;
    public string PickUpDateStr;
    public string DropOffDateStr;
    public string AvailabilityJSON;
    public string VehiclePriceStr;
    public string CurrencySelector;
    public string VehicleChargeContentURL,VehicleContentLink;

    public string ExtraHiresTable, ExtraHireItemsJSON, FerryJSON, InusranceOptionsStr, InsuranceProductJSON, AllInclusivePanel, AllInclusiveJSON, VehicleJSON;

    public Vehicle SelectedVehicle;

    public string DNAIMGTag, ParentSiteURL;
    
    public decimal AdminFee = 0.0m;

    public string AvailRequestJSON = "null";

    ApplicationData appData;

    public string debugStr, debugURL, debugHTML;

    public string VehicleImageConvension;

    public string SSLSeal;

    public string OneWayFee;

    public string BrandTitle;

    public string VehicleBrand, CTRLCoR;

    public string ContactHeaderText;

    public string HTTPPrefix;

    public bool hasBonusPack;

    public string ThemeBaseStr { get; set; }
    public string RemarketingCode { get; set; }
    public string CSSLink { get; set; }
    public string OpenTag { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {

       //try
       //{
            bool secureMode = false;//TODO: move to config manager,, managed globally
            bool.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["SecureMode"], out secureMode);

            HTTPPrefix = ApplicationManager.HTTPPrefix;

            THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);

            if (appDomain == null)//dubug line
                appDomain = new THLDomain("secure.maui.co.nz", THLBrands.Maui, CountryCode.NZ, "B2CNZ", false, "uahere");//dubug line
            brandStr = appDomain.BrandChar.ToString();
            ParentSiteURL = appDomain.ParentSite;

            //DecideDNA Tracking Img
            TrackingDisplayer trd = new TrackingDisplayer(appDomain, secureMode);
            DNAIMGTag = trd.RenderDNATrackingImg("configure");
            SSLSeal = trd.GetSealID();
        
            //marin tracking
            DNAIMGTag += trd.RenderMarinTrackingPixelIMG(SessionManager.MarinTrackingParams, MarinStep.CONFIGURE, 0.0m, string.Empty, string.Empty);

            //Analytics Tracking Code
            AnalyticsCode = trd.RenderAnalsCode();
            RemarketingCode = trd.RenderRemarketingSnippets(); 

            ContentURL = HTTPPrefix + appDomain.Name + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["BookingControlContentPath"];
            string countryCode = appDomain.Country.ToString().ToUpper();  //(Request.Params["cc"] != null ? Request.Params["cc"] : "NZ").ToUpper();   


            //ContentURL = System.Web.Configuration.WebConfigurationManager.AppSettings["BookingControlContentURL"];
            AssetsURL = HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];
            string avpId = Request.Params["avp"];
            SessionData sessionData = SessionManager.GetSessionData();
            appData = ApplicationManager.GetApplicationData();

            string BrandChar = appDomain.BrandChar.ToString();
            CSSLink = string.IsNullOrEmpty(Request.Params["cssLink"]) ? AssetsURL + "/css/" + BrandChar + "/selection.css" : Request.Params["cssLink"];
        
            if (sessionData != null && sessionData.HasRequestData && sessionData.HasResponseData)//any session
            {
                if (IsPostBack && (Request.Params["bookingSubmitted"] == "1") || Request.Params["bookingSubmitted"] == "2")//Move to payment step
                {
                    AvailabilityResponse aRes = sessionData.AvailabilityResponseData;
                    AvailabilityItem ai = aRes.GetAvailabilityItem(avpId);
                    AvailabilityRequest aReq = sessionData.AvailabilityRequestData;
                    
                    if(ai == null)
                        Response.Redirect("Selection.aspx?msg=sessionExpired&cc=" + (aReq != null ? aReq.CountryCode.ToString() : string.Empty));

                    AuroraBooking submitedData = new AuroraBooking(Request.Params, avpId, ai);
                    PackageManager pm = new PackageManager(ai, appData);//TODO: save this call by cache managing inside PackageManager(cache key is avpId)                    
                    
                    sessionData.AvailableCards = pm.GetAvailableCards(avpId);                    
                    
                    submitedData.BonusPackSelected = (Request.Params["hasBonusPack"] == "1");

                    submitedData.UpdateProductDefinitions(pm, avpId);
                    if (submitedData.BonusPackSelected)
                    {//swap with the inclusives AVP ID
                        submitedData.InclusiveAvpID = pm.GetInclusiveAvpId();
                        submitedData.InclusivePack = pm.GetInclusiveProduct(avpId);
                        submitedData.SetInclusive(pm);
                    }

                    submitedData.WaiveOneWayFee = pm.GetInclusiveOneWayFeeWaived();
                    submitedData.OneWayFeeComponent = ai.OneWayFeeComponent;
                    
                    submitedData.CheckOutTime = aReq.PickUpDate;
                    submitedData.CheckInTime = aReq.DropOffDate;
                    submitedData.VehicleCode = ai.VehicleCode;
                    submitedData.AvailabilityType = ai.AvailabilityType;
                    submitedData.BookingTermAndConditionsLink = ai.TandCLink;

                    submitedData.AgentReference = appDomain.AgentCode;
                    submitedData.UserCode = SessionManager.AgentReference; 
                    submitedData.ChannelCode = aReq.ChannelCode;

                    submitedData.RequiresLoyaltyNumber = ai.DiplayLoyaltyCard;//Added for NRMA Support
                    
                    submitedData.PackageCode = ai.PackageCode;

                    submitedData.AdminFee = ai.AdminFee;
                    submitedData.CountryCode = aReq.CountryCode;
                    submitedData.Brand = ai.Brand;
                    //ai.TODO: pass terms and conditions link into booking(submitedData)
                    bool submittedDataSuccess = sessionData.UpdateBookingData(submitedData);


                    if(Request.Params["bookingSubmitted"] == "2")//added 19/07/2010 for save quote
                        Response.Redirect("SaveQuote.aspx");
                    else
                        Response.Redirect("Payment.aspx");
                    
                }
                else//first timer
                {                    
                    //--debug Code
                    Session["requestURL"] = "";//debugCode
                    Session["debugStr"] = "";//debugCode
                    AvailRequestJSON = (SessionManager.GetSessionData().HasRequestData ? SessionManager.GetSessionData().AvailabilityRequestData.ToJSON() : "null");//load ctrl from session
                    //--debug code
                    
                    hasBonusPack = (Request.Params["hasBonusPack"] == "1");

                    if (avpId != null)//TODO: verify AVP valid and exists within sessions <AvailabilityResponse>
                    {
                        if (sessionData.HasRequestData && sessionData.HasResponseData)//render configure state
                        {                            
                            AvailabilityRequest aReq = sessionData.AvailabilityRequestData;

                            ThemeBaseStr = AgentTheme.CSSPath(SessionManager.AgentCode, brandStr);//support for themed agents  

                            CurrencyCode defaultCurrency = (countryCode == "NZ" ? CurrencyCode.NZD : CurrencyCode.AUD);//TODO: MaC update here
                            CurrencyManager curMan = new CurrencyManager(CurrencyConversionProvider.BNZ, defaultCurrency);//todo: from appManager
                            CurrencySelector = new AvailabilityResponseDisplayer(null, aReq, curMan, AssetsURL, appDomain.Brand).RenderCurrencyDropDown(defaultCurrency);
                            
                            //CurrencyStr = (aReq.CountryCode == CountryCode.NZ ? "NZD" : "AUD"); //TODO: get from originating site
                            CurrencyStr = CurrencyManager.GetCurrencyForCountryCode(aReq.CountryCode).ToString();

                            AvailabilityResponse aRes = sessionData.AvailabilityResponseData;
                            AvailabilityItem ai = aRes.GetAvailabilityItem(avpId);                                  
                            if(ai == null)
                                Response.Redirect("Selection.aspx?msg=sessionExpired");

                            ThemeBaseStr = AgentTheme.CSSPath(SessionManager.AgentCode, brandStr);//support for themed agents 

                            VehicleBrand = THLBrand.GetBrandChar(ai.Brand).ToString();

                            CTRLCoR = sessionData.CountryOfResidence;  //refactored 25/10/10 from: aReq.CountryOfResidence.ToUpper();
                            
                            AdminFee = ai.AdminFee;                           

                            THLBrands containerBrand = THLBrand.GetBrandForString(aReq.Brand);

                            BrandTitle = THLBrand.GetNameForBrand(ai.Brand);

                            VehicleType contentResourceVehicleType = aReq.VehicleType;
                            if (ai.VehicleCode.ToLower().Contains("pfmr")) contentResourceVehicleType = VehicleType.Campervan;//Added exception 08/01/10 for PFMR

                            VehicleChargeContentURL = ContentManager.GetLinkElementForBrand(ai.Brand, contentResourceVehicleType, aReq.CountryCode, "VehicleCharges", aReq.PickUpDate, containerBrand).Content;
                            if (VehicleChargeContentURL != null && VehicleChargeContentURL.Length > 0) VehicleChargeContentURL = VehicleChargeContentURL.Replace("&amp;", "&");
                            
                            VehicleContentLink = ContentManager.GetVehicleLink(ai.Brand, aReq.CountryCode, ai.VehicleCode, containerBrand);
                            if (VehicleContentLink != null && VehicleContentLink.Length > 0) VehicleContentLink = VehicleContentLink.Replace("&amp;", "&");

                            VehicleDetailsText = (ai.PackageInfoText != null ? ai.PackageInfoText : string.Empty) + " " +( ai.VehicleMsgText != null ? ai.VehicleMsgText : string.Empty);
                            HasMsg = (VehicleDetailsText != null && VehicleDetailsText.Length > 0);
                            
                            ContactHeaderText = ContentManager.GetMessageForCode(THLBrand.GetBrandForString(brandStr), appDomain.Country, "ContactText").Content + "<br />"
                            + ContentManager.GetMessageForCode(THLBrand.GetBrandForString(brandStr), appDomain.Country, "DomesticPhone").Content + " - "
                            + ContentManager.GetMessageForCode(THLBrand.GetBrandForString(brandStr), appDomain.Country, "InternationalPhone").Content;

                            initControl(aReq);
                            
                            SelectedVehicle = ai.GetVehicle();
                                                       
                            PackageManager pm = new PackageManager(ai, appData);
                            AvailabilityItem bonusPackAvailability = pm.GetInclusiveVehicleCharges(avpId);

                            //031109
                            decimal oneWayFeeWaive = 0;
                            if (ai.OneWayFeeComponent > 0 && hasBonusPack && pm.GetInclusiveOneWayFeeWaived())
                                oneWayFeeWaive = ai.OneWayFeeComponent;
                            OneWayFee = oneWayFeeWaive.ToString();
                            //031109

                            vehicleBrandStr = (brandStr == "z" || brandStr == "c" ? THLBrand.GetBrandChar(ai.Brand).ToString() : brandStr);


                            VehicleImagePath = AvailabilityHelper.GetVehicleImageBasePath(aReq.CountryCode, ai.Brand, aReq.VehicleType, ai.VehicleCode, AssetsURL, false);
                            VehicleInclusionPath = AvailabilityHelper.GetVehicleImageBasePath(aReq.CountryCode, ai.Brand, aReq.VehicleType, ai.VehicleCode, AssetsURL, true);

                            //Vehicle charge
                            VehiclePriceStr = CurrencyStr + " $ " + String.Format("{0:N2}", (ai.EstimatedTotal - oneWayFeeWaive));
                            VehicleJSON = new VehicleDisplayer().VehicleChargesJSON(ai.EstimatedTotal, ai.EstimatedTotal + ((bonusPackAvailability != null && bonusPackAvailability.GetBonusPackCharge() != null) ? bonusPackAvailability.GetBonusPackCharge().ProductPrice : 0), hasBonusPack && (bonusPackAvailability != null));

                            //Extra Hires
                            ExtraHireItem[] EHItems = pm.GetExtraHireItems(avpId);
                            ExtraHireItemsDisplayer ehd = new ExtraHireItemsDisplayer(EHItems, ai.GetHirePeriod(), aReq, ai);
                            ExtraHiresTable = ehd.RenderExtraHirePanel();
                            ExtraHireItemsJSON = ehd.RenderExtraHireJSON();

                            //Insurance
                            InsuranceProduct[] inuranceItems = pm.GetInsuranceItems(avpId);
                            InsuranceProductDisplayer ipd = new InsuranceProductDisplayer(inuranceItems, ai, CurrencyStr, aReq);
                            InusranceOptionsStr = ipd.Render((hasBonusPack && (bonusPackAvailability != null)));
                            InsuranceProductJSON = ipd.RenderJSON(hasBonusPack && (bonusPackAvailability != null));

                            AllInclusiveJSON = ipd.RenderAllInlusiveJSON();

                            //Ferry
                            FerryProduct[] ferryProducts = pm.GetFerryProducts(avpId);
                            FerryProductDisplayer fpd = new FerryProductDisplayer(ferryProducts, aReq.PickUpDate, aReq.DropOffDate, CurrencyStr);
                            FerryPanel = fpd.RenderFerryPanel();
                            FerryJSON = fpd.RenderJSON();
                            
                            if (hasBonusPack && (bonusPackAvailability != null))//render configure in IsInclusive mode
                            {
                                PriceDetailsDiv = new AvailabilityResponseDisplayer(aRes, aReq, curMan, AssetsURL, appDomain.Brand).RenderConfigurePagePriceDetails(bonusPackAvailability);//TODO: rename to get GetInclusiveAvailabilityItem
                                AllInclusivePanel = ipd.RenderAllInlusivePanel(avpId, bonusPackAvailability, (bonusPackAvailability != null), pm.InclusiveName, pm.InclusiveDailyRate, pm.InclusiveEstimatedTotal, pm.HasDTR, ai.VehicleCode, pm);//bonus pack seleceted
                                AvailabilityJSON = new AvailabilityResponseDisplayer().RenderAvailabilityRowJSON(ai, bonusPackAvailability.GetBonusPackCharge().ProductPrice);
                            }
                            else
                            {
                                AllInclusivePanel = ipd.RenderAllInlusivePanel(avpId, null, (bonusPackAvailability != null), pm.InclusiveName, pm.InclusiveDailyRate, pm.InclusiveEstimatedTotal, pm.HasDTR, ai.VehicleCode, pm);//no bonus pack selected
                                PriceDetailsDiv = new AvailabilityResponseDisplayer(aRes, aReq, curMan, AssetsURL, appDomain.Brand).RenderConfigurePagePriceDetails(ai);
                                AvailabilityJSON = new AvailabilityResponseDisplayer().RenderAvailabilityRowJSON(ai, 0);
                            }
                        }
                        else//no Session data available, redirect 
                        {
                            Response.Redirect("Selection.aspx?msg=sessionExpired");
                        }
                    }
                }
            }
            else
            { 
                Response.Redirect("Selection.aspx?msg=sessionExpired");
            }
        //}
        //catch (Exception ex)
        //{
        //    string Message = ex.Message;
        //    THLDebug.LogError(ErrorTypes.Application, "Configure.Page_Load", ex.Message, "{}");
        //    Response.Redirect("Selection.aspx?msg=sessionExpired");
        //}

        //-------------------------------- Debug Zone -------------------------
        debugURL = "unittests/debugStr.aspx?ts=" + DateTime.Now.Millisecond;
        debugHTML = (Request.Params["debugOn"] != null && Request.Params["debugOn"] == "true" ? "<div id='debugSection' style='float: left; margin-top: 20px; width:757px; height: 800px; background-color: #fff;'><div id='debugValue'>" + debugStr + "</div><iframe src='" + debugURL + "' width='757px' height='800px' /></div>" : string.Empty);
        OpenTag = AvailabilityHelper.LoadOpenTag(appDomain);
    }
    
    public string PickUpLocationStr, DropOffLocationStr;

    void initControl(AvailabilityRequest aRequest)
    {
        PickUpLocationStr  = (appData.GetLocationStringForCtrlCode(aRequest.PickUpBranch) != "n/a") ? appData.GetLocationStringForCtrlCode(aRequest.PickUpBranch)  : aRequest.PickUpBranch;  
        DropOffLocationStr = (appData.GetLocationStringForCtrlCode(aRequest.DropOffBranch) != "n/a") ? appData.GetLocationStringForCtrlCode(aRequest.DropOffBranch) : aRequest.DropOffBranch;
        PickUpDateStr = aRequest.PickUpDate.ToString("dddd, dd MMMM yyyy HH:mm tt");
        DropOffDateStr = aRequest.DropOffDate.ToString("dddd, dd MMMM yyyy HH:mm tt");        
        HorizontalControl1.PUDate = aRequest.PickUpDate.ToString("dd-MMM-yyyy");
        HorizontalControl1.DODate = aRequest.DropOffDate.ToString("dd-MMM-yyyy");
        HorizontalControl1.Brand = aRequest.Brand;
        HorizontalControl1.CountryCode = aRequest.CountryCode.ToString();
        HorizontalControl1.PULoc = aRequest.PickUpBranch.ToUpper();
        HorizontalControl1.DOLoc = aRequest.DropOffBranch.ToUpper();
        HorizontalControl1.CountryOfResidence = aRequest.CountryOfResidence.ToUpper();
        HorizontalControl1.VCode = aRequest.VehicleModel;
        HorizontalControl1.VehicleTypeStr = (aRequest.VehicleType == VehicleType.Car ? "ac" : "av");      

    }
}