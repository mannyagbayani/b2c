﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class HorizontalDemo : System.Web.UI.Page
{
    public string ContentURL;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        ContentURL = System.Web.Configuration.WebConfigurationManager.AppSettings["BookingControlContentURL"];            

    }
}
