﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="B2BSelect.aspx.cs" Inherits="B2BSelectionPage" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Campervans Availability Selection Page1</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/js/cbox.js"></script>
    <!--<script type="text/javascript" src="/js/isotope.js"></script>-->
    <script type="text/javascript" src="/js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="/js/jstorage.min.js"></script>
    <script type="text/javascript" src="/js/b2b.js?v=999"></script>


    <%--<script type="text/javascript" src="http://local.maui.co.nz/js/b2b.js"></script>--%>



    <script type="text/javascript">
        var sortBy = '', filterBy = '', isAsc = true;
        var rqstParams = <%= RequestJSON %>;
        var ctrlParams = <%= ctrlParams %>;
        var domain = <%= DomainJSON %>;
        var agt = <%= AgentJSON %>;
        var rqst = null;
        var initParams = null;
        jQuery(document).ready(function () {
            initParams = updateRequest(<%= InitJSON %>);
            rqst = new B2CRequest(initParams);
            initCS({'request': rqst});                
        });


        

    </script>


    <style>
        .bluehighlight {
            background-color: rgb(143, 205, 242);
            color: rgb(0, 0, 0);
            font-weight: bold;
        }
    </style>

    <link rel="stylesheet" href="<%= CSSLink %>" />
    <%--<link rel="stylesheet" href="http://localhost:2873/css/b2b.css" />--%>

    <%= AnalyticsCode %>
</head>
<body class="<%= bodyClass %>" style="overflow: hidden;">
    <form id="form1" runat="server">
        <div id="container">
            <!-- Filter/Sort panel starts -->
            <div id="sortPanel">
                <div class="header">
                    <div class="left">
                        <h2>Filter &amp; Sort Vehicles</h2>
                        <small><em>Hide</em>&nbsp;<i class="icon-chevron-up icon-white"></i></small>
                    </div>
                    <div class="right">
                        <span class="alternative-currency">
                            <label>Alternative Currency</label>
                            <%= CurrencySelector %>
                        </span>
                        <label id="infoLbl" for="vehicleInfo">Vehicle Info/Images:</label>
                        <ul id="vehicleInfoSelector">
                            <li><span class="hideshow hide"><i class="icon-eye-close"></i>Hide</span></li>
                            <li><span class="hideshow active show"><i class="icon-eye-open"></i>Show</span></li>
                        </ul>
                    </div>
                </div>
                <div class="body">
                    <div class="filterPanel">
                        <span class="lbl">Filter by:</span>
                        <!-- wrap filters -->
                        <span class="filterOptions" data-filter-group="sleeps">
                            <span class="selector" id="filter_sleeps">
                                <small>sleeps</small>
                                <span class="btns">
                                    <label class="selected" data-filter=".s2, .s3, .s4, .s5, .s6"><span>All</span></label>
                                    <label data-filter=".s2"><span>2</span></label>
                                    <label data-filter=".s3"><span>3</span></label>
                                    <label data-filter=".s4"><span>4</span></label>
                                    <label data-filter=".s5"><span>5</span></label>
                                    <label data-filter=".s6"><span>6</span></label>
                                </span>
                            </span>
                        </span>
                        <span class="filterOptions" data-filter-group="shower">
                            <span class="selector" id="filter_toilet">
                                <small>Toilet/Shower</small>
                                <span class="btns">
                                    <label class="selected" data-filter=".tlt, .notlt"><span>All</span></label>
                                    <label data-filter=".tlt"><span>Yes</span></label>
                                    <label data-filter=".notlt"><span>No</span></label>
                                </span>
                            </span>
                        </span>
                        <span class="filterOptions" data-filter-group="wheel">
                            <span class="selector" id="filter_wheel">
                                <small>2WD or 4WD</small>
                                <span class="btns">
                                    <label class="selected" data-filter=".wna, .w2wd, .w4wd"><span>All</span></label>
                                    <label data-filter=".w2wd"><span>2WD</span></label>
                                    <label data-filter=".w4wd"><span>4WD</span></label>
                                </span>
                            </span>
                        </span>
                        <span class="filterOptions" data-filter-group="availability">
                            <span class="selector" id="filter_availability">
                                <small>Vehicle Availability</small>
                                <span class="btns">
                                    <label data-filter=""><span>All</span></label>
                                    <label data-filter=".avail" class="selected"><span>Available</span></label>
                                    <label data-filter=".noavail"><span>Unavailable</span></label>
                                </span>
                            </span>
                        </span>
                        <%--  <span class="filterOptions" >
                            <span class="selector" id="filter_sleeps">
                                <small>sleeps</small>
                                <span class="btns">
                                    <label class="selected" data-filter=".s2, .s3, .s4, .s5, .s6"><span>All</span></label>
                                    <label data-filter=".s2"><span>2</span></label>
                                    <label data-filter=".s3"><span>3</span></label>
                                    <label data-filter=".s4"><span>4</span></label>
                                    <label data-filter=".s5"><span>5</span></label>
                                    <label data-filter=".s6"><span>6</span></label>
                                </span>
                            </span>
                            <span class="selector" id="filter_toilet">
                                <small>Toilet/Shower</small>
                                <span class="btns">
                                    <label data-filter=".tlt, .notlt" class="selected"><span>All</span></label>
                                    <label data-filter=".tlt"><span>Yes</span></label>
                                    <label data-filter=".notlt"><span>No</span></label>
                                </span>
                            </span>
                            <span class="selector" id="filter_wheel">
                                <small>2WD or 4WD</small>
                                <span class="btns">
                                    <label data-filter=".wna, .w2wd, .w4wd" class="selected"><span>All</span></label>
                                    <label data-filter=".w2wd"><span>2WD</span></label>
                                    <label data-filter=".w4wd"><span>4WD</span></label>
                                </span>
                            </span>
                            <span class="selector" id="filter_availability">
                                <small>Vehicle Availability</small>
                                <span class="btns">
                                    <label data-filter=""><span>All</span></label>
                                    <label data-filter=".avail" class="selected"><span>Available</span></label>
                                    <label data-filter=".noavail"><span>Unavailable</span></label>
                                </span>
                            </span>
                        </span>--%>
                        <!-- /END wrap filters -->
                    </div>
                    <div class="sortPanel">
                        <span class="lbl">Sort by:</span>
                        <!-- wrap sorting -->
                        <span class="filterOptions">
                            <!--
                                    <span class="selector" id="sort_price">
                                        <small>price $</small>                        
                                        <span class="btns">    
                                            <label data-filter="desc"><span>High to Low</span></label>
                                            <label data-filter="asc"><span>Low to High</span></label>                            
                                        </span>
                                    </span>                          
                                    <span class="selector" id="sort_length">
                                        <small>vehicle length</small>                        
                                        <span class="btns">    
                                            <label data-filter="desc"><span>Long to Short</span></label>
                                            <label data-filter="asc"><span>Short to Long</span></label>                            
                                        </span>
                                    </span>
                                    -->
                            <span id="selectorPanel">
                                <small>Sleeps, Price & Length</small>
                                <select id="sortSelector" class="combo">
                                    <option value="price_asc">Price - Low to High</option>
                                    <option value="price_desc">Price - High to Low</option>
                                    <option value="sleeps_asc">Sleeps - Low to High</option>
                                    <option value="sleeps_desc">Sleeps - High to Low</option>
                                    <option value="length_asc">Length - Short to Long</option>
                                    <option value="length_desc">Length - Long to Short</option>
                                </select>
                            </span>
                        </span>
                        <!-- /END wrap sorting -->
                    </div>
                </div>
            </div>
            <!-- wrap logos & vehicles -->
            <div id="vehicleCatalogAll">
                <div id="vehicleCatalogHead">
                    <div class="logos">
                        <ul>
                            <li><span rel="kea" class="kea"></span></li>
                            <li><span rel="maui" class="maui"></span></li>
                            <li><span rel="united" class="united"></span></li>
                            <li><span rel="britz" class="britz"></span></li>
                            <li><span rel="alpha" class="alpha"></span></li>
                            <li><span rel="mighty" class="mighty"></span></li>
                            <li><span rel="econo" class="econo"></span></li>
                        </ul>
                    </div>
                    <!--
                        rev:mia may 13, 2014 - injection of code to display Gross  and NET AMOUNT 
                                               THL history: last day of niven  
                    -->

                    <div align="center" style="" id="availabilityFilter">


                        <span class="lblDisplay">Display:</span>
                        <span class="spanRadioAvailability">

                            <input type="radio" name="grsnett" value="gross" id="gnGross" />
                            Gross Amount
                            <input type="radio" name="grsnett" value="nett" id="gnNett" />
                            Nett Amount
                        </span>
                        &nbsp;
                        <span id="spancurrencySelectorb2b">
                            <label>
                                Alternative Currency
                            </label>
                            <select id="currencySelectorb2b">
                                <!-- sample data
                                    <option rel="AUD" value="1">Australian Dollar (AUD)</option>
                                    <option rel="GBP" value="1.545148">Pound Sterling (GBP)</option>
                                -->
                            </select>
                        </span>
                        <br />
                        <span id="spanPriceHighlight" class="bluehighlight"></span>
                    </div>

                    <div class="arrow">
                        <span class="left"></span>
                        <span class="line"></span>
                        <span class="right"></span>
                    </div>
                    <div class="prem-econo">
                        <span class="premium">Premium</span>
                        <span class="econo">Economy</span>
                    </div>
                </div>
                <div id="vehicleCatalogContainer">
                    <div id="vehicleCatalog">
                        <%= VehicleCatalog %>
                    </div>
                </div>
            </div>
        </div>

    </form>
    <script type="text/javascript" src="/js/b2bselect.js?v=999"></script>

</body>
</html>
