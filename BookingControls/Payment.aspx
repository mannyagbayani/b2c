﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Payment.aspx.cs" Inherits="Payment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Payment Page</title>    
    <link href="<%= AssetsURL %>/css/Base.css?v=1.7" type="text/css" rel="stylesheet" />    
    <link href="<%= AssetsURL %>/css/<%= ThemeBaseStr %>/selection.css?v=1.7" type="text/css" rel="stylesheet" />    
    <script type="text/javascript" src="<%= ContentURL %>js/jquery-1.3.1.min.js"></script>    
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.validate.js"></script>    
    <style type="text/css">
        label { width: 10em; float: left; }
        label.error { float: none; color: red; padding-left: .5em; vertical-align: top; }
    </style>    
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.dimensions.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.tooltip.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/date.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/hCtrl.js?v=1.7"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/selection.js?v=1.7"></script>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <%= AnalyticsCode %>  
    <%= OpenTag %>   
</head>
<body>
    <form id="paymentForm" onsubmit="return submitPaymentForm()" method="post">
        <%= DNAIMGTag %>
        <script type="text/javascript">
            var ContentbaseUrl = '<%= ContentURL %>';
            var SiteInfo = <%= SiteInfo %>;
            var currencyStr = '<%=  CurrencyStr %>';
            var currentSurcharge = <%= CurrentSurcharge %>;
            var vehicleCharges = <%= VehicleChargeJSON %>;
            var $j = jQuery.noConflict();
            jQuery(document).ready(function() { 
                initPaymentPage(); 
                //$j("span.payPeriod").each(function(){ if($j(this).html() == "(payable 150 days prior to vehicle collection)") $j(this).html("(payable on pickup)"); })//rwc
            });
        </script>
        <div id="paymentContainer">
            <div id="catalogHeader">
                <span class="Image">
                    <img src="<%= AssetsURL %>/images/<%= brandStr %>/logo.jpg" alt="logo" />
                </span>
                <span class="ContactInfo">
                    <%= ContactHeaderText %> or <a href="<%= ParentSiteURL %>/contactus" target="_blank">contact us</a>
                </span>
            </div>            
            <div id="breadCrumbs">
                <span class="Crumbs">
                    <a href="<%= ParentSiteURL %>" class="HomeLnk">Home</a>&nbsp;&gt;&nbsp;<a  href="javascript:history.go(-2);" class="HomeLnk">Select your vehicle</a>&nbsp;&gt;&nbsp;<a href="javascript:history.back();" class="HomeLnk">Configure your vehicle</a>&nbsp;&gt;&nbsp;Payment details
                </span>
                <ul id="stepsDisplayer">
                    <li>
                        <span id="PaymentStep">3.Payment</span>
                    </li>
                    <li>
                        <span id="ConfigureStep">2.Configure</span>
                    </li>
                    <li>
                        <span id="SelectStep">1.Select</span>
                    </li>
                </ul>
            </div>
            <div id="vehicleCatalogContainer">
            <h2>3. Payment Details</h2>
            <div id="paymentPanel">
               <div id="topHeader">
                    <span class="CurrencyPanel" style="margin-top:20px;">
                        <!--
                        <small>Alternative Currency:</small>
                        <%= CurrencySelector %>
                        -->
                    </span>                  
                </div>
                <div id="rentalSummary">
                    <h3>
                        <!--<a class='VehicleTitle' href='http://pathtocontentprovider/vehibclepage?vehicleCode=<%= SelectedVehicle.Code %>'>-->
                            <%= BrandTitle %> <%= SelectedVehicle.Name %>
                        <!--</a>-->
                    </h3>
                    <div id="vehicleDetails">
                        <div id="travelDetails">
                            <big>Travel details:</big>
                            <table class="TravelTable">
                                <tbody>
                                    <tr>
                                        <td class="lbl">Pick up:</td>
                                        <td class="date"><%= PickUpDateStr %></td>
                                        <td class="loc"><%= PickUpLocationStr %></td>
                                    </tr>
                                    <tr>
                                        <td class="lbl">Drop off:</td>
                                        <td class="date"><%= DropOffDateStr %></td>
                                        <td class="loc"><%= DropOffLocationStr %></td>
                                    </tr>
                                </tbody>
                            </table>                            
                        </div>
                        <div id="vehicleSpecs">
                            <img src="<%= VehicleImagePath %>-ClearCut-144.jpg" title="<%= VehicleCode %>" />
                            <!--<img src="<%= AssetsURL %>/images/<%= brandStr %>/vehicles/inclusions/<%= VehicleImageConvension %>.gif" title="<%= VehicleCode %> inclusions" />-->
                        </div>                        
                    </div>
                    <big class="ChargeSum">Rental charge summary</big>
                    <%= RentalSummary %>                    
                    <!--
                    <div class="Disclaimer MaCHirePeriodTerms <%= Lead45 %>">                        
                        <span class="More45">In order to confirm your booking, a deposit of 20% is required.  Final payment will be collected from the credit card details provided on your original booking request 45 days prior to travel.</span>
                        <span class="Less45">As your date of booking is within 45 days of travel full payment is required upon booking confirmation.</span>
                    </div>
                    -->                  
                    <div class="Disclaimer">                        
                        Booking cancellation fee applies, please refer to terms and conditions link below for details<br />
                        <span id="ccDesclaimerTxt">If paying final balance by credit card, the credit card surcharge will apply.</span>                        
                    </div>
                </div>
                <div id="creditCardDetails">
                    <h3>PAYMENT AND CUSTOMER DETAILS</h3>
                    <big>Credit card details: (required)</big>
                    <div id="creditFormContainer">
                        <div id="creditFormPanel">
                            <ul>
                                <li>
                                    <p>
                                        <label for="cardTypeDD">Card type:*</label>
                                        <%= CCDropDown %>
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <label for="cardNumberTxt">Card number:*</label>
                                        <input id="cardNumberTxt" name="cardNumberTxt" class="required creditcard ClickTaleSensitive" autocomplete="off"/>
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <label for="cardExpiryYearTxt">Expiry date:*</label>
                                        <span id="cardExpirySpan">
                                            <select  id="cardExpiryMonthTxt" name="cardExpiryMonthTxt">
                                                <option value="01">01</option>
                                                <option value="02">02</option>
                                                <option value="03">03</option>
                                                <option value="04">04</option>
                                                <option value="05">05</option>
                                                <option value="06">06</option>
                                                <option value="07">07</option>
                                                <option value="08">08</option>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                            <span>&nbsp;/&nbsp;</span>
                                            <select id="cardExpiryYearTxt" name="cardExpiryYearTxt">
                                                <option value="14">2014</option>
                                                <option value="15">2015</option> 
                                                <option value="16">2016</option>
                                                <option value="17">2017</option>   
                                                <option value="18">2018</option>
                                                <option value="19">2019</option> 
                                                <option value="20">2020</option>
                                                <option value="21">2021</option>                                                                        
                                            </select>                                                                               
                                        </span>
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <label id="cardVerifyLbl" for="cardNumberTxt">Card Security Code:*</label>
                                        <input id="cardVerify" name="cardVerify" class="number ClickTaleSensitive required" autocomplete="off" size="3" maxlength="3" />
                                        <a href="http://www.paymentexpress.com/securitycode.htm" class="PopUp">What is this?</a>
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        <label for="cardHolderName">Card holder name:*</label>
                                        <input id="cardHolderName" name="cardHolderName" minlength="2" class="required ClickTaleSensitive" />
                                    </p>
                                </li>                                
                            </ul>
                        </div>
                        <div id="sealPanel">
                            <!--<script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=<%= SSLSeal %>"></script>-->
                            <script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=<%= SSLSeal %>&amp;size=L&amp;lang=en"></script>
                        </div>                    
                    </div>                   
                </div>
                <div id="personalDetails">
                    <big>Personal details: (required)</big>
                    <ul>
                        <li>
                            <p>
                                <label for="customerTitleDD">Title:*</label>
                                <select id="customerTitleDD" name="customerTitleDD">
                                    <option value="Mr">Mr</option>
	                                <option value="Mrs">Mrs</option>
	                                <option value="Ms">Ms</option>
	                                <option value="Miss">Miss</option>
	                                <option value="Master">Master</option>
	                                <option value="Dr">Dr</option>
                                </select>
                            </p>
                        </li>
                        <li>
                            <p>
                                <label for="customerFirstName">First Name:*</label>
                                <input id="customerFirstName" name="customerFirstName" minlength="2" class="required ClickTaleSensitive" />
                            </p>
                        </li>
                        <li>
                            <p>
                                <label for="customerLastName">Last Name:*</label>
                                <input id="customerLastName" name="customerLastName" minlength="2" class="required ClickTaleSensitive" />
                            </p>
                        </li>
                        <li>
                            <p>
                                <span>Note: Please enter your email address carefully as this will be the address we send your confirmation to.</span>
                            </p>
                        </li>
                        <li>
                            <p>
                                <label for="emailAddressTxt">Email address:*</label>
                                <input id="emailAddressTxt" name="emailAddressTxt" class="required email ClickTaleSensitive"/>
                            </p>
                        </li>
                        <li>
                            <p>
                                <label for="confirmEmailAddressTxt">Confirm email:*</label>
                                <input id="confirmEmailAddressTxt" name="confirmEmailAddressTxt" class="required email ClickTaleSensitive" />
                            </p>
                        </li>
                        <li>
                            <p>
                                <label for="telephoneTxt1">Telephone:*</label>
                                <span class="PhoneNumber">
                                    <span class="number">
                                        <small>country code</small>
                                        <input id="telephoneTxt1" name="telephoneTxt1" size="10" class="required number"/>
                                    </span>                                    
                                    <span class="number">
                                        <small>area code</small>
                                        <input id="telephoneTxt2" name="telephoneTxt2" size="10" class="required number" />
                                    </span>
                                    <span class="number">
                                        <small>number</small>
                                        <input id="telephoneTxt3" name="telephoneTxt3" size="10" class="required number ClickTaleSensitive"/>
                                    </span>                                    
                                </span>
                            </p>
                        </li>                        
                        <!-- NRMA 21/06/10 starts -->                         
                        <%= LoyaltyCardHTML %>
                        <!-- NRMA 21/06/10 ends -->
                        <li style="display:none;">
                            <p>
                                <label for="customerAgeDD">Age:</label>
                                <select id="customerAgeDD" name="customerAgeDD">
                                    <option selected="selected" value="">Please Select</option>
		                            <option value="18-25">18-25</option>
		                            <option value="26-35">26-35</option>
		                            <option value="36-45">36-45</option>
		                            <option value="46+">46+</option>
                                </select>
                            </p>
                        </li>
                         <li>
                            <p>
                                <span>Any special requests or comments relating to your upcoming hire?</span>
                            </p>
                        </li>
                        <li>
                            <p>
                                <label for="customerNoteTxt">Enter here:</label>
                                <textarea id="customerNoteTxt" name="customerNoteTxt" cols="50" rows="5" onblur="this.value = this.value.replace(/[^a-zA-Z 0-9 \n]+/g,'')" class="ClickTaleSensitive"></textarea>
                            </p>
                        </li>                                             
                    </ul>
                </div>              
                <div id="termsAndConditions">
                    <big>Terms & Conditions</big>
                    <input type="checkbox" id="acceptedTerms" name="acceptedTerms" />
                    <span>YES - I have read and accept the rental <a class="PopUp" href="<%= TandCLink %>">terms and conditions</a> and acknowledge that I am responsible for obtaining all required travel documents and visas and for complying with all country entry/transit requirements.</span>
                </div>
                <div id="macTerms">
                    <span>YES - I have read and accept the <a class="PopUp" href="http://www.motorhomesandcars.com/terms-conditions-0910.aspx?template=vehicledetialpopup">MotorhomesAndCars.com Terms and Conditions</a></span>
                </div>                
                <div id="payTotalPanel">
                    <input type="hidden" id="payTotalCB" value="on"/>                    
                    <%--<input type="checkbox" id="payTotalCB" name="payTotalCB"/>
                    <span>Pay total amount today?</span>--%>
                </div>                
                <div id="depositPanel">
                    <span class="left">
                        <big class="PayDeposit"><%= (AddRWCMsg ? "Non refundable deposit" : "Deposit") %> (payable today)</big>
                        <big class="PayFull">Total charge <span class='payPeriod'>(payable today)</span></big>
                        <small class="PayDeposit"><span class='payPeriod'>Final balance (Full payment due 28 days before pick-up)</span></small>                        
                    </span>                    
                    <%= PaymentTotalPanel %>                    
                </div>
                <div id="footerPanel" class="<%= OnRequest %>">
                    <span>Total includes all taxes (GST)</span>
                    <span id="ccDesclaimerTxtBtm">If paying final balance by credit card, the credit card surcharge will apply.</span>
                    <span class="Buttons">
                        <a href="javascript:history.back();">Back</a>
                        <small class="right">
                            <label id="globalFormError">Please try again</label>                       
                            <input type="submit" class="SubmitBtn" value="Book Now" />
                            <input type="hidden" id="bookingCompleted" name="bookingCompleted" value="0"/>
                        </small>
                    </span>
                </div>
            </div>                        
        </div>                  
            <div id="disclaimer">
                <span>Please note: Rental prices quoted above are only valid at the time of booking.</span>
            </div>
            <!-- Debug Code Starts -->
            <%= debugHTML %>
            <!-- Debug Code Ends -->
        </div>
    </form>
    <%= RemarketingCode %>    
</body>
</html>
