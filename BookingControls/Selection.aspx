﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Selection.aspx.cs" Inherits="Selection" %>
<%@ Register Src="HorizontalControl.ascx" TagName="HorizontalControl" TagPrefix="thl" %>
<%@ Register Src="CrossSellHeader.ascx" TagName="CrossSellHeader" TagPrefix="thl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Selection Catalog Page</title>    
    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
    <link href="<%= AssetsURL %>/css/Base.css?v=<%= Version %>" type="text/css" rel="stylesheet" />
    <link href="<%= AssetsURL %>/css/<%= brandStr %>/selection_v1.css?v=<%= Version %>" type="text/css" rel="stylesheet" /> 
    <link rel="stylesheet" type="text/css" media="all" href="<%= AssetsURL %>/css/<%= brandStr %>/horizontal_v1.css?v=<%= Version %>" />   
    <!--<script src="<%= ContentURL %>js/jquery-1.3.1.min.js" type="text/javascript"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="<%= ContentURL %>js/jquery.dimensions.min.js" type="text/javascript"></script>
    <script src="<%= ContentURL %>js/jquery.tooltip.min.js" type="text/javascript"></script>
    <script src="<%= ContentURL %>js/date.js" type="text/javascript"></script>    
    <script src="<%= ContentURL %>js/hCtrl.js?v=<%= Version %>" type="text/javascript"></script>         
    <script src="<%= ContentURL %>js/selection.js?v=<%= Version %>" type="text/javascript"></script>
    <script src="<%= ContentURL %>js/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <link href="<%= AssetsURL %>/css/<%= brandStr %>/jcal.css?v=<%= Version %>" type="text/css" rel="stylesheet" />
    <script src="js/ui.core.js" type="text/javascript"></script>
    <script src="js/ui.carousel.js" type="text/javascript"></script>
    <script src="js/jquery.sticky.js" type="text/javascript"></script> 

    <script type="text/javascript">
        var ContentbaseUrl = '<%= ContentURL %>';
        var assetsBase = '<%= AssetsURL %>';
        var affCode = '<%= AffCode %>';
        var crossSellUrl = '<%= CrossSellURL %>';
        var brandId = '<%= brandStr %>';
        var ctrlParams =  <%= HorizontalControl1.RequestParams.ToJSON() %>;        
        var requestParams = <%= AvailRequestJSON %>;        
        var currencyStr = '<%=  CurrencyStr %>';        
        var defaultHours = {avOpen : '09:30', avClose : '17:30',acOpen : '10:30', acClose : '11:30'};
        var $j = jQuery.noConflict();
        jQuery(document).ready(function() {
            initSelectionPage(); 
            //resubmit the form if browser back is hit
            if(getCookie('history')==1){
                deleteCookie('history','','');
                $j('#vehicleCatalogContainer').hide();
                $j('#staticLoader').show();
                setTimeout("document.forms[0].submit()",1000);
            } 
                        
            t = window.setTimeout(function() {
                if($j("#staticLoader").is(':visible')){
                    $j("#carousel").carousel({
                        animateByMouse: false,
                        pausable: true,
                        radius: 250,
                        animate:0.004,
                        pauseSpeed:0
                    });
                }
                else{
                    window.clearTimeout(t);
                }
            }, $j.browser.safari ? 100 : 0); 
                     
            bindIsoTope($j('#vehicleCatalog'), $j('#sortPanel'), null);//sorting     
            $j("#resultsHeader").sticky({topSpacing:0});
                   
        });

    </script>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">    
    <%= AnalyticsCode %>
    <%= OpenTag %>
    <script type="text/javascript" src="/js/isotope.js"></script>
     <style>
        #sortPanel { float: right; margin-top: 4px; list-style: none; font-size:11px; margin-right:10px;}
		#sortPanel li {float:left; padding-top:4px; padding-left:5px; color:#666;}
		#sortPanel li.second {padding-left:15px;}
		#sortPanel select {width:150px;}
        #sortPanel a { float: left; display: block; padding-left: 20px; height: 15px; margin-right: 20px;}
        #sortPanel a.asc { background: transparent url(../../images/b/icons/price_list_expand.gif) no-repeat 2px 0px;}
        #sortPanel a.dsc { background: transparent url(../../images/b/icons/price_list_expand.gif) no-repeat 2px -15px; }        
        #resultsHeader h3 { width: 180px !important; }  
		#vehicleCatalogContainer h2 span {float:right; margin-right:5px; *margin-top:-17px;}
		#vehicleCatalogContainer h2 span small {font-size:11px; font-weight:normal;}  
		#vehicleCatalogContainer h2 #currencySelector {width:200px;} 
		#resultsHeader { z-index: 100000; }
		.isotope-hidden { display: none; } /* isotope hide fix */	
    </style>
</head>
<body class="selection">
    <form id="form1" runat="server">       
        <div id="thlCrossSellBanner">
                <thl:CrossSellHeader ID="CrossSellHeader1" runat="server" />    
        </div>
        <div id="vehicleSelectionContainer" class="<%= CCode %>">
            <div id="catalogHeader">
                <span class="Image">
                    <a href="<%= ParentSiteURL %>">
                        <img src="<%= AssetsURL %>/images/<%= brandStr %>/logo.jpg" alt="logo" />
                    </a>
                </span>
                <a class="priceMatch" href="javascript:openPriceMatchWindow();">Price match guarantee</a>
                <span class="ContactInfo">
                    <%= ContactHeaderText %> <a href="<%= ParentSiteURL %>/contactus" target="_blank" >contact us</a>
                </span>                
            </div>
            <div id="bookingControlContainer">
                <div id="sslSeal">
                    <script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=<%= SSLSeal %>&amp;size=L&amp;lang=en"></script>
                </div>
                <thl:HorizontalControl ID="HorizontalControl1" VehicleTypeStr="av" CountryOfResidence="" CountryCode="NZ" VehicleCode="" Brand="x" SplashMode="false" Target="" runat="server" />
            </div>
            <div id="breadCrumbs">
                <span class="Crumbs">
                    <a href="<%= ParentSiteURL %>" class="HomeLnk">Home</a> &gt; Select your vehicle
                </span>
                <ul id="stepsDisplayer">
                    <li>
                        <span id="PaymentStep">3.Payment</span> 
                    </li>
                    <li>
                        <span id="ConfigureStep">2.Configure</span>
                    </li>
                    <li>
                        <span id="SelectStep">1.Select</span>
                    </li>
                </ul>
            </div>
            <div id="vehicleCatalogContainer">
            <h2>1. Select your vehicle
            	<span class="CurrencyPanel">
                    <small>Alternative Currency:</small>
                    <%= CurrencySelector %>
                </span>
            </h2>
            <div id="resultsHeader" >
                <h3>Search results below</h3>
                <ul id="sortPanel">
                	<li>Sort by:</li>
                    <li>
                    	<select id="sortBy" class="sort">
							<option value="price_a">Price – Low to High</option>
							<option value="price_d">Price – High to Low</option>
							<option value="size_a">Size – Small to Big</option>
							<option value="size_d">Size – Big to Small</option>
                    </select>
                    </li>
                    <li class="second">Show:</li>
                    <li>
                    	<select id="filterBy" class="filter">
							<option value="all">All Vehicles</option>
							<option value="avail">Available Vehicles</option>
							<option value="noavail">Unavailable Vehicles</option>
						</select>
                    </li>
                </ul>
                <!--
                <ul id="sortPanel">
                    <li><a href="#size">Size</a></li>
                    <li><a href="#price" class="asc">Price</a></li>
                    <li>
                        <input type="checkbox" id="availFilter" />
                        <label for="availFilter">Available only</label>
                    </li>
                </ul>
                -->
            </div>
            <%= VehicleCatalog %>
            <div id="emptySelectionMsg">
                <span>
                    <%= GlobalMeassage %>
                </span>
            </div>                
        </div>
            <div id="staticLoader">
                <span>
                Reloading your search results...
                </span>
                <ul id="carousel">
                    <li><img src="<%= AdsPath %>1.png" alt="" /></li>
                    <li><img src="<%= AdsPath %>2.png" alt="" /></li>
                    <li><img src="<%= AdsPath %>3.png" alt="" /></li>
                    <li><img src="<%= AdsPath %>4.png" alt="" /></li>                            
                </ul>  
            </div>
        <div id="disclaimer" title="disclaimer">
            <span>Please note: Rental prices quoted above are only valid at the time of booking.</span>
            <span>Alternative currency is based on approximate current exchange rates. All payments will be processed in <%= defaultCurrency.ToString() %>.</span>
        </div>
        <!-- Debug Code Starts -->
        <%= debugHTML %>
        <!-- Debug Code Ends -->
        </div>
        <%= DNAIMGTag %>    
    </form>
    <%= RemarketingCode %>   
</body>
</html>