﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class ExpiredQuote : System.Web.UI.Page
{

    public string AuroraRentalStr;
    public string FeesView;//MVC Ready
    public string EHIView;//MVC Ready
    public string INSView;//MVC Ready
    public string FerryView, FerryJSON;//MVC Ready
    public string RequestStr;
    public string SearchAgainURLParams, SearchAgainURLParamsNoVehicle;
    public string AnalyticsCode, DNAIMGTag, SiteInfoJSON;
    public string ContentURL, AssetsURL;
    public string HTTPPrefix;
    public string ParentSiteURL, SSLSeal, VehicleName;
    public string ContactHeaderText, CurrencySelector, BrandTitle;
    public string PickUpLocationStr, PickUpDateStr, DropOffLocationStr, DropOffDateStr, VehicleImageConvension, vehicleBrandStr, VehicleChargeContentURL;


    public string VehicleContentLink;

    public string QuoteStatusClass, QuoteStatusMsg;

    public string debugHTML;

    public AuroraQuote quote;

    public QuoteStatus Status;

    ApplicationData appData;

    public decimal AdminFee = 0.0m;

    public string VehicleImagePath, VehicleInclusionPath;
    THLDomain appDomain;

    public string OpenTag { get; set; }
    public string BrandChar { get; set; }

    public string Version
    {
        get { return ApplicationManager.ReleaseVersion; }
        set { value = ApplicationManager.ReleaseVersion; }
    }

    public string ContactUsURL
    {
        get
        {
            THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
            string contactUrl = appDomain.ParentSite + "/contactus";
            switch (appDomain.ParentSite)
            {
                case "keacampers.co.nz":
                    contactUrl = "http://nzrentals.keacampers.com/en/contact/contact_us.aspx";
                    break;
                case "keacampers.com.au":
                    contactUrl = "http://nzrentals.keacampers.com/en/contact/contact_us.aspx";
                    break;
            }
            
            return contactUrl;
        }
    }

    public string Currency
    {
        //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
        //get
        //{
        //    appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
        //    return CurrencyManager.GetCurrencyForCountryCode(appDomain.Country.ToString()).ToString();
        //}
        get;
        set;
    }

    public string CSSLink { get; set; }  

    protected void Page_Load(object sender, EventArgs e)
    {
        
        //-- init from configure
        bool secureMode = false;//TODO: move to config manager,, managed globally
        bool.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["SecureMode"], out secureMode);
        HTTPPrefix = ApplicationManager.HTTPPrefix;
        THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
        if (appDomain == null)//dubug line
            appDomain = new THLDomain("secure.maui.co.nz", THLBrands.Maui, CountryCode.NZ, "B2CNZ", false, "uahere");//dubug line
        BrandChar = appDomain.BrandChar.ToString();
        ParentSiteURL = appDomain.ParentSite;
        TrackingDisplayer trd = new TrackingDisplayer(appDomain, secureMode);//DecideDNA Tracking Img
        DNAIMGTag = trd.RenderDNATrackingImg("configure");
        SSLSeal = trd.GetSealID();
        AnalyticsCode = trd.RenderAnalsCode();//Analytics Tracking Code
        ContentURL = HTTPPrefix + appDomain.Name + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["BookingControlContentPath"];
        string countryCode = appDomain.Country.ToString().ToUpper();  //(Request.Params["cc"] != null ? Request.Params["cc"] : "NZ").ToUpper();   
        AssetsURL = HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];
        //-- init from configure ends   

        



        if (SessionManager.CurrentQuote != null)//must have the quote in session to submit
        {
            SessionData sessionData = SessionManager.GetSessionData();
            AuroraQuote quote = SessionManager.CurrentQuote;
            AvailabilityRequest aReq = quote.Request;
            
            VehicleContentLink = ContentManager.GetVehicleLink(THLBrand.GetBrandForString(aReq.Brand), aReq.CountryCode, aReq.VehicleModel, THLBrands.Generic);

            VehicleImagePath = AvailabilityHelper.GetVehicleImageBasePath(aReq.CountryCode, THLBrand.GetBrandForString(aReq.Brand), aReq.VehicleType, aReq.VehicleModel, AssetsURL, false) + "-Clearcut-144.jpg";
            VehicleInclusionPath = AvailabilityHelper.GetVehicleImageBasePath(aReq.CountryCode, THLBrand.GetBrandForString(aReq.Brand), aReq.VehicleType, aReq.VehicleModel, AssetsURL, true) + "-InclusionIcons.gif";

            QuoteStatusClass = quote.Status.ToString();
            QuoteStatusMsg = getMessageTextForQuoteStatus(quote.Status);

            try
            {
                //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
                Currency = CurrencyManager.GetCurrencyForCountryCode(aReq.CountryCode).ToString();
            }
            catch (Exception)
            {
            }

            //AuroraRentalStr = new UserDataDisplayer().RenderCustomerDetails(quote.GetCustomerData());
            THLBrands requestBrand = THLBrand.GetBrandForString(quote.Request.Brand);
            ContactHeaderText = ContentManager.GetMessageForCode(THLBrand.GetBrandForString(BrandChar), appDomain.Country, "ContactText").Content + "<br />"
                            + ContentManager.GetMessageForCode(THLBrand.GetBrandForString(BrandChar), appDomain.Country, "DomesticPhone").Content + " - "
                            + ContentManager.GetMessageForCode(THLBrand.GetBrandForString(BrandChar), appDomain.Country, "InternationalPhone").Content;
            VehicleImageConvension = quote.Request.CountryCode.ToString().ToLower() + AvailabilityHelper.GetAuroraVehicleStrForVehicleType(quote.Request.VehicleType) + appDomain.BrandChar.ToString() + "." + quote.Request.VehicleModel;
            appData = ApplicationManager.GetApplicationData();
            VehicleName = quote.VehicleName;
            PickUpLocationStr = appData.GetLocationStringForCtrlCode(quote.Request.PickUpBranch);  //appData.GetAddressForLocationCode(aRequest.PickUpBranch);
            DropOffLocationStr = appData.GetLocationStringForCtrlCode(quote.Request.DropOffBranch); //appData.GetAddressForLocationCode(aRequest.DropOffBranch);
            PickUpDateStr = quote.Request.PickUpDate.ToString("dddd, dd MMMM yyyy HH:mm tt");
            DropOffDateStr = quote.Request.DropOffDate.ToString("dddd, dd MMMM yyyy HH:mm tt");
            VehicleType contentResourceVehicleType = quote.Request.VehicleType;
            VehicleChargeContentURL = ContentManager.GetLinkElementForBrand(requestBrand, contentResourceVehicleType, quote.Request.CountryCode, "VehicleCharges", quote.Request.PickUpDate, requestBrand).Content;
            SiteInfoJSON = "{'brand':'" + BrandChar + "'}";
            SearchAgainURLParams = quote.Request.GenerateSelectionParamsURL(true);
            SearchAgainURLParamsNoVehicle = quote.Request.GenerateSelectionParamsURL(false);
        }

        OpenTag = AvailabilityHelper.LoadOpenTag(appDomain);
    }

    /// <summary>
    /// Returns the releveant context (message) of the Quotes Status
    /// TODO: remove to the content manager if needed at brand level
    /// </summary>
    /// <param name="status"></param>
    /// <returns></returns>
    string getMessageTextForQuoteStatus(QuoteStatus status)
    {
        string message = "";
        switch (status)
        { 
            case QuoteStatus.Expired:
                message = "Your save quote has passed its 7 day validity so is no longer valid, however this vehicle is still available.";
                break;
            case QuoteStatus.NoAvailability:
                message = "Sorry the vehicle we quoted on is no longer available.<br/>Saving a quote does not reserve a vehicle. Saved quotes are valid for 7 days as long as the vehicle is available."; 
                break;
            case QuoteStatus.Retrieved:
                message = "This quote is still valid";
                break;
        }   
        return message;
    }

}
