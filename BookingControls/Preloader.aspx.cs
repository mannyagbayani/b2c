﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Text;
using THL.Booking;

public partial class Preloader : System.Web.UI.Page
{

    delegate string ProcessTask(string id);

    public string Status = "init";

    public string PassedParams;

    public string ContentURL;
    public string ContactHeaderText;
    public string HTTPPrefix, AssetsURL, brandStr;
    public string ParentSiteURL;
    public string AdsPath;
    public string PingServiceParam = string.Empty;
    public string RequestParams = string.Empty;

    /// <summary>
    /// Support release version from config for forcing static assets cache refresh.
    /// </summary>
    public string Version
    {
        get { return ApplicationManager.ReleaseVersion; }
        set { value = ApplicationManager.ReleaseVersion; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        PassedParams = (Request.RawUrl.Contains("?") ? Request.RawUrl.Split('?')[1] : string.Empty);
        ContentURL = /*HTTPPrefix + appDomain.Name +*/ "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["BookingControlContentPath"];
        HTTPPrefix = ApplicationManager.HTTPPrefix;
        AssetsURL = HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];

        AvailabilityRequest request = SessionManager.GetSessionData().HasRequestData ? SessionManager.GetSessionData().AvailabilityRequestData : null;

        if (request == null)//No Data to trigger a preloader
            Response.Redirect("Selection.aspx?msg=sessionExpired");

        
        
        
        if (Request.Params["rType"] != null)
        {
            string RequestType = Request.Params["rType"].ToString();
            PingServiceParam = String.Format("taskType={0}&timeoutInSeconds={1}{2}", RequestType, 90, String.IsNullOrEmpty(PassedParams) ? "" : "&param=" + PipelineOrigionalRequest(PassedParams));
        }

        if (!String.IsNullOrEmpty(SessionManager.AgentCode))
            request.AgentCode = SessionManager.AgentCode;

        RequestParams = request.GenerateSelectionParamsURL(false);

        ///CentralLibraryImages/Britz/New-Zealand/Cars/AlternativePreloader/Ad1.png
        string preLoaderTypeStr = "AlternativePreloader";
        AdsPath = AvailabilityHelper.GetCentralLibPath(request, ApplicationManager.GetDomainForURL(Request.Url.Host), AssetsURL) + "/" + preLoaderTypeStr + "/Ad";

        THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);

        ContactHeaderText = ContentManager.GetMessageForCode(appDomain.Brand, appDomain.Country, "ContactText").Content + "<br />"
                        + ContentManager.GetMessageForCode(appDomain.Brand, appDomain.Country, "DomesticPhone").Content + " - "
                        + ContentManager.GetMessageForCode(appDomain.Brand, appDomain.Country, "InternationalPhone").Content;
        brandStr = THLBrand.GetBrandChar(appDomain.Brand).ToString();
        ParentSiteURL = appDomain.ParentSite;

        if (Session["PreLoadStatus"] == null)//TODO: refactor into session manager
        {
            if (Request.Params["tid"] != null && Request.Params["state"] != "ping")//initial load with rental param
            {
                Status = "loading..";
                ProcessTask processTask = new ProcessTask(LoadRental);
                processTask.BeginInvoke(Request.Params["rentalId"], new AsyncCallback(RentalRetrivalEnded), processTask);
            }
            else if (Request.Params["tid"] == null)//blank initial request 
                Status = "No QuoteID Provided";
        }
        else
        {
            Status = "loaded...";
        }
    }

    string PipelineOrigionalRequest(string oRequest)
    {
        return oRequest.Replace('&', '~').Replace('=', '|');
    }
    /// <summary>
    /// Async Process for DAL retreival of quoted rental info of Auorora into Session
    /// </summary>
    /// <param name="rentalId">Rental ID from request</param>
    /// <returns></returns>
    string LoadRental(string rentalId)
    {
        //load rental into session    
        DataAccess dal = new DataAccess();
        Status = "loading";
        Thread.Sleep(10000);//debug line
        Session["PreLoadStatus"] = dal.GetQuoteForRentalId(rentalId);//TODO:  refactor into session manager
        if (Session["Quote"] == null)//TODO: handle the no matching scenario
            Status = "NoMatch";
        return Status;
    }

    public void RentalRetrivalEnded(IAsyncResult result)
    {
        ProcessTask processTask = (ProcessTask)result.AsyncState;
        string id = processTask.EndInvoke(result);
        Status = "loaded...";
    }
}