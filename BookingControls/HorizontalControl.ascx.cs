﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace THL.Booking {

    public partial class HorizontalControl : System.Web.UI.UserControl
    {
        public AvailabilityRequest RequestParams;        
        
        public string brandStr;
        public string CountryCode;
        public string VehicleTypeStr;
        public VehicleType requiredVType = VehicleType.Campervan;
        public string VehicleCode;
        public string BookingCtrlJSON;
        
        public bool SplashMode;
        public bool AggregatorMode;

        public string RadioClass;

        public string Target;
        public string Brand;
        public string CountryOfResidence;
        public string PickUpDD, DropOfDD, VehicleDD, CompanyDD, PickUpTimeDD, DropOffTimeDD, numAdultDD, numChildrenDD, TravelCountryDD, DODate, PUDate, PULoc, DOLoc, VCode, CVAvail;
        public string BaseURL;
        public string ContentURL , AssetsURL;
        public string MaxTravellers;
        public string PanelTitle;
        public string IsDisabled = string.Empty;

        public string PassedSID;//added to support session passed from CrossSell

        private CacheManager cm = null;
        
        protected void Page_Load(object sender, EventArgs e)
        {

            THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);

            NameValueCollection incomingParams;
            if (Request.Params["vc"] == null)//Aurora incoming format
                incomingParams = Request.Params;
            else
                incomingParams = AvailabilityHelper.CtrlToAuroraCollectionMapper(Request.Params);
            
            brandStr = Brand;// (Request.Params["brand"] != null ? Request.Params["brand"] : Brand);//todo verify incoming brand
            ContentURL = ApplicationManager.HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["BookingControlContentURL"];
            AssetsURL = ApplicationManager.HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];

            PULoc = (incomingParams["pickUpLoc"] != null ? incomingParams["pickUpLoc"] : PULoc);
            DOLoc = (incomingParams["dropOffLoc"] != null ? incomingParams["dropOffLoc"] : DOLoc);
            VCode = (incomingParams["VehicleCode"] != null ? incomingParams["VehicleCode"] : VCode);

            if(!string.IsNullOrEmpty(Request.Params["SID"]))
                PassedSID = Request.Params["SID"];//Added for cross site clicktale support

            //init the App Cache if required
            bool disableCache = false;
            bool.TryParse(incomingParams["disableCache"], out disableCache);
            
            disableCache = true;
            bool useLocalResource = true;
            string CacheDataPath = String.Empty;

            if (Application["BookingCacheManager"] == null || (disableCache))
            {
                CacheDataPath = useLocalResource ? System.Web.Configuration.WebConfigurationManager.AppSettings["LocalDataPath"] : System.Web.Configuration.WebConfigurationManager.AppSettings["RemoteBookingControlContentURL"];
                cm = new CacheManager(CacheDataPath, useLocalResource);
                Application.Lock();
                Application["BookingCacheManager"] = cm;
                Application.UnLock();
            }
            else
            {
                cm = (CacheManager)(Application["BookingCacheManager"]);
            }
             
            RequestParams = new AvailabilityRequest();
            
            //RequestParams.VehicleType = VehicleType.Campervan;//default to campers

            RequestParams.LoadFromQueryStringParams(Request.Params);

            CountryOfResidence = (incomingParams["countryOfResidence"] != null ? incomingParams["countryOfResidence"].ToUpper() : CountryOfResidence);
            RequestParams.CountryOfResidence = CountryOfResidence;

            string defaultPickUp = DateTime.Now.AddDays(1).ToString("dd-MMM-yyyy"), defaultDropOff = DateTime.Now.AddDays(2).ToString("dd-MMM-yyyy");

            if(PUDate == null)
                PUDate = (incomingParams["ctrlSearchBox_calPickUp_DateText"] != null ? incomingParams["ctrlSearchBox_calPickUp_DateText"] : defaultPickUp);
            if(DODate == null)
                DODate = (incomingParams["ctrlSearchBox_calDropOff_DateText"] != null ? incomingParams["ctrlSearchBox_calDropOff_DateText"] : defaultDropOff);
                   

            if (SessionManager.AgentCode != null && SessionManager.AgentCode != string.Empty)
            {
                Affiliate affiliate = ApplicationManager.GetAffeliateForCode(SessionManager.AgentCode);//load ctrl according to affil
                if (affiliate != null)
                {
                    VehicleType affVehicleType = affiliate.GetVehicleTypeForCountryBrand(appDomain.Country, appDomain.Brand);
                    string AffVehicleTypeStr = AvailabilityHelper.GetAuroraVehicleStrForVehicleType(affVehicleType);
                    if (AffVehicleTypeStr != "both")
                    {
                        RadioClass = "SingleType";
                        VehicleTypeStr = AffVehicleTypeStr;
                    }
                    else if (VehicleTypeStr != "ac")
                        VehicleTypeStr = "av";
                }
            }
            
            if (brandStr == null || brandStr == string.Empty )//call within booking pages (no defined Brand at ctrl external attributes)
                brandStr = appDomain.BrandChar.ToString();

            AggregatorMode = (brandStr == "z" || brandStr == "c");//Set aggregator mode
            if (AggregatorMode)
            {
                Company[] cmps = cm.GetAggregator(brandStr).GetCompaniesForCountry(CountryCode, requiredVType);
                CompanyDD = new CompanyDisplayer(cmps).RenderCompaniesDropDown(null, (VehicleTypeStr.ToLower() == "ac" ? VehicleType.Car : VehicleType.Campervan));
            }
            else
            {
                CompanyDD = "<input type='hidden' id='ctrlSearchBox_dropVendor' value='" + brandStr  + "' />";//revisit
            }
            
            THLBrands brandType = THLBrand.GetBrandForString(brandStr);

            if (SplashMode && incomingParams["countryCode"] != null)
                CountryCode = incomingParams["countryCode"];

            //if (Brand == "x" || brandStr == "x") CountryCode = "NZ";//Exploremore country force, TODO: revisit

            if (CountryCode != null && CountryCode != string.Empty )
            {

                if (brandType == THLBrands.KiwiCampers || 
                    brandType == THLBrands.EasyGo ||
                    /*brandType == THLBrands.Kea ||*/
                    brandType == THLBrands.Apollo) 
                    brandType = THLBrands.MAC;
                
                BranchCountryData bcd = cm.GetCountryData(CountryCode, brandType);
                CVAvail = bcd.VehicleAvailability.ToString(); 
                string defaultTime = (requiredVType == VehicleType.Campervan ? bcd.CampersDefaultHour : bcd.CarsDefaultHour); 
                MaxTravellers = (requiredVType == VehicleType.Campervan ? bcd.MaxPassengersCamper : bcd.MaxPassengersCar).ToString();//TODO: verify
                BranchDisplayer bd = new BranchDisplayer(cm.GetBranchesForBrand(brandType));
                PickUpDD = bd.GetBranchDDForBrand(CountryCode, VehicleTypeStr, "ctrlSearchBox_dropPickUpLocation", BranchType.PickUp, string.Empty);
                DropOfDD = bd.GetBranchDDForBrand(CountryCode, VehicleTypeStr, "ctrlSearchBox_dropDropOffLocation", BranchType.DropOff, string.Empty);
                PickUpTimeDD = bd.GetOpeningHoursDD(bcd, requiredVType, defaultTime, "ctrlSearchBox_dropPickUpTime");
                DropOffTimeDD = bd.GetOpeningHoursDD(bcd, requiredVType, "15:00", "ctrlSearchBox_dropDropOffTime");

                VehicleType ddVType = (Request.Params["vt"] == "ac" ? VehicleType.Car : VehicleType.Campervan); 
                
                if (!(brandType == THLBrands.AIRNZ || brandType == THLBrands.MAC))//show vehicles only for THL specific brand 
                    VehicleDD = bd.GetVehiclesDDForBranch(ddVType, cm.GetBrand(brandType), VehicleCode, CountryCode);
                else
                    VehicleDD = (new VehicleDisplayer()).Render(null,null);
                numAdultDD = bd.GetPassengerDD(PassengerType.Adult, (requiredVType == VehicleType.Campervan ? bcd.MaxAdultsCamper : bcd.MaxAdultsCar), "ctrlSearchBox_dropAdults");
                numChildrenDD = bd.GetPassengerDD(PassengerType.Child, (requiredVType == VehicleType.Campervan ? bcd.MaxChildrenCamper : bcd.MaxChildrenCar), "ctrlSearchBox_dropChildren");
                TravelCountryDD = UserDataDisplayer.GetCountryOfTravelDD(CountryCode, brandType);
            }
            else {//no associeted data 
                PickUpDD = "<select class='SearchBox_FullListBox LocationDD' id='ctrlSearchBox_dropPickUpLocation' name='ctrlSearchBox_dropPickUpLocation'><option>Pick up location</option></select>";
                DropOfDD = "<select class='SearchBox_FullListBox LocationDD' id='ctrlSearchBox_dropPickUpLocation' name='ctrlSearchBox_dropPickUpLocation'><option>Drop off location</option></select>";
                PickUpTimeDD = "<select name='ctrlSearchBox_dropPickUpTime' id='ctrlSearchBox_dropPickUpTime' class='SearchBox_TimeListBox'><option value='10:00'>10:00am</option></select>";
                DropOffTimeDD = "<select name='ctrlSearchBox_dropPickUpTime' id='ctrlSearchBox_dropPickUpTime' class='SearchBox_TimeListBox'><option value='10:00'>10:00am</option></select>";
                VehicleDD = "<select class='SearchBox_FullListBox' id='ctrlSearchBox_dropVehicleType' name='ctrlSearchBox_dropVehicleType'><option value='-1'>Search all</option></select>";
                numAdultDD = "<select class='SearchBox_AdultsListBox' id='ctrlSearchBox_dropAdults' name='ctrlSearchBox_dropAdults'><option value='0' selected='selected'>Adults</option></select>";
                numChildrenDD = "<select class='SearchBox_AdultsListBox' id='ctrlSearchBox_dropChildren' name='ctrlSearchBox_dropChildren'><option value='0' selected='selected'>Children</option></select>";
                TravelCountryDD = UserDataDisplayer.GetCountryOfTravelDD("NONE", brandType);//todo: pass none
            }
        }


        public String GetBrandLabel()
        {
            return THLBrand.GetNameForBrand(THLBrand.GetBrandForString(Brand));        
        }
    }
}