﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Diagnostics" %>

<script runat="server">       
    
    void Application_Start(object sender, EventArgs e) 
    {
        //Debugger.Break();
        Application["ApplicationData"] = new THL.Booking.ApplicationData();
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    {
        //get reference to the source of the exception chain
        Exception ex = Server.GetLastError().GetBaseException();

        //log the details of the exception and page state to the
        EventLog.WriteEntry("PhoenixWeb",
          "MESSAGE: " + ex.Message +
          "\nSOURCE: " + ex.Source +
          "\nFORM: " + Request.Form.ToString() +
          "\nQUERYSTRING: " + Request.QueryString.ToString() +
          "\nTARGETSITE: " + ex.TargetSite +
          "\nSTACKTRACE: " + ex.StackTrace,
          EventLogEntryType.Error);

        //Insert optional email notification here...
        Response.Redirect("Error.aspx");

    }

    void Session_Start(object sender, EventArgs e) 
    {
        //Application_Start(null, null);
        Session["BookingStatus"] = THL.Booking.BookingStatus.Created;
    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
