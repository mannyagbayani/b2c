﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class PaymentV2Old : System.Web.UI.Page
{
    public string ContentURL, AssetsURL;
    public string brandStr, CurrencyStr, VehicleImagePath;
    public string CurrencySelector;
    public string PickUpLocationStr, DropOffLocationStr, PickUpDateStr, DropOffDateStr;
    public string VehicleCode;
    public string RentalSummary;
    public string VehicleChargeJSON;
    public string PaymentTotalPanel;
    public string CCDropDown;
    public string CurrentSurcharge;
    public string SurchargeClass;
    
    public string TandCLink;
    public decimal AdminFee;

    public string AVPID;
    public string DNAIMGTag, AnalyticsCode, ParentSiteURL;

    public Vehicle SelectedVehicle;

    public string VehicleImageConvension;

    public string BrandTitle;

    public string HTTPPrefix;

    public string ContactHeaderText;

    public string MaCDeposit, Lead45, OnRequest; //MaC UIs

    public string SSLSeal;

    ApplicationData appData;

    public string debugStr, debugURL, debugHTML;

    public string SiteInfo;


    public bool AddRWCMsg;
    public int EarlyEventPayment;

    AvailabilityRequest aReq;
    AvailabilityItem ai;

    THLDomain appDomain;

    public string ThemeBaseStr { get; set; }
    public string RemarketingCode { get; set; }
    public string CSSLink { get; set; }
    public string BrandChar { get; set; }
    public string OpenTag { get; set; }

    /// <summary>
    /// Support release version from config for forcing static assets cache refresh.
    /// </summary>
    public string Version
    {
        get { return ApplicationManager.ReleaseVersion; }
        set { value = ApplicationManager.ReleaseVersion; }
    }

    public string Currency
    {
        //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
        //get
        //{
        //    appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
        //    return CurrencyManager.GetCurrencyForCountryCode(appDomain.Country.ToString()).ToString();
        //}
        get;
        set;
    }

    public string ContactUsURL
    {
        get
        {
            THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
            string contactUrl = appDomain.ParentSite + "/contactus";
            switch (appDomain.ParentSite)
            {
                case "keacampers.co.nz":
                    contactUrl = "http://nzrentals.keacampers.com/en/contact/contact_us.aspx";
                    break;
                case "keacampers.com.au":
                    contactUrl = "http://nzrentals.keacampers.com/en/contact/contact_us.aspx";
                    break;
            }
            return contactUrl;
        }
    } 

    //Added to support united/Key advance payment
    public string FinalPayText {
        get {
            int unitedLead = 28, keaLead = 35;
            string msg = "Final balance ";
            try
            {
                //DateTime pickUpDay = aReq.PickUpDate;
                //int leadTime = (DateTime.Today - pickUpDay).Days;                
                switch (appDomain.Brand)
                {
                    case THLBrands.United:
                    case THLBrands.Alpha:
                    case THLBrands.Econo:
                        if (/*leadTime > unitedLead*/ai.DepositAmount == ai.EstimatedTotal)
                            msg += string.Empty;
                        else
                            msg += "(Full payment due " + unitedLead + " days before pick-up)";
                        break;
                    case THLBrands.Kea:
                        if (/*leadTime > keaLead*/ai.DepositAmount == ai.EstimatedTotal)
                            msg += string.Empty;
                        else
                            msg += "(Full payment due " + keaLead + " days before pick-up)";
                        break;
                    default:
                        msg += string.Empty;
                        break;
                }
            }
            catch (Exception ex)//default to pickup, or define
            {
                THLDebug.LogError(ErrorTypes.Presentation, "Payment.FinalPayText", "failed to show pickup payment", ex.Message);
            }
            return msg;
        }        
    }

    public string LoyaltyCardHTML = string.Empty;//Added for loyalty card support 21/06/10

    protected void Page_Load(object sender, EventArgs e)
    {
        //try
        //{                           

            //Response.Cache.SetCacheability(HttpCacheability.NoCache);

            bool secureMode = false;//TODO: move to config manager
            bool.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["SecureMode"], out secureMode);

            HTTPPrefix = ApplicationManager.HTTPPrefix;   

            appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
            if (appDomain == null)//dubug line
                appDomain = new THLDomain("secure.maui.co.nz", THLBrands.Maui, CountryCode.NZ, "B2CNZ", false, "uahere");//dubug line
            ParentSiteURL = appDomain.ParentSite;

            //DecideDNA Tracking Img
            TrackingDisplayer trd = new TrackingDisplayer(appDomain, secureMode);
            DNAIMGTag = trd.RenderDNATrackingImg("payment");

            //marin tracking
            DNAIMGTag += trd.RenderMarinTrackingPixelIMG(SessionManager.MarinTrackingParams, MarinStep.PAYMENT, 0.0m, string.Empty, string.Empty);

            SSLSeal = trd.GetSealID();
        
            //Analytics Tracking Code
            AnalyticsCode = trd.RenderAnalsCode();
            RemarketingCode = trd.RenderRemarketingSnippets();
        
            brandStr = appDomain.BrandChar.ToString(); //(Request.Params["brand"] != null ? Request.Params["brand"] : "m");//TODO: this should be swapped by baseURL. 

            ThemeBaseStr = AgentTheme.CSSPath(SessionManager.AgentCode, brandStr);//support for themed agents
        
            ContentURL = HTTPPrefix + appDomain.Name + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["BookingControlContentPath"];
       
            string countryCode = appDomain.Country.ToString().ToUpper();  //(Request.Params["cc"] != null ? Request.Params["cc"] : "NZ").ToUpper();   
                       
            AssetsURL = HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];
            SessionData sessionData = SessionManager.GetSessionData();
            appData = ApplicationManager.GetApplicationData();

            BrandChar = appDomain.BrandChar.ToString();

            //Added by nimesh below on 25/07/2014 by nimesh as it was not picking up agent code
            string agentCode = "mac";

            if (!string.IsNullOrEmpty(SessionManager.AgentCode))
            {
                string acCode = SessionManager.AgentCode.ToLower();
                BrandChar = /*Request.Params["ac"].ToLower();//*/ AgentTheme.CSSPath(acCode, BrandChar);
                //agentCode = SessionManager.AgentCode.ToLower();

            }
            else
            {
                agentCode = appDomain.AgentCode;
            }
            // end Added by nimesh on 25/07/2014
            CSSLink = string.IsNullOrEmpty(Request.Params["cssLink"]) ? AssetsURL + "/css/" + BrandChar + "/configure.css" : Request.Params["cssLink"];
        
            if(!sessionData.HasBookingData)
                Response.Redirect(redirectURL("sessionExpired"));

            if (sessionData.BookingData.Status == BookingStatus.BookingSuccessful)
                Response.Redirect(redirectURL("bcomp"));

            if (sessionData != null && sessionData.HasRequestData && sessionData.HasResponseData)
            {
                
                aReq = sessionData.AvailabilityRequestData;
                if (appDomain.AgentCode == "AIRNZB2C") appDomain.Country = aReq.CountryCode;
                
                SiteInfo = "{isAggregator:" + appDomain.IsAggregator.ToString().ToLower() + ", cot:'" + aReq.CountryCode + "'}";//js site level info

                //--- Aded for RWC2011 support remove once over --------------------------
                //DateTime rwcStarts = new DateTime(2011, 9, 1);
                //DateTime rwcEnds = new DateTime(2011, 11, 01);
                AddRWCMsg = false;// (aReq.CountryCode == CountryCode.NZ) && (aReq.PickUpDate >= rwcStarts && aReq.PickUpDate <= rwcEnds);
                
                //------------ RWC2011 support ends --------------------------------------

                AvailabilityResponse aRes = sessionData.AvailabilityResponseData;

               

                if (aRes == null)
                    Response.Redirect(redirectURL("sessionExpired"));//TODO: country
                
                CurrencyManager curMan = new CurrencyManager(CurrencyConversionProvider.BNZ, CurrencyManager.GetCurrencyForCountryCode(appDomain.Country));//from Application Cache
                
                CurrencyCode defaultCurrency = (aReq.CountryCode == CountryCode.NZ ? CurrencyCode.NZD : CurrencyCode.AUD);

                if (appDomain.IsAggregator)
                    curMan = new CurrencyManager(CurrencyConversionProvider.BNZ, defaultCurrency);

                AvailabilityResponseDisplayer ard = new AvailabilityResponseDisplayer(aRes, aReq, curMan, AssetsURL, appDomain.Brand);
                CurrencySelector = ard.RenderCurrencyDropDown(defaultCurrency);

                if (sessionData.HasBookingData)
                {
                    AuroraBooking bookingInfo = sessionData.BookingData;
                    
                    ai = aRes.GetAvailabilityItem(bookingInfo.AVPID);


                    if (ai.DepositPercentage == 100.0m)
                        AddRWCMsg = false;

                    BrandTitle = THLBrand.GetNameForBrand(ai.Brand);

                    VehicleImagePath = AvailabilityHelper.GetVehicleImageBasePath(aReq.CountryCode, ai.Brand, aReq.VehicleType, ai.VehicleCode, AssetsURL, false);

                    SelectedVehicle = ai.GetVehicle();
                    TandCLink = "http://" + AvailabilityHelper.NormalizeURL(ai.TandCLink);//TODO: hande HTTP prefixes

                    AdminFee = (bookingInfo.BonusPackSelected ? 0.0m : bookingInfo.AdminFee);//Waive Admin Fee on IncSelected sat 17/10/2009

                    AVPID = ai.AVPID;
                    //brandStr = aReq.Brand;
                    CurrencyStr = (aReq.CountryCode == CountryCode.NZ ? "NZD" : "AUD"); //TODO: get from originating site

                    //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
                    Currency = CurrencyStr;
                    
                    bool hasSurcharge = sessionData.HasSurecharge();

                    decimal currentSurcharge = (sessionData.AvailableCards != null && sessionData.AvailableCards.Length > 0 ? (sessionData.AvailableCards[0].Surcharge/100) : 0.0m);
                    CurrentSurcharge = currentSurcharge.ToString();                        

                    //Populate Travel Details
                    PickUpLocationStr = appData.GetLocationStringForCtrlCode(aReq.PickUpBranch);
                    if (String.IsNullOrEmpty(PickUpLocationStr) || PickUpLocationStr.Equals("n/a"))
                        PickUpLocationStr = aReq.PickUpBranch;//reverese to Code added for AltAvail 13/05/11
                    //TODO: refactor the branch mapper as BPAE is now gone 
                    
                    DropOffLocationStr = appData.GetLocationStringForCtrlCode(aReq.DropOffBranch);
                    if (String.IsNullOrEmpty(DropOffLocationStr) || DropOffLocationStr.Equals("n/a"))
                        DropOffLocationStr = aReq.DropOffBranch;//reverese to Code added for AltAvail 13/05/11
                    //TODO: refactor the branch mapper as BPAE is now gone 
                                        
                    PickUpDateStr = bookingInfo.CheckOutTime.ToString("dddd, dd MMMM yyyy HH:mm tt");
                    DropOffDateStr = bookingInfo.CheckInTime.ToString("dddd, dd MMMM yyyy HH:mm tt");
                    VehicleCode = bookingInfo.VehicleCode;

                    Lead45 = (bookingInfo.CheckOutTime.AddDays(-45) > DateTime.Today ? "Lead45" : string.Empty);//for MaC TaC
                    MaCDeposit = ai.DepositAmount.ToString("F");

                    OnRequest = (ai.AvailabilityType == AvailabilityItemType.OnRequest ? "onRequest" : string.Empty);

                    if(sessionData.AvailableCards != null)
                        CCDropDown = new UserDataDisplayer().RenderCCDropDown(sessionData.AvailableCards, brandStr);
                    //else TODO: no cards found

                    ContactHeaderText = ContentManager.GetMessageForCode(THLBrand.GetBrandForString(brandStr), appDomain.Country, "ContactText").Content + "<br />"
                            + ContentManager.GetMessageForCode(THLBrand.GetBrandForString(brandStr), appDomain.Country, "DomesticPhone").Content + " - "
                            + ContentManager.GetMessageForCode(THLBrand.GetBrandForString(brandStr), appDomain.Country, "InternationalPhone").Content;
                    
                    //Populate Rental UI
                    RentalSummary = ard.RenderRentalSummary(bookingInfo, bookingInfo.AVPID, currentSurcharge, hasSurcharge);
                    PaymentTotalPanel = ard.RenderPaymentTotalsPanel(bookingInfo, currentSurcharge);
                    VehicleChargeJSON = ard.RenderAvailabilityRowJSON(bookingInfo.AVPID, bookingInfo);
                    
                    //-- Added Loyalty card Support
                    LoyaltyCardHTML = ard.RenderLoyaltyField(bookingInfo.RequiresLoyaltyNumber);
                    
                    char brandChar = THLBrand.GetBrandChar(ai.Brand);
                    VehicleImageConvension = aReq.CountryCode.ToString().ToLower() + AvailabilityHelper.GetAuroraVehicleStrForVehicleType(aReq.VehicleType) + brandChar + "." + ai.VehicleCode;
                }
                else//could not populate payment page (no session info) redirect to selection..
                {
                    Response.Redirect(redirectURL("sessionExpired"));//TODO: get err url
                }                
            }
            //-------------------------------- Debug Zone -------------------------
            debugURL = "unittests/debugStr.aspx?ts=" + DateTime.Now.Millisecond;
            debugHTML = (Request.Params["debugOn"] != null && Request.Params["debugOn"] == "true" ? "<div id='debugSection' style='float: left; margin-top: 20px; width:757px; height: 800px; background-color: #fff;'><div id='debugValue'>" + debugStr + "</div><iframe src='" + debugURL + "' width='757px' height='800px' /></div>" : string.Empty);
        
        //}
        //catch (Exception ex)
        //{
        //    string Error = ex.Message;
        //    THLDebug.LogError(ErrorTypes.Application, "Page_Load.Payment", Error, string.Empty);
        //    Response.Redirect("Selection.aspx?msg=sessionExpired");
        //}
            OpenTag = AvailabilityHelper.LoadOpenTag(appDomain);
    }

    string redirectURL(string key) {
        string url = "/Selection.aspx?msg="+ key;
        switch (appDomain.Brand)
        { 
            case THLBrands.Alpha:
                url = "/nz/select/?msg=" + key;
                break;
            case THLBrands.MAC:
                url = "/nz/select/?msg=" + key;
                break;
            //Migration point TODO: update and refactor.
        }
        return url;
    }
}
