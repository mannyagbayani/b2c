﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Bookings : System.Web.UI.Page 
{
    public string ContentURL;
    public string Brand;
    
    protected void Page_Load(object sender, EventArgs e)
    {

        ContentURL = System.Web.Configuration.WebConfigurationManager.AppSettings["BookingControlContentURL"];
            
        Brand = Request.Params["brand"];
    }
}
