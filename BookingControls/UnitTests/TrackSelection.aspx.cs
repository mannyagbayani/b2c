﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UnitTests_TrackSelection : System.Web.UI.Page
{

    public string TrackerStr;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        int randomAvailabilityInt = new Random().Next(10);
        int randomTotalOptionsInt = randomAvailabilityInt + new Random().Next(10);
        int randomLeadTime = new Random().Next(10, 120);
        int randomFinWeek = new Random().Next(1, 52);

        if (randomAvailabilityInt < 5) {//simulate no availabilty  
            TrackerStr = "pageTracker._trackEvent('Availability','none');\n";
            TrackerStr += "pageTracker._trackEvent('Lead Time','" + randomLeadTime + "'," + randomLeadTime + ");\n";
            TrackerStr += "pageTracker._trackEvent('Fin Week','" + randomFinWeek + "'," + randomFinWeek + ");\n"; 
        }
        else {
            float ratio =  randomTotalOptionsInt > 0 ? (float)randomAvailabilityInt / (float)randomTotalOptionsInt : 0;
            
            TrackerStr = "pageTracker._trackEvent('Availability','" + randomAvailabilityInt + "/" + randomTotalOptionsInt + "'," + ratio + " );\n";
            TrackerStr += "pageTracker._trackEvent('Lead Time','" + randomLeadTime + "'," + randomLeadTime + ");\n";
            TrackerStr += "pageTracker._trackEvent('Fin Week','" + randomFinWeek + "'," + randomFinWeek + ");\n"; 
        }      
        
    }
}
