﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Preloader.aspx.cs" Inherits="Preloader_Test" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" >
    <title>Retrieving Your Quote</title>    
    <link href="<%= AssetsURL %>/css/Base.css?v=1.5" type="text/css" rel="stylesheet" />     
    <link href="<%= AssetsURL %>/css/<%= brandStr %>/selection.css?v=1.6" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="../js/jquery-1.3.1.min.js"></script>    
    <script src="http://jquery-ui.googlecode.com/svn/branches/labs/carousel/external/ui.core.js"></script>
    <script src="http://jquery-ui.googlecode.com/svn/branches/labs/carousel/ui.carousel.js"></script>
    <style type="text/css">
        /* TODO: move this into branded styling */
        #carousel { position: relative; height: 160px; margin: 0; padding: 0; }
        #carousel li { float: left; cursor: pointer; cursor: hand; list-style: none; margin: 0; padding: 0; width: 200px;; }
        #carousel li img { height: 100%; width: 100%; }
        .quote  #configurePanel .progressIndicator {
             float: left;
             clear:both;             
             height : 100px;             
             margin-left: 20px;
             font-size: 13px;  
             text-align: center;    
             width: 95%;
             height: 480px; 
             padding-top: 50px;                      
        }
        
        .quote  #configurePanel .progressIndicator img {
            margin-top:20px;            
        }
        
        
    </style>
</head>
<body>    
    <form id="form1" runat="server">
        <div style="display:none;">
            <%= Status %>
        </div>
        <div id="vehicleConfigureContainer" class="quote">       
            <div id="catalogHeader">
                <span class="Image">
                    <img src="<%= AssetsURL %>/images/<%= brandStr %>/logo.jpg" alt="logo" />
                </span>
                <a class="priceMatch" href="javascript:openPriceMatchWindow();">Price match guarantee</a>
                <span class="ContactInfo">
                    <%= ContactHeaderText %> or <a href="<%= ParentSiteURL %>/contactus" target="_blank">contact us</a>
                </span>
            </div>
            <div id="breadCrumbs">
                <span class="Crumbs">
                </span>
            </div>
            <div id="vehicleCatalogContainer">           
                <h2>Alternative Availabilities</h2>
                <div id="configurePanel">
                    <div class="progressIndicator">
                        <span>Loading Alternative Availabilities</span>
                        <br />
                        <span>Thank you for your patience</span>
                        <br />
                        <br />
                        <ul id="carousel">
                            <li><img src="<%= AdsPath %>1.png" alt="" /></li>
                            <li><img src="<%= AdsPath %>2.png" alt="" /></li>
                            <li><img src="<%= AdsPath %>3.png" alt="" /></li>
                            <li><img src="<%= AdsPath %>4.png" alt="" /></li>
                            <li><img src="<%= AdsPath %>5.png" alt="" /></li>
                        </ul>   
                    </div>
                </div>
            </div>
            <div id="disclaimer">
                <span>Please note: Rental prices quoted above are only valid at the time of booking.</span>
            </div>
        </div>    
    </form>
    <script type="text/javascript">
        var sessionID = '<%= Session.SessionID %>';
        function getStatus() {
            //var url = pingServiceUrl + '?tid=' + uniqueId + "&" + providedParams + (progressStarted ? "&state=ping" : "");
            var url = pingServiceUrl + '?sid='+sessionID+'&taskType=GetAlternateOptions&timeoutInSeconds=30&param=' + providedParams.replace('=','|');
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'text',
                success: function (data) {
                    if (data.indexOf("initialised") > -1) {
                        progressStarted = true;
                        $('#status').html("Initialised");
                        $('#statusFill').width(data);
                        window.setTimeout("getStatus()", warmUpPeriod);
                    }
                    if (data.indexOf("pending") > -1 && --loadAttempts > 0) {
                        $('#status').html(data);
                        $('#statusFill').width(data);
                        window.setTimeout("getStatus()", pingFreq);
                    }
                    if (data.indexOf("failed") > -1) {
                        //TODO: redirect user to selection page;
                        alert("failed!");
                    }
                    if (data.indexOf("timedout") > -1) {
                        //TODO: action when time out;
                        alert("timeout!");
                    }
                    else if (data.indexOf("success") > -1) {
                        window.location = redirectURL;
                    };
                }
            });

        }

        var loadAttempts = 30; //define number of trys
        var redirectURL = "../unittests/AltAvailability.aspx?<%= PassedParams %>";
        var providedParams = "<%= PassedParams %>";
        var progressStarted = false;
        var warmUpPeriod = 2000;
        var pingFreq = 1000;
        var pingServiceUrl = "../dispatcher/loadprogress.asmx/LoadSlowFunction";

        $(document).ready(function(event) {
           getStatus();
        });    

        window.setTimeout(function() {
            $("#carousel").carousel({
                animateByMouse: false,
                pausable: false
            });
        }, $.browser.safari ? 100 : 0); 
     </script>    
</body>
</html>