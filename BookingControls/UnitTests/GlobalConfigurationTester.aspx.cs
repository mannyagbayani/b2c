﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class UnitTests_GlobalConfigurationTester : System.Web.UI.Page
{

    ApplicationData applicationData;
    public string BranchStr;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.Params["code"] != null)
        {
            applicationData = ApplicationManager.GetApplicationData();
            Branch branch =  applicationData.GetBranchForCode(Request.Params["code"]);
            BranchStr = branch.ToString();
        }
    }
}
