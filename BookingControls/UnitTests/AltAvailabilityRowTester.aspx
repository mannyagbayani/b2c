﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AltAvailabilityRowTester.aspx.cs"
    Inherits="AltAvailabilityRowTester" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Alternative Availability</title>
    <link href="http://images.thl.com/css/Base.css?v=1.5" type="text/css" rel="stylesheet" />
    <link href="http://images.thl.com/css/<%= BrandChar %>/selection.css?v=1.7" type="text/css" rel="stylesheet" />
    <link href="AltAvailCSS.css" type="text/css" rel="stylesheet" /><!-- TODO: remove this into branded and base css -->
    <link rel="stylesheet" type="text/css" media="all" href="http://images.thl.com/css/m/horizontal.css" />
    <script src="../js/jquery-1.3.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery.dimensions.min.js" type="text/javascript"></script>
    <script src="../js/jquery.tooltip.min.js" type="text/javascript"></script>
    <script src="../js/date.js" type="text/javascript"></script>
    <script src="../js/hCtrl.js?v=1.7" type="text/javascript"></script>
    <script src="../js/selection.js?v=1.7" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <link href="http://images.thl.com/css/m/jcal.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript">
        var $j = jQuery.noConflict();
        jQuery(document).ready(function() {
            $j(".ShowPriceList").click(function() {//bind expand
                var parentElm = $j(this);
                while (!$j(parentElm).hasClass("OptionRow"))                   
                    parentElm = $j(parentElm).parent()[0];
                $j(parentElm).toggleClass("Expand");
                if ($j(parentElm).hasClass("Expand"))
                    $j(this).text("Price Details");
                else
                    $j(this).text("Hide Details");
            });
        });
    </script>
    <script type='text/javascript'>
        //async
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'MAUIAU-00']);
        _gaq.push(['_setDomainName', '.maui.com.au']);
        _gaq.push(['_setAllowHash', false]);
        _gaq.push(['_trackPageview']);
        _gaq.push(['_trackEvent', 'Campervan', 'Availability', '08/09/37-2010/ADL/ADL', 8]);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
</head>
<body>
    <form name="form1" method="post" action="selection.aspx" id="form1">
    <div>
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTUwODAxMTY1M2RkLe38Vc6Ra6ZMMMMyNP5rrK6xQow=" />
    </div>
    <img src='http://au.track.decideinteractive.com/n/45517/53551/MAUI109/x/e?domain=au.track.decideinteractive.com'
        width='1' height='1' border='0' />
    <div id="vehicleSelectionContainer">
        <div id="catalogHeader">
            <span class="Image">
                <img src="http://images.thl.com/images/<%= BrandChar %>/logo.jpg" alt="logo" />
            </span><a class="priceMatch" href="javascript:openPriceMatchWindow();">Price match guarantee</a>
            <span class="ContactInfo">Call us now - 24 hours a day, 7 days a week:<br />
                Call Within AU Toll Free: 1 300 363 800 - Worldwide Toll Free: +800 200 80 801 <a
                    href="http://www.maui.com.au/contactus" target="_blank">contact us</a> </span>
        </div>        
        <div id="breadCrumbs">
            <span class="Crumbs"><a href="http://www.maui.com.au" class="HomeLnk">Home</a> &gt;
                Select your vehicle </span>
            <ul id="stepsDisplayer">
                <li><span id="PaymentStep">3.Payment</span> </li>
                <li><span id="ConfigureStep">2.Configure</span> </li>
                <li><span id="SelectStep">1.Select</span> </li>
            </ul>
        </div>
        <div id="vehicleCatalogContainer">
            <h2>
                1. Select your vehicle</h2>
            <div id="resultsHeader">
                <h3>
                    Search results below</h3>
                <span class="CurrencyPanel"><small>Alternative Currency:</small>
                    <select id='currencySelector'>
                        <option value='NZD' rel='1.0'>New Zealand Dollar (NZD)</option>                        
                    </select>
                </span>
            </div>                    
                                    
            <script type="text/javascript">

                jQuery(document).ready(function() {
                    //updateRange();
                });

                var spannerWidth = 400;
                function updateRange() {

                    var hireDays = $j("#hirePeriod").val();
                    var originalHirePeriod = $j("#originalHirePeriod").val();
                    var suggestedPickUpPriorToTravel = $j('#priorDays').val();


                    var totalDays = 20 + parseInt(originalHirePeriod);
                    var PxPerDay = spannerWidth / totalDays;

                    PxPerDay = Math.round(PxPerDay);

                    $j(".preORDates").css('width', PxPerDay * 10 + "px");
                    $j(".hireORDates").css('width', spannerWidth - PxPerDay * 20 + "px");
                    //$j(".postORDates").css('width', PxPerDay * 10 + "px");

                    $j(".preDates").css('width', PxPerDay * (10 - suggestedPickUpPriorToTravel) + "px");
                    $j(".hireDates").css('width', PxPerDay * hireDays + "px");
                    //$j(".postDates").css('width', PxPerDay * 10 + "px");                           
                }                    
            </script>               
            <!-- Original Request Starts -->                     
            <div class="AltRow">
                <div class="AltType">
                    <span>ORIGINAL SEARCH</span>
                </div>
                <div class='AltTime'>
                    <div class="TimeLabels period">                                                           
                        <span class="preORDates" style="width:130px;">&nbsp;</span> 
                        <span class="hireORDates" style="margin-left:50px;" rel="pudate"><!-- margin left to the center: (days*pixelperDay)/2 - 40/2 -->
                            <small>10 days</small>
                        </span>
                    </div>
                    <div class="TimeSpan">
                        <span class="preORDates">&nbsp;</span>
                        <span class="hireORDates" style=" background-color:#A14343;" >&nbsp;</span>
                        <span class="postORDates">&nbsp;</span>
                    </div>
                    <div class="TimeLabels dates">
                        <span class="preORDates" style="width:130px;">&nbsp;</span> 
                        <span class="hireORDates" rel="pudate">
                            <small>10 Jan '11 Christchurch</small>
                        </span>
                        <span class="postORDates" rel="dodate">
                            <small>20 Jan '11 Christchurch</small>
                        </span>
                    </div>
                </div>
                <div class='AltPrice' title='MAUI5'>
                    <span style="padding-left: 25px; width:113px; font-size:11px;margin:15px 0 0 0;display:block;background: url(http://images.thl.com/images/m/icons/altX.png) no-repeat 0 0 transparent !important; display:block;">Unavailable for some of the days requested</span>
                </div>                                               
                <a href='Configure.aspx?avp=9109EBDFF1FA470EA0CC65BF2E01D296' class='AltSelect'>Select</a>
            </div>            
            <!-- Original Request Ends -->
            <!-- Generated Tester View -->
            <%= TestRow %>
            <!-- Generated Tester View -->           
            <div style="float:left; clear:both; margin: 30px 0 30px 0;">
                <label>Original Hire Period:</label>
                <input type="text" size="3" id="originalHirePeriod" onblur="updateRange()" value="10"/>
                <label>Alternative Hire Period:</label>
                <input type="text" size="3" id="hirePeriod" onblur="updateRange()" value="10"/>                     
                <label>Suggested prior to original travel</label>
                <input type="text" size="3" id="priorDays" onblur="updateRange()" value="4" />                        
            </div>             
        </div>
        <div id="disclaimer" title="disclaimer">
            <span>Please note: Rental prices quoted above are only valid at the time of booking.</span>
            <span>Alternative currency is based on approximate current exchange rates. All payments
                will be processed in AUD.</span>
        </div>
        <!-- Debug Code Starts -->
        <!-- Debug Code Ends -->
    </div>
    </form>
</body>
</html>
