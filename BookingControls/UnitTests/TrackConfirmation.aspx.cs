﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UnitTests_TrackConfirmation : System.Web.UI.Page
{

    public string TrackerStr, BookingId, CountryOfTravel, LeadTime, Price;
    
    protected void Page_Load(object sender, EventArgs e)
    {

        string randomBookigId = "B" + new Random().Next(6600000, 6700000) + "/1"; //"B6600530/1"//
        string randomBookingCountry = (new Random().Next(1, 2) == 1 ? "AU" : "NZ");
        int randomLeadTime = new Random().Next(10, 120);
        int randomPrice = new Random().Next(1500,4500); 

        TrackerStr = "";
        BookingId = randomBookigId;
        CountryOfTravel = randomBookingCountry;
        LeadTime = randomLeadTime.ToString();
        Price = randomPrice + ".00";
    }
}
