﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class UnitTests_ShowBookingInfo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionData sessionData = SessionManager.GetSessionData();

        if (sessionData.HasBookingData)
        {
            AuroraBooking bookingInfo = sessionData.BookingData;
            Response.ContentType = "text/xml";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.Write(bookingInfo.ToXML().InnerXml);
        }
        else 
        {
            Response.Write("<?xml version='1.0'?><Message>No Session Data</Message><Data Msg='No Session Information Available'></Data>");
        }

    }
}
