﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ControlsTestPage.aspx.cs" Inherits="ControlsTestPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="float: left; width: 340px; height: 600px;">
        <iframe src="../VerticalDemo.aspx?<%= QueryStr %>" width=340 height="600"></iframe>
    </div>
    <div style="float: left; width: 700px; height: 600px;">
        <iframe src="../HorizontalDemo.aspx?<%= QueryStr %>" width=700 height="300"></iframe>
        <iframe src="AvailabilityParamsTester.aspx?<%= QueryStr %>" width=700 height="300"></iframe>
    </div>
    </form>
</body>
</html>
