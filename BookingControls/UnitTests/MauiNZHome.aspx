﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MauiNZHome.aspx.cs" Inherits="UnitTests_MauiNZHome" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head id="ctl00_Head1">
    <meta name="GENERATOR" content="Microsoft SharePoint" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Expires" content="0" />
    <meta name="language" scheme="ISO639-2" content="eng" />
    <meta name="keywords" content="campers, camper vans, campervans, motor homes, motorhomes, maui motorhomes, new zealand" />
    <meta name="description" content="Maui New Zealand offers motorhome rental, RV, car and campervan hire NZ.  See our full range of motorhomes & campervans and book your campervan holiday with Maui." />
    <meta name="robots" content=",index,follow" /><meta name="msvalidate.01" content="FBA510F5D9F039DFC5E5CB6787E1F5B5" />
    <title>Campervan Hire NZ - RV, Motorhome &amp; Car Rental - Maui New Zealand</title>
    <!--Styles used for positioning, font and spacing definitions-->
    <link href="http://www.maui.co.nz/SitecollectionDocuments/css/main.css" rel="Stylesheet" type="text/css" />
    <link rel="shortcut icon" href="http://www.maui.co.nz/SitecollectionDocuments/images/favicon.ico" /><script src="/_layouts/1033/init.js?rev=qX%2BG3yl4pldKy9KbPLXf9w%3D%3D"></script>
    <script type="text/javascript" language="javascript" src="http://www.maui.co.nz/_layouts/1033/non_ie.js?rev=yfNry4hY0Gwa%2FPDNGrqXVg%3D%3D"></script>
    <!--Sharepoint styles required for authors only-->
    <!--Placeholder for additional overrides-->
    <script>
        function ProcessImn()
        { }
        function ProcessImnMarkers()
        { }
    </script>
    <style type="text/css">
        .ms-bodyareaframe
        {
            padding: 0px;
        }
    </style>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>                        
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://dev.britz.com.au/SiteCollectionDocuments/js/jquery.customSelect.min.js"></script>
    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
    <script src="http://www.maui.co.nz/SitecollectionDocuments/js/easing.js" type="text/javascript"></script>    
       
    <script src="http://www.maui.co.nz/SitecollectionDocuments/js/core-functions.js" type="text/javascript"></script>    
    <script src="http://www.maui.co.nz/SitecollectionDocuments/js/jquery.jtruncate.pack.js" type="text/javascript"></script>
    <script src="http://www.maui.co.nz/SitecollectionDocuments/js/jquery.effects.core.js" type="text/javascript"></script>
    <script src="http://www.maui.co.nz/SitecollectionDocuments/js/jquery.megamenu.js" type="text/javascript"></script>
    <script src="http://www.maui.co.nz/SitecollectionDocuments/js/jquery.simplyscroll-1.0.4.js" type="text/javascript"></script>
    <script src="http://www.maui.co.nz/SitecollectionDocuments/js/ytplayer.js" type="text/javascript"></script>    
    <script src="http://www.maui.co.nz/SitecollectionDocuments/js/main.js" type="text/javascript"></script>
	<!-- Google Analytics Asychronous Code MAUI NZ-->
	<script type="text/javascript">

	    var _gaq = _gaq || [];
	    _gaq.push(['_setAccount', 'UA-000-1']);
	    _gaq.push(['_setDomainName', '.maui.co.nz']);
	    _gaq.push(['_setAllowHash', false]);
	    _gaq.push(['_trackPageview']);

	    (function () {
	        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	    })();

	</script>
	<!--Analytics end-->
    <!-- BoldChat VisitInfo generator -->
     <script type="text/javascript">
         function getRefID() {
             var chars = "0123456789";
             var string_length = 6;
             var rstring = '';
             for (var i = 0; i < string_length; i++) {
                 var rnum = Math.floor(Math.random() * chars.length);
                 rstring += chars.substring(rnum, rnum + 1);
             }
             return rstring;
         }
         var visitInfoVal = "CHAT" + getRefID();
     </script>
    <!-- BoldChat VisitInfo generator end -->


    </head>
<body class="body" scroll="yes" onload="javascript:_spBodyOnLoadWrapper();">

<!-- ClickTale Top part -->
<script type="text/javascript">
    var WRInitTime = (new Date()).getTime();
</script>
<!-- ClickTale end of Top part -->

    <form name="aspnetForm" method="post" action="/" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
<div>
<input type="hidden" name="__SPSCEditMenu" id="__SPSCEditMenu" value="true" />
<input type="hidden" name="MSOWebPartPage_PostbackSource" id="MSOWebPartPage_PostbackSource" value="" />
<input type="hidden" name="MSOTlPn_SelectedWpId" id="MSOTlPn_SelectedWpId" value="" />
<input type="hidden" name="MSOTlPn_View" id="MSOTlPn_View" value="0" />
<input type="hidden" name="MSOTlPn_ShowSettings" id="MSOTlPn_ShowSettings" value="False" />
<input type="hidden" name="MSOGallery_SelectedLibrary" id="MSOGallery_SelectedLibrary" value="" />
<input type="hidden" name="MSOGallery_FilterString" id="MSOGallery_FilterString" value="" />
<input type="hidden" name="MSOTlPn_Button" id="MSOTlPn_Button" value="none" />
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__REQUESTDIGEST" id="__REQUESTDIGEST" value="0xAA097D4D3F8AE71C909823058BD8EB0342BB8806B0D44CE239594B9CD635D52A9D624E8AC32350A0FBE3E1CC916772B9BAFFD4B82C6FF079AA3591BF1EC4C402,20 Aug 2013 20:22:30 -0000" />
<input type="hidden" name="MSOAuthoringConsole_FormContext" id="MSOAuthoringConsole_FormContext" value="" />
<input type="hidden" name="MSOAC_EditDuringWorkflow" id="MSOAC_EditDuringWorkflow" value="" />
<input type="hidden" name="MSOSPWebPartManager_DisplayModeName" id="MSOSPWebPartManager_DisplayModeName" value="Browse" />
<input type="hidden" name="MSOWebPartPage_Shared" id="MSOWebPartPage_Shared" value="" />
<input type="hidden" name="MSOLayout_LayoutChanges" id="MSOLayout_LayoutChanges" value="" />
<input type="hidden" name="MSOLayout_InDesignMode" id="MSOLayout_InDesignMode" value="" />
<input type="hidden" name="MSOSPWebPartManager_OldDisplayModeName" id="MSOSPWebPartManager_OldDisplayModeName" value="Browse" />
<input type="hidden" name="MSOSPWebPartManager_StartWebPartEditingName" id="MSOSPWebPartManager_StartWebPartEditingName" value="false" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUBMA9kFgJmD2QWBAICD2QWBAIID2QWAmYPZBYCAgEPFgIeE1ByZXZpb3VzQ29udHJvbE1vZGULKYgBTWljcm9zb2Z0LlNoYXJlUG9pbnQuV2ViQ29udHJvbHMuU1BDb250cm9sTW9kZSwgTWljcm9zb2Z0LlNoYXJlUG9pbnQsIFZlcnNpb249MTIuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49NzFlOWJjZTExMWU5NDI5YwFkAg8PZBYEAgEPFgIeB1Zpc2libGVoZAIDDxYCHwFoZAIED2QWBAIFD2QWBgIBD2QWAgIBDxYCHwFoFgJmD2QWBAICD2QWAgIDDxYCHwFoZAIDDw8WAh4JQWNjZXNzS2V5BQEvZGQCBw9kFgICAg8PFgIfAWcWAh4Fc3R5bGUFDmRpc3BsYXk6YmxvY2s7ZAIJD2QWAgIDD2QWAgIBDw8WAh8BZ2QWBAIBDw8WAh8BaGQWHAIBDw8WAh8BaGRkAgMPFgIfAWhkAgUPDxYCHwFoZGQCBw8WAh8BaGQCCQ8PFgIfAWhkZAILDw8WAh8BaGRkAg0PDxYCHwFoZGQCDw8PFgQeB0VuYWJsZWRoHwFoZGQCEQ8PFgIfAWhkZAITDw8WBB8EaB8BaGRkAhUPDxYCHwFoZGQCFw8WAh8BaGQCGQ8WAh8BaGQCGw8PFgIfAWdkZAIDDw8WAh8BZ2QWBgIBDw8WAh8BZ2RkAgMPDxYCHwFnZGQCBQ8PFgIfAWdkZAIHD2QWBgIFDw8WAh4LUGFyYW1WYWx1ZXMyoAQAAQAAAP////8BAAAAAAAAAAwCAAAAWE1pY3Jvc29mdC5TaGFyZVBvaW50LCBWZXJzaW9uPTEyLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPTcxZTliY2UxMTFlOTQyOWMFAQAAAD1NaWNyb3NvZnQuU2hhcmVQb2ludC5XZWJQYXJ0UGFnZXMuUGFyYW1ldGVyTmFtZVZhbHVlSGFzaHRhYmxlAQAAAAVfY29sbAMcU3lzdGVtLkNvbGxlY3Rpb25zLkhhc2h0YWJsZQIAAAAJAwAAAAQDAAAAHFN5c3RlbS5Db2xsZWN0aW9ucy5IYXNodGFibGUHAAAACkxvYWRGYWN0b3IHVmVyc2lvbghDb21wYXJlchBIYXNoQ29kZVByb3ZpZGVyCEhhc2hTaXplBEtleXMGVmFsdWVzAAADAwAFBQsIHFN5c3RlbS5Db2xsZWN0aW9ucy5JQ29tcGFyZXIkU3lzdGVtLkNvbGxlY3Rpb25zLklIYXNoQ29kZVByb3ZpZGVyCOxROD8DAAAACgoLAAAACQQAAAAJBQAAABAEAAAAAwAAAAYGAAAABlVzZXJJRAYHAAAABVdlYklEBggAAAAFVG9kYXkQBQAAAAMAAAAGCQAAAA9DdXJyZW50VXNlck5hbWUGCgAAAAdSb290V2ViBgsAAAAUMjAxMy0wOC0yMVQwODoyMjozMFoLZGQCBw9kFhwCBw8WAh8ACysEAWQCCQ8WAh8ACysEAWQCCw8WAh8ACysEAWQCDQ8WAh8ACysEAWQCDw8WAh8ACysEAWQCEQ8WAh8ACysEAWQCEw8WAh8ACysEAWQCFQ8WAh8ACysEAWQCFw8WAh8ACysEAWQCGQ8WAh8ACysEAWQCGw8WAh8ACysEAWQCHQ8WAh8ACysEAWQCHw8WAh8ACysEAWQCIQ8PFgIfBTLcBAABAAAA/////wEAAAAAAAAADAIAAABYTWljcm9zb2Z0LlNoYXJlUG9pbnQsIFZlcnNpb249MTIuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49NzFlOWJjZTExMWU5NDI5YwUBAAAAPU1pY3Jvc29mdC5TaGFyZVBvaW50LldlYlBhcnRQYWdlcy5QYXJhbWV0ZXJOYW1lVmFsdWVIYXNodGFibGUBAAAABV9jb2xsAxxTeXN0ZW0uQ29sbGVjdGlvbnMuSGFzaHRhYmxlAgAAAAkDAAAABAMAAAAcU3lzdGVtLkNvbGxlY3Rpb25zLkhhc2h0YWJsZQcAAAAKTG9hZEZhY3RvcgdWZXJzaW9uCENvbXBhcmVyEEhhc2hDb2RlUHJvdmlkZXIISGFzaFNpemUES2V5cwZWYWx1ZXMAAAMDAAUFCwgcU3lzdGVtLkNvbGxlY3Rpb25zLklDb21wYXJlciRTeXN0ZW0uQ29sbGVjdGlvbnMuSUhhc2hDb2RlUHJvdmlkZXII7FE4PwYAAAAKCgsAAAAJBAAAAAkFAAAAEAQAAAAEAAAABgYAAAAGVXNlcklEBgcAAAAGV2ViVVJMBggAAAAFVG9kYXkGCQAAAAhMaXN0TmFtZRAFAAAABAAAAAYKAAAAD0N1cnJlbnRVc2VyTmFtZQYLAAAAFHtzaXRlY29sbGVjdGlvbnJvb3R9BgwAAAAUMjAxMy0wOC0yMVQwODoyMjozMFoGDQAAABpNYXVpIEhvbWVwYWdlIENvbnRlbnQgTGlzdAtkZAIPD2QWAgIBDw9kFgQeB29uY2xpY2sFeWRvY3VtZW50LmZvcm1zWzBdLm9ua2V5cHJlc3MgPSBuZXcgRnVuY3Rpb24oInJldHVybiBXZWJGb3JtX0ZpcmVEZWZhdWx0QnV0dG9uKGV2ZW50LCAnY3RsMDBfU2VhcmNoQm94MV9TZWFyY2hCdXR0b24nKTsiKTseB29uZm9jdXMFeWRvY3VtZW50LmZvcm1zWzBdLm9ua2V5cHJlc3MgPSBuZXcgRnVuY3Rpb24oInJldHVybiBXZWJGb3JtX0ZpcmVEZWZhdWx0QnV0dG9uKGV2ZW50LCAnY3RsMDBfU2VhcmNoQm94MV9TZWFyY2hCdXR0b24nKTsiKTtkGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYBBR1jdGwwMCRTZWFyY2hCb3gxJFNlYXJjaEJ1dHRvbiJ9BkUI2Vh1BHZxhL2tJH/Loh4w" />
</div>

    <script type="text/javascript">
    //<![CDATA[
        var theForm = document.forms['aspnetForm'];
        if (!theForm) {
            theForm = document.aspnetForm;
        }
        function __doPostBack(eventTarget, eventArgument) {
            if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
                theForm.__EVENTTARGET.value = eventTarget;
                theForm.__EVENTARGUMENT.value = eventArgument;
                theForm.submit();
            }
        }
    //]]>
    </script>


    <script src="http://www.maui.co.nz/WebResource.axd?d=frHV1YZmGXYiOpP5j3vKsA2&amp;t=633408290452760872" type="text/javascript"></script>

    <script>    var MSOWebPartPageFormName = 'aspnetForm';</script><script type="text/JavaScript" language="JavaScript">
    <!--
                                                                    var L_Menu_BaseUrl = "";
                                                                    var L_Menu_LCID = "1033";
                                                                    var L_Menu_SiteTheme = "";
    //-->
    </script>
    <script type="text/javascript">
    //<![CDATA[

        function DoCallBack(filterText) {
            WebForm_DoCallback('ctl00$g_2adf51ea_8ec4_4b7d_b8a5_a8fa9d63a682', filterText, UpdateFilterCallback, 0, CallBackError, true);
        }
        function CallBackError(result, clientsideString) {
        }
        function WebForm_OnSubmit() {
            UpdateFormDigest('\u002f', 1440000); return _spFormOnSubmitWrapper();
            return true;
        }
    //]]>
    </script>
    <div>
    </div>
        
    <div id="ctl00_MSO_ContentDiv" class="root">
        <div id="page">
            <script type="text/javascript">
                //if (typeof $(document).getUrlParam('popup') != 'object')
                //    $('#page').addClass('popup');
            </script>
            <!-- Start CrossSell Header -->
            <div id="crossSellWrapper">
                <div id="crossSell">
                    <!--<div id="crossSellLogo"><img src="/SiteCollectionImages/Core/logo-thl.jpg" height="25" width="85" alt="thl"></div>-->
                    <ul class="crossSellNav">
                        <li><span class="countryOn"><span class="catTitle">RENTALS</span> New Zealand</span></li>
                        <li><a href="http://nzrentals.keacampers.com/?utm_source=maui.co.nz&utm_medium=referral&utm_campaign=CrossLink"
                            title="KEA Camper Hire NZ" class="maui">KEA</a></li>
                        <li><span class="mauiCurrent">Maui</span></li>
                        <li><a href="http://www.unitedcampervans.co.nz/?utm_source=maui.co.nz&utm_medium=referral&utm_campaign=CrossLink"
                            title="United Motorhome Rentals" class="maui">United</a></li>
                        <li><a href="http://www.britz.co.nz/?utm_source=maui.co.nz&utm_medium=referral&utm_campaign=CrossLink"
                            class="maui">Britz</a></li>
                        <li><a href="http://www.alphacampervans.co.nz/?utm_source=maui.co.nz&utm_medium=referral&utm_campaign=CrossLink"
                            title="Alpha Campervan Rental" class="maui">Alpha</a></li>
                        <li><a href="http://www.mightycampers.co.nz/?utm_source=maui.co.nz&utm_medium=referral&utm_campaign=CrossLink"
                            class="maui">Mighty</a></li>
                    </ul>
                    <div id="crossSellCountry">
                        <ul class="crossSellNav" style="border-left: 1px solid #f5f5f5;">
                            <li><span class="country"><span class="catTitle">RENTALS</span>Australia</span>
                                <ul class="crossSellNavDD">
                                    <li class="mauiC"><a href="http://aurentals.keacampers.com/?utm_source=maui.co.nz&utm_medium=referral&utm_campaign=CrossLink"
                                        title="KEA Campervan Hire">KEA</a></li>
                                    <li class="mauiC"><a href="http://www.maui.com.au/?utm_source=maui.co.nz&utm_medium=referral&utm_campaign=CrossLink">
                                        Maui</a></li>
                                    <li class="mauiC"><a href="http://www.britz.com.au/?utm_source=maui.co.nz&utm_medium=referral&utm_campaign=CrossLink">
                                        Britz</a></li>
                                    <li class="mauiC"><a href="http://www.mightycampers.com.au/?utm_source=maui.co.nz&utm_medium=referral&utm_campaign=CrossLink">
                                        Mighty</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="crossSellNav" style="border-left: 1px solid #f5f5f5;">
                            <li><span class="country"><a href="http://www.roadbearrv.com"><span class="catTitle">
                                RENTALS</span>USA</a></span></li>
                        </ul>
                        <ul class="crossSellNav" style="border-left: 1px solid #f5f5f5; border-right: 1px solid #f5f5f5;">
                            <li><span class="country"><a href="http://www.motekvehicles.com/?utm_source=maui.co.nz&utm_medium=referral&utm_campaign=CrossLink">
                                <span class="catTitle">BUY</span> a Campervan</a></span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- End CrossSell Header -->
            <div id="header">
            	<!-- app link -->
            	
            
                <div id="header-top">
                <a href="/holiday-advice/tourism-radio" id="mauiapp"></a>
                    <div id="header-text">
						
</div>
                    <div id="quick-link">
                        <ul id="quick-link-menu" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal"><li><a href="/online-rental-check-in" title="checkin" id="check-in">Check in online</a></li><li><a href="/contact-maui-motorhome-hire-new-zealand" title="needhelp" id="need-help">contact us</a></li><li><a href="http://www.thlonline.com/AgentMediaCentre/AgentResources/maui/Pages/Maui-Resources.aspx" title="travelagent" id="travel-agent">travel agents</a></li><li><a href="http://www.thlonline.com/Media/Pages/default.aspx" title="media" id="media">media</a></li><li id="flag-container"><a href="#" title="flag" id="flagicon" class="flag">language</a><div class="groupitems"><ul><li class="first">Change language:</li><li><img alt="Français" border="0" src="/SiteCollectionImages/assets/French-flag.gif" style="BORDER: 0px solid; "><a href="/Fr">Français</a></li><li><img alt="Nederlands" border="0" src="/SiteCollectionImages/assets/Dutch-Flag.gif" style="BORDER: 0px solid; "><a href="/een-motorhome-nieuw-zeeland?lang=dutch">Nederlands</a></li><li><img alt="Deutsch" border="0" src="/SiteCollectionImages/assets/German-Flag.gif" style="BORDER: 0px solid; "><a href="/wohnmobile-campervans-neuseeland?lang=german">Deutsch</a></li><li><img alt="" border="0" src="/PublishingImages/english-flag.jpg" style="BORDER: 0px solid; "><a href="/" title="English">English</a></li></ul></div></li></ul>
                    </div>
                </div>
                <div id="header-bottom">
                        <a id="maui-logo" href="/">maui motorhome, car rental and campervan hire nz</a>
                    <div id="menu">
                        <ul id="mega-menu">
                            <li id="menu-home" class="level1 vehicle" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal"><a href="/motorhome-hire">motorhome hire</a><div class="level2"><div class="vertical"><div class="group-header"><h3><a href="/motorhome-hire/platinum-campervans" title="Platinum Series Campervans - Luxury Motorhome Hire">platinum campervan series</a></h3><div class="description">apartment styled living on the road, leather couches, comfort, space &amp; style.</div></div><div class="groupitems"><ul><li class="groupitem first"><a href="/motorhome-hire/platinum-campervans-ultima" title="ultima" class="pageLink">ultima</a><span class="imgContainer"><img alt="ultima campervan" border="0" src="/CentralLibraryImages/Maui/New-Zealand/Campervans/nzavm.2BTSMTV/nzavm.2BTSMTV-Clearcut-144.jpg" style="BORDER: 0px solid; "></span><h3><a href="/motorhome-hire/platinum-campervans-ultima" title="Ultima Campervan">Ultima</a></h3><p>sleeps up to 2<br><span class="description">kiwi cuisine courtesy of exterior BBQ</span><br></p></li><li class="groupitem"><a href="/motorhome-hire/platinum-campervans-beach" title="platinum beach" class="pageLink">platinum beach</a><span class="imgContainer"><img alt="platinum beach motorhome" border="0" src="/CentralLibraryImages/Maui/New-Zealand/Campervans/nzavm.4BMP/nzavm.4BMP-Clearcut-144.jpg" style="BORDER: 0px solid; "></span><h3><a href="/motorhome-hire/platinum-campervans-beach" title="platinum beach campervan">Platinum Beach</a></h3><p>sleeps up to 4<br><span class="description">unparalleled personal space</span><br></p></li><li class="groupitem"><a href="/motorhome-hire/platinum-campervans-river" title="platinum river" class="pageLink">platinum river</a><span class="imgContainer"><img alt="plainum river motorhome" border="0" src="/CentralLibraryImages/Maui/New-Zealand/Campervans/nzavm.6BMPC/nzavm.6BMPC-Clearcut-144.jpg" style="BORDER: 0px solid; "></span><h3><a href="/motorhome-hire/platinum-campervans-river" title="platinum river campervan">Platinum River</a></h3><p>sleeps up to 6<br><span class="description">space &amp; storage designed for families</span><br></p></li><li class="groupitem"><a href="http://www.motekvehicles.com" title="Motek Vehicle Sales" class="pageLink">Motek Vehicle Sales</a><span class="imgContainer"><img alt="Motek Vehicle Sales" border="0" src="/SiteCollectionImages/promotions/MM-motek.gif" style="BORDER: 0px solid; "></span><h3><a href="http://www.motekvehicles.com">Buy a motorhome</a></h3><p>Built rental tough<br><span class="description">New &amp; Used Motorhomes, Campervans &amp; Caravans for Sale</span><br></p></li></ul></div></div><div class="vertical"><div class="group-header"><h3>other info</h3><div class="description"></div></div><div class="groupitems noimage"><ul><li><a href="/motorhome-hire/compare-maui-motorhomes" title="Campervan comparison chart">Compare all maui campervans</a></li><li><a href="/motorhome-hire/faqs" title="Frequently asked questions">Frequently asked questions</a></li><li><a href="/motorhome-hire/buy-a-motorhome">Buy a motorhome or campervan</a></li></ul></div></div></div></li><li id="menu-car" class="level1 vehicle" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal"><a href="/car-rental">car rental</a><div class="level2"><div class="vertical"><div class="group-header"><h3>car rental selection</h3><div class="description">maui offers an extensive fleet of rental cars. From smaller Economy cars to larger 7 seater vans, we can provide a vehicle to suit your needs</div></div><div class="groupitems"><ul><li class="groupitem first"><a href="/car-rental/economy-car" title="economy car" class="pageLink">economy car</a><span class="imgContainer"><img alt="economy car hire" border="0" src="/CentralLibraryImages/Maui/New-Zealand/Cars/nzacm.ecmrm/nzacm.ecmrm-Clearcut-144.jpg" style="BORDER: 0px solid; "></span><h3><a href="/car-rental/economy-car" title="Economy Car">Economy car</a></h3><p>up to 4 adults<br><span class="description">Toyota Yaris or similar</span><br></p></li><li class="groupitem"><a href="/car-rental/compact-car" title="compact car" class="pageLink">compact car</a><span class="imgContainer"><img alt="" border="0" src="/CentralLibraryImages/Maui/New-Zealand/Cars/nzacm.cdar/nzacm.cdar-Clearcut-144.jpg" style="BORDER: 0px solid; "></span><h3><a href="/car-rental/compact-car">Compact Car</a></h3><p>up to 5 adults<br><span class="description">Holden Barina or similar</span><br></p></li><li class="groupitem"><a href="/car-rental/mid-size-car-hire" title="intermediate car" class="pageLink">intermediate car</a><span class="imgContainer"><img alt="intermediate car hire" border="0" src="/CentralLibraryImages/Maui/New-Zealand/Cars/nzacm.icarm/nzacm.icarm-Clearcut-144.jpg" style="BORDER: 0px solid; "></span><h3><a href="/car-rental/mid-size-car-hire">Intermediate car</a></h3><p>up to 5 adults<br><span class="description">Ford Focus or similar</span><br></p></li><li class="groupitem"><a href="/car-rental/standard-car" title="Standard Car" class="pageLink">Standard Car</a><span class="imgContainer"><img alt="Standard Car hire" border="0" src="/CentralLibraryImages/Maui/New-Zealand/Cars/nzacm.sdar/nzacm.sdar-Clearcut-144.jpg" style="BORDER: 0px solid; "></span><h3><a href="/car-rental/standard-car">Standard Car</a></h3><p>up to 5 adults<br><span class="description">Hyundai Elantra or similar</span><br></p></li><li class="groupitem"><a href="/car-rental/executive-car" title="Full Size Car" class="pageLink">Full Size Car</a><span class="imgContainer"><img alt="Full Size Car rental" border="0" src="/CentralLibraryImages/Maui/New-Zealand/Cars/nzacm.fcarm/nzacm.FCARM-Clearcut-144.jpg" style="BORDER: 0px solid; "></span><h3><a href="/car-rental/executive-car">Full Size Car</a></h3><p>up to 5 adults<br><span class="description">Hyundai i45 or similar</span><br></p></li><li class="groupitem"><a href="/car-rental/4wd-car" title="4wd car" class="pageLink">4wd car</a><span class="imgContainer"><img alt="4WD Car hire" border="0" src="/CentralLibraryImages/Maui/New-Zealand/Cars/nzacm.pfarm/nzacm.pfarm-Clearcut-144.jpg" style="BORDER: 0px solid; "></span><h3><a href="/car-rental/4wd-car" title="4WD Car Rental">4WD car</a></h3><p>up to 5 adults<br><span class="description">Ford Kuga or similar</span><br></p></li><li class="groupitem"><a href="/car-rental/standard-awd" title="Standard AWD" class="pageLink">Standard AWD</a><span class="imgContainer"><img alt="Standard AWD hire" border="0" src="/CentralLibraryImages/Maui/New-Zealand/Cars/nzacm.sfar/nzacm.sfar-Clearcut-144.jpg" style="BORDER: 0px solid; "></span><h3><a href="/car-rental/standard-awd">Standard AWD</a></h3><p>up to 5 adults<br><span class="description">Holden Captiva or similar</span><br></p></li><li class="groupitem"><a href="/car-rental/standard-4wd-ute" title="Standard 4WD Ute" class="pageLink">Standard 4WD Ute</a><span class="imgContainer"><img alt="Standard 4WD Ute hire" border="0" src="/CentralLibraryImages/Maui/New-Zealand/Cars/nzacm.sfbr/nzacm.sfbr-Clearcut-144.jpg" style="BORDER: 0px solid; "></span><h3><a href="/car-rental/standard-4wd-ute">Standard 4WD Ute</a></h3><p>up to 5 adults<br><span class="description">Mitsubishi Triton or similar</span><br></p></li><li class="groupitem"><a href="/car-rental/large-4wd" title="Large 4WD" class="pageLink">Large 4WD</a><span class="imgContainer"><img alt="Large 4WD hire" border="0" src="/CentralLibraryImages/Maui/New-Zealand/Cars/nzacm.ffar/nzacm.ffar-Clearcut-144.jpg" style="BORDER: 0px solid; "></span><h3><a href="/car-rental/large-4wd">Large 4WD</a></h3><p>up to 5 adults<br><span class="description">Toyota Highlander or similar</span><br></p></li><li class="groupitem"><a href="/car-rental/people-mover" title="people mover" class="pageLink">people mover</a><span class="imgContainer"><img alt="People mover car rental" border="0" src="/CentralLibraryImages/Maui/New-Zealand/Cars/nzacm.lvarm/nzacm.lvarm-Clearcut-144.jpg" style="BORDER: 0px solid; "></span><h3><a href="/car-rental/people-mover" title="People Mover Car Hire">People Mover </a></h3><p>up to 8 adults<br><span class="description">Hyundai iMax or similar</span><br></p></li><li class="groupitem"><a href="/car-rental/intermediate-12-seater" title="Intermediate 12 Seater" class="pageLink">Intermediate 12 Seater</a><span class="imgContainer"><img alt="Intermediate 12 Seater hire" border="0" src="/CentralLibraryImages/Maui/New-Zealand/Cars/nzacm.ivar/nzacm.ivar-Clearcut-144.jpg" style="BORDER: 0px solid; "></span><h3><a href="/car-rental/intermediate-12-seater">Intermediate 12 Seater</a></h3><p>up to 12 adults<br><span class="description">Toyota Hiace or similar</span><br></p></li></ul></div></div><div class="vertical"><div class="group-header"><h3>other info</h3><div class="description"></div></div><div class="groupitems noimage"><ul><li><a href="/car-rental/compare-maui-cars">Compare all maui cars</a></li></ul></div></div></div></li><li id="menu-promotion" class="level1" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal"><a href="/motorhome-hire-promotions">promotions</a><div class="level2"><div class="vertical"><div class="group-header"><h3>our campervan hire NZ offers</h3><div class="description"></div></div><div class="groupitems"><ul><li class="groupitem first"><a href="/special-motorhome-17OFF" title="Spring Deal - Up to 17% off" class="pageLink">Spring Deal - Up to 17% off</a><span class="imgContainer"><img alt="Spring deal - up to 17% off" border="0" src="/SiteCollectionImages/promotions/Mega-Menu-6B-17OFF.png" style="BORDER: 0px solid; "></span><h3><a href="/special-motorhome-17OFF">Travel by 30 November 2013</a></h3><p>book before 31 August 2013<br><span class="description">All New Zealand Locations*</span><br></p></li><li class="groupitem"><a href="/motorhome-hire-promotions/short-breaks-inclusive-special" title="3 Day Short Break All Inclusive" class="pageLink">3 Day Short Break All Inclusive</a><span class="imgContainer"><img alt="3 Day Short Break All Inclusive" border="0" src="/SiteCollectionImages/promotions/Mega-Menus_Short_Breaks.png" style="BORDER: 0px solid; "></span><h3><a href="/motorhome-hire-promotions/short-breaks-inclusive-special">Travel by 15 September 2013</a></h3><p>book before 12 Sept 2013<br><span class="description">To/From: Auckland, Christchurch, Queenstown</span><br></p></li><li class="groupitem"><a href="/motorhome-hire-promotions/maui-winery-havens" title="maui Winery Havens" class="pageLink">maui Winery Havens</a><span class="imgContainer"><img alt="maui Winery Havens" border="0" src="/SiteCollectionImages/promotions/MM-promo-winery-havens.png" style="BORDER: 0px solid; "></span><h3><a href="/motorhome-hire-promotions/maui-winery-havens"> </a></h3><p>4 world class wineries<br><span class="description">3 course gourmet hamper x 2, Camping on well secured sites</span><br></p></li></ul></div></div><div class="vertical"><div class="group-header"><h3>more great value and ideas from maui</h3><div class="description"></div></div><div class="groupitems noimage"><ul><li><a href="/motorhome-hire-promotions/doc-campsite-passes">DOC Campsite Passes</a></li><li><a href="/motorhome-hire-promotions/AA-101-Must-Dos">AA 101 must-do's</a></li><li><a href="/motorhome-hire-promotions/sponsored-events">Events we love</a></li><li><a href="/motorhome-hire-promotions/early-booking-rates">Early booking rates</a></li><li><a href="/motorhome-hire-promotions/long-hire-rates">Long hire rates</a></li><li><a href="/motorhome-hire-promotions/multi-hire-discounts">Multi-hire discounts</a></li><li><a href="/motorhome-hire-promotions/convoys">Motorhome convoys/groups</a></li><li><a href="/motorhome-hire-promotions/feature-film-hires">Film trailer hire</a></li><li><a href="/motorhome-hire-promotions/relocations">Campervan relocations</a></li><li><a href="/motorhome-hire-promotions/gift-vouchers">maui gift vouchers</a></li></ul></div></div></div></li><li id="menu-location" class="level1 location" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal"><a href="/campervan-hire-locations">campervan and car hire locations NZ</a><div class="level2"><div class="vertical"><div class="group-header"><h3>campervan &amp; car hire locations NZ</h3><div class="description">get directions with our interactive Google maps</div></div><div class="groupitems"><ul><li class="groupitem first"><a href="/campervan-hire-locations/auckland-airport" title="Auckland airport branch" class="pageLink">Auckland airport branch</a><span class="imgContainer"><img alt="Auckland Airport Campervan Hire" border="0" src="/SiteCollectionImages/BranchLocations/Auckland-Airport.jpg" style="BORDER: 0px solid; "></span><h3><a href="/campervan-hire-locations/auckland-airport" title="Auckland Motorhome Hire">Auckland airport branch</a></h3><p>campervan hire<br><span class="description">free airport pick up &amp; drop off</span><br></p></li><li class="groupitem"><a href="/campervan-hire-locations/christchurch-airport" title="Christchurch airport branch" class="pageLink">Christchurch airport branch</a><span class="imgContainer"><img alt="Christchurch Airport Branch - view our interacitve Google map" border="0" src="/SiteCollectionImages/BranchLocations/Christchurch-Airport.jpg" style="BORDER: 0px solid; "></span><h3><a href="/campervan-hire-locations/christchurch-airport" title="Christchurch Motorhome Hire">Christchurch airport branch</a></h3><p>campervan hire<br><span class="description">free airport pick up and drop off</span><br></p></li><li class="groupitem"><a href="/campervan-hire-locations/queenstown-airport" title="Queenstown branch" class="pageLink">Queenstown branch</a><span class="imgContainer"><img alt="Queenstown Motorhome Hire - view our interactive Google map" border="0" src="/SiteCollectionImages/BranchLocations/Queenstown.jpg" style="BORDER: 0px solid; "></span><h3><a href="/campervan-hire-locations/queenstown-airport" title="Queenstown Motorhome Hire">Queenstown branch</a></h3><p>campervan hire<br><span class="description">free shuttle to branch</span><br></p></li><li class="groupitem"><a href="/car-hire-locations" title="Car hire locations" class="pageLink">Car hire locations</a><span class="imgContainer"><img alt="Car Hire New Zealand" border="0" src="/SiteCollectionImages/BranchLocations/car-hire.jpg" style="BORDER: 0px solid; "></span><h3><a href="/car-hire-locations" title="Car hire locations">Car hire locations</a></h3><p>car hire<br><span class="description"></span><br></p></li></ul></div></div></div></li><li id="menu-holiday" class="level1" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal"><a href="/motorhome-holiday-advice">holiday advice</a><div class="level2"><div class="horizontal"><h3>before you arrive</h3><ul><li class="groupitem"><a href="/accident-liability-reduction-options">Reduce your accident liability</a></li><li class="groupitem"><a href="/extra-hire-items-vans">Extra hire items</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/what-to-pack">Things to bring</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/new-zealand-accommodation">Where to stay</a></li><li class="groupitem"><a href="/holiday-advice/campervan-hire-locations">Branch locations</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/new-zealand-ferries">North/South Island Ferries</a></li><li class="groupitem"><a href="/online-rental-check-in" title="Campervan check-in online">Online check-In</a></li></ul></div><div class="horizontal"><h3><a href="/motorhome-holiday-advice/campervan-driving-routes" title="Campervan driving routes in New Zealand">driving routes</a></h3><ul><li class="groupitem"><a href="/motorhome-holiday-advice/campervan-driving-routes/twin-coast-discovery-highway" title="Twin Coast Discovery Highway">Twin Coast Discovery Highway</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/campervan-driving-routes/pacific-coast-highway" title="Pacific Coast Highway">Pacific Coast Highway</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/campervan-driving-routes/thermal-explorer-highway" title="Thermal Explorer Highway - Camper Driving Routes New Zealand">Thermal Explorer Highway</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/campervan-driving-routes/classic-wine-trail" title="Wine Trail - Motor Home Driving Routes New Zealand">Classic Wine Trail</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/campervan-driving-routes/southern-scenic-route" title="Southern Scenic Route - Camper Van Driving Routes New Zealand">Southern Scenic Route</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/campervan-driving-routes/alpine-pacific-highway" title="Alpine Pacific Highway - Motorhome Driving Routes New Zealand">Alpine Pacific Highway</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/campervan-driving-routes/inland-scenic-route-72" title="Inland Scenic Route 72 - Motorhome Road Trips New Zealand">Inland Scenic Route 72</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/campervan-driving-routes/the-northern-lights" title="The Northern Lights - Motorhome Road Trips New Zealand">The Northern Lights</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/campervan-driving-routes/middle-country" title="Middle Country - Campervan Road Trips New Zealand">Middle Country</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/campervan-driving-routes/native-parks-north-island" title="Native Parks - Motorhome Havens North Island">Native Parks - North Island</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/campervan-driving-routes/native-parks-south-island" title="Native Parks - Motorhome Havens South Island">Native Parks - South Island</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/campervan-driving-routes/cultural-tourism-north-island" title="Cultural Tourism North Island - Campervan Road Trips">Cultural Tourism - North Island</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/campervan-driving-routes/cultural-tourism-south-island" title="Cultural Tourism South Island - Campervan Road Trips">Cultural Tourism - South Island</a></li></ul></div><div class="horizontal"><h3><a href="/motorhome-holiday-advice/regional-highlights" title="NZ regional highlights">regional highlights</a></h3><ul><li class="groupitem"><a href="/motorhome-holiday-advice/regional-highlights/auckland-northland" title="Auckland &amp; Northland Tourist Information">Auckland &amp; Northland</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/regional-highlights/christchurch-canterbury" title="Christchurch &amp; Canterbury Tourist Information">Christchurch &amp; Canterbury</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/regional-highlights/dunedin-otago-southland" title="Dunedin, Otago &amp; Southland Tourist Information">Dunedin, Otago &amp; Southland</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/regional-highlights/fiordland-wanaka" title="Queenstown, Fiordland &amp; Wanaka Tourist Information">Queenstown, Fiordland &amp; Wanaka</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/regional-highlights/marlborough-nelson" title="Marlborough &amp; Nelson Tourist Information">Marlborough &amp; Nelson</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/regional-highlights/eastern-north-island" title="Eastern North Island Tourist Information">Eastern North Island</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/regional-highlights/central-north-island" title="Central North Island Tourist Information">Central North Island</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/regional-highlights/wellington-wairarapa" title="Wellington &amp; Wairarapa Tourist Information">Wellington &amp; Wairarapa</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/regional-highlights/western-north-island" title="Western North Island Tourist Information">Western North Island</a></li></ul></div><div class="horizontal"><h3>on the road</h3><ul><li class="groupitem"><a href="/motorhome-holiday-advice/campervan-driving-tips" title="Motorhome &amp; Campervan driving tips in New Zealand">Campervan driving tips</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/new-zealand-produce" title="Meals in your camper">Meals in your camper</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/dump-stations-and-waste-disposal" title="New Zealand campervan waste disposal">Camper waste disposal</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/camper-safety" title="Motorhome safety tips">Safety tips</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/maui-service" title="Maui service while on the road">Service while on the road</a></li><li class="groupitem"><a href="/holiday-advice/tourism-radio" title="Tourism Radio New Zealand">Tourism Radio Tour Guide</a></li></ul></div><div class="horizontal"><h3>your departure</h3><ul><li class="groupitem"><a href="/motorhome-holiday-advice/free-airport-transfers" title="Transfers to the airport">Transfers to the airport</a></li><li class="groupitem"><a href="/motorhome-holiday-advice/express-campervan-returns" title="Express campervan returns">Express campervan returns</a></li></ul></div></div></li><li id="menu-why" class="level1" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal"><a href="/why-choose-maui-campervans">why choose maui?</a><div class="level2"><div class="horizontal"><ul><li class="groupitem"><a href="/why-choose-maui-campervans/better-by-design" title="Design led - Better By Design">Design led - Better By Design</a></li><li class="groupitem"><a href="/why-choose-maui-campervans/motek" title="quality vehicles">Quality vehicles</a></li><li class="groupitem"><a href="/why-choose-maui-campervans/environmental-commitment" title="Caring for the environment">Caring for the environment</a></li><li class="groupitem"><a href="/why-choose-maui-campervans/conservation-volunteers">Conservation volunteers</a></li><li class="groupitem"><a href="/why-choose-maui-campervans/hassle-free-holidays" title="Nothing is too much trouble">Nothing is too much trouble</a></li><li class="groupitem"><a href="/why-choose-maui-campervans/about-thl" title="About maui &amp; thl">About maui &amp; thl</a></li></ul></div></div></li>
                        </ul>
                    </div>
                </div>
            </div>
            
	<div id="homePage">
        <div id="filmstrip">
            <div id="filmstrip-main">
                <div id="promotion-container">
                    <div class="transparent-bg">
                    </div>
                    <div id="promotions">
                        <h2 xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer" xmlns:SharePoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal"><a href="/promotions/" class="promotions-title">promotions</a><a href="/promotions/" id="all-promotions">see all promotions</a></h2><ul class="star"><li><a href="/special-motorhome-17OFF">Spring Deal - Up to 17% off</a></li ><li><a href="/motorhome-holiday-advice/new-zealand-accommodation">Book your Holiday Parks in advance online</a></li ></ul><ul class="star"><li><a href="/motorhome-hire-promotions/short-breaks-inclusive-special">3 Day Short Break All Inclusive</a></li ><li><a href="/motorhome-hire-promotions/maui-winery-havens">maui Winery Havens</a></li ></ul>
                    </div>
                </div>
                <div id="booking-container">
                    <div class="transparent-bg"></div>
                    <div id="booking-header"></div>
                	
    <!-- Start Booking Section -->
                    <div id="bookingControlContainer" class="bookingControlForm">
                        <!--<link rel="stylesheet" type="text/css" media="all" href="http://www.maui.co.nz/SiteCollectionDocuments/BookingControls/css/calendar/calendar.css" />-->
                        <link rel="stylesheet" type="text/css" media="all" href="http://www.maui.co.nz/SiteCollectionDocuments/BookingControls/css/m/vertical.css" />
                        <link href="https://secure.britz.com.au/css/m/jcal.css?v=2.4" type="text/css" rel="stylesheet" />
                        <style type="text/css">
                            
                            .booking-form div {
                                float: left;
                                clear:both;
                                margin: 5px 0 0 10px; 
                            }
                            
                            span.customSelect {
                                background-color: #FFFFFF;
                                color: #5C5C5C;
                                cursor: pointer;
                                display: inline-block;
                                font-size: 16px;
                                height: 27px;                                
                                padding: 8px 7px 0 10px;                                
                            } 
                            
                            .booking-form select {
                                
                            }
                            
                            span.customSelect .customSelectInner {
                                background: url("http://dev.britz.com.au/SiteCollectionImages/Core/customselect.png") no-repeat scroll right -6px transparent;
                            }
                            
                            .vehicleTab {
                                cursor: pointer;
                            }
                            
                            .vehicleTab.TabSelected.searchCars   {
                                background-position: -200px -640px !important;
                                 border-bottom: 1px solid #8B6F4D !important;
                                cursor: default !important;
                            }
                            
                            .vehicleTab.TabSelected.searchCampervans   {
                                background-position: 0 -640px !important;
                                 border-bottom: 1px solid #8B6F4D !important;
                                cursor: default !important;
                            }
                            
                            #vehicleModel, #pickUpLoc, #dropOffLoc, #countrOfRes {
                                width: 308px;
                            }
                            
                            #numberAdults, #numberChildren {
                                width: 152px;
                            }
                            
                            #pickUpDate, #dropOffDate {
                                padding-top: 4px;
                                height: 31px;
                                width: 152px;
                                border: none;
                                text-align: center;
                            }
                            
                            #pickUpTime, #dropOffTime {
                                height: 32px;
                                width: 152px;
                                border: none;
                            }    
                            
                             input.required, select.required + span  {
                                background-color: #FDEE00;
                            }  
                            
                            .bookingControlForm .spinner {
                                display: inline-block;
                                height: 24px;
                                position: absolute;
                                right: 220px;
                                top: 370px;
                                width: 24px;
                            }
                                            
                        </style>
                       <script src="/js/jstorage.min.js" type="text/javascript"></script>
                        <script type="text/javascript" src="/js/brandedWidget.js"></script>
                        <script type="text/javascript">
                            jQuery(document).ready(function () {
                                $('select').customSelect();                                
                                var options = { 'brand': 'm', 'cc': 'nz', 'vt': 'campervan', 'target': 'bookingcontrol', 'engine': 'https://secure.maui.co.nz/Selection.aspx' };
                                initWidget(options);
                            });                        
                        </script>
                        <div id="ctrlSearchBox_up1">
                            <div id="booking-front" class="vertical">
                                <ul id="booking-tab">
                                    <li id="booking-motorhome" class="searchCampervans TabSelected vehicleTab" data-vtype="campervan">
                                        <a id="campersTab">get a quote for a motorhome</a>
                                    </li>
                                    <li id="booking-car" class="searchCars vehicleTab" data-vtype="car">
                                        <a id="carsTab">get a quote for a car</a>
                                    </li>
                                </ul>
                                <div class="booking-form">
                                    <div>
                                        <span class="SearchBox_Title" id="ctrlSearchBox_lblTitle">Campervan hire</span>
                                    </div>
                                    <div>
                                        <span class="SearchBox_Subtitle" id="ctrlSearchBox_lblSubtitle">Search rates &amp; book</span>                                        
                                    </div>
                                    <div>
                                        <select id="vehicleModel" class="styled3" data-b2c="vh">
				                            <option value="">All models or select</option>				                                
				                        </select>                                                    
                                    </div>
                                    <div>
                                        <select class="styled5 pax" id="numberAdults" data-b2c="na">
                                            <option selected='selected' value='0'>Adults</option>
                                            <option value='1'>1 Adult</option>
                                            <option value='2'>2 Adults</option>
                                            <option value='3'>3 Adults</option>
                                            <option value='4'>4 Adults</option>
                                            <option value='5'>5 Adults</option>
                                            <option value='6'>6 Adults</option>
                                        </select>
                                    
                                        <select class="styled2 pax" id="numberChildren" data-b2c="nc">
                                            <option selected='selected' value='0'>Children</option>
                                            <option value='1'>1 Child</option>
                                            <option value='2'>2 Children</option>
                                            <option value='3'>3 Children</option>
                                            <option value='4'>4 Children</option>
                                            <option value='5'>5 Children</option>
                                        </select>                                                    
                                    </div>
                                    <div>
                                        <select class="styled combo" id="pickUpLoc" data-b2c="pb">
				                            <option value="">Pick up from?</option>                                                				                                
				                        </select>
                                    </div>
                                    <div>
                                        <input class="calElm" type="text" id="pickUpDate" value="Choose what date?" />
                                        <select class="time styled2" id="pickUpTime" data-b2c="pt">
                                            <option value="08:00">08:00am</option>
                                            <option value="08:30">08:30am</option>
                                            <option value="09:00">09:00am</option>
                                            <option value="09:30">09:30am</option>
                                            <option value="10:00" selected="selected">10:00am</option>
                                            <option value="10:30">10:30am</option>
                                            <option value="11:00">11:00am</option>
                                            <option value="11:30">11:30am</option>
                                            <option value="12:00">12:00pm</option>
                                            <option value="12:30">12:30pm</option>
                                            <option value="13:00">13:00pm</option>
                                            <option value="13:30">13:30pm</option>
                                            <option value="14:00">14:00pm</option>
                                            <option value="14:30">14:30pm</option>
                                            <option value="15:00">15:00pm</option>
                                            <option value="15:30">15:30pm</option>
                                            <option value="16:00">16:00pm</option>
                                            <option value="16:30">16:30pm</option>
                                            <option value="17:00">17:00pm</option>
                                        </select>
                                    </div>
                                    <div>
                                        <select class="styled combo" id="dropOffLoc" data-b2c="db">
				                            <option value="">Same as pick up?</option>				                                
				                        </select>
                                    </div>
                                    <div>
                                        <input class="calElm" type="text" id="dropOffDate" value="Choose what date?" />
                                        <select class="time styled2" id="dropOffTime" data-b2c="dt">
                                            <option value="08:00">08:00am</option>
                                            <option value="08:30">08:30am</option>
                                            <option value="09:00">09:00am</option>
                                            <option value="09:30">09:30am</option>
                                            <option value="10:00" selected="selected">10:00am</option>
                                            <option value="10:30">10:30am</option>
                                            <option value="11:00">11:00am</option>
                                            <option value="11:30">11:30am</option>
                                            <option value="12:00">12:00pm</option>
                                            <option value="12:30">12:30pm</option>
                                            <option value="13:00">13:00pm</option>
                                            <option value="13:30">13:30pm</option>
                                            <option value="14:00">14:00pm</option>
                                            <option value="14:30">14:30pm</option>
                                            <option value="15:00">15:00pm</option>
                                            <option value="15:30">15:30pm</option>
                                            <option value="16:00">16:00pm</option>
                                            <option value="16:30">16:30pm</option>
                                            <option value="17:00">17:00pm</option>
                                        </select>
                                    </div>
                                    <div>
                                        <select class="styled4" id="countrOfRes" data-b2c="cr">
                                            <option value="" selected="selected">Please select</option>
                                            <option value="AU">Australia</option>
                                            <option value="CA">Canada</option>
                                            <option value="DK">Denmark</option>
                                            <option value="FR">France</option>
                                            <option value="DE">Germany</option>
                                            <option value="NL">Netherlands</option>
                                            <option value="NZ">New Zealand</option>
                                            <option value="SE">Sweden</option>
                                            <option value="CH">Switzerland</option>
                                            <option value="UK">United Kingdom</option>
                                            <option value="US">United States Of America</option>
                                            <option value="-1" disabled="disabled">--------------------</option>
                                            <option value="AF">Afghanistan</option>
                                            <option value="AL">Albania</option>
                                            <option value="DZ">Algeria</option>
                                            <option value="AS">American Samoa</option>
                                            <option value="AD">Andora</option>
                                            <option value="AO">Angola</option>
                                            <option value="AI">Anguilla</option>
                                            <option value="AQ">Antarctica</option>
                                            <option value="AG">Antigua & Barbados</option>
                                            <option value="AR">Argentina</option>
                                            <option value="AM">Armenia</option>
                                            <option value="AW">Aruba</option>
                                            <option value="AT">Austria</option>
                                            <option value="AZ">Azerbaijan</option>
                                            <option value="BS?">Bahamas</option>
                                            <option value="BH">Bahrain</option>
                                            <option value="BD">Bangladesh</option>
                                            <option value="BB">Barbados</option>
                                            <option value="BY">Belarus</option>
                                            <option value="BE">Belgium</option>
                                            <option value="BZ">Belize</option>
                                            <option value="BJ">Benin</option>
                                            <option value="BM">Bermuda</option>
                                            <option value="BO">Bolivia</option>
                                            <option value="BA">Bosnia and Herzegovina</option>
                                            <option value="BW">Botswana</option>
                                            <option value="BV">Bouvet Island</option>
                                            <option value="BR">Brazil</option>
                                            <option value="IO">British Indian Ocean Territory</option>
                                            <option value="BN">Brunei</option>
                                            <option value="BG">Bulgaria</option>
                                            <option value="BF">Burkina Faso</option>
                                            <option value="BI">Burundi</option>
                                            <option value="BT">Butan</option>
                                            <option value="KH">Cambodia</option>
                                            <option value="CM">Cameroon</option>
                                            <option value="CV">Cape Verde</option>
                                            <option value="KY">Cayman Islands</option>
                                            <option value="CF">Cental African Republic</option>
                                            <option value="TD">Chad</option>
                                            <option value="CL">Chile</option>
                                            <option value="CN">China</option>
                                            <option value="CX">Christmas Island</option>
                                            <option value="CC">Cocos (Keeling) Island</option>
                                            <option value="CO">Colombia</option>
                                            <option value="KM">Comoros</option>
                                            <option value="CG">Congo</option>
                                            <option value="CK">Cook Island</option>
                                            <option value="CR">Costa Rica</option>
                                            <option value="CI">Cote D'ivoire</option>
                                            <option value="HR">Croatia</option>
                                            <option value="CU">Cuba</option>
                                            <option value="CY">Cyprus</option>
                                            <option value="CZ">Czech Republic</option>
                                            <option value="DJ">DjiboutiI</option>
                                            <option value="DM">Dominica</option>
                                            <option value="DO">Dominican Republic</option>
                                            <option value="TP">East Timor</option>
                                            <option value="EC">Ecuador</option>
                                            <option value="EG">Egypt</option>
                                            <option value="SV">El Salvador</option>
                                            <option value="GQ">Equatorial Guinea</option>
                                            <option value="ER">Eritrea</option>
                                            <option value="EE">Estonia</option>
                                            <option value="ET">Ethiopia</option>
                                            <option value="FK">Falkland Islands</option>
                                            <option value="FO">Faroe Islands</option>
                                            <option value="FJ">Fiji</option>
                                            <option value="FI">Finland</option>
                                            <option value="FX">France, Metropolitan</option>
                                            <option value="GF">French Guiana</option>
                                            <option value="PF">French Polynesia</option>
                                            <option value="TF">French Souther Territories</option>
                                            <option value="GA">Gabon</option>
                                            <option value="GM">Gambia</option>
                                            <option value="GE">Georgia</option>
                                            <option value="GH">Ghana</option>
                                            <option value="GI">Gibraltar</option>
                                            <option value="GR">Greece</option>
                                            <option value="GL">Greenland</option>
                                            <option value="GD">Grenada</option>
                                            <option value="GP">Guadeloupe</option>
                                            <option value="GU">Guam</option>
                                            <option value="GT">Guatemala</option>
                                            <option value="GN">Guinea</option>
                                            <option value="GW">Guinea-Bissau</option>
                                            <option value="GY">Guyana</option>
                                            <option value="HT">Haiti</option>
                                            <option value="HM">Heard and McDonald Islands</option>
                                            <option value="HN">Honduras</option>
                                            <option value="HK">Hong Kong</option>
                                            <option value="HU">Hungary</option>
                                            <option value="IS">Iceland</option>
                                            <option value="IN">India</option>
                                            <option value="ID">Indonesia</option>
                                            <option value="IR">Iran</option>
                                            <option value="IQ">Iraq</option>
                                            <option value="IE">Ireland</option>
                                            <option value="IL">Israel</option>
                                            <option value="IT">Italy</option>
                                            <option value="JM">Jamaica</option>
                                            <option value="JP">Japan</option>
                                            <option value="JO">Jordan</option>
                                            <option value="KZ">Kazakhstan</option>
                                            <option value="KE">Kenya</option>
                                            <option value="KI">Kiribati</option>
                                            <option value="KW">Kuwait</option>
                                            <option value="KG">Kyrgyzstan</option>
                                            <option value="LA">Lao Peoples Democratic Republic</option>
                                            <option value="LV">Latvia</option>
                                            <option value="LB">Lebanon</option>
                                            <option value="LS">Lesotho</option>
                                            <option value="LR">Liberia</option>
                                            <option value="LY">Libya</option>
                                            <option value="LI">Liechtenstein</option>
                                            <option value="LT">Lithuania</option>
                                            <option value="LU">Luxembourg</option>
                                            <option value="MO">Macau</option>
                                            <option value="MK">Macedonia</option>
                                            <option value="MG">Madagascar</option>
                                            <option value="MW">Malawi</option>
                                            <option value="MY">Malaysia</option>
                                            <option value="MV">Maldives</option>
                                            <option value="ML">Mali</option>
                                            <option value="MT">Malta</option>
                                            <option value="MH">Marshall Islands</option>
                                            <option value="MQ">Martinique</option>
                                            <option value="MR">Mauritania</option>
                                            <option value="MU">Mauritius</option>
                                            <option value="YT">Mayotte</option>
                                            <option value="MX">Mexico</option>
                                            <option value="FM">Micronesia, Federated States of</option>
                                            <option value="MD">Moldova</option>
                                            <option value="MC">Monaco</option>
                                            <option value="MN">Mongolia</option>
                                            <option value="MS">Montserrat</option>
                                            <option value="MA">Morocco</option>
                                            <option value="MZ">Mozambique</option>
                                            <option value="MM">Myanamar</option>
                                            <option value="NA">Namibia</option>
                                            <option value="NR">Nauru</option>
                                            <option value="NP">Nepal</option>
                                            <option value="AN">Netherlands Antilles</option>
                                            <option value="NC">New Caledonia</option>
                                            <option value="NI">Nicaragua</option>
                                            <option value="NE">Niger</option>
                                            <option value="NG">Nigeria</option>
                                            <option value="NU">Niue</option>
                                            <option value="NF">Norfolk Island</option>
                                            <option value="KP">North Korea</option>
                                            <option value="MP">Northern Mariana Islands</option>
                                            <option value="NO">Norway</option>
                                            <option value="OM">Oman</option>
                                            <option value="PK">Pakistan</option>
                                            <option value="PW">Palau</option>
                                            <option value="PA">Panama</option>
                                            <option value="PG">Papua New Guinea</option>
                                            <option value="PY">Paraguay</option>
                                            <option value="PE">Peru</option>
                                            <option value="PH">Philippines</option>
                                            <option value="PN">Pitcairn Islands</option>
                                            <option value="PL">Poland</option>
                                            <option value="PT">Portugal</option>
                                            <option value="PR">Puerto Rico</option>
                                            <option value="QA">Qatar</option>
                                            <option value="RE">Reunion</option>
                                            <option value="RO">Romania</option>
                                            <option value="RU">Russian Federation</option>
                                            <option value="RW">Rwanda</option>
                                            <option value="SH">Saint Helena</option>
                                            <option value="KN">Saint Kitts and Nevis</option>
                                            <option value="LC">Saint Lucia</option>
                                            <option value="PM">Saint Pierre and Miquelon</option>
                                            <option value="VC">Saint Vincent and The Grenadines</option>
                                            <option value="WS">Samoa</option>
                                            <option value="SM">San Marino</option>
                                            <option value="ST">Sao Tome and Principe</option>
                                            <option value="SA">Saudi Arabia</option>
                                            <option value="SN">Senegal</option>
                                            <option value="SC">Seychelles</option>
                                            <option value="SG">Singapore</option>
                                            <option value="SK">Slovakia</option>
                                            <option value="SI">Slovenia</option>
                                            <option value="SB">Solomon Islands</option>
                                            <option value="SO">Somalia</option>
                                            <option value="ZA">South Africa</option>
                                            <option value="GS">South Georgia</option>
                                            <option value="KR">South Korea</option>
                                            <option value="ES">Spain</option>
                                            <option value="SL">Sri Lanka</option>
                                            <option value="SD">Sudan</option>
                                            <option value="SR">Suriname</option>
                                            <option value="SJ">Svalbardand Jan Mayen Islands</option>
                                            <option value="SY">Syria</option>
                                            <option value="TI">Tahiti</option>
                                            <option value="TW">Taiwan</option>
                                            <option value="TJ">Tajikistan</option>
                                            <option value="TZ">Tanzania</option>
                                            <option value="TH">Thailand</option>
                                            <option value="TG">Togo</option>
                                            <option value="TK">Tokelau</option>
                                            <option value="TO">Tonga</option>
                                            <option value="TT">Trinidad and Tobago</option>
                                            <option value="TN">Tunisia</option>
                                            <option value="TR">Turkey</option>
                                            <option value="TM">Turkmenistan</option>
                                            <option value="TC">Turks and Caicos Islands</option>
                                            <option value="TV">Tuvalu</option>
                                            <option value="UG">Uganda</option>
                                            <option value="UA">Ukraine</option>
                                            <option value="AE">United Arab Emirates</option>
                                            <option value="UY">Uruguay</option>
                                            <option value="UM">US Minor Outlying Islands</option>
                                            <option value="UZ">Uzbekistan</option>
                                            <option value="VU">Vanuatu</option>
                                            <option value="VA">Vatican City</option>
                                            <option value="VE">Venezuala</option>
                                            <option value="VN">Vietnam</option>
                                            <option value="VG">Virgin Islands (GB)</option>
                                            <option value="VI">Virgin Islands (US)</option>
                                            <option value="WF">Wallis and Futuna Islands</option>
                                            <option value="EH">Western Sahara</option>
                                            <option value="YE">Yemen</option>
                                            <option value="YU">Yugoslavia</option>
                                            <option value="ZR">Zaire</option>
                                            <option value="ZM">Zambia</option>
                                            <option value="ZW">Zimbabwe</option>
                                        </select>
                                    </div>
                                    <div>
                                        <a href="#" id="searchButton" class="button StartSearchBtn">Get Quote</a>
                                        <span class="spinner"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    </div>
                </div>
                <div id="loadingBar" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal"
                    xmlns:js="urn:custom-javascript">
                    loading images...</div>
                <script type="text/javascript" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal"
                    xmlns:js="urn:custom-javascript">
                    function loading() {
                        $("#loadingBar").append('.');
                    }
                    loadInterval = setInterval(function () { loading(); }, 1000);
                </script>
                <div id="filmstrip-front-container" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal"
                    xmlns:js="urn:custom-javascript">
                    <div id="filmstrip-info">
                        <div id="filmstrip-textbox">
                            <div class="filmstrip-text-slider">
                                <div class="filmstrip-text-inner">
                                    <div class="filmstrip-text">
                                        <a href="/motorhome-hire-promotions/maui-winery-havens" title="NEW – Winery Havens"
                                            class="titleText">NEW – Winery Havens</a><div class="filmstrip-description">
                                                Enjoy an overnight winery stay</div>
                                        <ul>
                                            <li><a href="/motorhome-hire-promotions/maui-winery-havens">maui Winery Havens</a></li></ul>
                                    </div>
                                    <div class="filmstrip-text">
                                        <a href="/motorhome-hire/platinum-campervans-ultima" title="Ultima" class="titleText">
                                            Ultima</a><div class="filmstrip-description">
                                                Kiwi cuisine courtesy of exterior BBQ</div>
                                        <ul>
                                            <li><a href="/motorhome-hire/platinum-campervans-ultima">Ultima campervan</a></li><li>
                                                <span class="separator">|</span><a href="/motorhome-hire">See all campervans</a></li></ul>
                                    </div>
                                    <div class="filmstrip-text">
                                        <a href="/motorhome-hire/platinum-campervans-beach" title="Platinum Beach" class="titleText">
                                            Platinum Beach</a><div class="filmstrip-description">
                                                unparalleled personal space</div>
                                        <ul>
                                            <li><a href="/motorhome-hire/platinum-campervans-beach">Platinum Beach campervan</a></li><li>
                                                <span class="separator">|</span><a href="/motorhome-hire">See all campervans</a></li></ul>
                                    </div>
                                    <div class="filmstrip-text">
                                        <a href="/motorhome-hire/platinum-campervans-river" title="Platinum River" class="titleText">
                                            Platinum River</a><div class="filmstrip-description">
                                                space & storage designed for families</div>
                                        <ul>
                                            <li><a href="/motorhome-hire/platinum-campervans-river">Platinum River campervan</a></li><li>
                                                <span class="separator">|</span><a href="/motorhome-hire">See all campervans</a></li></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="filmstrip-controlbox">
                            <div id="filmstrip-arrow" class="blue-arrow-up">
                            </div>
                            <div id="filmstrip-control">
                                <div class="filmstrip-text">
                                    <ul>
                                        <li class="slider-control"></li>
                                        <li class="slider-control"></li>
                                        <li class="slider-control"></li>
                                        <li class="slider-control"></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="filmstrip-images" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal"
                    xmlns:js="urn:custom-javascript">
                    <div class="element">
                        <img alt="maui Winery Havens" border="0" src="http://www.maui.co.nz/SiteCollectionImages/promotions/Large-Home-winery-havens.jpg"
                            style="border: 0px solid;"></div>
                    <div class="element">
                        <img alt="" border="0" src="http://www.maui.co.nz/SiteCollectionImages/Homepage/Large-Ultima-Interior.jpg"
                            style="border: 0px solid;"></div>
                    <div class="element">
                        <img alt="Platinum Beach" border="0" src="http://www.maui.co.nz/SiteCollectionImages/Homepage/Large-Beach-Interior.jpg"
                            style="border: 0px solid;"></div>
                    <div class="element">
                        <img alt="Platinum River Motorhome" border="0" src="http://www.maui.co.nz/SiteCollectionImages/Homepage/Large-River-Exterior.jpg"
                            style="border: 0px solid;"></div>
                </div>
            </div>
        </div>
        <div id="contentPlaceHolder">
            <div id="quickInfo" class="contentFrame">
                <div class="col">
                    <h2>
                        &nbsp;contact us 24/7&nbsp;</h2>
                    <div class="col-main">
                        <div class="text">
                            <div id="ctl00_PlaceHolderMain_quickinfoContent1__ControlWrapper_RichHtmlField" style="display: inline">
                                <p>
                                    International tollfree</p>
                                <ul>
                                    <li><a href="/contact-maui-motorhome-hire-new-zealand">Where are you calling from?</a></li></ul>
                                <br>
                            </div>
                        </div>
                        <div class="img">
                            <div id="ctl00_PlaceHolderMain_quickinfoImg1__ControlWrapper_RichImageField" style="display: inline">
                                <span dir=""><a href="/contact-maui-motorhome-hire-new-zealand">
                                    <img alt="Contact Us 24 hours 7 days a week" border="0" src="http://www.maui.co.nz/SiteCollectionImages/Homepage/24-7-logo.jpg"
                                        style="border: 0px solid"></a></span>&nbsp;</div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <h2>
                        &nbsp;browse vehicles&nbsp;</h2>
                    <div class="col-main">
                        <div class="text">
                            <div id="ctl00_PlaceHolderMain_ctl02__ControlWrapper_RichHtmlField" style="display: inline">
                                <ul>
                                    <li><a title="Platinum series motorhomes &amp; campervans" href="/motorhome-hire/platinum-campervans">
                                        Platinum campervan series</a></li>
                                    <li><a title="Car rental New Zealand, Branches in Auckland, Christchurch &amp; Queenstown"
                                        href="/car-rental">Car rental</a></li>
                                    <li><a title="Buy a new or used campervan or motorhome" href="/motorhome-hire/buy-a-motorhome">
                                        Buy a new or used motorhome</a></li></ul>
                            </div>
                        </div>
                        <div class="img">
                            <div id="ctl00_PlaceHolderMain_ctl03__ControlWrapper_RichImageField" style="display: inline">
                                <span dir=""><a href="/motorhome-hire">
                                    <img alt="Campervan or Motorhome" border="0" src="http://www.maui.co.nz/SiteCollectionImages/Homepage/campervan.jpg"
                                        style="border: 0px solid"></a></span>&nbsp;</div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <h2>
                        &nbsp;plan your journey&nbsp;</h2>
                    <div class="col-main">
                        <div class="text">
                            <div id="ctl00_PlaceHolderMain_ctl05__ControlWrapper_RichHtmlField" style="display: inline">
                                <ul>
                                    <li><a title="maui Winery Havens" href="/motorhome-hire-promotions/maui-winery-havens">
                                        maui Winery Havens</a></li><li><a title="New Zealand Campervan Driving routes" href="/motorhome-holiday-advice/campervan-driving-routes">
                                            Camper driving routes</a></li><li><a title="North Island &amp; South Island Regional Highlights"
                                                href="/motorhome-holiday-advice/regional-highlights">New Zealand highlights</a></li><li>
                                                    <a title="Tourism Radio - Campervan hire New Zealand tour guide" href="/holiday-advice/tourism-radio">
                                                        Mobile Travel App</a></li><li><a title="Tips for driving your campervan in New Zealand"
                                                            href="/motorhome-holiday-advice/campervan-driving-tips">Campervan driving tips</a></li></ul>
                            </div>
                        </div>
                        <div class="img">
                            <div id="ctl00_PlaceHolderMain_ctl06__ControlWrapper_RichImageField" style="display: inline">
                                <span dir=""><a href="/motorhome-holiday-advice">
                                    <img alt="Map of New Zealand" border="0" src="http://www.maui.co.nz/SiteCollectionImages/Homepage/new-zealand-map.jpg"
                                        style="border: 0px solid"></a></span>&nbsp;</div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <h2>
                        &nbsp;camp responsibly&nbsp;</h2>
                    <div class="col-main">
                        <div class="text">
                            <div id="ctl00_PlaceHolderMain_ctl08__ControlWrapper_RichHtmlField" style="display: inline">
                                <ul>
                                    <li><a title="maui Winery Havens" href="/motorhome-hire-promotions/maui-winery-havens">
                                        maui Winery Havens</a></li>
                                    <li><a title="Caring for the environment" href="/why-choose-maui-campervans/environmental-commitment">
                                        Camping Our Way</a></li>
                                    <li><a title="Where to stay" href="/motorhome-holiday-advice/new-zealand-accommodation">
                                        Holiday Parks</a></li>
                                    <li><a title="Where to stay" href="/motorhome-holiday-advice/new-zealand-accommodation">
                                        Motorhome Havens</a></li>
                                    <li><a title="" href="/motorhome-holiday-advice/new-zealand-accommodation">Dept of Conservation
                                        campsites</a></li></ul>
                                <div style="margin-top: 10px; float: right; margin-right: 10px">
                                    <img style="border-bottom: 0px solid; border-left: 0px solid; border-top: 0px solid;
                                        border-right: 0px solid" border="0" alt="Qualmark Enviro Gold Award" src="/SiteCollectionImages/Homepage/Qualmark-logo.jpg">
                                    <img style="border-bottom: 0px solid; border-left: 0px solid; margin-left: 5px; border-top: 0px solid;
                                        border-right: 0px solid" border="0" alt="Qualmark Visitor Transport" src="/SiteCollectionImages/Homepage/Qualmark-visitor-transport.jpg"></div>
                            </div>
                        </div>
                        <div class="img">
                            <div id="ctl00_PlaceHolderMain_ctl09__ControlWrapper_RichImageField" style="display: inline">
                                <span dir=""><a href="/motorhome-holiday-advice/new-zealand-accommodation">
                                    <img alt="Camp Responsibly" border="0" src="http://www.maui.co.nz/SiteCollectionImages/Homepage/camping-our-way.jpg"
                                        style="border: 0px solid"></a></span>&nbsp;</div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="contentMain" class="contentFrame">
                <!-- AddThis Button BEGIN -->
                <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=thlonline"></script>
                <div class="addthis_toolbox addthis_default_style">
                    <a href="http://www.addthis.com/bookmark.php?v=250&amp;pubid=thlonline" class="addthis_button_compact">
                    </a><span class="addthis_separator">|</span> <a class="addthis_button_preferred_1">
                    </a><a class="addthis_button_preferred_2"></a><a class="addthis_button_preferred_3">
                    </a><a class="addthis_button_email"></a>
                    <!--<a class="addthis_button_google_plusone"></a>-->
                </div>
                <!-- AddThis Button END -->
                <h1>
                    &nbsp;car rental &amp; campervan hire NZ - maui New Zealand&nbsp;</h1>
                <div class="section" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                    <div class="contentMainLeft">
                        <p>
                            <p>
                                <strong>maui </strong>is an experienced and trusted <strong><a title="Campervan Hire New Zealand"
                                    href="/motorhome-hire"><strong>campervan hire</strong></a></strong> and <a title="Car rental in New Zealand"
                                        href="/car-rental"><strong>car rental</strong></a> brand in NZ and we have
                                delivered great self-drive holidays for more than 25 years. We are focused on our
                                continual quest for design enhancement and innovation to ensure our travellers,
                                just like you, experience the very best in campervan holidays with our stylish interiors
                                and spacious design right down to our warm personalised service.</p>
                        </p>
                    </div>
                    <div class="contentMainRight">
                        <span class="thumb"></span>
                    </div>
                </div>
                <div class="section" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                    <div class="contentMainLeft">
                        <h2>
                            platinum motorhomes &amp; campervans</h2>
                        <p>
                            <p>
                                If your idea of a real holiday is to have all the comforts of home at your fingertips,
                                whilst having the flexibility to travel wherever the road may take you, then we
                                think a maui campervan hire is the perfect self-drive option for your New Zealand
                                journey. With the freedom and flexibility to explore NZ at your own pace, you can
                                stay in one place for as long as you like. Because you decide the pace, the only
                                schedule you have to work to is your own.</p>
                            <p>
                                Choose from the range of maui campervans that sleeps up to two, four and six people
                                – perfect whether you are travelling as a couple, family or group of friends.</p>
                            <p>
                                If you are looking for ultimate space, style &amp; comfort, then we recommend <a
                                    title="Platinum campervan hire" href="/motorhome-hire/platinum-campervans"><strong>maui</strong>
                                    Platinum campervan series</a> to you. Designed with warm neutral tones, wooden
                                floors, real leather couches, reading spotlights and roomy living areas, we believe
                                they have a feel more akin to an apartment than a traditional motorhome.</p>
                        </p>
                    </div>
                    <div class="contentMainRight">
                        <img alt="maui Platinum River Interior" border="0" src="http://www.maui.co.nz/SiteCollectionImages/Vehicles/Crosslink-River-Interior.jpg"
                            style="border: 0px solid;"><ul>
                                <li><a title="Luxury campervan hire" href="/motorhome-hire/platinum-campervans">Platinum
                                    series campervans</a></li>
                                <li><a title="car rental in New Zealand" href="/car-rental"><strong>maui</strong> car
                                    rental</a></li></ul>
                        <span class="thumb"></span>
                    </div>
                </div>
                <div class="section" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                    <div class="contentMainLeft">
                        <p>
                            <p>
                                Sleeping between two and six people comfortably, the <a title="Ultima campervan NZ"
                                    href="/motorhome-hire/platinum-campervans-ultima">Ultima</a>, <a title="Platinum Beach, 4 berth campervan hire"
                                        href="/motorhome-hire/platinum-campervans-beach">Platinum Beach</a> and
                                <a title="River campervan hire - sleeps 6" href="/motorhome-hire/platinum-campervans-river">
                                    Platinum River campervan</a> each feature a unique layout design, so you can
                                choose the layout and space that works best for you. Built on quality European Mercedes
                                or VW chassis, the Platinum campervan series drive as superbly as they look. Plus
                                they’re automatic transmission, so a pleasure and breeze to drive.</p>
                        </p>
                    </div>
                    <div class="contentMainRight">
                        <img alt="Appartment style kitchen equipment for your campervan" border="0" src="/SiteCollectionImages/Homepage/Crosslink-Apartment.jpg"
                            style="border: 0px solid;"><ul>
                                <li><a title="" href="/why-choose-maui-campervans">Why choose maui?</a></li></ul>
                        <span class="thumb"></span>
                    </div>
                </div>
                <div class="section" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                    <div class="contentMainLeft">
                        <h2>
                            apartment-style kitchen &amp; living equipment</h2>
                        <p>
                            <p>
                                We try to think of everything that will make your holiday easy and convenient. So
                                you’ll find all our motorhomes are stocked with apartment-style kitchen equipment
                                (including holiday essentials such as wine glasses, china crockery and coffee plunger),
                                freshly laundered bedding and a range of other useful items so you can travel the
                                country in our campervans without sacrificing the comforts of home.</p>
                        </p>
                    </div>
                    <div class="contentMainRight">
                        <span class="thumb"></span>
                    </div>
                </div>
                <div class="section" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                    <div class="contentMainLeft">
                        <h2>
                            the personal touch for your campervan hire NZ</h2>
                        <p>
                            <p>
                                When you travel with us, you can also be sure that our service compliments our superb
                                motorhomes. We know it’s the small things that count, from a personal recommendation
                                of the best cafe in town to the convenience of being able to replenish your linen
                                at any of our branches during the course of your holiday. Peace of mind makes a
                                holiday even more relaxing so we also offer 24/7 Customer Care should you need us.
                                We are just a phone call away.
                            </p>
                        </p>
                    </div>
                    <div class="contentMainRight">
                        <img alt="maui Service in the branch and on the road" border="0" src="/SiteCollectionImages/Locations-Holiday/Crosslink-Maui-Service.jpg"
                            style="border: 0px solid;"><ul>
                                <li><a title="" href="/motorhome-holiday-advice/maui-service">Maui service on the road</a></li></ul>
                        <span class="thumb"></span>
                    </div>
                </div>
                <div class="section" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                    <div class="contentMainLeft">
                        <h2>
                            car rental New Zealand</h2>
                        <p>
                            We also have a selection of late model <strong>rental cars</strong> if you are looking
                            to nip away to a hotel or perhaps a friend’s holiday home. All maui <strong>car rentals</strong>
                            are from 0-2 years old and you can be sure you are getting a great quality car for
                            a great price.
                        </p>
                    </div>
                    <div class="contentMainRight">
                        <span class="thumb"></span>
                    </div>
                </div>
                <div class="section" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                    <div class="contentMainLeft">
                        <h2>
                            maui - quality campervans and motorhomes</h2>
                        <p>
                            <p>
                                You may already know that maui is part of Tourism Holdings Limited (thl), a business
                                firmly established as New Zealand’s premier tourism company with operations also
                                in Australia. Maui, has a new vehicle sales brand: <a title="" href="http://www.motekvehicles.com/">
                                    Motek</a>. If you are a Kiwi you may have known them previously as Ci Munro
                                and Maui Vehicle Sales. <a title="" href="http://www.motekvehicles.com/" target="_blank">
                                    Motek</a> manufacturing excellence means you can rest assured that your camper
                                has been designed and built by an experienced company within the thl house of brands.</p>
                            <p>
                                <strong>maui</strong> is also endorsed by the quality assurance programme Qualmark,
                                backing up our commitment to delivering a quality product and experience. We also
                                take the environment seriously and are the first company in the country to have
                                its camperans independently emissions tested. We are also a partner with LOVE NZ
                                to encourage recycling at all of our branches.</p>
                            <p>
                                It would be our pleasure to have you travel with us for your campervan hire New
                                Zealand. Please contact our team, anytime, 24/7.
                            </p>
                        </p>
                    </div>
                    <div class="contentMainRight">
                        <img alt="Ci Munro" border="0" src="http://www.maui.co.nz/SiteCollectionImages/WhyChooseMaui/Crosslink-Quality-Vehicles.jpg"
                            style="border: 0px solid;"><ul>
                                <li><a title="" href="/why-choose-maui-campervans/better-by-design">Better by Design</a></li>
                                <li><a title="" href="/why-choose-maui-campervans/motek">Quality motorhome manufacturing
                                    and maintenance</a></li></ul>
                        <span class="thumb"></span>
                    </div>
                </div>
                <!-- AddThis Button BEGIN -->
                <div class="addthis_toolbox addthis_default_style ">
                    <a href="http://www.addthis.com/bookmark.php?v=250&amp;pubid=thlonline" class="addthis_button_compact">
                        Share</a>
                </div>
                <!-- AddThis Button END -->
            </div>
        </div>
    </div>
            <div class="footer">
                <div id="footerContainer">
                    <div id="footerTop">
                        <div id="custom-reviews">
                            <h2>
                                Customer reviews:</h2>
                            <ul class="cr-list">
                                <li xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                    <div>
                                        We found the vans very well appointed, lots of storage with everything you would
                                        need! Elizabeth Keller
                                    </div>
                                </li>
                                <li xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                    <div>
                                        Your staff in Queenstown were very friendly – Matthew Hamilton</div>
                                </li>
                                <li xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                    <div>
                                        What a lovely, beautifully appointed van. Thank you once again for setting up the
                                        trip - it was very much appreciated. – Kareen</div>
                                </li>
                                <li xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                    <div>
                                        We enjoyed the Tourism Radio!&nbsp; Wasn't even sure what it was to start with and
                                        didn't put it on for a couple of days - once we put it on we were completely HOOKED!
                                        What a FAB gizmo! – Celia Burley</div>
                                </li>
                                <li xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                    <div>
                                        It’s one of the best vacations I’ve ever had, and I’d do it again in a heartbeat
                                        - caroundtheworld.com</div>
                                </li>
                                <li xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                    <div>
                                        I almost cried when we had to return the campervan. I could have stayed in that
                                        thing for a month, roaming the open road and sipping NZ Sauvignon Blanc. It’s one
                                        of the best vacations I’ve ever had, and I’d do it again in a heartbeat. - Chris</div>
                                </li>
                                <li xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                    We promise that next time we will use maui again. My family very enjoy with maui
                                    Platinum River Campervan during our trip in South Island of NZ - Boonchoke Warotamaporn</li><li
                                        xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                        xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                        xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                        We spent recently spent 60 nights in a Maui Ultima traveling both the North and
                                        South Island of NZ. We loved every minute. And the Ultima and the service we received
                                        from Thl was fantastic - Rick+Marilyn Paul</li><li xmlns:x="http://www.w3.org/2001/XMLSchema"
                                            xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" xmlns:asp="http://schemas.microsoft.com/ASPNET/20"
                                            xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                            xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                            <div>
                                                WOW! We have just experienced the fun of a Maui motor home. We had the best weekend
                                                and can't wait for the next holidays to do it all again! - Susanne Thomson</div>
                                        </li>
                            </ul>
                        </div>
                        <ul id="socialMedia">
                            <li id="list-findus">Find us on:</li>
                            <li id="list-youtube"><a target="_blank" title="Check out our YouTube Channel" href="http://www.youtube.com/user/MauiMotorhomes">
                                <span>YouTube</span></a></li>
                            <li id="list-twitter"><a target="_blank" title="Follow us on Twitter" href="http://twitter.com/mauirentals">
                                <span>Twitter</span></a></li>
                            <li id="list-facebook"><a target="_blank" title="Follow us on Facebook" href="http://www.facebook.com/mauimotorhomesfanpage">
                                <span>Facebook</span></a></li>
                        </ul>
                    </div>
                    <div id="footerBottom">
                        <div id="footerBottomLeft">
                            <div class="col">
                                <div class="item">
                                    <h3>
                                        browse our vehicles</h3>
                                    <div class="item-href">
                                        <div class="ExternalClass70E0A49ECE5A45A1A687DD2F39051CA1">
                                            <div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>
                                        <a title="Luxury Motorhome Hire New Zealand" href="/motorhome-hire/platinum-campervans">
                                            Platinum series campervans</a><br>
                                        <a title="Car Hire New Zealand" href="/car-rental">Car rentals</a>
                                    </p>
                                </div>
                                <div class="item">
                                    <h3>
                                        holiday advice</h3>
                                    <div class="item-href">
                                        <div class="ExternalClass50EB2E17C16745948FAA7975E074FA23">
                                            <div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>
                                        <a title="What to pack for your New Zealand campervan journey" href="/motorhome-holiday-advice/what-to-pack">
                                            What to pack</a><br>
                                        <a title="Where to stay in your camper" href="/motorhome-holiday-advice/new-zealand-accommodation">
                                            Where to stay in your camper</a><br>
                                        <a title="" href="/motorhome-holiday-advice/maui-service">Maui service on the road</a><br>
                                        <a title="Meals in your campervan or motorhome" href="/motorhome-holiday-advice/new-zealand-produce">
                                            Meals in your camper</a></p>
                                </div>
                            </div>
                            <div class="col">
                                <div class="item">
                                    <h3>
                                        maui branches</h3>
                                    <div class="item-href">
                                        <div class="ExternalClass0AE114E216AA486FADD8F7A56AA0D432">
                                            <a href="/campervan-hire-locations">branch map</a></div>
                                    </div>
                                    <p>
                                        <a title="Campervan hire pick up Auckland airport" href="/ourlocations/Pages/AucklandAirport.aspx">
                                            Auckland motorhome hire</a><br>
                                        <a title="Campervan hire pick up Christchurch airport" href="/ourlocations/Pages/Christchurch.aspx">
                                            Christchurch motorhome hire</a><br>
                                        <a title="Campervan hire pick up Queenstown airport" href="/ourlocations/Pages/QueenstownAirport.aspx">
                                            Queenstown motorhome hire</a><br>
                                        <a title="Car hire pick up New Zealand" href="/car-hire-locations">New Zealand car hire</a></p>
                                </div>
                            </div>
                            <div class="col">
                                <div class="item">
                                    <h3>
                                        save time and check-in online</h3>
                                    <div class="item-href">
                                        <div class="ExternalClassA170BDD6E3D94138A7F5EBA1EBD147F5">
                                            <div>
                                                <a href="/online-rental-check-in">check-in online</a></div>
                                        </div>
                                    </div>
                                    <p>
                                        If you want to save time at the branch when picking up your motorhome, you have
                                        the option to use our Self Check-in service - just like you would at the airport.
                                        You can choose to use our online Self Check-in at your convenience from any computer
                                        or when you arrive at our branch.</p>
                                </div>
                            </div>
                        </div>
                        <div id="footerBottomRight">
                            <div id="customerService">
                                <h3>
                                    Customer service</h3>
                                <ul class="cs-simple">
                                    <li class="callus">Call us anytime 24/7 International Toll Free</li>
                                    <li>
                                        <script type="text/javascript" src="https://www.google.com/jsapi?key=ABQIAAAAWqQjtvG0Ey1zuy0Yi5ylrRSvB4ANtyecigTcIZwt9YT7B9rUjxTr5Dy-RjXevBpd_FVxDcP65itgeQ"></script>
                                        <script type="text/javascript">
                                            $(document).ready(function () {

                                                if (google.loader.ClientLocation != null) {
                                                    $("#min #countrySelector").val(google.loader.ClientLocation.address.country_code);
                                                    var ddElm = $("#min #countrySelector");
                                                    var trElm = $('#' + ddElm.val() + '_tr');
                                                    var number = $('.fx', trElm).text() + " " + $('.no', trElm).text();
                                                    $('#min  #countryTollFree').text(number);
                                                    if (this.value == '-1')
                                                        $('#min #hideLabel').css('display', 'none');
                                                    else
                                                        $('#min #hideLabel').css('display', 'block');
                                                }
                                                $("#min #countrySelector").change(function () {
                                                    var trElm = $('#' + this.value + '_tr');
                                                    var number = $('.fx', trElm).text() + " " + $('.no', trElm).text();
                                                    $('#min #countryTollFree').text(number);
                                                    if (this.value == '-1')
                                                        $('#min #hideLabel').css('display', 'none');
                                                    else
                                                        $('#min #hideLabel').css('display', 'block');
                                                });
                                            });
                                        </script>
                                        <div id="min" class="intNumbersDiv">
                                            <div id="callSelector">
                                                <span>
                                                    <label for="countrySelector">
                                                        calling from:</label>
                                                    <select id='countrySelector'>
                                                        <option value='-1'>Select your country</option>
                                                        <option value='AR'>Argentina</option>
                                                        <option value='AU'>Australia</option>
                                                        <option value='AT'>Austria</option>
                                                        <option value='BE'>Belgium</option>
                                                        <option value='BR'>Brazil</option>
                                                        <option value='CA'>Canada</option>
                                                        <option value='CN'>China</option>
                                                        <option value='DK'>Denmark</option>
                                                        <option value='FI'>Finland</option>
                                                        <option value='FR'>France</option>
                                                        <option value='DE'>Germany</option>
                                                        <option value='HKP'>Hong Kong PCCW</option>
                                                        <option value='HKW'>Hong Kong Wharf</option>
                                                        <option value='HKH'>Hong Kong Hutchison Telecom</option>
                                                        <option value='HKN'>Hong Kong New World</option>
                                                        <option value='HU'>Hungary</option>
                                                        <option value='IE'>Ireland</option>
                                                        <option value='IT'>Italy</option>
                                                        <option value='JP'>Japan</option>
                                                        <option value='MY'>Malaysia</option>
                                                        <option value='NL'>Netherlands</option>
                                                        <option value='NZ'>New Zealand</option>
                                                        <option value='NO'>Norway</option>
                                                        <option value='PH'>Philippines</option>
                                                        <option value='ZA'>South Africa</option>
                                                        <option value='KP'>South Korea</option>
                                                        <option value='ES'>Spain</option>
                                                        <option value='SE'>Sweden</option>
                                                        <option value='CH'>Switzerland</option>
                                                        <option value='TW'>Taiwan</option>
                                                        <option value='UK'>United Kingdom</option>
                                                        <option value='US'>USA</option>
                                                    </select>
                                                </span><span>
                                                    <label for="countryTollFree" id="hideLabel" style="display: none;">
                                                        dial:</label>
                                                    <big id="countryTollFree"></big></span>
                                            </div>
                                            <div id="toolFreeList">
                                                <span class="hideIntTable">+ show all international tollfree numbers</span>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Country
                                                            </th>
                                                            <th>
                                                                Prefix
                                                            </th>
                                                            <th>
                                                                Number
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr id='AR_tr'>
                                                            <td>
                                                                Argentina
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='AU_tr'>
                                                            <td>
                                                                Australia
                                                            </td>
                                                            <td class='fx'>
                                                                1
                                                            </td>
                                                            <td class='no'>
                                                                300 363 800
                                                            </td>
                                                        </tr>
                                                        <tr id='AT_tr'>
                                                            <td>
                                                                Austria
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='BE_tr'>
                                                            <td>
                                                                Belgium
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='BR_tr'>
                                                            <td>
                                                                Brazil
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='CA_tr'>
                                                            <td>
                                                                Canada
                                                            </td>
                                                            <td class='fx'>
                                                                011
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='CN_tr'>
                                                            <td>
                                                                China
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='DK_tr'>
                                                            <td>
                                                                Denmark
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='FI_tr'>
                                                            <td>
                                                                Finland
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='FR_tr'>
                                                            <td>
                                                                France
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='DE_tr'>
                                                            <td>
                                                                Germany
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='HKP_tr'>
                                                            <td>
                                                                Hong Kong PCCW
                                                            </td>
                                                            <td class='fx'>
                                                                001
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='HKW_tr'>
                                                            <td>
                                                                Hong Kong Wharf
                                                            </td>
                                                            <td class='fx'>
                                                                007
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='HKH_tr'>
                                                            <td>
                                                                Hong Kong Hutchison Telecom
                                                            </td>
                                                            <td class='fx'>
                                                                008
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='HKN_tr'>
                                                            <td>
                                                                Hong Kong New World
                                                            </td>
                                                            <td class='fx'>
                                                                009
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='HU_tr'>
                                                            <td>
                                                                Hungary
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='IE_tr'>
                                                            <td>
                                                                Ireland
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='IT_tr'>
                                                            <td>
                                                                Italy
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='JP_tr'>
                                                            <td>
                                                                Japan
                                                            </td>
                                                            <td class='fx'>
                                                                010
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='MY_tr'>
                                                            <td>
                                                                Malaysia
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='NL_tr'>
                                                            <td>
                                                                Netherlands
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='NZ_tr'>
                                                            <td>
                                                                New Zealand
                                                            </td>
                                                            <td class='fx'>
                                                                0
                                                            </td>
                                                            <td class='no'>
                                                                800 651 080
                                                            </td>
                                                        </tr>
                                                        <tr id='NO_tr'>
                                                            <td>
                                                                Norway
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='PH_tr'>
                                                            <td>
                                                                Philippines
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='ZA_tr'>
                                                            <td>
                                                                South Africa
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='KP_tr'>
                                                            <td>
                                                                South Korea
                                                            </td>
                                                            <td class='fx'>
                                                                001
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='ES_tr'>
                                                            <td>
                                                                Spain
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='SE_tr'>
                                                            <td>
                                                                Sweden
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='CH_tr'>
                                                            <td>
                                                                Switzerland
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='TW_tr'>
                                                            <td>
                                                                Taiwan
                                                            </td>
                                                            <td class='fx'>
                                                                002
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='UK_tr'>
                                                            <td>
                                                                United Kingdom
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='US_tr'>
                                                            <td>
                                                                USA
                                                            </td>
                                                            <td class='fx'>
                                                                011
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div id="search">
                                <h3>
                                    Search the site</h3>
                                <div id="searchField">
                                    <div id="ctl00_SearchBox1" __markuptype="vsattributemarkup" __webpartid="{43a3e458-0a84-4f0e-96c9-b05a4b9182ec}"
                                        webpart="true">
                                        <script language="javascript" type="text/javascript">                                            var __nonMSDOMBrowser = (window.navigator.appName.toLowerCase().indexOf('explorer') == -1); function WebForm_FireDefaultButton(event, target) { if (event.keyCode == 13 && !(event.srcElement && (event.srcElement.tagName.toLowerCase() == "textarea"))) { var defaultButton; if (__nonMSDOMBrowser) { defaultButton = document.getElementById(target); } else { defaultButton = document.all[target]; } if (defaultButton && typeof (defaultButton.click) != "undefined") { defaultButton.click(); event.cancelBubble = true; if (event.stopPropagation) event.stopPropagation(); return false; } } return true; }</script>
                                        <input name="ctl00$SearchBox1$txtSearchTerms" type="text" maxlength="255" id="ctl00_SearchBox1_txtSearchTerms"
                                            title="Enter search words" class="SearchTerm" name="txtSearchTerms" onclick="document.forms[0].onkeypress = new Function(&quot;return WebForm_FireDefaultButton(event, 'ctl00_SearchBox1_SearchButton');&quot;);"
                                            onfocus="document.forms[0].onkeypress = new Function(&quot;return WebForm_FireDefaultButton(event, 'ctl00_SearchBox1_SearchButton');&quot;);" /><input
                                                type="image" name="ctl00$SearchBox1$SearchButton" id="ctl00_SearchBox1_SearchButton"
                                                title="Search website" class="SearchButton" src="http://www.maui.co.nz/SiteCollectionImages/assets/gobutton.png"
                                                style="border-width: 0px;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="footerLinks">
                    <div id="footerLinksTop">
                        <ul>
                            <!--<li><a href="#" title="Sign up for news">Sign up for news</a></li>-->
                            <li><a href="http://www.maui.com.au" title="maui Australia">visit <strong>maui</strong>
                                australia</a></li>
                            <li><a href="/motorhome-hire/faqs" title="FAQs">FAQs</a></li>
                            <li><a href="/motorhome-hire/buy-a-motorhome" title="Buy a maui motorhome">buy a <strong>
                                maui</strong> motorhome</a></li>
                            <!--<li><a href="#" title="Booking Process">Booking Process</a></li>-->
                            <!--<li><a href="#" title="Qualmark">Qualmark</a></li>-->
                            <li><a href="/Pages/Sitemap.aspx" title="Sitemap"><strong>maui</strong> new zealand
                                sitemap</a></li>
                        </ul>
                    </div>
                    <div id="footerLinksBottom">
                        <ul id="footerLinksLogos">
                            <li id="logos"><a href="http://www.thlonline.com" title="Tourism Holding Ltd" id="THLLogo">
                                THL</a> <a href="http://www.roadbearrv.com" title="Road Bear RV USA" id="roadbearlogo">
                                    Road Bear RV USA</a> <a href="http://www.maui-rentals.com" title="maui motorhomes & car rental"
                                        id="MauiLogo">Maui</a> <a href="http://www.britz.com" title="Britz" id="BritzLogo">Britz</a>
                                <a href="http://www.mightycampers.com" title="Mighty campervan & car hire" id="mightyLogo">
                                    Mighty Campers</a> <a href="http://www.motekvehicles.com" title="Motek Vehicles"
                                        id="moteklogo">Motek Vehicles</a> </li>
                            <li>
                                <ul>
                                    <li>You are viewing www.maui.co.nz. See all <a href="http://www.thlonline.com" title="thl brands">
                                        thl brands</a></li>
                                </ul>
                                <ul>
                                    <li><a href="/Pages/Privacystatement.aspx" title="Privacy" class="minor">Privacy Policy</a></li>
                                    <li><a href="/legal" title="Terms and Conditions" class="minor">Website Terms and Conditions</a></li>
                                    <li>&#169; <span class="footer-year"></span>
                                        <script>                                            var currentTime = new Date(); jQuery('.footer-year').html(currentTime.getFullYear());</script>
                                        <strong><i>&nbsp;thl </i></strong></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="popupFooter">
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
    <div>
        <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEWAwL7mu31AwLXgr2SDgKAy4/eD0FO+77tmT2JzmDDPq+fsvwmpCL9" />
    </div>
    <script type="text/javascript">
//<![CDATA[

        WebForm_InitCallback(); var __wpmExportWarning = 'This Web Part Page has been personalized. As a result, one or more Web Part properties may contain confidential information. Make sure the properties contain information that is safe for others to read. After exporting this Web Part, view properties in the Web Part description file (.WebPart) by using a text editor such as Microsoft Notepad.'; var __wpmCloseProviderWarning = 'You are about to close this Web Part.  It is currently providing data to other Web Parts, and these connections will be deleted if this Web Part is closed.  To close this Web Part, click OK.  To keep this Web Part, click Cancel.'; var __wpmDeleteWarning = 'You are about to permanently delete this Web Part.  Are you sure you want to do this?  To delete this Web Part, click OK.  To keep this Web Part, click Cancel.';//]]>
    </script>
    <script type="text/javascript" language="javascript" src="/_layouts/1033/core.js?rev=CNBZRdV1h3pKuA7LsMXf3w%3D%3D"></script>
    </form>
    <!-- BEGIN: Marin Software Tracking Script -->
    <script type="text/javascript">        var _mTrack = _mTrack || []; _mTrack.push(['trackPage']); (function () { var mClientId = '1624rrj16003'; var mProto = ('https:' == document.location.protocol ? 'https://' : 'http://'); var mHost = 'tracker.marinsm.com'; var mt = document.createElement('script'); mt.type = 'text/javascript'; mt.async = true; mt.src = mProto + mHost + '/tracker/async/' + mClientId + '.js'; var fscr = document.getElementsByTagName('script')[0]; fscr.parentNode.insertBefore(mt, fscr); })(); </script>
    <noscript>
        <img src="https://tracker.marinsm.com/tp?act=1&cid=1624rrj16003&script=no">
    </noscript>
    <!-- END: Copyright Marin Software -->
    <!-- Bold Software Visitor Monitor HTML v4.00 (Website=Maui NZ,ChatButton=Default Floating Chat Button,ChatInvitation=Maui Invite Ruleset) -->
    <script type="text/javascript">
        var _bcvma = _bcvma || [];
        _bcvma.push(["setAccountID", "308737748753154415"]);
        _bcvma.push(["setParameter", "WebsiteDefID", "2199042831358885081"]);
        _bcvma.push(["setParameter", "InvitationDefID", "3553932244050431"]);
        _bcvma.push(["setParameter", "VisitName", ""]);
        _bcvma.push(["setParameter", "VisitPhone", ""]);
        _bcvma.push(["setParameter", "VisitEmail", ""]);
        _bcvma.push(["setParameter", "VisitRef", ""]);
        _bcvma.push(["setParameter", "VisitInfo", "CHATmauinz"]);
        _bcvma.push(["setParameter", "CustomUrl", ""]);
        _bcvma.push(["setParameter", "WindowParameters", ""]);
        _bcvma.push(["addFloat", { type: "chat", id: "4230793468628211388"}]);
        _bcvma.push(["pageViewed"]);
        (function () {
            var vms = document.createElement("script"); vms.type = "text/javascript"; vms.async = true;
            vms.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + "vmss.boldchat.com/aid/308737748753154415/bc.vms4/vms.js";
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(vms, s);
        })();
    </script>
    <noscript>
        <a href="http://www.boldchat.com" title="Visitor Monitoring" target="_blank">
            <img alt="Visitor Monitoring" src="https://vms.boldchat.com/aid/308737748753154415/bc.vmi?wdid=2199042831358885081&amp;vr=&amp;vi=&amp;vn=&amp;vp=&amp;ve=&amp;curl="
                border="0" width="1" height="1" /></a>
    </noscript>
    <!-- /Bold Software Visitor Monitor HTML v4.00 -->
    <!-- ClickTale Bottom part -->
    <div id="ClickTaleDiv" style="display: none;">
    </div>
    <script type='text/javascript'>
        document.write(unescape("%3Cscript%20src='" +
 (document.location.protocol == 'https:' ?
  'https://clicktale.pantherssl.com/' :
  'http://s.clicktale.net/') +
 "WRb6.js'%20type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        var ClickTaleSSL = 1;
        ClickTaleCookieDomain = "maui.co.nz";
        if (typeof ClickTale == 'function') ClickTale(6299, 0.0, "www07");
    </script>
    <!-- ClickTale end of Bottom part -->
    <!-- Google Code for Remarketing tag -->
    <script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 973575663;
var google_conversion_label = "wR9_CInn4wQQ76ue0AM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
    </script>
    <script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display: inline;">
            <img height="1" width="1" style="border-style: none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/973575663/?value=0&amp;label=wR9_CInn4wQQ76ue0AM&amp;guid=ON&amp;script=0" />
        </div>
    </noscript>
</body>
</html>
