﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="JSFunctionTests.aspx.cs" Inherits="UnitTests_JSFunctionTests" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>JS Function Tests</title>
    <script type="text/javascript" src="<%= ContentURL %>/js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>/js/selection.js"></script> 
     
    <script type="text/javascript">
        var $j = jQuery.noConflict();
        jQuery(document).ready(function() {
            //initConfigurePage(); 
        });

        function testcurrency() {
            var rawValue = $j('#inputTag').val();
            var formatedValue = CurrencyFormatted(rawValue, "NZD", 12);
            $j('#resuleElm').html(formatedValue);
        }    
    </script>     
</head>
<body>
    <form id="form1" runat="server">
    <div>
        value:
        <input id="inputTag" type="text" size="15" />
        <span onclick="javascript:testcurrency();">test </span>
        <span id="resuleElm" style="display:block;float:left;"></span>
    </div>
    </form>
</body>
</html>
