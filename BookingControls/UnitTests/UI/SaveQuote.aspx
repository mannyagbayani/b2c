﻿<%@ Page Language="C#" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>    
    <link href="http://images.thl.com/css/Base.css" type="text/css" rel="stylesheet" />     
    <link href="http://images.thl.com/css/p/selection.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" media="all" href="http://images.thl.com/css/p/horizontal.css" />
    <script type="text/javascript" src="http://local.backpackercampervans.co.nz/bookingcontrols/js/jquery-1.3.1.min.js"></script>    
    <script type="text/javascript" src="http://local.backpackercampervans.co.nz/bookingcontrols/js/jquery.dimensions.min.js"></script>
    <script type="text/javascript" src="http://local.backpackercampervans.co.nz/bookingcontrols/js/jquery.tooltip.min.js"></script>
    <script src="http://local.backpackercampervans.co.nz/bookingcontrols/js/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://images.thl.com/css/jquery.ui.dialog.css" rel="stylesheet" type="text/css"/>
    <link href="http://images.thl.com/css/p/jcal.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <ul>
            <li>
                <p style="margin-top:100px;">
                    <a href="#"  class="popUp" title="Save Quote" >Save Quote</a>
                </p>
            </li>
            <li>
                <p style="margin-top:100px;">
                    <a href="#"  class="popUp" title="Save Quote" >Save Quote</a>
                </p>
            </li>
            <li>
                <p style="margin-top:100px;">
                    <a href="#"  class="popUp" title="Save Quote" >Save Quote</a>
                </p>
            </li>
            <li>
                <p style="margin-top:100px;">
                    <a href="#"  class="popUp" title="Save Quote" >Save Quote</a>
                </p>
            </li>
        </ul>
    
    
    
    </div>
    </form>
    <!-- save Quote Starts -->
    <script type="text/javascript">
        var $j = jQuery.noConflict();
        jQuery(document).ready(function() {
        //--save quote
        $j("a.popUp").click(function(e) {
            e.stopPropagation();
            e.preventDefault();
            $j("#saveQuoteDiv").dialog({ draggable: false, width: 420, height: 240, resizable: false });
            $j('#saveQuoteDiv').dialog('open');
            var myDialogX = $j(this).position().left - $j(this).outerWidth();
            var myDialogY = $j(this).position().top - ($j(document).scrollTop() + $j('#saveQuoteDiv').outerHeight()) - 65;
            $j('#saveQuoteDiv').dialog({ position: [myDialogX, myDialogY], dialogClass: 'saveQuote' });
            $j(this).addClass('selected');
            //$j("#ui-datepicker-div").addClass("saveQuote");        
        });
        //--save quote ends           
        });
    </script>

    <div id="saveQuoteDiv" title="Save This Quote" style="display:none;">
        <form id="quoteForm">            
            <span>Saved quotes will be valid for one week from today.</span>
            <ul>
                <li>
                    <p>
                        <label for="quoteFName">First Name</label> 
                        <input id="quoteFName" name="quoteFName" class="required"/>                       
                    </p>
                </li>
                <li>
                    <p>
                        <label for="quoteLName">Last Name</label>
                        <input id="quoteLNamet" name="quoteLName" class="required"/>
                    </p>
                </li>
                <li>
                    <p>
                        <label for="quoteEmail">Email</label>
                        <input id="quoteEmail" name="quoteEmail" class="required"/>
                    </p>
                </li>
                <li>
                    <p>
                        <label for="quoteCEmail">Confirm Email</label>
                        <input id="quoteCEmail" name="quoteCEmail" class="required"/>
                    </p>
                </li>
                <li>
                    <p>
                        <label for="quotePhone">Phone Number</label>
                        <input id="quotePhone" name="quotePhone"/>
                    </p>
                </li>
                <li>
                    <p class="submitPanel">
                        <input type="submit" value="Submit Details"/>
                    </p>
                </li>         
            </ul>           
        </form>    
    </div>   
    <!-- save Quote Ends -->
</body>
</html>
