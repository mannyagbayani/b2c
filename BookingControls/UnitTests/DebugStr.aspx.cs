﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UnitTests_DebugStr : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string xmlBody = ((string)Session["debugStr"]);
        if (xmlBody != null && xmlBody.Length > 50)
        {
            xmlBody = xmlBody.Substring(38, xmlBody.Length - 38);
            string xml = "<AuroraRequest><url><![CDATA[" + ((string)Session["requestURL"]) + "]]></url><response>" + xmlBody + "</response></AuroraRequest>";
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.Write(xml);
            Response.End();
        }
    }
}