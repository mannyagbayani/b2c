﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="KeaNZ.aspx.cs" Inherits="UnitTests_KeaNZ" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>2 Berth Shower & Toilet Campervan New Zealand </title>
    <script language="javascript" src="http://nzrentals.keacampers.com/scripts/jquery-1.4.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="http://nzrentals.keacampers.com/scripts/jquery-ui-1.8.7.custom.min.js"></script>
    <script type="text/javascript" src="http://nzrentals.keacampers.com/scripts/jquery-ui-1.8.6.custom.min.js"></script>
    <script type="text/javascript" src="http://nzrentals.keacampers.com/scripts/equalheights.js"></script>
    <script type="text/javascript" src="http://nzrentals.keacampers.com/scripts/jquery.sexyslider.min.js"></script>
    <script type="text/javascript" src="http://nzrentals.keacampers.com/scripts/tabs-script.js"></script>
    <script type="text/javascript" src="js/generalstuff_keanz.js"></script>
    <script src="http://nzrentals.keacampers.com/scripts/language.js" type="text/javascript"></script>
    <link href="http://nzrentals.keacampers.com/App_Themes/KEA%20-%20NEW/jquery-ui-1.8.6.custom.css" type="text/css"
        rel="stylesheet" />
    <link href="http://nzrentals.keacampers.com/App_Themes/KEA%20-%20NEW/main.css" type="text/css" rel="stylesheet" />
    <meta name="Description" content="This luxury self-contained 2-berth campervan has all the features of a full sized motorhome. Comfortably sleeping 2, it has shower and toilet, microwave and TV/DVD.
" />
    <meta name="Keywords" content="campervan, shower, toilet, 2 berth, self, contained, couples" />
    <link href="http://nzrentals.keacampers.com/Sitefinity/ControlTemplates/Search/searchCommonLayout.css" type="text/css"
        rel="stylesheet" media="screen"></link>
    <link href="http://nzrentals.keacampers.com/WebResource.axd?d=6lr4w78tSOURaIU-ywPi57-T6WJearxjQpvOPzDUScUSjgTaUBxf9gYoAy0J3v7uBX5OXAcbPCiOiGiJ2C0oRqBqarEpNl6bRLCdc_56hdYv87Mehys5hmRcxBivRPcc5Rh-Ww2&amp;t=634039947500000000"
        type="text/css" rel="stylesheet" class="Telerik_stylesheet" />
    <link href="http://nzrentals.keacampers.com/WebResource.axd?d=PM4Wbt9uhLY4ZNTJRb60U0zJJuYJ3T8m7KcKxbsSd8b03xrOW2GK3FpanKzX04Ks0P2ZYlAPFw9Stpcuma3rb3m7h5H2kkpwbg6xoYZ6SuCZlMpgM60Do5fMptwEbkC9_RtgYKzHZmqw8GF4B6Yd0VVoUE8sejQrjH-I6xf6osjepuh_0&amp;t=634039116320000000"
        type="text/css" rel="stylesheet" media="screen"></link>
    <meta name="Generator" content="Sitefinity 3.7.2096.220:1" />
</head>
<body>
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-15153186-6']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <form name="aspnetForm" method="post" action="2-berth-campervan.aspx" id="aspnetForm">
    <div>
        <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
        <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUENTM4MQ9kFgJmD2QWAgIEEGRkFgQCBQ9kFgJmD2QWAgICDxQrAAIUKwACDxYEHgtfIURhdGFCb3VuZGceF0VuYWJsZUFqYXhTa2luUmVuZGVyaW5naGQPFCsABhQrAAIPFgweBFRleHQFCUFib3V0IEtFQR4LTmF2aWdhdGVVcmwFE34vZW4vYWJvdXQta2VhLmFzcHgeBVZhbHVlBQlBYm91dCBLRUEeB1Rvb2xUaXBlHghDc3NDbGFzc2UeBF8hU0ICAmQQFgVmAgECAgIDAgQWBRQrAAIPFgwfAgUPV2h5IGNob29zZSBLRUE/HwMFIn4vZW4vYWJvdXQta2VhL3doeS1jaG9vc2Uta2VhLmFzcHgfBAUPV2h5IGNob29zZSBLRUE/HwVlHwZlHwcCAmRkFCsAAg8WDB8CBQ1UaGUgS0VBIHN0b3J5HwMFIX4vZW4vYWJvdXQta2VhL3RoZS1rZWEtc3RvcnkuYXNweB8EBQ1UaGUgS0VBIHN0b3J5HwVlHwZlHwcCAmRkFCsAAg8WDB8CBQlLRUEgR3JlZW4fAwUffi9lbi9hYm91dC1rZWEvZW52aXJvbm1lbnQuYXNweB8EBQlLRUEgR3JlZW4fBWUfBmUfBwICZGQUKwACDxYMHwIFBE5ld3MfAwUYfi9lbi9hYm91dC1rZWEvbmV3cy5hc3B4HwQFBE5ld3MfBWUfBmUfBwICZGQUKwACDxYMHwIFDUhhcHB5IGNhbXBlcnMfAwUhfi9lbi9hYm91dC1rZWEvaGFwcHktY2FtcGVycy5hc3B4HwQFDUhhcHB5IGNhbXBlcnMfBWUfBmUfBwICZBAWAWYWARQrAAIPFgwfAgUVQWR2ZW50dXJlIGluIHRoZSBzbm93HwMFN34vZW4vYWJvdXQta2VhL2hhcHB5LWNhbXBlcnMvYWR2ZW50dXJlLWluLXRoZS1zbm93LmFzcHgfBAUVQWR2ZW50dXJlIGluIHRoZSBzbm93HwVlHwZlHwcCAmRkDxYBZhYBBXNUZWxlcmlrLldlYi5VSS5SYWRNZW51SXRlbSwgVGVsZXJpay5XZWIuVUksIFZlcnNpb249MjAxMC4xLjMwOS4yMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj0xMjFmYWU3ODE2NWJhM2Q0DxYFZmZmZmYWAQVzVGVsZXJpay5XZWIuVUkuUmFkTWVudUl0ZW0sIFRlbGVyaWsuV2ViLlVJLCBWZXJzaW9uPTIwMTAuMS4zMDkuMjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49MTIxZmFlNzgxNjViYTNkNBQrAAIPFgwfAgUIVmVoaWNsZXMfAwUbfi9lbi92ZWhpY2xlcy92ZWhpY2xlcy5hc3B4HwQFCFZlaGljbGVzHwVlHwYFCXJtRm9jdXNlZB8HAgJkEBYJZgIBAgICAwIEAgUCBgIHAggWCRQrAAIPFgwfAgUIVmVoaWNsZXMfAwUbfi9lbi92ZWhpY2xlcy92ZWhpY2xlcy5hc3B4HwQFCFZlaGljbGVzHwVlHwZlHwcCAmRkFCsAAg8WDB8CBRQyICsgMiBCZXJ0aCBGbGlwLVRvcB8DBSp+L2VuL3ZlaGljbGVzLzItcGx1cy0yLWJlcnRoLWZsaXAtdG9wLmFzcHgfBAUUMiArIDIgQmVydGggRmxpcC1Ub3AfBWUfBmUfBwICZGQUKwACDxYMHwIFETIgQmVydGggQ2FtcGVydmFuHwMFJH4vZW4vdmVoaWNsZXMvMi1iZXJ0aC1jYW1wZXJ2YW4uYXNweB8EBREyIEJlcnRoIENhbXBlcnZhbh8FZR8GBQlybUZvY3VzZWQfBwICZGQUKwACDxYIHwIFDjMgQmVydGggT3JpZ2luHwMFGX4vZW4vdmVoaWNsZXMvb3JpZ2luLmFzcHgfBAUOMyBCZXJ0aCBPcmlnaW4fBWVkZBQrAAIPFggfAgURNCBCZXJ0aCBNb3RvcmhvbWUfAwUkfi9lbi92ZWhpY2xlcy80LWJlcnRoLW1vdG9yaG9tZS5hc3B4HwQFETQgQmVydGggTW90b3Job21lHwVlZGQUKwACDxYIHwIFETYgQmVydGggTW90b3Job21lHwMFJH4vZW4vdmVoaWNsZXMvNi1iZXJ0aC1tb3RvcmhvbWUuYXNweB8EBRE2IEJlcnRoIE1vdG9yaG9tZR8FZWRkFCsAAg8WCB8CBRBMaXZpbmcgZXF1aXBtZW50HwMFI34vZW4vdmVoaWNsZXMvbGl2aW5nLWVxdWlwbWVudC5hc3B4HwQFEExpdmluZyBlcXVpcG1lbnQfBWVkZBQrAAIPFggfAgUJQnV5IGEgS0VBHwMFHWh0dHA6Ly9uenNhbGVzLmtlYWNhbXBlcnMuY29tHwQFCUJ1eSBhIEtFQR8FZWRkFCsAAg8WCB8CBRNNYWRlIGluIE5ldyBaZWFsYW5kHwMFHX4vZW4vdmVoaWNsZXMvbWFkZS1pbi1uei5hc3B4HwQFE01hZGUgaW4gTmV3IFplYWxhbmQfBWVkZA8WCWZmZmZmZmZmZhYBBXNUZWxlcmlrLldlYi5VSS5SYWRNZW51SXRlbSwgVGVsZXJpay5XZWIuVUksIFZlcnNpb249MjAxMC4xLjMwOS4yMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj0xMjFmYWU3ODE2NWJhM2Q0FCsAAg8WCB8CBQxLRUEgc3BlY2lhbHMfAwUWfi9lbi9LRUFfc3BlY2lhbHMuYXNweB8EBQxLRUEgc3BlY2lhbHMfBWVkEBYBZhYBFCsAAg8WCB8CBRFQaG90byBjb21wZXRpdGlvbh8DBSh+L2VuL0tFQV9zcGVjaWFscy9waG90by1jb21wZXRpdGlvbi5hc3B4HwQFEVBob3RvIGNvbXBldGl0aW9uHwVlZGQPFgFmFgEFc1RlbGVyaWsuV2ViLlVJLlJhZE1lbnVJdGVtLCBUZWxlcmlrLldlYi5VSSwgVmVyc2lvbj0yMDEwLjEuMzA5LjIwLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPTEyMWZhZTc4MTY1YmEzZDQUKwACDxYIHwIFDlBsYW4gWW91ciBUcmlwHwMFGH4vZW4vcGxhbi15b3VyLXRyaXAuYXNweB8EBQ5QbGFuIFlvdXIgVHJpcB8FZWQQFgZmAgECAgIDAgQCBRYGFCsAAg8WCB8CBRFBYm91dCBOZXcgWmVhbGFuZB8DBSp+L2VuL3BsYW4teW91ci10cmlwL2Fib3V0LW5ldy16ZWFsYW5kLmFzcHgfBAURQWJvdXQgTmV3IFplYWxhbmQfBWVkZBQrAAIPFggfAgURV2hlcmUgY2FuIEkgY2FtcD8fAwUofi9lbi9wbGFuLXlvdXItdHJpcC9jYW1waW5nLWdyb3VuZHMuYXNweB8EBRFXaGVyZSBjYW4gSSBjYW1wPx8FZWRkFCsAAg8WCB8CBQtJdGluZXJhcmllcx8DBSR+L2VuL3BsYW4teW91ci10cmlwL2l0aW5lcmFyaWVzLmFzcHgfBAULSXRpbmVyYXJpZXMfBWVkEBYCZgIBFgIUKwACDxYIHwIFGTggRGF5IE5vcnRobGFuZCBJdGluZXJhcnkfAwU+fi9lbi9wbGFuLXlvdXItdHJpcC9pdGluZXJhcmllcy84LWRheS1ub3J0aGxhbmQtaXRpbmVyYXJ5LmFzcHgfBAUZOCBEYXkgTm9ydGhsYW5kIEl0aW5lcmFyeR8FZWRkFCsAAg8WCB8CBR0xMSBEYXkgU291dGggSXNsYW5kIEl0aW5lcmFyeR8DBUJ+L2VuL3BsYW4teW91ci10cmlwL2l0aW5lcmFyaWVzLzExLWRheS1zb3V0aC1pc2xhbmQtaXRpbmVyYXJ5LmFzcHgfBAUdMTEgRGF5IFNvdXRoIElzbGFuZCBJdGluZXJhcnkfBWVkZA8WAmZmFgEFc1RlbGVyaWsuV2ViLlVJLlJhZE1lbnVJdGVtLCBUZWxlcmlrLldlYi5VSSwgVmVyc2lvbj0yMDEwLjEuMzA5LjIwLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPTEyMWZhZTc4MTY1YmEzZDQUKwACDxYIHwIFFFRyYXZlbGxpbmcgd2l0aCBraWRzHwMFLX4vZW4vcGxhbi15b3VyLXRyaXAvdHJhdmVsbGluZy13aXRoLWtpZHMuYXNweB8EBRRUcmF2ZWxsaW5nIHdpdGgga2lkcx8FZWQQFgFmFgEUKwACDxYIHwIFDEtpZHMnIGNvcm5lch8DBTl+L2VuL3BsYW4teW91ci10cmlwL3RyYXZlbGxpbmctd2l0aC1raWRzL2tpZHMtY29ybmVyLmFzcHgfBAUMS2lkcycgY29ybmVyHwVlZGQPFgFmFgEFc1RlbGVyaWsuV2ViLlVJLlJhZE1lbnVJdGVtLCBUZWxlcmlrLldlYi5VSSwgVmVyc2lvbj0yMDEwLjEuMzA5LjIwLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPTEyMWZhZTc4MTY1YmEzZDQUKwACDxYIHwIFC1RyYXZlbCBpbmZvHwMFJH4vZW4vcGxhbi15b3VyLXRyaXAvVHJhdmVsX2luZm8uYXNweB8EBQtUcmF2ZWwgaW5mbx8FZWQQFgZmAgECAgIDAgQCBRYGFCsAAg8WCB8CBQxBcnJpdmFsIGluZm8fAwU4fi9lbi9wbGFuLXlvdXItdHJpcC9UcmF2ZWxfaW5mby9hcnJpdmFsLWluZm9ybWF0aW9uLmFzcHgfBAUMQXJyaXZhbCBpbmZvHwVlZGQUKwACDxYIHwIFDERyaXZpbmcgaW5mbx8DBTF+L2VuL3BsYW4teW91ci10cmlwL1RyYXZlbF9pbmZvL2RyaXZpbmctaW5mby5hc3B4HwQFDERyaXZpbmcgaW5mbx8FZWRkFCsAAg8WCB8CBQpGZXJyeSBpbmZvHwMFNn4vZW4vcGxhbi15b3VyLXRyaXAvVHJhdmVsX2luZm8vZmVycnktaW5mb3JtYXRpb24uYXNweB8EBQpGZXJyeSBpbmZvHwVlZGQUKwACDxYIHwIFCURpc3RhbmNlcx8DBS5+L2VuL3BsYW4teW91ci10cmlwL1RyYXZlbF9pbmZvL2Rpc3RhbmNlcy5hc3B4HwQFCURpc3RhbmNlcx8FZWRkFCsAAg8WCB8CBQ1XaGF0IHRvIGJyaW5nHwMFMn4vZW4vcGxhbi15b3VyLXRyaXAvVHJhdmVsX2luZm8vd2hhdC10by1icmluZy5hc3B4HwQFDVdoYXQgdG8gYnJpbmcfBWVkZBQrAAIPFggfAgUMS2VlcGluZyBzYWZlHwMFMX4vZW4vcGxhbi15b3VyLXRyaXAvVHJhdmVsX2luZm8va2VlcGluZy1zYWZlLmFzcHgfBAUMS2VlcGluZyBzYWZlHwVlZGQPFgZmZmZmZmYWAQVzVGVsZXJpay5XZWIuVUkuUmFkTWVudUl0ZW0sIFRlbGVyaWsuV2ViLlVJLCBWZXJzaW9uPTIwMTAuMS4zMDkuMjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49MTIxZmFlNzgxNjViYTNkNBQrAAIPFggfAgUJS0VBIE5lc3RzHwMFIn4vZW4vcGxhbi15b3VyLXRyaXAvS0VBX05lc3RzLmFzcHgfBAUJS0VBIE5lc3RzHwVlZGQPFgZmZmZmZmYWAQVzVGVsZXJpay5XZWIuVUkuUmFkTWVudUl0ZW0sIFRlbGVyaWsuV2ViLlVJLCBWZXJzaW9uPTIwMTAuMS4zMDkuMjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49MTIxZmFlNzgxNjViYTNkNBQrAAIPFggfAgULUmVudGFsIEluZm8fAwUVfi9lbi9yZW50YWwtaW5mby5hc3B4HwQFC1JlbnRhbCBJbmZvHwVlZBAWBWYCAQICAgMCBBYFFCsAAg8WCB8CBRFJbnN1cmFuY2Ugb3B0aW9ucx8DBSd+L2VuL3JlbnRhbC1pbmZvL2luc3VyYW5jZV9vcHRpb25zLmFzcHgfBAURSW5zdXJhbmNlIG9wdGlvbnMfBWVkZBQrAAIPFggfAgUTT25saW5lIHByZSBjaGVjay1pbh8DBWlodHRwczovL3NlY3VyZS50cGwtc2VydmljZXMuY29tL21vZHVsZXMvcHJlY2hlY2tpbi9sb2dpbi5hc3B4P3NpdGVJRD1iOGU3Yjc5Ny03NTQzLTQ5MDEtODU4Yy1jOGUyOWQxNjY1MDQfBAUTT25saW5lIHByZSBjaGVjay1pbh8FZWRkFCsAAg8WCB8CBQ9PcHRpb25hbCBleHRyYXMfAwUlfi9lbi9yZW50YWwtaW5mby9vcHRpb25hbC1leHRyYXMuYXNweB8EBQ9PcHRpb25hbCBleHRyYXMfBWVkZBQrAAIPFggfAgUDRkFRHwMFGX4vZW4vcmVudGFsLWluZm8vZmFxLmFzcHgfBAUDRkFRHwVlZGQUKwACDxYIHwIFElRlcm1zICYgY29uZGl0aW9ucx8DBSp+L2VuL3JlbnRhbC1pbmZvL3Rlcm1zLWFuZC1jb25kaXRpb25zLmFzcHgfBAUSVGVybXMgJiBjb25kaXRpb25zHwVlZGQPFgVmZmZmZhYBBXNUZWxlcmlrLldlYi5VSS5SYWRNZW51SXRlbSwgVGVsZXJpay5XZWIuVUksIFZlcnNpb249MjAxMC4xLjMwOS4yMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj0xMjFmYWU3ODE2NWJhM2Q0FCsAAg8WCB8CBQdDb250YWN0HwMFEX4vZW4vY29udGFjdC5hc3B4HwQFB0NvbnRhY3QfBWVkEBYEZgIBAgICAxYEFCsAAg8WCB8CBQpDb250YWN0IHVzHwMFHH4vZW4vY29udGFjdC9jb250YWN0X3VzLmFzcHgfBAUKQ29udGFjdCB1cx8FZWRkFCsAAg8WCB8CBQ5BdWNrbGFuZCBkZXBvdB8DBSB+L2VuL2NvbnRhY3QvYXVja2xhbmQtZGVwb3QuYXNweB8EBQ5BdWNrbGFuZCBkZXBvdB8FZWRkFCsAAg8WCB8CBRJDaHJpc3RjaHVyY2ggZGVwb3QfAwUkfi9lbi9jb250YWN0L2NocmlzdGNodXJjaC1kZXBvdC5hc3B4HwQFEkNocmlzdGNodXJjaCBkZXBvdB8FZWRkFCsAAg8WCB8CBQ1LRUEgd29ybGR3aWRlHwMFH34vZW4vY29udGFjdC9LRUFfd29ybGR3aWRlLmFzcHgfBAUNS0VBIHdvcmxkd2lkZR8FZWRkDxYEZmZmZhYBBXNUZWxlcmlrLldlYi5VSS5SYWRNZW51SXRlbSwgVGVsZXJpay5XZWIuVUksIFZlcnNpb249MjAxMC4xLjMwOS4yMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj0xMjFmYWU3ODE2NWJhM2Q0DxQrAQZmZmZmZmYWAQVzVGVsZXJpay5XZWIuVUkuUmFkTWVudUl0ZW0sIFRlbGVyaWsuV2ViLlVJLCBWZXJzaW9uPTIwMTAuMS4zMDkuMjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49MTIxZmFlNzgxNjViYTNkNGQWDGYPDxYMHwIFCUFib3V0IEtFQR8DBRN+L2VuL2Fib3V0LWtlYS5hc3B4HwQFCUFib3V0IEtFQR8FZR8GZR8HAgJkFgpmDw8WDB8CBQ9XaHkgY2hvb3NlIEtFQT8fAwUifi9lbi9hYm91dC1rZWEvd2h5LWNob29zZS1rZWEuYXNweB8EBQ9XaHkgY2hvb3NlIEtFQT8fBWUfBmUfBwICZGQCAQ8PFgwfAgUNVGhlIEtFQSBzdG9yeR8DBSF+L2VuL2Fib3V0LWtlYS90aGUta2VhLXN0b3J5LmFzcHgfBAUNVGhlIEtFQSBzdG9yeR8FZR8GZR8HAgJkZAICDw8WDB8CBQlLRUEgR3JlZW4fAwUffi9lbi9hYm91dC1rZWEvZW52aXJvbm1lbnQuYXNweB8EBQlLRUEgR3JlZW4fBWUfBmUfBwICZGQCAw8PFgwfAgUETmV3cx8DBRh+L2VuL2Fib3V0LWtlYS9uZXdzLmFzcHgfBAUETmV3cx8FZR8GZR8HAgJkZAIEDw8WDB8CBQ1IYXBweSBjYW1wZXJzHwMFIX4vZW4vYWJvdXQta2VhL2hhcHB5LWNhbXBlcnMuYXNweB8EBQ1IYXBweSBjYW1wZXJzHwVlHwZlHwcCAmQWAmYPDxYMHwIFFUFkdmVudHVyZSBpbiB0aGUgc25vdx8DBTd+L2VuL2Fib3V0LWtlYS9oYXBweS1jYW1wZXJzL2FkdmVudHVyZS1pbi10aGUtc25vdy5hc3B4HwQFFUFkdmVudHVyZSBpbiB0aGUgc25vdx8FZR8GZR8HAgJkZAIBDw8WDB8CBQhWZWhpY2xlcx8DBRt+L2VuL3ZlaGljbGVzL3ZlaGljbGVzLmFzcHgfBAUIVmVoaWNsZXMfBWUfBgUJcm1Gb2N1c2VkHwcCAmQWEmYPDxYMHwIFCFZlaGljbGVzHwMFG34vZW4vdmVoaWNsZXMvdmVoaWNsZXMuYXNweB8EBQhWZWhpY2xlcx8FZR8GZR8HAgJkZAIBDw8WDB8CBRQyICsgMiBCZXJ0aCBGbGlwLVRvcB8DBSp+L2VuL3ZlaGljbGVzLzItcGx1cy0yLWJlcnRoLWZsaXAtdG9wLmFzcHgfBAUUMiArIDIgQmVydGggRmxpcC1Ub3AfBWUfBmUfBwICZGQCAg8PFgwfAgURMiBCZXJ0aCBDYW1wZXJ2YW4fAwUkfi9lbi92ZWhpY2xlcy8yLWJlcnRoLWNhbXBlcnZhbi5hc3B4HwQFETIgQmVydGggQ2FtcGVydmFuHwVlHwYFCXJtRm9jdXNlZB8HAgJkZAIDDw8WCB8CBQ4zIEJlcnRoIE9yaWdpbh8DBRl+L2VuL3ZlaGljbGVzL29yaWdpbi5hc3B4HwQFDjMgQmVydGggT3JpZ2luHwVlZGQCBA8PFggfAgURNCBCZXJ0aCBNb3RvcmhvbWUfAwUkfi9lbi92ZWhpY2xlcy80LWJlcnRoLW1vdG9yaG9tZS5hc3B4HwQFETQgQmVydGggTW90b3Job21lHwVlZGQCBQ8PFggfAgURNiBCZXJ0aCBNb3RvcmhvbWUfAwUkfi9lbi92ZWhpY2xlcy82LWJlcnRoLW1vdG9yaG9tZS5hc3B4HwQFETYgQmVydGggTW90b3Job21lHwVlZGQCBg8PFggfAgUQTGl2aW5nIGVxdWlwbWVudB8DBSN+L2VuL3ZlaGljbGVzL2xpdmluZy1lcXVpcG1lbnQuYXNweB8EBRBMaXZpbmcgZXF1aXBtZW50HwVlZGQCBw8PFggfAgUJQnV5IGEgS0VBHwMFHWh0dHA6Ly9uenNhbGVzLmtlYWNhbXBlcnMuY29tHwQFCUJ1eSBhIEtFQR8FZWRkAggPDxYIHwIFE01hZGUgaW4gTmV3IFplYWxhbmQfAwUdfi9lbi92ZWhpY2xlcy9tYWRlLWluLW56LmFzcHgfBAUTTWFkZSBpbiBOZXcgWmVhbGFuZB8FZWRkAgIPDxYIHwIFDEtFQSBzcGVjaWFscx8DBRZ+L2VuL0tFQV9zcGVjaWFscy5hc3B4HwQFDEtFQSBzcGVjaWFscx8FZWQWAmYPDxYIHwIFEVBob3RvIGNvbXBldGl0aW9uHwMFKH4vZW4vS0VBX3NwZWNpYWxzL3Bob3RvLWNvbXBldGl0aW9uLmFzcHgfBAURUGhvdG8gY29tcGV0aXRpb24fBWVkZAIDDw8WCB8CBQ5QbGFuIFlvdXIgVHJpcB8DBRh+L2VuL3BsYW4teW91ci10cmlwLmFzcHgfBAUOUGxhbiBZb3VyIFRyaXAfBWVkFgxmDw8WCB8CBRFBYm91dCBOZXcgWmVhbGFuZB8DBSp+L2VuL3BsYW4teW91ci10cmlwL2Fib3V0LW5ldy16ZWFsYW5kLmFzcHgfBAURQWJvdXQgTmV3IFplYWxhbmQfBWVkZAIBDw8WCB8CBRFXaGVyZSBjYW4gSSBjYW1wPx8DBSh+L2VuL3BsYW4teW91ci10cmlwL2NhbXBpbmctZ3JvdW5kcy5hc3B4HwQFEVdoZXJlIGNhbiBJIGNhbXA/HwVlZGQCAg8PFggfAgULSXRpbmVyYXJpZXMfAwUkfi9lbi9wbGFuLXlvdXItdHJpcC9pdGluZXJhcmllcy5hc3B4HwQFC0l0aW5lcmFyaWVzHwVlZBYEZg8PFggfAgUZOCBEYXkgTm9ydGhsYW5kIEl0aW5lcmFyeR8DBT5+L2VuL3BsYW4teW91ci10cmlwL2l0aW5lcmFyaWVzLzgtZGF5LW5vcnRobGFuZC1pdGluZXJhcnkuYXNweB8EBRk4IERheSBOb3J0aGxhbmQgSXRpbmVyYXJ5HwVlZGQCAQ8PFggfAgUdMTEgRGF5IFNvdXRoIElzbGFuZCBJdGluZXJhcnkfAwVCfi9lbi9wbGFuLXlvdXItdHJpcC9pdGluZXJhcmllcy8xMS1kYXktc291dGgtaXNsYW5kLWl0aW5lcmFyeS5hc3B4HwQFHTExIERheSBTb3V0aCBJc2xhbmQgSXRpbmVyYXJ5HwVlZGQCAw8PFggfAgUUVHJhdmVsbGluZyB3aXRoIGtpZHMfAwUtfi9lbi9wbGFuLXlvdXItdHJpcC90cmF2ZWxsaW5nLXdpdGgta2lkcy5hc3B4HwQFFFRyYXZlbGxpbmcgd2l0aCBraWRzHwVlZBYCZg8PFggfAgUMS2lkcycgY29ybmVyHwMFOX4vZW4vcGxhbi15b3VyLXRyaXAvdHJhdmVsbGluZy13aXRoLWtpZHMva2lkcy1jb3JuZXIuYXNweB8EBQxLaWRzJyBjb3JuZXIfBWVkZAIEDw8WCB8CBQtUcmF2ZWwgaW5mbx8DBSR+L2VuL3BsYW4teW91ci10cmlwL1RyYXZlbF9pbmZvLmFzcHgfBAULVHJhdmVsIGluZm8fBWVkFgxmDw8WCB8CBQxBcnJpdmFsIGluZm8fAwU4fi9lbi9wbGFuLXlvdXItdHJpcC9UcmF2ZWxfaW5mby9hcnJpdmFsLWluZm9ybWF0aW9uLmFzcHgfBAUMQXJyaXZhbCBpbmZvHwVlZGQCAQ8PFggfAgUMRHJpdmluZyBpbmZvHwMFMX4vZW4vcGxhbi15b3VyLXRyaXAvVHJhdmVsX2luZm8vZHJpdmluZy1pbmZvLmFzcHgfBAUMRHJpdmluZyBpbmZvHwVlZGQCAg8PFggfAgUKRmVycnkgaW5mbx8DBTZ+L2VuL3BsYW4teW91ci10cmlwL1RyYXZlbF9pbmZvL2ZlcnJ5LWluZm9ybWF0aW9uLmFzcHgfBAUKRmVycnkgaW5mbx8FZWRkAgMPDxYIHwIFCURpc3RhbmNlcx8DBS5+L2VuL3BsYW4teW91ci10cmlwL1RyYXZlbF9pbmZvL2Rpc3RhbmNlcy5hc3B4HwQFCURpc3RhbmNlcx8FZWRkAgQPDxYIHwIFDVdoYXQgdG8gYnJpbmcfAwUyfi9lbi9wbGFuLXlvdXItdHJpcC9UcmF2ZWxfaW5mby93aGF0LXRvLWJyaW5nLmFzcHgfBAUNV2hhdCB0byBicmluZx8FZWRkAgUPDxYIHwIFDEtlZXBpbmcgc2FmZR8DBTF+L2VuL3BsYW4teW91ci10cmlwL1RyYXZlbF9pbmZvL2tlZXBpbmctc2FmZS5hc3B4HwQFDEtlZXBpbmcgc2FmZR8FZWRkAgUPDxYIHwIFCUtFQSBOZXN0cx8DBSJ+L2VuL3BsYW4teW91ci10cmlwL0tFQV9OZXN0cy5hc3B4HwQFCUtFQSBOZXN0cx8FZWRkAgQPDxYIHwIFC1JlbnRhbCBJbmZvHwMFFX4vZW4vcmVudGFsLWluZm8uYXNweB8EBQtSZW50YWwgSW5mbx8FZWQWCmYPDxYIHwIFEUluc3VyYW5jZSBvcHRpb25zHwMFJ34vZW4vcmVudGFsLWluZm8vaW5zdXJhbmNlX29wdGlvbnMuYXNweB8EBRFJbnN1cmFuY2Ugb3B0aW9ucx8FZWRkAgEPDxYIHwIFE09ubGluZSBwcmUgY2hlY2staW4fAwVpaHR0cHM6Ly9zZWN1cmUudHBsLXNlcnZpY2VzLmNvbS9tb2R1bGVzL3ByZWNoZWNraW4vbG9naW4uYXNweD9zaXRlSUQ9YjhlN2I3OTctNzU0My00OTAxLTg1OGMtYzhlMjlkMTY2NTA0HwQFE09ubGluZSBwcmUgY2hlY2staW4fBWVkZAICDw8WCB8CBQ9PcHRpb25hbCBleHRyYXMfAwUlfi9lbi9yZW50YWwtaW5mby9vcHRpb25hbC1leHRyYXMuYXNweB8EBQ9PcHRpb25hbCBleHRyYXMfBWVkZAIDDw8WCB8CBQNGQVEfAwUZfi9lbi9yZW50YWwtaW5mby9mYXEuYXNweB8EBQNGQVEfBWVkZAIEDw8WCB8CBRJUZXJtcyAmIGNvbmRpdGlvbnMfAwUqfi9lbi9yZW50YWwtaW5mby90ZXJtcy1hbmQtY29uZGl0aW9ucy5hc3B4HwQFElRlcm1zICYgY29uZGl0aW9ucx8FZWRkAgUPDxYIHwIFB0NvbnRhY3QfAwURfi9lbi9jb250YWN0LmFzcHgfBAUHQ29udGFjdB8FZWQWCGYPDxYIHwIFCkNvbnRhY3QgdXMfAwUcfi9lbi9jb250YWN0L2NvbnRhY3RfdXMuYXNweB8EBQpDb250YWN0IHVzHwVlZGQCAQ8PFggfAgUOQXVja2xhbmQgZGVwb3QfAwUgfi9lbi9jb250YWN0L2F1Y2tsYW5kLWRlcG90LmFzcHgfBAUOQXVja2xhbmQgZGVwb3QfBWVkZAICDw8WCB8CBRJDaHJpc3RjaHVyY2ggZGVwb3QfAwUkfi9lbi9jb250YWN0L2NocmlzdGNodXJjaC1kZXBvdC5hc3B4HwQFEkNocmlzdGNodXJjaCBkZXBvdB8FZWRkAgMPDxYIHwIFDUtFQSB3b3JsZHdpZGUfAwUffi9lbi9jb250YWN0L0tFQV93b3JsZHdpZGUuYXNweB8EBQ1LRUEgd29ybGR3aWRlHwVlZGQCJQ9kFgICAQ9kFgJmD2QWAmYPZBYCAgUPFgIeC18hSXRlbUNvdW50AgEWAgIBD2QWEgIBDw8WAh8DBUN+L2VuL2Fib3V0LWtlYS9uZXdzL2luZGl2aWR1YWwtbmV3cy8xMi0wOS0yMC9MZXRfc19nb19HbGFtcGluZy5hc3B4ZBYCAgEPFgIfAgUSTGV0J3MgZ28gR2xhbXBpbmchZAIDDxYEHwIFGDIwLzA5LzIwMTIgMTA6MTQ6NDUgYS5tLh4HVmlzaWJsZWhkAgUPFgIfAgWmAURFRklOSVRJT046IEEgZm9ybSBvZiBjYW1waW5nIGluIHdoaWNoIHBhcnRpY2lwYW50cyBlbmpveSBwaHlzaWNhbCBjb21mb3J0cyBhc3NvY2lhdGVkIHdpdGggbW9yZSBsdXh1cmlvdXMgdHlwZXMgb2YgaG9saWRheSDigJMgU2hvcnRoYW5kIGZvciDigJxHbGFtb3JvdXMgQ2FtcGluZ+KAnS5kAgcPDxYEHwMFQ34vZW4vYWJvdXQta2VhL25ld3MvaW5kaXZpZHVhbC1uZXdzLzEyLTA5LTIwL0xldF9zX2dvX0dsYW1waW5nLmFzcHgfCWhkZAIJDxYCHwloZAILDxYEHwIFDFNjb3R0IENsZWFyeR8JaGQCDQ8WBB8CBQ5UaGUgQXVja2xhbmRlch8JaGQCDw8PFgQfAmUfCWhkZAIRDxYEHwgC/////w8fCWhkGBoFJGN0bDAwJGZvb3RlcmxpbmtzJHRtX0dlbmVyaWNDb250ZW50NA8UKwABZGQFG2N0bDAwJHRhYjEkR2VuZXJpY0NvbnRlbnQxMA8UKwABZGQFImN0bDAwJHNlYXJjaGJveCR0bV9HZW5lcmljQ29udGVudDEPFCsAAQUPR2VuZXJpY19Db250ZW50ZAUeY3RsMDAkaW5mb2JveDEkR2VuZXJpY0NvbnRlbnQ0DxQrAAEFD0dlbmVyaWNfQ29udGVudGQFHWN0bDAwJGZvb3RlcmJveDMkdG1fTmV3c1ZpZXcxDxQrAAIUKwAEKClYU3lzdGVtLkd1aWQsIG1zY29ybGliLCBWZXJzaW9uPTIuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjc3YTVjNTYxOTM0ZTA4OSQwMDAwMDAwMC0wMDAwLTAwMDAtMDAwMC0wMDAwMDAwMDAwMDACAWQLKZQBVGVsZXJpay5DbXMuRW5naW5lLldlYkNvbnRyb2xzLkNvbnRlbnRWaWV3K0JlaGF2aW9yTW9kZXMsIFRlbGVyaWsuQ21zLkVuZ2luZSwgVmVyc2lvbj0zLjcuMjA5Ni4yMjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49ZGZlYWVlMGUzOTc4YWM3OQEFN1B1YmxpY2F0aW9uX0RhdGUgPD0gIiNub3ciIEFORCBFeHBpcmF0aW9uX0RhdGUgPiAiI25vdyJkBRpjdGwwMCR0YWIzJEdlbmVyaWNDb250ZW50OQ8UKwABZGQFHmN0bDAwJGluZm9ib3gzJEdlbmVyaWNDb250ZW50Ng8UKwABBQ9HZW5lcmljX0NvbnRlbnRkBSJjdGwwMCRmZWF0dXJlc2JveCRHZW5lcmljQ29udGVudDEzDxQrAAFkZAUjY3RsMDAkZm9vdGVyYm94NCR0bV9HZW5lcmljQ29udGVudDgPFCsAAQUPR2VuZXJpY19Db250ZW50ZAUjY3RsMDAkZm9vdGVyYm94MSR0bV9HZW5lcmljQ29udGVudDYPFCsAAQUPR2VuZXJpY19Db250ZW50ZAUbY3RsMDAkdGFiMiRHZW5lcmljQ29udGVudDExDxQrAAFkZAUeY3RsMDAkaW5mb2JveDQkR2VuZXJpY0NvbnRlbnQ3DxQrAAEFD0dlbmVyaWNfQ29udGVudGQFG2N0bDAwJHRhYjEkR2VuZXJpY0NvbnRlbnQxMg8UKwABZGQFI2N0bDAwJGZvb3RlcmJveDIkdG1fR2VuZXJpY0NvbnRlbnQ3DxQrAAEFD0dlbmVyaWNfQ29udGVudGQFKmN0bDAwJGZvb3RlcmJveDMkdG1fTmV3c1ZpZXcxJGN0bDAwJHBhZ2VyMQ8UKwADZmZoZAUaY3RsMDAkdGFiNCRHZW5lcmljQ29udGVudDEPFCsAAWRkBSJjdGwwMCRzZWFyY2hib3gkdG1fR2VuZXJpY0NvbnRlbnQyDxQrAAFkZAUeY3RsMDAkaW5mb2JveDIkR2VuZXJpY0NvbnRlbnQ1DxQrAAEFD0dlbmVyaWNfQ29udGVudGQFGmN0bDAwJHRhYnMkR2VuZXJpY0NvbnRlbnQyDxQrAAFkZAUlY3RsMDAkbGl2aW5nZXF1aXBtZW50JEdlbmVyaWNDb250ZW50OA8UKwABBQ9HZW5lcmljX0NvbnRlbnRkBSNjdGwwMCRmb290ZXJib3gzJHRtX0dlbmVyaWNDb250ZW50OQ8UKwABZGQFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYCBTZjdGwwMCRzZWFyY2hib3gkdG1fU2VhcmNoQm94MSRjdGwwMCRjdGwwMCRzZWFyY2hCdXR0b24FSmN0bDAwJG1lbnUkdG1fc2l0ZWZpbml0eV91c2VyY29udHJvbHNfbmF2aWdhdGlvbjM1X3NpdGVtZW51X2FzY3gxJFJhZE1lbnUxBSNjdGwwMCRyZXNlcnZlYm94JHRtX0dlbmVyaWNDb250ZW50Mw8UKwABZGQFJGN0bDAwJGZvb3RlcmJveDMkdG1fR2VuZXJpY0NvbnRlbnQxMA8UKwABZGQFI2N0bDAwJGZvb3RlcmxvZ28kdG1fR2VuZXJpY0NvbnRlbnQ1DxQrAAFkZAUaY3RsMDAkdGFiMSRHZW5lcmljQ29udGVudDMPFCsAAWRkXLqpdGFHDF5MjaGXdoZol11w2fY=" />
    </div>
    <script type="text/javascript">
//<![CDATA[
        var theForm = document.forms['aspnetForm'];
        if (!theForm) {
            theForm = document.aspnetForm;
        }
        function __doPostBack(eventTarget, eventArgument) {
            if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
                theForm.__EVENTTARGET.value = eventTarget;
                theForm.__EVENTARGUMENT.value = eventArgument;
                theForm.submit();
            }
        }
//]]>
    </script>
    <script src="http://nzrentals.keacampers.com/WebResource.axd?d=vspu114YNVfe0aHWxuWydOsvHGHf1zHeMl-bSeytaFlb6m4y66Ilc28liWX83_WuvkimAvSzRAVjKnyFTpAhNgN8C_s1&amp;t=634605978834856163"
        type="text/javascript"></script>
    <script src="http://nzrentals.keacampers.com/WebResource.axd?d=Bxlo2niccExBxH14C5ApJCCrt-maTVER1m_elL5Y_8NlwVBfRPSOAVJiAbbMTpCK5kEoWhPz6MQ9ndwl7N45OQunQ_gI1m98EASXrwvepvhh93N0CxAB0blpeFDvEDmXl4vzMA2&amp;t=634039116320000000"
        type="text/javascript"></script>
    <script src="http://nzrentals.keacampers.com/ScriptResource.axd?d=hw_tme9xZvHMHzcaa1h9sjvneZD_IFUYhnw2wG97RBSSy2mXQjTGfwPZSFWAVnvdH9HZNGnwaHLVwAVn4vbzrnB__0KJSk7IxdEfycc5_tc_yBCiyKnFpEL2r_haJFtlYkmWAQ2&amp;t=634039947500000000"
        type="text/javascript"></script>
    <script src="http://nzrentals.keacampers.com/ScriptResource.axd?d=40UTB3MkQsNlRaRIE2bez4OVrW75FDrWGI5D_tIAN5vNaPfumuFr4rwwf-ZdOOAVKElFA_mZiFOTS5R7SKLyBEEubOoWnRxDRSnFjsdeu9EcWkTRIKU_zKXIC780cvjv-RHQDw2&amp;t=634039947500000000"
        type="text/javascript"></script>
    <script src="http://nzrentals.keacampers.com/ScriptResource.axd?d=FPAgDzVoBry3rZHA2wIsHGNYxZsJ2iXYnEjY68cNdSMOhjDv7KAHYVpisxLZvHE2teZN5qlDxBFh2rqtJ7E1dsxg5sq9qoXUUurwDhbZTDjdXeZ8uwWO-8qdQIxu6niQNnc7zA2&amp;t=634039947500000000"
        type="text/javascript"></script>
    <script src="http://nzrentals.keacampers.com/ScriptResource.axd?d=er96aEzEUvhBY7NVZCbNZA3_teTikKSS0kffA9gYlCbb9XbmlPZCjqkKUMV7_6lHDNXZbWwTR9EpO96DmmEEa3srt7sV9-7sAjOqjd-9hYu4E4GjIxM349ucEkAiUrUqNF6Xsw2&amp;t=634039947500000000"
        type="text/javascript"></script>
    <script src="http://nzrentals.keacampers.com/ScriptResource.axd?d=8s7mdWz5yHER5oBYb-BAZet3xwVVgp65LunXrpuQWdL6nnGk-A7QJupkMqgLeRVxnbPkdWgecOn1dVpJlmF9PG1aoC0jhLvn2np6CtHtLyy4vDFmYe6eiITQZaZP44aavtqUQQ2&amp;t=634039947500000000"
        type="text/javascript"></script>
    <script src="http://nzrentals.keacampers.com/ScriptResource.axd?d=dciJ3sG0eVRq7DIihWeywR6rYM8lOEzHlb_CmT594B01HPAU9aVDZB7-EaDWRO0o3lSJlV63GZVK7iOkcnvZegl0hm1nyk3zj_zvh74Q4lq9eOSbbp0WLu1RLqitc3QqOKtuYw9ktSRwkPK7WIlw5bBC1uk1&amp;t=634039947500000000"
        type="text/javascript"></script>
    <script src="http://nzrentals.keacampers.com/ScriptResource.axd?d=-v-kUq4x-m8Wxg_G1sZvpEKh3ySQJ3f3PZM0wP1qcXY10zD0GJosWwWOuZSnXxNxGpdM6TX-VY_X0V-0hei0Rp34jLgzzdVnB8U6r5gB45cMPe5IzWT_m4a-viX8Jj0DNp8nC1L3lhJT9roxd91aO2a0A241&amp;t=634039947500000000"
        type="text/javascript"></script>
    <script src="http://nzrentals.keacampers.com/ScriptResource.axd?d=WPJf9tDV8dtBwG1udQyvKbFdzLWiSXUgPC6XlepvGP1xIi5lSp02LJRqNDaCjazyNnGHdzdkLV0_faETkN3KZeVSxthLEUt7rzIgQgW1JCV8B5kTBc0mhaMhNxrSpRr6UHln5A2&amp;t=634039947500000000"
        type="text/javascript"></script>
    <div>
        <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEWAwKS4qqpBgKA79SLCQKLu9O+AlwUxt97uLlTNP1mKsugftKzCg03" />
    </div>
    <script type="text/javascript">
//<![CDATA[
        Sys.WebForms.PageRequestManager._initialize('ctl00$ScriptManager1', document.getElementById('aspnetForm'));
        Sys.WebForms.PageRequestManager.getInstance()._updateControls([], [], [], 90);
//]]>
    </script>
    <!-- wrapper start -->
    <div id="wrapper">
        <!-- header start -->
        <div id="header-wrap">
            <div id="header-content">
                <a href="javascript:homeLink();">
                    <img src="http://nzrentals.keacampers.com/images/kea-logo2.gif" alt="" class="logo" /></a>
                <!-- header search and reserve area start -->
                <div id="searchandreserve">
                    <fieldset class="sf_searchBox">
                        <label for="ctl00_searchbox_tm_SearchBox1_ctl00_ctl00_queryText">
                        </label>
                        <input name="ctl00$searchbox$tm_SearchBox1$ctl00$ctl00$queryText" type="text" value="Search this site"
                            id="ctl00_searchbox_tm_SearchBox1_ctl00_ctl00_queryText" class="sf_searchText" /><input
                                src="http://nzrentals.keacampers.com/images/search-button.gif" name="ctl00$searchbox$tm_SearchBox1$ctl00$ctl00$searchButton"
                                type="image" id="ctl00_searchbox_tm_SearchBox1_ctl00_ctl00_searchButton" class="sf_searchSubmit"
                                alt="Search" />
                    </fieldset>
                    <a href="http://www.keacampers.com" class="globalbut">GLOBAL SITE</a><a href="#"
                        id="reserve-button1" shape="rect">Get A Quote<br />
                        Book Online</a>
                    <ul id="reserve-drop">
                        <li class="vt">Vehicle type<br />
                            <select id="vehicletype">
                                <option selected="true" value="bn">2 + 2 Berth Flip-Top Campervan</option>
                                <option value="en">2 Berth Shower &amp; Toilet Campervan</option>
                                <option value="on">3 Berth Shower &amp; Toilet Motorhome</option>
                                <option value="jn">4 Berth Shower &amp; Toilet Motorhome</option>
                                <option value="ln">6 Berth Shower &amp; Toilet Motorhome</option>
                            </select>
                        </li>
                        <li class="pl">Pickup location<br />
                            <select id="puloc">
                                <option selected="true" value="akl">Auckland</option>
                                <option value="chc">Christchurch</option>
                            </select>
                        </li>
                        <li class="pl">Pickup Date<br />
                            <input id="pudatepicker" class="datepicker" readonly="true" type="text" />
                        </li>
                        <li class="pl">DropOff location<br />
                            <select id="doloc">
                                <option selected="true" value="akl">Auckland</option>
                                <option value="chc">Christchurch</option>
                            </select>
                        </li>
                        <li class="pl">DropOff Date<br />
                            <input id="dodatepicker" class="datepicker" readonly="true" type="text" />
                            <script>
                                jQuery(function () {
                                    jQuery("#pudatepicker").datepicker(
{
    onSelect: function (datetext) {
        pudate = jQuery("#pudatepicker").datepicker("getDate");
        pudate.setDate(pudate.getDate() + 4);
        jQuery("#dodatepicker").datepicker("setDate", pudate);
    }
});
                                    jQuery("#dodatepicker").datepicker({});
                                    jQuery("#pudatepicker").datepicker('option', 'showOn', 'button');
                                    jQuery("#pudatepicker").datepicker('option', 'buttonImage', 'http://nzrentals.keacampers.com/images/datepicker/calendar.gif');
                                    jQuery("#pudatepicker").datepicker('option', 'buttonImageOnly', 'true');
                                    jQuery("#pudatepicker").datepicker('option', 'dateFormat', 'dd-mm-yy');
                                    jQuery("#pudatepicker").datepicker('option', 'minDate', '+2');
                                    jQuery("#pudatepicker").datepicker("setDate", new Date());

                                    var pudate = jQuery("#pudatepicker").datepicker("getDate");
                                    pudate.setDate(pudate.getDate() + 4);

                                    jQuery("#dodatepicker").datepicker('option', 'showOn', 'button');
                                    jQuery("#dodatepicker").datepicker('option', 'buttonImage', 'http://nzrentals.keacampers.com/images/datepicker/calendar.gif');
                                    jQuery("#dodatepicker").datepicker('option', 'buttonImageOnly', 'true');
                                    jQuery("#dodatepicker").datepicker('option', 'dateFormat', 'dd-mm-yy');
                                    jQuery("#dodatepicker").datepicker('option', 'minDate', pudate);
                                    jQuery("#dodatepicker").datepicker("setDate", pudate);
                                });
                            </script>
                        </li>
                        <li class="pl"><a href="#" onclick="vehicleSearch()" shape="rect">
                            <img alt="" style="border: 0px solid;" src="http://nzrentals.keacampers.com/images/book-now-but.gif" /></a>
                        </li>
                        <li class="pl"><a href="#" id="reserve-close" shape="rect">close</a> </li>
                    </ul>
                </div>
                <!-- header search and reserve area end -->
                <!-- language select place holder start -->
                <div id="language-select">
                    <a href="javascript:english();">
                        <img src="http://nzrentals.keacampers.com/images/gb.png" alt="" /></a> <a href="javascript:german();">
                            <img src="http://nzrentals.keacampers.com/images/de.png" alt="" /></a> <a href="javascript:french();">
                                <img src="http://nzrentals.keacampers.com/images/fr.png" alt="" /></a> <a href="http://cn.keacampers.com/zh-cn/about-kea.aspx">
                                    <img src="http://nzrentals.keacampers.com/images/cn.png" alt="Chinese" /></a>
                    <!-- <a href="http://nzrentals.keacampers.com/nl/about-kea.aspx"><img src="/images/nl.png" alt="Dutch" /></a> -->
                    <a href="http://nzrentals.keacampers.com/es/default.aspx">
                        <img src="http://nzrentals.keacampers.com/images/es.png" alt="Spanish" /></a>
                </div>
                <!-- language select place holder end -->
                <!-- main nav start -->
                <div id="main-nav">
                    <div id="ctl00_menu_tm_sitefinity_usercontrols_navigation35_sitemenu_ascx1_RadMenu1"
                        class="RadMenu RadMenu_Default">
                        <!-- 2010.1.309.20 -->
                        <ul class="rmHorizontal rmRootGroup">
                            <li class="rmItem rmFirst"><a href="http://nzrentals.keacampers.com/en/about-kea.aspx" class="rmLink"><span class="rmText">
                                About KEA</span></a><div class="rmSlide">
                                    <ul class="rmVertical rmGroup rmLevel1">
                                        <li class="rmItem rmFirst"><a href="http://nzrentals.keacampers.com/en/about-kea/why-choose-kea.aspx" class="rmLink">
                                            <span class="rmText">Why choose KEA?</span></a></li><li class="rmItem "><a href="http://nzrentals.keacampers.com/en/about-kea/the-kea-story.aspx"
                                                class="rmLink"><span class="rmText">The KEA story</span></a></li><li class="rmItem ">
                                                    <a href="http://nzrentals.keacampers.com/en/about-kea/environment.aspx" class="rmLink"><span class="rmText">KEA Green</span></a></li><li
                                                        class="rmItem "><a href="http://nzrentals.keacampers.com/en/about-kea/news.aspx" class="rmLink"><span class="rmText">
                                                            News</span></a></li><li class="rmItem rmLast"><a href="http://nzrentals.keacampers.com/en/about-kea/happy-campers.aspx"
                                                                class="rmLink"><span class="rmText">Happy campers</span></a><div class="rmSlide">
                                                                    <ul class="rmVertical rmGroup rmLevel2">
                                                                        <li class="rmItem rmFirst rmLast"><a href="http://nzrentals.keacampers.com/en/about-kea/happy-campers/adventure-in-the-snow.aspx"
                                                                            class="rmLink"><span class="rmText">Adventure in the snow</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="rmItem "><a href="vehicles.aspx" class="rmLink rmFocused"><span class="rmText">
                                Vehicles</span></a><div class="rmSlide">
                                    <ul class="rmVertical rmGroup rmLevel1">
                                        <li class="rmItem rmFirst"><a href="vehicles.aspx" class="rmLink"><span class="rmText">
                                            Vehicles</span></a></li><li class="rmItem "><a href="2-plus-2-berth-flip-top.aspx"
                                                class="rmLink"><span class="rmText">2 + 2 Berth Flip-Top</span></a></li><li class="rmItem ">
                                                    <a href="2-berth-campervan.aspx" class="rmLink rmFocused"><span class="rmText">2 Berth
                                                        Campervan</span></a></li><li class="rmItem "><a href="origin.aspx" class="rmLink"><span
                                                            class="rmText">3 Berth Origin</span></a></li><li class="rmItem "><a href="4-berth-motorhome.aspx"
                                                                class="rmLink"><span class="rmText">4 Berth Motorhome</span></a></li><li class="rmItem ">
                                                                    <a href="6-berth-motorhome.aspx" class="rmLink"><span class="rmText">6 Berth Motorhome</span></a></li><li
                                                                        class="rmItem "><a href="living-equipment.aspx" class="rmLink"><span class="rmText">
                                                                            Living equipment</span></a></li><li class="rmItem "><a href="http://nzsales.keacampers.com"
                                                                                class="rmLink"><span class="rmText">Buy a KEA</span></a></li><li class="rmItem rmLast">
                                                                                    <a href="made-in-nz.aspx" class="rmLink"><span class="rmText">Made in New Zealand</span></a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="rmItem "><a href="../KEA_specials.aspx" class="rmLink"><span class="rmText">
                                KEA specials</span></a><div class="rmSlide">
                                    <ul class="rmVertical rmGroup rmLevel1">
                                        <li class="rmItem rmFirst rmLast"><a href="../KEA_specials/photo-competition.aspx"
                                            class="rmLink"><span class="rmText">Photo competition</span></a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="rmItem "><a href="../plan-your-trip.aspx" class="rmLink"><span class="rmText">
                                Plan Your Trip</span></a><div class="rmSlide">
                                    <ul class="rmVertical rmGroup rmLevel1">
                                        <li class="rmItem rmFirst"><a href="../plan-your-trip/about-new-zealand.aspx" class="rmLink">
                                            <span class="rmText">About New Zealand</span></a></li><li class="rmItem "><a href="../plan-your-trip/camping-grounds.aspx"
                                                class="rmLink"><span class="rmText">Where can I camp?</span></a></li><li class="rmItem ">
                                                    <a href="../plan-your-trip/itineraries.aspx" class="rmLink"><span class="rmText">Itineraries</span></a><div
                                                        class="rmSlide">
                                                        <ul class="rmVertical rmGroup rmLevel2">
                                                            <li class="rmItem rmFirst"><a href="../plan-your-trip/itineraries/8-day-northland-itinerary.aspx"
                                                                class="rmLink"><span class="rmText">8 Day Northland Itinerary</span></a></li><li
                                                                    class="rmItem rmLast"><a href="../plan-your-trip/itineraries/11-day-south-island-itinerary.aspx"
                                                                        class="rmLink"><span class="rmText">11 Day South Island Itinerary</span></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                        <li class="rmItem "><a href="../plan-your-trip/travelling-with-kids.aspx" class="rmLink">
                                            <span class="rmText">Travelling with kids</span></a><div class="rmSlide">
                                                <ul class="rmVertical rmGroup rmLevel2">
                                                    <li class="rmItem rmFirst rmLast"><a href="../plan-your-trip/travelling-with-kids/kids-corner.aspx"
                                                        class="rmLink"><span class="rmText">Kids' corner</span></a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="rmItem "><a href="../plan-your-trip/Travel_info.aspx" class="rmLink"><span
                                            class="rmText">Travel info</span></a><div class="rmSlide">
                                                <ul class="rmVertical rmGroup rmLevel2">
                                                    <li class="rmItem rmFirst"><a href="../plan-your-trip/Travel_info/arrival-information.aspx"
                                                        class="rmLink"><span class="rmText">Arrival info</span></a></li><li class="rmItem ">
                                                            <a href="../plan-your-trip/Travel_info/driving-info.aspx" class="rmLink"><span class="rmText">
                                                                Driving info</span></a></li><li class="rmItem "><a href="../plan-your-trip/Travel_info/ferry-information.aspx"
                                                                    class="rmLink"><span class="rmText">Ferry info</span></a></li><li class="rmItem "><a
                                                                        href="../plan-your-trip/Travel_info/distances.aspx" class="rmLink"><span class="rmText">
                                                                            Distances</span></a></li><li class="rmItem "><a href="../plan-your-trip/Travel_info/what-to-bring.aspx"
                                                                                class="rmLink"><span class="rmText">What to bring</span></a></li><li class="rmItem rmLast">
                                                                                    <a href="../plan-your-trip/Travel_info/keeping-safe.aspx" class="rmLink"><span class="rmText">
                                                                                        Keeping safe</span></a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="rmItem rmLast"><a href="../plan-your-trip/KEA_Nests.aspx" class="rmLink">
                                            <span class="rmText">KEA Nests</span></a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="rmItem "><a href="../rental-info.aspx" class="rmLink"><span class="rmText">
                                Rental Info</span></a><div class="rmSlide">
                                    <ul class="rmVertical rmGroup rmLevel1">
                                        <li class="rmItem rmFirst"><a href="../rental-info/insurance_options.aspx" class="rmLink">
                                            <span class="rmText">Insurance options</span></a></li><li class="rmItem "><a href="https://secure.tpl-services.com/modules/precheckin/login.aspx?siteID=b8e7b797-7543-4901-858c-c8e29d166504"
                                                class="rmLink"><span class="rmText">Online pre check-in</span></a></li><li class="rmItem ">
                                                    <a href="../rental-info/optional-extras.aspx" class="rmLink"><span class="rmText">Optional
                                                        extras</span></a></li><li class="rmItem "><a href="../rental-info/faq.aspx" class="rmLink">
                                                            <span class="rmText">FAQ</span></a></li><li class="rmItem rmLast"><a href="../rental-info/terms-and-conditions.aspx"
                                                                class="rmLink"><span class="rmText">Terms & conditions</span></a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="rmItem rmLast"><a href="../contact.aspx" class="rmLink"><span class="rmText">
                                Contact</span></a><div class="rmSlide">
                                    <ul class="rmVertical rmGroup rmLevel1">
                                        <li class="rmItem rmFirst"><a href="../contact/contact_us.aspx" class="rmLink"><span
                                            class="rmText">Contact us</span></a></li><li class="rmItem "><a href="../contact/auckland-depot.aspx"
                                                class="rmLink"><span class="rmText">Auckland depot</span></a></li><li class="rmItem ">
                                                    <a href="../contact/christchurch-depot.aspx" class="rmLink"><span class="rmText">Christchurch
                                                        depot</span></a></li><li class="rmItem rmLast"><a href="../contact/KEA_worldwide.aspx"
                                                            class="rmLink"><span class="rmText">KEA worldwide</span></a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                        <input id="ctl00_menu_tm_sitefinity_usercontrols_navigation35_sitemenu_ascx1_RadMenu1_ClientState"
                            name="ctl00_menu_tm_sitefinity_usercontrols_navigation35_sitemenu_ascx1_RadMenu1_ClientState"
                            type="hidden" />
                    </div>
                </div>
                <!-- main nav end -->
            </div>
        </div>
        <!-- header end -->
        <!-- slideshow start -->
        <div id="image-wrapper">
            <div id="image-container">
                <img src="" class="image-swap" />
                <iframe class="youtube-video" width="480" height="341" src="" frameborder="0" allowfullscreen>
                </iframe>
            </div>
            <div id="features-container">
                <div id="features-box">
                    <table style="float: left; border-right-color: #666666; border-right-width: 1px;
                        border-right-style: solid;">
                        <tbody>
                            <tr>
                                <td>
                                    <img alt="" class="features" src="http://nzrentals.keacampers.com/images/vehicles/2st/vehicle.jpg" />
                                </td>
                                <td style="padding-right: 20px;">
                                    <img alt="" src="http://nzrentals.keacampers.com/images/vehicles/2st/bed.jpg" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img alt="" class="features" src="http://nzrentals.keacampers.com/images/vehicles/2st/heating.jpg" />
                                </td>
                                <td>
                                    <img alt="" class="features" src="http://nzrentals.keacampers.com/images/vehicles/2st/visual.jpg" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img alt="" src="http://nzrentals.keacampers.com/images/vehicles/2st/bathroom.jpg" />
                                </td>
                                <td>
                                    <img alt="" src="http://nzrentals.keacampers.com/images/vehicles/2st/kitchen.jpg" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <img alt="" class="video-button" src="http://nzrentals.keacampers.com/images/vehicles/video-button.jpg" />
                    <div class="textbox">
                        This is a vehicle with practical, easy living with great options for indoor/outdoor
                        flow<a href="http://vimeo.com/28692685"><img alt="" src="http://nzrentals.keacampers.com/Images/video-button4.jpg"
                            style="margin-top: 27px;" /></a></div>
                </div>
            </div>
        </div>
        <!-- slideshow end -->
        <!-- sub page content start -->
        <div class="standard-page-content-wrap">
            <div id="right-nav">
                <div id="reserve-button-right">
                    Get A Quote</div>
                <div id="reserve-drop3">
                    <div class="vt">
                        Vehicle type<br />
                        <select id="vehicletype2">
                            <option selected="true" value="bn">2 + 2 Berth Flip-Top Campervan</option>
                            <option value="en">2 Berth Shower &amp; Toilet Campervan</option>
                            <option value="on">3 Berth Shower &amp; Toilet Campervan</option>
                            <option value="jn">4 Berth Shower &amp; Toilet Motorhome</option>
                            <option value="ln">6 Berth Shower &amp; Toilet Motorhome</option>
                        </select></div>
                    <div class="pl">
                        Pickup location<br />
                        <select id="puloc2">
                            <option selected="true" value="akl">Auckland</option>
                            <option value="chc">Christchurch</option>
                        </select></div>
                    <div class="pl">
                        Pickup Date<br />
                        <input id="pudatepicker2" class="datepicker" readonly="true" type="text" />
                    </div>
                    <div class="pl">
                        DropOff location<br />
                        <select id="doloc2">
                            <option selected="true" value="akl">Auckland</option>
                            <option value="chc">Christchurch</option>
                        </select></div>
                    <div class="pl">
                        DropOff Date<br />
                        <input id="dodatepicker2" class="datepicker" readonly="true" type="text" />
                        <script>
                            jQuery(function () {
                                jQuery("#pudatepicker2").datepicker(
{
    onSelect: function (datetext) {
        pudate = jQuery("#pudatepicker2").datepicker("getDate");
        pudate.setDate(pudate.getDate() + 4);
        jQuery("#dodatepicker2").datepicker("setDate", pudate);
    }
});
                                jQuery("#dodatepicker2").datepicker({});
                                jQuery("#pudatepicker2").datepicker('option', 'showOn', 'button');
                                jQuery("#pudatepicker2").datepicker('option', 'buttonImage', 'http://nzrentals.keacampers.com/images/datepicker/calendar.gif');
                                jQuery("#pudatepicker2").datepicker('option', 'buttonImageOnly', 'true');
                                jQuery("#pudatepicker2").datepicker('option', 'dateFormat', 'dd-mm-yy');
                                jQuery("#pudatepicker2").datepicker('option', 'minDate', '+2');
                                jQuery("#pudatepicker2").datepicker("setDate", new Date());

                                var pudate = jQuery("#pudatepicker2").datepicker("getDate");
                                pudate.setDate(pudate.getDate() + 4);

                                jQuery("#dodatepicker2").datepicker('option', 'showOn', 'button');
                                jQuery("#dodatepicker2").datepicker('option', 'buttonImage', 'http://nzrentals.keacampers.com/images/datepicker/calendar.gif');
                                jQuery("#dodatepicker2").datepicker('option', 'buttonImageOnly', 'true');
                                jQuery("#dodatepicker2").datepicker('option', 'dateFormat', 'dd-mm-yy');
                                jQuery("#dodatepicker2").datepicker('option', 'minDate', pudate);
                                jQuery("#dodatepicker2").datepicker("setDate", pudate);
                            });
                        </script>
                    </div>
                    <div class="pl">
                        <a href="#" onclick="vehicleSearch2()">
                            <img alt="" style="border: 0px solid;" src="http://nzrentals.keacampers.com/images/book-now-but.gif" /></a></div>
                </div>
                <div class="right-nav-info-box" id="first-rnib">
                    <img alt="" src="http://nzrentals.keacampers.com/Images/2FT_D_nz.jpg" style="width: 70px; height: 60px;" /><br />
                    <h2>
                        2 + 2 Berth Flip-Top</h2>
                    <p>
                        Descended from the fabulous VW Kombi, this superbly equipped campervan doubles as
                        a highly efficient family wagon...</p>
                    <a href="http://nzrentals.keacampers.com/en/vehicles/2-plus-2-berth-flip-top.aspx">Read more...</a>
                </div>
                <div class="right-nav-info-box">
                    <img alt="" src="http://nzrentals.keacampers.com/Images/2ST_floorplan_day_cropped.jpg" style="width: 70px; height: 60px;" /><br />
                    <h2>
                        2 Berth Campervan</h2>
                    <p>
                        This luxury 2 Berth campervan has all the outstanding features of a full sized motorhome...</p>
                    <a href="http://nzrentals.keacampers.com/en/vehicles/2-berth-campervan.aspx">Read more...</a>
                </div>
                <div class="right-nav-info-box">
                    <img alt="" src="http://nzrentals.keacampers.com/Images/4b_D_nz.jpg" style="width: 70px; height: 60px;" /><br />
                    <h2>
                        4 Berth Motorhome</h2>
                    <p>
                        Even after the longest day exploring the wonders of New Zealand, you will find a
                        warm welcome...</p>
                    <a href="http://nzrentals.keacampers.com/en/vehicles/4-berth-motorhome.aspx">Read more...</a>
                </div>
                <div class="right-nav-info-box">
                    <img alt="" src="http://nzrentals.keacampers.com/Images/6b_D_nz.jpg" style="width: 70px; height: 60px;" /><br />
                    <h2>
                        6 Berth Motorhome</h2>
                    <p>
                        This is the ultimate motorhome for a family of up to 6 people. Large and spacious
                        inside - with all the comforts of home...</p>
                    <a href="http://nzrentals.keacampers.com/en/vehicles/6-berth-motorhome.aspx">Read more...</a><br />
                    <p>
                        &nbsp;</p>
                    <p>
                    </p>
                    <p style="margin-top: 7.5pt; line-height: 150%;">
                        <strong><span style="line-height: 150%; font-size: 10.5pt; color: #7da42d;">All KEA
                            vehicles feature:</span></strong></p>
                    <p>
                        <br />
                    </p>
                    <p>
                    </p>
                    • Solar panels for enhanced battery life<br />
                    • Water filters – saves plastic drink bottles<br />
                    • Extra-large fridges (90l +)<br />
                    • Modern, fresh decor<br />
                    • Fly screens<br />
                    • Tinted windows – provides privacy and reduces glare<br />
                    • Safe for valuables (excluding 2FT)<br />
                    • Gas heater (excluding 2FT)<br />
                    • TV/DVD (2FT DVD only)<br />
                    • Auxiliary plugs for I-pods<br />
                    • A/C in living area and cab (excluding 2FT)<br />
                    • Efficient Euro4 turbo diesel engines<br />
                    • Built by KEA in New Zealand for New Zealand &nbsp;&nbsp;conditions<br />
                    • All vehicles manufactured using environmentally-friendly &nbsp;&nbsp;alkorcell-covering
                    and sustainably sourced plywood to &nbsp;&nbsp;reduce the weight of campers and
                    fuel use, deforestation, &nbsp;&nbsp;and use of harmful toxins<br />
                    <p>
                        <br />
                    </p>
                </div>
                <div class="right-nav-info-box">
                </div>
            </div>
            <!-- sub page main content area end -->
            <div class="left-content-area">
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1" id="tab1" shape="rect">2 Berth Campervan</a> </li>
                        <li><a href="#tabs-2" id="tab2" shape="rect">Image Gallery</a> </li>
                        <li><a href="#tabs-3" id="tab3" shape="rect">Specifications</a> </li>
                        <li><a href="#tabs-4" id="tab4" shape="rect">Made in NZ</a> </li>
                    </ul>
                    <div id="tabs-1">
                        <h1>
                            KEA | 2 Berth Shower/Toilet Deluxe Campervan
                            <img alt="" src="http://nzrentals.keacampers.com/Images/vehicles/persontall.png" /><img alt="" src="http://nzrentals.keacampers.com/Images/vehicles/persontall.png" /></h1>
                        <h2>
                            Your comfortable studio on wheels – exploring at its best!</h2>
                        <img alt="" class="thumb" src="http://nzrentals.keacampers.com/images/vehicles/2st/2st-1-thumb.png" />&nbsp;&nbsp;
                        <img alt="" class="thumb" src="http://nzrentals.keacampers.com/images/vehicles/2st/2st-2-thumb.png" />&nbsp;&nbsp;
                        <img alt="" class="thumb" src="http://nzrentals.keacampers.com/images/vehicles/2st/2st-3-thumb.png" />&nbsp;&nbsp;
                        <img alt="" class="thumb" src="http://nzrentals.keacampers.com/images/vehicles/2st/2st-4-thumb.png" />&nbsp;&nbsp;
                        <img alt="" class="thumb" src="http://nzrentals.keacampers.com/images/vehicles/2st/2st-5-thumb.png" />&nbsp;&nbsp;
                        <img alt="" class="thumb" src="http://nzrentals.keacampers.com/images/vehicles/2st/2st-6-thumb.png" /><br />
                        <br />
                        <p>
                            This vehicle is all about luxury on 4 wheels, designed with all the features of
                            a full sized motorhome for maximum living in an easily navigable space. It offers
                            practical, easy living with multiple access options for great indoor/outdoor flow.
                            It’s the perfect adventurer’s vehicle – imagine coming home after a busy day of
                            activities to the aroma of freshly brewed coffee and surrounded by all the comforts
                            on the road. The luxurious interior has a cosy design to enjoy everything at your
                            fingertips with ample space to stretch and relax on your king size bed. Welcome
                            to the great indoors!</p>
                        <strong>Special features</strong>
                        <ul>
                            <li>Gas central heating – instant warmth on cooler days! </li>
                            <li>Shower/toilet ensuite – certified self-contained to allow you the freedom to camp
                                in our beautiful wide open spaces </li>
                            <li>Large water tank capacity (110l) </li>
                            <li>Fully equipped kitchen and microwave </li>
                            <li>1 king or 2 single beds – provides flexibility </li>
                            <li>Safe for valuables </li>
                            <li>A/C in living area and cab </li>
                            <li>TV/DVD </li>
                            <li>Powerful engine – 2.4l turbo diesel </li>
                        </ul>
                        <strong>Good for</strong><br />
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;✓ Fuel consumption<br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;✓ Easy driving, manoeuvrability and parking
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;✓ Compact<br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;✓ Warm and cosy<br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;✓ Simple but luxurious – no need to compromise<br />
                        <br />
                        <strong>Who books me?</strong>
                        <ul>
                            <li>Couples </li>
                            <li>Hikers &amp; explorers </li>
                            <li>Freedom campers </li>
                            <li>Friends (2 mates) </li>
                            <li>Mother/daughter or father/son </li>
                            <li>Honeymooners </li>
                            <li>Young executives </li>
                        </ul>
                        <br />
                    </div>
                    <div id="tabs-2">
                        <h2>
                            Interior Photo</h2>
                        <table style="background-color: #e6e6e6;">
                            <tbody>
                                <tr>
                                    <td style="padding-bottom: 5px; padding-left: 5px; padding-top: 5px;">
                                        <img alt="" class="thumb" src="http://nzrentals.keacampers.com/Images/vehicles/2st/2st-interior1-thumb.jpg" />
                                    </td>
                                    <td style="padding-bottom: 5px; padding-left: 5px; padding-top: 5px;">
                                        <img alt="" class="thumb" src="http://nzrentals.keacampers.com/Images/vehicles/2st/2st-interior2-thumb.jpg" />
                                    </td>
                                    <td style="padding-bottom: 5px; padding-left: 5px; padding-top: 5px;">
                                        <img alt="" class="thumb" src="http://nzrentals.keacampers.com/Images/vehicles/2st/2st-interior3-thumb.jpg" />
                                    </td>
                                    <td style="padding-bottom: 5px; padding-left: 5px; padding-top: 5px;">
                                        <img alt="" class="thumb" src="http://nzrentals.keacampers.com/Images/vehicles/2st/2st-interior4-thumb.jpg" />
                                    </td>
                                    <td style="padding-bottom: 5px; padding-left: 5px; padding-right: 5px; padding-top: 5px;">
                                        <img alt="" class="thumb" src="http://nzrentals.keacampers.com/Images/vehicles/2st/2st-interior5-thumb.jpg" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br />
                        <h2>
                            Exterior Photo</h2>
                        <table style="background-color: #e6e6e6;">
                            <tbody>
                                <tr>
                                    <td style="padding-bottom: 5px; padding-left: 5px; padding-top: 5px;">
                                        <img alt="" class="thumb" src="http://nzrentals.keacampers.com/Images/vehicles/2st/2st-exterior1-thumb.jpg" />
                                    </td>
                                    <td style="padding-bottom: 5px; padding-left: 5px; padding-top: 5px;">
                                        <img alt="" class="thumb" src="http://nzrentals.keacampers.com/Images/vehicles/2st/2st-exterior2-thumb.jpg" />
                                    </td>
                                    <td style="padding-bottom: 5px; padding-left: 5px; padding-top: 5px;">
                                        <img alt="" class="thumb" src="http://nzrentals.keacampers.com/Images/vehicles/2st/2st-exterior3-thumb.jpg" />
                                    </td>
                                    <td style="padding-bottom: 5px; padding-left: 5px; padding-top: 5px;">
                                        <img alt="" class="thumb" src="http://nzrentals.keacampers.com/Images/vehicles/2st/2st-exterior4-thumb.jpg" />
                                    </td>
                                    <td style="padding-bottom: 5px; padding-left: 5px; padding-right: 5px; padding-top: 5px;">
                                        <img alt="" class="thumb" src="http://nzrentals.keacampers.com/Images/vehicles/2st/2st-exterior5-thumb.jpg" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="tabs-3">
                        <div id="living-equipment">
                            <h2>
                                Living Equipment</h2>
                            <table border="0">
                                <tbody>
                                    <tr>
                                        <td>
                                            Pillows &amp; duvets
                                        </td>
                                        <td>
                                            Scissors
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Travel rug
                                        </td>
                                        <td>
                                            Tin opener
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Sheets &amp; towels
                                        </td>
                                        <td>
                                            Corkscrew
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Cooking utensils
                                        </td>
                                        <td>
                                            Gas lighter
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Pots/pans/frypan
                                        </td>
                                        <td>
                                            Coffee plunger
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Plates &amp; bowls
                                        </td>
                                        <td>
                                            Microwave cookware
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Cups &amp; glasses
                                        </td>
                                        <td>
                                            Food containers
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Wine glasses
                                        </td>
                                        <td>
                                            Chopping board
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Gas kettle
                                        </td>
                                        <td>
                                            Clothes hangers
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Electric kettle
                                        </td>
                                        <td>
                                            Clothes pegs
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Dish cloth &amp; brush
                                        </td>
                                        <td>
                                            Fire extinguisher
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Tea towels
                                        </td>
                                        <td>
                                            Fresh water hose
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Dishwashing liquid
                                        </td>
                                        <td>
                                            Waste water hose
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Bucket
                                        </td>
                                        <td>
                                            Toilet chemicals
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Dustpan &amp; brush
                                        </td>
                                        <td>
                                            Toilet paper roll
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Electric toaster
                                        </td>
                                        <td>
                                            Full LPG bottle
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Cutlery
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <p>
                                <strong>Note:</strong> You can request sleeping bags if preferred to duvets.</p>
                        </div>
                        <table id="veichle-specs-table" border="0">
                            <tbody>
                                <tr>
                                    <th>
                                        Vehicle
                                    </th>
                                    <td>
                                        Ford Transit 2.4 l intercooled turbo diesel, 6 speed manual, power steering
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Vehicle Safety
                                    </th>
                                    <td>
                                        ABS, dual air bags, central locking, certified self-contained
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Dimensions
                                    </th>
                                    <td>
                                        Length 5.9 m, Width 2.0 m, Height 2.7 m, Interior Height 1.83 m
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Seatbelts
                                    </th>
                                    <td>
                                        2
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Air Conditioning
                                    </th>
                                    <td>
                                        Driver cabin, living area (240V)
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Audio Visual
                                    </th>
                                    <td>
                                        AM/FM radio, CD player, TV*, DVD
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Beds (Extra Large)
                                    </th>
                                    <td>
                                        2.0m x 1.7m (or 2.0 m x 0.65 m/1.8 m x 0.8 m)
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Cooking
                                    </th>
                                    <td>
                                        2 burner gas hob, microwave
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Entrance
                                    </th>
                                    <td>
                                        Front / Side / Rear
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Fridge/Freezer
                                    </th>
                                    <td>
                                        90 L
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Fuel Consumption
                                    </th>
                                    <td>
                                        Approx 12 l / 100 km
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Gas Bottles
                                    </th>
                                    <td>
                                        1 x 4.5 kg
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Heating
                                    </th>
                                    <td>
                                        A/C (240V) and gas heater
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Power Supply
                                    </th>
                                    <td>
                                        240 V, 1 x 12 V house battery, solar power
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Extras
                                    </th>
                                    <td>
                                        Safe, fly screens, voltmeter, tank monitors, 240 V battery charger, macro hide fabric
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Shower / Toilet
                                    </th>
                                    <td>
                                        Shower with handbasin, cassette flush toilet
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Water
                                    </th>
                                    <td>
                                        Hot and cold pressurised (240 V/heat exchange)
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Water Tanks
                                    </th>
                                    <td>
                                        100 l fresh, 82 l waste
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="tabs-4">
                        <h2>
                            Proudly made by KEA in New Zealand</h2>
                        <img alt="factory photo 1" src="http://nzrentals.keacampers.com/images/about-kea/factory1.jpg" />
                        <img alt="factory photo 2" src="http://nzrentals.keacampers.com/images/about-kea/factory2.jpg" />
                        <img alt="factory photo 3" src="http://nzrentals.keacampers.com/images/about-kea/factory3.jpg" />
                        <p>
                            KEA campervans and motorhomes are proudly New Zealand designed and built. Since
                            the company started in 1995, our manufacturing plant, <a href="http://kea-manufacturing.co.nz"
                                name="KEA Manufacturing New Zealand">KEA Manufacturing</a> (KMC) has expanded
                            to include two factory premises, as well as shareholdings in a fibreglass and a
                            coach building company, all based in Auckland, New Zealand.</p>
                        <p>
                            Operating from ultra modern factories with state of the art computer design facilities
                            and computer-aided CNC machines, KMC constantly challenges traditional manufacturing
                            and design practices to produce campervans and motorhomes specifically built to
                            withstand New Zealand conditions, and setting the highest standards of innovation,
                            quality and consistency.</p>
                        <p>
                            Modern European kitchen design and high quality appliances, flatscreen TV and DVD
                            players, spacious toilet/shower cubicles, high-grade macro suede fabric seat covers,
                            central locking systems for the entire vehicle, safes, security screens and solar
                            panels are just some of the many innovative features to be found in a KEA campervan
                            or motorhome. KEA vehicles also feature the largest beds in the industry.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer start -->
        <!-- footer boxes start -->
        <div id="footer-boxes-wrapper">
            <div class="footer-box" id="choose-your-kea">
                <h2>
                    Why choose KEA</h2>
                <img alt="KEA Campers all inclusive Motorhome and Caravan holidays" src="http://nzrentals.keacampers.com/Images/Lesac Lake Wakatipu219w.jpg"
                    longdesc="KEA includes everything in their price for motorhome and caravan holidays in New Zealand" />
                <p>
                    With KEA it is all taken care of. We won't surprise you with any hidden costs either!&nbsp;
                    <a href="http://nzrentals.keacampers.com/en/about-kea/why-choose-kea.aspx" title="Why choose KEA">
                        Read more...</a></p>
            </div>
            <div class="footer-box">
                <h2>
                    KEA GREEN</h2>
                <div>
                    <img alt="KEA is dedicated to sustainability and protecting our precious environment"
                        src="http://nzrentals.keacampers.com/Images/KEAGreenthe silence219w.jpg" /><br />
                </div>
                <div>
                    KEA is dedicated to sustainability and protecting our precious environment. <a href="/en/about-kea/environment.aspx">
                        Read more...</a><br />
                </div>
            </div>
            <div class="footer-box" id="news-footer-box">
                <h1>
                    LATEST NEWS</h1>
                <ul class="sf_newsList">
                    <li>
                        <h2 class="sf_newsTitle">
                            <a id="ctl00_footerbox3_tm_NewsView1_ctl00_repeater_ctl01_fullContent1" href="../about-kea/news/individual-news/12-09-20/Let_s_go_Glamping.aspx">
                                Let's go Glamping! </a>
                        </h2>
                        <p class="sf_newsDate">
                        </p>
                        <p>
                            DEFINITION: A form of camping in which participants enjoy physical comforts associated
                            with more luxurious types of holiday – Shorthand for “Glamorous Camping”.
                        </p>
                        <p class="sf_readMore">
                        </p>
                        <p class="sf_newsAuthor">
                        </p>
                        <p>
                        </p>
                        <p class="sf_postCategory">
                        </p>
                        <p class="sf_postTags">
                        </p>
                    </li>
                </ul>
                <a>Read more...</a>
            </div>
            <div class="footer-box" id="last-footer-box">
                <h2>
                    Happy campers</h2>
                <img alt="" src="http://nzrentals.keacampers.com/images/editors-picks-footer-placer.jpg" />
                <p>
                    See what our customers say about us here!</p>
                <a href="/en/about-kea/happy-campers.aspx">Read more...</a>
            </div>
        </div>
        <!-- footer boxes end -->
        <div class="footer-content">
            <div class="footer-content-logos">
                <a href="http://nzrentals.keacampers.com/de/plan-your-trip/camping-grounds.aspx"
                    title="Wo kann ich campen?">
                    <img alt="Find out where to camp here" src="http://nzrentals.keacampers.com/Images/Freedom camping label100h.gif" /></a>&nbsp;<img
                        width="48" height="59" alt="Qualmark Enviro Gold Certified" src="http://nzrentals.keacampers.com/Images/Enviro Gold_50294 48 x 59.jpg" />
                <img alt="" src="http://nzrentals.keacampers.com/images/footer-tourism-logo.jpg" />
            </div>
            <div class="footer-content-links">
                <ul>
                    <li><a href="http://twitter.com/keacampers" class="foot-tweet" target="_blank">Follow
                        us on twitter</a> </li>
                    <li><a href="http://www.facebook.com/pages/KEA-Campers/131399783473?ref=ts" class="foot-face"
                        target="_blank">Connect with us on facebook</a> </li>
                    <li><a href="http://nzrentals.keacampers.com/en/about-kea/news.aspx" class="foot-news" target="_blank">KEA News</a>&nbsp;&nbsp;
                    </li>
                </ul>
                <p class="footer-links">
                    <span><a href="/en/Disclaimer_and_privacy.aspx">Disclaimer &amp; Privacy Policy</a></span>
                    <span><a href="http://keacampers.us1.list-manage.com/subscribe?u=5d4921319a35ba590377bab7f&amp;id=bd0fba3ea2"
                        target="_blank">E-Newsletter</a></span> <span><a href="/en/sitemap.aspx">Sitemap</a></span>
                    <span id="last"><a href="/en/Agents_area.aspx">Agents Area</a></span></p>
            </div>
        </div>
        <!-- footer end -->
    </div>
    <!-- wrapper end -->
    <script type="text/javascript">
//<![CDATA[
        SearchBox.Create('../default/search-result.aspx', 'AllWords', 'KEA_RENTALS_NZ', 'ctl00_searchbox_tm_SearchBox1_ctl00_ctl00_queryText', 'ctl00_searchbox_tm_SearchBox1_ctl00_ctl00_searchButton'); Sys.Application.initialize();
        Sys.Application.add_init(function () {
            $create(Telerik.Web.UI.RadMenu, { "_childListElementCssClass": null, "_skin": "Default", "clientStateFieldID": "ctl00_menu_tm_sitefinity_usercontrols_navigation35_sitemenu_ascx1_RadMenu1_ClientState", "collapseAnimation": "{\"type\":0,\"duration\":0}", "collapseDelay": 0, "expandAnimation": "{\"type\":0,\"duration\":0}", "expandDelay": 0, "itemData": [{ "items": [{ "value": "Why choose KEA?", "navigateUrl": "~/en/about-kea/why-choose-kea.aspx" }, { "value": "The KEA story", "navigateUrl": "~/en/about-kea/the-kea-story.aspx" }, { "value": "KEA Green", "navigateUrl": "~/en/about-kea/environment.aspx" }, { "value": "News", "navigateUrl": "~/en/about-kea/news.aspx" }, { "items": [{ "value": "Adventure in the snow", "navigateUrl": "~/en/about-kea/happy-campers/adventure-in-the-snow.aspx"}], "value": "Happy campers", "navigateUrl": "~/en/about-kea/happy-campers.aspx"}], "value": "About KEA", "navigateUrl": "~/en/about-kea.aspx" }, { "items": [{ "value": "Vehicles", "navigateUrl": "~/en/vehicles/vehicles.aspx" }, { "value": "2 + 2 Berth Flip-Top", "navigateUrl": "~/en/vehicles/2-plus-2-berth-flip-top.aspx" }, { "value": "2 Berth Campervan", "navigateUrl": "~/en/vehicles/2-berth-campervan.aspx", "cssClass": "rmFocused" }, { "value": "3 Berth Origin", "navigateUrl": "~/en/vehicles/origin.aspx" }, { "value": "4 Berth Motorhome", "navigateUrl": "~/en/vehicles/4-berth-motorhome.aspx" }, { "value": "6 Berth Motorhome", "navigateUrl": "~/en/vehicles/6-berth-motorhome.aspx" }, { "value": "Living equipment", "navigateUrl": "~/en/vehicles/living-equipment.aspx" }, { "value": "Buy a KEA", "navigateUrl": "http://nzsales.keacampers.com" }, { "value": "Made in New Zealand", "navigateUrl": "~/en/vehicles/made-in-nz.aspx"}], "value": "Vehicles", "navigateUrl": "~/en/vehicles/vehicles.aspx", "cssClass": "rmFocused" }, { "items": [{ "value": "Photo competition", "navigateUrl": "~/en/KEA_specials/photo-competition.aspx"}], "value": "KEA specials", "navigateUrl": "~/en/KEA_specials.aspx" }, { "items": [{ "value": "About New Zealand", "navigateUrl": "~/en/plan-your-trip/about-new-zealand.aspx" }, { "value": "Where can I camp?", "navigateUrl": "~/en/plan-your-trip/camping-grounds.aspx" }, { "items": [{ "value": "8 Day Northland Itinerary", "navigateUrl": "~/en/plan-your-trip/itineraries/8-day-northland-itinerary.aspx" }, { "value": "11 Day South Island Itinerary", "navigateUrl": "~/en/plan-your-trip/itineraries/11-day-south-island-itinerary.aspx"}], "value": "Itineraries", "navigateUrl": "~/en/plan-your-trip/itineraries.aspx" }, { "items": [{ "value": "Kids\u0027 corner", "navigateUrl": "~/en/plan-your-trip/travelling-with-kids/kids-corner.aspx"}], "value": "Travelling with kids", "navigateUrl": "~/en/plan-your-trip/travelling-with-kids.aspx" }, { "items": [{ "value": "Arrival info", "navigateUrl": "~/en/plan-your-trip/Travel_info/arrival-information.aspx" }, { "value": "Driving info", "navigateUrl": "~/en/plan-your-trip/Travel_info/driving-info.aspx" }, { "value": "Ferry info", "navigateUrl": "~/en/plan-your-trip/Travel_info/ferry-information.aspx" }, { "value": "Distances", "navigateUrl": "~/en/plan-your-trip/Travel_info/distances.aspx" }, { "value": "What to bring", "navigateUrl": "~/en/plan-your-trip/Travel_info/what-to-bring.aspx" }, { "value": "Keeping safe", "navigateUrl": "~/en/plan-your-trip/Travel_info/keeping-safe.aspx"}], "value": "Travel info", "navigateUrl": "~/en/plan-your-trip/Travel_info.aspx" }, { "value": "KEA Nests", "navigateUrl": "~/en/plan-your-trip/KEA_Nests.aspx"}], "value": "Plan Your Trip", "navigateUrl": "~/en/plan-your-trip.aspx" }, { "items": [{ "value": "Insurance options", "navigateUrl": "~/en/rental-info/insurance_options.aspx" }, { "value": "Online pre check-in", "navigateUrl": "https://secure.tpl-services.com/modules/precheckin/login.aspx?siteID=b8e7b797-7543-4901-858c-c8e29d166504" }, { "value": "Optional extras", "navigateUrl": "~/en/rental-info/optional-extras.aspx" }, { "value": "FAQ", "navigateUrl": "~/en/rental-info/faq.aspx" }, { "value": "Terms & conditions", "navigateUrl": "~/en/rental-info/terms-and-conditions.aspx"}], "value": "Rental Info", "navigateUrl": "~/en/rental-info.aspx" }, { "items": [{ "value": "Contact us", "navigateUrl": "~/en/contact/contact_us.aspx" }, { "value": "Auckland depot", "navigateUrl": "~/en/contact/auckland-depot.aspx" }, { "value": "Christchurch depot", "navigateUrl": "~/en/contact/christchurch-depot.aspx" }, { "value": "KEA worldwide", "navigateUrl": "~/en/contact/KEA_worldwide.aspx"}], "value": "Contact", "navigateUrl": "~/en/contact.aspx"}] }, null, null, $get("ctl00_menu_tm_sitefinity_usercontrols_navigation35_sitemenu_ascx1_RadMenu1"));
        });
//]]>
    </script>
    </form>
    <script language="javascript" type="text/javascript" src="http://nzrentals.keacampers.com/bumpbox/bumpbox.js"></script>
</body>
</html>
