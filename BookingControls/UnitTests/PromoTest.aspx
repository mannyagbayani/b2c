﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PromoTest.aspx.cs" Inherits="UnitTests_PromoTest" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Selection Catalog Page</title>    
    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
    <link href="http://images.thl.com/css/Base.css?v=2.1" type="text/css" rel="stylesheet" />
    <link href="http://images.thl.com/css/<%= Brand %>/selection.css?v=2.1" type="text/css" rel="stylesheet" /> 
    <link rel="stylesheet" type="text/css" media="all" href="http://images.thl.com/css/<%= Brand %>/horizontal.css" />   
    <script src="http://test.maui.co.nz/js/jquery-1.3.1.min.js" type="text/javascript"></script>
    <script src="http://test.maui.co.nz/js/jquery.dimensions.min.js" type="text/javascript"></script>
    <script src="http://test.maui.co.nz/js/jquery.tooltip.min.js" type="text/javascript"></script>
    <script src="http://test.maui.co.nz/js/date.js" type="text/javascript"></script>    
    <script src="http://test.maui.co.nz/js/hCtrl.js?v=1.9" type="text/javascript"></script>         
    <script src="http://test.maui.co.nz/js/selection.js?v=2.1" type="text/javascript"></script>
    <script src="http://test.maui.co.nz/js/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <link href="http://images.thl.com/css/<%= Brand %>/jcal.css" type="text/css" rel="stylesheet" />
    <script src="/js/ui.core.js" type="text/javascript"></script>
    <script src="/js/ui.carousel.js" type="text/javascript"></script>
    <script type="text/javascript">
        var ContentbaseUrl = 'http://local.maui.co.nz//';
        var assetsBase = 'http://images.thl.com';
        var affCode = '';
        var crossSellUrl = 'http://local.motorhomesandcars.com/bookingcontrols';
        var brandId = 'm';
        var ctrlParams = { pb: '', db: '', cr: '', cc: 'NONE', rf: '', vt: 'av', vc: '', pd: 'dd-mmm-yyyy', pt: '00:00', dd: 'dd-mmm-yyyy', dt: '00:00', na: '0', nc: '0', pb: '', db: '', com: '', brand: '', debug: '' };
        var requestParams = { pb: 'akl', db: 'akl', cr: '', cc: 'NZ', rf: '', vt: 'av', vc: '', pd: '22-Aug-2012', pt: '10:00', dd: '30-Aug-2012', dt: '15:00', na: '1', nc: '0', pb: 'AKL', db: 'AKL', com: '', brand: 'm', debug: '' };
        var currencyStr = 'NZD';
        var defaultHours = { avOpen: '09:30', avClose: '17:30', acOpen: '10:30', acClose: '11:30' };
        var $j = jQuery.noConflict();
        jQuery(document).ready(function () {
            initSelectionPage();
            //resubmit the form if browser back is hit
            if (getCookie('history') == 1) {
                deleteCookie('history', '', '');
                $j('#vehicleCatalogContainer').hide();
                $j('#staticLoader').show();
                setTimeout("document.forms[0].submit()", 1000);
            }
            t = window.setTimeout(function () {
                if ($j("#staticLoader").is(':visible')) {
                    $j("#carousel").carousel({
                        animateByMouse: false,
                        pausable: true,
                        radius: 250,
                        animate: 0.004,
                        pauseSpeed: 0
                    });
                }
                else {
                    window.clearTimeout(t);
                }
            }, $j.browser.safari ? 100 : 0);

        });

    </script>     
                
</head>
<body class="selection">
    <form name="form1" method="post" action="Selection.aspx?__utma=1.75905084.1320798339.1343962369.1344995084.35&amp;__utmz=1.1340757179.30.2.utmcsr%3dlocal.motorhomesandcars.com%7cutmccn%3d(referral)%7cutmcmd%3dreferral%7cutmcct%3d%2fSelection.aspx&amp;__utmc=1&amp;SID=prfqtmj4pybnk4j4lumbmy45&amp;pv=1.0" id="form1">
<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTExMjk3MDU2MjZkZPAW/+npPgzjqyrR/lo50AizVLm4" />
</div>
       
        <div id="thlCrossSellBanner">
                
<!-- Start CrossSell Header, TODO: swap to Web Control -->	            
<div id="crossSell">     			
    <ul class="crossSellNav">            					
        <li><span class="logo" id="Span1">New Zealand</span></li>
        <li class="maui"><a href="http://www.maui.co.nz" class='current'>Maui</a></li>
        <li class="britz"><a href="http://www.britz.co.nz" >Britz</a></li>
        <li class="BP"><a href="http://www.backpackercampervans.co.nz" >Backpacker</a></li>						            
        <li class="EX"><a href="http://www.exploremore.co.nz" >Explore More</a></li>						            
    </ul>
    <div id="crossSellCountry">
        <ul class="crossSellNav">
            <li>
	            <span class="country">Australia </span>
	            <ul class="crossSellNavDD">	                
		            <li class="maui"><a href="http://www.maui.com.au">Maui</a></li>
		            <li class="britz"><a href="http://www.britz.com.au">Britz</a></li>
		            <li class="BP"><a href="http://www.backpackercampervans.com.au">Backpacker</a></li>
		            <li class="EX"><a href="http://www.exploremore.com.au">Explore More</a></li>
	            </ul>
            </li>
        </ul>
        <ul class="crossSellNav">
				<li>
					<span class="country">USA</span>
					<ul class="crossSellNavDD">
						<li class="RBC"><a href="http://www.roadbearrv.com">Road Bear RV</a></li>

					</ul>
				</li>
		</ul>
    </div>            	
</div>
<!-- End CrossSell Header -->
    
        </div>
        <div id="vehicleSelectionContainer" class="NZ">
            <div id="catalogHeader">
                <span class="Image">
                    <a href="http://www.maui.co.nz">
                        <img src="http://images.thl.com/images/<%= Brand %>/logo.jpg" alt="logo" />
                    </a>
                </span>
                <a class="priceMatch" href="javascript:openPriceMatchWindow();">Price match guarantee</a>
                <span class="ContactInfo">
                    Call us now - 24 hours a day, 7 days a week<br />Call within NZ Toll Free: 0800 651 0800 - Worldwide Toll Free: +800 200 80 801 <a href="http://www.maui.co.nz/contactus" target="_blank" >contact us</a>
                </span>                
            </div>
            <div id="bookingControlContainer">
                <div id="sslSeal">
                    <script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=secure.maui.co.nz&amp;size=L&amp;lang=en"></script>
                </div>
                
<div id="HorizontalControlContainer">
    <script type="text/javascript">
        var bookingControlParams = { pb: '', db: '', cr: '', cc: 'NONE', rf: '', vt: 'av', vc: '', pd: 'dd-mmm-yyyy', pt: '00:00', dd: 'dd-mmm-yyyy', dt: '00:00', na: '0', nc: '0', pb: '', db: '', com: '', brand: '', debug: '' };
        var ContentbaseUrl = 'http://a1243/bookingcontrols';
        var MAX_PASSENGERS = 6;
        var psid = 'prfqtmj4pybnk4j4lumbmy45';
        var noa = 0, noc = 0;
        var $j = jQuery.noConflict();
        jQuery(document).ready(function () {
            initControl();
            var options = { height: 24, imgUrl: "/images/tiles/slow-loader.png", btnClass: "searchBtn", spinnerClass: "spinner" }
            bindSpinner(options);
        });         
    </script>
    <div id="bookingControl" class="  ">
        <!-- Horizontal Booking Control -->
        <div id="leftCol" class="CtrlCol">
            <span class="Radios ">
                <label for="av">
                    Motorhome</label>
                <input type="radio" id="avRadio" value="av" name="vt" />
                <label for="av">
                    Car</label>
                <input type="radio" id="acRadio" value="ac" name="vt" />
                <input type="hidden" id="radiosSwapped" name="radiosSwapped" />
            </span>
            <span class="required">Pick up</span>
            <span class="required"> Drop off</span>
            <span class="required CofR"> Drivers Licence</span>
        </div>
        <div id="centreCol" class="CtrlCol">
            <span class="Model"><small>Model:</small>
                <select name='ctrlSearchBox_dropVehicleType' id='ctrlSearchBox_dropVehicleType' class='SearchBox_FullListBox'><option value=''>Search all</option><option value='nzavm.2BTSM' >Ultima</option><option value='nzavm.4BMP' >Platinum Beach</option><option value='nzavm.6BMPC' >Platinum River</option></select>
            </span><span class="Splash">
                <select id='countryCode' name='countryCode'><option value=''   >Select Country of Travel</option><option  value='AU'>Australia</option><option selected='selected' value='NZ'>New Zealand</option></select>
            </span><span class="Company">
                <input type='hidden' id='ctrlSearchBox_dropVendor' value='m' />
            </span><span class="TimeSpan">
                <input size="15" id="ctrlSearchBox_calPickUp_DateText" name="ctrlSearchBox_calPickUp_DateText"
                    value="22-Aug-2012" type="text" autocomplete="off" />
                <select class="SearchBox_TimeListBox" id="ctrlSearchBox_dropPickUpTime" name="ctrlSearchBox_dropPickUpTime"><option value="08:00">08:00am</option><option value="08:30">08:30am</option><option value="09:00">09:00am</option><option value="09:30">09:30am</option><option value="10:00" selected="selected">10:00am</option><option value="10:30">10:30am</option><option value="11:00">11:00am</option><option value="11:30">11:30am</option><option value="12:00">12:00pm</option><option value="12:30">12:30pm</option><option value="13:00">13:00pm</option><option value="13:30">13:30pm</option><option value="14:00">14:00pm</option><option value="14:30">14:30pm</option><option value="15:00">15:00pm</option><option value="15:30">15:30pm</option><option value="16:00">16:00pm</option><option value="16:30">16:30pm</option><option value="17:00">17:00pm</option></select>
            </span><span class="TimeSpan">
                <input size="15" id="ctrlSearchBox_calDropOff_DateText" name="ctrlSearchBox_calDropOff_DateText"
                    value="30-Aug-2012" type="text" autocomplete="off" />
                <select class="SearchBox_TimeListBox" id="ctrlSearchBox_dropDropOffTime" name="ctrlSearchBox_dropDropOffTime"><option value="08:00">08:00am</option><option value="08:30">08:30am</option><option value="09:00">09:00am</option><option value="09:30">09:30am</option><option value="10:00">10:00am</option><option value="10:30">10:30am</option><option value="11:00">11:00am</option><option value="11:30">11:30am</option><option value="12:00">12:00pm</option><option value="12:30">12:30pm</option><option value="13:00">13:00pm</option><option value="13:30">13:30pm</option><option value="14:00">14:00pm</option><option value="14:30">14:30pm</option><option value="15:00" selected="selected">15:00pm</option><option value="15:30">15:30pm</option><option value="16:00">16:00pm</option><option value="16:30">16:30pm</option><option value="17:00">17:00pm</option></select>
            </span>
            <span class="TimeSpan CofR">
                <select id="ctrlSearchBox_dropCountryOfResidence">
                    <option value="0" selected="selected">Please select</option>
                    <option value="AU">Australia</option>
                    <option value="CA">Canada</option>
                    <option value="DK">Denmark</option>
                    <option value="FR">France</option>
                    <option value="DE">Germany</option>
                    <option value="NL">Netherlands</option>
                    <option value="NZ">New Zealand</option>
                    <option value="SE">Sweden</option>
                    <option value="CH">Switzerland</option>
                    <option value="UK">United Kingdom</option>
                    <option value="US">United States Of America</option>
                    <option value="-" disabled="disabled">--------------------</option>
                    <option value="AF">Afghanistan</option>
                    <option value="AL">Albania</option>
                    <option value="DZ">Algeria</option>
                    <option value="AS">American Samoa</option>
                    <option value="AD">Andora</option>
                    <option value="AO">Angola</option>
                    <option value="AI">Anguilla</option>
                    <option value="AQ">Antarctica</option>
                    <option value="AG">Antigua & Barbados</option>
                    <option value="AR">Argentina</option>
                    <option value="AM">Armenia</option>
                    <option value="AW">Aruba</option>
                    <option value="AT">Austria</option>
                    <option value="AZ">Azerbaijan</option>
                    <option value="BS">Bahamas</option>
                    <option value="BH">Bahrain</option>
                    <option value="BD">Bangladesh</option>
                    <option value="BB">Barbados</option>
                    <option value="BY">Belarus</option>
                    <option value="BE">Belgium</option>
                    <option value="BZ">Belize</option>
                    <option value="BJ">Benin</option>
                    <option value="BM">Bermuda</option>
                    <option value="BO">Bolivia</option>
                    <option value="BA">Bosnia and Herzegovina</option>
                    <option value="BW">Botswana</option>
                    <option value="BV">Bouvet Island</option>
                    <option value="BR">Brazil</option>
                    <option value="IO">British Indian Ocean Territory</option>
                    <option value="BN">Brunei</option>
                    <option value="BG">Bulgaria</option>
                    <option value="BF">Burkina Faso</option>
                    <option value="BI">Burundi</option>
                    <option value="BT">Butan</option>
                    <option value="KH">Cambodia</option>
                    <option value="CM">Cameroon</option>
                    <option value="CV">Cape Verde</option>
                    <option value="KY">Cayman Islands</option>
                    <option value="CF">Cental African Republic</option>
                    <option value="TD">Chad</option>
                    <option value="CL">Chile</option>
                    <option value="CN">China</option>
                    <option value="CX">Christmas Island</option>
                    <option value="CC">Cocos (Keeling) Island</option>
                    <option value="CO">Colombia</option>
                    <option value="KM">Comoros</option>
                    <option value="CG">Congo</option>
                    <option value="CK">Cook Island</option>
                    <option value="CR">Costa Rica</option>
                    <option value="CI">Cote D'ivoire</option>
                    <option value="HR">Croatia</option>
                    <option value="CU">Cuba</option>
                    <option value="CY">Cyprus</option>
                    <option value="CZ">Czech Republic</option>
                    <option value="DJ">DjiboutiI</option>
                    <option value="DM">Dominica</option>
                    <option value="DO">Dominican Republic</option>
                    <option value="TP">East Timor</option>
                    <option value="EC">Ecuador</option>
                    <option value="EG">Egypt</option>
                    <option value="SV">El Salvador</option>
                    <option value="GQ">Equatorial Guinea</option>
                    <option value="ER">Eritrea</option>
                    <option value="EE">Estonia</option>
                    <option value="ET">Ethiopia</option>
                    <option value="FK">Falkland Islands</option>
                    <option value="FO">Faroe Islands</option>
                    <option value="FJ">Fiji</option>
                    <option value="FI">Finland</option>
                    <option value="FX">France, Metropolitan</option>
                    <option value="GF">French Guiana</option>
                    <option value="PF">French Polynesia</option>
                    <option value="TF">French Souther Territories</option>
                    <option value="GA">Gabon</option>
                    <option value="GM">Gambia</option>
                    <option value="GE">Georgia</option>
                    <option value="GH">Ghana</option>
                    <option value="GI">Gibraltar</option>
                    <option value="GR">Greece</option>
                    <option value="GL">Greenland</option>
                    <option value="GD">Grenada</option>
                    <option value="GP">Guadeloupe</option>
                    <option value="GU">Guam</option>
                    <option value="GT">Guatemala</option>
                    <option value="GN">Guinea</option>
                    <option value="GW">Guinea-Bissau</option>
                    <option value="GY">Guyana</option>
                    <option value="HT">Haiti</option>
                    <option value="HM">Heard and McDonald Islands</option>
                    <option value="HN">Honduras</option>
                    <option value="HK">Hong Kong</option>
                    <option value="HU">Hungary</option>
                    <option value="IS">Iceland</option>
                    <option value="IN">India</option>
                    <option value="ID">Indonesia</option>
                    <option value="IR">Iran</option>
                    <option value="IQ">Iraq</option>
                    <option value="IE">Ireland</option>
                    <option value="IL">Israel</option>
                    <option value="IT">Italy</option>
                    <option value="JM">Jamaica</option>
                    <option value="JP">Japan</option>
                    <option value="JO">Jordan</option>
                    <option value="KZ">Kazakhstan</option>
                    <option value="KE">Kenya</option>
                    <option value="KI">Kiribati</option>
                    <option value="KW">Kuwait</option>
                    <option value="KG">Kyrgyzstan</option>
                    <option value="LA">Lao Peoples Democratic Republic</option>
                    <option value="LV">Latvia</option>
                    <option value="LB">Lebanon</option>
                    <option value="LS">Lesotho</option>
                    <option value="LR">Liberia</option>
                    <option value="LY">Libya</option>
                    <option value="LI">Liechtenstein</option>
                    <option value="LT">Lithuania</option>
                    <option value="LU">Luxembourg</option>
                    <option value="MO">Macau</option>
                    <option value="MK">Macedonia</option>
                    <option value="MG">Madagascar</option>
                    <option value="MW">Malawi</option>
                    <option value="MY">Malaysia</option>
                    <option value="MV">Maldives</option>
                    <option value="ML">Mali</option>
                    <option value="MT">Malta</option>
                    <option value="MH">Marshall Islands</option>
                    <option value="MQ">Martinique</option>
                    <option value="MR">Mauritania</option>
                    <option value="MU">Mauritius</option>
                    <option value="YT">Mayotte</option>
                    <option value="MX">Mexico</option>
                    <option value="FM">Micronesia, Federated States of</option>
                    <option value="MD">Moldova</option>
                    <option value="MC">Monaco</option>
                    <option value="MN">Mongolia</option>
                    <option value="MS">Montserrat</option>
                    <option value="MA">Morocco</option>
                    <option value="MZ">Mozambique</option>
                    <option value="MM">Myanamar</option>
                    <option value="NA">Namibia</option>
                    <option value="NR">Nauru</option>
                    <option value="NP">Nepal</option>
                    <option value="AN">Netherlands Antilles</option>
                    <option value="NC">New Caledonia</option>
                    <option value="NI">Nicaragua</option>
                    <option value="NE">Niger</option>
                    <option value="NG">Nigeria</option>
                    <option value="NU">Niue</option>
                    <option value="NF">Norfolk Island</option>
                    <option value="KP">North Korea</option>
                    <option value="MP">Northern Mariana Islands</option>
                    <option value="NO">Norway</option>
                    <option value="OM">Oman</option>
                    <option value="PK">Pakistan</option>
                    <option value="PW">Palau</option>
                    <option value="PA">Panama</option>
                    <option value="PG">Papua New Guinea</option>
                    <option value="PY">Paraguay</option>
                    <option value="PE">Peru</option>
                    <option value="PH">Philippines</option>
                    <option value="PN">Pitcairn Islands</option>
                    <option value="PL">Poland</option>
                    <option value="PT">Portugal</option>
                    <option value="PR">Puerto Rico</option>
                    <option value="QA">Qatar</option>
                    <option value="RE">Reunion</option>
                    <option value="RO">Romania</option>
                    <option value="RU">Russian Federation</option>
                    <option value="RW">Rwanda</option>
                    <option value="SH">Saint Helena</option>
                    <option value="KN">Saint Kitts and Nevis</option>
                    <option value="LC">Saint Lucia</option>
                    <option value="PM">Saint Pierre and Miquelon</option>
                    <option value="VC">Saint Vincent and The Grenadines</option>
                    <option value="WS">Samoa</option>
                    <option value="SM">San Marino</option>
                    <option value="ST">Sao Tome and Principe</option>
                    <option value="SA">Saudi Arabia</option>
                    <option value="SN">Senegal</option>
                    <option value="SC">Seychelles</option>
                    <option value="SG">Singapore</option>
                    <option value="SK">Slovakia</option>
                    <option value="SI">Slovenia</option>
                    <option value="SB">Solomon Islands</option>
                    <option value="SO">Somalia</option>
                    <option value="ZA">South Africa</option>
                    <option value="GS">South Georgia</option>
                    <option value="KR">South Korea</option>
                    <option value="ES">Spain</option>
                    <option value="SL">Sri Lanka</option>
                    <option value="SD">Sudan</option>
                    <option value="SR">Suriname</option>
                    <option value="SJ">Svalbardand Jan Mayen Islands</option>
                    <option value="SY">Syria</option>
                    <option value="TI">Tahiti</option>
                    <option value="TW">Taiwan</option>
                    <option value="TJ">Tajikistan</option>
                    <option value="TZ">Tanzania</option>
                    <option value="TH">Thailand</option>
                    <option value="TG">Togo</option>
                    <option value="TK">Tokelau</option>
                    <option value="TO">Tonga</option>
                    <option value="TT">Trinidad and Tobago</option>
                    <option value="TN">Tunisia</option>
                    <option value="TR">Turkey</option>
                    <option value="TM">Turkmenistan</option>
                    <option value="TC">Turks and Caicos Islands</option>
                    <option value="TV">Tuvalu</option>
                    <option value="UG">Uganda</option>
                    <option value="UA">Ukraine</option>
                    <option value="AE">United Arab Emirates</option>
                    <option value="UY">Uruguay</option>
                    <option value="UM">US Minor Outlying Islands</option>
                    <option value="UZ">Uzbekistan</option>
                    <option value="VU">Vanuatu</option>
                    <option value="VA">Vatican City</option>
                    <option value="VE">Venezuala</option>
                    <option value="VN">Vietnam</option>
                    <option value="VG">Virgin Islands (GB)</option>
                    <option value="VI">Virgin Islands (US)</option>
                    <option value="WF">Wallis and Futuna Islands</option>
                    <option value="EH">Western Sahara</option>
                    <option value="YE">Yemen</option>
                    <option value="YU">Yugoslavia</option>
                    <option value="ZR">Zaire</option>
                    <option value="ZM">Zambia</option>
                    <option value="ZW">Zimbabwe</option>
                </select>
            </span>
        </div>
        <div id="rightCol" class="CtrlCol">
            <span class="Passengers"><span class="LeftPass">
                <select name='ctrlSearchBox_dropChildren' id='ctrlSearchBox_dropChildren' class='SearchBox_AdultsListBox'><option selected='selected' value='0'>Children</option><option value='1'>1 Child</option><option value='2'>2 Children</option><option value='3'>3 Children</option><option value='4'>4 Children</option><option value='5'>5 Children</option></select>
            </span><span class="RightPass">
                <select name='ctrlSearchBox_dropAdults' id='ctrlSearchBox_dropAdults' class='SearchBox_AdultsListBox'><option selected='selected' value='1'>Adults</option><option value='1'>1 Adult</option><option value='2'>2 Adults</option><option value='3'>3 Adults</option><option value='4'>4 Adults</option><option value='5'>5 Adults</option><option value='6'>6 Adults</option></select>
            </span></span><span id="bottomRight">
            <span id="locationsPanel">
                <select name="ctrlSearchBox_dropPickUpLocation" id="ctrlSearchBox_dropPickUpLocation" class="SearchBox_FullListBox LocationDD"><option value="AKL">Auckland airport</option><option value="CHC">Christchurch airport</option><option value="ZQN">Queenstown airport</option></select>
                <select name="ctrlSearchBox_dropDropOffLocation" id="ctrlSearchBox_dropDropOffLocation" class="SearchBox_FullListBox LocationDD"><option value="AKL">Auckland airport</option><option value="CHC">Christchurch airport</option><option value="ZQN">Queenstown airport</option></select>
             </span>
            <a href="javascript:submitControl('','get')" class="searchBtn">Search</a> 
             </span>
            <span id="ctrlValidationMsg">= Required</span> 
            <input type="hidden" name="controlTarget" id="controlTarget" value="" />
            <input type="hidden" name="controlBrand" id="controlBrand" value="m" />
            <input type="hidden" name="vehicleType" id="vehicleType" value="av" />
            <input type="hidden" name="countryOfResidence" id="countryOfResidence" value="" />
            <input type="hidden" name="pickUpLoc" id="pickUpLoc" value="" />
            <input type="hidden" name="dropOffLoc" id="dropOffLoc" value="" />
            <input type="hidden" name="vehicleCode" id="vehicleCode" value="" />
            <input type="hidden" name="contryVehicleAvailability" id="contryVehicleAvailability"
                value="Both" />
        </div>
        <div class="botCol">
            <div class="crossSell">
                <span class="CSellLbl">Campervan brand</span>
                <select class="CSellOpt">
                    <option value="brand">
                        Maui</option>
                    <option value="all">All campervan brands</option>
                </select>
                <span class="crossSellBrand"></span>
            </div>
            <a href="javascript:submitControl('','get')" class="searchBtn">Search</a>
            <span class="spinner"></span>
        </div>
    </div>   
</div>

            </div>
            <div id="breadCrumbs">
                <span class="Crumbs">
                    <a href="http://www.maui.co.nz" class="HomeLnk">Home</a> &gt; Select your vehicle
                </span>
                <ul id="stepsDisplayer">
                    <li>
                        <span id="PaymentStep">3.Payment</span> 
                    </li>
                    <li>
                        <span id="ConfigureStep">2.Configure</span>
                    </li>
                    <li>
                        <span id="SelectStep">1.Select</span>
                    </li>
                </ul>
            </div>
            <div id="vehicleCatalogContainer">
            <h2>1. Select your vehicle</h2>
            <div id="resultsHeader" >
                <h3>Search results below</h3>
                <span class="CurrencyPanel">
                    <small>Alternative Currency:</small>
                    <select id='currencySelector'><option selected='selected' value='NZD' rel='1.0'>New Zealand Dollar (NZD)</option><option  value='USD' rel='0.7308'>United States Dollar (USD)</option><option  value='GBP' rel='0.4415'>Pound Sterling (GBP)</option><option  value='AUD' rel='0.7989'>Australian Dollar (AUD)</option><option  value='EUR' rel='0.4927'>Euro (EUR)</option><option  value='CAD' rel='0.7808'>Canadian Dollar (CAD)</option><option  value='XPF' rel='58.7755'>C.F.P. Franc (XPF)</option><option  value='DKK' rel='3.6671'>Danish Krone (DKK)</option><option  value='FJD' rel='1.3949'>Fijian Dollar (FJD)</option><option  value='HKD' rel='5.6665'>Hong Kong Dollar (HKD)</option><option  value='INR' rel='34.3581'>Indian Rupee (INR)</option><option  value='JPY' rel='66.8471'>Japanese Yen (JPY)</option><option  value='NOK' rel='4.1235'>Norwegian Krone (NOK)</option><option  value='PKR' rel='999999.9049'>Pakistan Rupee (PKR)</option><option  value='PGK' rel='1.8653'>Papua New Guinean Kina (PGK)</option><option  value='PHP' rel='34.9065'>Philippine Peso (PHP)</option><option  value='SGD' rel='1.0217'>Singapore Dollar (SGD)</option><option  value='SBD' rel='5.8073'>Solomon Islands Dollar (SBD)</option><option  value='ZAR' rel='5.6338'>South African Rand (ZAR)</option><option  value='LKR' rel='83.7538'>Sri Lankan Rupee (LKR)</option><option  value='SEK' rel='5.0953'>Swedish Krona (SEK)</option><option  value='CHF' rel='0.7448'>Swiss Franc (CHF)</option><option  value='THB' rel='24.2742'>Thai Baht (THB)</option><option  value='TOP' rel='1.361'>Tongan Pa'anga (TOP)</option><option  value='VUV' rel='70.3095'>Vanuatu Vatu (VUV)</option><option  value='WST' rel='1.8478'>Western Samoan Tala (WST)</option></select>
                </span>
            </div>
                <ul id='vehicleCatalog'>
                    <li class='Collapse'>
                        <div class='VehicleItem'>
                            <img src='http://images.thl.com/images/<%= Brand %>/icons/rowLogo.gif' title='Maui' class='RowLogo PopUp' /><a
                                href='http://www.maui.co.nz/motorhome-hire/platinum-campervans-ultima?popup'
                                class='VehicleThumb PopUp'><img src='http://images.thl.com/CentralLibraryImages/Maui/New-Zealand/Campervans/NZavm.2BTSM/NZavm.2BTSM-ClearCut-72.jpg'
                                    border='0' title='2BTSM' /></a><div class='VehicleFeatures'>
                                        <a href='http://www.maui.co.nz/motorhome-hire/platinum-campervans-ultima?popup' class='PopUp'>
                                            Vehicle Name</a><img src='http://images.thl.com/CentralLibraryImages/Maui/New-Zealand/Campervans/NZavm.2BTSM/NZavm.2BTSM-InclusionIcons.gif'
                                                border='0' title='Maui' /></div>
                            <div class='PriceDescription'>
                                <a href='Configure.aspx?avp=EA6DB2C5D7C04792BFF9628A8D03EB88' class='Avail'>AVAILABLE
                                    NOW</a>
                                <span class="promo">
                                    <big><%= PromoHeader%></big>
                                    <%= PromoText %>
                                </span>
                            </div>
                            <div class='TotalPrice' title='WINNERS'>
                                <ul class='Prices'>
                                    <li><span>NZD 648.00</span></li><li class='Fp Hide NZD'><span>NZD $ 648.00</span></li><li
                                        class='Fp Hide USD'><span>USD $ 473.56</span></li><li class='Fp Hide GBP'><span>GBP
                                            £ 286.09</span></li><li class='Fp Hide AUD'><span>AUD $ 517.69</span></li><li class='Fp Hide EUR'>
                                                <span>EUR € 319.27</span></li><li class='Fp Hide CAD'><span>CAD $ 505.96</span></li><li
                                                    class='Fp Hide XPF'><span>XPF 38,086.52</span></li><li class='Fp Hide DKK'><span>DKK
                                                        2,376.28</span></li><li class='Fp Hide FJD'><span>FJD 903.90</span></li><li class='Fp Hide HKD'>
                                                            <span>HKD 3,671.89</span></li><li class='Fp Hide INR'><span>INR 22,264.05</span></li><li
                                                                class='Fp Hide JPY'><span>JPY ¥ 43,316.92</span></li><li class='Fp Hide NOK'><span>NOK
                                                                    2,672.03</span></li><li class='Fp Hide PKR'><span>PKR 647,999,938.38</span></li><li
                                                                        class='Fp Hide PGK'><span>PGK 1,208.71</span></li><li class='Fp Hide PHP'><span>PHP
                                                                            22,619.41</span></li><li class='Fp Hide SGD'><span>SGD $ 662.06</span></li><li class='Fp Hide SBD'>
                                                                                <span>SBD 3,763.13</span></li><li class='Fp Hide ZAR'><span>ZAR R 3,650.70</span></li><li
                                                                                    class='Fp Hide LKR'><span>LKR 54,272.46</span></li><li class='Fp Hide SEK'><span>SEK
                                                                                        3,301.75</span></li><li class='Fp Hide CHF'><span>CHF 482.63</span></li><li class='Fp Hide THB'>
                                                                                            <span>THB ฿ 15,729.68</span></li><li class='Fp Hide TOP'><span>TOP 881.93</span></li><li
                                                                                                class='Fp Hide VUV'><span>VUV 45,560.56</span></li><li class='Fp Hide WST'><span>WST
                                                                                                    1,197.37</span></li></ul>
                                <a class='ShowPriceList'>Price Details</a></div>
                            <a href='Configure.aspx?avp=EA6DB2C5D7C04792BFF9628A8D03EB88' class='SelectBtn'>Select</a></div>
                        <div class='PriceDetailsList NoFreeDays NoDiscount'>
                            <table class='chargesTable'>
                                <thead>
                                    <tr>
                                        <th class='RateTH'>
                                            Product Description
                                        </th>
                                        <th class='FreeTH'>
                                            <span>Free Days</span><span rel='freeDays' class='popup' style='display: none;'>[?]</span>
                                        </th>
                                        <th class='HireTH'>
                                            Hire Days
                                        </th>
                                        <th class='PerTH'>
                                            Per Day Price
                                        </th>
                                        <th class='TotalTH'>
                                            Total
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class='rt'>
                                            22-Aug-2012 to 30-Aug-2012
                                        </td>
                                        <td class='fd'>
                                        </td>
                                        <td class='hp'>
                                            9 day(s)
                                        </td>
                                        <td class='pdp'>
                                            $72.00
                                        </td>
                                        <td>
                                            $648.00
                                        </td>
                                    </tr>
                                    <tr rel='TOURRAD'>
                                        <td colspan='3'>
                                            Complimentary Tourism Radio Guide
                                        </td>
                                        <td>
                                            included
                                        </td>
                                    </tr>
                                    <tr class='total'>
                                        <td class='TotalTD' colspan='3'>
                                            Total
                                        </td>
                                        <td>
                                            $648.00
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class='DiscountedDaysPopUp bubbleBody'>
                                <h3>
                                    Discounted Day Price</h3>
                                <table>
                                    <tbody>
                                        <tr class='total' rel='0'>
                                            <td>
                                                Total Discount
                                            </td>
                                            <td>
                                                0%
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <span class='note'>(off standard per day rates)</span></div>
                        </div>
                    </li>
                </ul>
                <div id="emptySelectionMsg">
                <span>
                    
                </span>
            </div>                
        </div>
            <div id="staticLoader">
                <span>
                Reloading your search results...
                </span>
                <ul id="carousel">
                    <li><img src="http://images.thl.com/CentralLibraryImages/Maui/New-Zealand/Campervans/SelectionPreloader/Ad1.png" alt="" /></li>
                    <li><img src="http://images.thl.com/CentralLibraryImages/Maui/New-Zealand/Campervans/SelectionPreloader/Ad2.png" alt="" /></li>
                    <li><img src="http://images.thl.com/CentralLibraryImages/Maui/New-Zealand/Campervans/SelectionPreloader/Ad3.png" alt="" /></li>
                    <li><img src="http://images.thl.com/CentralLibraryImages/Maui/New-Zealand/Campervans/SelectionPreloader/Ad4.png" alt="" /></li>                            
                </ul>  
            </div>
        <div id="disclaimer" title="disclaimer">
            <span>Please note: Rental prices quoted above are only valid at the time of booking.</span>
            <span>Alternative currency is based on approximate current exchange rates. All payments will be processed in NZD.</span>
        </div>
        <style type="text/css">
            #promoPanel {
                float: left;
                height:40px;
                width: 230px; 
                font-size: 11px;
            }
            
            #promoPanel span {
                display: block;
                float: left;
                clear: both;
            }        
            
            #promoPanel input,  #promoPanel textarea {
                width: 240px;
            }
            
            #copyToCB {
                cursor: pointer;
                width: 100px;
                height: 30px;
                width: 240px;
                text-decoration: underline;
            }        
                
        </style>
        <script type="text/javascript" src="js/zclip.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                $j('#promoTitle').keyup(function () {
                    var elm = $j(this);
                    $j('.promo big').html(elm.val());
                });

                $j('#promoBody').keyup(function () {
                    var elm = $j(this);
                    $j('.promo').html("<big>" + $j('#promoTitle').val() + "</big>" + elm.val());
                });                                   

            });
            
        </script>
        <div id="promoPanel">
            <span>
                <label>Promo Text Title</label>
                <input type="text" id="promoTitle" value="<%= PromoHeader %>" />
            </span>
            <span>
                <label>Promo Text Body</label>
                <textarea cols="30" rows="4" id="promoBody"><%= PromoText %></textarea>
            </span>            
        </div> 
    </form>   
</body>
</html>