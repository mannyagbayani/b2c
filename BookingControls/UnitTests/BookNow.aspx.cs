﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class UnitTests_BookNow : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.Params["quote"]))
        {
            DataAccess dal = new DataAccess();
            AvailabilityRequest req = new AvailabilityRequest();
            AvailabilityResponse res = null;
            //http://local.britz.com.au/bookingcontrols/Selection.aspx?cc=AU&brand=b&ac=&sc=rv&vtype=rv&pc=&na=1&nc=0&cr=0&pb=BNE&pd=29&pm=6&py=2011&pt=10%3a00&db=SYD&dd=20&dm=7&dy=2011&dt=15%3a00&vh=auavb.4BBJ&pv=1.0

            NameValueCollection reqParams = AvailabilityHelper.OldControlCollectionMapper(Request.Params);
            if (AvailabilityHelper.HasRequiredParams(reqParams))
            {   
                req.LoadFromAuroraParams(reqParams);
                res = new DataAccess().GetAvailability(req);
                AvailabilityItem aItem =  res.GetAvailabilityItems(string.Empty)[0];
                decimal estimatedTotal = aItem.EstimatedTotal;
                Response.ContentType = "application/json";
                Response.Write("{\"total\":"+ estimatedTotal + ",\"prdName\":\""+ aItem.VehicleName + "\"  }");
                Response.End();
            }            
        }
    }
}
