﻿function initCS(initParams) {    
    loadLocations();  
    
    $('#sortPanel .btns label').click(function () {                
        var elm = $(this);
        var parentSelector = elm.parent();
        while (!parentSelector.hasClass('selector')) parentSelector = parentSelector.parent();
        $('#sortPanel label.selected').removeClass('selected');
        elm.addClass('selected');
        var actionType = parentSelector.attr('id').split('_')[0];
        var actionCol = parentSelector.attr('id').split('_')[1];               
        if (actionType == "filter") 
            filterBy = elm.attr('data-filter');
        else {
            sortBy = actionCol;
            isAsc = (elm.attr('data-filter') == "asc");
        }
        isoTope();
    });

    //combo bind:
    $(".combo, #currencySelector").combobox({change: function( event, ui ) { alert(ui); } });
    //datepicker
    initDatePicker();
    /*
    $('#vehicleCatalog .vehicle .inf').click(function(e) { 
        var elm = $(this);
        var parentSelector = elm.parent();
        while (!parentSelector.hasClass('vehicle')) parentSelector = parentSelector.parent();
        parentSelector.toggleClass('col');
    });
    */
    
    $('#vehicleInfoSelector span').click(function(e){
        $('#vehicleInfoSelector span').removeClass('active');
        $(this).addClass('active');
        if($(this).hasClass('show'))
            $('#vehicleCatalog .vehicle').removeClass('col');
        else
            $('#vehicleCatalog .vehicle').addClass('col')
    })

    $('#bookingControl .ui-combobox-input').focus(function() { $(this).select(); });

    //only relevant brands
    $('#vehicleCatalogHead .logos li span').each(function(i, el) {
        var elm = $(el);
        if (!initParams.request.includedBrand(elm.attr('rel')))
            elm.css('display','none');
    });
    $('#vehicleCatalog .brand').each(function(i, el) {
        var elm = $(el);
        if (!initParams.request.includedBrand(elm.attr('id').split('_')[0]))
            elm.css('display','none');
    });
    //only relevant brands ends

    
    initParams.request.query();//request
}



function loadLocations(){
    var cc = rqst.params.cc
    $(ctrlParams.branches).each(function(i, el) {
        if ((el.vt == "campervan") && el.cc == cc) {
            $('select.location').append($('<option>', {
                value: el.code
            }).text(el.name));
        }
    });
}

function isoTope() { 
    $('#vehicleCatalog .brand').isotope({
        animationEngine: 'jquery',
        getSortData: {
            sleeps: function ($elem) {
                return $elem.attr('data-sleeps');
            },
            price: function ($elem) {
                return parseInt($elem.attr('data-price'));
            },
            wheel : function ($elem) {
                return $elem.attr('data-wdr');
            }, 
            length : function ($elem) {
                return parseFloat($elem.attr('data-length'));
            },
        },
        sortBy: sortBy, 
        sortAscending: isAsc,
        filter: filterBy
    });
}

function tgl(el) {
    var elm = $(el);
    var parentSelector = elm.parent();
    while (!parentSelector.hasClass('vehicle')) parentSelector = parentSelector.parent();
    parentSelector.toggleClass('col');
    isoTope();
}

function initDatePicker() {
    var targetFrom = "pickUpTimeTxt";
    pdDatePicker = $('#' + targetFrom).datepicker({
        dateFormat: 'M dd, yy',
        //beforeShowDay: function(){} ,
        numberOfMonths: 1,
        changeMonth: true,
        changeYear: false,
        minDate: new Date(),
        onSelect: function (dateText, inst) {
            var month = parseInt(inst.selectedMonth,10) + 1;
            rqst.params.pd = inst.selectedDay + "-" + (month < 9 ? "0" : "") + month + "-" + inst.selectedYear;
            setDateFromDatePicker($('#pickUpTimeTxt'), dateText, inst);
        },
        onChangeMonthYear: function (year, month, inst) {
            changeMonthYear($(this), year, month, inst, $('#pickUpTimeTxt'));
        }
    });
    ddDatePicker = $("#dropOffTimeTxt").datepicker({
        dateFormat: 'M dd, yy',
        numberOfMonths: 1,
        minDate: new Date(),
        changeMonth: true,
        changeYear: false,
        onSelect: function (dateText, inst) {
            var month = parseInt(inst.selectedMonth,10) + 1;
            setDateFromDatePicker($('#dropOffTimeTxt'), dateText, inst);
            rqst.params.dd = inst.selectedDay + "-" + (month < 9 ? "0" : "") + month + "-" + inst.selectedYear;            
        },
        onChangeMonthYear: function (year, month, inst) {
            changeMonthYear($(this), year, month, inst, $('#dropOffTimeTxt'));
        }
    });
}

function changeMonthYear(calendar, year, month, inst, hiddenObj) {
    var oldDate = calendar.datepicker("getDate");
    if (oldDate != null) {
        var newDate = new Date(year, month - 1, oldDate.getDate());
        if (newDate.getDate() != oldDate.getDate()) {
            var lastDay = 32 - new Date(year, month - 1, 32).getDate();
            newDate = new Date(year, month - 1, lastDay);
        }
        calendar.datepicker("setDate", newDate);
        setDateFromDatePicker(hiddenObj, inst.input.val(), inst);
    }
}

function setDateFromDatePicker(hiddenObj, dateText, inst) {
    $(hiddenObj).val(dateText);
    setDate(inst);
    //$('#errorPanel span').html('');
}

function setDate(inst) {
    var toDate = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
	var nextDay = new Date(toDate.getTime() + 1 * 86400000);
    $('#dropOffTxt').datepicker("option", "minDate", nextDay);
}

function submitSearch() {
    //http://local.motorhomesandcars.com/availability/au/thl/bne/1030-01-03-2013/syd/1500-15-03-2013/aff-travex/pckId-flexbgr/pax-6/sess-AED0A5
    $('#vehicleCatalog .brand .vehicle').addClass('loading');
    rqst.query();
}

function ddChange(event, ui) {//handle the combo event on thl cols
    var elm = $(ui.item.option.parentElement);
    ddId = $(elm).attr('id');
    var val = elm.val().toLowerCase()
    switch(ddId)
    {
        case 'dlSelector':
            rqst.params.dl = val;
            break;
        case 'pickUpSelector':
            rqst.params.pb = val;
            break;
        case 'dropOffSelector':
            rqst.params.db = val;
            break;
        case 'pickTimeSelector':
            rqst.params.pt = val;
            break;
        case 'dropTimeSelector':
            rqst.params.dt = val;
            break;
        case 'adultSelector':
            rqst.params.na = val;
            break;
        case 'childSelector':
            rqst.params.nc = val;
            break;
    }
}

/* Request Object */
function B2CRequest(params) { this.params = params; }

B2CRequest.prototype.toString = function () {
    var results = "params:";
    for (var key in this.params) {
        var obj = this.params[key];
        results +=  key + " = " + obj + ",";
    }
    return results;
} 

B2CRequest.prototype.getRequestUrl = function(queryBrand) {
    ///availability/au/thl/bne/1030-01-03-2013/syd/1500-15-03-2013/aff-travex/pckId-flexbgr/pax-6/sess-AED0A5
    //TODO: month needs two digits!
    var reqParams = this.params;
    var url = "/availability/" + reqParams.cc + "/" + queryBrand + "/" + reqParams.pb + "/" + reqParams.pt + "-" + reqParams.pd + "/" + reqParams.db + "/" + reqParams.dt + "-" + reqParams.dd + "/";
    return url;
}

B2CRequest.prototype.getConfigureURL = function(vCode) {
    var pickDayParams = this.params.pd.split('-');
    var dropDayParams = this.params.dd.split('-');     
    var url = "/Selection.aspx?cc=" + this.params.cc + "&brand=&ac=&sc=rv&vtype=rv&pc=&na=1&nc=0&cr=AU&pb=" + this.params.pb + "&pd=" + pickDayParams[0] + "&pm=" + pickDayParams[1] + "&py=" + pickDayParams[2] + "&pt=10:00&db=" + this.params.db + "&dd=" + dropDayParams[0] + "&dm=" + dropDayParams[1] + "&dy=" + dropDayParams[2] + "&dt=15:00&vh="+ vCode +"&pv=1.0";
    return url
}

B2CRequest.prototype.includedBrand = function(brand) {
    for(var i=0; i< this.params.queryBrands.length; i++)
        if(this.params.queryBrands[i] == brand) return true;
    return false;
}

var loadCount = 0;

B2CRequest.prototype.query = function() {    
    loadCount = this.params.queryBrands.length;
    for(var i=0; i < this.params.queryBrands.length; i++)
    {    
        var options = {
            url: this.getRequestUrl(this.params.queryBrands[i]) ,
            data: "{'type':'json'}",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'post',
            beforeSend: function () { /* */ },
            success: function (json) {
                loadCount--;
                var items = eval(json.items);
                for(var i=0; i<items.length; i++) {
                    var elm = $('#v_' + items[i].code);
                    var priceNode = elm.find('.prc');
                    elm.find('.vehicle').removeClass('loading');
                    if(items[i].price > 0 ) {                        
                        priceNode.text("$" + items[i].price + ".00");
                        elm.attr('data-price', items[i].price);      
                        elm.find('.book').attr('data-code', items[i].code).click(function() { 
                            var elm = $(this);
                            window.location = rqst.getConfigureURL(elm.attr('data-code'));
                        });                  
                    }
                    else{
                        priceNode.text("Unavailable");
                        elm.attr('data-price', 99999);//max out
                    }
                }
                if(loadCount == 0) 
                {
                    $('#vehicleCatalog .vehicle').each(function(i, el) { 
                        var elm = $(el);
                        if(elm.hasClass('loading'))
                        {
                            elm.removeClass('loading');
                            var priceNode = elm.find('.prc');
                            priceNode.text("Unavailable");//prompt point here..
                            elm.attr('data-price', 99999);//max out
                        }
                    });                
                }
            },
            error: function (XMLHttpRequest, textStatus, error) {
                //XMLHttpRequest, error
                loadCount--;
                //alert(error);
            },
            timeout: function () {
                loadCount--;
                //alert("timeout");//TODO: 
            }
        };
        var xhr = $.ajax(options);
    }
}