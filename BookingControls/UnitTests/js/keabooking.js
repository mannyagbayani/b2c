﻿jQuery.noConflict();

jQuery(document).ready(function (jQuery) {
    jQuery("#imageslider").SexySlider({
        width: 942,
        height: 500,
        delay: 5000,
        strips: 1,
        autopause: true,
        titlePosition: 'left',
        titleStyle: false,
        titleOpacity: 1.0,
        titleSpeed: 400,
        onComplete: function () { jQuery('#imageslider .sexyslider-title').css({ 'height': 275, 'width': 400 }); },
        navigation: '#slide-navigation',
        control: '#slide-control',
        effect: 'fade'
    });
});

jQuery(document).ready(function () {
    if (jQuery('img.thumb').length > 0) {
        jQuery('#image-container').css('background-image', 'url(' + jQuery('img.thumb').attr('src').substring(0, jQuery('img.thumb').attr('src').lastIndexOf('-')) + '.jpg)');
    }
});
jQuery(document).ready(function () {
    jQuery('img.thumb').hover(
        function () { jQuery('img.image-swap').attr('src', jQuery(this).attr('src').substring(0, jQuery(this).attr('src').lastIndexOf('-')) + '.jpg'); jQuery('.youtube-video').hide(); jQuery('img.image-swap').stop().fadeTo(300, 1); },
        function () { jQuery('img.image-swap').stop().fadeTo(0, 0); jQuery('#image-container').css('background-image', 'url(' + jQuery(this).attr('src').substring(0, jQuery(this).attr('src').lastIndexOf('-')) + '.jpg' + ')'); }
      );

});
jQuery(document).ready(function () {
    if (jQuery('.youtube-video').length > 0) {
        var path = window.location.pathname;
        if (path.indexOf('2-berth') >= 0) {
            jQuery('.youtube-video').attr('src', 'http://www.youtube.com/embed/KQWxZ6CMTzA?wmode=transparent');
        }
        if (path.indexOf('2-plus-2-berth') >= 0) {
            jQuery('.youtube-video').attr('src', 'http://www.youtube.com/embed/vu0ippzrMHM?wmode=transparent');
        }
        if (path.indexOf('4-berth') >= 0) {
            jQuery('.youtube-video').attr('src', 'http://www.youtube.com/embed/y4wQ92zsL8o?wmode=transparent');
        }
        if (path.indexOf('6-berth') >= 0) {
            jQuery('.youtube-video').attr('src', 'http://www.youtube.com/embed/K9r42ljNn4o?wmode=transparent');
        }
        if (path.indexOf('pop-top') >= 0) {
            jQuery('.youtube-video').attr('src', 'http://www.youtube.com/embed/BaUxewifNL0?wmode=transparent');
        }
        jQuery('.video-button').click(
        function () { jQuery('.youtube-video').show(); }
      );
    }
});

jQuery(window).load(function () {


    jQuery('div.box-image').hover(function () {
        jQuery(this).children('div.description').css('display', 'block');

    }, function () {
        jQuery(this).children('div.description').css('display', 'none');

    });

});

jQuery(document).ready(function () {

    jQuery(".box-image, .news-item-home, .footer-box, .right-nav-info-box").click(function () {
        window.location = jQuery(this).find("a").attr("href"); return false;

    });

    jQuery(".box-image, .news-item-home").hover(
  function () {
      jQuery(this).addClass("hover");
  },
  function () {
      jQuery(this).removeClass("hover");
  }
);

    jQuery(".footer-box, .right-nav-info-box").hover(
  function () {
      jQuery(this).addClass("hover2");
  },
  function () {
      jQuery(this).removeClass("hover2");
  }
);
});


jQuery(document).ready(function () {
    jQuery("#footer-boxes-wrapper").equalheight();
});




jQuery(document).ready(function () {

    jQuery('#veichle-specs-table > tbody > tr:odd').css("background-color", "#f7f8f1");

});

jQuery(function () {
    jQuery("#reserve-button1").hover(function () {
        jQuery("#reserve-drop").show(); //your show code
    });
});
jQuery(function () {
    jQuery('#reserve-close').click(function () {
        jQuery('#reserve-drop').hide(); //your show code
    });
});

jQuery(function () {
    jQuery("#reserve-button-right").hover(function () {
        jQuery("#reserve-drop2").slideDown(); //your show code
    });
});

jQuery(function () {
    jQuery('#reserve-close2').click(function () {
        jQuery('#reserve-drop2').slideUp(); //your show code
        return false;
    });
});

jQuery(function () {
    jQuery('#tab2').click(function () {
        jQuery('.right-nav-info-box').show(); //your show code
    });
});
jQuery(function () {
    jQuery('#tab2b').click(function () {
        jQuery('.right-nav-info-box').show(); //your show code
    });
});

jQuery(function () {
    jQuery('#tab1').click(function () {
        jQuery('.right-nav-info-box').show(); //your show code
    });
});

jQuery(function () {
    jQuery('#tab3').click(function () {
        jQuery('.right-nav-info-box').hide(); //your show code
    });
});
jQuery(function () {
    jQuery('#tab4').click(function () {
        jQuery('.right-nav-info-box').show(); //your show code
    });
});

/* Booking Button */

function vehicleSearch() {

    var vehicle = document.getElementById("vehicletype").value;
    var pickupDate = document.getElementById("pudatepicker").value;
    var dropoffDate = document.getElementById("dodatepicker").value;
    var pickupLocation = document.getElementById("puloc").value;
    var dropoffLocation = document.getElementById("doloc").value;

    var pudate = jQuery("#pudatepicker").datepicker("getDate");
    var dodate = jQuery("#dodatepicker").datepicker("getDate");

    if (pickupDate == "") {
        alert("Please select a pick up date");
    }
    else if (dropoffDate == "") {
        alert("Please select a drop off date");
    }
    else if (dodate.setDate(dodate.getDate() - 4) < pudate) {
        alert("Drop off date must be at least 4 days after pick up date!");
    }
    else {
    
        //-- THL Code Starts
        var pdParams = pickupDate.split('-');
        var ddParams = dropoffDate.split('-');
        var paxAdl = jQuery('#doadl').val(), paxChld = jQuery('#dochl').val();

        var queryParams = "/Selection.aspx?cc=AU&brand=q&ac=&sc=rv&vtype=rv&pc=&na=" + paxAdl +"&nc="+ paxChld +"&cr=&pb=" + pickupLocation + "&pd=" + pdParams[0] + "&pm=" + pdParams[1] + "&py=" + pdParams[2] + "&pt=" + "10:00" + "&db=" + dropoffLocation + "&dd=" + ddParams[0] + "&dm=" + ddParams[1] + "&dy=" + ddParams[2] + "&dt=" + "10:00" + "&vh=" + thlcodeforkea(vehicle) + "&pv=1.0";
        document.location.href = queryParams;
        //-- THL Code Ends

        //var infantNumber = document.getElementById("infantnumber").value;
        //alert("http://www.tpl-services.com/modules/booking/default.aspx?siteID=b8e7b797-7543-4901-858c-c8e29d166504&v=" + vehicle + "&pl=" + pickupLocation + "&dl=" + dropoffLocation + "&pd=" + pickupDate + "&dd=" + dropoffDate + "&qsvs=auto");
        //document.location.href="http://www.tpl-services.com/modules/booking/default.aspx?siteID=b8e7b797-7543-4901-858c-c8e29d166504&v=" + vehicle + "&pl=" + pickupLocation + "&dl=" + dropoffLocation + "&pd=" + pickupDate + "&dd=" + dropoffDate + "&qsvs=auto";
        //document.location.href = "http://www.tpl-services.com/modules/booking/default.aspx?siteID=45c3f703-71c8-4d13-bb76-ba0cdac3588a&v=" + vehicle + "&pl=" + pickupLocation + "&dl=" + dropoffLocation + "&pd=" + pickupDate + "&dd=" + dropoffDate + "&qsvs=auto"
    }

}

function vehicleSearch2() {

    var vehicle = document.getElementById("vehicletype2").value;
    var pickupDate = document.getElementById("pudatepicker2").value;
    var dropoffDate = document.getElementById("dodatepicker2").value;
    var pickupLocation = document.getElementById("puloc2").value;
    var dropoffLocation = document.getElementById("doloc2").value;

    var pudate = jQuery("#pudatepicker2").datepicker("getDate");
    var dodate = jQuery("#dodatepicker2").datepicker("getDate");

    if (pickupDate == "") {
        alert("Please select a pick up date");
    }
    else if (dropoffDate == "") {
        alert("Please select a drop off date");
    }
    else if (dodate.setDate(dodate.getDate() - 4) < pudate) {
        alert("Drop off date must be at least 4 days after pick up date!");
    }
    else {
        //var infantNumber = document.getElementById("infantnumber").value;
        //alert("http://www.tpl-services.com/modules/booking/default.aspx?siteID=b8e7b797-7543-4901-858c-c8e29d166504&v=" + vehicle + "&pl=" + pickupLocation + "&dl=" + dropoffLocation + "&pd=" + pickupDate + "&dd=" + dropoffDate + "&qsvs=auto");
        //document.location.href="http://www.tpl-services.com/modules/booking/default.aspx?siteID=b8e7b797-7543-4901-858c-c8e29d166504&v=" + vehicle + "&pl=" + pickupLocation + "&dl=" + dropoffLocation + "&pd=" + pickupDate + "&dd=" + dropoffDate + "&qsvs=auto";
        
        //-- THL Code Starts
        var pdParams = pickupDate.split('-');
        var ddParams = dropoffDate.split('-');
        var queryParams = "/Selection.aspx?cc=AU&brand=q&ac=&sc=rv&vtype=rv&pc=&na=1&nc=0&cr=&pb=" + pickupLocation + "&pd=" + pdParams[0] + "&pm=" + pdParams[1] + "&py=" + pdParams[3] + "&pt=" + "10:00" + "&db=" + dropoffLocation + "&dd=" + ddParams[0] + "&dm=" + ddParams[1] + "&dy=" + ddParams[2] + "&dt=" + "10:00" + "&vh=" + thlcodeforkea(vehicle) + "&pv=1.0";
        document.location.href = queryParams;
        //-- THL Code Ends
        
        //document.location.href = "http://www.tpl-services.com/modules/booking/default.aspx?siteID=45c3f703-71c8-4d13-bb76-ba0cdac3588a&v=" + vehicle + "&pl=" + pickupLocation + "&dl=" + dropoffLocation + "&pd=" + pickupDate + "&dd=" + dropoffDate + "&qsvs=auto"
    }

}

function thlcodeforkea(keacode) { 
    var tcode = '';
    switch (keacode) {
        case "ea":
            tcode = "AUavq.2K";
            break;
        case "ja":
            tcode = "AUavq.4K";
            break;
        case "la":
            tcode = "AUavq.6K";
            break;
        case "xa":
            tcode = "AUavq.2K4WD";
            break;
    }
    return tcode;
}
