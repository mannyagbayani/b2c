﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using THL.Booking;

public partial class UnitTests_AltAvailability : System.Web.UI.Page
{

    public string CurrentRow, OriginalSearchRow, BrandChar = "b", HTTPPrefix, ParentSite;
    
    public AvailabilityRequest AvailabilityRequest;
    public AvailabilityItem AItem;//session instance of non available row

    public string debugSTR;

    public string ParentSiteURL, SelectionURL;

    public string CurrencySelector;
    public CurrencyCode defaultCurrency;
    CurrencyManager curMan;

    //Content objects
    public string AssetsURL, BrandCountryAssetPath, VehicleTypeStr;
    public string VehicleImageConvension;

    public string VehicleImagePath;

    public string DNAIMGTag, AnalyticsCode;//Tracking

    decimal originalHirePeriod;
    DateTime originalRequestTravelDate;

    AvailabilityRequest aRequest;

    ApplicationData appData;

    THLDomain appDomain;

    protected void Page_Load(object sender, EventArgs e)
    {

        bool secureMode = false;
        bool.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["SecureMode"], out secureMode);

        HTTPPrefix = ApplicationManager.HTTPPrefix;
               

        appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
        if (appDomain == null)//dubug line
            appDomain = new THLDomain("secure.maui.co.nz", THLBrands.Maui, CountryCode.NZ, "B2CNZ", false, "uahere");//dubug line
        BrandChar = appDomain.BrandChar.ToString(); //(Request.Params["brand"] != null ? Request.Params["brand"] : "m");//TODO: this should be swapped by baseURL. 
        ParentSiteURL = appDomain.ParentSite;
        string countryCode = appDomain.Country.ToString().ToUpper(); 

        AssetsURL = HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];
        BrandCountryAssetPath = THLBrand.GetNameForBrand(appDomain.Brand) + "/" + AvailabilityHelper.GetCountryName(appDomain.Country).Replace(" ", "-"); //"Britz/Australia"

        
        //if (SessionManager.GetSessionData() && SessionManager.GetSessionData().HasRequestData)

        appData = ApplicationManager.GetApplicationData();
         
        string brandStr = appDomain.BrandChar.ToString();
        ParentSite = appDomain.ParentSite;

        SessionData sessionData = SessionManager.GetSessionData();
        if(!String.IsNullOrEmpty(Request.Params["avp"]) && sessionData.HasResponseData)//update session and redirect
        {           
            AvailabilityResponse aResponse = sessionData.AvailabilityResponseData;
            AvailabilityItem aItem = (aResponse.GetAvailabilityItem(Request.Params["avp"]));//get the relevant availability object:
            
            if (aItem == null)
                Response.Redirect("../Selection.aspx?" + sessionData.AvailabilityRequestData.GenerateSelectionParamsURL(false));//back buttom scenario fix
                        
            AlternativeAvailabilityItem aaItem = (AlternativeAvailabilityItem)aItem;//cast to alternate
            //TODO: refactor below into Session AI Loader
            sessionData.AvailabilityRequestData.PickUpBranch = aaItem.PickUpLoc;
            sessionData.AvailabilityRequestData.PickUpDate = aaItem.PickUpDate;
            sessionData.AvailabilityRequestData.DropOffBranch = aaItem.DropOffLoc;
            sessionData.AvailabilityRequestData.DropOffDate = aaItem.DropOffDate;                    
            //end refactor below into Session AI Loader
            Response.Redirect("../Configure.aspx?avp=" + Request.Params["avp"]);
        }
        else if (sessionData.HasRequestData)  
        {
            aRequest = SessionManager.GetSessionData().AvailabilityRequestData;                        
            SelectionURL = "../Selection.aspx?" + aRequest.GenerateSelectionParamsURL(false);            
            originalRequestTravelDate = aRequest.PickUpDate;
            BrandChar = String.IsNullOrEmpty(Request.Params["brand"]) ? "b" : Request.Params["brand"];
            AvailabilityRequest = SessionManager.GetSessionData().AvailabilityRequestData;
            AvailabilityRequest.VehicleModel = (!String.IsNullOrEmpty(Request.Params["vCode"]) ? Request.Params["vCode"] : string.Empty);
            AItem = sessionData.AvailabilityResponseData.GetAvailabilityItemForVehicleCode(Request.Params["vCode"]);
            //TODO: refactor below into Session Manager as SessionData.AlternateOptions
            AlternativeAvailabilityItem[] alternativeItems = (Session["GetAlternateOptions"] == null) ? (new DataAccess().GetAlternateAvailability(aRequest)) : (AlternativeAvailabilityItem[])Session["GetAlternateOptions"];
            //AlternativeAvailabilityItem[] alternativeItems = (new DataAccess().GetAlternateAvailability(aRequest));//back end call
            setAvailabilityResponse(alternativeItems);//Load Session for Configure Phase
            VehicleTypeStr = (aRequest.VehicleType == VehicleType.Car ? "Cars" : "Campervans");
            VehicleImageConvension = aRequest.CountryCode.ToString().ToLower() + AvailabilityHelper.GetAuroraVehicleStrForVehicleType(aRequest.VehicleType) + brandStr + "." + AItem.VehicleCode;
            VehicleImagePath = AssetsURL + "/CentralLibraryImages/" + BrandCountryAssetPath + "/" + VehicleTypeStr + "/" + VehicleImageConvension + "/" + VehicleImageConvension;
            OriginalSearchRow = RenderAlternateRow(GetOriginalItemForRequest(AvailabilityRequest), originalHirePeriod);
            string aaCollectionData = string.Empty;            
            //currency selector:
            defaultCurrency = CurrencyManager.GetCurrencyForCountryCode(countryCode);
            curMan = new CurrencyManager(CurrencyConversionProvider.BNZ, defaultCurrency);//todo: from appManager
            CurrencySelector = new AvailabilityResponseDisplayer(null, aRequest, curMan, AssetsURL, appDomain.Brand).RenderCurrencyDropDown(defaultCurrency);
            foreach (AlternativeAvailabilityItem item in alternativeItems)
                CurrentRow += RenderAlternateRow(item, originalHirePeriod);    
            //tracking bits
            TrackingDisplayer trd = new TrackingDisplayer(appDomain, secureMode);
            DNAIMGTag = trd.RenderDNATrackingImg("configure");
            AnalyticsCode = trd.RenderAnalsCode();//Analytics Tracking Code
            if (!String.IsNullOrEmpty(Request.Params["debugOn"]) && Request.Params["debugOn"].ToUpper().Equals("TRUE"))
            {
                //debug line TODO: remove or set to config ref
                debugSTR =  "http://pap-app-002/PhoenixWS%20Alternate/service.asmx/GetAlternateAvailability?" + AvailabilityHelper.GetAlternateAvailabilityRequestURL(aRequest);
                debugSTR = "<iframe src='" + debugSTR + "' style='width:900px;height:800px' />";            
            }            
        }
        else
        {
            Response.Redirect("../Selection.aspx?msg=sessionExpired");
        }
    }


    /// <summary>
    /// Refactor into AvaliabilityResponseDisplayer 
    /// </summary>
    /// <param name="aType"></param>
    /// <returns></returns>
    public string RenderAlternateRow(AlternativeAvailabilityItem alternateItem, decimal originalHirePeriod)
    {
        AlternativeAvailabilityType aType = alternateItem.Type;
        bool isAvailable = (alternateItem.AvailabilityType == AvailabilityItemType.Available);        
        
        string alternateTypeLabel = GetAlternativeTypeLabel(aType);//refacor into static helper

        int spannerWidth = 400;
        int hireDays =  alternateItem.HirePeriod;
        int leadTime = 5 + new Random().Next(5, 100);

        int suggestedPickUpPriorToTravel = (alternateItem.PickUpDate - originalRequestTravelDate.AddDays(-10)).Days ;
                
        decimal totalDays = 20m + originalHirePeriod;
        decimal PxPerDay = spannerWidth / totalDays;
        DateTime pickUpDate = alternateItem.PickUpDate;
        DateTime dropOffDate = alternateItem.DropOffDate;
        string pickUpLoc = alternateItem.PickUpLoc , dropOffLoc = alternateItem.DropOffLoc, pickUpDateStr = pickUpDate.ToString("dd MMM yy") + "'", dropOffDateStr = dropOffDate.ToString("dd MMM yy") + "'";
        string spanClass = (aType == AlternativeAvailabilityType.OriginalSearch ? "noAvail" : "Avail");

        string brandChar = appDomain.BrandChar.ToString();
        
        StringBuilder sb = new StringBuilder();
        sb.Append(@"
        <div class=""OptionRow"">
            <div class=""AltRow " + aType.ToString() + @""">
                <div class=""AltType"" title=""" + alternateItem.PackageCode + @""">
                    <span>" + alternateTypeLabel + @"</span>
                </div>
        ");
        if (isAvailable)
        {            
            sb.Append(@"
                <div class=""AltTime"">
                    <div class=""TimeLabels period"">                
                        <span style=""width: " + (Math.Round(PxPerDay * (suggestedPickUpPriorToTravel)) + Math.Round((PxPerDay * hireDays)/2)) + @"px;"" class=""preORDates"">&nbsp;</span> 
                        <span rel=""pudate"" style=""width:40px;"" class=""hireORDates"">
                            <small>" + hireDays + @" days</small>
                        </span>
                    </div>
                    <div class=""TimeSpan"">
                        <span class=""preDates"" rel='" + PxPerDay + @"' style=""width:" +  Math.Round(PxPerDay * (suggestedPickUpPriorToTravel)) + @"px;"">&nbsp;</span>
                        <span class=""hireDates " + spanClass + @""" style=""width:" + Math.Floor(PxPerDay * hireDays) + @"px;"">&nbsp;</span>
                        <div style=""display:none;"" class=""popUpTxt""><div style='height:20px;width:100px;line-height:18px;color:#000;'>text text text...</div></div>
                        <span class=""postDates"">&nbsp;</span>
                    </div>
                    <div class=""TimeLabels dates"">
                        <span style=""width: " + (Math.Round(PxPerDay * (suggestedPickUpPriorToTravel))- 20) + @"px;"" class=""preORDates"">&nbsp;</span> 
                        <span rel=""pudate"" class=""hireORDates"" style=""width: 72px;"">
                            <small>" + pickUpDateStr + " " + pickUpLoc + @"</small>
                        </span>
                        <span style=""width:" + ((((PxPerDay * hireDays)) + (Math.Round(PxPerDay * (suggestedPickUpPriorToTravel)))) < 350 ? ((PxPerDay * hireDays) - 72) : (Math.Floor(PxPerDay * hireDays)-82) )/*q&d pad extreme long */ + @"px"">&nbsp;</span>
                        <span rel=""dodate"" class=""postORDates"" style=""width: 72px;"">
                            <small>" + dropOffDateStr + " " + dropOffLoc + @"</small>
                        </span>
                    </div>
                </div>
                ");
            if (!(aType == AlternativeAvailabilityType.OriginalSearch))
            {
                sb.Append(@"
                <div class=""AltPrice"" title=""Available"">
                    " + getForeignListElm(alternateItem.EstimatedTotal, aRequest, curMan, defaultCurrency.ToString()) + @"
                    <a class=""ShowPriceList"">Price Details</a>
                    <!-- RateBand PopUp -->    
                    <div class=""DiscountedDaysPopUp bubbleBody"">
                        <div class=""LargeContainer"">
                            <h3>Price</h3>
                ");
                if (alternateItem.DisplayPromoTile)//only show on promo tiles
                    sb.Append(@" 
                                <img src=""" + AssetsURL + @"/images/" + brandChar + @"/promotions/" + alternateItem.PackageCode + @".png"" class=""ads"" /> 
                    ");
                sb.Append(@"                                               
                                    " + renderRateBandTable(alternateItem.GetRateBands(), alternateItem.GetNonVehicleCharges()) + @"                               
                        </div>
                    </div>
                </div>                                               
                <a href=""?avp=" + alternateItem.AVPID + @""" class=""AltSelect"">Select</a>
                ");
            }
            else
                sb.Append(@"
                <div class=""noAvailability"">
                    <span>Unavailable for some of the</span>
                    <span>days requested</span> 
                </div>
                ");
        }
        else//this option is unavailable
            sb.Append(@"
                <div class=""NoAlt"">
                    <span>Unavailable</span>
                    <a href=""#"" class=""details"">Details</a>
                    <small class=""noAvailX"" title=""Unavailable"">X</small>
                    <a href=""" + ParentSite  + @"/callus"" class=""call"">FREE PHONE</a>
                </div>
            ");        
            sb.Append(@"
            </div>            
        </div>
        ");
        return sb.ToString();
    }

    string renderRateBandTable(AvailabilityItemChargeRateBand[] availabilityRateBands, AvailabilityItemChargeRow[] availabilityCharges)
    {
        bool hasDiscount = AvailabilityHelper.HasDiscountedDays(availabilityRateBands);
        StringBuilder sb = new StringBuilder();
        sb.Append(@"        
                            <table class=""chargesTable"">
                                <thead>
                                    <tr>
                                        <th class=""RateTH"">
                                            Product Description
                                        </th>
                                        <!--
                                        <th class=""FreeTH"">
                                            Free Days
                                        </th>
                                        -->
                                        <th class=""HireTH"">
                                            Hire Days
                                        </th>
                                        <th class=""PerTH"">
                                            Per Day Price
                                        </th>");
        if (hasDiscount)
            sb.Append(@"
                                        <th class=""DiscTH"">
                                            Discounted Day Price
                                        </th>
            ");
        sb.Append(@"                                        
                                        <th class=""TotalTH"">
                                            Total
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
        ");       
        foreach (AvailabilityItemChargeRateBand chargeRateRow in availabilityRateBands)
        {            
            bool hasFreeDays = false;            
            string freeDaysStr = string.Empty;
            if (chargeRateRow.IncludesFreeDay > 0)
            {
                freeDaysStr = chargeRateRow.IncludesFreeDay.ToString();
                hasFreeDays = true;
            }            
            string fromDateStr = chargeRateRow.FromDate.ToString("dd-MMM-yyyy");
            string toDateStr = chargeRateRow.ToDate.ToString("dd-MMM-yyyy");
            sb.Append(@"
                                    <tr>
                                        <td class=""rt""'>" + (fromDateStr + " to " + toDateStr) + @"</td>
                                        <!--<td class=""fd"">" + freeDaysStr + @"</td>-->
                                        <td class=""hp"">" + chargeRateRow.HirePeriod + @" day(s)</td>
                                        <td class=""pdp"">$" + chargeRateRow.OriginalRate.ToString("F") + @"</td>");
            if (hasDiscount)
                sb.Append(@"
                                        <td class=""dpd"">$" + chargeRateRow.DiscountedRate.ToString("F") + @"</td>");
            sb.Append(@"
                                        <td>$" + chargeRateRow.GrossAmount.ToString("F") + @"</td>
                                    </tr>");
        }

        foreach (AvailabilityItemChargeRow chargeRow in availabilityCharges)//Non Rate Band Charges
        {
            sb.Append(@"
                                    <tr>
                                        <td class=""rt""'>" + chargeRow.ProductName + @"</td>
                                        <!--<td class=""fd""></td>-->
                                        <td class=""hp"">" + chargeRow.HirePeriod + @"</td>
                                        <td class=""pdp"">$" + chargeRow.AverageDailyRate.ToString("F") + @"</td>");
            if (hasDiscount)
                sb.Append(@"
                                        <td class=""dpd""></td>");
            sb.Append(@"
                                        <td>$" +  chargeRow.ProductPrice.ToString("F") + @"</td>
                                    </tr>");        
        }

        sb.Append(@"
                                </tbody>
                            </table>
        ");
        return sb.ToString();
    }


    string getForeignListElm(decimal price, AvailabilityRequest availabilityRequest, CurrencyManager currencyManager, string currencyStr)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<ul class='Prices'>");
        sb.Append("<li>");
        sb.Append("<span>" + currencyStr + " " + String.Format("{0:N2}", price) + "</span>");
        sb.Append("</li>");
        foreach (Currency currency in currencyManager.CurrencyRates)
        {
            //if AUD price should be 
            //if (currencyManager.DefaultCurrency == CurrencyCode.AUD) price = 
            decimal currentPriceNZD = price;

            if (availabilityRequest.CountryCode != CountryCode.NZ)
                currentPriceNZD = currencyManager.ConvertToNZD(availabilityRequest.CountryCode, price);


            sb.Append("<li class='Fp Hide " + currency.CountryCode + "'>");
            sb.Append("<span>" + currency.CountryCode + " " + PaymentHelper.GetCurrencySymbolForISO(currency.CountryCode) + " " + String.Format("{0:N2}", (currentPriceNZD * currency.IndicativeRate)) + "</span>");
            sb.Append("</li>");
        }
        sb.Append("</ul>");
        return sb.ToString();
    }


    /// <summary>
    /// Refactor into static helper on AvailabilityResponseDisaplayer
    /// </summary>
    /// <param name="aType"></param>
    /// <returns></returns>
    public string GetAlternativeTypeLabel(AlternativeAvailabilityType aType)
    {
        string responseStr = string.Empty;
        switch (aType)
        {
            case AlternativeAvailabilityType.DropOffBased:
                responseStr = "Fixed drop off date";
                break;
            case AlternativeAvailabilityType.PickUpBased:
                responseStr = "Fixed pick up date";
                break;
            case AlternativeAvailabilityType.SwitchLocation:
                responseStr = "Reverse route";
                break;
            case AlternativeAvailabilityType.MoveForward:
                responseStr = "Later pick up date";
                break;
            case AlternativeAvailabilityType.MoveBackwards:
                responseStr = "Earlier pick up date";
                break;
            case AlternativeAvailabilityType.OneWaySwitchLocation:
                responseStr = "One way switch location";
                break;
            case AlternativeAvailabilityType.OriginalSearch:
                responseStr = "Original Search";
                break;
            default:
                responseStr = "Normal Based";//TODO: define defaults and biz logic if any
                break;
        }
        return responseStr.ToUpper();
    }
    
    /// <summary>
    /// Constract the original Request Avaliability instance in its Alternate format
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    AlternativeAvailabilityItem GetOriginalItemForRequest(AvailabilityRequest request)
    {
        AlternativeAvailabilityItem item = new AlternativeAvailabilityItem(AlternativeAvailabilityType.OriginalSearch);
        item.PickUpDate = request.PickUpDate;
        item.DropOffDate = request.DropOffDate;
        item.PickUpLoc = appData.GetLocationStringForCtrlCode(AvailabilityRequest.PickUpBranch);
        item.DropOffLoc = appData.GetLocationStringForCtrlCode(AvailabilityRequest.DropOffBranch);
        item.HirePeriod = (request.DropOffDate - request.PickUpDate).Days+1;
        originalHirePeriod = item.HirePeriod;
        return item;
    }

    /// <summary>
    /// Set a relevant Session reposne for the config booking phase
    /// </summary>
    /// <returns></returns>
    bool setAvailabilityResponse(AlternativeAvailabilityItem[] alternateItems)
    {
        AvailabilityResponse aResponse = new AvailabilityResponse(AvailabilityRequest.AgentCode);
        
        
        
        aResponse.SetAvailabilityItems(alternateItems);
        SessionData sessionData = SessionManager.GetSessionData();
        //TODO: validate need for context    
        //aResponse.Currency = defaultCurrency;
        sessionData.AvailabilityResponseData = aResponse;        
        return true;
        //TODO:exp handle and manage return cases
    }
}