﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TrackConfirmation.aspx.cs" Inherits="UnitTests_TrackConfirmation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Booking Confirmed..<br />
        <a href="TrackSelection.aspx">Back To Start</a>
    </div>
    </form>   
    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        try {
            var pageTracker = _gat._getTracker("UA-16069586-1");
            pageTracker._setDomainName(".britz.com.au");
            pageTracker._setAllowLinker(true);
            pageTracker._trackPageview();
            pageTracker._addTrans(
                "<%= BookingId %>", // Aurora Booking ID - required
                "", // Not required
                "<%= Price %>" // total - required
            );
            pageTracker._addItem(
                "<%= BookingId %>", // Aurora ID
                "<%= CountryOfTravel %>", // SKU Country of Origincode - required swapped with Country of Travel
                "Explorer", // Vehicle Name
                "<%= LeadTime %>", // swap with: length of hire in days
                "<%= Price %>", // unit price - required
                "1" // quantity - required
            );
            pageTracker._trackTrans();
        } catch (err) { }
    </script>   
</body>
</html>
