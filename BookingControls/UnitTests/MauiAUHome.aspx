﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MauiAUHome.aspx.cs" Inherits="UnitTests_MauiAUHome" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head id="ctl00_Head1">
    <meta name="GENERATOR" content="Microsoft SharePoint" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Expires" content="0" />
    <meta name="language" scheme="ISO639-2" content="eng" />
    <meta name="keywords" content="campervan hire, motorhome hire, camper hire, motorhome rental, maui, Australia" />
    <meta name="description" content="Maui offers motorhome rental, RV, car and campervan hire in Australia.  See our full range of motorhomes & campervans and book your campervan holiday with Maui." />
    <meta name="robots" content=",index,follow" />
    <meta name="msvalidate.01" content="FBA510F5D9F039DFC5E5CB6787E1F5B5" />
    <title>maui Motorhome Rental, RV, Car &amp; Campervan Hire Australia </title>
    <!--Styles used for positioning, font and spacing definitions-->
    <link href="http://www.maui.com.au/SiteCollectionDocuments/css/jquery-ui-custom-theme/jquery-ui-1.8.4.custom.css"
        rel="Stylesheet" type="text/css" />
    <link href="http://www.maui.com.au/SitecollectionDocuments/css/main.css" rel="Stylesheet" type="text/css" />
    <link rel="shortcut icon" href="http://www.maui.com.au/SitecollectionDocuments/images/favicon.ico" />
    <script src="http://www.maui.com.au/_layouts/1033/init.js?rev=qX%2BG3yl4pldKy9KbPLXf9w%3D%3D"></script>
    <script type="text/javascript" language="javascript" src="http://www.maui.com.au/_layouts/1033/non_ie.js?rev=yfNry4hY0Gwa%2FPDNGrqXVg%3D%3D"></script>
    <!--Sharepoint styles required for authors only-->
    <!--Placeholder for additional overrides-->
    <script>

        function ProcessImn()
        { }
        function ProcessImnMarkers()
        { }
    </script>
    <style type="text/css">
        .ms-bodyareaframe
        {
            padding: 0px;
        }
    </style>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.4/jquery-ui.min.js"
        type="text/javascript"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
    <script src="http://www.maui.com.au/SitecollectionDocuments/js/easing.js" type="text/javascript"></script>
    <script src="http://www.maui.com.au/SitecollectionDocuments/js/jquery.tabs.pack.js" type="text/javascript"></script>
    <script src="http://www.maui.com.au/SitecollectionDocuments/js/jquery.jtruncate.pack.js" type="text/javascript"></script>
    <script src="http://www.maui.com.au/SitecollectionDocuments/js/jquery.effects.core.js" type="text/javascript"></script>
    <script src="http://www.maui.com.au/SitecollectionDocuments/js/core-functions.js" type="text/javascript"></script>
    <script src="http://www.maui.com.au/SitecollectionDocuments/js/jquery.megamenu.js" type="text/javascript"></script>
    <script src="http://www.maui.com.au/SitecollectionDocuments/js/jquery.simplyscroll-1.0.4.js" type="text/javascript"></script>
    <script src="http://www.maui.com.au/SitecollectionDocuments/js/ytplayer.js" type="text/javascript"></script>
    <script src="http://www.maui.com.au/SitecollectionDocuments/js/main.js" type="text/javascript"></script>
    <!--Addthis-->
    <script type="text/javascript">
        var addthis_config = {
            ui_offset_top: 0,
            ui_offset_left: 0,
            data_track_clickback: true
        }
    </script>
    <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=mauiaustralia"></script>
    <script type="text/javascript" src="JS/Selectbox.js"></script>
    <!--End Addthis-->
    <!-- Google Analytics Asychronous Code MAUI AU-->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-2806904-1']);
        _gaq.push(['_setDomainName', '.maui.com.au']);
        _gaq.push(['_setAllowHash', false]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <!--Analytics end-->
    <!-- BoldChat VisitInfo generator -->
    <script type="text/javascript">
        function getRefID() {
            var chars = "0123456789";
            var string_length = 6;
            var rstring = '';
            for (var i = 0; i < string_length; i++) {
                var rnum = Math.floor(Math.random() * chars.length);
                rstring += chars.substring(rnum, rnum + 1);
            }
            return rstring;
        }
        var visitInfoVal = "CHAT" + getRefID();
    </script>

    <style type="text/css">
        #dropPickupLocation
        {
            width: 190px;
        }
        
        .selectBox
        {
            position: relative;
            z-index: 10000;
            height: 300px;
            padding:0px 0px;
            background:#a88f70;
            overflow-y:scroll;
        }
        
        a.selectBoxItem
        {
            display:block;
            color: #fff;
            text-decoration:none;
            padding:10px;
        }
        
        a.selectBoxItem:hover,
        a.selectBoxItem:hover span.selectBoxMatchedText,
        a.selectBoxItemSelected,
        a.selectBoxItemSelected span.selectBoxMatchedText
        {
            background: #1f474f;
            color:#fff;
        }
        
        span.selectBoxMatchedText
        {
            color:#1f474f;
        }
        
        div.selectBoxMessage
        {
            background:#1f474f;
            padding:10px;
            color:#fff;
        }
        
        div.selectBoxGroup
        {
            padding:10px;
            border-bottom: 1px dotted #fff;
        }
    </style>

    <!-- BoldChat VisitInfo generator end -->
</head>
<body class="body" scroll="yes" onload="javascript:_spBodyOnLoadWrapper();">
    <form name="aspnetForm" method="post" action="/" onsubmit="javascript:return WebForm_OnSubmit();"
    id="aspnetForm">
    <div>
        <input type="hidden" name="__SPSCEditMenu" id="__SPSCEditMenu" value="true" />
        <input type="hidden" name="MSOWebPartPage_PostbackSource" id="MSOWebPartPage_PostbackSource" value="" />
        <input type="hidden" name="MSOTlPn_SelectedWpId" id="MSOTlPn_SelectedWpId" value="" />
        <input type="hidden" name="MSOTlPn_View" id="MSOTlPn_View" value="0" />
        <input type="hidden" name="MSOTlPn_ShowSettings" id="MSOTlPn_ShowSettings" value="False" />
        <input type="hidden" name="MSOGallery_SelectedLibrary" id="MSOGallery_SelectedLibrary" value="" />
        <input type="hidden" name="MSOGallery_FilterString" id="MSOGallery_FilterString" value="" />
        <input type="hidden" name="MSOTlPn_Button" id="MSOTlPn_Button" value="none" />
        <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
        <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
        <input type="hidden" name="__REQUESTDIGEST" id="__REQUESTDIGEST" value="0x0DF0F7E2F86CB617F6636FE32FF8A4F2A60B964F6BC01F1A9FFA724ACF4D2E73956BC9ACA30372A4F3023A1C16A388C5EEAC8313D24A1B8BABEA7A33A63100A8,03 Mar 2013 21:09:23 -0000" />
        <input type="hidden" name="MSOAuthoringConsole_FormContext" id="MSOAuthoringConsole_FormContext" value="" />
        <input type="hidden" name="MSOAC_EditDuringWorkflow" id="MSOAC_EditDuringWorkflow" value="" />
        <input type="hidden" name="MSOSPWebPartManager_DisplayModeName" id="MSOSPWebPartManager_DisplayModeName" value="Browse" />
        <input type="hidden" name="MSOWebPartPage_Shared" id="MSOWebPartPage_Shared" value="" />
        <input type="hidden" name="MSOLayout_LayoutChanges" id="MSOLayout_LayoutChanges" value="" />
        <input type="hidden" name="MSOLayout_InDesignMode" id="MSOLayout_InDesignMode" value="" />
        <input type="hidden" name="MSOSPWebPartManager_OldDisplayModeName" id="MSOSPWebPartManager_OldDisplayModeName" value="Browse" />
        <input type="hidden" name="MSOSPWebPartManager_StartWebPartEditingName" id="MSOSPWebPartManager_StartWebPartEditingName" value="false" />
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUBMA9kFgJmD2QWBAICD2QWBAIID2QWAmYPZBYCAgEPFgIeE1ByZXZpb3VzQ29udHJvbE1vZGULKYgBTWljcm9zb2Z0LlNoYXJlUG9pbnQuV2ViQ29udHJvbHMuU1BDb250cm9sTW9kZSwgTWljcm9zb2Z0LlNoYXJlUG9pbnQsIFZlcnNpb249MTIuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49NzFlOWJjZTExMWU5NDI5YwFkAg8PZBYEAgEPFgIeB1Zpc2libGVoZAIDDxYCHwFoZAIED2QWBAIFD2QWBgIBD2QWAgIBDxYCHwFoFgJmD2QWBAICD2QWAgIDDxYCHwFoZAIDDw8WAh4JQWNjZXNzS2V5BQEvZGQCBw9kFgICAg8PFgIfAWcWAh4Fc3R5bGUFDmRpc3BsYXk6YmxvY2s7ZAIJD2QWAgIDD2QWAgIBDw8WAh8BZ2QWBAIBDw8WAh8BaGQWHAIBDw8WAh8BaGRkAgMPFgIfAWhkAgUPDxYCHwFoZGQCBw8WAh8BaGQCCQ8PFgIfAWhkZAILDw8WAh8BaGRkAg0PDxYCHwFoZGQCDw8PFgQeB0VuYWJsZWRoHwFoZGQCEQ8PFgIfAWhkZAITDw8WBB8EaB8BaGRkAhUPDxYCHwFoZGQCFw8WAh8BaGQCGQ8WAh8BaGQCGw8PFgIfAWdkZAIDDw8WAh8BZ2QWBgIBDw8WAh8BZ2RkAgMPDxYCHwFnZGQCBQ8PFgIfAWdkZAIHD2QWBgIFDw8WAh4LUGFyYW1WYWx1ZXMyoAQAAQAAAP////8BAAAAAAAAAAwCAAAAWE1pY3Jvc29mdC5TaGFyZVBvaW50LCBWZXJzaW9uPTEyLjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPTcxZTliY2UxMTFlOTQyOWMFAQAAAD1NaWNyb3NvZnQuU2hhcmVQb2ludC5XZWJQYXJ0UGFnZXMuUGFyYW1ldGVyTmFtZVZhbHVlSGFzaHRhYmxlAQAAAAVfY29sbAMcU3lzdGVtLkNvbGxlY3Rpb25zLkhhc2h0YWJsZQIAAAAJAwAAAAQDAAAAHFN5c3RlbS5Db2xsZWN0aW9ucy5IYXNodGFibGUHAAAACkxvYWRGYWN0b3IHVmVyc2lvbghDb21wYXJlchBIYXNoQ29kZVByb3ZpZGVyCEhhc2hTaXplBEtleXMGVmFsdWVzAAADAwAFBQsIHFN5c3RlbS5Db2xsZWN0aW9ucy5JQ29tcGFyZXIkU3lzdGVtLkNvbGxlY3Rpb25zLklIYXNoQ29kZVByb3ZpZGVyCOxROD8DAAAACgoLAAAACQQAAAAJBQAAABAEAAAAAwAAAAYGAAAABlVzZXJJRAYHAAAABVdlYklEBggAAAAFVG9kYXkQBQAAAAMAAAAGCQAAAA9DdXJyZW50VXNlck5hbWUGCgAAAAdSb290V2ViBgsAAAAUMjAxMy0wMy0wNFQxMDowOToyM1oLZGQCBw9kFhwCBw8WAh8ACysEAWQCCQ8WAh8ACysEAWQCCw8WAh8ACysEAWQCDQ8WAh8ACysEAWQCDw8WAh8ACysEAWQCEQ8WAh8ACysEAWQCEw8WAh8ACysEAWQCFQ8WAh8ACysEAWQCFw8WAh8ACysEAWQCGQ8WAh8ACysEAWQCGw8WAh8ACysEAWQCHQ8WAh8ACysEAWQCHw8WAh8ACysEAWQCIQ8PFgIfBTLcBAABAAAA/////wEAAAAAAAAADAIAAABYTWljcm9zb2Z0LlNoYXJlUG9pbnQsIFZlcnNpb249MTIuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49NzFlOWJjZTExMWU5NDI5YwUBAAAAPU1pY3Jvc29mdC5TaGFyZVBvaW50LldlYlBhcnRQYWdlcy5QYXJhbWV0ZXJOYW1lVmFsdWVIYXNodGFibGUBAAAABV9jb2xsAxxTeXN0ZW0uQ29sbGVjdGlvbnMuSGFzaHRhYmxlAgAAAAkDAAAABAMAAAAcU3lzdGVtLkNvbGxlY3Rpb25zLkhhc2h0YWJsZQcAAAAKTG9hZEZhY3RvcgdWZXJzaW9uCENvbXBhcmVyEEhhc2hDb2RlUHJvdmlkZXIISGFzaFNpemUES2V5cwZWYWx1ZXMAAAMDAAUFCwgcU3lzdGVtLkNvbGxlY3Rpb25zLklDb21wYXJlciRTeXN0ZW0uQ29sbGVjdGlvbnMuSUhhc2hDb2RlUHJvdmlkZXII7FE4PwYAAAAKCgsAAAAJBAAAAAkFAAAAEAQAAAAEAAAABgYAAAAGVXNlcklEBgcAAAAGV2ViVVJMBggAAAAFVG9kYXkGCQAAAAhMaXN0TmFtZRAFAAAABAAAAAYKAAAAD0N1cnJlbnRVc2VyTmFtZQYLAAAAFHtzaXRlY29sbGVjdGlvbnJvb3R9BgwAAAAUMjAxMy0wMy0wNFQxMDowOToyNFoGDQAAABpNYXVpIEhvbWVwYWdlIENvbnRlbnQgTGlzdAtkZAIPD2QWAgIBDw9kFgQeB29uY2xpY2sFeWRvY3VtZW50LmZvcm1zWzBdLm9ua2V5cHJlc3MgPSBuZXcgRnVuY3Rpb24oInJldHVybiBXZWJGb3JtX0ZpcmVEZWZhdWx0QnV0dG9uKGV2ZW50LCAnY3RsMDBfU2VhcmNoQm94MV9TZWFyY2hCdXR0b24nKTsiKTseB29uZm9jdXMFeWRvY3VtZW50LmZvcm1zWzBdLm9ua2V5cHJlc3MgPSBuZXcgRnVuY3Rpb24oInJldHVybiBXZWJGb3JtX0ZpcmVEZWZhdWx0QnV0dG9uKGV2ZW50LCAnY3RsMDBfU2VhcmNoQm94MV9TZWFyY2hCdXR0b24nKTsiKTtkGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYBBR1jdGwwMCRTZWFyY2hCb3gxJFNlYXJjaEJ1dHRvblyM8QSmDi66gm0x4D+jgd1pkXJO" />
    </div>
    <script type="text/javascript">
//<![CDATA[
        var theForm = document.forms['aspnetForm'];
        if (!theForm) {
            theForm = document.aspnetForm;
        }
        function __doPostBack(eventTarget, eventArgument) {
            if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
                theForm.__EVENTTARGET.value = eventTarget;
                theForm.__EVENTARGUMENT.value = eventArgument;
                theForm.submit();
            }
        }
//]]>
    </script>
    <script src="http://www.maui.com.au/WebResource.axd?d=lGanEeVG7jaFrh3OxLtDFw2&amp;t=633408290533593750" type="text/javascript"></script>
    <script>        var MSOWebPartPageFormName = 'aspnetForm';</script>
    <script type="text/JavaScript" language="JavaScript">
        <!--
                var L_Menu_BaseUrl = "";
                var L_Menu_LCID = "1033";
                var L_Menu_SiteTheme = "";
        //-->
    </script>
    <script type="text/javascript">
    //<![CDATA[
        function DoCallBack(filterText) {
            WebForm_DoCallback('ctl00$g_2adf51ea_8ec4_4b7d_b8a5_a8fa9d63a682', filterText, UpdateFilterCallback, 0, CallBackError, true);
        }
        function CallBackError(result, clientsideString) {
        }
        function WebForm_OnSubmit() {
            UpdateFormDigest('\u002f', 1440000); return _spFormOnSubmitWrapper();
            return true;
        }
    //]]>
    </script>
    <div>
    </div>
    <div id="ctl00_MSO_ContentDiv" class="root">
        <div id="page">
            <script type="text/javascript">
                if (typeof $(document).getUrlParam('popup') != 'object')
                    $('#page').addClass('popup');
            </script>
            <!-- Start CrossSell Header -->
            <div id="crossSellWrapper">
                <div id="crossSell">
                    <!--<div id="crossSellLogo"><img src="/SiteCollectionImages/Core/logo-thl.jpg" height="25" width="85" alt="thl"></div>-->
                    <ul class="crossSellNav">
                        <li><span class="countryOn"><span class="catTitle">RENTALS</span> Australia</span></li>
                        <li><a href="http://aurentals.keacampers.com/?utm_source=maui.com.au&utm_medium=referral&utm_campaign=CrossLink"
                            title="KEA campers Australia" class="maui">KEA</a></li>
                        <li><span class="mauiCurrent">Maui</span></li>
                        <li><a href="http://www.britz.com.au/?utm_source=maui.com.au&utm_medium=referral&utm_campaign=CrossLink"
                            class="maui">Britz</a></li>
                        <li><a href="http://www.mightycampers.com.au/?utm_source=maui.com.au&utm_medium=referral&utm_campaign=CrossLink"
                            class="maui">Mighty</a></li>
                    </ul>
                    <div id="crossSellCountry">
                        <ul class="crossSellNav" style="border-left: 1px solid #f5f5f5;">
                            <li><span class="country"><span class="catTitle">RENTALS</span>New Zealand</span>
                                <ul class="crossSellNavDD">
                                    <li class="mauiC"><a href="http://www.nzrentals.keacampers.com/?utm_source=maui.com.au&utm_medium=referral&utm_campaign=CrossLink">
                                        KEA</a></li>
                                    <li class="mauiC"><a href="http://www.maui.co.nz/?utm_source=maui.com.au&utm_medium=referral&utm_campaign=CrossLink">
                                        Maui</a></li>
                                    <li class="mauiC"><a href="http://www.unitedcampervans.co.nz/?utm_source=maui.com.au&utm_medium=referral&utm_campaign=CrossLink">
                                        United</a></li>
                                    <li class="mauiC"><a href="http://www.britz.co.nz/?utm_source=maui.com.au&utm_medium=referral&utm_campaign=CrossLink">
                                        Britz</a></li>
                                    <li class="mauiC"><a href="http://www.alphacampervans.co.nz/?utm_source=maui.com.au&utm_medium=referral&utm_campaign=CrossLink">
                                        Alpha</a></li>
                                    <li class="mauiC"><a href="http://www.mightycampers.co.nz/?utm_source=maui.com.au&utm_medium=referral&utm_campaign=CrossLink">
                                        Mighty</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="crossSellNav" style="border-left: 1px solid #f5f5f5;">
                            <li><span class="country"><a href="http://www.roadbearrv.com"><span class="catTitle">
                                RENTALS</span>USA</a></span></li>
                        </ul>
                        <ul class="crossSellNav" style="border-left: 1px solid #f5f5f5; border-right: 1px solid #f5f5f5;">
                            <li><span class="country"><a href="http://www.motekvehicles.com/?utm_source=maui.com.au&utm_medium=referral&utm_campaign=CrossLink">
                                <span class="catTitle">BUY</span> a Campervan</a></span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- End CrossSell Header -->
            <div id="header">
                <div id="header-top">
                    <div id="header-text">
                    </div>
                    <div id="quick-link">
                        <ul id="quick-link-menu" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                            xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                            xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                            <li><a href="http://www.maui.com.au/online-rental-check-in" title="checkin" id="check-in">Check in online</a></li><li>
                                <a href="http://www.maui.com.au/contact-maui-motorhome-hire-australia" title="needhelp" id="need-help">contact
                                    us</a></li><li><a href="http://www.thlonline.com/AgentMediaCentre/AgentResources/maui/Pages/Maui-Resources.aspx"
                                        title="travelagent" id="travel-agent">travel agents</a></li><li><a href="http://www.thlonline.com/Media/Pages/default.aspx"
                                            title="media" id="media">media</a></li><li id="flag-container"><a href="#" title="flag"
                                                id="flagicon" class="flag">language</a><div class="groupitems">
                                                    <ul>
                                                        <li class="first">Change language:</li><li>
                                                            <img alt="Français" border="0" src="http://www.maui.com.au/SiteCollectionImages/assets/French-flag.gif"
                                                                style="border: 0px solid;"><a href="/Fr">Français</a></li><li>
                                                                    <img alt="Nederlands" border="0" src="http://www.maui.com.au/SiteCollectionImages/assets/Dutch-Flag.gif"
                                                                        style="border: 0px solid;"><a href="http://www.maui.com.au/een-motorhome-australie?lang=dutch">Nederlands</a></li><li>
                                                                            <img alt="Deutsch" border="0" src="http://www.maui.com.au/SiteCollectionImages/assets/German-Flag.gif"
                                                                                style="border: 0px solid;"><a href="http://www.maui.com.au/australien-wohnmobile-campervans?lang=german">Deutsch</a></li><li>
                                                                                    <img alt="" border="0" src="http://www.maui.com.au/PublishingImages/english-flag.jpg" style="border: 0px solid;"><a
                                                                                        href="/" title="English">English</a></li></ul>
                                                </div>
                                            </li>
                        </ul>
                    </div>
                </div>
                <div id="header-bottom">
                    <a id="maui-logo" href="/">maui Motorhome, Car Rental and Campervan Hire Australia
                    </a>
                    <div id="menu">
                        <ul id="mega-menu">
                            <li id="menu-home" class="level1 vehicle" xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                <a href="http://www.maui.com.au/motorhome-hire">campervan hire</a><div class="level2">
                                    <div class="vertical">
                                        <div class="group-header">
                                            <h3>
                                                <a href="http://www.maui.com.au/motorhome-hire">our campervan hire range</a></h3>
                                            <div class="description">
                                                whatever your choice you’ll experience apartment living on the road with comfort,
                                                space and style</div>
                                        </div>
                                        <div class="groupitems">
                                            <ul>
                                                <li class="groupitem first"><a href="http://www.maui.com.au/motorhome-hire/platinum-campervans-ultima"
                                                    title="Ultima" class="pageLink">Ultima</a><span class="imgContainer"><img alt="Ultima Campervan"
                                                        border="0" src="http://www.maui.com.au/SiteCollectionImages/Campervans/auavm.2BTSM/auavm.2BTSM-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/motorhome-hire/platinum-campervans-ultima" title="Ultima Campervan">Ultima</a></h3>
                                                    <p>
                                                        sleeps up to 2<br>
                                                        <span class="description">aussie cuisine courtesy of exterior BBQ</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/motorhome-hire/platinum-campervans-beach" title="Platinum Beach"
                                                    class="pageLink">Platinum Beach</a><span class="imgContainer"><img alt="Platinum Beach Motorhome"
                                                        border="0" src="http://www.maui.com.au/SiteCollectionImages/Campervans/auavm.4BMP/auavm.4BMP-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/motorhome-hire/platinum-campervans-beach" title="platinum beach motorhome">
                                                                Platinum Beach</a></h3>
                                                    <p>
                                                        4 person layout<br>
                                                        <span class="description">unparalleled personal space</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/motorhome-hire/platinum-campervans-river" title="Platinum River"
                                                    class="pageLink">Platinum River</a><span class="imgContainer"><img alt="Platinum River Motorhome"
                                                        border="0" src="http://www.maui.com.au/SiteCollectionImages/Campervans/auavm.6BMPC/auavm.6BMPC-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/motorhome-hire/platinum-campervans-river" title="platinum river motorhome">
                                                                Platinum River</a></h3>
                                                    <p>
                                                        6 person layout<br>
                                                        <span class="description">space &amp; storage designed for families</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/motorhome-hire/platinum-campervans-forest" title="Platinum Forest"
                                                    class="pageLink">Platinum Forest</a><span class="imgContainer"><img alt="Platinum Forest Motorhome"
                                                        border="0" src="http://www.maui.com.au/SiteCollectionImages/Campervans/auavm.6BMP/auavm.6BMP-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/motorhome-hire/platinum-campervans-forest" title="platinum forest motorhome">
                                                                Platinum Forest</a></h3>
                                                    <p>
                                                        sleeps up to 6<br>
                                                        <span class="description">making it easy – permanent rear bed</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.motekvehicles.com" title="Motek Vehicle Sales"
                                                    class="pageLink">Motek Vehicle Sales</a><span class="imgContainer"><img alt="Motek Vehicle Sales"
                                                        border="0" src="http://www.maui.com.au/SiteCollectionImages/promotions/MM-motek.gif" style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.motekvehicles.com">Buy a motorhome</a></h3>
                                                    <p>
                                                        Built rental tough<br>
                                                        <span class="description">New &amp; Used Motorhomes, Campervans &amp; Caravans for Sale</span><br>
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="vertical">
                                        <div class="group-header">
                                            <h3>
                                                other info</h3>
                                            <div class="description">
                                            </div>
                                        </div>
                                        <div class="groupitems noimage">
                                            <ul>
                                                <li><a href="http://www.maui.com.au/motorhome-hire/compare-maui-motorhomes" title="Campervan comparison chart">
                                                    Compare all maui campervans</a></li><li><a href="http://www.maui.com.au/motorhome-hire-faqs" title="Frequently asked questions">
                                                        Frequently asked questions</a></li><li><a href="http://www.maui.com.au/motorhome-hire/buy-a-motorhome">Buy
                                                            a motorhome or campervan</a></li></ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li id="menu-car" class="level1 vehicle" xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                <a href="http://www.maui.com.au/car-rental">car rental</a><div class="level2">
                                    <div class="vertical">
                                        <div class="group-header">
                                            <h3>
                                                car rental selection</h3>
                                            <div class="description">
                                                maui offers an extensive fleet of rental cars. From smaller Economy cars to larger
                                                7 seater vans, we can provide a vehicle to suit your needs</div>
                                        </div>
                                        <div class="groupitems">
                                            <ul>
                                                <li class="groupitem first"><a href="http://www.maui.com.au/car-rental/economy-car" title="economy car"
                                                    class="pageLink">economy car</a><span class="imgContainer"><img alt="Economy Car"
                                                        border="0" src="http://www.maui.com.au/SiteCollectionImages/Cars/auacm.ecmr/auacm.ecmr-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/car-rental/economy-car" title="Economy Car">Economy car</a></h3>
                                                    <p>
                                                        up to 4 adults<br>
                                                        <span class="description">Hyundai Getz or similar</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/car-rental/compact-car" title="compact car hire"
                                                    class="pageLink">compact car hire</a><span class="imgContainer"><img alt="Compact car"
                                                        border="0" src="http://www.maui.com.au/SiteCollectionImages/Cars/auacm.ccar/auacm.ccar-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/car-rental/compact-car">Compact car</a></h3>
                                                    <p>
                                                        up to 4 adults<br>
                                                        <span class="description">Toyota Corolla or similar</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/car-rental/intermediate-car" title="Intermediate car"
                                                    class="pageLink">Intermediate car</a><span class="imgContainer"><img alt="Intermediate Car"
                                                        border="0" src="http://www.maui.com.au/SiteCollectionImages/Cars/auacm.icar/auacm.icar-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/car-rental/intermediate-car">Intermediate car</a></h3>
                                                    <p>
                                                        up to 4 adults<br>
                                                        <span class="description">Hyundai Elantra or similar</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/car-rental/mid-size-car" title="Mid-Size Car" class="pageLink">
                                                    Mid-Size Car</a><span class="imgContainer"><img alt="Mid-size car" border="0" src="http://www.maui.com.au/SiteCollectionImages/Cars/auacm.SCAR/auacm.scar-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/car-rental/mid-size-car" title="Mid-Size Sedan">Mid-Size Sedan</a></h3>
                                                    <p>
                                                        up to 5 adults<br>
                                                        <span class="description">Mitsubishi Lancer or similar</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/car-rental/medium-wagon" title="Mid-Size wagon" class="pageLink">
                                                    Mid-Size wagon</a><span class="imgContainer"><img alt="Intermediate Wagon" border="0"
                                                        src="http://www.maui.com.au/SiteCollectionImages/Cars/auacm.iwar/auacm.iwar-Clearcut-144.jpg" style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/car-rental/medium-wagon" title="Mid-Size wagon">Mid-Size Wagon</a></h3>
                                                    <p>
                                                        up to 5 adults<br>
                                                        <span class="description">Mitsubishi Outlander or similar</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/car-rental/full-size-car" title="Full-Size car" class="pageLink">
                                                    Full-Size car</a><span class="imgContainer"><img alt="Full-size car" border="0" src="http://www.maui.com.au/SiteCollectionImages/Cars/auacm.fcar/auacm.fcar-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/car-rental/full-size-car" title="People Mover Car Hire">Full-Size car</a></h3>
                                                    <p>
                                                        up to 5 adults<br>
                                                        <span class="description">Ford XR6 or similar</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/car-rental/urban-wagon" title="Urban Wagon" class="pageLink">
                                                    Urban Wagon</a><span class="imgContainer"><img alt="Urban Wagon" border="0" src="http://www.maui.com.au/SiteCollectionImages/Cars/auacm.uwar/auacm.uwar-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/car-rental/urban-wagon" title="Urban Wagon car hire Australia">Urban Wagon</a></h3>
                                                    <p>
                                                        up to 5 adults<br>
                                                        <span class="description">Ford Territory or similar</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/car-rental/8-seater-van" title="8 Seater Van" class="pageLink">
                                                    8 Seater Van</a><span class="imgContainer"><img alt="8 seater van" border="0" src="http://www.maui.com.au/SiteCollectionImages/Cars/auacm.ivar/auacm.ivar-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/car-rental/8-seater-van" title="8 Seater Van hire">8 Seater Van</a></h3>
                                                    <p>
                                                        up to 8 adults<br>
                                                        <span class="description">Toyota Tarago or similar</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/ourcars/Pages/NewCompactCarHire.aspx" title="New Compact Car"
                                                    class="pageLink">New Compact Car</a><span class="imgContainer"><img alt="New Compact Car"
                                                        border="0" src="http://www.maui.com.au/SiteCollectionImages/Cars/auacm.cdar/auacm.cdar-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/ourcars/Pages/NewCompactCarHire.aspx" title="New Compact Car">New Compact
                                                                Car</a></h3>
                                                    <p>
                                                        up to 4 adults<br>
                                                        <span class="description">Hyundai i20 3dr or similar</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/ourcars/Pages/NewIntermediateCarHire.aspx" title="New Intermediate Car"
                                                    class="pageLink">New Intermediate Car</a><span class="imgContainer"><img alt="New Intermediate Car"
                                                        border="0" src="http://www.maui.com.au/SiteCollectionImages/Cars/auacm.idar/auacm.idar-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/ourcars/Pages/NewIntermediateCarHire.aspx" title="New Intermediate Car">New
                                                                Intermediate Car</a></h3>
                                                    <p>
                                                        up to 5 adults<br>
                                                        <span class="description">Hyundai Accent Sedan or similar</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/ourcars/Pages/StandardCar.aspx" title="Standard Card"
                                                    class="pageLink">Standard Card</a><span class="imgContainer"><img alt="Standard Card"
                                                        border="0" src="http://www.maui.com.au/SiteCollectionImages/Cars/auacm.sdar/auacm.sdar-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/ourcars/Pages/StandardCar.aspx" title="Standard Card">Standard Card</a></h3>
                                                    <p>
                                                        up to 5 adults<br>
                                                        <span class="description">Hyundai Elantra or similar</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/ourcars/Pages/NewFullSizeCarRental.aspx" title="New Full Size Car"
                                                    class="pageLink">New Full Size Car</a><span class="imgContainer"><img alt="Full Size Car"
                                                        border="0" src="http://www.maui.com.au/SiteCollectionImages/Cars/auacm.fdar/auacm.fdar-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/ourcars/Pages/NewFullSizeCarRental.aspx" title="New Full Size Car">New Full
                                                                Size Car</a></h3>
                                                    <p>
                                                        up to 5 adults<br>
                                                        <span class="description">Volkswagen Passat or similar</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/ourcars/Pages/IntermediateAWD.aspx" title="Intermediate AWD"
                                                    class="pageLink">Intermediate AWD</a><span class="imgContainer"><img alt="Intermediate AWD"
                                                        border="0" src="http://www.maui.com.au/SiteCollectionImages/Cars/auacm.ifdr/auacm.ifdr-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/ourcars/Pages/IntermediateAWD.aspx" title="Intermediate AWD">Intermediate
                                                                AWD</a></h3>
                                                    <p>
                                                        up to 5 adults<br>
                                                        <span class="description">Nissan X-Trail or similar</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/ourcars/Pages/FullsizeWagonHire.aspx" title="Full Size Wagon"
                                                    class="pageLink">Full Size Wagon</a><span class="imgContainer"><img alt="Full Size Wagon"
                                                        border="0" src="http://www.maui.com.au/SiteCollectionImages/Cars/auacm.fwar/auacm.fwar-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/ourcars/Pages/FullsizeWagonHire.aspx" title="Full Size Wagon">Full Size Wagon</a></h3>
                                                    <p>
                                                        up to 5 adults<br>
                                                        <span class="description">Subaru Outback or similar</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/ourcars/Pages/Large4WD.aspx" title="Large 4WD" class="pageLink">
                                                    Large 4WD</a><span class="imgContainer"><img alt="Large 4WD" border="0" src="http://www.maui.com.au/SiteCollectionImages/Cars/auacm.ffar/auacm.ffar-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/ourcars/Pages/Large4WD.aspx" title="Large 4WD">Large 4WD</a></h3>
                                                    <p>
                                                        up to 5 adults<br>
                                                        <span class="description">Mitsubishi Pajero or similar</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/ourcars/Pages/Premium8SeaterVan.aspx" title="Premium 8 Seater"
                                                    class="pageLink">Premium 8 Seater</a><span class="imgContainer"><img alt="Premium 8 Seater"
                                                        border="0" src="http://www.maui.com.au/SiteCollectionImages/Cars/auacm.pvar/auacm.pvar-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/ourcars/Pages/Premium8SeaterVan.aspx" title="Premium 8 Seater">Premium 8 Seater</a></h3>
                                                    <p>
                                                        up to 8 adults<br>
                                                        <span class="description">Kia Grand Canival or similar</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/ourcars/Pages/Intermediate12Seater.aspx" title="Intermediate 12 Seater"
                                                    class="pageLink">Intermediate 12 Seater</a><span class="imgContainer"><img alt="Intermediate 12 Seater"
                                                        border="0" src="http://www.maui.com.au/SiteCollectionImages/Cars/auacm.ivar/auacm.ivar-Clearcut-144.jpg"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/ourcars/Pages/Intermediate12Seater.aspx" title="Intermediate 12 Seater">Intermediate
                                                                12 Seater</a></h3>
                                                    <p>
                                                        up to 12 adults<br>
                                                        <span class="description">Toyota Communter or similar</span><br>
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="vertical">
                                        <div class="group-header">
                                            <h3>
                                                other info</h3>
                                            <div class="description">
                                            </div>
                                        </div>
                                        <div class="groupitems noimage">
                                            <ul>
                                                <li><a href="http://www.maui.com.au/car-rental/compare-maui-cars">Compare all maui cars</a></li></ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li id="menu-promotion" class="level1" xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                <a href="http://www.maui.com.au/motorhome-hire-promotions">promotions</a><div class="level2">
                                    <div class="vertical">
                                        <div class="group-header">
                                            <h3>
                                                our campervan hire offers</h3>
                                            <div class="description">
                                            </div>
                                        </div>
                                        <div class="groupitems">
                                            <ul>
                                                <li class="groupitem first"><a href="http://www.maui.com.au/motorhome-hire-promotions/25-off-april-to-june"
                                                    title="25% off april to june travel" class="pageLink">25% off april to june travel</a><span
                                                        class="imgContainer"><img alt="25% off april to june travel" border="0" src="http://www.maui.com.au/SiteCollectionImages/promotions/Mega-Menu_25off_april_june.png"
                                                            style="border: 0px solid;"></span><h3>
                                                                <a href="http://www.maui.com.au/motorhome-hire-promotions/25-off-april-to-june">Travel by 30 June 2013</a></h3>
                                                    <p>
                                                        book before 31 March 2013<br>
                                                        <span class="description">All locations</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/motorhome-hire-promotions/early-booking-rates" title="Early booking rates"
                                                    class="pageLink">Early booking rates</a><span class="imgContainer"><img alt="Early booking rates"
                                                        border="0" src="http://www.maui.com.au/SiteCollectionImages/promotions/MM-Promo-early-booking-rates.png"
                                                        style="border: 0px solid;"></span><h3>
                                                            <a href="http://www.maui.com.au/motorhome-hire-promotions/early-booking-rates">Travel by 31 Mar 2013</a></h3>
                                                    <p>
                                                        <br>
                                                        <span class="description">All locations</span><br>
                                                    </p>
                                                </li>
                                                <li class="groupitem"><a href="http://www.maui.com.au/motorhome-hire-promotions/mecure-tasmania-pamper-package"
                                                    title="Tasmania Pamper Package" class="pageLink">Tasmania Pamper Package</a><span
                                                        class="imgContainer"><img alt="Tasmania Pamper Package" border="0" src="http://www.maui.com.au/SiteCollectionImages/promotions/MM-promo-PamperPackage.png"
                                                            style="border: 0px solid;"></span><h3>
                                                                <a href="http://www.maui.com.au/motorhome-hire-promotions/mecure-tasmania-pamper-package"></a>
                                                            </h3>
                                                    <p>
                                                        valid till 31 Dec 2013<br>
                                                        <span class="description">Mercure Hobart &amp; Launceston only</span><br>
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="vertical">
                                        <div class="group-header">
                                            <h3>
                                                more great value offers</h3>
                                            <div class="description">
                                            </div>
                                        </div>
                                        <div class="groupitems noimage">
                                            <ul>
                                                <li><a href="http://www.maui.com.au/motorhome-hire-promotions/long-hire-rates">Long hire rates</a></li><li>
                                                    <a href="http://www.maui.com.au/why-choose-maui-campervans/conservation-volunteers">Conservation volunteers</a></li><li>
                                                        <a href="http://www.maui.com.au/motorhome-hire-promotions/multi-hire-discounts">Multi-hire discounts</a></li><li>
                                                            <a href="http://www.maui.com.au/motorhome-hire-promotions/convoys">Motorhome convoys</a></li><li><a href="/motorhome-hire-promotions/feature-film-hires">
                                                                Film trailer hire</a></li><li><a href="http://www.maui.com.au/motorhome-hire-promotions/relocations">Campervan
                                                                    relocations</a></li><li><a href="http://www.maui.com.au/motorhome-hire-promotions/gift-vouchers">maui gift
                                                                        vouchers</a></li></ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li id="menu-location" class="level1 location" xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                <a href="http://www.maui.com.au/rentals-australia">our campervan and car locations</a><div class="level2">
                                    <div class="horizontal">
                                        <h3>
                                            <a href="http://www.maui.com.au/rentals-australia/campervan-locations" title="Campervan hire locations Australia">
                                                campervan hire locations</a></h3>
                                        <ul>
                                            <li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/adelaide"
                                                title="Adelaide Motorhome Hire">Adelaide branch</a></li><li class="groupitem"><a
                                                    href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/alice-springs" title="Alice Springs Motorhome Hire">
                                                    Alice Springs branch</a></li><li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/ballina-byronbay">
                                                        Ballina - Byron Bay</a></li><li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/brisbane"
                                                            title="Brisbane Motorhome Hire">Brisbane branch</a></li><li class="groupitem"><a
                                                                href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/broome" title="Broome Motorhome Hire">
                                                                Broome branch</a></li><li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/cairns"
                                                                    title="Cairns motorhome hire">Cairns branch</a></li><li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/darwin"
                                                                        title="Darwin motorhome hire">Darwin branch</a></li><li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/hobart"
                                                                            title="Hobart motorhome hire">Hobart branch</a></li><li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/melbourne"
                                                                                title="Melbourne campervan hire">Melbourne branch</a></li><li class="groupitem"><a
                                                                                    href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/perth" title="Perth motorhome hire, Australia">
                                                                                    Perth branch</a></li><li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/sydney"
                                                                                        title="Sydney campervan hire, New South Wales">Sydney branch</a></li></ul>
                                    </div>
                                    <div class="horizontal">
                                        <h3>
                                            <a href="http://www.maui.com.au/rentals-australia/car-hire-locations" title="Car hire locations in Australia">
                                                car hire locations</a></h3>
                                        <ul>
                                            <li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/car-hire-locations/adelaide" title="Adelaide car hire">
                                                Adelaide branches</a></li><li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/car-hire-locations/alice-springs"
                                                    title="Alice Springs car hire">Alice Springs branches</a></li><li class="groupitem">
                                                        <a href="http://www.maui.com.au/rentals-australia/car-hire-locations/ayers-rock" title="Ayers Rock car hire">
                                                            Ayers Rock Branch</a></li><li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/car-hire-locations/ballina"
                                                                title="Ballina car hire">Ballina branch</a></li><li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/car-hire-locations/brisbane"
                                                                    title="Brisbane car hire">Brisbane branches</a></li><li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/car-hire-locations/cairns"
                                                                        title="Carins car rental">Cairns branches</a></li><li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/car-hire-locations/coolangatta"
                                                                            title="Coolangatta car hire">Coolangatta branch</a></li><li class="groupitem"><a
                                                                                href="http://www.maui.com.au/rentals-australia/car-hire-locations/darwin" title="Darwin car rental">Darwin
                                                                                branches</a></li><li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/car-hire-locations/devonport"
                                                                                    title="Devonport car hire">Devonport branches</a></li><li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/car-hire-locations/hobart"
                                                                                        title="Hobart car rental">Hobart branches</a></li><li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/car-hire-locations/launceston"
                                                                                            title="Launceston car hire">Launceston Branches</a></li><li class="groupitem"><a
                                                                                                href="http://www.maui.com.au/rentals-australia/car-hire-locations/melbourne" title="Melbourne car rental">
                                                                                                Melbourne branches</a></li><li class="groupitem"><a href="http://www.maui.com.au/rentals-australia/car-hire-locations/perth"
                                                                                                    title="Perth car rental">Perth branches</a></li><li class="groupitem"><a href="http://www.maui.com.au/ourlocations/car/Pages/sydneycarhire.aspx"
                                                                                                        title="Sydney car rental">Sydney branches</a></li></ul>
                                    </div>
                                </div>
                            </li>
                            <li id="menu-holiday" class="level1" xmlns:xs="http://www.w3.org/2001/XMLSchema"
                                xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                <a href="http://www.maui.com.au/motorhome-holiday-advice">holiday advice</a><div class="level2">
                                    <div class="horizontal">
                                        <h3>
                                            before you arrive</h3>
                                        <ul>
                                            <li class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/insurance-options">Accident
                                                liability cover</a></li><li class="groupitem"><a href="http://www.maui.com.au/campervan-extra-hire-items">
                                                    Campervan extra hire items</a></li><li class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/what-to-pack">
                                                        Things to bring</a></li><li class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/ferry-crossings">
                                                            Ferries</a></li><li class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/australian-accommodation">
                                                                Where to stay</a></li><li class="groupitem"><a href="http://www.maui.com.au/branch-location-crosslink">Branch
                                                                    locations</a></li><li class="groupitem"><a href="http://www.maui.com.au/online-rental-check-in" title="Campervan check-in online">
                                                                        Online check-In</a></li></ul>
                                    </div>
                                    <div class="horizontal">
                                        <h3>
                                            <a href="http://www.maui.com.au/motorhome-holiday-advice/campervan-driving-routes" title="Campervan driving routes in New Zealand">
                                                driving routes</a></h3>
                                        <ul>
                                            <li class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/campervan-driving-routes/great-southern-touring-route"
                                                title="Great Southern Touring Route">Great Southern Touring Route</a></li><li class="groupitem">
                                                    <a href="http://www.maui.com.au/motorhome-holiday-advice/campervan-driving-routes/great-tropical-drive"
                                                        title="Great Tropical Drive">Great Tropical Drive</a></li><li class="groupitem"><a
                                                            href="http://www.maui.com.au/motorhome-holiday-advice/campervan-driving-routes/natures-way" title="Nature&#39;s Way - Camper Driving Routes Australia">
                                                            Nature's Way</a></li><li class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/campervan-driving-routes/western-wonderland"
                                                                title="Western Wonderland - Camper Van Driving Routes Australia">Western Wonderland</a></li><li
                                                                    class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/campervan-driving-routes/island-adventure"
                                                                        title="Island Adventure - Motorhome Driving Routes Australia">Island Adventure</a></li><li
                                                                            class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/campervan-driving-routes/southern-cruiser"
                                                                                title="Southern Cruiser - Motorhome Road Trips Australia">Southern Cruiser</a></li><li
                                                                                    class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/campervan-driving-routes/pacific-coastal-run"
                                                                                        title="Pacific Coastal Run - Motorhome Road Trips Australia">Pacific Coastal Run</a></li><li
                                                                                            class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/campervan-driving-routes/great-barrier-reef"
                                                                                                title="The Great Barrier Reef - Campervan Road Trips Australia">The Great Barrier
                                                                                                Reef</a></li></ul>
                                    </div>
                                    <div class="horizontal">
                                        <h3>
                                            <a href="http://www.maui.com.au/motorhome-holiday-advice/regional-highlights" title="australian state highlights">
                                                state highlights</a></h3>
                                        <ul>
                                            <li class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/regional-highlights/new-south-wales"
                                                title="New South Wales Tourist Information">New South Wales</a></li><li class="groupitem">
                                                    <a href="http://www.maui.com.au/motorhome-holiday-advice/regional-highlights/victoria" title="Victoria Tourist Information">
                                                        Victoria</a></li><li class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/regional-highlights/queensland"
                                                            title="Queensland Tourist Information">Queensland</a></li><li class="groupitem"><a
                                                                href="http://www.maui.com.au/motorhome-holiday-advice/regional-highlights/south-australia" title="South Australia Tourist Information">
                                                                South Australia</a></li><li class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/regional-highlights/western-australia"
                                                                    title="Western Australia Tourist Information">Western Australia</a></li><li class="groupitem">
                                                                        <a href="http://www.maui.com.au/motorhome-holiday-advice/regional-highlights/nothern-territory" title="Northern Territory Tourist Information">
                                                                            Northern Territory</a></li><li class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/regional-highlights/tasmania"
                                                                                title="Tasmania Tourist Information">Tasmania</a></li><li class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/regional-highlights/tourism-australia">
                                                                                    Tourism Australia Widget</a></li></ul>
                                    </div>
                                    <div class="horizontal">
                                        <h3>
                                            on the road</h3>
                                        <ul>
                                            <li class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/campervan-driving-tips"
                                                title="Motorhome driving tips">Motorhome driving tips</a></li><li class="groupitem">
                                                    <a href="http://www.maui.com.au/motorhome-holiday-advice/australia-produce" title="Meals in your camper">Meals
                                                        in your camper</a></li><li class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/dump-stations-and-waste-disposal"
                                                            title="New Zealand campervan waste disposal">Camper waste disposal</a></li><li class="groupitem">
                                                                <a href="http://www.maui.com.au/motorhome-holiday-advice/camper-safety" title="Motorhome safety tips">Safety
                                                                    tips</a></li><li class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/maui-customer-care"
                                                                        title="Maui service while on the road">Service while on the road</a></li></ul>
                                    </div>
                                    <div class="horizontal">
                                        <h3>
                                            your departure</h3>
                                        <ul>
                                            <li class="groupitem"><a href="http://www.maui.com.au/motorhome-holiday-advice/free-airport-transfers"
                                                title="Transfers to the airport">Transfers to the airport</a></li><li class="groupitem">
                                                    <a href="http://www.maui.com.au/motorhome-holiday-advice/express-campervan-return" title="Express campervan returns">
                                                        Express campervan returns</a></li></ul>
                                    </div>
                                </div>
                            </li>
                            <li id="menu-why" class="level1" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:x="http://www.w3.org/2001/XMLSchema"
                                xmlns:d="http://schemas.microsoft.com/sharepoint/dsp" xmlns:asp="http://schemas.microsoft.com/ASPNET/20"
                                xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                <a href="http://www.maui.com.au/why-choose-maui-campervans">why choose maui?</a><div class="level2">
                                    <div class="horizontal">
                                        <ul>
                                            <li class="groupitem"><a href="http://www.maui.com.au/why-choose-maui-campervans/better-by-design" title="Design led - Better By Design">
                                                Design led - Better By Design</a></li><li class="groupitem"><a href="http://www.maui.com.au/why-choose-maui-campervans/quality-vehicles"
                                                    title="quality vehicles">Quality vehicles</a></li><li class="groupitem"><a href="http://www.maui.com.au/why-choose-maui-campervans/environmental-commitment"
                                                        title="Caring for the environment">Caring for the environment</a></li><li class="groupitem">
                                                            <a href="http://www.maui.com.au/why-choose-maui-campervans/conservation-volunteers" title="Conservation Volunteers Australia">
                                                                Conservation volunteers</a></li><li class="groupitem"><a href="http://www.maui.com.au/why-choose-maui-campervans/hassle-free-holidays"
                                                                    title="Nothing is too much trouble">Nothing is too much trouble</a></li><li class="groupitem">
                                                                        <a href="http://www.maui.com.au/why-choose-maui-campervans/about-thl" title="About Maui &amp; thl">About Maui
                                                                            &amp; thl</a></li></ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="homePage">
                <div id="filmstrip">
                    <div id="filmstrip-main">
                        <div id="promotion-container">
                            <div class="transparent-bg">
                            </div>
                            <div id="promotions">
                                <h2 xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                    <a href="http://www.maui.com.au/promotions/" class="promotions-title">promotions</a><a href="http://www.maui.com.au/promotions/"
                                        id="all-promotions">see all promotions</a></h2>
                                <ul class="star">
                                    <li><a href="http://www.maui.com.au/motorhome-hire-promotions/25-off-april-to-june">25% off April to June
                                        travel</a></li><li><a href="http://www.maui.com.au/motorhome-hire-promotions/mecure-tasmania-pamper-package">
                                            Mercure Tasmania Pamper Package</a></li></ul>
                                <ul class="star">
                                    <li><a href="http://www.maui.com.au/motorhome-hire-promotions/convoys" title="Great deals for campervan groups &amp; convoys">
                                        Great deals for campervan groups &amp; convoys</a></li><li><a href="http://www.maui.com.au/motorhome-hire-promotions/gift-vouchers"
                                            title="Give great maui moments - camper gift vouchers">Give great maui moments -
                                            camper gift vouchers</a></li></ul>
                            </div>
                        </div>
                        <div id="booking-container">
                            <div class="transparent-bg">
                            </div>
                            <div id="booking-header">
                            </div>
                            <div id="bookingControlContainer" class="">
                                <!--<link rel="stylesheet" type="text/css" media="all" href="http://www.maui.com.au/SiteCollectionDocuments/BookingControls/css/calendar/calendar.css" />-->
                                <link rel="stylesheet" type="text/css" media="all" href="http://www.maui.com.au/SiteCollectionDocuments/BookingControls/css/m/vertical.css" />
                                <script src="http://www.maui.com.au/SiteCollectionDocuments/BookingControls/js/CalendarBase.js"
                                    type="text/javascript"></script>
                                <script src="http://www.maui.com.au/SiteCollectionDocuments/BookingControls/js/date.js"
                                    type="text/javascript"></script>
                                <script src="http://www.maui.com.au/SiteCollectionDocuments/BookingControls/js/selection.js"
                                    type="text/javascript"></script>
                                <script src="http://www.maui.com.au/SiteCollectionDocuments/BookingControls/js/vCtrl.js"
                                    type="text/javascript"></script>
                                <script type="text/javascript">
                                    var cr = null;
                                    var vt = null;
                                    var dxr = null;
                                    var ContentbaseUrl = 'http://www.maui.com.au/SiteCollectionDocuments/BookingControls';
                                    var CallBackURL = 'http://www.maui.com.au/SiteCollectionDocuments/BookingControls';
                                    var TargetUrl = 'http://secure.maui.com.au';
                                    var MAX_PASSENGERS = 4;
                                    var pudate = 'dd/mm/yyyy', dod = 'dd/mm/yyyy';
                                    var noa = 0, noc = 0;
                                    var brandStr = null;
                                    var $ = jQuery.noConflict();
                                    
                                    //added for async requests
                                    var ctrlParams = { "state": { pb: "adl", db : "syd", "brand": "m", "cc": "au", "type": "campervan", "cr": "by", "model": "auavm.2BTSM", "agent": "" }, branchTypes: [{name : "AIRPORT BRANCHES", id : "T"}], "branches": [{ "name": "Auckland airport", "code": "AKL", "brands": "mbyuae", "vt": "campervan", "cc": "nz", type : "T" }, { "name": "Christchurch airport", "code": "CHC", "brands": "mbyuae", "vt": "campervan", "cc": "nz", type : "T" }, { "name": "Queenstown airport", "code": "ZQN", "brands": "mby", "vt": "campervan", "cc": "nz", type : "T" }, { "name": "Auckland international airport", "code": "AIN", "brands": "mby", "vt": "car", "cc": "nz", type : "T" }, { "name": "Auckland domestic airport", "code": "ADO", "brands": "mby", "vt": "car", "cc": "nz", type : "T" }, { "name": "Auckland city", "code": "ACI", "brands": "mby", "vt": "car", "cc": "nz" }, { "name": "Blenheim airport", "code": "BDO", "brands": "mby", "vt": "car", "cc": "nz", type : "T" }, { "name": "Christchurch airport", "code": "CDO", "brands": "mby", "vt": "car", "cc": "nz", type : "T" }, { "name": "Dunedin airport", "code": "DDO", "brands": "mby", "vt": "car", "cc": "nz", type : "T" }, { "name": "Greymouth", "code": "GRA", "brands": "mby", "vt": "car", "cc": "nz" }, { "name": "Hamilton airport", "code": "HDO", "brands": "mby", "vt": "car", "cc": "nz", type : "T" }, { "name": "Napier airport", "code": "NDA", "brands": "mby", "vt": "car", "cc": "nz", type : "T" }, { "name": "Nelson airport", "code": "NDO", "brands": "mby", "vt": "car", "cc": "nz", type : "T" }, { "name": "Picton", "code": "PFE", "brands": "mby", "vt": "car", "cc": "nz" }, { "name": "Queenstown airport", "code": "QDO", "brands": "mby", "vt": "car", "cc": "nz", type : "T" }, { "name": "Rotorua airport", "code": "RDO", "brands": "mby", "vt": "car", "cc": "nz", type : "T" }, { "name": "Rotorua city", "code": "RCI", "brands": "mby", "vt": "car", "cc": "nz" }, { "name": "Wellington airport", "code": "WDO", "brands": "mby", "vt": "car", "cc": "nz", type : "T" }, { "name": "Wellington ferry", "code": "WFE", "brands": "mby", "vt": "car", "cc": "nz" }, { "name": "Adelaide", "code": "ADL", "brands": "mby", "vt": "campervan", "cc": "au" }, { "name": "Alice Springs", "code": "ASP", "brands": "mby", "vt": "campervan", "cc": "au" }, { "name": "Ballina - Byron Bay", "code": "BYR", "brands": "mby", "vt": "campervan", "cc": "au" }, { "name": "Brisbane", "code": "BNE", "brands": "mby", "vt": "campervan", "cc": "au" }, { "name": "Broome", "code": "BME", "brands": "mby", "vt": "campervan", "cc": "au" }, { "name": "Cairns", "code": "CNS", "brands": "mby", "vt": "campervan", "cc": "au" }, { "name": "Darwin", "code": "DRW", "brands": "mby", "vt": "campervan", "cc": "au" }, { "name": "Hobart", "code": "HBT", "brands": "mb", "vt": "campervan", "cc": "au" }, { "name": "Melbourne", "code": "MEL", "brands": "mby", "vt": "campervan", "cc": "au" }, { "name": "Perth", "code": "PER", "brands": "mby", "vt": "campervan", "cc": "au" }, { "name": "Sydney", "code": "SYD", "brands": "mby", "vt": "campervan", "cc": "au" }, { "name": "Adelaide Airport", "code": "ALT", "brands": "mby", "vt": "car", "cc": "au", type : "T" }, { "name": "Adelaide City", "code": "ATC", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Alice Springs Airport", "code": "AIT", "brands": "mby", "vt": "car", "cc": "au", type : "T" }, { "name": "Alice Springs City", "code": "APT", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Airlie Beach Airport", "code": "AIA", "brands": "mby", "vt": "car", "cc": "au", type : "T" }, { "name": "Airlie Beach City", "code": "AIC", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Ayers Rock Airport", "code": "AYQ", "brands": "mby", "vt": "car", "cc": "au", type : "T" }, { "name": "Ballina Airport", "code": "BAT", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Brisbane City", "code": "BCT", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Brisbane Airport (Domestic)", "code": "BDT", "brands": "mby", "vt": "car", "cc": "au", type : "T" }, { "name": "Brisbane Airport (International)", "code": "BIT", "brands": "mby", "vt": "car", "cc": "au", type : "T" }, { "name": "Cairns Airport", "code": "CIA", "brands": "mby", "vt": "car", "cc": "au", type : "T" }, { "name": "Cairns City", "code": "CTC", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Coffs Harbour Airport", "code": "COA", "brands": "mby", "vt": "car", "cc": "au", type : "T" }, { "name": "Coolangatta Airport", "code": "OOL", "brands": "mby", "vt": "car", "cc": "au", type : "T" }, { "name": "Darling Harbour", "code": "DAH", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Darwin Airport", "code": "DIT", "brands": "mby", "vt": "car", "cc": "au", type : "T" }, { "name": "Devonport Airport", "code": "DPI", "brands": "mby", "vt": "car", "cc": "au", type : "T" }, { "name": "Devonport City", "code": "DPF", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Esperance Airport", "code": "ESA", "brands": "mby", "vt": "car", "cc": "au", type : "T" }, { "name": "Esperance City", "code": "ESC", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Exmouth Airport", "code": "EXA", "brands": "mby", "vt": "car", "cc": "au", type : "T" }, { "name": "Geraldton Airport", "code": "GEA", "brands": "mby", "vt": "car", "cc": "au", type : "T" }, { "name": "Geraldton City", "code": "GEC", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Gladstone Airport", "code": "GLA", "brands": "mby", "vt": "car", "cc": "au", type : "T" }, { "name": "Hervey Bay Airport", "code": "HEA", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Hobart Airport", "code": "HIT", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Hobart City", "code": "HCT", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Kalgoorlie Airport", "code": "KAA", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Kalgoorlie City", "code": "KAC", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Karatha Airport", "code": "KRA", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Launceston Airport", "code": "LIT", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Launceston City", "code": "LCT", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Mackay Airport", "code": "MAA", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Maroochydore Airport", "code": "MRA", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Melbourne Airport (Domestic)", "code": "MDT", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Melbourne Airport (International)", "code": "MIT", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Melbourne City", "code": "MTC", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Melbourne St. Kilda", "code": "MEK", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Melbourne Tullamarine Off Airport", "code": "MET", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Melbourne Southern Cross Station", "code": "MEU", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Newcastle Airport", "code": "NEA", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Perth City", "code": "PCT", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Perth Airport (Domestic)", "code": "PDT", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Perth Airport (International)", "code": "PIT", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Port Hedland Airport", "code": "PHA", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Roma Airport", "code": "RMA", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Sydney Airport (Domestic)", "code": "SDT", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Sydney Airport (International)", "code": "SIT", "brands": "mby", "vt": "car", "cc": "au" }, { "name": "Sydney City", "code": "STC", "brands": "mby", "vt": "car", "cc": "au"}], "vehicles": [{ "name": "Economy Car", "code": "nzacm.ECMRM", "brand": "m", "cc": "nz", "vt": "car" }, { "name": "Compact Car", "code": "nzacm.CDAR", "brand": "m", "cc": "nz", "vt": "car" }, { "name": "Intermediate Car", "code": "nzacm.ICARM", "brand": "m", "cc": "nz", "vt": "car" }, { "name": "Standard Car", "code": "nzacm.SDAR", "brand": "m", "cc": "nz", "vt": "car" }, { "name": "Full Size Car", "code": "nzacm.FCARM", "brand": "m", "cc": "nz", "vt": "car" }, { "name": "4WD", "code": "nzacm.PFARM", "brand": "m", "cc": "nz", "vt": "car" }, { "name": "Standard AWD", "code": "nzacm.SFAR", "brand": "m", "cc": "nz", "vt": "car" }, { "name": "Standard 4WD Ute", "code": "nzacm.SFBR", "brand": "m", "cc": "nz", "vt": "car" }, { "name": "Large 4WD", "code": "nzacm.FFAR", "brand": "m", "cc": "nz", "vt": "car" }, { "name": "People Mover", "code": "nzacm.LVARM", "brand": "m", "cc": "nz", "vt": "car" }, { "name": "Intermediate Minivan", "code": "nzacm.IVAR", "brand": "m", "cc": "nz", "vt": "car" }, { "name": "Ultima", "code": "nzavm.2BTSM", "brand": "m", "cc": "nz", "vt": "campervan" }, { "name": "Platinum Beach", "code": "nzavm.4BMP", "brand": "m", "cc": "nz", "vt": "campervan" }, { "name": "Platinum River", "code": "nzavm.6BMPC", "brand": "m", "cc": "nz", "vt": "campervan" }, { "name": "Economy Car", "code": "nzacb.ECMR", "brand": "b", "cc": "nz", "vt": "car" }, { "name": "Compact Car", "code": "nzacb.CCMR", "brand": "b", "cc": "nz", "vt": "car" }, { "name": "Standard Car", "code": "nzacb.SDAR", "brand": "b", "cc": "nz", "vt": "car" }, { "name": "Intermediate Car", "code": "nzacb.IWARB", "brand": "b", "cc": "nz", "vt": "car" }, { "name": "Full Size", "code": "nzacb.FCAR", "brand": "b", "cc": "nz", "vt": "car" }, { "name": "4WD", "code": "nzacb.PFAR", "brand": "b", "cc": "nz", "vt": "car" }, { "name": "Standard AWD", "code": "nzacb.SFAR", "brand": "b", "cc": "nz", "vt": "car" }, { "name": "Standard 4WD Ute", "code": "nzacb.SFBR", "brand": "b", "cc": "nz", "vt": "car" }, { "name": "Large 4WD", "code": "nzacb.FFAR", "brand": "b", "cc": "nz", "vt": "car" }, { "name": "People Mover", "code": "nzacb.LVAR", "brand": "b", "cc": "nz", "vt": "car" }, { "name": "Intermediate Minivan", "code": "nzacb.IVAR", "brand": "b", "cc": "nz", "vt": "car" }, { "name": "HiTop", "code": "nzavb.2BB", "brand": "b", "cc": "nz", "vt": "campervan" }, { "name": "Voyager", "code": "nzavb.4BBXS", "brand": "b", "cc": "nz", "vt": "campervan" }, { "name": "Trailblazer", "code": "nzavb.2BTSBT", "brand": "b", "cc": "nz", "vt": "campervan" }, { "name": "Venturer", "code": "nzavb.2BTSBV", "brand": "b", "cc": "nz", "vt": "campervan" }, { "name": "Elite", "code": "nzavb.2BTSB", "brand": "b", "cc": "nz", "vt": "campervan" }, { "name": "Explorer", "code": "nzavb.4BB", "brand": "b", "cc": "nz", "vt": "campervan" }, { "name": "Navigator", "code": "nzavb.4BBR", "brand": "b", "cc": "nz", "vt": "campervan" }, { "name": "Frontier", "code": "nzavb.6BB", "brand": "b", "cc": "nz", "vt": "campervan" }, { "name": "Renegade", "code": "nzavb.6BBR", "brand": "b", "cc": "nz", "vt": "campervan" }, { "name": "Compact Car", "code": "nzacp.CCAR", "brand": "p", "cc": "nz", "vt": "car" }, { "name": "Economy Car", "code": "nzacp.ECAR", "brand": "p", "cc": "nz", "vt": "car" }, { "name": "Intermediate Car", "code": "nzacp.ICARX", "brand": "p", "cc": "nz", "vt": "car" }, { "name": "Executive Sedan", "code": "nzacp.FCAR", "brand": "p", "cc": "nz", "vt": "car" }, { "name": "Caddy Car", "code": "nzacp.CCMR", "brand": "p", "cc": "nz", "vt": "car" }, { "name": "Executive Wagon", "code": "nzacp.FWAR", "brand": "p", "cc": "nz", "vt": "car" }, { "name": "4WD Car", "code": "nzacp.PFAB", "brand": "p", "cc": "nz", "vt": "car" }, { "name": "Breezer", "code": "nzavp.BP2BB", "brand": "p", "cc": "nz", "vt": "campervan" }, { "name": "Lowrider", "code": "nzavp.BP2BLR", "brand": "p", "cc": "nz", "vt": "campervan" }, { "name": "Nomad", "code": "nzavp.BP2BHT", "brand": "p", "cc": "nz", "vt": "campervan" }, { "name": "Wanderer", "code": "nzavp.BP2BTS", "brand": "p", "cc": "nz", "vt": "campervan" }, { "name": "Quattro", "code": "nzavp.BP4B", "brand": "p", "cc": "nz", "vt": "campervan" }, { "name": "Six Pack", "code": "nzavp.BP6B", "brand": "p", "cc": "nz", "vt": "campervan" }, { "name": "Gizzy (small)", "code": "nzacx.SCARX", "brand": "x", "cc": "nz", "vt": "car" }, { "name": "Palmy (economy)", "code": "nzacx.ECAR", "brand": "x", "cc": "nz", "vt": "car" }, { "name": "Welly (intermediate)", "code": "nzacx.ICARX", "brand": "x", "cc": "nz", "vt": "car" }, { "name": "Hoki (station wagon)", "code": "nzacx.IWAR", "brand": "x", "cc": "nz", "vt": "car" }, { "name": "The Franz (hitop)", "code": "nzavx.2BSX", "brand": "x", "cc": "nz", "vt": "campervan" }, { "name": "Queenie (hitop)", "code": "nzavx.2BHX", "brand": "x", "cc": "nz", "vt": "campervan" }, { "name": "RotoVegas (cruiser)", "code": "nzavx.2BGX", "brand": "x", "cc": "nz", "vt": "campervan" }, { "name": "Economy Car", "code": "nzacy.ECAR", "brand": "y", "cc": "nz", "vt": "car" }, { "name": "Intermediate Car", "code": "nzacy.ICARX", "brand": "y", "cc": "nz", "vt": "car" }, { "name": "Full Size", "code": "nzacy.FCAR", "brand": "y", "cc": "nz", "vt": "car" }, { "name": "Compact Car", "code": "nzacy.CCMR", "brand": "y", "cc": "nz", "vt": "car" }, { "name": "Executive Wagon", "code": "nzacy.FWAR", "brand": "y", "cc": "nz", "vt": "car" }, { "name": "4WD Car", "code": "nzacy.PFAB", "brand": "y", "cc": "nz", "vt": "car" }, { "name": "Deuce", "code": "nzavy.2Y", "brand": "y", "cc": "nz", "vt": "campervan" }, { "name": "Highball", "code": "nzavy.2YX", "brand": "y", "cc": "nz", "vt": "campervan" }, { "name": "Jackpot", "code": "nzavy.2YXQ", "brand": "y", "cc": "nz", "vt": "campervan" }, { "name": "Lowball", "code": "nzavy.2YXS", "brand": "y", "cc": "nz", "vt": "campervan" }, { "name": "Double Up", "code": "nzavy.4Y", "brand": "y", "cc": "nz", "vt": "campervan" }, { "name": "2+2 Flip Top Campervan", "code": "nzavq.4KXFT", "brand": "q", "cc": "nz", "vt": "campervan" }, { "name": "2 Berth Campervan", "code": "nzavq.2K", "brand": "q", "cc": "nz", "vt": "campervan" }, { "name": "3 Berth Origin", "code": "nzavq.3K", "brand": "q", "cc": "nz", "vt": "campervan" }, { "name": "4 Berth Motorhome", "code": "nzavq.4K", "brand": "q", "cc": "nz", "vt": "campervan" }, { "name": "6 Berth Motorhome", "code": "nzavq.6K", "brand": "q", "cc": "nz", "vt": "campervan" }, { "name": "2 Premier Motorhome", "code": "nzavu.2U", "brand": "u", "cc": "nz", "vt": "campervan" }, { "name": "2+1 Premier Motorhome", "code": "nzavu.3U", "brand": "u", "cc": "nz", "vt": "campervan" }, { "name": "4 Premier Motorhome", "code": "nzavu.4U", "brand": "u", "cc": "nz", "vt": "campervan" }, { "name": "6 Premier Motorhome", "code": "nzavu.6U", "brand": "u", "cc": "nz", "vt": "campervan" }, { "name": "2 Berth HiTop", "code": "nzava.2AX", "brand": "a", "cc": "nz", "vt": "campervan" }, { "name": "2 Berth", "code": "nzava.2A", "brand": "a", "cc": "nz", "vt": "campervan" }, { "name": "2 Berth Automatic", "code": "nzava.2AA", "brand": "a", "cc": "nz", "vt": "campervan" }, { "name": "2+1 Berth", "code": "nzava.3A", "brand": "a", "cc": "nz", "vt": "campervan" }, { "name": "2+1 Berth Automatic", "code": "nzava.3AA", "brand": "a", "cc": "nz", "vt": "campervan" }, { "name": "4 Berth", "code": "nzava.4A", "brand": "a", "cc": "nz", "vt": "campervan" }, { "name": "4 Berth Automatic", "code": "nzava.4AA", "brand": "a", "cc": "nz", "vt": "campervan" }, { "name": "6 Berth", "code": "nzava.6A", "brand": "a", "cc": "nz", "vt": "campervan" }, { "name": "The Ace Budget Campervan", "code": "nzave.2EX", "brand": "e", "cc": "nz", "vt": "campervan" }, { "name": "The Duke Campervan", "code": "nzave.2E", "brand": "e", "cc": "nz", "vt": "campervan" }, { "name": "Economy Car", "code": "auacm.ECMR", "brand": "m", "cc": "au", "vt": "car" }, { "name": "Compact Car", "code": "auacm.CCAR", "brand": "m", "cc": "au", "vt": "car" }, { "name": "Medium Wagon", "code": "auacm.IWAR", "brand": "m", "cc": "au", "vt": "car" }, { "name": "Intermediate Car", "code": "auacm.ICAR", "brand": "m", "cc": "au", "vt": "car" }, { "name": "Mid Size car", "code": "auacm.SCAR", "brand": "m", "cc": "au", "vt": "car" }, { "name": "Full-Size Car", "code": "auacm.FCAR", "brand": "m", "cc": "au", "vt": "car" }, { "name": "Urban Wagon", "code": "auacm.UWAR", "brand": "m", "cc": "au", "vt": "car" }, { "name": "Compact Car - 01 April 2013", "code": "auacm.CDAR", "brand": "m", "cc": "au", "vt": "car" }, { "name": "Intermediate Car - 01 April 2013", "code": "auacm.IDAR", "brand": "m", "cc": "au", "vt": "car" }, { "name": "Standard Car - 01 April 2013", "code": "auacm.SDAR", "brand": "m", "cc": "au", "vt": "car" }, { "name": "Full Size Car - from 01 April", "code": "auacm.FDAR", "brand": "m", "cc": "au", "vt": "car" }, { "name": "Intermediate AWD - 01 April 2013", "code": "auacm.IFDR", "brand": "m", "cc": "au", "vt": "car" }, { "name": "Large 4WD - 01 April 2013", "code": "auacm.FFAR", "brand": "m", "cc": "au", "vt": "car" }, { "name": "Premium 8 Seater - 01 April 2013", "code": "auacm.PVAR", "brand": "m", "cc": "au", "vt": "car" }, { "name": "Intermediate Van - 01 April 2013", "code": "auacm.IVAR", "brand": "m", "cc": "au", "vt": "car" }, { "name": "Ultima", "code": "auavm.2BTSM", "brand": "m", "cc": "au", "vt": "campervan" }, { "name": "Platinum Beach", "code": "auavm.4BMP", "brand": "m", "cc": "au", "vt": "campervan" }, { "name": "Platinum River", "code": "auavm.6BMPC", "brand": "m", "cc": "au", "vt": "campervan" }, { "name": "Platinum Forest", "code": "auavm.6BMP", "brand": "m", "cc": "au", "vt": "campervan" }, { "name": "Economy Car", "code": "auacb.ECMR", "brand": "b", "cc": "au", "vt": "car" }, { "name": "Compact Car", "code": "auacb.CCAR", "brand": "b", "cc": "au", "vt": "car" }, { "name": "Intermediate Car", "code": "auacb.ICAR", "brand": "b", "cc": "au", "vt": "car" }, { "name": "Medium Wagon", "code": "auacb.IWAR", "brand": "b", "cc": "au", "vt": "car" }, { "name": "Mid Size Car", "code": "auacb.SCAR", "brand": "b", "cc": "au", "vt": "car" }, { "name": "Full Size Car", "code": "auacb.FCAR", "brand": "b", "cc": "au", "vt": "car" }, { "name": "4WD", "code": "auacb.PFMR", "brand": "b", "cc": "au", "vt": "car" }, { "name": "Urban Wagon", "code": "auacb.UWAR", "brand": "b", "cc": "au", "vt": "car" }, { "name": "Compact Car - 01 April 2013", "code": "auacb.CDAR", "brand": "b", "cc": "au", "vt": "car" }, { "name": "Intermediate Car - 01 April 2013", "code": "auacb.IDAR", "brand": "b", "cc": "au", "vt": "car" }, { "name": "Standard Car - 01 April 2013", "code": "auacb.SDAR", "brand": "b", "cc": "au", "vt": "car" }, { "name": "Full Size Car - from 01 April", "code": "auacb.FDAR", "brand": "b", "cc": "au", "vt": "car" }, { "name": "Intermediate AWD - 01 April 2013", "code": "auacb.IFDR", "brand": "b", "cc": "au", "vt": "car" }, { "name": "Large 4WD - 01 April 2013", "code": "auacb.FFAR", "brand": "b", "cc": "au", "vt": "car" }, { "name": "Premium 8 Seater - 01 April 2013", "code": "auacb.PVAR", "brand": "b", "cc": "au", "vt": "car" }, { "name": "Intermediate Van - 01 April 2013", "code": "auacb.IVAR", "brand": "b", "cc": "au", "vt": "car" }, { "name": "HiTop", "code": "auavb.2BB", "brand": "b", "cc": "au", "vt": "campervan" }, { "name": "Voyager", "code": "auavb.4BBXS", "brand": "b", "cc": "au", "vt": "campervan" }, { "name": "Elite", "code": "auavb.2BTSB", "brand": "b", "cc": "au", "vt": "campervan" }, { "name": "Venturer", "code": "auavb.2BTSBV", "brand": "b", "cc": "au", "vt": "campervan" }, { "name": "Explorer", "code": "auavb.4BB", "brand": "b", "cc": "au", "vt": "campervan" }, { "name": "Maverick", "code": "auavb.4BBJ", "brand": "b", "cc": "au", "vt": "campervan" }, { "name": "Escape", "code": "auavb.5BB", "brand": "b", "cc": "au", "vt": "campervan" }, { "name": "Frontier", "code": "auavb.6BB", "brand": "b", "cc": "au", "vt": "campervan" }, { "name": "Renegade", "code": "auavb.6BBR", "brand": "b", "cc": "au", "vt": "campervan" }, { "name": "Challenger 4WD", "code": "auavb.2B4WDBC", "brand": "b", "cc": "au", "vt": "campervan" }, { "name": "Bushcamper 4WD", "code": "auavb.2B4WDB", "brand": "b", "cc": "au", "vt": "campervan" }, { "name": "Safari 4WD", "code": "auavb.PFMRS", "brand": "b", "cc": "au", "vt": "campervan" }, { "name": "Compact Car", "code": "auacp.CCAR", "brand": "p", "cc": "au", "vt": "car" }, { "name": "Economy Car", "code": "auacp.ECMR", "brand": "p", "cc": "au", "vt": "car" }, { "name": "Intermediate Car", "code": "auacp.ICAR", "brand": "p", "cc": "au", "vt": "car" }, { "name": "Breezer", "code": "auavp.BP2BB", "brand": "p", "cc": "au", "vt": "campervan" }, { "name": "Nomad", "code": "auavp.BP2BHT", "brand": "p", "cc": "au", "vt": "campervan" }, { "name": "Warrior", "code": "auavp.BP4BS", "brand": "p", "cc": "au", "vt": "campervan" }, { "name": "Wanderer", "code": "auavp.BP2BTS", "brand": "p", "cc": "au", "vt": "campervan" }, { "name": "Quattro", "code": "auavp.BP4B", "brand": "p", "cc": "au", "vt": "campervan" }, { "name": "Off-Roader 4WD", "code": "auavp.BP4WD", "brand": "p", "cc": "au", "vt": "campervan" }, { "name": "Tazzy", "code": "auavx.2BEXT", "brand": "x", "cc": "au", "vt": "campervan" }, { "name": "Brisvegas", "code": "auavx.2BEXB", "brand": "x", "cc": "au", "vt": "campervan" }, { "name": "Compact Car", "code": "auacy.CCAR", "brand": "y", "cc": "au", "vt": "car" }, { "name": "Economy Car", "code": "auacy.ECMR", "brand": "y", "cc": "au", "vt": "car" }, { "name": "Intermediate Car", "code": "auacy.ICAR", "brand": "y", "cc": "au", "vt": "car" }, { "name": "Intermediate 4WD", "code": "auacy.IFDR", "brand": "y", "cc": "au", "vt": "car" }, { "name": "Large 4WD", "code": "auacy.FFAR", "brand": "y", "cc": "au", "vt": "car" }, { "name": "Compact Car - 01 April 2013", "code": "auacy.CDAR", "brand": "y", "cc": "au", "vt": "car" }, { "name": "Intermediate Car - 01 April 2013", "code": "auacy.IDAR", "brand": "y", "cc": "au", "vt": "car" }, { "name": "Deuce", "code": "auavy.2Y", "brand": "y", "cc": "au", "vt": "campervan" }, { "name": "Highball", "code": "auavy.2YX", "brand": "y", "cc": "au", "vt": "campervan" }, { "name": "Jackpot", "code": "auavy.2YXQ", "brand": "y", "cc": "au", "vt": "campervan" }, { "name": "Lowball", "code": "auavy.2YXS", "brand": "y", "cc": "au", "vt": "campervan" }, { "name": "Double Down", "code": "auavy.4YX", "brand": "y", "cc": "au", "vt": "campervan" }, { "name": "Double Up", "code": "auavy.4Y", "brand": "y", "cc": "au", "vt": "campervan" }, { "name": "2 Berth Shower & Toilet Campervan", "code": "auavq.2K", "brand": "q", "cc": "au", "vt": "campervan" }, { "name": "4 Berth Shower & Toilet Motorhome", "code": "auavq.4K", "brand": "q", "cc": "au", "vt": "campervan" }, { "name": "6 Berth Shower & Toilet Motorhome", "code": "auavq.6K", "brand": "q", "cc": "au", "vt": "campervan"}] };

                                    var setControlState = function (state) {
                                        var pickupLocation = state.pb.toLowerCase(),
                                        currentBranch = (
                                            $.grep(ctrlParams.branches,
                                                    function (item) { return item.code.toLowerCase() == pickupLocation; }
                                                  )[0] || { name: '', code: '' });

                                        $('#dropPickupLocation').val(currentBranch.name);
                                        $('#dropPickupLocationValue').val(currentBranch.code);
                                        $('#ctrlSearchBox_dropVehicleType').val(ctrlParams.state.model);
                                    }

                                    $(document).ready(function () {
                                        dxr = $('#booking-container').position().left;
                                        vt = 'av';
                                        cot = 'AU';
                                        cr = '';
                                        brandStr = 'm'
                                        if (window.vehicleInfo != undefined) {
                                            vt = (vehicleInfo.type == '1') ? 'av' : 'ca';
                                            $('#vehicleCode').val(vehicleInfo.code);
                                        }
                                        initControl();
                                        //bindResize();
                                        //swapTabs((vt == 'av' ? 'rv' : 'c'));

                                        setControlState(ctrlParams.state);

                                        var branches = [];
                                        for (var i = 0; i < ctrlParams.branches.length; i++)
                                            if (ctrlParams.branches[i].cc == "au")
                                                branches.push(ctrlParams.branches[i]);

                                        new SelectBox('dropPickupLocation', 'dropPickupLocationValue',
                                                    ctrlParams.branchTypes, branches, //Data
                                                    'name', 'id', //Data members: groupTextMemberName, groupIdMemberName
                                                    'name', 'code', 'type', //Data members: itemTextMemberName, itemValueMemberName, itemGroupIdMemberName
                                                    'selectBox', 'selectBoxItem', 'selectBoxItemSelected', 'selectBoxMatchedText', 'selectBoxGroup', //CSS class names to use
                                                    '<div class="selectBoxMessage">Sorry, no branches found at the location you specified.</div>', //No-result message as requested by Erik
                                                     0, //The minimum number of characters to trigger the drop-down box
                                                     !ctrlParams.state.pb); //Boolean value indicating whether the text box should be cleared on the first focus
                                        //(init this with "false" when setting the text box to a default value)
                                    });                                    
                                </script>
                                <div id="ctrlSearchBox_up1">
                                    <div id="booking-front" class="vertical">
                                        <ul id="booking-tab">
                                            <li id="booking-motorhome" class="tabs-selected"><a id="campersTab" href="">
                                                get a quote for a motorhome</a></li>
                                            <li id="booking-car" class=""><a id="carsTab" href="">get a
                                                quote for a car</a></li>
                                        </ul>
                                        <div class="booking-form">
                                            <div class="table">
                                                <ul>
                                                    <li class="Title">
                                                        <p>
                                                            <span class="SearchBox_Title" id="ctrlSearchBox_lblTitle">Campervan hire</span>
                                                        </p>
                                                    </li>
                                                    <li class="Title">
                                                        <p>
                                                            <span class="SearchBox_Subtitle" id="ctrlSearchBox_lblSubtitle">Search rates &amp; book</span>
                                                            <hr class="SearchBox_Line" id="ctrlSearchBox_hrLine1" />
                                                        </p>
                                                    </li>
                                                    <li class="CountryOfTravel">
                                                        <p>
                                                            <label>
                                                                Country of travel:</label>
                                                            <select class="SearchBox_FullListBox" id="ddCountryOfTravel" onchange="javascript:enablePanel(this);"
                                                                name="ddCountryOfTravel">
                                                                <option value="-1" selected="selected">Please select</option>
                                                                <option value="AU">Australia</option>
                                                                <option value="NZ">New Zealand</option>
                                                            </select>
                                                            <input type="hidden" id="countryOfTravel" value="AU" />
                                                        </p>
                                                    </li>
                                                    <li class="VehicleModel">
                                                        <p class="">
                                                            <label>
                                                                Vehicle Model</label>
                                                            <select name='ctrlSearchBox_dropVehicleType' id='ctrlSearchBox_dropVehicleType' class='SearchBox_FullListBox'>
                                                                <option value=''>Search all</option>
                                                                <option value='auavm.2BTSM'>Ultima</option>
                                                                <option value='auavm.4BMP'>Platinum Beach</option>
                                                                <option value='auavm.6BMPC'>Platinum River</option>
                                                                <option value='auavm.6BMP'>Platinum Forest</option>
                                                            </select>
                                                            <input type="hidden" id="vehicleType" value="av" />
                                                            <input type="hidden" id="vehicleCode" value="" />
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p>
                                                            <label>
                                                                Travellers</label>
                                                            <select name='ctrlSearchBox_dropAdults' id='ctrlSearchBox_dropAdults' class='SearchBox_AdultsListBox'>
                                                                <option selected='selected' value='0'>Adults</option>
                                                                <option value='1'>1 Adult</option>
                                                                <option value='2'>2 Adults</option>
                                                                <option value='3'>3 Adults</option>
                                                                <option value='4'>4 Adults</option>
                                                                <option value='5'>5 Adults</option>
                                                                <option value='6'>6 Adults</option>
                                                            </select><select name='ctrlSearchBox_dropChildren' id='ctrlSearchBox_dropChildren'
                                                                class='SearchBox_AdultsListBox'><option selected='selected' value='0'>Children</option>
                                                                <option value='1'>1 Child</option>
                                                                <option value='2'>2 Children</option>
                                                                <option value='3'>3 Children</option>
                                                                <option value='4'>4 Children</option>
                                                                <option value='5'>5 Children</option>
                                                            </select>
                                                            <input type="hidden" id="maxTravellers" value="6" />
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p>
                                                            <label>
                                                                Pick up</label>
                                                            <input type="text" name="dropPickupLocation" id="dropPickupLocation" value="Please select" />
                                                            <%--<select name="ctrlSearchBox_dropPickUpLocation" id="ctrlSearchBox_dropPickUpLocation"
                                                                class="SearchBox_FullListBox LocationDD">
                                                                <option value="ADL">Adelaide</option>
                                                                <option value="ASP">Alice Springs</option>
                                                                <option value="BNE">Brisbane</option>
                                                                <option value="BME">Broome</option>
                                                                <option value="CNS">Cairns</option>
                                                                <option value="DRW">Darwin</option>
                                                                <option value="HBT">Hobart</option>
                                                                <option value="MEL">Melbourne</option>
                                                                <option value="PER">Perth</option>
                                                                <option value="SYD">Sydney</option>
                                                            </select>--%>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p>
                                                            <label class="empty">
                                                                &nbsp;</label>
                                                            <input type="text" onfocus="this.select();" class="SearchBox_CalTextBox" id="ctrlSearchBox_calPickUp_DateText"
                                                                value="dd/mm/yyyy" name="ctrlSearchBox$calPickUp$DateText" />
                                                            <select class="SearchBox_TimeListBox" id="ctrlSearchBox_dropPickUpTime" name="ctrlSearchBox_dropPickUpTime">
                                                                <option value="08:00">08:00am</option>
                                                                <option value="08:30">08:30am</option>
                                                                <option value="09:00">09:00am</option>
                                                                <option value="09:30">09:30am</option>
                                                                <option value="10:00" selected="selected">10:00am</option>
                                                                <option value="10:30">10:30am</option>
                                                                <option value="11:00">11:00am</option>
                                                                <option value="11:30">11:30am</option>
                                                                <option value="12:00">12:00pm</option>
                                                                <option value="12:30">12:30pm</option>
                                                                <option value="13:00">13:00pm</option>
                                                                <option value="13:30">13:30pm</option>
                                                                <option value="14:00">14:00pm</option>
                                                                <option value="14:30">14:30pm</option>
                                                                <option value="15:00">15:00pm</option>
                                                                <option value="15:30">15:30pm</option>
                                                                <option value="16:00">16:00pm</option>
                                                                <option value="16:30">16:30pm</option>
                                                                <option value="17:00">17:00pm</option>
                                                            </select>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p>
                                                            <label>
                                                                Drop off</label>
                                                            <select name="ctrlSearchBox_dropDropOffLocation" id="ctrlSearchBox_dropDropOffLocation"
                                                                class="SearchBox_FullListBox LocationDD">
                                                                <option value="ADL">Adelaide</option>
                                                                <option value="ASP">Alice Springs</option>
                                                                <option value="BNE">Brisbane</option>
                                                                <option value="BME">Broome</option>
                                                                <option value="CNS">Cairns</option>
                                                                <option value="DRW">Darwin</option>
                                                                <option value="HBT">Hobart</option>
                                                                <option value="MEL">Melbourne</option>
                                                                <option value="PER">Perth</option>
                                                                <option value="SYD">Sydney</option>
                                                            </select>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p>
                                                            <label class="empty">
                                                                &nbsp;</label>
                                                            <input type="text" onfocus="this.select();" class="SearchBox_CalTextBox" id="ctrlSearchBox_calDropOff_DateText"
                                                                value="dd/mm/yyyy" name="ctrlSearchBox$calDropOff$DateText" />
                                                            <select class="SearchBox_TimeListBox" id="ctrlSearchBox_dropDropOffTime" name="ctrlSearchBox_dropDropOffTime">
                                                                <option value="08:00">08:00am</option>
                                                                <option value="08:30">08:30am</option>
                                                                <option value="09:00">09:00am</option>
                                                                <option value="09:30">09:30am</option>
                                                                <option value="10:00" selected="selected">10:00am</option>
                                                                <option value="10:30">10:30am</option>
                                                                <option value="11:00">11:00am</option>
                                                                <option value="11:30">11:30am</option>
                                                                <option value="12:00">12:00pm</option>
                                                                <option value="12:30">12:30pm</option>
                                                                <option value="13:00">13:00pm</option>
                                                                <option value="13:30">13:30pm</option>
                                                                <option value="14:00">14:00pm</option>
                                                                <option value="14:30">14:30pm</option>
                                                                <option value="15:00">15:00pm</option>
                                                                <option value="15:30">15:30pm</option>
                                                                <option value="16:00">16:00pm</option>
                                                                <option value="16:30">16:30pm</option>
                                                                <option value="17:00">17:00pm</option>
                                                            </select>
                                                        </p>
                                                    </li>
                                                    <li class="CountryOfResidence">
                                                        <p id="ctrlSearchBox_tblrCountryOfResidence">
                                                            <label class="SearchBoxLabel" id="ctrlSearchBox_lblCountryOfResidence">
                                                                Country of residence</label>
                                                            <select class="SearchBox_FullListBox" id="ctrlSearchBox_dropCountryOfResidence" name="ctrlSearchBox$dropCountryOfResidence">
                                                                <option value="0" selected="selected">Please select</option>
                                                                <option value="AU">Australia</option>
                                                                <option value="CA">Canada</option>
                                                                <option value="DK">Denmark</option>
                                                                <option value="FR">France</option>
                                                                <option value="DE">Germany</option>
                                                                <option value="NL">Netherlands</option>
                                                                <option value="NZ">New Zealand</option>
                                                                <option value="SE">Sweden</option>
                                                                <option value="CH">Switzerland</option>
                                                                <option value="UK">United Kingdom</option>
                                                                <option value="US">United States Of America</option>
                                                                <option value="">--------------------</option>
                                                                <option value="AF">Afghanistan</option>
                                                                <option value="AL">Albania</option>
                                                                <option value="DZ">Algeria</option>
                                                                <option value="AS">American Samoa</option>
                                                                <option value="AD">Andora</option>
                                                                <option value="AO">Angola</option>
                                                                <option value="AI">Anguilla</option>
                                                                <option value="AQ">Antarctica</option>
                                                                <option value="AG">Antigua & Barbados</option>
                                                                <option value="AR">Argentina</option>
                                                                <option value="AM">Armenia</option>
                                                                <option value="AW">Aruba</option>
                                                                <option value="AT">Austria</option>
                                                                <option value="AZ">Azerbaijan</option>
                                                                <option value="BS?">Bahamas</option>
                                                                <option value="BH">Bahrain</option>
                                                                <option value="BD">Bangladesh</option>
                                                                <option value="BB">Barbados</option>
                                                                <option value="BY">Belarus</option>
                                                                <option value="BE">Belgium</option>
                                                                <option value="BZ">Belize</option>
                                                                <option value="BJ">Benin</option>
                                                                <option value="BM">Bermuda</option>
                                                                <option value="BO">Bolivia</option>
                                                                <option value="BA">Bosnia and Herzegovina</option>
                                                                <option value="BW">Botswana</option>
                                                                <option value="BV">Bouvet Island</option>
                                                                <option value="BR">Brazil</option>
                                                                <option value="IO">British Indian Ocean Territory</option>
                                                                <option value="BN">Brunei</option>
                                                                <option value="BG">Bulgaria</option>
                                                                <option value="BF">Burkina Faso</option>
                                                                <option value="BI">Burundi</option>
                                                                <option value="BT">Butan</option>
                                                                <option value="KH">Cambodia</option>
                                                                <option value="CM">Cameroon</option>
                                                                <option value="CV">Cape Verde</option>
                                                                <option value="KY">Cayman Islands</option>
                                                                <option value="CF">Cental African Republic</option>
                                                                <option value="TD">Chad</option>
                                                                <option value="CL">Chile</option>
                                                                <option value="CN">China</option>
                                                                <option value="CX">Christmas Island</option>
                                                                <option value="CC">Cocos (Keeling) Island</option>
                                                                <option value="CO">Colombia</option>
                                                                <option value="KM">Comoros</option>
                                                                <option value="CG">Congo</option>
                                                                <option value="CK">Cook Island</option>
                                                                <option value="CR">Costa Rica</option>
                                                                <option value="CI">Cote D'ivoire</option>
                                                                <option value="HR">Croatia</option>
                                                                <option value="CU">Cuba</option>
                                                                <option value="CY">Cyprus</option>
                                                                <option value="CZ">Czech Republic</option>
                                                                <option value="DJ">DjiboutiI</option>
                                                                <option value="DM">Dominica</option>
                                                                <option value="DO">Dominican Republic</option>
                                                                <option value="TP">East Timor</option>
                                                                <option value="EC">Ecuador</option>
                                                                <option value="EG">Egypt</option>
                                                                <option value="SV">El Salvador</option>
                                                                <option value="GQ">Equatorial Guinea</option>
                                                                <option value="ER">Eritrea</option>
                                                                <option value="EE">Estonia</option>
                                                                <option value="ET">Ethiopia</option>
                                                                <option value="FK">Falkland Islands</option>
                                                                <option value="FO">Faroe Islands</option>
                                                                <option value="FJ">Fiji</option>
                                                                <option value="FI">Finland</option>
                                                                <option value="FX">France, Metropolitan</option>
                                                                <option value="GF">French Guiana</option>
                                                                <option value="PF">French Polynesia</option>
                                                                <option value="TF">French Souther Territories</option>
                                                                <option value="GA">Gabon</option>
                                                                <option value="GM">Gambia</option>
                                                                <option value="GE">Georgia</option>
                                                                <option value="GH">Ghana</option>
                                                                <option value="GI">Gibraltar</option>
                                                                <option value="GR">Greece</option>
                                                                <option value="GL">Greenland</option>
                                                                <option value="GD">Grenada</option>
                                                                <option value="GP">Guadeloupe</option>
                                                                <option value="GU">Guam</option>
                                                                <option value="GT">Guatemala</option>
                                                                <option value="GN">Guinea</option>
                                                                <option value="GW">Guinea-Bissau</option>
                                                                <option value="GY">Guyana</option>
                                                                <option value="HT">Haiti</option>
                                                                <option value="HM">Heard and McDonald Islands</option>
                                                                <option value="HN">Honduras</option>
                                                                <option value="HK">Hong Kong</option>
                                                                <option value="HU">Hungary</option>
                                                                <option value="IS">Iceland</option>
                                                                <option value="IN">India</option>
                                                                <option value="ID">Indonesia</option>
                                                                <option value="IR">Iran</option>
                                                                <option value="IQ">Iraq</option>
                                                                <option value="IE">Ireland</option>
                                                                <option value="IL">Israel</option>
                                                                <option value="IT">Italy</option>
                                                                <option value="JM">Jamaica</option>
                                                                <option value="JP">Japan</option>
                                                                <option value="JO">Jordan</option>
                                                                <option value="KZ">Kazakhstan</option>
                                                                <option value="KE">Kenya</option>
                                                                <option value="KI">Kiribati</option>
                                                                <option value="KW">Kuwait</option>
                                                                <option value="KG">Kyrgyzstan</option>
                                                                <option value="LA">Lao Peoples Democratic Republic</option>
                                                                <option value="LV">Latvia</option>
                                                                <option value="LB">Lebanon</option>
                                                                <option value="LS">Lesotho</option>
                                                                <option value="LR">Liberia</option>
                                                                <option value="LY">Libya</option>
                                                                <option value="LI">Liechtenstein</option>
                                                                <option value="LT">Lithuania</option>
                                                                <option value="LU">Luxembourg</option>
                                                                <option value="MO">Macau</option>
                                                                <option value="MK">Macedonia</option>
                                                                <option value="MG">Madagascar</option>
                                                                <option value="MW">Malawi</option>
                                                                <option value="MY">Malaysia</option>
                                                                <option value="MV">Maldives</option>
                                                                <option value="ML">Mali</option>
                                                                <option value="MT">Malta</option>
                                                                <option value="MH">Marshall Islands</option>
                                                                <option value="MQ">Martinique</option>
                                                                <option value="MR">Mauritania</option>
                                                                <option value="MU">Mauritius</option>
                                                                <option value="YT">Mayotte</option>
                                                                <option value="MX">Mexico</option>
                                                                <option value="FM">Micronesia, Federated States of</option>
                                                                <option value="MD">Moldova</option>
                                                                <option value="MC">Monaco</option>
                                                                <option value="MN">Mongolia</option>
                                                                <option value="MS">Montserrat</option>
                                                                <option value="MA">Morocco</option>
                                                                <option value="MZ">Mozambique</option>
                                                                <option value="MM">Myanamar</option>
                                                                <option value="NA">Namibia</option>
                                                                <option value="NR">Nauru</option>
                                                                <option value="NP">Nepal</option>
                                                                <option value="AN">Netherlands Antilles</option>
                                                                <option value="NC">New Caledonia</option>
                                                                <option value="NI">Nicaragua</option>
                                                                <option value="NE">Niger</option>
                                                                <option value="NG">Nigeria</option>
                                                                <option value="NU">Niue</option>
                                                                <option value="NF">Norfolk Island</option>
                                                                <option value="KP">North Korea</option>
                                                                <option value="MP">Northern Mariana Islands</option>
                                                                <option value="NO">Norway</option>
                                                                <option value="OM">Oman</option>
                                                                <option value="PK">Pakistan</option>
                                                                <option value="PW">Palau</option>
                                                                <option value="PA">Panama</option>
                                                                <option value="PG">Papua New Guinea</option>
                                                                <option value="PY">Paraguay</option>
                                                                <option value="PE">Peru</option>
                                                                <option value="PH">Philippines</option>
                                                                <option value="PN">Pitcairn Islands</option>
                                                                <option value="PL">Poland</option>
                                                                <option value="PT">Portugal</option>
                                                                <option value="PR">Puerto Rico</option>
                                                                <option value="QA">Qatar</option>
                                                                <option value="RE">Reunion</option>
                                                                <option value="RO">Romania</option>
                                                                <option value="RU">Russian Federation</option>
                                                                <option value="RW">Rwanda</option>
                                                                <option value="SH">Saint Helena</option>
                                                                <option value="KN">Saint Kitts and Nevis</option>
                                                                <option value="LC">Saint Lucia</option>
                                                                <option value="PM">Saint Pierre and Miquelon</option>
                                                                <option value="VC">Saint Vincent and The Grenadines</option>
                                                                <option value="WS">Samoa</option>
                                                                <option value="SM">San Marino</option>
                                                                <option value="ST">Sao Tome and Principe</option>
                                                                <option value="SA">Saudi Arabia</option>
                                                                <option value="SN">Senegal</option>
                                                                <option value="SC">Seychelles</option>
                                                                <option value="SG">Singapore</option>
                                                                <option value="SK">Slovakia</option>
                                                                <option value="SI">Slovenia</option>
                                                                <option value="SB">Solomon Islands</option>
                                                                <option value="SO">Somalia</option>
                                                                <option value="ZA">South Africa</option>
                                                                <option value="GS">South Georgia</option>
                                                                <option value="KR">South Korea</option>
                                                                <option value="ES">Spain</option>
                                                                <option value="SL">Sri Lanka</option>
                                                                <option value="SD">Sudan</option>
                                                                <option value="SR">Suriname</option>
                                                                <option value="SJ">Svalbardand Jan Mayen Islands</option>
                                                                <option value="SY">Syria</option>
                                                                <option value="TI">Tahiti</option>
                                                                <option value="TW">Taiwan</option>
                                                                <option value="TJ">Tajikistan</option>
                                                                <option value="TZ">Tanzania</option>
                                                                <option value="TH">Thailand</option>
                                                                <option value="TG">Togo</option>
                                                                <option value="TK">Tokelau</option>
                                                                <option value="TO">Tonga</option>
                                                                <option value="TT">Trinidad and Tobago</option>
                                                                <option value="TN">Tunisia</option>
                                                                <option value="TR">Turkey</option>
                                                                <option value="TM">Turkmenistan</option>
                                                                <option value="TC">Turks and Caicos Islands</option>
                                                                <option value="TV">Tuvalu</option>
                                                                <option value="UG">Uganda</option>
                                                                <option value="UA">Ukraine</option>
                                                                <option value="AE">United Arab Emirates</option>
                                                                <option value="UY">Uruguay</option>
                                                                <option value="UM">US Minor Outlying Islands</option>
                                                                <option value="UZ">Uzbekistan</option>
                                                                <option value="VU">Vanuatu</option>
                                                                <option value="VA">Vatican City</option>
                                                                <option value="VE">Venezuala</option>
                                                                <option value="VN">Vietnam</option>
                                                                <option value="VG">Virgin Islands (GB)</option>
                                                                <option value="VI">Virgin Islands (US)</option>
                                                                <option value="WF">Wallis and Futuna Islands</option>
                                                                <option value="EH">Western Sahara</option>
                                                                <option value="YE">Yemen</option>
                                                                <option value="YU">Yugoslavia</option>
                                                                <option value="ZR">Zaire</option>
                                                                <option value="ZM">Zambia</option>
                                                                <option value="ZW">Zimbabwe</option>
                                                            </select>
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p>
                                                            <a href="javascript:submitForm();" id="searchButton" class="button">Get Quote</a>
                                                        </p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <input id="dropPickupLocationValue" type="hidden" />

                        <script type="text/javascript">
                            
                                     //$('#dropPickupLocation').unbind('click', setupSelect);
                        </script>

                        <div id="loadingBar" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                            xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                            xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal"
                            xmlns:js="urn:custom-javascript">
                            loading images...</div>
                        <script type="text/javascript" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                            xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                            xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal"
                            xmlns:js="urn:custom-javascript">
                            function loading() {
                                $("#loadingBar").append('.');
                            }
                            loadInterval = setInterval(function () { loading(); }, 1000);
                        </script>
                        <div id="filmstrip-front-container" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                            xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                            xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal"
                            xmlns:js="urn:custom-javascript">
                            <div id="filmstrip-info">
                                <div id="filmstrip-textbox">
                                    <div class="filmstrip-text-slider">
                                        <div class="filmstrip-text-inner">
                                            <div class="filmstrip-text">
                                                <a href="http://www.maui.com.au/motorhome-hire/platinum-campervans-river" title="Platinum River" class="titleText">
                                                    Platinum River</a><div class="filmstrip-description">
                                                        space & storage designed for families</div>
                                                <ul>
                                                    <li><a href="http://www.maui.com.au/motorhome-hire/platinum-campervans-river">Platinum River campervan</a></li><li>
                                                        <span class="separator">|</span><a href="http://www.maui.com.au/motorhome-hire">View all campervans</a></li></ul>
                                            </div>
                                            <div class="filmstrip-text">
                                                <a href="http://www.maui.com.au/motorhome-hire/platinum-campervans-ultima" title="Ultima" class="titleText">
                                                    Ultima</a><div class="filmstrip-description">
                                                        Aussie cuisine courtesy of exterior BBQ</div>
                                                <ul>
                                                    <li><a href="http://www.maui.com.au/motorhome-hire/platinum-campervans-ultima">Ultima campervan</a></li><li>
                                                        <span class="separator">|</span><a href="http://www.maui.com.au/motorhome-hire">View all campervans</a></li></ul>
                                            </div>
                                            <div class="filmstrip-text">
                                                <a href="http://www.maui.com.au/motorhome-hire/platinum-campervans-beach" title="Platinum Beach" class="titleText">
                                                    Platinum Beach</a><div class="filmstrip-description">
                                                        unparalleled personal space</div>
                                                <ul>
                                                    <li><a href="http://www.maui.com.au/motorhome-hire/platinum-campervans-beach">Platinum Beach campervan</a></li><li>
                                                        <span class="separator">|</span><a href="http://www.maui.com.au/ourmotorhomes/platinum/Pages/LandingPage.aspx">View
                                                            all campervans</a></li></ul>
                                            </div>
                                            <div class="filmstrip-text">
                                                <a href="http://www.maui.com.au/motorhome-holiday-advice/regional-highlights/tourism-australia" title="Experience Why"
                                                    class="titleText">Experience Why</a><div class="filmstrip-description">
                                                        There's Nothing Like Australia</div>
                                                <ul>
                                                    <li><a href="http://www.maui.com.au/motorhome-holiday-advice/regional-highlights/tourism-australia">What to
                                                        see and do in Australia</a></li></ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="filmstrip-controlbox">
                                    <div id="filmstrip-arrow" class="blue-arrow-up">
                                    </div>
                                    <div id="filmstrip-control">
                                        <div class="filmstrip-text">
                                            <ul>
                                                <li class="slider-control"></li>
                                                <li class="slider-control"></li>
                                                <li class="slider-control"></li>
                                                <li class="slider-control"></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="filmstrip-images" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                            xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                            xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal"
                            xmlns:js="urn:custom-javascript">
                            <div class="element">
                                <img alt="Platinum River Motorhome" border="0" src="http://www.maui.com.au/SiteCollectionImages/Homepage/Large-River-Exterior.jpg"
                                    style="border: 0px solid;"></div>
                            <div class="element">
                                <img alt="Ultima interior" border="0" src="http://www.maui.com.au/SiteCollectionImages/Homepage/Large-Ultima-Interior.jpg"
                                    style="border: 0px solid;"></div>
                            <div class="element">
                                <img alt="Platinum Beach" border="0" src="http://www.maui.com.au/SiteCollectionImages/Homepage/Large-Beach-Interior.jpg"
                                    style="border: 0px solid;"></div>
                            <div class="element">
                                <img alt="" border="0" src="http://www.maui.com.au/SiteCollectionImages/Homepage/Large-Home-Tourism-Australia.jpg"
                                    style="border: 0px solid;"></div>
                        </div>
                    </div>
                </div>
                <div id="contentPlaceHolder">
                    <div id="quickInfo" class="contentFrame">
                        <div class="col">
                            <h2>
                                &nbsp;contact us 24/7&nbsp;</h2>
                            <div class="col-main">
                                <div class="text">
                                    <div id="ctl00_PlaceHolderMain_quickinfoContent1__ControlWrapper_RichHtmlField" style="display: inline">
                                        <p>
                                            International tollfree</p>
                                        <ul>
                                            <li><a title="Contact us freephone 24/7" href="http://www.maui.com.au/contact-maui-motorhome-hire-australia">
                                                Where are you calling from?</a></li></ul>
                                    </div>
                                </div>
                                <div class="img">
                                    <div id="ctl00_PlaceHolderMain_quickinfoImg1__ControlWrapper_RichImageField" style="display: inline">
                                        <span dir=""><a href="http://www.maui.com.au/contact-maui-motorhome-hire-australia">
                                            <img alt="Contact Us 24 hours 7 days a week" border="0" src="http://www.maui.com.au/SiteCollectionImages/Homepage/24-7-logo.jpg"
                                                style="border: 0px solid"></a></span>&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <h2>
                                &nbsp;browse vehicles&nbsp;</h2>
                            <div class="col-main">
                                <div class="text">
                                    <div id="ctl00_PlaceHolderMain_ctl02__ControlWrapper_RichHtmlField" style="display: inline">
                                        <ul>
                                            <li><a title="Campervan hire Australia" href="http://www.maui.com.au/motorhome-hire">Campervan hire selection</a></li>
                                            <li><a title="Car rental Australia" href="http://www.maui.com.au/car-rental">Car rental selection</a></li></ul>
                                    </div>
                                </div>
                                <div class="img">
                                    <div id="ctl00_PlaceHolderMain_ctl03__ControlWrapper_RichImageField" style="display: inline">
                                        <span dir=""><a href="http://www.maui.com.au/motorhome-hire">
                                            <img alt="Campervan or Motorhome" border="0" src="http://www.maui.com.au/SiteCollectionImages/Homepage/campervan.jpg"
                                                style="border: 0px solid"></a></span>&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <h2>
                                &nbsp;plan your journey&nbsp;</h2>
                            <div class="col-main">
                                <div class="text">
                                    <div id="ctl00_PlaceHolderMain_ctl05__ControlWrapper_RichHtmlField" style="display: inline">
                                        <ul>
                                            <li><a title="Campervan Driving routes in Australia" href="http://www.maui.com.au/motorhome-holiday-advice/campervan-driving-routes">
                                                Campervan driving routes</a></li>
                                            <li><a title="Meals ideas to make in your campervan" href="http://www.maui.com.au/motorhome-holiday-advice/australia-produce">
                                                Meals in your camper</a></li>
                                            <li><a title="Tips for driving your campervan in Australia" href="http://www.maui.com.au/motorhome-holiday-advice/campervan-driving-tips">
                                                Campervan driving tips</a></li>
                                            <li><a title="There's Nothing Like Australia" href="http://www.maui.com.au/motorhome-holiday-advice/regional-highlights/tourism-australia">
                                                There's Nothing Like Australia</a></li></ul>
                                    </div>
                                </div>
                                <div class="img">
                                    <div id="ctl00_PlaceHolderMain_ctl06__ControlWrapper_RichImageField" style="display: inline">
                                        <span dir=""><a href="http://www.maui.com.au/motorhome-holiday-advice/regional-highlights">
                                            <img alt="Australian Map" border="0" src="http://www.maui.com.au/SiteCollectionImages/Homepage/au-map.jpg"
                                                style="border: 0px solid"></a></span>&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <h2>
                                &nbsp;camp responsibly&nbsp;</h2>
                            <div class="col-main">
                                <div class="text">
                                    <div id="ctl00_PlaceHolderMain_ctl08__ControlWrapper_RichHtmlField" style="display: inline">
                                        <ul>
                                            <li><a title="" href="http://www.maui.com.au/motorhome-holiday-advice/australian-accommodation">Where to stay</a></li>
                                            <li><a title="" href="http://www.maui.com.au/why-choose-maui-campervans/environmental-commitment">Environmental
                                                commitment</a></li></ul>
                                    </div>
                                </div>
                                <div class="img">
                                    <div id="ctl00_PlaceHolderMain_ctl09__ControlWrapper_RichImageField" style="display: inline">
                                        <span dir=""><a href="http://www.maui.com.au/motorhome-holiday-advice/australian-accommodation">
                                            <img alt="Big4 Holiday Parks" border="0" src="http://www.maui.com.au/SiteCollectionImages/Homepage/big4.jpg"
                                                style="border: 0px solid"></a></span>&nbsp;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="contentMain" class="contentFrame">
                        <!-- AddThis Button BEGIN -->
                        <div class="addthis_toolbox addthis_default_style">
                            <a href="http://www.addthis.com/bookmark.php?v=250&amp;pubid=thlonline" class="addthis_button_compact">
                            </a><span class="addthis_separator">|</span> <a class="addthis_button_preferred_1">
                            </a><a class="addthis_button_preferred_2"></a><a class="addthis_button_preferred_3">
                            </a><a class="addthis_button_email"></a>
                            <!--<a class="addthis_button_google_plusone"></a>-->
                        </div>
                        <!-- AddThis Button END -->
                        <h1>
                            &nbsp;maui motorhome rental &amp; campervan hire Australia&nbsp;</h1>
                        <div class="section" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                            xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                            xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                            <div class="contentMainLeft">
                                <p>
                                    <p>
                                        <strong>maui</strong> is an experienced and trusted campervan hire and car rental
                                        brand in Australia and we have delivered great self-drive holidays for more than
                                        25 years. We are focused on our continual quest for design enhancement and innovation
                                        to ensure our travellers, just like you, experience the very best in campervan holidays
                                        with our stylish interiors and spacious design right down to our warm personalised
                                        service.</p>
                                </p>
                            </div>
                            <div class="contentMainRight">
                                <span class="thumb"></span>
                            </div>
                        </div>
                        <div class="section" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                            xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                            xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                            <div class="contentMainLeft">
                                <h2>
                                    our campervan hire range</h2>
                                <p>
                                    <p>
                                        If your idea of a real holiday is to have all the comforts of home at your fingertips,
                                        whilst having the flexibility to travel wherever the road may take you, then we
                                        think a maui campervan hire is the perfect self-drive option for your Australian
                                        journey. With the freedom and flexibility to explore at your own pace, you can stay
                                        in one place for as long as you like. Because you decide the pace, the only schedule
                                        you have to work to is your own.</p>
                                    <p>
                                        Choose from our range of <a title="Campervan hire in Australia" href="/motorhome-hire">
                                            <strong>maui</strong> campervans</a> that sleep up to two, four and six people
                                        – perfect whether you are travelling as a couple, family or group of friends.
                                    </p>
                                    <p>
                                        If you are looking for ultimate space, style &amp; comfort, then we recommend <strong>
                                            maui Platinum campervans</strong> to you. Designed with warm neutral tones,
                                        wooden floors, real leather couches, reading spotlights and roomy living areas,
                                        we believe they have a feel more akin to an apartment than a traditional motorhome.</p>
                                    <p>
                                        Sleeping between two and six people comfortably, the <a title="Ultima Campervan hire Australia"
                                            href="http://www.maui.com.au/motorhome-hire/platinum-campervans-ultima">Ultima</a>, <a title="Platinum Beach Motorhome - Sleeps 4"
                                                href="http://www.maui.com.au/motorhome-hire/platinum-campervans-beach">Platinum Beach</a>, <a title="Platinum River Motorhome Rental"
                                                    href="http://www.maui.com.au/motorhome-hire/platinum-campervans-river">Platinum River</a>
                                        and <a title="Platinum Forest Camper Hire Australia" href="http://www.maui.com.au/motorhome-hire/platinum-campervans-forest">
                                            Platinum Forest</a> campervans each feature a unique layout design, so you can
                                        choose the layout and space that works best for you. Built on quality European Mercedes
                                        or VW chassis, the Platinum campervan series drive as superbly as they look. Plus
                                        they’re automatic transmission, so a pleasure and breeze to drive.</p>
                                </p>
                            </div>
                            <div class="contentMainRight">
                                <img alt="Platinum Lake" border="0" src="http://www.maui.com.au/SiteCollectionImages/Vehicles/Crosslink-Lake-Interior.jpg"
                                    style="border: 0px solid;"><ul>
                                        <li><a title="" href="http://www.maui.com.au/motorhome-hire/platinum-campervans-ultima">Ultima</a></li>
                                        <li><a title="" href="http://www.maui.com.au/motorhome-hire/platinum-campervans-river">Platinum River</a></li>
                                        <li><a title="" href="http://www.maui.com.au/motorhome-hire/platinum-campervans-forest">Platinum Forest</a></li></ul>
                                <span class="thumb"></span>
                            </div>
                        </div>
                        <div class="section" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                            xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                            xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                            <div class="contentMainLeft">
                                <h2>
                                    apartment-style kitchen &amp; living equipment</h2>
                                <p>
                                    <p>
                                        We try to think of everything that will make your campervan holiday easy and convenient.
                                        So you’ll find all our motorhomes are stocked with apartment-style kitchen equipment
                                        (including holiday essentials such as wine glasses, china crockery and coffee plunger),
                                        freshly laundered bedding and a range of other useful items so you can travel the
                                        country in our <strong>maui</strong> campervans without sacrificing the comforts
                                        of home.
                                    </p>
                                </p>
                            </div>
                            <div class="contentMainRight">
                                <img alt="Apartment style facilities" border="0" src="http://www.maui.com.au/SiteCollectionImages/Homepage/Crosslink-Apartment.jpg"
                                    style="border: 0px solid;"><span class="thumb"></span></div>
                        </div>
                        <div class="section" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                            xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                            xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                            <div class="contentMainLeft">
                                <h2>
                                    the personal touch for your campervan hire Australia</h2>
                                <p>
                                    <p>
                                        When you travel with us, you can also be sure that our service compliments our superb
                                        motorhomes . We know it’s the small things that count, from a personal recommendation
                                        of the best cafe in town to the convenience of being able to replenish your linen
                                        at any of our branches during the course of your campervan holiday. We have a range
                                        of exclusive maui services which you are free to use if you wish:</p>
                                    <p>
                                        <strong>Linen Exchange</strong> - if you wish to replace any of your linen and bedding
                                        for a freshly laundered supply, just swing by any of our branches during our regular
                                        opening hours.</p>
                                    <p>
                                        <strong>Valet Service</strong> - if you are travelling for over 21 days, you can
                                        arrange with the branch closest to you to have your camper returned to the spotless
                                        condition you picked it up in. To make an appointment for your campervan valet service,
                                        all you have to do is call the branch you have selected 48 hours ahead of time.</p>
                                    <p>
                                        <strong>Express Return (available if you have selected the maui premium package)</strong>
                                        - to help you get away with a minimum of fuss once you have returned your camper,
                                        you can simply drop off all sets of keys at the return desk. We would just ask that
                                        you do this during our branch opening hours.</p>
                                    <p>
                                        Peace of mind makes a holiday even more relaxing so we also offer 24/7 Customer
                                        Care on-road assistance should you need us. We are just a phone call away.</p>
                                </p>
                            </div>
                            <div class="contentMainRight">
                                <img alt="Maui service" border="0" src="http://www.maui.com.au/SiteCollectionImages/Locations-Holiday/Crosslink-Maui-Service.jpg"
                                    style="border: 0px solid;"><ul>
                                        <li><a title="" href="http://www.maui.com.au/motorhome-holiday-advice/maui-customer-care">Maui service on
                                            the road</a></li></ul>
                                <span class="thumb"></span>
                            </div>
                        </div>
                        <div class="section" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                            xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                            xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                            <div class="contentMainLeft">
                                <h2>
                                    car rentals australia</h2>
                                <p>
                                    <p>
                                        We also have a selection of late model <a title="Car rentals in Australia" href="/car-rental">
                                            car rentals</a> if you are looking to nip away to a hotel or perhaps a friend’s
                                        holiday home. All maui car rentals are from 0-18 months old and you can be sure
                                        you are getting a great quality car for a great price.
                                    </p>
                                </p>
                            </div>
                            <div class="contentMainRight">
                                <span class="thumb"></span>
                            </div>
                        </div>
                        <div class="section" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                            xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                            xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                            <div class="contentMainLeft">
                                <h2>
                                    maui - quality campervans and motorhomes</h2>
                                <p>
                                    <p>
                                        You may already know that maui is part of Tourism Holdings Limited (thl), a business
                                        firmly established as New Zealand's premier tourism company with operations also
                                        in Australia. Maui, has a new vehicle sales brand: <a title="" href="http://www.motekvehicles.com/"
                                            target="_blank">Motek</a>. <a title="" href="http://www.motekvehicles.com/" target="_blank">
                                                Motek</a> manufacturing excellence means you can rest assured that your
                                        camper has been designed and built by an experienced company within the thl house
                                        of brands., ensuring manufacturing and design excellence.
                                    </p>
                                    <p>
                                        We also take the environment seriously and have partnered with the Leave No Trace
                                        programme in Australia.</p>
                                    <p>
                                        It would be our pleasure to have you travel with us for your campervan hire Australia.
                                        Please contact our team, anytime, 24/7.
                                    </p>
                                </p>
                            </div>
                            <div class="contentMainRight">
                                <img alt="Quality vehicles" border="0" src="http://www.maui.com.au/SiteCollectionImages/WhyChooseMaui/Crosslink-Quality-Vehicles.jpg"
                                    style="border: 0px solid;"><ul>
                                        <li><a title="" href="http://www.maui.com.au/why-choose-maui-campervans/quality-vehicles">Quality motorhome
                                            manufacturing and maintenance</a></li></ul>
                                <span class="thumb"></span>
                            </div>
                        </div>
                        <div class="addthis_toolbox addthis_default_style ">
                            <a href="http://www.addthis.com/bookmark.php?v=250&amp;pubid=thlonline" class="addthis_button_compact">
                                Share</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div id="footerContainer">
                    <div id="footerTop">
                        <div id="custom-reviews">
                            <h2>
                                Customer reviews:</h2>
                            <ul class="cr-list">
                                <li xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                    <div>
                                        I'm looking forward to travelling to Maui again in the near future. I'd like to
                                        thank the team of the maui for providing a wonderful service and kind staff – Matthew
                                        Tran</div>
                                </li>
                                <li xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                    <div>
                                        We thoroughly enjoyed our break and the camper van was excellent in every way. Kay
                                        and Dave Hey</div>
                                </li>
                                <li xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                    <div>
                                        I want to congratulate you on the superb service.&nbsp; It has made planning from
                                        a distance easy and effective.&nbsp; Richard Derman</div>
                                </li>
                            </ul>
                        </div>
                        <ul id="socialMedia">
                            <li id="list-findus">Find us on:</li>
                            <li id="list-youtube"><a target="_blank" title="Check out our YouTube Channel" href="http://www.youtube.com/user/MauiMotorhomes">
                                <span>YouTube</span></a></li>
                            <li id="list-twitter"><a target="_blank" title="Follow us on Twitter" href="http://twitter.com/mauirentals">
                                <span>Twitter</span></a></li>
                            <li id="list-facebook"><a target="_blank" title="Follow us on Facebook" href="http://www.facebook.com/mauimotorhomesfanpage">
                                <span>Facebook</span></a></li>
                        </ul>
                    </div>
                    <div id="footerBottom">
                        <div id="footerBottomLeft">
                            <div class="col">
                                <div class="col" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                    <div class="item">
                                        <h3>
                                            browse our vehicles</h3>
                                        <div class="item-href">
                                            <div class="ExternalClass70E0A49ECE5A45A1A687DD2F39051CA1">
                                                <div>
                                                </div>
                                            </div>
                                        </div>
                                        <p>
                                            <a title="Luxury motorhome and campervan hire in Australia" href="http://www.maui.com.au/motorhome-hire">Platinum
                                                campervan hire series</a></li>
                                            <br>
                                            <a title="Car Hire New Zealand" href="http://www.maui.com.au/car-rental">Car rentals</a>
                                        </p>
                                    </div>
                                    <div class="item">
                                        <h3>
                                            holiday advice</h3>
                                        <div class="item-href">
                                            <div class="ExternalClass50EB2E17C16745948FAA7975E074FA23">
                                                <div>
                                                </div>
                                            </div>
                                        </div>
                                        <p>
                                            <a title="What to pack for your New Zealand campervan journey" href="http://www.maui.com.au/motorhome-holiday-advice/what-to-pack">
                                                What to pack</a><br>
                                            <a title="Where to stay in your camper" href="http://www.maui.com.au/motorhome-holiday-advice/australian-accommodation">
                                                Where to stay in your camper</a><br>
                                            <a title="" href="http://www.maui.com.au/motorhome-holiday-advice/maui-customer-care">Maui service on the
                                                road</a><br>
                                            <a title="Meals in your campervan or motorhome" href="http://www.maui.com.au/motorhome-holiday-advice/australia-produce">
                                                Meals in your camper</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="col" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                    <div class="item">
                                        <h3>
                                            maui branches</h3>
                                        <div class="item-href">
                                            <div class="ExternalClass0AE114E216AA486FADD8F7A56AA0D432">
                                                <a href="http://www.maui.com.au/rentals-australia">branch map</a></div>
                                        </div>
                                        <p>
                                            <a title="Adelaide Campervan hire" href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/adelaide">
                                                Adelaide</a><br>
                                            <a title="Alice Springs campervan hire" href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/alice-springs">
                                                Alice Springs</a><br>
                                            <a title="Brisbane campervan hire" href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/brisbane">
                                                Ballina - Byron Bay</a><br>
                                            <a title="Ballina - Byron Bay campervan hire" href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/ballina-byronbay">
                                                Brisbane</a><br>
                                            <a title="Broome campervan hire" href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/broome">
                                                Broome</a><br>
                                            <a title="Carins campervan hire" href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/cairns">
                                                Cairns</a><br>
                                            <a title="Darwin campervan hire" href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/darwin">
                                                Darwin</a><br>
                                            <a title="Hobart campervan hire" href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/hobart">
                                                Hobart</a><br>
                                            <a title="Melbourne campervan hire" href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/melbourne">
                                                Melbourne</a><br>
                                            <a title="Perth motorhome hire" href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/perth">
                                                Perth</a><br>
                                            <a title="Sydney campervan hire" href="http://www.maui.com.au/rentals-australia/campervan-hire-locations/sydney">
                                                Sydney</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="col" xmlns:x="http://www.w3.org/2001/XMLSchema" xmlns:d="http://schemas.microsoft.com/sharepoint/dsp"
                                    xmlns:asp="http://schemas.microsoft.com/ASPNET/20" xmlns:__designer="http://schemas.microsoft.com/WebParts/v2/DataView/designer"
                                    xmlns:sharepoint="Microsoft.SharePoint.WebControls" xmlns:ddwrt2="urn:frontpage:internal">
                                    <div class="item">
                                        <h3>
                                            save time and check-in online</h3>
                                        <div class="item-href">
                                            <div class="ExternalClassA170BDD6E3D94138A7F5EBA1EBD147F5">
                                                <div>
                                                    <a href="http://www.maui.com.au/online-rental-check-in">check-in online</a></div>
                                            </div>
                                        </div>
                                        <p>
                                            If you want to save time at the branch when picking up your motorhome, you have
                                            the option to use our Self Check-in service - just like you would at the airport.
                                            You can choose to use our online Self Check-in at your convenience from any computer
                                            or when you arrive at our branch.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="footerBottomRight">
                            <div id="customerService">
                                <h3>
                                    Customer service</h3>
                                <ul class="cs-simple">
                                    <li class="callus">Call us anytime 24/7 International Toll Free</li>
                                    <li>
                                        <script type="text/javascript" src="https://www.google.com/jsapi?key=ABQIAAAAWqQjtvG0Ey1zuy0Yi5ylrRSvB4ANtyecigTcIZwt9YT7B9rUjxTr5Dy-RjXevBpd_FVxDcP65itgeQ"></script>
                                        <script type="text/javascript">
                                            $(document).ready(function () {

                                                if (google.loader.ClientLocation != null) {
                                                    $("#min #countrySelector").val(google.loader.ClientLocation.address.country_code);
                                                    var ddElm = $("#min #countrySelector");
                                                    var trElm = $('#' + ddElm.val() + '_tr');
                                                    var number = $('.fx', trElm).text() + " " + $('.no', trElm).text();
                                                    $('#min  #countryTollFree').text(number);
                                                    if (this.value == '-1')
                                                        $('#min #hideLabel').css('display', 'none');
                                                    else
                                                        $('#min #hideLabel').css('display', 'block');
                                                }
                                                $("#min #countrySelector").change(function () {
                                                    var trElm = $('#' + this.value + '_tr');
                                                    var number = $('.fx', trElm).text() + " " + $('.no', trElm).text();
                                                    $('#min #countryTollFree').text(number);
                                                    if (this.value == '-1')
                                                        $('#min #hideLabel').css('display', 'none');
                                                    else
                                                        $('#min #hideLabel').css('display', 'block');
                                                });
                                            });
                                        </script>
                                        <div id="min" class="intNumbersDiv">
                                            <div id="callSelector">
                                                <span>
                                                    <label for="countrySelector">
                                                        calling from:</label>
                                                    <select id='countrySelector'>
                                                        <option value='-1'>Select your country</option>
                                                        <option value='AR'>Argentina</option>
                                                        <option value='AU'>Australia</option>
                                                        <option value='AT'>Austria</option>
                                                        <option value='BE'>Belgium</option>
                                                        <option value='BR'>Brazil</option>
                                                        <option value='CA'>Canada</option>
                                                        <option value='CN'>China</option>
                                                        <option value='DK'>Denmark</option>
                                                        <option value='FI'>Finland</option>
                                                        <option value='FR'>France</option>
                                                        <option value='DE'>Germany</option>
                                                        <option value='HK'>Hong Kong PCCW</option>
                                                        <option value='HKW'>Hong Kong Wharf</option>
                                                        <option value='HKH'>Hong Kong Hutchison Telecom</option>
                                                        <option value='HKN'>Hong Kong New World</option>
                                                        <option value='HU'>Hungary</option>
                                                        <option value='IE'>Ireland</option>
                                                        <option value='IT'>Italy</option>
                                                        <option value='JP'>Japan</option>
                                                        <option value='MY'>Malaysia</option>
                                                        <option value='NL'>Netherlands</option>
                                                        <option value='NZ'>New Zealand</option>
                                                        <option value='NO'>Norway</option>
                                                        <option value='PH'>Philippines</option>
                                                        <option value='ZA'>South Africa</option>
                                                        <option value='KP'>South Korea</option>
                                                        <option value='ES'>Spain</option>
                                                        <option value='SE'>Sweden</option>
                                                        <option value='CH'>Switzerland</option>
                                                        <option value='TW'>Taiwan</option>
                                                        <option value='UK'>United Kingdom</option>
                                                        <option value='US'>USA</option>
                                                    </select>
                                                </span><span>
                                                    <label for="countryTollFree" id="hideLabel" style="display: none;">
                                                        dial:</label>
                                                    <big id="countryTollFree"></big></span>
                                            </div>
                                            <div id="toolFreeList">
                                                <span class="hideIntTable">+ show all international tollfree numbers</span>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Country
                                                            </th>
                                                            <th>
                                                                Prefix
                                                            </th>
                                                            <th>
                                                                Number
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr id='AR_tr'>
                                                            <td>
                                                                Argentina
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='AU_tr'>
                                                            <td>
                                                                Australia
                                                            </td>
                                                            <td class='fx'>
                                                                1
                                                            </td>
                                                            <td class='no'>
                                                                300 363 800
                                                            </td>
                                                        </tr>
                                                        <tr id='AT_tr'>
                                                            <td>
                                                                Austria
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='BE_tr'>
                                                            <td>
                                                                Belgium
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='BR_tr'>
                                                            <td>
                                                                Brazil
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='CA_tr'>
                                                            <td>
                                                                Canada
                                                            </td>
                                                            <td class='fx'>
                                                                011
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='CN_tr'>
                                                            <td>
                                                                China
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='DK_tr'>
                                                            <td>
                                                                Denmark
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='FI_tr'>
                                                            <td>
                                                                Finland
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='FR_tr'>
                                                            <td>
                                                                France
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='DE_tr'>
                                                            <td>
                                                                Germany
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='HK_tr'>
                                                            <td>
                                                                Hong Kong PCCW
                                                            </td>
                                                            <td class='fx'>
                                                                001
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='HKW_tr'>
                                                            <td>
                                                                Hong Kong Wharf
                                                            </td>
                                                            <td class='fx'>
                                                                007
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='HKH_tr'>
                                                            <td>
                                                                Hong Kong Hutchison Telecom
                                                            </td>
                                                            <td class='fx'>
                                                                008
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='HKN_tr'>
                                                            <td>
                                                                Hong Kong New World
                                                            </td>
                                                            <td class='fx'>
                                                                009
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='HU_tr'>
                                                            <td>
                                                                Hungary
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='IE_tr'>
                                                            <td>
                                                                Ireland
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='IT_tr'>
                                                            <td>
                                                                Italy
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='JP_tr'>
                                                            <td>
                                                                Japan
                                                            </td>
                                                            <td class='fx'>
                                                                010
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='MY_tr'>
                                                            <td>
                                                                Malaysia
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='NL_tr'>
                                                            <td>
                                                                Netherlands
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='NZ_tr'>
                                                            <td>
                                                                New Zealand
                                                            </td>
                                                            <td class='fx'>
                                                                0
                                                            </td>
                                                            <td class='no'>
                                                                800 651 080
                                                            </td>
                                                        </tr>
                                                        <tr id='NO_tr'>
                                                            <td>
                                                                Norway
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='PH_tr'>
                                                            <td>
                                                                Philippines
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='ZA_tr'>
                                                            <td>
                                                                South Africa
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='KP_tr'>
                                                            <td>
                                                                South Korea
                                                            </td>
                                                            <td class='fx'>
                                                                001
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='ES_tr'>
                                                            <td>
                                                                Spain
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='SE_tr'>
                                                            <td>
                                                                Sweden
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='CH_tr'>
                                                            <td>
                                                                Switzerland
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='TW_tr'>
                                                            <td>
                                                                Taiwan
                                                            </td>
                                                            <td class='fx'>
                                                                002
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='UK_tr'>
                                                            <td>
                                                                United Kingdom
                                                            </td>
                                                            <td class='fx'>
                                                                00
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                        <tr id='US_tr'>
                                                            <td>
                                                                USA
                                                            </td>
                                                            <td class='fx'>
                                                                011
                                                            </td>
                                                            <td class='no'>
                                                                800 200 80 801
                                                            </td>
                                                        </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div id="search">
                                <h3>
                                    Search the site</h3>
                                <div id="searchField">
                                    <div id="ctl00_SearchBox1" __markuptype="vsattributemarkup" __webpartid="{43a3e458-0a84-4f0e-96c9-b05a4b9182ec}"
                                        webpart="true">
                                        <script language="javascript" type="text/javascript">                                            var __nonMSDOMBrowser = (window.navigator.appName.toLowerCase().indexOf('explorer') == -1); function WebForm_FireDefaultButton(event, target) { if (event.keyCode == 13 && !(event.srcElement && (event.srcElement.tagName.toLowerCase() == "textarea"))) { var defaultButton; if (__nonMSDOMBrowser) { defaultButton = document.getElementById(target); } else { defaultButton = document.all[target]; } if (defaultButton && typeof (defaultButton.click) != "undefined") { defaultButton.click(); event.cancelBubble = true; if (event.stopPropagation) event.stopPropagation(); return false; } } return true; }</script>
                                        <input name="ctl00$SearchBox1$txtSearchTerms" type="text" maxlength="255" id="ctl00_SearchBox1_txtSearchTerms"
                                            title="Enter search words" class="SearchTerm" name="txtSearchTerms" onclick="document.forms[0].onkeypress = new Function(&quot;return WebForm_FireDefaultButton(event, 'ctl00_SearchBox1_SearchButton');&quot;);"
                                            onfocus="document.forms[0].onkeypress = new Function(&quot;return WebForm_FireDefaultButton(event, 'ctl00_SearchBox1_SearchButton');&quot;);" /><input
                                                type="image" name="ctl00$SearchBox1$SearchButton" id="ctl00_SearchBox1_SearchButton"
                                                title="Search website" class="SearchButton" src="http://www.maui.com.au/SiteCollectionImages/assets/gobutton.png"
                                                style="border-width: 0px;" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="footerLinks">
                    <div id="footerLinksTop">
                        <ul>
                            <!--<li><a href="#" title="Sign up for news">Sign up for news</a></li>-->
                            <li><a href="http://www.maui.co.nz" title="maui New Zealand"><strong>maui</strong> new
                                zealand</a></li>
                            <li><a href="http://www.maui.com.au/motorhome-hire-faqs" title="FAQs">FAQs</a></li>
                            <li><a href="http://www.maui.com.au/motorhome-hire/buy-a-motorhome" title="Buy a maui motorhome">buy a <strong>
                                maui</strong> motorhome</a></li>
                            <!--<li><a href="#" title="Booking Process">Booking Process</a></li>-->
                            <!--<li><a href="#" title="Qualmark">Qualmark</a></li>-->
                            <li><a href="http://www.maui.com.au/Pages/Sitemap.aspx" title="Sitemap"><strong>maui</strong> australia sitemap</a></li>
                        </ul>
                    </div>
                    <div id="footerLinksBottom">
                        <ul id="footerLinksLogos">
                            <li id="logos"><a href="http://www.thlonline.com" title="Tourism Holding Ltd" id="THLLogo">
                                THL</a> <a href="http://www.roadbearrv.com" title="Road Bear RV USA" id="roadbearlogo">
                                    Road Bear RV USA</a> <a href="http://www.maui-rentals.com" title="maui motorhomes & car rental"
                                        id="MauiLogo">Maui</a> <a href="http://www.britz.com" title="Britz" id="BritzLogo">Britz</a>
                                <a href="http://www.mightycampers.com" title="Mighty campervan & car hire" id="mightyLogo">
                                    Mighty Campers</a> <a href="http://www.motekvehicles.com" title="Motek Vehicles"
                                        id="moteklogo">Motek Vehicles</a> </li>
                            <li>
                                <ul>
                                    <li>You are viewing www.maui.com.au. See all <a href="http://www.thlonline.com" title="thl brands">
                                        thl brands</a></li>
                                </ul>
                                <ul>
                                    <li><a href="http://www.maui.com.au/privacy-policy" title="Privacy" class="minor">Privacy Policy</a></li>
                                    <li><a href="http://www.maui.com.au/legal" title="Terms and Conditions" class="minor">Website Terms and Conditions</a></li>
                                    <li>&#169; <span class="footer-year"></span>
                                        <script>                                            var currentTime = new Date(); jQuery('.footer-year').html(currentTime.getFullYear());</script>
                                        <strong><i>&nbsp;thl </i></strong></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!--TQUAL LOGO-->
                    <div id="tqual">
                        <img src="http://www.maui.com.au/SiteCollectionImages/assets/tqual.png" alt="TQUAL - Australian Tourism Quality Assured"
                            width="95" height="40" />
                    </div>
                </div>
            </div>
            <div id="popupFooter">
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
    <div>
        <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEWAwKQgc6ODwLXgr2SDgKAy4/eD5jJHDCAswJjEi3mf1S+ReIptdpN" />
    </div>
    <script type="text/javascript">
//<![CDATA[

        WebForm_InitCallback(); var __wpmExportWarning = 'This Web Part Page has been personalized. As a result, one or more Web Part properties may contain confidential information. Make sure the properties contain information that is safe for others to read. After exporting this Web Part, view properties in the Web Part description file (.WebPart) by using a text editor such as Microsoft Notepad.'; var __wpmCloseProviderWarning = 'You are about to close this Web Part.  It is currently providing data to other Web Parts, and these connections will be deleted if this Web Part is closed.  To close this Web Part, click OK.  To keep this Web Part, click Cancel.'; var __wpmDeleteWarning = 'You are about to permanently delete this Web Part.  Are you sure you want to do this?  To delete this Web Part, click OK.  To keep this Web Part, click Cancel.';//]]>
    </script>
    <script type="text/javascript" language="javascript" src="http://www.maui.com.au/_layouts/1033/core.js?rev=CNBZRdV1h3pKuA7LsMXf3w%3D%3D"></script>
    </form>
    <!-- BEGIN: Marin Software Tracking Script -->
    <script type="text/javascript">        var _mTrack = _mTrack || []; _mTrack.push(['trackPage']); (function () { var mClientId = '1624rrj16003'; var mProto = ('https:' == document.location.protocol ? 'https://' : 'http://'); var mHost = 'tracker.marinsm.com'; var mt = document.createElement('script'); mt.type = 'text/javascript'; mt.async = true; mt.src = mProto + mHost + '/tracker/async/' + mClientId + '.js'; var fscr = document.getElementsByTagName('script')[0]; fscr.parentNode.insertBefore(mt, fscr); })(); </script>
    <noscript>
        <img src="https://tracker.marinsm.com/tp?act=1&cid=1624rrj16003&script=no">
    </noscript>
    <!-- END: Copyright Marin Software -->
    <!-- Bold Software Visitor Monitor HTML v4.00 (Website=Maui AU,ChatButton=Maui Floating Chat Button,ChatInvitation=Maui Invite Ruleset) -->
    <script type="text/javascript">
        var _bcvma = _bcvma || [];
        _bcvma.push(["setAccountID", "308737748753154415"]);
        _bcvma.push(["setParameter", "WebsiteDefID", "5732516405649115384"]);
        _bcvma.push(["setParameter", "InvitationDefID", "3553932244050431"]);
        _bcvma.push(["setParameter", "VisitName", ""]);
        _bcvma.push(["setParameter", "VisitPhone", ""]);
        _bcvma.push(["setParameter", "VisitEmail", ""]);
        _bcvma.push(["setParameter", "VisitRef", ""]);
        _bcvma.push(["setParameter", "VisitInfo", "CHATmauiau"]);
        _bcvma.push(["setParameter", "CustomUrl", ""]);
        _bcvma.push(["setParameter", "WindowParameters", ""]);
        _bcvma.push(["addFloat", { type: "chat", id: "4230793468628211388"}]);
        _bcvma.push(["pageViewed"]);
        (function () {
            var vms = document.createElement("script"); vms.type = "text/javascript"; vms.async = true;
            vms.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + "vmss.boldchat.com/aid/308737748753154415/bc.vms4/vms.js";
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(vms, s);
        })();
    </script>
    <noscript>
        <a href="http://www.boldchat.com" title="Visitor Monitoring" target="_blank">
            <img alt="Visitor Monitoring" src="https://vms.boldchat.com/aid/308737748753154415/bc.vmi?wdid=5732516405649115384&amp;vr=&amp;vi=&amp;vn=&amp;vp=&amp;ve=&amp;curl="
                border="0" width="1" height="1" /></a>
    </noscript>
    <!-- /Bold Software Visitor Monitor HTML v4.00 -->
    <!-- Google Code for Maui AU Home Page -->
    <script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1043274355;
var google_conversion_label = "DahiCK-U-QEQ87S88QM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
    </script>
    <script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display: inline;">
            <img height="1" width="1" style="border-style: none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1043274355/?value=0&amp;label=DahiCK-U-QEQ87S88QM&amp;guid=ON&amp;script=0" />
        </div>
    </noscript>
</body>
</html>
