﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WidgetContainer.aspx.cs" Inherits="UnitTests_WidgetContainer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Booking Widget Test Zone</title> 
    </head>
    <body>
        <style type="text/css">
                    
        </style>
        <form id="form1" runat="server">        
            <div id="bookingWidget" style="width:225px; height:451; position:absolute;" >                
                <script type="text/javascript">
                    window.onload = function () {
                        document.getElementById("widgetHead").onclick = function () {
                            //document.getElementById("bookingWidget").innerHTML = "<iframe width='225px' frameborder='0' height='451px' src='http://local.britz.com.au/unittests/widget.aspx'></iframe>";                    
                            document.getElementById("frameContainer").style.display = "block";
                        }
                    }
                </script>
                <div id="widgetHead">Book Now</div>
                <div id="frameContainer" style="display:none;">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fu
                </div>
            </div>
        </form>
    </body>
</html>