﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BritzNzHome.aspx.cs" Inherits="UnitTests_UI_BritzNzHome" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" dir="ltr">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
    <meta name="GENERATOR" content="Microsoft SharePoint" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="Expires" content="0" />
    <title>Campervan Hire NZ - Car, Caravan &amp; Motorhome Rental - Britz New Zealand
    </title>
    <link rel="stylesheet" type="text/css" href="http://Staging.Britz.co.nz/_layouts/15/1033/styles/Themable/corev15.css?rev=BdxJNFd%2FTPOed3Z8IKEJ9A%3D%3D" />
    <link rel="stylesheet" type="text/css" href="http://Staging.Britz.co.nz/Style%20Library/en-US/Themable/Core%20Styles/controls15.css" />
    <link rel="stylesheet" type="text/css" href="http://Staging.Britz.co.nz/SiteCollectionDocuments/css/styles.css" />
    <link rel="stylesheet" type="text/css" href="http://Staging.Britz.co.nz/SiteCollectionDocuments/css/homemenu.css" />
    <link rel="stylesheet" type="text/css" href="http://Staging.Britz.co.nz/SiteCollectionDocuments/css/jcal.css" />


    <script type="text/javascript" src="http://Staging.Britz.co.nz/_layouts/15/init.js?rev=7Tix801bl5ZziOKr5K1nrw%3D%3D"></script>
    <script type="text/javascript" src="http://Staging.Britz.co.nz/ScriptResource.axd?d=A8_YcCFY7ryB63EVHzHovTlrNegGuOJG0WnOt_Rk38VNgoxs3FKoImXKedHH9aAjFS-816halAMDEwLKk2J-nEeSTmA46hNBgdiCNwA6ChCE1YZIDJWMrmyWZ4ZlpurqgQtQSnMHOyPiwhutWz9nIcQjb2_WwyJKfWMS_icdb88HqorSDW53g0sehIck0kbb0&amp;t=6119e399"></script>
    <script type="text/javascript" src="http://Staging.Britz.co.nz/_layouts/15/blank.js?rev=ZaOXZEobVwykPO9g8hq%2F8A%3D%3D"></script>
    <script type="text/javascript" src="http://Staging.Britz.co.nz/ScriptResource.axd?d=ae-PyHZ8XNs1CKmbfkZjWNi_O711GwYO7ibQ5kE5Vh-sS1PycgiTFeTYnL9LkFkFl4aLuVzBfEGzVNSSHg4Y7tTHAiz-KM8gjgcmhsxEfbDTC4thxyzfEzlaByI4ZK71s9Kvx0E9okguuNSHUxgkRT3JW8cULyK8gdSE3zUV8-3k4B62wuYo82iKEXWyxK1s0&amp;t=6119e399"></script>
    <script type="text/javascript">RegisterSod("initstrings.js", "\u002f_layouts\u002f15\u002f1033\u002finitstrings.js?rev=uNmvBiHdrBzcPQzXRpm\u00252FnQ\u00253D\u00253D");</script>
    <script type="text/javascript">RegisterSod("strings.js", "\u002f_layouts\u002f15\u002f1033\u002fstrings.js?rev=cSu1pcWiRc999fyCNzJplg\u00253D\u00253D");RegisterSodDep("strings.js", "initstrings.js");</script>
    <script type="text/javascript">RegisterSod("sp.init.js", "\u002f_layouts\u002f15\u002fsp.init.js?rev=QI1yUCfCoUkadL93jNZLOg\u00253D\u00253D");</script>
    <script type="text/javascript">RegisterSod("sp.res.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252ERes\u0026rev=yNk\u00252FhRzgBn40LJVP\u00252BqfgdQ\u00253D\u00253D");</script>
    <script type="text/javascript">RegisterSod("sp.ui.dialog.js", "\u002f_layouts\u002f15\u002fsp.ui.dialog.js?rev=0xf6wCIW4E1pN83I9nSIJQ\u00253D\u00253D");RegisterSodDep("sp.ui.dialog.js", "sp.init.js");RegisterSodDep("sp.ui.dialog.js", "sp.res.resx");</script>
    <script type="text/javascript">RegisterSod("core.js", "\u002f_layouts\u002f15\u002fcore.js?rev=\u00252FmcwmyWAFSbQRHlXU4BIBg\u00253D\u00253D");RegisterSodDep("core.js", "strings.js");</script>
    <script type="text/javascript">RegisterSod("menu.js", "\u002f_layouts\u002f15\u002fmenu.js?rev=pcr83s11QGFA2kLt5rDQ1g\u00253D\u00253D");</script>
    <script type="text/javascript">RegisterSod("mQuery.js", "\u002f_layouts\u002f15\u002fmquery.js?rev=VYAJYBo5H8I3gVSL3MzD6A\u00253D\u00253D");</script>
    <script type="text/javascript">RegisterSod("callout.js", "\u002f_layouts\u002f15\u002fcallout.js?rev=ryx2n4ePkYj1\u00252FALmcsXZfA\u00253D\u00253D");RegisterSodDep("callout.js", "strings.js");RegisterSodDep("callout.js", "mQuery.js");RegisterSodDep("callout.js", "core.js");</script>
    <script type="text/javascript">RegisterSod("clienttemplates.js", "\u002f_layouts\u002f15\u002fclienttemplates.js?rev=I3TuvDSjjPdZ0z\u00252F1VD79TA\u00253D\u00253D");RegisterSodDep("clienttemplates.js", "initstrings.js");</script>
    <script type="text/javascript">RegisterSod("sharing.js", "\u002f_layouts\u002f15\u002fsharing.js?rev=EojJW\u00252FD7vytDfjPyrFWfzw\u00253D\u00253D");RegisterSodDep("sharing.js", "strings.js");RegisterSodDep("sharing.js", "mQuery.js");RegisterSodDep("sharing.js", "clienttemplates.js");RegisterSodDep("sharing.js", "core.js");</script>
    <script type="text/javascript">RegisterSod("suitelinks.js", "\u002f_layouts\u002f15\u002fsuitelinks.js?rev=LW8KECQqnuLhetUeLd3iSg\u00253D\u00253D");RegisterSodDep("suitelinks.js", "strings.js");RegisterSodDep("suitelinks.js", "core.js");</script>
    <script type="text/javascript">RegisterSod("clientrenderer.js", "\u002f_layouts\u002f15\u002fclientrenderer.js?rev=PWwV4FATEiOxN90BeB5Hzw\u00253D\u00253D");</script>
    <script type="text/javascript">RegisterSod("srch.resources.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=Srch\u00252EResources\u0026rev=RtqWXJ0qK1Fldjs7sAaqWg\u00253D\u00253D");</script>
    <script type="text/javascript">RegisterSod("search.clientcontrols.js", "\u002f_layouts\u002f15\u002fsearch.clientcontrols.js?rev=4CdfNbT3fyiqGDFCn59UyA\u00253D\u00253D");RegisterSodDep("search.clientcontrols.js", "sp.init.js");RegisterSodDep("search.clientcontrols.js", "clientrenderer.js");RegisterSodDep("search.clientcontrols.js", "srch.resources.resx");</script>
    <script type="text/javascript">RegisterSod("sp.runtime.js", "\u002f_layouts\u002f15\u002fsp.runtime.js?rev=5f2WkYJoaxlIRdwUeg4WEg\u00253D\u00253D");RegisterSodDep("sp.runtime.js", "sp.res.resx");</script>
    <script type="text/javascript">RegisterSod("sp.js", "\u002f_layouts\u002f15\u002fsp.js?rev=33bA6vu78Vj9BHUu9nwStw\u00253D\u00253D");RegisterSodDep("sp.js", "sp.runtime.js");RegisterSodDep("sp.js", "sp.ui.dialog.js");RegisterSodDep("sp.js", "sp.res.resx");</script>
    <script type="text/javascript">RegisterSod("userprofile", "\u002f_layouts\u002f15\u002fsp.userprofiles.js?rev=CNRoSSzRfPnsUK3d\u00252FoiNMg\u00253D\u00253D");RegisterSodDep("userprofile", "sp.runtime.js");</script>
    <script type="text/javascript">RegisterSod("followingcommon.js", "\u002f_layouts\u002f15\u002ffollowingcommon.js?rev=sObFmln\u00252BGVrczIpXNeFRrg\u00253D\u00253D");RegisterSodDep("followingcommon.js", "strings.js");RegisterSodDep("followingcommon.js", "sp.js");RegisterSodDep("followingcommon.js", "userprofile");RegisterSodDep("followingcommon.js", "core.js");RegisterSodDep("followingcommon.js", "mQuery.js");</script>
    <script type="text/javascript">RegisterSod("profilebrowserscriptres.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=ProfileBrowserScriptRes\u0026rev=PEn82Mp1iMY2FYDo2ApuAQ\u00253D\u00253D");</script>
    <script type="text/javascript">RegisterSod("sp.ui.mysitecommon.js", "\u002f_layouts\u002f15\u002fsp.ui.mysitecommon.js?rev=Ua8qmZSU9nyf53S7PEyJwQ\u00253D\u00253D");RegisterSodDep("sp.ui.mysitecommon.js", "sp.init.js");RegisterSodDep("sp.ui.mysitecommon.js", "sp.runtime.js");RegisterSodDep("sp.ui.mysitecommon.js", "userprofile");RegisterSodDep("sp.ui.mysitecommon.js", "profilebrowserscriptres.resx");</script>
    <script type="text/javascript">RegisterSod("browserScript", "\u002f_layouts\u002f15\u002fnon_ie.js?rev=W2q45TO627Zi6ztdktTOtA\u00253D\u00253D");RegisterSodDep("browserScript", "strings.js");</script>
    <script type="text/javascript">RegisterSod("inplview", "\u002f_layouts\u002f15\u002finplview.js?rev=K5zrfCDSdbrB8fpmpszSPg\u00253D\u00253D");RegisterSodDep("inplview", "strings.js");RegisterSodDep("inplview", "core.js");RegisterSodDep("inplview", "sp.js");</script>
    <script type="text/javascript">RegisterSod("datepicker.js", "\u002f_layouts\u002f15\u002fdatepicker.js?rev=r1fn95g\u00252FHgaje9S\u00252FqZnKYg\u00253D\u00253D");RegisterSodDep("datepicker.js", "strings.js");</script>
    <script type="text/javascript">RegisterSod("sp.publishing.resources.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252EPublishing\u00252EResources\u0026rev=GGzheobKQRsxiF6yBwAfZA\u00253D\u00253D");</script>
    <script type="text/javascript">RegisterSod("mediaplayer.js", "\u002f_layouts\u002f15\u002fmediaplayer.js?rev=YyqTLoWJZuIG93N00x4YTw\u00253D\u00253D");RegisterSodDep("mediaplayer.js", "sp.publishing.resources.resx");</script>
    <script type="text/javascript">RegisterSod("sp.core.js", "\u002f_layouts\u002f15\u002fsp.core.js?rev=b89b6nnwOk\u00252FeDkJa0KhP5w\u00253D\u00253D");RegisterSodDep("sp.core.js", "strings.js");RegisterSodDep("sp.core.js", "sp.init.js");RegisterSodDep("sp.core.js", "core.js");</script>
    <script type="text/javascript">RegisterSod("dragdrop.js", "\u002f_layouts\u002f15\u002fdragdrop.js?rev=rvaZEcJ\u00252F\u00252BHcW7vn3OONdGw\u00253D\u00253D");RegisterSodDep("dragdrop.js", "strings.js");</script>
    <script type="text/javascript">RegisterSod("quicklaunch.js", "\u002f_layouts\u002f15\u002fquicklaunch.js?rev=SV6sY1PxQIso9KC9IL8e2Q\u00253D\u00253D");RegisterSodDep("quicklaunch.js", "strings.js");RegisterSodDep("quicklaunch.js", "dragdrop.js");</script>
    <link type="text/xml" rel="alternate" href="http://Staging.Britz.co.nz/en/_vti_bin/spsdisco.aspx" />


    <link rel="canonical" href="http://staging.britz.co.nz:80/en/Pages/default.aspx" />
    <meta name="keywords" content="campervan hire, motorhome rental, caravan hire, campervans, motorhomes, car hire, britz, new zealand, nz" />
    <meta name="description" content="Britz Campervans NZ offer a wide range of campers, motorhomes, cars &amp; caravans for your adventure. Book with Britz for best campervan hire in New Zealand." />

    <span id="DeltaSPWebPartManager"></span>
    <link rel="shortcut icon" href="/SiteCollectionImages/Core/favicon.ico" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js">//<![CDATA[        //]]></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js">//<![CDATA[//]]></script>
    <!-- Google Map Data -->
    <script src="http://maps.google.com/maps/api/js?v=3.5&amp;sensor=false" type="text/javascript">//<![CDATA[//]]></script>
    <script src="/SiteCollectionDocuments/js/branchlocationsNZ.js" type="text/javascript">//<![CDATA[ //]]></script>
    <script src="/SiteCollectionDocuments/js/jquery.customSelect.min.js">//<![CDATA[//]]></script>
    <script src="/SiteCollectionDocuments/js/britzIni.js">//<![CDATA[ //]]></script>
    <!-- Booking Functionality Starts -->

    <script type="text/javascript" src="/SiteCollectionDocuments/js/booking.js?brand=b&amp;v=1.0">//<![CDATA[//]]></script>
    <!-- Booking Functionality Ends -->
    <!-- BoldChat VisitInfo generator -->
    <script type="text/javascript">//<![CDATA[        
        function getRefID() {
            var chars = "0123456789";
            var string_length = 6;
            var rstring = '';
            for (var i=0; i<string_length; i++) {
                var rnum = Math.floor(Math.random() * chars.length);
                rstring += chars.substring(rnum,rnum+1);
            }
            return rstring;
        } 
        var visitInfoVal = "CHAT" + getRefID();
        
        
        //]]></script>
    <!-- BoldChat VisitInfo generator end -->
    <!-- Google Analytics Asychronous Code-->
    <script type="text/javascript">//<![CDATA[
		
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-2807184-1']);
        _gaq.push(['_setDomainName', '.britz.co.nz']);
        _gaq.push(['_trackPageview']); 
		  
        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
		
		
        
        
        //]]></script>
    <!--Analytics end-->
    <!--Universal Analytics-->
    <script>//<![CDATA[
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
        ga('create', 'UA-2807184-3', 'britz.co.nz');
        ga('send', 'pageview');
		
		
        //]]></script>
    <!--end Analytics-->


</head>
<body onhashchange="if (typeof(_spBodyOnHashChange) != 'undefined') _spBodyOnHashChange();">
    <div id="imgPrefetch" style="display: none">
        <img src="http://Staging.Britz.co.nz/_layouts/15/images/spcommon.png?rev=23" />
    </div>
    <form method="post" action="default.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
        <div class="aspNetHidden">
            <input type="hidden" name="_wpcmWpid" id="_wpcmWpid" value="" />
            <input type="hidden" name="wpcmVal" id="wpcmVal" value="" />
            <input type="hidden" name="MSOWebPartPage_PostbackSource" id="MSOWebPartPage_PostbackSource" value="" />
            <input type="hidden" name="MSOTlPn_SelectedWpId" id="MSOTlPn_SelectedWpId" value="" />
            <input type="hidden" name="MSOTlPn_View" id="MSOTlPn_View" value="0" />
            <input type="hidden" name="MSOTlPn_ShowSettings" id="MSOTlPn_ShowSettings" value="False" />
            <input type="hidden" name="MSOGallery_SelectedLibrary" id="MSOGallery_SelectedLibrary" value="" />
            <input type="hidden" name="MSOGallery_FilterString" id="MSOGallery_FilterString" value="" />
            <input type="hidden" name="MSOTlPn_Button" id="MSOTlPn_Button" value="none" />
            <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
            <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
            <input type="hidden" name="__REQUESTDIGEST" id="__REQUESTDIGEST" value="InvalidFormDigest" />
            <input type="hidden" name="MSOSPWebPartManager_DisplayModeName" id="MSOSPWebPartManager_DisplayModeName" value="Browse" />
            <input type="hidden" name="MSOSPWebPartManager_ExitingDesignMode" id="MSOSPWebPartManager_ExitingDesignMode" value="false" />
            <input type="hidden" name="MSOWebPartPage_Shared" id="MSOWebPartPage_Shared" value="" />
            <input type="hidden" name="MSOLayout_LayoutChanges" id="MSOLayout_LayoutChanges" value="" />
            <input type="hidden" name="MSOLayout_InDesignMode" id="MSOLayout_InDesignMode" value="" />
            <input type="hidden" name="_wpSelected" id="_wpSelected" value="" />
            <input type="hidden" name="_wzSelected" id="_wzSelected" value="" />
            <input type="hidden" name="MSOSPWebPartManager_OldDisplayModeName" id="MSOSPWebPartManager_OldDisplayModeName" value="Browse" />
            <input type="hidden" name="MSOSPWebPartManager_StartWebPartEditingName" id="MSOSPWebPartManager_StartWebPartEditingName" value="false" />
            <input type="hidden" name="MSOSPWebPartManager_EndWebPartEditing" id="MSOSPWebPartManager_EndWebPartEditing" value="false" />
            <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUBMA9kFgJmD2QWAgIBD2QWBAIBD2QWCAIFD2QWAmYPZBYCAgMPFgIeE1ByZXZpb3VzQ29udHJvbE1vZGULKYgBTWljcm9zb2Z0LlNoYXJlUG9pbnQuV2ViQ29udHJvbHMuU1BDb250cm9sTW9kZSwgTWljcm9zb2Z0LlNoYXJlUG9pbnQsIFZlcnNpb249MTUuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49NzFlOWJjZTExMWU5NDI5YwFkAhEPZBYCAgMPZBYCZg9kFgJmDzwrAAYAZAITD2QWAgIBD2QWBAUmZ180YTkyZjZlYV83NDk1XzQ2NzZfOTYzNl8wNzRmNWIyN2U4ZDcPZBYEZg8WAh4HVmlzaWJsZWhkAgEPFgIfAWhkBSZnX2EzMmVmNzk2X2NhNDZfNGEwYl9hMGFkXzUzYmE0Mzk2ZWU0Yg9kFgRmDxYCHwFoZAIBDxYCHwFoZAIUDw8WAh8BaGRkAgUPZBYGAgQPZBYCAgIPZBYCAgUPZBYCAgMPFgIfAWgWAmYPZBYEAgIPZBYGAgEPFgIfAWhkAgMPFgIfAWhkAgUPFgIfAWhkAgMPDxYCHglBY2Nlc3NLZXkFAS9kZAIID2QWAgIBD2QWAgINDw8WAh8BaGQWAgIDD2QWAgIDD2QWAgIBDzwrAAkBAA8WAh4NTmV2ZXJFeHBhbmRlZGdkZAIKD2QWAgICD2QWCAIBDxYCHwALKwQBZAIFDxYCHwALKwQBZAIJD2QWAgIBDxYCHwALKwQBZAILD2QWBgIBDxYCHwALKwQBZAIFDxYCHwALKwQBZAIJDxYCHwALKwQBZBgBBS1jdGwwMCRQbGFjZUhvbGRlckxlZnROYXZCYXIkVjRRdWlja0xhdW5jaE1lbnUPD2QFB0VuZ2xpc2hkBkYm7VzOpi4cB781kcW9YGf0/7Gc1//8knOOV2peWos=" />
        </div>

        <script type="text/javascript">
            //<![CDATA[
            var theForm = document.forms['aspnetForm'];
            if (!theForm) {
                theForm = document.aspnetForm;
            }
            function __doPostBack(eventTarget, eventArgument) {
                if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
                    theForm.__EVENTTARGET.value = eventTarget;
                    theForm.__EVENTARGUMENT.value = eventArgument;
                    theForm.submit();
                }
            }
            //]]>
        </script>


        <script src="http://Staging.Britz.co.nz/WebResource.axd?d=Pu8jTP38iy4gFOGEy5p6RBgG5AvjITwsxmdLRS_Lk3OE8C9IHgMQEdQPOJDhcseaB-igrZxX-TMPK4JaF4TV5X4u4_TmpW89PssaWMlC1aA1&amp;t=634773866700000000" type="text/javascript"></script>


        <script type="text/javascript">
            //<![CDATA[
            var MSOWebPartPageFormName = 'aspnetForm';
            var g_presenceEnabled = true;
            var g_wsaEnabled = false;
            var g_wsaQoSEnabled = false;
            var g_wsaQoSDataPoints = [];
            var g_wsaLCID = 1033;
            var g_wsaListTemplateId = 850;
            var g_wsaSiteTemplateId = 'CMSPUBLISHING#0';
            var _fV4UI=true;var _spPageContextInfo = {webServerRelativeUrl: "\u002fen", webAbsoluteUrl: "http:\u002f\u002fstaging.britz.co.nz\u002fen", siteAbsoluteUrl: "http:\u002f\u002fstaging.britz.co.nz", serverRequestPath: "\u002fen\u002fPages\u002fdefault.aspx", layoutsUrl: "_layouts\u002f15", webTitle: "English", webTemplate: "39", tenantAppVersion: "0", webLogoUrl: "\u002fSiteCollectionImages\u002fCore\u002flogo.png", webLanguage: 1033, currentLanguage: 1033, currentUICultureName: "en-US", currentCultureName: "en-NZ", clientServerTimeDelta: new Date("2014-10-29T20:10:09.3691460Z") - new Date(), siteClientTag: "3799$$15.0.4481.1005", crossDomainPhotosEnabled:false, webUIVersion:15, webPermMasks:{High:16,Low:196673},pageListId:"{3f28119c-12fd-42bf-9eec-5d32745d64b8}",pageItemId:3, pagePersonalizationScope:1, alertsEnabled:false, siteServerRelativeUrl: "\u002f", allowSilverlightPrompt:'True'};
            function DoCallBack(filterText)
            {WebForm_DoCallback('ctl00$ctl34$g_465c6ba5_705a_468c_bc5b_da6ab503f35e',filterText,UpdateFilterCallback,0,CallBackError,true)
            }
            function CallBackError(result, clientsideString)
            {                
            }
            document.onreadystatechange=fnRemoveAllStatus; function fnRemoveAllStatus(){removeAllStatus(true)};
            function _spNavigateHierarchy(nodeDiv, dataSourceId, dataPath, url, listInContext, type) {

                CoreInvoke('ProcessDefaultNavigateHierarchy', nodeDiv, dataSourceId, dataPath, url, listInContext, type, document.forms.aspnetForm, "", "\u002fen\u002fPages\u002fdefault.aspx");

            }
            var _spWebPartComponents = new Object();//]]>
        </script>

        <script src="http://Staging.Britz.co.nz/_layouts/15/blank.js?rev=ZaOXZEobVwykPO9g8hq%2F8A%3D%3D" type="text/javascript"></script>
        <script type="text/javascript">
            //<![CDATA[
            (function(){

                if (typeof(_spBodyOnLoadFunctions) === 'undefined' || _spBodyOnLoadFunctions === null) {
                    return;
                }
                _spBodyOnLoadFunctions.push(function() {

                    if (typeof(SPClientTemplates) === 'undefined' || SPClientTemplates === null || (typeof(APD_InAssetPicker) === 'function' && APD_InAssetPicker())) {
                        return;
                    }

                    var renderFollowFooter = function(renderCtx,  calloutActionMenu)
                    {
                        if (renderCtx.ListTemplateType == 700) 
                            myDocsActionsMenuPopulator(renderCtx, calloutActionMenu);
                        else
                            CalloutOnPostRenderTemplate(renderCtx, calloutActionMenu);

                        var listItem = renderCtx.CurrentItem;
                        if (typeof(listItem) === 'undefined' || listItem === null) {
                            return;
                        }
                        if (listItem.FSObjType == 0) {
                            calloutActionMenu.addAction(new CalloutAction({
                                text: Strings.STS.L_CalloutFollowAction,
                                tooltip: Strings.STS.L_CalloutFollowAction_Tooltip,
                                onClickCallback: function (calloutActionClickEvent, calloutAction) {
                                    var callout = GetCalloutFromRenderCtx(renderCtx);
                                    if (!(typeof(callout) === 'undefined' || callout === null))
                                        callout.close();
                                    SP.SOD.executeFunc('followingcommon.js', 'FollowSelectedDocument', function() { FollowSelectedDocument(renderCtx); });
                                }
                            }));
                        }
                    };

                    var registerOverride = function(id) {
                        var followingOverridePostRenderCtx = {};
                        followingOverridePostRenderCtx.BaseViewID = 'Callout';
                        followingOverridePostRenderCtx.ListTemplateType = id;
                        followingOverridePostRenderCtx.Templates = {};
                        followingOverridePostRenderCtx.Templates.Footer = function(renderCtx) {
                            var  renderECB;
                            if (typeof(isSharedWithMeView) === 'undefined' || isSharedWithMeView === null) {
                                renderECB = true;
                            } else {
                                var viewCtx = getViewCtxFromCalloutCtx(renderCtx);
                                renderECB = !isSharedWithMeView(viewCtx);
                            }
                            return CalloutRenderFooterTemplate(renderCtx, renderFollowFooter, renderECB);
                        };
                        SPClientTemplates.TemplateManager.RegisterTemplateOverrides(followingOverridePostRenderCtx);
                    }
                    registerOverride(101);
                    registerOverride(700);
                });
            })();(function(){

                if (typeof(_spBodyOnLoadFunctions) === 'undefined' || _spBodyOnLoadFunctions === null) {
                    return;
                }
                _spBodyOnLoadFunctions.push(function() 
                {
                    ExecuteOrDelayUntilScriptLoaded(
                      function()
                      {
                          var pairs = SP.ScriptHelpers.getDocumentQueryPairs();
                          var followDoc, itemId, listId, docName;
                          for (var key in pairs)
                          {
                              if(key.toLowerCase() == 'followdocument') 
                                  followDoc = pairs[key];
                              else if(key.toLowerCase() == 'itemid') 
                                  itemId = pairs[key];
                              else if(key.toLowerCase() == 'listid') 
                                  listId = pairs[key];
                              else if(key.toLowerCase() == 'docname') 
                                  docName = decodeURI(pairs[key]);
                          } 

                          if(followDoc != null && followDoc == '1' && listId!=null && itemId != null && docName != null)
                          {
                              SP.SOD.executeFunc('followingcommon.js', 'FollowDocumentFromEmail', function() 
                              { 
                                  FollowDocumentFromEmail(itemId, listId, docName);
                              });
                          }

                      }, 'SP.init.js');

                });
            })();if (typeof(DeferWebFormInitCallback) == 'function') DeferWebFormInitCallback();function WebForm_OnSubmit() {
                UpdateFormDigest('\u002fen', 1440000);if (typeof(_spFormOnSubmitWrapper) != 'undefined') {return _spFormOnSubmitWrapper();} else {return true;};
                return true;
            }
            //]]>
        </script>
        <script type="text/javascript">
            //<![CDATA[
            Sys.WebForms.PageRequestManager._initialize('ctl00$ScriptManager', 'aspnetForm', [], [], [], 90, 'ctl00');
            //]]>
        </script>

        <!-- BEGIN: Marin Software -->
        <script type="text/javascript">//<![CDATA[
            var _mTrack = _mTrack || [];
            _mTrack.push(['trackPage']);
            (function() {
                var mClientId = '1624rrj16003';
                var mProto = ('https:' == document.location.protocol ? 'https://' : 'http://');
                var mHost = 'tracker.marinsm.com';
                var mt = document.createElement('script'); mt.type = 'text/javascript'; mt.async =
				true; mt.src = mProto + mHost + '/tracker/async/' + mClientId + '.js';
                var fscr = document.getElementsByTagName('script')[0];
                fscr.parentNode.insertBefore(mt, fscr);
            })();
				
        
        
            //]]></script>
        <noscript>
            <img src="https://tracker.marinsm.com/tp?act=1&amp;cid=1624rrj16003&amp;script=no" />
        </noscript>
        <!-- END: Copyright Marin Software -->



        <div>

            <div id="TurnOnAccessibility" style="display: none" class="s4-notdlg noindex">
                <a id="linkTurnOnAcc" href="#" class="ms-accessible ms-acc-button" onclick="SetIsAccessibilityFeatureEnabled(true);UpdateAccessibilityUI();document.getElementById('linkTurnOffAcc').focus();return false;">Turn on more accessible mode
                </a>
            </div>
            <div id="TurnOffAccessibility" style="display: none" class="s4-notdlg noindex">
                <a id="linkTurnOffAcc" href="#" class="ms-accessible ms-acc-button" onclick="SetIsAccessibilityFeatureEnabled(false);UpdateAccessibilityUI();document.getElementById('linkTurnOnAcc').focus();return false;">Turn off more accessible mode
                </a>
            </div>

        </div>
        <div id="ms-designer-ribbon">
            <div>

                <div id="s4-ribbonrow" style="visibility: hidden; display: none"></div>

            </div>



        </div>

        <div id="s4-workspace">
            <div id="s4-bodyContainer">
                <!-- ############### CROSS SELL STARTS ###############-->
                <div class="crossSell">
                    <ul id="crossSellList">
                        <li class="left">
                            <span>RENTALS New Zealand
                            </span>
                            <i class="icon-crossSell-nodrop"></i>
                            <ul class="crossSellBrands">
                                <li>
                                    <a href="http://nzrentals.keacampers.com/?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLink" class="kea">KEA
                                    </a>
                                </li>
                                <li>
                                    <a href="http://www.maui.co.nz/?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLink" class="maui">Maui New Zealand
                                    </a>
                                </li>
                                <li>
                                    <a href="http://www.unitedcampervans.co.nz/?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLink" class="united">United
                                    </a>
                                </li>
                                <li>
                                    <a href="http://staging.britz.co.nz/" class="britz current">Britz Campervan. 4WD. Car Rentals.
                                    </a>
                                </li>
                                <li>
                                    <a href="http://www.alphacampervans.co.nz/?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLink" class="alpha">Alpha
                                    </a>
                                </li>
                                <li>
                                    <a href="http://www.mightycampers.co.nz/?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLink" class="mighty">Mighty Cars &amp; Campers
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="brands right">
                            <span>BUY a Campervan
                            
                            
                            
                            </span>
                            <i class="icon-crossSell"></i>
                            <ul class="crossSellBrands down">
                                <li>
                                    <a href="http://www.motekvehicles.com/?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLink" class="motek">Motek Vehicle Sales AU/NZ
                                    
                                    
                                    
                                    </a>
                                </li>
                                <li>
                                    <a href="http://sales.keacampers.com/nz?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLink" class="kea">KEA Motorhome Sales
                                    
                                    
                                    
                                    </a>
                                </li>
                                <li>
                                    <a href="http://www.unitedsalescentre.co.nz?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLink" class="united">United Motorhome Sales
                                    
                                    
                                    
                                    </a>
                                </li>
                                <li class="close">close 
                                    
                                    
                                    
                                    <i class="icon-remove-circle"></i>
                                </li>
                            </ul>
                        </li>
                        <li class="brands right">
                            <span>RENTALS USA
                            
                            
                            
                            </span>
                            <i class="icon-crossSell"></i>
                            <ul class="crossSellBrands down">
                                <li>
                                    <a href="http://www.roadbearrv.com/?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLink" class="roadbear">Roadbear RV Rentals &amp; Sales
                                    
                                    
                                    
                                    </a>
                                </li>
                                <li>
                                    <a href="http://www.britz-usa.com/?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLink" class="britz">Britz Campervan. 4WD. Car Rentals.
                                    
                                    
                                    
                                    </a>
                                </li>
                                <li class="close">close 
                                    
                                    
                                    
                                    <i class="icon-remove-circle"></i>
                                </li>
                            </ul>
                        </li>
                        <li class="brands right">
                            <span>RENTALS Australia
                            
                            
                            
                            </span>
                            <i class="icon-crossSell"></i>
                            <ul class="crossSellBrands down">
                                <li>
                                    <a href="http://aurentals.keacampers.com/?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLink" class="kea">KEA
                                    
                                    
                                    
                                    </a>
                                </li>
                                <li>
                                    <a href="http://www.maui.com.au/?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLink" class="maui">Maui Australia
                                    
                                    
                                    
                                    </a>
                                </li>
                                <li>
                                    <a href="http://www.britz.com.au/?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLink" class="britz">Britz Campervan. 4WD. Car Rentals.
                                    
                                    
                                    
                                    </a>
                                </li>
                                <li>
                                    <a href="http://www.mightycampers.com.au/?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLink" class="mighty">Mighty Cars &amp; Campers
                                    
                                    
                                    
                                    </a>
                                </li>
                                <li class="close">close 
                                    
                                    
                                    
                                    <i class="icon-remove-circle"></i>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- ############### CROSS SELL ENDS ###############-->
                <!-- ############### CONTACT BAR STARTS ###############-->
                <div class="contactBar">
                    <div class="contactBarCentre">
                        <div class="contactBarCountry">
                            New Zealand
                        
                        
                        
                        </div>
                        <div class="contactBarContainer">
                            <div class="contactBarLink contactBarLinkLast" id="Flags">
                                <img src="http://Staging.Britz.co.nz/SiteCollectionImages/Core/lang.png" alt="English" />
                                <div class="changeLang">
                                    <a href="/" class="lang English" title="Britz Campervan. 4WD. Car Rentals - English">
                                        <img src="http://Staging.Britz.co.nz/SiteCollectionImages/Core/English.jpg" alt="Britz Campervan. 4WD. Car Rentals" />
                                    </a>
                                    <a href="http://Staging.Britz.co.nz/fr/Pages/default.aspx" title="Locations Britz: camping-cars, voitures à 2 et 4 roues motrices">
                                        <img src="/SiteCollectionImages/Core/French.jpg" alt="Locations Britz: camping-cars, voitures à 2 et 4 roues motrices" />
                                    </a>
                                    <a href="http://Staging.Britz.co.nz/GermanOld/Pages/default.aspx" title="Wohnmobile, Allradwagen &amp; PKWs zur Miete bei Britz">
                                        <img src="http://Staging.Britz.co.nz/SiteCollectionImages/Core/German.jpg" alt="Wohnmobile, Allradwagen &amp; PKWs zur Miete bei Britz" />
                                    </a>
                                    <a href="http://Staging.Britz.co.nz/fr/Pages/default.aspx" title="Britz verhuur van campers, 4WD en auto's">
                                        <img src="http://Staging.Britz.co.nz/SiteCollectionImages/Core/dutch.jpg" alt="Britz verhuur van campers, 4WD en auto's" />
                                    </a>
                                    <a href="http://Staging.Britz.co.nz/chinaOld/Pages/default.aspx" title="新西兰布里茨房车露营车租赁 房车租车">
                                        <img src="http://Staging.Britz.co.nz/SiteCollectionImages/Core/Chinese.jpg" alt="新西兰布里茨房车露营车租赁 房车租车" />
                                    </a>
                                </div>
                            </div>
                            <a href="https://www.facebook.com/britzfanpage" class="contactBarLink">
                                <img src="/SiteCollectionImages/Core/contactBar-FB.png" alt="Facebook" />
                            </a>
                            <a href="https://twitter.com/BritzCampervans" class="contactBarLink">
                                <img src="/SiteCollectionImages/Core/contactBar-Twitter.png" alt="Twitter" />
                            </a>
                            <a href="http://Staging.Britz.co.nz/en/contactus/Pages/default.aspx" class="contactBarTitle">
                                <strong>Contact us
                                
                                </strong>


                                - International Toll Free                                                                                                                                                                                                                                                                                                                                                                                                                                    </a>
                        </div>
                    </div>
                </div>
                <!-- ############### CONTACT BAR ENDS ###############-->
                <div class="container">
                    <!-- ############### HEADER STARTS ###############-->
                    <div class="header">
                        <div class="logo">
                            <a href="/">
                                <img src="http://Staging.Britz.co.nz/SiteCollectionImages/Core/logo.png" alt="Britz Campervan Hire New Zealand" />
                            </a>
                        </div>
                    </div>
                    <!-- ############### HEADER ENDS ###############-->
                    <!-- ############### BOOKING CONTROL STARTS ###############-->
                    <script type="text/javascript">//<![CDATA[
                        jQuery(document).ready(function () { initWidget({ 'brand': 'b', 'cc': 'nz', 'vt': 'campervan', 'target': 'bookingcontrol', 'engine': 'https://secure.britz.co.nz/Selection.aspx' }); });                        
                    
                    
                    
                    
                    
                    
                    
                    
                        //]]></script>
                    <div class="bookingControl clearfix collapsed" id="bookingcontrol">
                        <div class="bookingControlInner">
                            <div id="bcPin">
                                <img src="http://Staging.Britz.co.nz/SiteCollectionImages/Core/ping.png" width="25" height="34" alt="Pin" />
                            </div>
                            <!-- Tabs -->
                            <div id="bookingtabs">
                                <div class="searchCampervans TabSelected vehicleTab" data-vtype="campervan">
                                    SEARCH CAMPERVANS
                                
                                
                                
                                
                                
                                
                                
                                </div>
                                <div class="searchCars vehicleTab" data-vtype="car">
                                    SEARCH CARS
                                
                                
                                
                                
                                
                                
                                
                                </div>
                                <div class="viewprice">
                                    view price &amp; availability
                                
                                
                                
                                
                                
                                
                                
                                </div>
                                <a href="https://selfcheckin.thlonline.com/Pages/BookingSearch.aspx" class="checkinOnline">CHECK-IN ONLINE 
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    <i class="icon-checkin"></i>
                                </a>
                            </div>
                            <!-- form -->
                            <div class="bookingControlForm">
                                <!--<div class="openform"></div>-->
                                <div class="content" style="height: 400px">
                                    <div class="formfields" style="height: 400px">
                                        <div class="row1">
                                            <select class="styled combo" id="pickUpLoc" data-b2c="pb">
                                                <option value="">Where would you like to pick up from?</option>
                                                <option value="akl">Auckland Airport</option>
                                                <option value="chc">Christchurch Airport</option>
                                                <option value="qns">Queenstown Airport</option>
                                                    
                                            </select>
                                            <span class="calander">
                                                <span class="calanderInner">
                                                    <input class="calElm" type="text" readonly="readonly" id="pickUpDate" value="31-10-2014" />
                                                </span>
                                            </span>
                                            <select class="time styled2" id="pickUpTime" data-b2c="pt">
                                                <option value="08:00">08:00am</option>
                                                <option value="08:30">08:30am</option>
                                                <option value="09:00">09:00am</option>
                                                <option value="09:30">09:30am</option>
                                                <option value="10:00" selected="selected">10:00am</option>
                                                <option value="10:30">10:30am</option>
                                                <option value="11:00">11:00am</option>
                                                <option value="11:30">11:30am</option>
                                                <option value="12:00">12:00pm</option>
                                                <option value="12:30">12:30pm</option>
                                                <option value="13:00">13:00pm</option>
                                                <option value="13:30">13:30pm</option>
                                                <option value="14:00">14:00pm</option>
                                                <option value="14:30">14:30pm</option>
                                                <option value="15:00">15:00pm</option>
                                                <option value="15:30">15:30pm</option>
                                                <option value="16:00">16:00pm</option>
                                                <option value="16:30">16:30pm</option>
                                                <option value="17:00">17:00pm</option>
                                            </select>
                                        </div>
                                        <%--<div class="row2">--%>
                                            <select class="styled combo" id="dropOffLoc" data-b2c="db">
                                                <option value="">Drop Off - Same as pick up</option>
                                                <option value="akl">Auckland Airport</option>
                                                <option value="chc">Christchurch Airport</option>
                                                <option value="qns">Queenstown Airport</option>
                                            </select>
                                            <span class="calander">
                                                <span class="calanderInner">
                                                    <input class="calElm" type="text" readonly="readonly" id="dropOffDate" value="15-11-2014" />
                                                </span>
                                            </span>
                                            <select class="time styled2" id="dropOffTime" data-b2c="dt">
                                                <option value="08:00">08:00am</option>
                                                <option value="08:30">08:30am</option>
                                                <option value="09:00">09:00am</option>
                                                <option value="09:30">09:30am</option>
                                                <option value="10:00">10:00am</option>
                                                <option value="10:30">10:30am</option>
                                                <option value="11:00">11:00am</option>
                                                <option value="11:30">11:30am</option>
                                                <option value="12:00">12:00pm</option>
                                                <option value="12:30">12:30pm</option>
                                                <option value="13:00">13:00pm</option>
                                                <option value="13:30">13:30pm</option>
                                                <option value="14:00">14:00pm</option>
                                                <option value="14:30">14:30pm</option>
                                                <option value="15:00" selected="selected">15:00pm</option>
                                                <option value="15:30">15:30pm</option>
                                                <option value="16:00">16:00pm</option>
                                                <option value="16:30">16:30pm</option>
                                                <option value="17:00">17:00pm</option>
                                            </select>
                                        <%--</div>--%>
                                       <%-- <div class="row3">--%>
                                            <select id="vehicleModel" class="styled3" data-b2c="vh">
                                                <option value="">All models or select
                                                </option>
                                            </select>
                                            <select class="styled4" id="countrOfRes" data-b2c="cr">
                                                <option value="">Drivers Licence</option>
                                                <option value="au">Australia</option>
                                                <option value="ca">Canada</option>
                                                <option value="cn">China</option>
                                                <option value="dk">Denmark</option>
                                                <option value="fr">France</option>
                                                <option value="de">Germany</option>
                                                <option value="nl">Netherlands</option>
                                                <option value="nz">New Zealand</option>
                                                <option value="se">Sweden</option>
                                                <option value="ch">Switzerland</option>
                                                <option value="uk">United Kingdom</option>
                                                <option value="us">United States Of America</option>
                                                <option value="-" disabled="disabled">--------------------</option>
                                                <option value="af">Afghanistan</option>
                                                <option value="al">Albania</option>
                                                <option value="dz">Algeria</option>
                                                <option value="as">American Samoa</option>
                                                <option value="ad">Andora</option>
                                                <option value="ao">Angola</option>
                                                <option value="ai">Anguilla</option>
                                                <option value="aq">Antarctica</option>
                                                <option value="ag">Antigua &amp; Barbados</option>
                                                <option value="ar">Argentina</option>
                                                <option value="am">Armenia</option>
                                                <option value="aw">Aruba</option>
                                                <option value="at">Austria</option>
                                            </select>
                                            <select class="styled5 pax" id="numberAdults" data-b2c="na">
                                                <option value="">Number of Adults</option>
                                                <option value="1">1 Adult</option>
                                                <option value="2">2 Adults</option>
                                                <option value="3">3 Adults</option>
                                                <option value="4">4 Adults</option>
                                                <option value="5">5 Adults</option>
                                                <option value="6">6 Adults</option>
                                            </select>
                                            <select class="styled2 children pax" id="numberChildren" data-b2c="nc">
                                                <option value="">and Children?</option>
                                                <option value="1">1 Child</option>
                                                <option value="2">2 Children</option>
                                                <option value="3">3 Children</option>
                                                <option value="4">4 Children</option>
                                            </select>
                                        </div>
                                    <%--</div>--%>
                                    <div class="StartSearch">
                                        <div class="promocode">
                                    		Promo Code:<br />
                                    		<input type="text" style="float:left; width:100%; padding:5px; border:none; margin-top:10px;" id="promoCodeTxt" />
                                    	</div>

                                        <span class="line"></span>
                                        <span class="StartExpandClose">
                                            <span class="StartSearchBtn">
                                                <span class="lbl">Start Your Search
                                                
                                                </span>
                                                <span class="spinner"></span>
                                                <span class="requiredBox">
                                                    <em>Required Fields:
                                                    
                                                    
                                                    </em>
                                                    <span class="requiredBoxExample"></span>
                                                </span>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                                <!-- texture bgs -->
                                <span class="topleft"></span>
                                <span class="topright"></span>
                                <span class="bottomleft"></span>
                                <span class="bottomright"></span>
                            </div>
                            <span class="contentbottom"></span>
                            <div class="ExpandClose">
                                <span>Expand
                                </span>
                                <i class="icon-expandclose"></i>
                            </div>
                        </div>
                    </div>
                    <!-- ############### BOOKING CONTROL ENDS ###############-->
                    <!-- ############### HOT DEALS STARTS ###############-->
                    <div class="hotDeals clearfix">
                        <div id="AdspaceHomepage">



                            <div class="ms-webpart-chrome ms-webpart-chrome-fullWidth ">
                                <div webpartid="00000000-0000-0000-0000-000000000000" haspers="true" id="WebPartWPQ1" width="100%" class="ms-WPBody " onlyformepart="true" allowdelete="false" style="">
                                    <div class="britzslider boom carousel">
                                        <ul class="slides">
                                            <li><a href="http://staging.britz.co.nz/en/campervan-hire-deals/Pages/south-island-special.aspx" class="adBox"><span class="text"><span class="adTitle">South Island Special</span><span class="bookbefore">Book before: 15 Dec 2013</span><span class="travel">Travel: 13 Jan - 31 Mar 2014</span><span class="pricepoint"><span class="small">up<br>
                                                to</span>35% off</span><span class="findoutmore">Find out more </span></span><span class="image">
                                                    <img src="/SiteCollectionImages/promotions/south-island-35off.jpg?renditionid=11" /></span></a></li>
                                            <li><a href="http://staging.britz.co.nz/en/campervan-hire-deals/Pages/up-to-60-percent-off.aspx" class="adBox"><span class="text"><span class="adTitle">Christchurch to Auckland</span><span class="bookbefore">Book Before: 10 Dec 2013</span><span class="travel">Travel Before: 15 Dec 2013</span><span class="pricepoint"><span class="small">up<br>
                                                to</span>60% OFF</span><span class="findoutmore">Find out more </span></span><span class="image">
                                                    <img src="/SiteCollectionImages/promotions/upto60.jpg?renditionid=11" /></span></a></li>
                                            <li><a href="http://staging.britz.co.nz/en/campervan-hire-deals/Pages/south-to-north.aspx" class="adBox"><span class="text"><span class="adTitle">South to North</span><span class="bookbefore">Book Before: 10 Dec 2013</span><span class="travel">Travel Before: 15 Dec 2013</span><span class="pricepoint"><span class="small">No one<br>
                                                way fee</span>Save $300</span><span class="findoutmore">Find out more </span></span><span class="image">
                                                    <img src="/SiteCollectionImages/promotions/no-one-way-fee.jpg?renditionid=11" /></span></a></li>
                                            <li><a href="http://staging.britz.co.nz/en/campervan-hire-deals/Pages/early-bird-deal.aspx" class="adBox"><span class="text"><span class="adTitle">Early Bird Special</span><span class="bookbefore">Book 120 days prior to collection</span><span class="travel">& on your daily vehicle hire</span><span class="pricepoint">Save 5%</span><span class="findoutmore">Find out more </span></span><span class="image">
                                                <img src="/SiteCollectionImages/promotions/earlybird.jpg?renditionid=11" /></span></a></li>
                                            <li><a href="http://staging.britz.co.nz/en/campervan-hire-deals/Pages/oxfam-trailwalker-2014.aspx" class="adBox"><span class="text"><span class="adTitle">Oxfam Trailwalker</span><span class="bookbefore">Book Before: 02 April 2014</span><span class="travel">Travel Before: 07 April 2014</span><span class="pricepoint">20% OFF</span><span class="findoutmore">Find out more </span></span><span class="image">
                                                <img src="/SiteCollectionImages/promotions/oxfam-trailwalker.jpg?renditionid=11" /></span></a></li>
                                            <li><a href="http://staging.britz.co.nz/en/campervans-nz/Pages/bike-hire.aspx" class="adBox"><span class="text"><span class="adTitle">Hire a Britz Bike</span><span class="bookbefore">Explore New Zealand </span><span class="travel">Cycle Trails</span><span class="pricepoint">ONLY $13<span class="small">per<br>
                                                day</span></span><span class="findoutmore">Find out more </span></span><span class="image">
                                                    <img src="/SiteCollectionImages/generic/bikes.jpg?renditionid=11" /></span></a></li>
                                        </ul>
                                    </div>
                                    <div class="ms-clear"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- ############### HOT DEALS ENDS ###############-->
                    <!-- ############### MAIN CONTENT STARTS ###############-->
                    <div class="mainContent clearfix">
                        <div class="mainContentMenu">
                            <div data-name="QuickLaunch">



                                <div id="sideNavBox" class="ms-dialogHidden ms-forceWrap ms-noList">
                                    <div id="DeltaPlaceHolderLeftNavBar" class="ms-core-navigation">












                                        <div id="ctl00_PlaceHolderLeftNavBar_QuickLaunchNavigationManager">


                                            <div id="zz1_V4QuickLaunchMenu" class=" noindex ms-core-listMenu-verticalBox">
                                                <ul id="zz2_RootAspMenu" class="root ms-core-listMenu-root static">
                                                    <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/zh-cn/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Chinese (PRC)</span></span></a></li>
                                                    <li class="static selected"><a class="static selected menu-item ms-core-listMenu-item ms-displayInline ms-core-listMenu-selected ms-navedit-linkNode" tabindex="0" href="/en/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">English</span><span class="ms-hidden">Currently selected</span></span></a><ul class="static">
                                                        <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en/campervan-hire-locations/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Branch Locations</span></span></a></li>
                                                        <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en/campervans-nz/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Campervans</span></span></a></li>
                                                        <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en/contactus/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Contact Us</span></span></a></li>
                                                        <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en/cycling-in-nz/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Cycling in New Zealand</span></span></a></li>
                                                        <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en/getaquote/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Get A Quote</span></span></a></li>
                                                        <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en/car-hire-nz/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Hire Cars</span></span></a></li>
                                                        <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en/campervan-hire-deals/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Hot Offers</span></span></a></li>
                                                        <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en/nz-travel-itinerary/Pages/Default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">New Zealand Driving Routes</span></span></a></li>
                                                        <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en/popups/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Popups</span></span></a></li>
                                                        <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en/travel-guide-nz/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Travel Guide NZ</span></span></a></li>
                                                    </ul>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>


                                        <hr />




                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="mainContentBlocks">
                            <div data-name="ContentPlaceHolderMain">


                                <span id="DeltaPlaceHolderMain">

                                    <div class="mainContentBlocksTop">
                                        <div class="mainContentBlock1">
                                            <h1>
                                                <div data-name="Page Field: PageHeading">
                                                    BRITZ CAMPERVAN HIRE &amp; CAR RENTAL NEW ZEALAND
                            
                                                </div>
                                            </h1>
                                            <div data-name="DeviceChannelPanel">
                                            </div>
                                            <div data-name="Page Field: Page Content">


                                                <div id="ctl00_PlaceHolderMain_ctl02_label" style='display: none'>Page Content</div>
                                                <div id="ctl00_PlaceHolderMain_ctl02__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display: inline" aria-labelledby="ctl00_PlaceHolderMain_ctl02_label">
                                                    <p>
                                                        Britz Campervan New Zealand&#160;offers a large range of 
   <strong>
       <a href="/campervans-nz/Pages/default.aspx">
           <strong>Campers</strong></a>,</strong>
                                                        <a title="Car rental in New Zealand" href="/car-hire-nz/Pages/default.aspx">
                                                            <strong>2WD</strong></a> &amp; 
   
      <strong><a href="/car-hire-nz/Pages/default.aspx">4WD Cars for hire</a></strong>. Whether you are travelling by yourself, as a couple or with your family, Britz will have a rental vehicle that will suit your needs.
                                                    </p>
                                                    <p>
                                                        Fly in and&#160;drive out, it's that easy. Britz Campervan Hire NZ&#160;have branches&#160;close to&#160; New Zealand's major international and domestic airports&#160;<a title="Campervan Hire Auckland" href="/campervan-hire-locations/Pages/auckland.aspx"><strong>Auckland</strong></a>, 
   <a title="Christchurch camper hire" href="/campervan-hire-locations/Pages/christchurch.aspx">
       <strong>Christchurch</strong></a> and 
   <a title="Britz Campervan Hire Queenstown" href="/campervan-hire-locations/Pages/queenstown.aspx">
       <strong>Queenstown</strong></a>. Get your personalised quote and book your campervan holiday online today.
                                                    </p>
                                                    <div class="comparebox">
                                                        <a class="comparecampers-front" href="/campervans-nz/Pages/default.aspx"></a><a class="comparecars-front" href="/car-hire-nz/Pages/default.aspx"></a>&#160;
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div data-name="DeviceChannelPanel">
                                        </div>
                                        <div data-name="DeviceChannelPanel">




                                            <div class="mainContentBlock2">
                                                <h3>
                                                    <a href="/about-britz-new-zealand">
                                                        <div data-name="Page Field: HeadingContentBlock2">
                                                            Experience Britz
                                        
                                                        </div>
                                                    </a>
                                                </h3>
                                                <div class="mainContentBlock2Section">
                                                    <div style="float: left; width: 100%;">
                                                        <div data-name="WebPartZone">


                                                            <div>
                                                                <div class="ms-webpart-zone ms-fullWidth">
                                                                    <div id="MSOZoneCell_WebPartWPQ7" class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
                                                                        <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
                                                                            <div webpartid="4a92f6ea-7495-4676-9636-074f5b27e8d7" haspers="false" id="WebPartWPQ7" width="100%" class="ms-WPBody " allowdelete="false" style="">
                                                                                <div class="ms-rtestate-field">
                                                                                    <div class="mainContentBlock2Section1" style="width: 285px; margin-left: 0px;">
                                                                                        <iframe width="310" height="205" src="http://www.youtube.com/embed/xvbsHzcqpdE?rel=0" frameborder="0"></iframe>
                                                                                        <p>
                                                                                            <strong>About Britz Campervans</strong>
                                                                                        </p>
                                                                                        <a class="findOutMoreBtn" href="/about-britz-new-zealand">Find out more</a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="ms-clear"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                    <div data-name="DeviceChannelPanel">




                                        <div class="mainContentBlocksBottom">
                                            <div class="mainContentBlock1">
                                                <h3>
                                                    <a href="/campervan-hire-new-zealand">
                                                        <div data-name="Page Field: HeadingContentBlock3">
                                                            Compare our Campers
                                        
                                                        </div>
                                                    </a>
                                                </h3>
                                                <div class="camperCompare">
                                                    <div class="vehicleSliderContainer">
                                                        <div class="vehicleContainer">
                                                            <div data-name="WebPartZone">


                                                                <div>
                                                                    <div class="ms-webpart-zone ms-fullWidth">
                                                                        <div id="MSOZoneCell_WebPartWPQ5" class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
                                                                            <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
                                                                                <div webpartid="465c6ba5-705a-468c-bc5b-da6ab503f35e" haspers="false" id="WebPartWPQ5" width="100%" class="ms-WPBody " allowdelete="false" style="">
                                                                                    <div class="britzVehicles rs-carousel boom">
                                                                                        <ul class="slides">
                                                                                            <li><a href="http://staging.britz.co.nz/en/campervans-nz/Pages/trailblazer-2-berth-campervan.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Campervans/nzavb.2BTSBT/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>Trailblazer</h3>
                                                                                                    <p>Sleeps 2<br />
                                                                                                        <span>View Camper</span></p>
                                                                                                </span></a></li>
                                                                                            <li><a href="http://staging.britz.co.nz/en/campervans-nz/Pages/explorer-4-berth-campervan.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Campervans/nzavb.4bb/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>Explorer</h3>
                                                                                                    <p>Sleeps 4<br />
                                                                                                        <span>View Camper</span></p>
                                                                                                </span></a></li>
                                                                                            <li><a href="http://staging.britz.co.nz/en/campervans-nz/Pages/frontier-6-berth-campervan.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Campervans/nzavb.6bb/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>Frontier</h3>
                                                                                                    <p>Sleeps 6<br />
                                                                                                        <span>View Camper</span></p>
                                                                                                </span></a></li>
                                                                                            <li><a href="http://staging.britz.co.nz/en/campervans-nz/Pages/renegade-6-berth-campervan.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Campervans/nzavb.6bbr/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>Renegade</h3>
                                                                                                    <p>Sleeps 6<br />
                                                                                                        <span>View Camper</span></p>
                                                                                                </span></a></li>
                                                                                            <li><a href="http://staging.britz.co.nz/en/campervans-nz/Pages/navigator-4-berth-campervan.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Campervans/nzavb.4BBR/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>Navigator</h3>
                                                                                                    <p>Sleeps 4<br />
                                                                                                        <span>View Camper</span></p>
                                                                                                </span></a></li>
                                                                                            <li><a href="http://staging.britz.co.nz/en/campervans-nz/Pages/venturer-2-berth-campervan.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Campervans/nzavb.2BTSBV/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>Venturer</h3>
                                                                                                    <p>Sleeps 2<br />
                                                                                                        <span>View Camper</span></p>
                                                                                                </span></a></li>
                                                                                            <li><a href="http://staging.britz.co.nz/en/campervans-nz/Pages/voyager-4-berth-campervan.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Campervans/nzavb.4BBXS/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>Voyager</h3>
                                                                                                    <p>Sleeps 4<br />
                                                                                                        <span>View Camper</span></p>
                                                                                                </span></a></li>
                                                                                            <li><a href="http://staging.britz.co.nz/en/campervans-nz/Pages/hitop-3-berth-campervan.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Campervans/nzavb.2bb/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>HiTop</h3>
                                                                                                    <p>Sleeps 3<br />
                                                                                                        <span>View Camper</span></p>
                                                                                                </span></a></li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="ms-clear"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h3>
                                                    <a href="/car-hire-new-zealand">
                                                        <div data-name="Page Field: HeadingContentBlock3a">
                                                            Compare our Cars
                                        
                                                        </div>
                                                    </a>
                                                </h3>
                                                <div class="carCompare">
                                                    <div class="vehicleSliderContainer">
                                                        <div class="vehicleContainer">
                                                            <div data-name="WebPartZone">


                                                                <div>
                                                                    <div class="ms-webpart-zone ms-fullWidth">
                                                                        <div id="MSOZoneCell_WebPartWPQ6" class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
                                                                            <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
                                                                                <div webpartid="3fba20c1-2af3-41cf-b218-6c47b25adff9" haspers="false" id="WebPartWPQ6" width="100%" class="ms-WPBody " allowdelete="false" style="">
                                                                                    <div class="britzVehicles rs-carousel boom">
                                                                                        <ul class="slides">
                                                                                            <li><a href="http://staging.britz.co.nz/en/car-hire-nz/Pages/mini-bus-hire.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Cars/nzacb.IVAR/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>Intermediate 12 Seater</h3>
                                                                                                    <p>Seats 12<br />
                                                                                                        <span>View Car</span></p>
                                                                                                </span></a></li>
                                                                                            <li><a href="http://staging.britz.co.nz/en/car-hire-nz/Pages/people-mover.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Cars/nzacb.LVAR/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>People Mover</h3>
                                                                                                    <p>Seats 8<br />
                                                                                                        <span>View Car</span></p>
                                                                                                </span></a></li>
                                                                                            <li><a href="http://staging.britz.co.nz/en/car-hire-nz/Pages/large-4wd.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Cars/nzacb.FFAR/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>Large 4WD</h3>
                                                                                                    <p>Seats 10<br />
                                                                                                        <span>View Car</span></p>
                                                                                                </span></a></li>
                                                                                            <li><a href="http://staging.britz.co.nz/en/car-hire-nz/Pages/4wd-ute-hire.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Cars/nzacb.SFBR/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>Standard 4WD Ute</h3>
                                                                                                    <p>Seats 4<br />
                                                                                                        <span>View Car</span></p>
                                                                                                </span></a></li>
                                                                                            <li><a href="http://staging.britz.co.nz/en/car-hire-nz/Pages/all-wheel-drive-hire.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Cars/nzacb.SFAR/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>Standard AWD</h3>
                                                                                                    <p>Seats 4<br />
                                                                                                        <span>View Car</span></p>
                                                                                                </span></a></li>
                                                                                            <li><a href="http://staging.britz.co.nz/en/car-hire-nz/Pages/standard-car-hire.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Cars/nzacb.sdar/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>Standard Car</h3>
                                                                                                    <p>Seats 5<br />
                                                                                                        <span>View Car</span></p>
                                                                                                </span></a></li>
                                                                                            <li><a href="http://staging.britz.co.nz/en/car-hire-nz/Pages/intermediate-car-hire.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Cars/nzacb.IWARB/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>Intermediate Car</h3>
                                                                                                    <p>Seats 4<br />
                                                                                                        <span>View Car</span></p>
                                                                                                </span></a></li>
                                                                                            <li><a href="http://staging.britz.co.nz/en/car-hire-nz/Pages/compact-car-hire.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Cars/nzacb.CCMR/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>Compact Car</h3>
                                                                                                    <p>Seats 4<br />
                                                                                                        <span>View Car</span></p>
                                                                                                </span></a></li>
                                                                                            <li><a href="http://staging.britz.co.nz/en/car-hire-nz/Pages/4wd-car-hire.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Cars/nzacb.PFAR/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>4WD</h3>
                                                                                                    <p>Seats 5<br />
                                                                                                        <span>View Car</span></p>
                                                                                                </span></a></li>
                                                                                            <li><a href="http://staging.britz.co.nz/en/car-hire-nz/Pages/sedan-car-hire.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Cars/nzacb.FCAR/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>Full Size Car </h3>
                                                                                                    <p>Seats 4<br />
                                                                                                        <span>View Car</span></p>
                                                                                                </span></a></li>
                                                                                            <li><a href="http://staging.britz.co.nz/en/car-hire-nz/Pages/economy-car-hire.aspx" class="vehBox"><span class="image">
                                                                                                <img src="/SiteCollectionImages/Cars/nzacb.ECMR/clearcut.png?renditionid=14" width="88" height="85" /></span><span class="text"><h3>Economy Car</h3>
                                                                                                    <p>Seats 4<br />
                                                                                                        <span>View Car</span></p>
                                                                                                </span></a></li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="ms-clear"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mainContentBlock2">
                                                <h3>
                                                    <a href="/new-zealand-branch-locations">
                                                        <div data-name="Page Field: HeadingContentBlock4">
                                                        </div>
                                                    </a>
                                                </h3>
                                                <div class="branchMap">
                                                    <div data-name="WebPartZone">


                                                        <div>
                                                            <div class="ms-webpart-zone ms-fullWidth">
                                                                <div id="MSOZoneCell_WebPartWPQ8" class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
                                                                    <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
                                                                        <div webpartid="a32ef796-ca46-4a0b-a0ad-53ba4396ee4b" haspers="false" id="WebPartWPQ8" width="100%" class="ms-WPBody " allowdelete="false" style="">
                                                                            <div class="ms-rtestate-field">
                                                                                <div class="branchMap">
                                                                                    <p>Use the map below to find information about our Campervan locations New Zealand wide.</p>
                                                                                    <span id="map-canvas" style="overflow: hidden; position: relative; background-color: #e5e3df;">
                                                                                        <div style="left: 0px; top: 0px; width: 100%; height: 100%; overflow: hidden; position: absolute; z-index: 0;">
                                                                                            <div style="left: 0px; top: 0px; width: 100%; height: 100%; overflow: hidden; position: absolute; z-index: 0;">
                                                                                                <div style="left: 0px; top: 0px; position: absolute; z-index: 1; cursor: url(http://maps.gstatic.com/mapfiles/openhand_8_8.cur), default;">
                                                                                                    <div style="left: 0px; top: 0px; position: absolute; z-index: 200;">
                                                                                                        <div style="left: 0px; top: 0px; position: absolute; z-index: 101;">&#160;</div>
                                                                                                    </div>
                                                                                                    <div style="left: 0px; top: 0px; position: absolute; z-index: 201;">
                                                                                                        <div style="left: 0px; top: 0px; position: absolute; z-index: 102;">
                                                                                                            <div style="left: 0px; top: 0px; position: absolute; z-index: 0;">
                                                                                                                <div style="left: 0px; top: 0px; position: absolute; z-index: 1;">
                                                                                                                    <div style="left: -51px; top: -132px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div>
                                                                                                                    <div style="left: -51px; top: 124px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div>
                                                                                                                    <div style="left: -307px; top: -132px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div>
                                                                                                                    <div style="left: -307px; top: 124px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div>
                                                                                                                    <div style="left: 205px; top: -132px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div>
                                                                                                                    <div style="left: 205px; top: 124px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div style="left: 0px; top: 0px; position: absolute; z-index: 103;">
                                                                                                            <div style="left: 0px; top: 0px; position: absolute; z-index: -1;">
                                                                                                                <div style="left: 0px; top: 0px; position: absolute; z-index: 1;">
                                                                                                                    <div style="left: -51px; top: -132px; width: 256px; height: 256px; overflow: hidden; position: absolute;">
                                                                                                                        <canvas width="256" height="256" draggable="false" style="left: 0px; top: 0px; width: 256px; height: 256px; position: absolute; -ms-user-select: none;"></canvas>
                                                                                                                    </div>
                                                                                                                    <div style="left: -51px; top: 124px; width: 256px; height: 256px; overflow: hidden; position: absolute;">
                                                                                                                        <canvas width="256" height="256" draggable="false" style="left: 0px; top: 0px; width: 256px; height: 256px; position: absolute; -ms-user-select: none;"></canvas>
                                                                                                                    </div>
                                                                                                                    <div style="left: -307px; top: -132px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div>
                                                                                                                    <div style="left: -307px; top: 124px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div>
                                                                                                                    <div style="left: 205px; top: -132px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div>
                                                                                                                    <div style="left: 205px; top: 124px; width: 256px; height: 256px; overflow: hidden; position: absolute;"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div style="left: 0px; top: 0px; position: absolute; z-index: 202;">
                                                                                                        <div style="left: 0px; top: 0px; position: absolute; z-index: 104;">&#160;</div>
                                                                                                        <div style="left: 0px; top: 0px; position: absolute; z-index: 105;">&#160;</div>
                                                                                                        <div style="left: 0px; top: 0px; position: absolute; z-index: 106;">&#160;</div>
                                                                                                    </div>
                                                                                                    <div style="left: 0px; top: 0px; position: absolute; z-index: 100;">
                                                                                                        <div style="left: 0px; top: 0px; position: absolute; z-index: 0;">
                                                                                                            <div style="left: 0px; top: 0px; position: absolute; z-index: 1;">
                                                                                                                <div class="gmnoprint" style="left: -51px; top: -132px; width: 256px; height: 256px; position: absolute; opacity: 0.01; background-color: white;"></div>
                                                                                                                <div class="gmnoprint" style="left: -51px; top: 124px; width: 256px; height: 256px; position: absolute; opacity: 0.01; background-color: white;"></div>
                                                                                                                <div class="gmnoprint" style="left: -307px; top: -132px; width: 256px; height: 256px; position: absolute; opacity: 0.01; background-color: white;"></div>
                                                                                                                <div class="gmnoprint" style="left: -307px; top: 124px; width: 256px; height: 256px; position: absolute; opacity: 0.01; background-color: white;"></div>
                                                                                                                <div class="gmnoprint" style="left: 205px; top: -132px; width: 256px; height: 256px; position: absolute; opacity: 0.01; background-color: white;"></div>
                                                                                                                <div class="gmnoprint" style="left: 205px; top: 124px; width: 256px; height: 256px; position: absolute; opacity: 0.01; background-color: white;"></div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div style="left: 0px; top: 0px; position: absolute; z-index: 0;">
                                                                                                        <div style="overflow: hidden;">&#160;</div>
                                                                                                        &#160;</div>
                                                                                                    <div style="left: 0px; top: 0px; position: absolute; z-index: 0;">
                                                                                                        <div style="left: 0px; top: 0px; position: absolute; z-index: 1;">
                                                                                                            <div style="transition: opacity 200ms ease-out; left: -51px; top: -132px; width: 256px; height: 256px; position: absolute; opacity: 1;">
                                                                                                                <img draggable="false" src="http://mt1.googleapis.com/vt?lyrs=m@244000000&amp;src=apiv3&amp;hl=en-NZ&amp;x=15&amp;y=9&amp;z=4&amp;apistyle=p.s%3A-80%2Cs.t%3A50%7Cs.e%3Ag%7Cp.h%3A%23e77b25%7Cp.s%3A50%2Cs.t%3A49%7Cs.e%3Ag%7Cp.h%3A%23e77b25%7Cp.s%3A50&amp;style=59,37%7Csmartmaps" alt="" style="margin: 0px; padding: 0px; border: 0px currentcolor; width: 256px; height: 256px; -ms-user-select: none;" /></div>
                                                                                                            <div style="transition: opacity 200ms ease-out; left: -51px; top: 124px; width: 256px; height: 256px; position: absolute; opacity: 1;">
                                                                                                                <img width="1" height="1" draggable="false" src="http://mt1.googleapis.com/vt?lyrs=m@244000000&amp;src=apiv3&amp;hl=en-NZ&amp;x=15&amp;y=10&amp;z=4&amp;apistyle=p.s%3A-80%2Cs.t%3A50%7Cs.e%3Ag%7Cp.h%3A%23e77b25%7Cp.s%3A50%2Cs.t%3A49%7Cs.e%3Ag%7Cp.h%3A%23e77b25%7Cp.s%3A50&amp;style=59,37%7Csmartmaps" alt="" style="margin: 0px; padding: 0px; border: 0px currentcolor; width: 256px; height: 256px; -ms-user-select: none;" /></div>
                                                                                                            <div style="transition: opacity 200ms ease-out; left: -307px; top: -132px; width: 256px; height: 256px; position: absolute; opacity: 1;">
                                                                                                                <img width="1" height="1" draggable="false" src="http://mt0.googleapis.com/vt?lyrs=m@244000000&amp;src=apiv3&amp;hl=en-NZ&amp;x=14&amp;y=9&amp;z=4&amp;apistyle=p.s%3A-80%2Cs.t%3A50%7Cs.e%3Ag%7Cp.h%3A%23e77b25%7Cp.s%3A50%2Cs.t%3A49%7Cs.e%3Ag%7Cp.h%3A%23e77b25%7Cp.s%3A50&amp;style=59,37%7Csmartmaps" alt="" style="margin: 0px; padding: 0px; border: 0px currentcolor; width: 256px; height: 256px; -ms-user-select: none;" /></div>
                                                                                                            <div style="transition: opacity 200ms ease-out; left: -307px; top: 124px; width: 256px; height: 256px; position: absolute; opacity: 1;">
                                                                                                                <img width="1" height="1" draggable="false" src="http://mt0.googleapis.com/vt?lyrs=m@244000000&amp;src=apiv3&amp;hl=en-NZ&amp;x=14&amp;y=10&amp;z=4&amp;apistyle=p.s%3A-80%2Cs.t%3A50%7Cs.e%3Ag%7Cp.h%3A%23e77b25%7Cp.s%3A50%2Cs.t%3A49%7Cs.e%3Ag%7Cp.h%3A%23e77b25%7Cp.s%3A50&amp;style=59,37%7Csmartmaps" alt="" style="margin: 0px; padding: 0px; border: 0px currentcolor; width: 256px; height: 256px; -ms-user-select: none;" /></div>
                                                                                                            <div style="transition: opacity 200ms ease-out; left: 205px; top: -132px; width: 256px; height: 256px; position: absolute; opacity: 1;">
                                                                                                                <img width="1" height="1" draggable="false" src="http://mt0.googleapis.com/vt?lyrs=m@244000000&amp;src=apiv3&amp;hl=en-NZ&amp;x=0&amp;y=9&amp;z=4&amp;apistyle=p.s%3A-80%2Cs.t%3A50%7Cs.e%3Ag%7Cp.h%3A%23e77b25%7Cp.s%3A50%2Cs.t%3A49%7Cs.e%3Ag%7Cp.h%3A%23e77b25%7Cp.s%3A50&amp;style=59,37%7Csmartmaps" alt="" style="margin: 0px; padding: 0px; border: 0px currentcolor; width: 256px; height: 256px; -ms-user-select: none;" /></div>
                                                                                                            <div style="transition: opacity 200ms ease-out; left: 205px; top: 124px; width: 256px; height: 256px; position: absolute; opacity: 1;">
                                                                                                                <img width="1" height="1" draggable="false" src="http://mt0.googleapis.com/vt?lyrs=m@244000000&amp;src=apiv3&amp;hl=en-NZ&amp;x=0&amp;y=10&amp;z=4&amp;apistyle=p.s%3A-80%2Cs.t%3A50%7Cs.e%3Ag%7Cp.h%3A%23e77b25%7Cp.s%3A50%2Cs.t%3A49%7Cs.e%3Ag%7Cp.h%3A%23e77b25%7Cp.s%3A50&amp;style=59,37%7Csmartmaps" alt="" style="margin: 0px; padding: 0px; border: 0px currentcolor; width: 256px; height: 256px; -ms-user-select: none;" /></div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div style="margin: 2px 5px 2px 2px; left: 0px; bottom: 0px; position: absolute; z-index: 1000000;"><a title="Click to see this area on Google Maps" href="http://maps.google.com/maps?ll=-40.90056,174.88597&amp;z=4&amp;hl=en-NZ&amp;mapclient=apiv3" target="_blank" style="overflow: visible; float: none; display: inline; position: static;">
                                                                                                <div style="width: 62px; height: 24px; cursor: pointer;">
                                                                                                    <img width="1" height="1" draggable="false" src="http://maps.gstatic.com/mapfiles/google_white.png" alt="" style="margin: 0px; padding: 0px; border: 0px currentcolor; left: 0px; top: 0px; width: 62px; height: 24px; position: absolute; -ms-user-select: none;" />
                                                                                                    <div class="gmnoprint" style="left: 0px; top: 0px; width: 100%; height: 100%; position: absolute; opacity: 0.01; background-color: white;"></div>
                                                                                                </div>
                                                                                            </a>&#160;</div>
                                                                                            <div class="gmnoprint" style="right: 0px; bottom: 0px; position: absolute; z-index: 1000001;">
                                                                                                <div draggable="false" style="background: -ms-linear-gradient(left, rgba(255, 255, 255, 0) 0px, rgba(255, 255, 255, 0.5) 50px); height: 19px; text-align: right; color: #444444; line-height: 19px; padding-right: 2px; padding-left: 50px; font-family: arial,sans-serif; font-size: 10px; white-space: nowrap; direction: ltr; -ms-user-select: none;"><a style="color: #444444; text-decoration: underline; display: none; cursor: pointer;">Map Data</a><span>Map data ©2013 Google</span><span> - </span><a href="http://www.google.com/intl/en-NZ_US/help/terms_maps.html" target="_blank" style="color: #444444; text-decoration: underline; cursor: pointer;">Terms of Use</a></div>
                                                                                            </div>
                                                                                            <div style="padding: 15px 21px; border: 1px solid #ababab; left: 5px; top: 33px; width: 239px; height: 148px; color: #222222; font-family: arial,sans-serif; display: none; position: absolute; z-index: 10000002; box-shadow: 0px 4px 16px rgba(0,0,0,0.2); background-color: white;">
                                                                                                <div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div>
                                                                                                <div style="font-size: 13px;">Map data ©2013 Google</div>
                                                                                                <div style="top: 12px; width: 10px; height: 10px; right: 12px; overflow: hidden; position: absolute; z-index: 10000; cursor: pointer; opacity: 0.7;">
                                                                                                    <img width="1" height="1" draggable="false" src="http://maps.gstatic.com/mapfiles/mv/imgs8.png" alt="" style="margin: 0px; padding: 0px; border: 0px currentcolor; left: -18px; top: -44px; width: 68px; height: 67px; position: absolute; -ms-user-select: none;" />
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="gmnoscreen" style="right: 0px; bottom: 0px; position: absolute;">
                                                                                                <div style="text-align: right; color: #444444; font-family: arial,sans-serif; font-size: 10px; direction: ltr; background-color: #f5f5f5;">Map data ©2013 Google</div>
                                                                                            </div>
                                                                                            <div class="gmnoprint" style="border: 1px solid #dcdcdc; height: 17px; right: 0px; bottom: 0px; line-height: 19px; font-size: 10px; display: none; position: absolute; background-color: #f5f5f5;"><a title="Report errors in the road map or imagery to Google" href="http://maps.google.com/maps?ll=-40.90056,174.88597&amp;z=4&amp;hl=en-NZ&amp;mapclient=apiv3&amp;skstate=action:mps_dialog$apiref:1&amp;output=classic" target="_new" style="padding: 1px 3px; bottom: 1px; color: #444444; font-family: arial,sans-serif; font-size: 85%; font-weight: bold; text-decoration: none; position: relative;">Report a map error</a></div>
                                                                                            <div class="gmnoprint" draggable="false" controlheight="39" controlwidth="22" style="margin: 5px; left: 0px; top: 0px; position: absolute; -ms-user-select: none;">
                                                                                                <div class="gmnoprint" controlheight="39" controlwidth="22" style="left: 0px; top: 0px; position: absolute;">
                                                                                                    <img width="1" height="1" draggable="false" src="http://maps.gstatic.com/mapfiles/szc4.png" alt="" style="margin: 0px; padding: 0px; border: 0px currentcolor; width: 22px; height: 39px; -ms-user-select: none;" />
                                                                                                    <div title="Zoom in" style="left: 0px; top: 0px; width: 22px; height: 17px; position: absolute; cursor: pointer; opacity: 0.01; background-color: white;"></div>
                                                                                                    <div title="Zoom out" style="left: 0px; top: 18px; width: 22px; height: 17px; position: absolute; cursor: pointer; opacity: 0.01; background-color: white;"></div>
                                                                                                    &#160;</div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="ms-clear"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                    </div>
                                    <div data-name="EditModePanelShowInEdit">
                                    </div>
                                    <div style='display: none' id='hidZone'>
                                        <menu class="ms-hide">
	<ie:menuitem id="MSOMenu_Help" iconsrc="/_layouts/15/images/HelpIcon.gif" onmenuclick="MSOWebPartPage_SetNewWindowLocation(MenuWebPart.getAttribute('helpLink'), MenuWebPart.getAttribute('helpMode'))" text="Help" type="option" style="display:none">

	</ie:menuitem>
</menu>
                                    </div>
                                </span>

                            </div>
                        </div>
                        <span class="topright"></span>
                        <span class="topleft"></span>
                        <span class="bottomright"></span>
                        <span class="bottomleft"></span>
                    </div>
                    <!-- ############### MAIN CONTENT ENDS ###############-->
                    <!-- ############### BOTTOM LINKS STARTS ###############-->
                    <div class="bottomLinks clearfix">



                        <div class="ms-webpart-chrome ms-webpart-chrome-fullWidth ">
                            <div webpartid="00000000-0000-0000-0000-000000000000" haspers="true" id="WebPartWPQ2" width="100%" class="ms-WPBody " onlyformepart="true" allowdelete="false" style="">
                                <a href="http://staging.britz.co.nz/en/travel-guide-nz/getting-around/Pages/travel-app.aspx" class="bottomLinksbox"><span class="bottomLinksImage">
                                    <img src="/SiteCollectionImages/Core/tourismRadio.jpg" /><span class="bottomLinksTitle">Tourism Radio</span></span><span class="bottomLinksText"><div class="ExternalClassA676C221553D4C6D8F0E994D6128E024">
                                        <p>Tourism Radio will share the secrets of the surrounding scenery as you travel.</p>
                                    </div>
                                    </span></a><a href="http://staging.britz.co.nz/en/campervan-hire-deals/Pages/hot-everyday-rates.aspx" class="bottomLinksbox"><span class="bottomLinksImage">
                                        <img src="/SiteCollectionImages/Core/priceGuarantee.jpg" /><span class="bottomLinksTitle">Price Match</span></span><span class="bottomLinksText"><div class="ExternalClass849CBF52C04D4F7287CD6E9DFA22424F">
                                            <p>​Book with confidence. We’ll make sure you get the best camper rate with our Price Match.</p>
                                        </div>
                                        </span></a><a href="http://staging.britz.co.nz/en/cycling-in-nz/Pages/default.aspx" class="bottomLinksbox"><span class="bottomLinksImage">
                                            <img src="/SiteCollectionImages/Core/nzcycletrail.jpg" /><span class="bottomLinksTitle">Cycling in New Zealand</span></span><span class="bottomLinksText"><div class="ExternalClassC2BF170F1E944B9DA8CFA263D8FCD48F">
                                                <p>​We have partnered with The New Zealand Cycle Trail, brining you some amazing cycle ways to explore.</p>
                                            </div>
                                            </span></a><a href="http://staging.britz.co.nz/en/travel-guide-nz/info/Pages/holiday-parks.aspx" class="bottomLinksbox"><span class="bottomLinksImage">
                                                <img src="/SiteCollectionImages/Core/campingourway.jpg" /><span class="bottomLinksTitle">Camping Our Way</span></span><span class="bottomLinksText"><div class="ExternalClass3A1B8428732D466DB9E66F452EE27311">
                                                    <p>There are districts in New Zealand that do not permit you to freedom camp. Always check with a local first.</p>
                                                </div>
                                                </span></a>
                                <div class="ms-clear"></div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- ############### BOTTOM LINKS ENDS ###############-->
            </div>
            <div class="clearfix">
            </div>
            <!-- ############### FOOTER STARTS ###############-->
            <div class="footerContainer">
                <div class="footerMainContent">
                    <div class="footerMainContentLeft">
                        <div class="footerLogo">
                            <a href="/">
                                <img src="/SiteCollectionImages/Core/footerLogo.png" alt="Britz Campervans" />
                            </a>
                        </div>
                        <div class="search">
                            <div data-name="SearchBox">


                                <div class="ms-webpart-chrome ms-webpart-chrome-fullWidth ">
                                    <div webpartid="00000000-0000-0000-0000-000000000000" haspers="true" id="WebPartWPQ3" width="100%" class="ms-WPBody " onlyformepart="true" allowdelete="false" style="">
                                        <div componentid="ctl00_ctl46_csr" id="ctl00_ctl46_csr">
                                            <div id="SearchBox" name="Control">
                                                <div class="ms-srch-sb ms-srch-sb-border" id="ctl00_ctl46_csr_sboxdiv">
                                                    <input type="text" value="Search..." maxlength="2048" accesskey="S" title="Search..." id="ctl00_ctl46_csr_sbox" autocomplete="off" autocorrect="off" onkeypress="EnsureScriptFunc('Search.ClientControls.js', 'Srch.U', function() {if (Srch.U.isEnterKey(String.fromCharCode(event.keyCode))) {$find('ctl00_ctl46_csr').search($get('ctl00_ctl46_csr_sbox').value);return Srch.U.cancelEvent(event);}})" onkeydown="EnsureScriptFunc('Search.ClientControls.js', 'Srch.U', function() {var ctl = $find('ctl00_ctl46_csr');ctl.activateDefaultQuerySuggestionBehavior();})" onfocus="EnsureScriptFunc('Search.ClientControls.js', 'Srch.U', function() {var ctl = $find('ctl00_ctl46_csr');ctl.hidePrompt();ctl.setBorder(true);})" onblur="EnsureScriptFunc('Search.ClientControls.js', 'Srch.U', function() {var ctl = $find('ctl00_ctl46_csr'); if (ctl){ ctl.showPrompt(); ctl.setBorder(false);}})" class="ms-textSmall ms-srch-sb-prompt ms-helperText" /><a title="Search" class="ms-srch-sb-searchLink" id="ctl00_ctl46_csr_SearchLink" onclick="EnsureScriptFunc('Search.ClientControls.js', 'Srch.U', function() {$find('ctl00_ctl46_csr').search($get('ctl00_ctl46_csr_sbox').value);})" href="javascript: {}"><img src="/_layouts/15/images/searchresultui.png?rev=23" class="ms-srch-sb-searchImg" id="searchImg" alt="Search" /></a></div>
                                            </div>
                                        </div>
                                        <noscript>
                                            <div id="ctl00_ctl46_noscript">It looks like your browser does not have JavaScript enabled. Please turn on JavaScript and try again.</div>
                                        </noscript>
                                        <div id="ctl00_ctl46">
                                        </div>
                                        <div class="ms-clear"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="languageConnectBar">
                            <a href="https://www.facebook.com/britzfanpage" class="social">
                                <img src="/SiteCollectionImages/Core/facebookFooter.jpg" alt="facebook" />
                            </a>
                            <a href="https://twitter.com/BritzCampervans" class="social">
                                <img src="/SiteCollectionImages/Core/twitterFooter.jpg" alt="twitter" />
                            </a>
                            <div class="language">
                                <div class="currentLang">
                                    Language
                                    
                                    
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                </div>
                                <div class="languageOption">
                                    <a href="/" class="lang English">English
                                    
                                    
                                    
                                    
                                    </a>
                                    <a href="/Location-de-camping-cars-Britz-en-Nouvelle-Zelande" class="lang French">French
                                    
                                    
                                    
                                    
                                    </a>
                                    <a href="/Britz-Wohnmobilvermietung-Neuseeland" class="lang German">German
                                    
                                    
                                    
                                    
                                    </a>
                                    <a href="/Britz-verhuur-van-campers-Nieuw-Zeeland" class="lang Dutch">Dutch
                                    
                                    
                                    
                                    
                                    </a>
                                    <a href="/chinaOld" class="lang Chinese">Chinese
                                    
                                    
                                    
                                    
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="copyright">
                            <p>
                                YOU ARE VIEWING 
                                    
                                    
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                <a href="/" class="website">WWW.BRITZ.CO.NZ
                                    
                                    
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                </a>
                            </p>
                            <p>
                                © 2013 Tourism Holdings Ltd (thl)
                            </p>
                        </div>
                    </div>
                    <div class="footerMainContentCentre">
                        <div class="footerMainContentCentreLeft">
                            <div class="ourSiteLinks">
                                <p>
                                    OUR SITE
                                </p>
                                <ul>
                                    <li>
                                        <a href="/en/campervan-hire-deals/Pages/default.aspx">Hot Travel Deals
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/en/campervans-nz/Pages/default.aspx">Campervans
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/en/car-hire-nz/Pages/default.aspx">Hire Cars
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/en/campervan-hire-locations/Pages/default.aspx">Branch Locations
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/en/travel-guide-nz/Pages/default.aspx">Travel Guide NZ
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/en/nz-travel-itinerary/Pages/Default.aspx">New Zealand Driving Routes
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/cycling-in-new-zealand">Cycling in New Zealand
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/en/contactus/Pages/about-us.aspx">About Us
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/en/contactus/Pages/default.aspx">Contact Us
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="footerMainContentCentreRight">
                            <div class="branchLinks">
                                <p>
                                    BRANCH LOCATIONS
                                
                                
                                </p>
                                <ul>
                                    <li>
                                        <a href="/en/campervan-hire-locations/Pages/auckland.aspx">Auckland 
                                        
                                        
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/en/campervan-hire-locations/Pages/christchurch.aspx">Christchurch
                                        
                                        
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/en/campervan-hire-locations/Pages/queenstown.aspx">Queenstown
                                        
                                        
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="miscLinks">
                                <p>
                                    MISCELLANEOUS
                                
                                
                                </p>
                                <ul>
                                    <li>
                                        <a href="http://www.thlonline.com/AgentMediaCentre/britz/Pages/Britz-Resources.aspx">Agent Media Resources 
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://selfcheckin.thlonline.com/Pages/BookingSearch.aspx">Check-in Online 
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="footerMainContentRight">
                        <div class="footerHotDeals">
                            <span class="footerHotDealsTitle">
                                <a href="/en/campervan-hire-deals/Pages/default.aspx">HOT NZ DEALS
                                </a>
                            </span>
                            <a href="/en/campervan-hire-deals/Pages/default.aspx" class="findOutMoreBtnFooter">View All
                            
                            
                            
                            </a>



                            <div class="ms-webpart-chrome ms-webpart-chrome-fullWidth ">
                                <div webpartid="00000000-0000-0000-0000-000000000000" haspers="true" id="WebPartWPQ4" width="100%" class="ms-WPBody " onlyformepart="true" allowdelete="false" style="">
                                    <ul>
                                        <li><a href="http://staging.britz.co.nz/en/campervan-hire-deals/Pages/south-island-special.aspx">South Island Special</a></li>
                                        <li><a href="http://staging.britz.co.nz/en/campervan-hire-deals/Pages/up-to-60-percent-off.aspx">Christchurch to Auckland</a></li>
                                        <li><a href="http://staging.britz.co.nz/en/campervan-hire-deals/Pages/south-to-north.aspx">South to North</a></li>
                                        <li><a href="http://staging.britz.co.nz/en/campervan-hire-deals/Pages/early-bird-deal.aspx">Early Bird Special</a></li>
                                        <li><a href="http://staging.britz.co.nz/en/campervan-hire-deals/Pages/oxfam-trailwalker-2014.aspx">Oxfam Trailwalker</a></li>
                                        <li><a href="http://staging.britz.co.nz/en/campervans-nz/Pages/bike-hire.aspx">Hire a Britz Bike</a></li>
                                    </ul>
                                    <div class="ms-clear"></div>
                                </div>
                            </div>

                        </div>
                        <div class="footerLogos">
                            <div class="tQual">
                                <img src="/SiteCollectionImages/Core/qualmarkawards.png" alt="Qualmark Awards" />
                            </div>
                            <div class="tourismAustralia">
                                <img src="/SiteCollectionImages/Core/AA101.png" alt="AA101" />
                            </div>
                            <div class="priceBeat">
                                <span class="footerLogosL1">WE WON'T BE
                                    
                                    
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                </span>
                                <span class="footerLogosL2">
                                    <strong>BEATEN
                                        
                                        
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    </strong>














                                    ON PRICE                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footerBase">
                    <div class="footerContact">
                        <div class="footerContactCall">
                            <span class="footerContactTitle">
                                <a href="/en/contactus">Call free within NZ &amp; Worldwide
                                    
                                    
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                </a>
                            </span>
                            <span class="footerContactInfo">
                                <a href="/en/contactus">0800 081 032
                                    
                                    
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                </a>
                            </span>
                        </div>
                        <div class="footerContactMessage">
                            <span class="footerContactTitle">EMAIL
                                
                                
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            </span>
                            <span class="footerContactInfo">
                                <a href="/en/contactus">Send us a message
                                    
                                    
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
                <div id="footerWrap">
                    <!-- virgin start -->
                    <div id="brandedFooter" class="body">
                    </div>
                    <!-- virgin end -->
                    <div class="thl body">
                        <ul>
                            <li>
                                <a class="thl-logo" href="http://www.thlonline.com?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLinkFooter">
                                    <span class="thl"></span>
                                </a>
                            </li>
                            <li>
                                <a class="thllogos" href="http://www.keacampers.com?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLinkFooter">
                                    <span class="kea"></span>
                                </a>
                            </li>
                            <li>
                                <a class="thllogos" href="http://www.maui-rentals.com?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLinkFooter">
                                    <span class="maui"></span>
                                </a>
                            </li>
                            <li>
                                <a class="thllogos" href="http://www.britz.com?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLinkFooter">
                                    <span class="britz"></span>
                                </a>
                            </li>
                            <li>
                                <a class="thllogos" href="http://www.unitedcampervans.com?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLinkFooter">
                                    <span class="united"></span>
                                </a>
                            </li>
                            <li>
                                <a class="thllogos" href="http://www.alphacampers.com?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLinkFooter">
                                    <span class="alpha"></span>
                                </a>
                            </li>
                            <li>
                                <a class="thllogos" href="http://www.mightycampers.com?utm_source=britz.co.nz&amp;utm_medium=referral&amp;utm_campaign=CrossLinkFooter">
                                    <span class="mighty"></span>
                                </a>
                            </li>
                        </ul>
                        <span class="privacy">
                            <b>© Tourism Holidings Limited
                                
                                <br />
                                <a href="/en/contactus/Pages/legal.aspx" class="website">• Legal
                                
                                </a>
                                <a href="/en/contactus/Pages/privacy-statement.aspx" class="website">• Privacy Statement
                                
                                </a>
                            </b>
                        </span>
                    </div>
                </div>
                <!-- ############### FOOTER ENDS ###############-->
            </div>
        </div>

        <script type="text/javascript" src="/SiteCollectionDocuments/js/jquery.flexslider-min.js">//<![CDATA[  
        //]]></script>


        <div data-name="EditModePanelShowInRead">


            <div>


                <!-- BoldChat Visitor Monitor HTML v4.00 (Website=Britz NZ,ChatButton=Britz Floating Chat Button,ChatInvitation=Britz Invite Ruleset) -->
                <script type="text/javascript">//<![CDATA[
                    var _bcvma = _bcvma || [];
                    _bcvma.push(["setAccountID", "308737748753154415"]);
                    _bcvma.push(["setParameter", "WebsiteDefID", "1507752472795397538"]);
                    _bcvma.push(["setParameter", "InvitationDefID", "623700199233307774"]);
                    _bcvma.push(["setParameter", "VisitName", ""]);
                    _bcvma.push(["setParameter", "VisitPhone", ""]);
                    _bcvma.push(["setParameter", "VisitEmail", ""]);
                    _bcvma.push(["setParameter", "VisitRef", ""]);
                    _bcvma.push(["setParameter", "VisitInfo", ""]);
                    _bcvma.push(["setParameter", "CustomUrl", ""]);
                    _bcvma.push(["setParameter", "WindowParameters", ""]);
                    _bcvma.push(["addFloat", {type: "chat", id: "6473739480483909844"}]);
                    _bcvma.push(["pageViewed"]);
                    (function(){
                        var vms = document.createElement("script"); vms.type = "text/javascript"; vms.async = true;
                        vms.src = ('https:'==document.location.protocol?'https://':'http://') + "vmss.boldchat.com/aid/308737748753154415/bc.vms4/vms.js";
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(vms, s);
                    })();
		
                
                    //]]></script>
                <noscript>
                    <a href="http://www.boldchat.com" title="Visitor Monitoring" target="_blank">
                        <img alt="Visitor Monitoring" src="https://vms.boldchat.com/aid/308737748753154415/bc.vmi?wdid=1507752472795397538&amp;vr=&amp;vi=&amp;vn=&amp;vp=&amp;ve=&amp;curl=" border="0" width="1" height="1" />
                    </a>
                </noscript>
                <!-- /BoldChat Visitor Monitor HTML v4.00 -->


            </div>

        </div>
        <!--remarketing new-->
        <script type="text/javascript">//<![CDATA[
            /*  */
            var google_conversion_id = 982569359;
            var google_custom_params = window.google_tag_params;
            var google_remarketing_only = true;
            /*  */
		
        
            //]]></script>
        <script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">//<![CDATA[
		
        
        //]]></script>
        <noscript>
            <div style="display: inline;">
                <img height="1" width="1" style="border-style: none;" alt="" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/982569359/?value=0&amp;guid=ON&amp;script=0" />
            </div>
        </noscript>
        <!--baidu-->
        <script type="text/javascript">//<![CDATA[
            <!-- 
            (function (d) {
                window.bd_cpro_rtid="rjT4PHc";
                var s = d.createElement("script");s.type = "text/javascript";s.async = true;s.src = location.protocol + "//cpro.baidu.com/cpro/ui/rt.js";
                var s0 = d.getElementsByTagName("script")[0];s0.parentNode.insertBefore(s, s0);
            })(document);
            //-->
		
            //]]></script>
        <!--END: baidu-->
        <script type="text/javascript">//<![CDATA[
            function ProcessImn() { }
            function ProcessImnMarkers() { }  
	

        
        
        
        
        
        
            //]]></script>
        <div id="DeltaFormDigest">
        </div>

        <script type="text/javascript">
            //<![CDATA[

            WebForm_InitCallback();var _spFormDigestRefreshInterval = 1440000;window.g_updateFormDigestPageLoaded = new Date(); window.g_updateFormDigestPageLoaded.setDate(window.g_updateFormDigestPageLoaded.getDate() -5);var _fV4UI = true;
            function _RegisterWebPartPageCUI()
            {
                var initInfo = {editable: false,isEditMode: false,allowWebPartAdder: false,listId: "{3f28119c-12fd-42bf-9eec-5d32745d64b8}",itemId: 3,recycleBinEnabled: true,enableMinorVersioning: true,enableModeration: false,forceCheckout: true,rootFolderUrl: "\u002fen\u002fPages",itemPermissions:{High:16,Low:196673}};
                SP.Ribbon.WebPartComponent.registerWithPageManager(initInfo);
                var wpcomp = SP.Ribbon.WebPartComponent.get_instance();
                var hid;
                hid = document.getElementById("_wpSelected");
                if (hid != null)
                {
                    var wpid = hid.value;
                    if (wpid.length > 0)
                    {
                        var zc = document.getElementById(wpid);
                        if (zc != null)
                            wpcomp.selectWebPart(zc, false);
                    }
                }
                hid = document.getElementById("_wzSelected");
                if (hid != null)
                {
                    var wzid = hid.value;
                    if (wzid.length > 0)
                    {
                        wpcomp.selectWebPartZone(null, wzid);
                    }
                }
            };
            function __RegisterWebPartPageCUI() {
                ExecuteOrDelayUntilScriptLoaded(_RegisterWebPartPageCUI, "sp.ribbon.js");}
            _spBodyOnLoadFunctionNames.push("__RegisterWebPartPageCUI");var __wpmExportWarning='This Web Part Page has been personalized. As a result, one or more Web Part properties may contain confidential information. Make sure the properties contain information that is safe for others to read. After exporting this Web Part, view properties in the Web Part description file (.WebPart) by using a text editor such as Microsoft Notepad.';var __wpmCloseProviderWarning='You are about to close this Web Part.  It is currently providing data to other Web Parts, and these connections will be deleted if this Web Part is closed.  To close this Web Part, click OK.  To keep this Web Part, click Cancel.';var __wpmDeleteWarning='You are about to permanently delete this Web Part.  Are you sure you want to do this?  To delete this Web Part, click OK.  To keep this Web Part, click Cancel.';function ctl00_ctl34_g_465c6ba5_705a_468c_bc5b_da6ab503f35e_init() {
                EnsureScript('mediaplayer.js', 
                    typeof(mediaPlayer) != 'undefined' ? typeof(mediaPlayer.attachToMediaLinks) : 'undefined');
                ExecuteOrDelayUntilScriptLoaded(
                    function() {
                        if (Silverlight.isInstalled('2.0')) {
                            mediaPlayer.createOverlayPlayer();
                            mediaPlayer.attachToMediaLinks(document.getElementById('cbqwpctl00_ctl34_g_465c6ba5_705a_468c_bc5b_da6ab503f35e'), ["wmv","wma","avi","mpg","mp3","mp4","asf","ogg","ogv","oga","webm"],'');
                        }
                    }, 'mediaplayer.js');
            }
            if (_spBodyOnLoadFunctionNames != null) { _spBodyOnLoadFunctionNames.push('ctl00_ctl34_g_465c6ba5_705a_468c_bc5b_da6ab503f35e_init'); }
            function ctl00_ctl34_g_3fba20c1_2af3_41cf_b218_6c47b25adff9_init() {
                EnsureScript('mediaplayer.js', 
                    typeof(mediaPlayer) != 'undefined' ? typeof(mediaPlayer.attachToMediaLinks) : 'undefined');
                ExecuteOrDelayUntilScriptLoaded(
                    function() {
                        if (Silverlight.isInstalled('2.0')) {
                            mediaPlayer.createOverlayPlayer();
                            mediaPlayer.attachToMediaLinks(document.getElementById('cbqwpctl00_ctl34_g_3fba20c1_2af3_41cf_b218_6c47b25adff9'), ["wmv","wma","avi","mpg","mp3","mp4","asf","ogg","ogv","oga","webm"],'');
                        }
                    }, 'mediaplayer.js');
            }
            if (_spBodyOnLoadFunctionNames != null) { _spBodyOnLoadFunctionNames.push('ctl00_ctl34_g_3fba20c1_2af3_41cf_b218_6c47b25adff9_init'); }
            function ctl00_g_78dae567_6727_44b2_ab27_37b95ea160fc_init() {
                EnsureScript('mediaplayer.js', 
                    typeof(mediaPlayer) != 'undefined' ? typeof(mediaPlayer.attachToMediaLinks) : 'undefined');
                ExecuteOrDelayUntilScriptLoaded(
                    function() {
                        if (Silverlight.isInstalled('2.0')) {
                            mediaPlayer.createOverlayPlayer();
                            mediaPlayer.attachToMediaLinks(document.getElementById('cbqwpctl00_g_78dae567_6727_44b2_ab27_37b95ea160fc'), ["wmv","wma","avi","mpg","mp3","mp4","asf","ogg","ogv","oga","webm"],'');
                        }
                    }, 'mediaplayer.js');
            }
            if (_spBodyOnLoadFunctionNames != null) { _spBodyOnLoadFunctionNames.push('ctl00_g_78dae567_6727_44b2_ab27_37b95ea160fc_init'); }
            function ctl00_g_36be6f05_6f79_4b2f_8f44_efdb4007d097_init() {
                EnsureScript('mediaplayer.js', 
                    typeof(mediaPlayer) != 'undefined' ? typeof(mediaPlayer.attachToMediaLinks) : 'undefined');
                ExecuteOrDelayUntilScriptLoaded(
                    function() {
                        if (Silverlight.isInstalled('2.0')) {
                            mediaPlayer.createOverlayPlayer();
                            mediaPlayer.attachToMediaLinks(document.getElementById('cbqwpctl00_g_36be6f05_6f79_4b2f_8f44_efdb4007d097'), ["wmv","wma","avi","mpg","mp3","mp4","asf","ogg","ogv","oga","webm"],'');
                        }
                    }, 'mediaplayer.js');
            }
            if (_spBodyOnLoadFunctionNames != null) { _spBodyOnLoadFunctionNames.push('ctl00_g_36be6f05_6f79_4b2f_8f44_efdb4007d097_init'); }
            function ctl00_g_3c019d29_b2d0_44fc_bf78_9740337bc3a6_init() {
                EnsureScript('mediaplayer.js', 
                    typeof(mediaPlayer) != 'undefined' ? typeof(mediaPlayer.attachToMediaLinks) : 'undefined');
                ExecuteOrDelayUntilScriptLoaded(
                    function() {
                        if (Silverlight.isInstalled('2.0')) {
                            mediaPlayer.createOverlayPlayer();
                            mediaPlayer.attachToMediaLinks(document.getElementById('cbqwpctl00_g_3c019d29_b2d0_44fc_bf78_9740337bc3a6'), ["wmv","wma","avi","mpg","mp3","mp4","asf","ogg","ogv","oga","webm"],'');
                        }
                    }, 'mediaplayer.js');
            }
            if (_spBodyOnLoadFunctionNames != null) { _spBodyOnLoadFunctionNames.push('ctl00_g_3c019d29_b2d0_44fc_bf78_9740337bc3a6_init'); }

            ExecuteOrDelayUntilScriptLoaded(
                function() 
                {                    
                    Srch.ScriptApplicationManager.get_current().states = {"browserLanguage":5129,"webUILanguageName":"en-NZ","webDefaultLanguageName":"en-US","contextUrl":"http://staging.britz.co.nz/en","showAdminDetails":false,"defaultPagesListName":"Pages","isSPFSKU":false,"defaultQueryProperties":{"culture":5129,"uiLanguage":5129,"summaryLength":180,"desiredSnippetLength":90,"enableStemming":true,"enablePhonetic":false,"enableNicknames":false,"trimDuplicates":true,"bypassResultTypes":false,"enableInterleaving":true,"enableQueryRules":true,"processBestBets":true,"enableOrderingHitHighlightedProperty":false,"hitHighlightedMultivaluePropertyLimit":-1,"processPersonalFavorites":true}};
                    Srch.U.trace(null, 'SerializeToClient', 'ScriptApplicationManager state initialized.');
                }, 'Search.ClientControls.js');var g_clientIdDeltaPlaceHolderMain = "DeltaPlaceHolderMain";
            var g_clientIdDeltaPlaceHolderUtilityContent = "DeltaPlaceHolderUtilityContent";
            g_QuickLaunchControlIds.push("zz1_V4QuickLaunchMenu");_spBodyOnLoadFunctionNames.push('QuickLaunchInitDroppable'); var g_zz1_V4QuickLaunchMenu = null; function init_zz1_V4QuickLaunchMenu() { if (g_zz1_V4QuickLaunchMenu == null) g_zz1_V4QuickLaunchMenu = $create(SP.UI.AspMenu, null, null, null, $get('zz1_V4QuickLaunchMenu')); } ExecuteOrDelayUntilScriptLoaded(init_zz1_V4QuickLaunchMenu, 'SP.Core.js');

            ExecuteOrDelayUntilScriptLoaded(
                function() 
                {
                    if ($isNull($find('ctl00_ctl46_csr')))
                    {
                        var sb = $create(Srch.SearchBox, {"delayLoadTemplateScripts":true,"messages":[],"queryGroupNames":["Default"],"renderTemplateId":"~sitecollection/_catalogs/masterpage/Display Templates/Search/Control_SearchBox.js","resultsPageAddress":"/aboutus/Pages/SearchResults.aspx","serverInitialRender":true,"showDataErrors":true,"showQuerySuggestions":false,"states":{},"tryInplaceQuery":false}, null, null, $get("ctl00_ctl46_csr"));
                        sb.activate('Search...', 'ctl00_ctl46_csr_sbox', 'ctl00_ctl46_csr_sboxdiv', 'ctl00_ctl46_csr_NavButton', 'ctl00_ctl46_csr_AutoCompList', 'ctl00_ctl46_csr_NavDropdownList', 'ctl00_ctl46_csr_SearchLink', 'ms-srch-sbprogress', 'ms-srch-sb-prompt ms-helperText');
                    }
                }, 'Search.ClientControls.js');//]]>
        </script>
    </form>
    <span id="DeltaPlaceHolderUtilityContent"></span>
</body>
</html>
<!-- Rendered using cache profile:Public Internet (Purely Anonymous) at: 2014-10-29T20:10:35 -->
