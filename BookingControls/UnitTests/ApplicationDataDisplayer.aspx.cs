﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class UnitTests_ApplicationDataDisplayer : System.Web.UI.Page
{
    public string ApplicationDataHTML;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        ApplicationDataHTML = THLDebug.DisplayApplicationData(ApplicationManager.GetApplicationData());
    }
}
