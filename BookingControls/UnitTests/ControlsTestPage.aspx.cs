﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ControlsTestPage : System.Web.UI.Page
{

    public string QueryStr = string.Empty;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.RawUrl.IndexOf('?') > 0)
            QueryStr = Request.RawUrl.Split('?')[1];
    }
}
