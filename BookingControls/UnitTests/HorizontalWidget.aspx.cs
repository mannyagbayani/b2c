﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class HorizontalWidget : System.Web.UI.Page
{
    public string JSONParams;

    public bool HasModel = false;

    AvailabilityRequest availabilityRequest;

    public int numMonths;

    protected void Page_Load(object sender, EventArgs e)
    {
        availabilityRequest = AvailabilityHelper.GetAvailabilityRequestForWidgetParams(Request.Params);
        THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
        ApplicationData appData = ApplicationManager.GetApplicationData();
        Branch[] branches = appData.GetWidgetBranches();
        Vehicle[] vehicles = appData.Vehicles;
        
        numMonths = 2;
        int.TryParse(Request.Params["months"], out numMonths);

        if(!string.IsNullOrEmpty(Request.Params["vc"]))
        {
            HasModel = true;
        }


        JSONParams = AvailabilityHelper.GetWidgetJSONForRequest(availabilityRequest, branches, vehicles);
        
        //Handle External Requests
        if(!string.IsNullOrEmpty(Request.Params["service"]) && Request.Params["service"].ToLower().Equals("json"))
        {
            Response.ContentType = "application/json";
            Response.Write(JSONParams);
            Response.End();
        }
        else if (!string.IsNullOrEmpty(Request.Params["service"]) && Request.Params["service"].ToLower().Equals("xml"))
        {
            Response.ContentType = "text/xml";
            Response.Write(AvailabilityHelper.GetWidgetXMLForRequest(availabilityRequest, branches, vehicles));
            Response.End();
        }        
    }
}