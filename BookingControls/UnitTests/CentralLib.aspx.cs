﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Text;
using THL.Booking;

/// <summary>
/// Structure for Vehicle Content Cache Element
/// </summary>
public class VehicleContent
{
    public bool Isloaded { get; set; }
    public string VehicleCode { get; set; }
    public string VehicleName { get; set; }
    public THLBrands Brand;
    public CountryCode Country { get; set; }

    
    public string Debug;

    /// <summary>
    /// Construct the content element using a vehicle list xml node
    /// </summary>
    /// <param name="vehicleNode"></param>
    public VehicleContent(XmlNode vehicleNode)
    {
        try
        {
            VehicleCode = vehicleNode.SelectSingleNode("VehicleCode").InnerText;
            VehicleName = vehicleNode.SelectSingleNode("LinkTitle").InnerText;
            string vehicleBrandStr = vehicleNode.SelectSingleNode("Brand").InnerText;
            string[] vehicleBrandParams = vehicleBrandStr.Replace("Explore More", "ExploreMore").Split(' ');//TODO: define ExMore Conve
            Debug = vehicleBrandParams[0] + ":" + vehicleBrandParams[1];
            Country = AvailabilityHelper.GetCountryCodeForString(vehicleBrandParams[1]);
            Brand = GetBrandForName(vehicleBrandParams[0]);

            Isloaded = true;
        }
        catch (Exception ex)
        {
            //TODO: log and handle
            string msg = ex.Message;
            Isloaded = false;
        }
    }
    
    
    
    public string ToJSON() 
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("{");
        sb.Append("\"code\":\"" + VehicleCode + "\",");
        sb.Append("\"name\":\"" + VehicleName + "\",");
        sb.Append("\"country\":\"" + Country + "\",");
        sb.Append("\"brand\":\"" + Brand + "\"");
        //sb.Append("\"debug\":\"" + Debug + "\"");       
        
        sb.Append("}");
        return sb.ToString();
    }

    /// <summary>
    /// TODO: remove into Brand Helper once unit tested
    /// </summary>
    /// <param name="brandName"></param>
    /// <returns></returns>
    public static THLBrands GetBrandForName(string brandName)        
    {
        THLBrands brand = THLBrands.Generic;
        switch (brandName.ToLower())
        { 
            case "maui":
                brand = THLBrands.Maui;
                break;
            case "britz":
                brand = THLBrands.Britz;
                break;
            case "backpacker":
                brand = THLBrands.Backpackers;
                break;
            case "exploremore":
                brand = THLBrands.ExploreMore;
                break;
            //TODO: more if any       
        }
        return brand;
    }

}

public partial class UnitTests_CentralLib : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        XmlDocument libDoc = new XmlDocument();
        //TODO: construct url and params (view, list)
        libDoc.Load("http://feed.motorhomesandcars.uat.thl.com/getsplistxml.ashx");
        XmlNodeList nodes = libDoc.SelectNodes("//row");
        foreach (XmlNode node in nodes)
        {
            string vehicleCode = (node.SelectSingleNode("VehicleCode") != null ? node.SelectSingleNode("VehicleCode").InnerText : string.Empty);
            if (!string.IsNullOrEmpty(NormaliseVehicleCode(vehicleCode)))
                Response.Write(new VehicleContent(node).ToJSON() + "<br />");
        }
    }

    /// <summary>
    /// TODO: remove into Vehicle Helper Static Method
    /// </summary>
    /// <param name="vehicleCode"></param>
    /// <returns>Normalised form of vehicle code, emty string on normalisation failure</returns>
    private string NormaliseVehicleCode(string vehicleCode)
    {
        if (string.IsNullOrEmpty(vehicleCode)) return string.Empty;
        string normalised = vehicleCode.ToLower();
        if (vehicleCode.IndexOf("nz") != 0 && vehicleCode.IndexOf("au") != 0)
            return string.Empty;
        //TODO: full implementation of 
        return normalised;    
    }
}
