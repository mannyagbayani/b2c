﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using THL.Booking;

public partial class AltAvailabilityRowTester : System.Web.UI.Page
{

    public string TestRow, BrandChar = "m";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //test zone        
        TestRow = RenderAlternateRow(AlternativeAvailabilityType.SwitchLocation);
        TestRow += RenderAlternateRow(AlternativeAvailabilityType.MoveForward);
        BrandChar = String.IsNullOrEmpty(Request.Params["brand"]) ? "m" : Request.Params["brand"];
    }

    /// <summary>
    /// Refactor into AvaliabilityResponseDisplayer 
    /// </summary>
    /// <param name="aType"></param>
    /// <returns></returns>
    public  string RenderAlternateRow(AlternativeAvailabilityType aType/*AlternativeAvailabilityItem alternateItem*/)
    {
        string alternateTypeLabel = GetAlternativeTypeLabel(aType);//refacor into static helper

        //Stub, TODO: take from biz object <AlternativeAvailabilityItem>
        int spannerWidth = 400;
        int hireDays = new Random().Next(5, 100);
        int leadTime = 5 + new Random().Next(5, 100);

        int originalHirePeriod = hireDays;
        int suggestedPickUpPriorToTravel = 0;
        int totalDays = 20 + originalHirePeriod ;
        decimal PxPerDay =  spannerWidth / totalDays ;
        PxPerDay = Math.Round(PxPerDay);
        DateTime pickUpDate = DateTime.Now.AddDays(leadTime);
        DateTime dropOffDate = pickUpDate.AddDays(hireDays);
        string pickUpLoc = "Auckland", dropOffLoc = "Christchurch", pickUpDateStr = pickUpDate.ToString("dd MMM yy") + "'", dropOffDateStr = dropOffDate.ToString("dd MMM yy") + "'";
        
        StringBuilder sb = new StringBuilder();       
        sb.Append(@"             
        <div class=""OptionRow"">
            <div class=""AltRow "+ aType.ToString() + @""">
                <div class=""AltType"">
                    <span>" + alternateTypeLabel + @"</span>
                </div>
                <div class=""AltTime"">
                    <div class=""TimeLabels period"">                
                        <span style=""width: " + ((spannerWidth/2)-20) + @"px;"" class=""preORDates"">&nbsp;</span> 
                        <span rel=""pudate"" style=""width:40px;"" class=""hireORDates"">
                            <small>" + hireDays +@" days</small>
                        </span>
                    </div>
                    <div class=""TimeSpan"">
                        <span class=""preDates"" style=""width:" + PxPerDay * (10 - suggestedPickUpPriorToTravel) + @"px;"">&nbsp;</span>
                        <span class=""hireDates"" style=""width:" + (PxPerDay * hireDays) + @"px;"">&nbsp;</span>
                        <span class=""postDates"">&nbsp;</span>
                    </div>
                    <div class=""TimeLabels dates"">
                        <span style=""width: " + ((PxPerDay * 10)- 36) + @"px;"" class=""preORDates"">&nbsp;</span> 
                        <span rel=""pudate"" class=""hireORDates"" style=""width: 72px;"">
                            <small>" + pickUpDateStr + " " + pickUpLoc + @"</small>
                        </span>
                        <span style=""width:" + ((PxPerDay * hireDays) - 72) /*subtract labels*/ + @"px"">&nbsp;</span>
                        <span rel=""dodate"" class=""postORDates"" style=""width: 72px;"">
                            <small>" + dropOffDateStr + " " + dropOffLoc + @"</small>
                        </span>
                    </div>
                </div>
                <div class=""AltPrice"" title=""MAUI5"">
                    <ul class=""Prices""><li><span>AUD 5,110.20</span></li><li class=""Fp Hide NZD""><span>NZD $ 6,860.25</span></li><li class=""Fp Hide USD""><span>USD $ 5,106.77</span></li><li class=""Fp Hide GBP""><span>GBP £ 3,149.54</span></li></ul>
                    <a class=""ShowPriceList"">Price Details</a>
                </div>                                               
                <a href=""Configure.aspx?avp=9109EBDFF1FA470EA0CC65BF2E01D296"" class=""AltSelect"">Select</a>
            </div>
            <div class=""PriceDetailsList NoFreeDays"">
                <table class=""chargesTable"">
                    <thead>
                        <tr>
                            <th class=""RateTH"">
                                Product Description
                            </th>
                            <th class=""FreeTH"">
                                <span>Free Days</span><span rel=""freeDays"" class=""popup"" style=""display: none;"">[?]</span>
                            </th>
                            <th class=""HireTH"">
                                Hire Days
                            </th>
                            <th class=""PerTH"">
                                Per Day Price
                            </th>
                            <th class=""DiscTH"">
                                <span>Discounted Day Price</span><span rel=""DiscountedDaysPopUp"" class=""popup"" onmouseover=""initPopUp(this)"">[?]</span>
                            </th>
                            <th class=""TotalTH"">
                                Total
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class=""rt"">
                                16-Mar-2011 to 20-Apr-2011
                            </td>
                            <td class=""fd"">
                            </td>
                            <td class=""hp"">
                                36 day(s)
                            </td>
                            <td class=""pdp"">
                                $167.00
                            </td>
                            <td class=""dpd"">
                                $141.95
                            </td>
                            <td>
                                $5110.20
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class=""DiscountedDaysPopUp bubbleBody"">
                    <h3>Discounted Day Price</h3>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    Long Hire Discount
                                </td>
                                <td>
                                    15.00%
                                </td>
                            </tr>
                            <tr class=""total"" rel=""15.00"">
                                <td>
                                    Total Discount
                                </td>
                                <td>
                                    15.00%
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <span class=""note"">(off standard per day rates)</span>
                </div>
            </div>
        </div>
        ");
        return sb.ToString();
    }

    /// <summary>
    /// Refactor into static helper on AvailabilityResponseDisaplayer
    /// </summary>
    /// <param name="aType"></param>
    /// <returns></returns>
    public string GetAlternativeTypeLabel(AlternativeAvailabilityType aType)
    {
        string responseStr = string.Empty;
        switch (aType)
        { 
            case AlternativeAvailabilityType.DropOffBased:
                responseStr = "Fixed drop off";
                break;
            case AlternativeAvailabilityType.PickUpBased:
                responseStr = "Fixed pick up date";
                break;
            case AlternativeAvailabilityType.SwitchLocation:
                responseStr = "Reverese route";
                break;
            case AlternativeAvailabilityType.MoveForward:
                responseStr = "Later pick up date";
                break;
            case AlternativeAvailabilityType.MoveBackwards:
                responseStr = "Earlier pick up date";
                break;
            case AlternativeAvailabilityType.OneWaySwitchLocation:
                responseStr = "One way switch location";
                break;
            default:
                responseStr = "Normal Based";//TODO: define defaults and biz logic if any
                break;        
        }
        return responseStr.ToUpper();
    }

}
