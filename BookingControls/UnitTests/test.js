﻿function getStatus() {
    var uniqueId = Math.random();
    var url = pingServiceUrl +'?tid=' + uniqueId + "&" + providedParams + (progressStarted ? "&state=ping" : "");
    $.get(url, function(data) {
        progressStarted = true;
        if (data.indexOf("loaded...") == -1 && data.indexOf("NoMatch") == -1 && --loadAttempts > 0) {
            $('#status').html(data);
            $('#statusFill').width(data);
            window.setTimeout("getStatus()", pingFreq);
        }
        else {
            //window.location = redirectURL; //alert(redirectURL);            
            alert('server data is ready');
        };
    });
}