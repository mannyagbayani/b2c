﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BritzAUNew.aspx.cs" Inherits="UnitTests_BritzAUNew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en" dir="ltr">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
    <meta name="GENERATOR" content="Microsoft SharePoint" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="Expires" content="0" />
    <title>Campervans Campervan Hire </title>
    <link rel="stylesheet" type="text/css" href="/_layouts/15/1033/styles/Themable/corev15.css?rev=BdxJNFd%2FTPOed3Z8IKEJ9A%3D%3D" />
    <link rel="stylesheet" type="text/css" href="/Style%20Library/en-US/Themable/Core%20Styles/controls15.css" />
    <link rel="stylesheet" type="text/css" href="/SiteCollectionDocuments/css/styles.css?v=1.0" />
    <script type="text/javascript" src="http://dev.britz.com.au/_layouts/15/init.js?rev=7Tix801bl5ZziOKr5K1nrw%3D%3D"></script>
    <script type="text/javascript" src="http://dev.britz.com.au/ScriptResource.axd?d=qQlo8omAnbNecNsT3WrgrU208ZgWmUx7vHCt51iKwpTfl0W0rEtLkla9dj_zsnHwgsElZ0CQlM0Wmkjzqb67tlSZfN3xnY8o9C7_8GzzglCLogICwNgscs4p8mmhFwHgHYWJtODiLgIQnd7_hv9S8CObIjcq8FTCKAusSDZhHXLWTuEdZ9P6v0lkNiwlPPjR0&amp;t=6119e399"></script>
    <script type="text/javascript" src="http://dev.britz.com.au/_layouts/15/blank.js?rev=ZaOXZEobVwykPO9g8hq%2F8A%3D%3D"></script>
    <script type="text/javascript" src="http://dev.britz.com.au/ScriptResource.axd?d=zhvOne9b3xs4uO0Xk1tm9Kq7R8ZcZPVsLVRLXg6Mr05QFyiaRqUVjGgyqdxqgI6CKky1A1x10rQnjuj3f--H0euW62QxultxzyMzci23AdPPqih89tYvWb2PD1PEa8VFxC0euADsdFTZkqFK9XhHbcbOFVf4DmTSfcUoOP9VeSu6SV5ZE2PF63H5Dzp7NQjY0&amp;t=6119e399"></script>
    <link type="text/xml" rel="alternate" href="http://dev.britz.com.au/campervans/_vti_bin/spsdisco.aspx" />
    <link rel="canonical" href="http://dev.britz.com.au:80/campervans/Pages/default.aspx" />
    <!--<span id="DeltaSPWebPartManager"></span>-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js">//<![CDATA[
        //]]></script>
    <script type="text/javascript" src="http://dev.britz.com.au/SiteCollectionDocuments/js/jquery.ui.widget.js">//<![CDATA[
        //]]></script>
    <!-- carousel core -->
    <script type="text/javascript" src="http://dev.britz.com.au/SiteCollectionDocuments/js/jquery.rs.carousel.js">//<![CDATA[
        //]]></script>
    <!-- carousel extensions (optional) -->
    <script type="text/javascript" src="http://dev.britz.com.au/SiteCollectionDocuments/js/jquery.rs.carousel-continuous.js">//<![CDATA[
        //]]></script>
    <script type="text/javascript" src="http://dev.britz.com.au/SiteCollectionDocuments/js/jquery.rs.carousel-autoscroll.js">//<![CDATA[
        //]]></script>
    <!-- to allow touch -->
    <script type="text/javascript" src="http://dev.britz.com.au/SiteCollectionDocuments/js/jquery.rs.carousel-touch.js">//<![CDATA[
        //]]></script>
    <!-- style booking control -->
    <script type="text/javascript" src="http://dev.britz.com.au/SiteCollectionDocuments/js/jquery.customSelect.min.js">//<![CDATA[
        //]]></script>
    <!-- Isotope filtering -->
    <script type="text/javascript" src="http://dev.britz.com.au/SiteCollectionDocuments/js/jquery.isotope.min.js">//<![CDATA[
        //]]></script>
        <!--
    <script type="text/javascript" src="http://dev.britz.com.au/SiteCollectionDocuments/js/britzIni.js">//<![CDATA[
        //]]></script>
        -->
    <style type="text/css">
        .s4-skipribbonshortcut
        {
            display: none;
        }
    </style>
</head>
<body onhashchange="if (typeof(_spBodyOnHashChange) != 'undefined') _spBodyOnHashChange();">
    <div id="imgPrefetch" style="display: none">
        <img src="/_layouts/15/images/spcommon.png?rev=23" />
        <img src="/_layouts/15/images/spcommon.png?rev=23" />
    </div>
    <form method="post" action="default.aspx" onsubmit="javascript:return WebForm_OnSubmit();"
    id="aspnetForm">
    <div class="aspNetHidden">
        <input type="hidden" name="_wpcmWpid" id="_wpcmWpid" value="" />
        <input type="hidden" name="wpcmVal" id="wpcmVal" value="" />
        <input type="hidden" name="MSOWebPartPage_PostbackSource" id="MSOWebPartPage_PostbackSource"
            value="" />
        <input type="hidden" name="MSOTlPn_SelectedWpId" id="MSOTlPn_SelectedWpId" value="" />
        <input type="hidden" name="MSOTlPn_View" id="MSOTlPn_View" value="0" />
        <input type="hidden" name="MSOTlPn_ShowSettings" id="MSOTlPn_ShowSettings" value="False" />
        <input type="hidden" name="MSOGallery_SelectedLibrary" id="MSOGallery_SelectedLibrary"
            value="" />
        <input type="hidden" name="MSOGallery_FilterString" id="MSOGallery_FilterString"
            value="" />
        <input type="hidden" name="MSOTlPn_Button" id="MSOTlPn_Button" value="none" />
        <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
        <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
        <input type="hidden" name="__REQUESTDIGEST" id="__REQUESTDIGEST" value="InvalidFormDigest" />
        <input type="hidden" name="MSOAuthoringConsole_FormContext" id="MSOAuthoringConsole_FormContext"
            value="" />
        <input type="hidden" name="MSOAC_EditDuringWorkflow" id="MSOAC_EditDuringWorkflow"
            value="" />
        <input type="hidden" name="InputComments" id="InputComments" value="" />
        <input type="hidden" name="MSOSPWebPartManager_DisplayModeName" id="MSOSPWebPartManager_DisplayModeName"
            value="Browse" />
        <input type="hidden" name="MSOSPWebPartManager_ExitingDesignMode" id="MSOSPWebPartManager_ExitingDesignMode"
            value="false" />
        <input type="hidden" name="MSOWebPartPage_Shared" id="MSOWebPartPage_Shared" value="" />
        <input type="hidden" name="MSOLayout_LayoutChanges" id="MSOLayout_LayoutChanges"
            value="" />
        <input type="hidden" name="MSOLayout_InDesignMode" id="MSOLayout_InDesignMode" value="" />
        <input type="hidden" name="_wpSelected" id="_wpSelected" value="" />
        <input type="hidden" name="_wzSelected" id="_wzSelected" value="" />
        <input type="hidden" name="MSOSPWebPartManager_OldDisplayModeName" id="MSOSPWebPartManager_OldDisplayModeName"
            value="Browse" />
        <input type="hidden" name="MSOSPWebPartManager_StartWebPartEditingName" id="MSOSPWebPartManager_StartWebPartEditingName"
            value="false" />
        <input type="hidden" name="MSOSPWebPartManager_EndWebPartEditing" id="MSOSPWebPartManager_EndWebPartEditing"
            value="false" />
        <input type="hidden" name="_maintainWorkspaceScrollPosition" id="_maintainWorkspaceScrollPosition"
            value="0" />
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPBSpWU0tleTpjMjk4MzI5My1hZWE0LTQyZjEtYjhlMS00ZTI2MTRiMzZlOGNk2qtAzpFtvBjt+zMc0j8k+1I+A1eR+AfXlKe1V0zQN7c=" />
    </div>
    <script type="text/javascript">
//<![CDATA[
        var theForm = document.forms['aspnetForm'];
        if (!theForm) {
            theForm = document.aspnetForm;
        }
        function __doPostBack(eventTarget, eventArgument) {
            if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
                theForm.__EVENTTARGET.value = eventTarget;
                theForm.__EVENTARGUMENT.value = eventArgument;
                theForm.submit();
            }
        }
//]]>
    </script>
    <script src="/WebResource.axd?d=BkWoBWG7qPFZzfBZSOsUEXquBc91BSgG4VBKwNerSjVlBmPfuNMRMQweY-9ZIMRulM9hleRRR91aH0wI7N9HyQqx0urWaTRpd4Y-mIMaxhQ1&amp;t=634773866700000000"
        type="text/javascript"></script>
    <script type="text/javascript">
//<![CDATA[
        var MSOWebPartPageFormName = 'aspnetForm';
        var g_presenceEnabled = true;
        var g_wsaEnabled = false;
        var g_wsaQoSEnabled = false;
        var g_wsaQoSDataPoints = [];
        var g_wsaLCID = 1033;
        var g_wsaListTemplateId = 850;
        var g_wsaSiteTemplateId = 'CMSPUBLISHING#0';
        var _fV4UI = true; var _spPageContextInfo = { webServerRelativeUrl: "\u002fcampervans", webAbsoluteUrl: "http:\u002f\u002fdev.britz.com.au\u002fcampervans", siteAbsoluteUrl: "http:\u002f\u002fdev.britz.com.au", serverRequestPath: "\u002fcampervans\u002fPages\u002fdefault.aspx", layoutsUrl: "_layouts\u002f15", webTitle: "Campervans", webTemplate: "39", tenantAppVersion: "0", webLogoUrl: "\u002fSiteCollectionImages\u002fCore\u002flogo.png", webLanguage: 1033, currentLanguage: 1033, currentUICultureName: "en-US", currentCultureName: "en-US", clientServerTimeDelta: new Date("2013-08-11T22:10:01.9071722Z") - new Date(), siteClientTag: "2421$$15.0.4481.1005", crossDomainPhotosEnabled: false, webUIVersion: 15, webPermMasks: { High: 2147483647, Low: 4294967295 }, pageListId: "{4a82c6d3-aadc-4627-81f3-b015b8b40367}", pageItemId: 1, pagePersonalizationScope: 1, userId: 1, systemUserKey: "i:0\u0029.w|s-1-5-21-2481416051-4211903990-3718014954-1119", alertsEnabled: false, siteServerRelativeUrl: "\u002f", allowSilverlightPrompt: 'True' };
        function DoCallBack(filterText) {
            WebForm_DoCallback('ctl00$ctl34$g_428a7651_8ea2_4c53_932c_e511ad3c6f66', filterText, UpdateFilterCallback, 0, CallBackError, true)
        }
        function CallBackError(result, clientsideString) {
        }
        var L_Menu_BaseUrl = "/campervans";
        var L_Menu_LCID = "1033";
        var L_Menu_SiteTheme = "null";
        document.onreadystatechange = fnRemoveAllStatus; function fnRemoveAllStatus() { removeAllStatus(true) };
        function _spNavigateHierarchy(nodeDiv, dataSourceId, dataPath, url, listInContext, type) {

            CoreInvoke('ProcessDefaultNavigateHierarchy', nodeDiv, dataSourceId, dataPath, url, listInContext, type, document.forms.aspnetForm, "", "\u002fcampervans\u002fPages\u002fdefault.aspx");

        }
        var _spWebPartComponents = new Object();//]]>
    </script>
    <script src="/_layouts/15/blank.js?rev=ZaOXZEobVwykPO9g8hq%2F8A%3D%3D" type="text/javascript"></script>
    <script type="text/javascript">
//<![CDATA[
        (function () {

            if (typeof (_spBodyOnLoadFunctions) === 'undefined' || _spBodyOnLoadFunctions === null) {
                return;
            }
            _spBodyOnLoadFunctions.push(function () {

                if (typeof (SPClientTemplates) === 'undefined' || SPClientTemplates === null || (typeof (APD_InAssetPicker) === 'function' && APD_InAssetPicker())) {
                    return;
                }

                var renderFollowFooter = function (renderCtx, calloutActionMenu) {
                    if (renderCtx.ListTemplateType == 700)
                        myDocsActionsMenuPopulator(renderCtx, calloutActionMenu);
                    else
                        CalloutOnPostRenderTemplate(renderCtx, calloutActionMenu);

                    var listItem = renderCtx.CurrentItem;
                    if (typeof (listItem) === 'undefined' || listItem === null) {
                        return;
                    }
                    if (listItem.FSObjType == 0) {
                        calloutActionMenu.addAction(new CalloutAction({
                            text: Strings.STS.L_CalloutFollowAction,
                            tooltip: Strings.STS.L_CalloutFollowAction_Tooltip,
                            onClickCallback: function (calloutActionClickEvent, calloutAction) {
                                var callout = GetCalloutFromRenderCtx(renderCtx);
                                if (!(typeof (callout) === 'undefined' || callout === null))
                                    callout.close();
                                SP.SOD.executeFunc('followingcommon.js', 'FollowSelectedDocument', function () { FollowSelectedDocument(renderCtx); });
                            }
                        }));
                    }
                };

                var registerOverride = function (id) {
                    var followingOverridePostRenderCtx = {};
                    followingOverridePostRenderCtx.BaseViewID = 'Callout';
                    followingOverridePostRenderCtx.ListTemplateType = id;
                    followingOverridePostRenderCtx.Templates = {};
                    followingOverridePostRenderCtx.Templates.Footer = function (renderCtx) {
                        var renderECB;
                        if (typeof (isSharedWithMeView) === 'undefined' || isSharedWithMeView === null) {
                            renderECB = true;
                        } else {
                            var viewCtx = getViewCtxFromCalloutCtx(renderCtx);
                            renderECB = !isSharedWithMeView(viewCtx);
                        }
                        return CalloutRenderFooterTemplate(renderCtx, renderFollowFooter, renderECB);
                    };
                    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(followingOverridePostRenderCtx);
                }
                registerOverride(101);
                registerOverride(700);
            });
        })(); (function () {

            if (typeof (_spBodyOnLoadFunctions) === 'undefined' || _spBodyOnLoadFunctions === null) {
                return;
            }
            _spBodyOnLoadFunctions.push(function () {
                ExecuteOrDelayUntilScriptLoaded(
            function () {
                var pairs = SP.ScriptHelpers.getDocumentQueryPairs();
                var followDoc, itemId, listId, docName;
                for (var key in pairs) {
                    if (key.toLowerCase() == 'followdocument')
                        followDoc = pairs[key];
                    else if (key.toLowerCase() == 'itemid')
                        itemId = pairs[key];
                    else if (key.toLowerCase() == 'listid')
                        listId = pairs[key];
                    else if (key.toLowerCase() == 'docname')
                        docName = decodeURI(pairs[key]);
                }

                if (followDoc != null && followDoc == '1' && listId != null && itemId != null && docName != null) {
                    SP.SOD.executeFunc('followingcommon.js', 'FollowDocumentFromEmail', function () {
                        FollowDocumentFromEmail(itemId, listId, docName);
                    });
                }

            }, 'SP.init.js');

            });
        })(); if (typeof (DeferWebFormInitCallback) == 'function') DeferWebFormInitCallback(); function WebForm_OnSubmit() {
            UpdateFormDigest('\u002fcampervans', 1440000);
            var workspaceElem = GetCachedElement("s4-workspace");
            if (workspaceElem != null) {
                var scrollElem = GetCachedElement("_maintainWorkspaceScrollPosition");
                if (scrollElem != null) {
                    scrollElem.value = workspaceElem.scrollTop;
                }
            };
            if (typeof (_spFormOnSubmitWrapper) != 'undefined') { return _spFormOnSubmitWrapper(); } else { return true; };
            return true;
        }
//]]>
    </script>
    <div class="aspNetHidden">
        <input type="hidden" name="__SCROLLPOSITIONX" id="__SCROLLPOSITIONX" value="0" />
        <input type="hidden" name="__SCROLLPOSITIONY" id="__SCROLLPOSITIONY" value="0" />
        <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAOC4GKCrOwB4a9byMzQZ098cGgNYb1+CWl+S5sJ9jZtsyJEtOXfcEJ5Q2L1wGar0uml52acIEvOx5Vnth5K8GiZhrNLE4prMgRYeUEbzJvDEg==" />
    </div>
    <script type="text/javascript">
//<![CDATA[
        Sys.WebForms.PageRequestManager._initialize('ctl00$ScriptManager', 'aspnetForm', ['fctl00$ctl42$WebPartAdderUpdatePanel', ''], [], ['ctl00$ctl42$WebPartAdder', ''], 90, 'ctl00');
//]]>
    </script>
    <div>
        <div id="TurnOnAccessibility" style="display: none" class="s4-notdlg noindex">
            <a id="linkTurnOnAcc" href="#" class="ms-accessible ms-acc-button" onclick="SetIsAccessibilityFeatureEnabled(true);UpdateAccessibilityUI();document.getElementById('linkTurnOffAcc').focus();return false;">
                Turn on more accessible mode </a>
        </div>
        <div id="TurnOffAccessibility" style="display: none" class="s4-notdlg noindex">
            <a id="linkTurnOffAcc" href="#" class="ms-accessible ms-acc-button" onclick="SetIsAccessibilityFeatureEnabled(false);UpdateAccessibilityUI();document.getElementById('linkTurnOnAcc').focus();return false;">
                Turn off more accessible mode </a>
        </div>
    </div>
    <div id="ms-designer-ribbon">
        <div>
            <div id="suiteBar" class="ms-dialogHidden noindex">
                <div id="suiteBarLeft">
                    <div class="ms-table ms-fullWidth">
                        <div class="ms-tableRow">
                            <div class="ms-tableCell ms-verticalAlignMiddle">
                                <div class="ms-core-brandingText">
                                    SharePoint</div>
                            </div>
                            <div id="DeltaSuiteLinks" class="ms-core-deltaSuiteLinks">
                                <div id="suiteLinksBox">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="suiteBarRight">
                    <div id="DeltaSuiteBarRight" class="ms-core-deltaSuiteBarRight">
                        <div id="welcomeMenuBox">
                            <span style="display: none">
                                <menu type='ServerMenu' id="zz1_ID_PersonalActionMenu" hideicons="true">
                                    <ie:menuitem id="zz2_ID_PersonalInformation" type="option" iconsrc="/_layouts/15/images/menuprofile.gif?rev=23"
                                        onmenuclick="javascript:GoToPage(&#39;\u002fcampervans\u002f_layouts\u002f15\u002fuserdisp.aspx?Force=True&#39;);return false;"
                                        text="My Settings" description="Update your user information, language and regional settings, and alerts."
                                        menugroupid="100"></ie:menuitem>
                                    <ie:menuitem id="zz3_ID_Logout" type="option" onmenuclick="STSNavigate2(event,&#39;/campervans/_layouts/15/SignOut.aspx&#39;);"
                                        text="Sign Out" description="Logout of this site." menugroupid="100"></ie:menuitem>
                                    <ie:menuitem id="zz4_ID_PersonalizePage" type="option" iconsrc="/_layouts/15/images/menupersonalize.gif?rev=23"
                                        onmenuclick="javascript:ChangeLayoutMode(true);" text="Personalize this Page"
                                        description="Add, remove, or update Web Parts on this page." menugroupid="200"></ie:menuitem>
                                </menu>
                            </span><span id="zz5_Menu_t" class="ms-menu-althov ms-welcome-root" title="Open Menu"
                                onmouseover="MMU_PopMenuIfShowing(this);MMU_EcbTableMouseOverOut(this, true)"
                                hoveractive="ms-menu-althov-active ms-welcome-root ms-welcome-hover" hoverinactive="ms-menu-althov ms-welcome-root"
                                onclick=" CoreInvoke('MMU_Open',byid('zz1_ID_PersonalActionMenu'), MMU_GetMenuFromClientId('zz5_Menu'),event,true, null, 0); return false;"
                                foa="MMU_GetMenuFromClientId('zz5_Menu')" oncontextmenu="ClkElmt(this); return false;"
                                style="white-space: nowrap"><a class="ms-core-menu-root" id="zz5_Menu" accesskey="/"
                                    href="javascript:;" title="Open Menu" onfocus="MMU_EcbLinkOnFocusBlur(byid('zz1_ID_PersonalActionMenu'), this, true);"
                                    onkeydown="MMU_EcbLinkOnKeyDown(byid('zz1_ID_PersonalActionMenu'), MMU_GetMenuFromClientId('zz5_Menu'), event);"
                                    onclick=" CoreInvoke('MMU_Open',byid('zz1_ID_PersonalActionMenu'), MMU_GetMenuFromClientId('zz5_Menu'),event,true, null, 0); return false;"
                                    oncontextmenu="ClkElmt(this); return false;" menutokenvalues="MENUCLIENTID=zz5_Menu,TEMPLATECLIENTID=zz1_ID_PersonalActionMenu"
                                    serverclientid="zz5_Menu">Content Editor<span class='ms-accessible'>Use SHIFT+ENTER
                                        to open the menu (new window).</span></a><span style="height: 4px; width: 7px; position: relative;
                                            display: inline-block; overflow: hidden;" class="s4-clust ms-viewselector-arrow ms-menu-stdarw ms-core-menu-arrow">
                                            <img src="/_layouts/15/images/spcommon.png?rev=23" alt="Open Menu" style="position: absolute;
                                                left: -95px !important; top: -259px !important;" />
                                        </span><span style="height: 4px; width: 7px; position: relative; display: inline-block;
                                            overflow: hidden;" class="s4-clust ms-core-menu-arrow ms-viewselector-arrow ms-menu-hovarw">
                                            <img src="/_layouts/15/images/spcommon.png?rev=23" alt="Open Menu" style="position: absolute;
                                                left: -86px !important; top: -259px !important;" />
                                        </span></span>
                        </div>
                        <div id="suiteBarButtons">
                            <span class="ms-siteactions-root" id="siteactiontd"><span style="display: none">
                                <menu type='ServerMenu' id="zz6_SiteActionsMenuMain" hideicons="true">
                                    <ie:menuitem id="ctl00_ctl42_SiteActionsMenuMain_ctl00_wsaShowMenu" type="option"
                                        iconsrc="/_layouts/15/images/RibbonShowHH.png?rev=23" onmenuclick="javascript:__doPostBack(&#39;ctl00$ctl42$SiteActionsMenuMain$ctl00$wsaShowMenu_CmsActionControl&#39;,&#39;reviewPage&#39;)"
                                        text="Show Ribbon" description="Display the page status and ribbon for this page."
                                        menugroupid="100"></ie:menuitem>
                                    <ie:menuitem id="zz7_MenuItem_ShareThisSite" type="option" onmenuclick="EnsureScriptFunc(&#39;sharing.js&#39;, &#39;DisplaySharedWithDialog&#39;, function () { DisplaySharedWithDialog(&#39;\u002fcampervans\u002f&#39;); })"
                                        text="Shared with..." description="See who&#39;s here and invite new people."
                                        menugroupid="100"></ie:menuitem>
                                    <ie:menuitem id="ctl00_ctl42_SiteActionsMenuMain_ctl00_wsaEditPage" type="option"
                                        iconsrc="/_layouts/15/images/ActionsEditPage.gif?rev=23" onmenuclick="javascript:if (document.forms[&#39;aspnetForm&#39;][&#39;MSOLayout_InDesignMode&#39;] != null) document.forms[&#39;aspnetForm&#39;][&#39;MSOLayout_InDesignMode&#39;].value = 1;if (document.forms[&#39;aspnetForm&#39;][&#39;MSOAuthoringConsole_FormContext&#39;] != null) document.forms[&#39;aspnetForm&#39;][&#39;MSOAuthoringConsole_FormContext&#39;].value = 1;if (document.forms[&#39;aspnetForm&#39;][&#39;MSOSPWebPartManager_DisplayModeName&#39;] != null) document.forms[&#39;aspnetForm&#39;][&#39;MSOSPWebPartManager_DisplayModeName&#39;].value = &#39;Design&#39;;__doPostBack(&#39;ctl05&#39;,&#39;edit&#39;)"
                                        text="Edit page" description="Change the content and Web Parts on this page."
                                        menugroupid="200"></ie:menuitem>
                                    <ie:menuitem id="ctl00_ctl42_SiteActionsMenuMain_ctl00_wsaCreatePage" type="option"
                                        iconsrc="/_layouts/15/images/crtpage.gif?rev=23" onmenuclick="javascript:if (LaunchCreateHandler(&#39;PublishingPage&#39;)) { SP.SOD.executeFunc(&#39;sp.ui.dialog.js&#39;, &#39;SP.UI.ModalDialog.showModalDialog&#39;, function() { var dlgOptions = { url:&#39;\u002fcampervans\u002f_layouts\u002f15\u002fCreatePublishingPageDialog.aspx&#39;, autoSize: true, autoSizeStartWidth: 550 };  SP.UI.ModalDialog.showModalDialog(dlgOptions); }); };"
                                        text="Add a page" description="Create a page in this site." menugroupid="200"></ie:menuitem>
                                    <ie:menuitem id="zz8_MenuItem_Create" type="option" onmenuclick="GoToPage(&#39;\u002fcampervans\u002f_layouts\u002f15\u002faddanapp.aspx&#39;)"
                                        text="Add an app" description="Create other types of pages, lists, libraries, and sites."
                                        menugroupid="200"></ie:menuitem>
                                    <ie:menuitem id="zz9_MenuItem_ViewAllSiteContents" type="option" iconsrc="/_layouts/15/images/allcontent32.png?rev=23"
                                        onmenuclick="STSNavigate2(event,&#39;/campervans/_layouts/15/viewlsts.aspx&#39;);"
                                        text="Site contents" description="View all libraries and lists in this site."
                                        menugroupid="200"></ie:menuitem>
                                    <ie:menuitem id="ctl00_ctl42_SiteActionsMenuMain_ctl00_wsaDesignEditor" type="option"
                                        iconsrc="/_layouts/15/images/designer.png?rev=23" onmenuclick="STSNavigate2(event,&#39;/_layouts/15/DesignSite.aspx&#39;);"
                                        text="Design Manager" description="Change the look and feel of this site." menugroupid="300"></ie:menuitem>
                                    <ie:menuitem id="zz10_MenuItem_Settings" type="option" iconsrc="/_layouts/15/images/settingsIcon.png?rev=23"
                                        onmenuclick="GoToPage(&#39;\u002fcampervans\u002f_layouts\u002f15\u002fsettings.aspx&#39;)"
                                        text="Site settings" description="Access all settings for this site." menugroupid="300"></ie:menuitem>
                                </menu>
                            </span><span id="zz11_SiteActionsMenu_t" class="ms-siteactions-normal" title="Settings"
                                onmouseover="MMU_PopMenuIfShowing(this);MMU_EcbTableMouseOverOut(this, true)"
                                hoveractive="ms-siteactions-normal ms-siteactions-hover" hoverinactive="ms-siteactions-normal"
                                onclick=" CoreInvoke('MMU_Open',byid('zz6_SiteActionsMenuMain'), MMU_GetMenuFromClientId('zz11_SiteActionsMenu'),event,true, null, 0); return false;"
                                foa="MMU_GetMenuFromClientId('zz11_SiteActionsMenu')" oncontextmenu="ClkElmt(this); return false;">
                                <a class="ms-core-menu-root" id="zz11_SiteActionsMenu" accesskey="/" href="javascript:;"
                                    title="Settings" onkeydown="MMU_EcbLinkOnKeyDown(byid('zz6_SiteActionsMenuMain'), MMU_GetMenuFromClientId('zz11_SiteActionsMenu'));"
                                    menutokenvalues="MENUCLIENTID=zz11_SiteActionsMenu,TEMPLATECLIENTID=zz6_SiteActionsMenuMain"
                                    serverclientid="zz11_SiteActionsMenu"><span class="ms-siteactions-imgspan">
                                        <img class="ms-core-menu-buttonIcon" src="/_layouts/15/images/spcommon.png?rev=23"
                                            alt="Settings" title="Settings" /></span><span class='ms-accessible'>Use SHIFT+ENTER
                                                to open the menu (new window).</span></a></span></span> <span id="ms-help"><a onmouseover="this.firstChild.firstChild.firstChild.style.left=&#39;-1px&#39;; this.firstChild.firstChild.firstChild.style.top=&#39;-1px&#39;;"
                                                    onmouseout="this.firstChild.firstChild.firstChild.style.left=&#39;-19px&#39;; this.firstChild.firstChild.firstChild.style.top=&#39;-1px&#39;;"
                                                    id="ctl00_ctl42_TopHelpLink" accesskey="6" title="Help" onclick="TopHelpButtonClick(&#39;HelpHome&#39;,event);return false"
                                                    href="../../_controltemplates/15/#" style="display: inline-block; height: 30px;
                                                    width: 30px;"><span style="display: inline-block; overflow: hidden; height: 16px;
                                                        width: 16px; padding-left: 7px; padding-top: 7px; padding-right: 7px; padding-bottom: 7px;">
                                                        <span style="height: 16px; width: 16px; position: relative; display: inline-block;
                                                            overflow: hidden;" class="s4-clust">
                                                            <img src="/_layouts/15/1033/images/spintl.png?rev=23" alt="Help" style="border: 0;
                                                                position: absolute; left: -19px; top: -1px;" />
                                                        </span></span></a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="s4-ribbonrow">
                <div id="globalNavBox">
                    <div id="ribbonBox">
                        <div id="s4-ribboncont">
                            <div id="DeltaSPRibbon">
                                <div class='ms-cui-ribbonTopBars'>
                                    <div class='ms-cui-topBar1'>
                                    </div>
                                    <div class='ms-cui-topBar2'>
                                        <div id='RibbonContainer-TabRowLeft' class='ms-cui-TabRowLeft ms-core-defaultFont ms-dialogHidden'>
                                        </div>
                                        <div id='RibbonContainer-TabRowRight' class='ms-cui-TabRowRight s4-trc-container s4-notdlg ms-core-defaultFont'>
                                            <a onmouseover="this.firstChild.firstChild.style.left=&#39;-91px&#39;; this.firstChild.firstChild.style.top=&#39;-232px&#39;;"
                                                onmouseout="this.firstChild.firstChild.style.left=&#39;-200px&#39;; this.firstChild.firstChild.style.top=&#39;-48px&#39;;"
                                                onclick="EnsureScriptFunc(&#39;sharing.js&#39;, &#39;DisplaySharingDialog&#39;, function(){DisplaySharingDialog(&#39;\u002fcampervans&#39;)}); return false;"
                                                id="ctl00_ctl42_site_share_button" title="Give people access to this site." class="ms-promotedActionButton"
                                                href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl42$site_share_button&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, true))"
                                                style="display: inline-block;"><span style="height: 16px; width: 16px; position: relative;
                                                    display: inline-block; overflow: hidden;" class="s4-clust ms-promotedActionButton-icon">
                                                    <img src="/_layouts/15/images/spcommon.png?rev=23" alt="Share" style="position: absolute;
                                                        left: -200px; top: -48px;" />
                                                </span><span class="ms-promotedActionButton-text">Share</span></a> <a onmouseover="this.firstChild.firstChild.style.left=&#39;-217px&#39;; this.firstChild.firstChild.style.top=&#39;-192px&#39;;"
                                                    onmouseout="this.firstChild.firstChild.style.left=&#39;-218px&#39;; this.firstChild.firstChild.style.top=&#39;-48px&#39;;"
                                                    onclick="SP.SOD.executeFunc(&#39;followingcommon.js&#39;, &#39;FollowSite&#39;, function() { FollowSite(); }); return false;"
                                                    id="site_follow_button" title="Follow this site and get back to it easily from your sites page."
                                                    class="ms-promotedActionButton" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl42$site_follow_button&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, true))"
                                                    style="display: inline-block;"><span style="height: 16px; width: 16px; position: relative;
                                                        display: inline-block; overflow: hidden;" class="s4-clust ms-promotedActionButton-icon">
                                                        <img src="/_layouts/15/images/spcommon.png?rev=23" alt="Follow" style="position: absolute;
                                                            left: -218px; top: -48px;" />
                                                    </span><span class="ms-promotedActionButton-text">Follow</span></a> <span id="fullscreenmodebox"
                                                        class="ms-qatbutton"><span id="fullscreenmode"><a onmouseover="this.firstChild.firstChild.firstChild.style.left=&#39;-125px&#39;; this.firstChild.firstChild.firstChild.style.top=&#39;-178px&#39;;"
                                                            onmouseout="this.firstChild.firstChild.firstChild.style.left=&#39;-143px&#39;; this.firstChild.firstChild.firstChild.style.top=&#39;-178px&#39;;"
                                                            id="ctl00_ctl42_fullscreenmodeBtn" title="Focus on Content" onclick="SetFullScreenMode(true);PreventDefaultNavigation();return false;"
                                                            href="../../_controltemplates/15/#" style="display: inline-block; height: 30px;
                                                            width: 30px;"><span style="display: inline-block; overflow: hidden; height: 16px;
                                                                width: 16px; padding-left: 7px; padding-top: 7px; padding-right: 7px; padding-bottom: 7px;">
                                                                <span style="height: 16px; width: 16px; position: relative; display: inline-block;
                                                                    overflow: hidden;" class="s4-clust">
                                                                    <img src="/_layouts/15/images/spcommon.png?rev=23" alt="Focus on Content" style="border: 0;
                                                                        position: absolute; left: -143px; top: -178px;" />
                                                                </span></span></a></span><span id="exitfullscreenmode" style="display: none;"><a
                                                                    onmouseover="this.firstChild.firstChild.firstChild.style.left=&#39;-179px&#39;; this.firstChild.firstChild.firstChild.style.top=&#39;-96px&#39;;"
                                                                    onmouseout="this.firstChild.firstChild.firstChild.style.left=&#39;-107px&#39;; this.firstChild.firstChild.firstChild.style.top=&#39;-178px&#39;;"
                                                                    id="ctl00_ctl42_exitfullscreenmodeBtn" title="Focus on Content" onclick="SetFullScreenMode(false);PreventDefaultNavigation();return false;"
                                                                    href="../../_controltemplates/15/#" style="display: inline-block; height: 30px;
                                                                    width: 30px;"><span style="display: inline-block; overflow: hidden; height: 16px;
                                                                        width: 16px; padding-left: 7px; padding-top: 7px; padding-right: 7px; padding-bottom: 7px;">
                                                                        <span style="height: 16px; width: 16px; position: relative; display: inline-block;
                                                                            overflow: hidden;" class="s4-clust">
                                                                            <img src="/_layouts/15/images/spcommon.png?rev=23" alt="Focus on Content" style="border: 0;
                                                                                position: absolute; left: -107px; top: -178px;" />
                                                                        </span></span></a></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span id="DeltaSPNavigation"><span id="ctl00_ctl42_ctl11_publishingRibbon"></span>
                        </span>
                    </div>
                    <div id="DeltaWebPartAdderUpdatePanelContainer" class="ms-core-webpartadder">
                        <div id="WebPartAdderUpdatePanelContainer">
                            <div id="ctl00_ctl42_WebPartAdderUpdatePanel">
                                <span id="ctl00_ctl42_WebPartAdder"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="notificationArea" class="ms-notif-box">
            </div>
            <div id="DeltaPageStatusBar">
                <div id="pageStatusBar">
                </div>
            </div>
        </div>
    </div>
    <div id="s4-workspace">
        <div id="s4-bodyContainer">
            <!-- ############### CROSS SELL STARTS ###############-->
            <div class="crossSell">
                <ul id="crossSellList">
                    <li class="left"><span>RENTALS Australia </span><i class="icon-crossSell-nodrop"></i>
                        <ul class="crossSellBrands">
                            <li><a href="#" class="kea">Kea </a></li>
                            <li><a href="#" class="maui">Maui Australia </a></li>
                            <li><a href="#" class="britz current">Britz Campervan. 4WD. Car Rentals. </a></li>
                            <li><a href="#" class="mighty">Mighty Cars &amp; Campers </a></li>
                            <!--<li class="close">close <i class="icon-remove-circle"></i></li>-->
                        </ul>
                    </li>
                    <li class="right"><a href="#"><span>BUY a Campervan </span>// Motek <i class="icon-tags icon-white">
                    </i></a></li>
                    <li class="brands right"><span>RENTALS USA </span><i class="icon-crossSell"></i>
                        <ul class="crossSellBrands down">
                            <li><a href="#" class="roadbear">Roadbear RV Rentals &amp; Sales </a></li>
                            <li><a href="#" class="britz">Britz Campervan. 4WD. Car Rentals. </a></li>
                            <li class="close">close <i class="icon-remove-circle"></i></li>
                        </ul>
                    </li>
                    <li class="brands right"><span>RENTALS New Zealand </span><i class="icon-crossSell">
                    </i>
                        <ul class="crossSellBrands down">
                            <li><a href="#" class="kea">Kea </a></li>
                            <li><a href="#" class="maui">Maui New Zealand </a></li>
                            <li><a href="#" class="united">United Campervans </a></li>
                            <li><a href="#" class="britz">Britz Campervan. 4WD. Car Rentals. </a></li>
                            <li><a href="#" class="alpha">Alpha Campervans </a></li>
                            <li><a href="#" class="mighty">Mighty Cars &amp; Campers </a></li>
                            <li class="close">close <i class="icon-remove-circle"></i></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- ############### CROSS SELL ENDS ###############-->
            <!-- ############### CONTACT BAR STARTS ###############-->
            <div class="contactBar">
                <div class="contactBarCentre">
                    <div class="contactBarCountry">
                        Australia
                    </div>
                    <div class="contactBarContainer">
                        <div class="contactBarLink contactBarLinkLast" id="Flags">
                            <img src="/SiteCollectionImages/Core/English.png" alt="English" />
                            <div class="changeLang">
                                <a href="#">
                                    <img src="/SiteCollectionImages/Core/French.jpg" alt="Autocar &amp; Voiture de Location Nouvelle-Zélande" />
                                </a><a href="#" title="wohnmobil, mietwagen, autovermietung Neuseeland">
                                    <img src="/SiteCollectionImages/Core/German.jpg" alt="wohnmobil, mietwagen, autovermietung Neuseeland" />
                                </a><a href="#" title="Kampeerauto &amp; Autoverhuur Nieuw-Zeeland">
                                    <img src="/SiteCollectionImages/Core/dutch.jpg" alt="Kampeerauto &amp; Autoverhuur Nieuw-Zeeland" />
                                </a><a href="#" title="Kampeerauto &amp; Autoverhuur Australië">
                                    <img src="/SiteCollectionImages/Core/Chinese.jpg" alt="Britz New Zealand" />
                                </a>
                            </div>
                        </div>
                        <a href="#" class="contactBarLink">
                            <img src="/SiteCollectionImages/Core/contactBar-FB.png" alt="Facebook" />
                        </a><a href="#" class="contactBarLink">
                            <img src="/SiteCollectionImages/Core/contactBar-Twitter.png" alt="Twitter" />
                        </a><a href="#" class="contactBarTitle"><strong>Contact us </strong>- International
                            Toll Free </a>
                    </div>
                </div>
            </div>
            <!-- ############### CONTACT BAR ENDS ###############-->
            <div class="container">
                <!-- ############### HEADER STARTS ###############-->
                <div class="headerPages">
                    <div class="logo">
                        <a href="/">
                            <img src="/SiteCollectionImages/Core/logo.png" alt="Britz Campervan Hire Asutralia" />
                        </a>
                    </div>
                </div>
                <!-- ############### HEADER ENDS ###############-->
                <!-- ############### BOOKING CONTROL STARTS ###############-->
                <div class="bookingControl clearfix" id="bookingcontrol">
                    <div class="bookingControlInner">
                        <div id="bcPin">
                            <img src="/SiteCollectionImages/Core/ping.png" width="25" height="34" alt="Pin" />
                        </div>
                        <!-- Tabs -->
                        <div id="bookingtabs">
                            <div class="searchCampervans TabSelected">
                                SEARCH CAMPERVANS
                            </div>
                            <div class="searchCars">
                                SEARCH CARS
                            </div>
                            <div class="viewprice">
                                view price &amp; availability
                            </div>
                            <a href="" class="checkinOnline">CHECK-IN ONLINE <i class="icon-checkin"></i></a>
                        </div>
                        <!-- form -->
                        <div class="bookingControlForm">
                            <!--<div class="openform"></div>-->
                            <div class="content">
                                <div class="formfields">
                                    <div class="row1">
                                        <select class="styled">
                                            <option>Where would you like to pick up from? </option>
                                            <option>Brisbane </option>
                                            <option>Sydney </option>
                                            <option>Cairns </option>
                                            <option>Perth </option>
                                        </select>
                                        <span class="calander"><span class="calanderInner">Choose what date? </span></span>
                                        <select class="styled2">
                                            <option>10:00am </option>
                                            <option>11:00am </option>
                                            <option>12:00pm </option>
                                            <option>13:00pm </option>
                                            <option>14:00pm </option>
                                        </select>
                                    </div>
                                    <div class="row2">
                                        <select class="styled">
                                            <option>Same as pick up? </option>
                                            <option>Brisbane </option>
                                            <option>Sydney </option>
                                            <option>Cairns </option>
                                            <option>Perth </option>
                                        </select>
                                        <span class="calander"><span class="calanderInner">Choose what date? </span></span>
                                        <select class="styled2">
                                            <option>10:00am </option>
                                            <option>11:00am </option>
                                            <option>12:00pm </option>
                                            <option>13:00pm </option>
                                            <option>14:00pm </option>
                                        </select>
                                    </div>
                                    <div class="row3">
                                        <select class="styled3">
                                            <option>All models or select </option>
                                            <option>HiTop </option>
                                            <option>Voyager </option>
                                            <option>Explorer </option>
                                            <option>Renegade </option>
                                        </select>
                                        <select class="styled4">
                                            <option>Drivers Licence </option>
                                            <option>New Zealand </option>
                                            <option>Australia </option>
                                            <option>The Netherlands </option>
                                            <option>Israel </option>
                                        </select>
                                        <select class="styled5">
                                            <option>Number of Adults </option>
                                            <option>1 </option>
                                            <option>2 </option>
                                            <option>3 </option>
                                            <option>4 </option>
                                            <option>5 </option>
                                            <option>6 </option>
                                        </select>
                                        <select class="styled2">
                                            <option>and Children? </option>
                                            <option>1 </option>
                                            <option>2 </option>
                                            <option>3 </option>
                                            <option>4 </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="StartSearch">
                                    <span class="line"></span><span class="StartExpandClose"><span class="StartSearchBtn">
                                        Start Your Search </span></span>
                                </div>
                            </div>
                            <!-- texture bgs -->
                            <span class="topleft"></span><span class="topright"></span><span class="bottomleft">
                            </span><span class="bottomright"></span>
                        </div>
                        <span class="contentbottom"></span>
                        <div class="ExpandClose">
                            <span>Expand </span><i class="icon-expandclose"></i>
                        </div>
                    </div>
                </div>
                <!-- ############### BOOKING CONTROL ENDS ###############-->
                <!-- ############### HOT DEALS STARTS ###############-->
                <div class="hotDealsPages clearfix">
                    <div id="AdspacePages">
                        <div class="ms-webpart-chrome ms-webpart-chrome-fullWidth ">
                            <div webpartid="00000000-0000-0000-0000-000000000000" haspers="true" id="WebPartWPQ1"
                                width="100%" class="ms-WPBody " onlyformepart="true" allowdelete="false" style="">
                                <div class="ad-carousel">
                                    <ul>
                                        <li><a href="http://dev.britz.com.au/promotions/Pages/Britz-Early-Bird.aspx" class="adBox balls">
                                            <span class="text"><span class="adTitle">Early Bird Savings</span><span class="bookbefore">Book
                                                120 Days in Advance</span><span class="pricepoint">Save 5%</span><span class="findoutmore">Find
                                                    out more </span></span><span class="image">
                                                        <img src="/SiteCollectionImages/core/earlybird.jpg?renditionid=12" /></span></a></li><li>
                                                            <a href="http://dev.britz.com.au/promotions/Pages/Britz-Hot-Everyday-Rates.aspx"
                                                                class="adBox balls"><span class="text"><span class="adTitle">Hot Everyday Rates</span><span
                                                                    class="bookbefore">We won't be beaten on price</span><span class="pricepoint">Hot Rates</span><span
                                                                        class="findoutmore">Find out more </span></span><span class="image">
                                                                            <img src="/SiteCollectionImages/Core/britztest.jpg?renditionid=12" /></span></a></li><li>
                                                                                <a href="http://dev.britz.com.au/promotions/Pages/wildflower-adventure.aspx" class="adBox balls">
                                                                                    <span class="text"><span class="adTitle">Wildflower Adventure</span><span class="bookbefore">Book
                                                                                        Before: 24 September </span><span class="pricepoint"><span class="small">Up To</span>40%
                                                                                            Off</span><span class="findoutmore">Find out more </span></span><span class="image">
                                                                                                <img src="/SiteCollectionImages/promotions/bathurst.jpg?renditionid=12" /></span></a></li><li>
                                                                                                    <a href="http://dev.britz.com.au/promotions/Pages/bathurst.aspx" class="adBox balls">
                                                                                                        <span class="text"><span class="adTitle">Britz for Bathurst</span><span class="bookbefore">Get
                                                                                                            in Quick these sell fast</span><span class="pricepoint">Bathurst '13</span><span
                                                                                                                class="findoutmore">Find out more </span></span><span class="image">
                                                                                                                    <img src="/SiteCollectionImages/promotions/bathurst.jpg?renditionid=12" /></span></a></li></ul>
                                </div>
                                <div class="ms-clear">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ############### HOT DEALS ENDS ###############-->
                <!-- ############### MAIN CONTENT STARTS ###############-->
                <div class="mainContent clearfix">
                    <div class="mainContentMenu">
                        <ul>
                            <li><a href="" class="mainContentMenuDeals"><span class="mainContentMenuDealsL1">HOT
                                AUSTRALIAN </span><span class="mainContentMenuDealsL2">TRAVEL DEALS </span></a>
                            </li>
                        </ul>
                        <div data-name="QuickLaunch">
                            <div id="sideNavBox" class="ms-dialogHidden ms-forceWrap ms-noList">
                                <div id="DeltaPlaceHolderLeftNavBar" class="ms-core-navigation">
                                    <div id="ctl00_PlaceHolderLeftNavBar_QuickLaunchNavigationManager">
                                        <div id="zz12_V4QuickLaunchMenu" class=" noindex ms-core-listMenu-verticalBox">
                                            <ul id="zz13_RootAspMenu" class="root ms-core-listMenu-root static">
                                                <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                    tabindex="0" href="/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                        <span class="menu-item-text">Home</span></span></a></li><li class="static selected">
                                                            <a class="static selected menu-item ms-core-listMenu-item ms-displayInline ms-core-listMenu-selected ms-navedit-linkNode"
                                                                tabindex="0" href="/campervans/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                    <span class="menu-item-text">Campervans</span><span class="ms-hidden">Currently selected</span></span></a><ul
                                                                        class="static">
                                                                        <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                            tabindex="0" href="/campervans/Pages/hi-top.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                <span class="menu-item-text">hi-top</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                                    tabindex="0" href="/campervans/Pages/Voyager.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                        <span class="menu-item-text">Voyager</span></span></a></li><li class="static"><a
                                                                                            class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                                            tabindex="0" href="/campervans/Pages/venturer.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                                <span class="menu-item-text">venturer</span></span></a></li><li class="static"><a
                                                                                                    class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                                                    tabindex="0" href="/campervans/Pages/explorer.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                                        <span class="menu-item-text">explorer</span></span></a></li><li class="static"><a
                                                                                                            class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                                                            tabindex="0" href="/campervans/Pages/Extra-Hire-Items.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                                                <span class="menu-item-text">Extra Hire Items</span></span></a></li><li class="static">
                                                                                                                    <a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                                                                        tabindex="0" href="/campervans/Pages/Frequently-Asked-Questions.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                                                            <span class="menu-item-text">Frequently Asked Questions</span></span></a></li>
                                                                    </ul>
                                                        </li>
                                                <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                    tabindex="0" href="/Cars/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                        <span class="menu-item-text">Car Hire</span></span></a><ul class="static">
                                                            <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                tabindex="0" href="/Cars/Pages/Economy-Car.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                    <span class="menu-item-text">Economy Car</span></span></a></li><li class="static"><a
                                                                        class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                        tabindex="0" href="/Cars/Pages/Full-Size.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                            <span class="menu-item-text">Full Size</span></span></a></li><li class="static"><a
                                                                                class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                                tabindex="0" href="/Cars/Pages/4WD.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                    <span class="menu-item-text">4WD</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                                        tabindex="0" href="/Cars/Pages/Compact-Car.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                            <span class="menu-item-text">Compact Car</span></span></a></li><li class="static"><a
                                                                                                class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                                                tabindex="0" href="/Cars/Pages/Accident-Liability.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                                    <span class="menu-item-text">Accident Liability</span></span></a></li><li class="static">
                                                                                                        <a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                                                            tabindex="0" href="/Cars/Pages/Terms-and-Conditions.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                                                <span class="menu-item-text">Terms and Conditions</span></span></a></li><li class="static">
                                                                                                                    <a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                                                                        tabindex="0" href="/Cars/Pages/FAQs.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                                                            <span class="menu-item-text">FAQs</span></span></a></li>
                                                        </ul>
                                                </li>
                                                <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                    tabindex="0" href="/Locations/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                        <span class="menu-item-text">Branch Locations</span></span></a><ul class="static">
                                                            <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                tabindex="0" href="/Locations/Pages/Adelaide.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                    <span class="menu-item-text">Adelaide</span></span></a></li>
                                                        </ul>
                                                </li>
                                                <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                    tabindex="0" href="/promotions/Pages/HotDealsLanding.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                        <span class="menu-item-text">Promotions</span></span></a><ul class="static">
                                                            <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                tabindex="0" href="/promotions/Pages/bathurst.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                    <span class="menu-item-text">Bathurst</span></span></a></li><li class="static"><a
                                                                        class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                        tabindex="0" href="/promotions/Pages/wildflower-adventure.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                            <span class="menu-item-text">wildflower-adventure</span></span></a></li><li class="static">
                                                                                <a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                                    tabindex="0" href="/promotions/Pages/Britz-Hot-Everyday-Rates.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                        <span class="menu-item-text">Britz Hot Everyday Rates</span></span></a></li><li class="static">
                                                                                            <a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                                                tabindex="0" href="/promotions/Pages/Britz-Early-Bird.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                                    <span class="menu-item-text">Britz Early Bird</span></span></a></li>
                                                        </ul>
                                                </li>
                                                <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                    tabindex="0" href="/aboutus/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                        <span class="menu-item-text">About Us</span></span></a><ul class="static">
                                                            <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                tabindex="0" href="/aboutus/Pages/contactus.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                    <span class="menu-item-text">contactus</span></span></a></li><li class="static"><a
                                                                        class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                        tabindex="0" href="/aboutus/Pages/sitemap.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                            <span class="menu-item-text">sitemap</span></span></a></li><li class="static"><a
                                                                                class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                                tabindex="0" href="/aboutus/Pages/SearchResults.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                    <span class="menu-item-text">Search Results</span></span></a></li>
                                                        </ul>
                                                </li>
                                                <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                    tabindex="0" href="/popups/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                        <span class="menu-item-text">Popups</span></span></a><ul class="static">
                                                            <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                tabindex="0" href="/popups/Pages/Accident-Liability-Reduction.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                    <span class="menu-item-text">Accident Liability Reduction</span></span></a></li><li
                                                                        class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                            tabindex="0" href="/popups/Pages/HiTop.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                <span class="menu-item-text">HiTop</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                                    tabindex="0" href="/popups/Pages/facebook-iframe.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                        <span class="menu-item-text">facebook iframe</span></span></a></li>
                                                        </ul>
                                                </li>
                                                <li class="static"><span class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                    tabindex="0"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">
                                                        Recent</span></span></span><ul class="static">
                                                            <li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                tabindex="0" href="/Lists/Promotions/AllItems.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                    <span class="menu-item-text">Ad Bottom</span></span></a></li><li class="static"><a
                                                                        class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                        tabindex="0" href="/Lists/RegEx/AllItems.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                            <span class="menu-item-text">RegEx</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                                tabindex="0" href="/Lists/Accident Liability/AllItems.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                    <span class="menu-item-text">Accident Liability</span></span></a></li><li class="static">
                                                                                        <a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                                            tabindex="0" href="/Lists/THL Vehicles/AllItems.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                                <span class="menu-item-text">THL Vehicles</span></span></a></li><li class="static"><a
                                                                                                    class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode"
                                                                                                    tabindex="0" href="/Lists/URLRedirection/AllItems.aspx"><span class="additional-background ms-navedit-flyoutArrow">
                                                                                                        <span class="menu-item-text">URLRedirection</span></span></a></li>
                                                        </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <hr />
                                    <a id="ctl00_PlaceHolderLeftNavBar_PlaceHolderQuickLaunchBottom_idNavLinkViewAllV4"
                                        accesskey="3" class="ms-core-listMenu-item" href="/campervans/{0}/viewlsts.aspx">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mainContentBlocks">
                        <!-- Start Page Layout (Below this) -->
                        <div class="contentContainer">
                            <div id="breadcrumbs">
                                <span sitemapproviders="SPSiteMapProvider,SPXmlContentMapProvider" hideinteriorrootnodes="true">
                                    <a href="#ctl00_ctl45_SkipLink">
                                        <img alt="Skip Navigation Links" src="/WebResource.axd?d=7T9G4ZoNvBSHC4dqlMiTqZPdBnWmuqn8vbM7qZ2Vr46QCKtaRYFyWMMnIdSYpRwLUeFqImey-qlT5Usogxdbysp67AM1ahiaimx3FPmTOp81&amp;t=634773866700000000"
                                            width="0" height="0" style="border-width: 0px;" /></a><span><a href="/Pages/default.aspx">Home</a></span><span>
                                                &gt; </span><span>Campervans</span><a id="ctl00_ctl45_SkipLink"></a></span>
                            </div>
                            <div data-name="ContentPlaceHolderMain">
                                <span id="DeltaPlaceHolderMain">
                                    <h1>
                                        <div data-name="Page Field: PageHeading">
                                            Campervans Australia
                                        </div>
                                    </h1>
                                    <div class="pageContent">
                                        <div data-name="Page Field: Page Content">
                                            <div id="ctl00_PlaceHolderMain_ctl01_label" style='display: none'>
                                                Page Content</div>
                                            <div id="ctl00_PlaceHolderMain_ctl01__ControlWrapper_RichHtmlField" class="ms-rtestate-field"
                                                style="display: inline" aria-labelledby="ctl00_PlaceHolderMain_ctl01_label">
                                                <div id="filters">
                                                    <strong>Filter by&#58;</strong>
                                                    <div class="option-combo sleeps">
                                                        <h4>
                                                            Sleeps</h4>
                                                        <ul class="filter option-set clearfix" data-filter-group="sleeps">
                                                            <li><a class="selected" href="#filter-sleeps-any">All</a> </li>
                                                            <li><a href="#filter-sleeps-two" data-filter-value=".sleeps2">2</a> </li>
                                                            <li><a href="#filter-sleeps-three" data-filter-value=".sleeps3">3</a> </li>
                                                            <li><a href="#filter-sleeps-four" data-filter-value=".sleeps4">4</a> </li>
                                                            <li><a href="#filter-sleeps-five" data-filter-value=".sleeps5">5</a> </li>
                                                            <li><a href="#filter-sleeps-six" data-filter-value=".sleeps6">6</a> </li>
                                                        </ul>
                                                    </div>
                                                    <div class="option-combo toiletshower">
                                                        <h4>
                                                            Toilet/Shower</h4>
                                                        <ul class="filter option-set clearfix" data-filter-group="toiletshower">
                                                            <li><a class="selected" href="#filter-toiletshower-any">All</a> </li>
                                                            <li><a href="#filter-toiletshower-yes" data-filter-value=".ToiletShower">Yes</a>
                                                            </li>
                                                            <li><a href="#filter-toiletshower-no" data-filter-value=".NoToiletShower">No</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="option-combo transmission">
                                                        <h4>
                                                            Transmission</h4>
                                                        <ul class="filter option-set clearfix " data-filter-group="transmission">
                                                            <li><a class=".selected" href="#filter-transmission-any">All</a> </li>
                                                            <li><a href="#filter-transmission-4wd" data-filter-value=".fwd">4WD</a> </li>
                                                            <li><a href="#filter-transmission-2wd" data-filter-value=".twd">2WD</a> </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <div class="pageFullwidth">
                                            <div class="content">
                                                <div data-name="WebPartZone">
                                                    <div>
                                                        <div class="ms-webpart-zone ms-fullWidth">
                                                            <div id="MSOZoneCell_WebPartWPQ4" class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
                                                                <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
                                                                    <div webpartid="428a7651-8ea2-4c53-932c-e511ad3c6f66" haspers="false" id="WebPartWPQ4"
                                                                        width="100%" class="ms-WPBody " allowdelete="false" style="">
                                                                        <div id="vehicles">
                                                                            <div class="vehicles sleeps3 twd ToiletShower">
                                                                                <div class="row">
                                                                                    <h3>
                                                                                        <a href="http://dev.britz.com.au/campervans/Pages/explorer.aspx">Explorer</a></h3>
                                                                                    <span class="sleeps">Sleeps <strong><span class="sleepsNum">3</span></strong></span></div>
                                                                                <div class="row">
                                                                                    <span class="vehImage"><a href="http://dev.britz.com.au/campervans/Pages/explorer.aspx">
                                                                                        <img src="/SiteCollectionImages/campervans/auavb.2BB/clearcut.png?RenditionID=15" /></a></span></div>
                                                                                <div class="row">
                                                                                    <div class="lengthMeter">
                                                                                        <span class="length Short"><span class="left">Length</span><span class="right">7.17-7.21m</span></span></div>
                                                                                </div>
                                                                                <div class="row desc">
                                                                                    <p>
                                                                                        Without a doubt, this home on wheels is the ultimate freedom machine. Mum, Dad &amp;
                                                                                        the kids ...</p>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <a href="https://secure.britz.com.au" class="getQuote">Get a Quote</a><a href="http://dev.britz.com.au/campervans/Pages/explorer.aspx"
                                                                                        class="moreInfo" target="" title="Read more about explorer">More Info</a></div>
                                                                            </div>
                                                                            <div class="vehicles sleeps2 twd NoToiletShower">
                                                                                <div class="row">
                                                                                    <h3>
                                                                                        <a href="http://dev.britz.com.au/campervans/Pages/venturer.aspx">Venturer</a></h3>
                                                                                    <span class="sleeps">Sleeps <strong><span class="sleepsNum">2</span></strong></span></div>
                                                                                <div class="row">
                                                                                    <span class="vehImage"><a href="http://dev.britz.com.au/campervans/Pages/venturer.aspx">
                                                                                        <img src="/SiteCollectionImages/campervans/auavb.2BB/clearcut.png?RenditionID=15" /></a></span></div>
                                                                                <div class="row">
                                                                                    <div class="lengthMeter">
                                                                                        <span class="length Long"><span class="left">Length</span><span class="right">7.00m</span></span></div>
                                                                                </div>
                                                                                <div class="row desc">
                                                                                    <p>
                                                                                        Its a freaken rad camper</p>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <a href="https://secure.britz.com.au" class="getQuote">Get a Quote</a><a href="http://dev.britz.com.au/campervans/Pages/venturer.aspx"
                                                                                        class="moreInfo" target="" title="Read more about venturer">More Info</a></div>
                                                                            </div>
                                                                            <div class="vehicles sleeps2 fwd NoToiletShower">
                                                                                <div class="row">
                                                                                    <h3>
                                                                                        <a href="http://dev.britz.com.au/campervans/Pages/Voyager.aspx">Voyager</a></h3>
                                                                                    <span class="sleeps">Sleeps <strong><span class="sleepsNum">2</span></strong></span></div>
                                                                                <div class="row">
                                                                                    <span class="vehImage"><a href="http://dev.britz.com.au/campervans/Pages/Voyager.aspx">
                                                                                        <img src="/SiteCollectionImages/campervans/auavb.2BB/clearcut.png?RenditionID=15" /></a></span></div>
                                                                                <div class="row">
                                                                                    <div class="lengthMeter">
                                                                                        <span class="length Medium"><span class="left">Length</span><span class="right">5.0m</span></span></div>
                                                                                </div>
                                                                                <div class="row desc">
                                                                                    <p>
                                                                                        Its another camper thats amazing!</p>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <a href="https://secure.britz.com.au" class="getQuote">Get a Quote</a><a href="http://dev.britz.com.au/campervans/Pages/Voyager.aspx"
                                                                                        class="moreInfo" target="" title="Read more about Voyager">More Info</a></div>
                                                                            </div>
                                                                            <div class="vehicles sleeps2 twd NoToiletShower">
                                                                                <div class="row">
                                                                                    <h3>
                                                                                        <a href="http://dev.britz.com.au/campervans/Pages/hi-top.aspx">HiTop</a></h3>
                                                                                    <span class="sleeps">Sleeps <strong><span class="sleepsNum">2</span></strong></span></div>
                                                                                <div class="row">
                                                                                    <span class="vehImage"><a href="http://dev.britz.com.au/campervans/Pages/hi-top.aspx">
                                                                                        <img src="/SiteCollectionImages/campervans/auavb.2bb/clearcut.png?RenditionID=15" /></a></span></div>
                                                                                <div class="row">
                                                                                    <div class="lengthMeter">
                                                                                        <span class="length Short"><span class="left">Length</span><span class="right">6</span></span></div>
                                                                                </div>
                                                                                <div class="row desc">
                                                                                    <p>
                                                                                        Yo whose the best camper?</p>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <a href="https://secure.britz.com.au" class="getQuote">Get a Quote</a><a href="http://dev.britz.com.au/campervans/Pages/hi-top.aspx"
                                                                                        class="moreInfo" target="" title="Read more about hi-top">More Info</a></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="ms-clear">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="ms-PartSpacingVertical">
                                                                </div>
                                                            </div>
                                                            <div id="MSOZoneCell_WebPartWPQ5" class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
                                                                <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
                                                                    <div webpartid="7f8940d0-284a-46f7-bd13-6fd85faaab54" haspers="false" id="WebPartWPQ5"
                                                                        width="100%" class="ms-WPBody " allowdelete="false" style="">
                                                                        <div class="ms-rtestate-field">
                                                                            <div class="conditionsList">
                                                                                <p>
                                                                                    <strong><em>Please note:</em></strong> These pictures and specifications are indicative
                                                                                    of the vehicle that will be supplied under your booking. Actual vehicles may vary
                                                                                    according to year of manufacture and availability but your vehicle will be suitable
                                                                                    for the required number of people and have similar specifications to those on this
                                                                                    website. Specific specifications cannot be requested. All measurements are approximate,
                                                                                    not guaranteed and specifications may change at any time without any prior notice.</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="ms-clear">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="ms-PartSpacingVertical">
                                                                </div>
                                                            </div>
                                                            <div id="MSOZoneCell_WebPartWPQ6" class="s4-wpcell-plain ms-webpartzone-cell ms-webpart-cell-vertical ms-fullWidth ">
                                                                <div class="ms-webpart-chrome ms-webpart-chrome-vertical ms-webpart-chrome-fullWidth ">
                                                                    <div webpartid="f18746c2-cb44-49bb-a6a5-0bfb195e75e9" haspers="false" id="WebPartWPQ6"
                                                                        width="100%" class="ms-WPBody " allowdelete="false" style="">
                                                                        <div class="conditionsList">
                                                                            <a class="veh_additionalinfo" href="http://dev.britz.com.au/campervans/Pages/Extra-Hire-Items.aspx">
                                                                                Extra Hire Items for your Campervan Hire</a><a class="veh_additionalinfo" href="http://dev.britz.com.au/campervans/Pages/Frequently-Asked-Questions.aspx">Frequently
                                                                                    Asked Questions</a></div>
                                                                        <div class="ms-clear">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div data-name="Page Field: PageContentBottom">
                                            <div id="ctl00_PlaceHolderMain_ctl02_label" style='display: none'>
                                                PageContentBottom</div>
                                            <div id="ctl00_PlaceHolderMain_ctl02__ControlWrapper_RichHtmlField" class="ms-rtestate-field"
                                                style="display: inline" aria-labelledby="ctl00_PlaceHolderMain_ctl02_label">
                                                <div class="conditionsList">
                                                    <p>
                                                        Like to see Australia at your leisure? To get close to nature, all you have to do
                                                        is step out of your Britz campervan and into the rainforest, ocean, outback or wherever
                                                        you decide to call home that day. A Britz campervan lets you go where you want to,
                                                        when you want to and how you want to.</p>
                                                    <p>
                                                        With ten campervan models and eight car types, the choice is yours! Britz is the
                                                        largest campervan operator in Australia!</p>
                                                    <p>
                                                        So, what are you waiting for? Get out there and Britz it!</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div data-name="EditModePanelShowInEdit">
                                    </div>
                                    <div style='display: none' id='hidZone'>
                                        <menu id="MSOMenu_WebPartMenu" class="ms-hide">
                                            <ie:menuitem title="Collapse this web part." id="MSOMenu_Minimize" onmenuclick="javascript:MSOLayout_MinimizeRestore(MenuWebPart)"
                                                text="Minimize" type="option">

	</ie:menuitem>
                                            <ie:menuitem title="Expand this web part." id="MSOMenu_Restore" onmenuclick="javascript:MSOLayout_MinimizeRestore(MenuWebPart)"
                                                text="Restore" type="option">

	</ie:menuitem>
                                            <ie:menuitem title="Close this Web Part. You can still find it under closed Web Parts section in the insert ribbon. These changes will apply to all users."
                                                id="MSOMenu_Close" onmenuclick="javascript:MSOLayout_RemoveWebPart(MenuWebPart)"
                                                text="Close" type="option">

	</ie:menuitem>
                                            <ie:menuitem title="Delete this Web Part from the page. These changes will apply to all users."
                                                id="MSOMenu_Delete" iconsrc="/_layouts/15/images/DelItem.gif" onmenuclick="if(confirm('You are about to permanently delete this Web Part. Are you sure you want to do this?')) {MSOWebPartPage_partDeleted = MenuWebPartID;MSOWebPartPage_MenuDoPostBack('ctl00$ctl34', MenuWebPartID + ';MSOMenu_Delete');}"
                                                text="Delete" type="option">

	</ie:menuitem>
                                            <ie:menuitem type="separator"></ie:menuitem>
                                            <ie:menuitem title="Change properties of this shared Web Part. These changes will apply to all users."
                                                id="MSOMenu_Edit" iconsrc="/_layouts/15/images/EditItem.gif" onmenuclick="javascript:MSOTlPn_ShowToolPane2Wrapper('Edit', 16, MenuWebPartID)"
                                                text="Edit Web Part" type="option">

	</ie:menuitem>
                                            <ie:menuitem title="Show connections options for this Web Part. These changes will apply to all users."
                                                id="MSOMenu_Connections" onmenuclick="" text="Connections" type="option">

	</ie:menuitem>
                                            <ie:menuitem type="separator"></ie:menuitem>
                                            <ie:menuitem title="Export this Web Part. These changes will apply to all users."
                                                id="MSOMenu_Export" onmenuclick="javascript:MSOWebPartPage_ExportCheckWarning(&#39;\u002fcampervans\u002f_vti_bin\u002fexportwp.aspx?pageurl=http\u00253A\u00252F\u00252Fdev\u00252Ebritz\u00252Ecom\u00252Eau\u00253A80\u00252Fcampervans\u00252FPages\u00252Fdefault\u00252Easpx\u0026guidstring=&#39;+ escape(MenuWebPartID), MenuWebPart.getAttribute(&#39;HasPers&#39;) == &#39;true&#39;)"
                                                text="Export..." type="option">

	</ie:menuitem>
                                            <ie:menuitem id="MSOMenu_Help" iconsrc="/_layouts/15/images/HelpIcon.gif" onmenuclick="MSOWebPartPage_SetNewWindowLocation(MenuWebPart.getAttribute('helpLink'), MenuWebPart.getAttribute('helpMode'))"
                                                text="Help" type="option" style="display: none">

	</ie:menuitem>
                                        </menu>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <!-- /END Page Layout (above this) -->
                    </div>
                    <span class="topright"></span><span class="topleft"></span><span class="bottomright">
                    </span><span class="bottomleft"></span>
                </div>
                <!-- ############### MAIN CONTENT ENDS ###############-->
                <!-- ############### BOTTOM LINKS STARTS ###############-->
                <div class="bottomLinks clearfix">
                    <a href="#" class="bottomLinksbox"><span class="bottomLinksImage">
                        <img src="/SiteCollectionImages/Core/tourismAustralia.jpg" />
                        <span class="bottomLinksTitle">There's Nothing Like Australia </span></span><span
                            class="bottomLinksText">In every part of our vast continent you'll find a wide range
                            of unique experiences and attractions that are sure to deliver. </span></a>
                    <a href="#" class="bottomLinksbox"><span class="bottomLinksImage">
                        <img src="/SiteCollectionImages/Core/tourismAustralia.jpg" />
                        <span class="bottomLinksTitle">There's Nothing Like Australia </span></span><span
                            class="bottomLinksText">In every part of our vast continent you'll find a wide range
                            of unique experiences and attractions that are sure to deliver. </span></a>
                    <a href="#" class="bottomLinksbox"><span class="bottomLinksImage">
                        <img src="/SiteCollectionImages/Core/tourismAustralia.jpg" />
                        <span class="bottomLinksTitle">There's Nothing Like Australia </span></span><span
                            class="bottomLinksText">In every part of our vast continent you'll find a wide range
                            of unique experiences and attractions that are sure to deliver. </span></a>
                    <a href="#" class="bottomLinksbox"><span class="bottomLinksImage">
                        <img src="/SiteCollectionImages/Core/tourismAustralia.jpg" />
                        <span class="bottomLinksTitle">There's Nothing Like Australia </span></span><span
                            class="bottomLinksText">In every part of our vast continent you'll find a wide range
                            of unique experiences and attractions that are sure to deliver. </span></a>
                </div>
                <!-- ############### BOTTOM LINKS ENDS ###############-->
            </div>
        </div>
        <div class="clearfix">
        </div>
        <!-- ############### FOOTER STARTS ###############-->
        <div class="footerContainer">
            <div class="footerMainContent">
                <div class="footerMainContentLeft">
                    <div class="footerLogo">
                        <img src="/SiteCollectionImages/Core/footerLogo.png" alt="Britz Campervans" />
                    </div>
                    <div class="search">
                        <div data-name="SearchBox">
                            <div class="ms-webpart-chrome ms-webpart-chrome-fullWidth ">
                                <div webpartid="00000000-0000-0000-0000-000000000000" haspers="true" id="WebPartWPQ2"
                                    width="100%" class="ms-WPBody " onlyformepart="true" allowdelete="false" style="">
                                    <div componentid="ctl00_ctl46_csr" id="ctl00_ctl46_csr">
                                        <div id="SearchBox" name="Control">
                                            <div class="ms-srch-sb ms-srch-sb-border" id="ctl00_ctl46_csr_sboxdiv">
                                                <input type="text" value="Search..." maxlength="2048" accesskey="S" title="Search..."
                                                    id="ctl00_ctl46_csr_sbox" autocomplete="off" autocorrect="off" onkeypress="EnsureScriptFunc('Search.ClientControls.js', 'Srch.U', function() {if (Srch.U.isEnterKey(String.fromCharCode(event.keyCode))) {$find('ctl00_ctl46_csr').search($get('ctl00_ctl46_csr_sbox').value);return Srch.U.cancelEvent(event);}})"
                                                    onkeydown="EnsureScriptFunc('Search.ClientControls.js', 'Srch.U', function() {var ctl = $find('ctl00_ctl46_csr');ctl.activateDefaultQuerySuggestionBehavior();})"
                                                    onfocus="EnsureScriptFunc('Search.ClientControls.js', 'Srch.U', function() {var ctl = $find('ctl00_ctl46_csr');ctl.hidePrompt();ctl.setBorder(true);})"
                                                    onblur="EnsureScriptFunc('Search.ClientControls.js', 'Srch.U', function() {var ctl = $find('ctl00_ctl46_csr'); if (ctl){ ctl.showPrompt(); ctl.setBorder(false);}})"
                                                    class="ms-textSmall ms-srch-sb-prompt ms-helperText" /><a title="Search" class="ms-srch-sb-searchLink"
                                                        id="ctl00_ctl46_csr_SearchLink" onclick="EnsureScriptFunc('Search.ClientControls.js', 'Srch.U', function() {$find('ctl00_ctl46_csr').search($get('ctl00_ctl46_csr_sbox').value);})"
                                                        href="javascript: {}"><img src="/_layouts/15/images/searchresultui.png?rev=23" class="ms-srch-sb-searchImg"
                                                            id="searchImg" alt="Search" /></a></div>
                                        </div>
                                    </div>
                                    <noscript>
                                        <div id="ctl00_ctl46_noscript">
                                            It looks like your browser does not have JavaScript enabled. Please turn on JavaScript
                                            and try again.</div>
                                    </noscript>
                                    <div id="ctl00_ctl46">
                                    </div>
                                    <div class="ms-clear">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="languageConnectBar">
                        <a href="#" class="social">
                            <img src="/SiteCollectionImages/Core/facebookFooter.jpg" alt="facebook" />
                        </a><a href="#" class="social">
                            <img src="/SiteCollectionImages/Core/twitterFooter.jpg" alt="twitter" />
                        </a>
                        <div class="language">
                            <div class="currentLang">
                                English
                            </div>
                            <div class="languageOption">
                                <a href="/Britz-verhuur-van-campers-Nieuw-Zeeland" class="lang French">French </a>
                                <a href="/Britz-Wohnmobilvermietung-Neuseeland " class="lang German">German </a>
                                <a href="/Location-de-camping-cars-Britz-en-Nouvelle-Zelande" class="lang Dutch">Dutch
                                </a><a href="/china" class="lang Chinese">Chinese </a>
                            </div>
                        </div>
                    </div>
                    <div class="copyright">
                        <p>
                            YOU ARE VIEWING <a href="" class="website">WWW.BRITZ.COM.AU </a>
                        </p>
                        <p>
                            © 2013 Tourism Holdings Ltd (thl)
                        </p>
                    </div>
                </div>
                <div class="footerMainContentCentre">
                    <div class="footerMainContentCentreLeft">
                        <div class="ourSiteLinks">
                            <p>
                                OUR SITE
                            </p>
                            <ul>
                                <li><a href="#">Hot oz deals </a></li>
                                <li><a href="#">Campervans </a></li>
                                <li><a href="#">Hire Cars </a></li>
                                <li><a href="#">Branch Locations </a></li>
                                <li><a href="#">Plan Your Trip </a></li>
                                <li><a href="#">Get to know Oz </a></li>
                                <li><a href="#">Contact Us </a></li>
                            </ul>
                        </div>
                        <div class="miscLinks">
                            <p>
                                MISCELLANEOUS
                            </p>
                            <ul>
                                <li><a href="#">Agent Media Resources </a></li>
                                <li><a href="#">Sponsorship Requests </a></li>
                                <li><a href="#">Check-in Online </a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="footerMainContentCentreRight">
                        <div class="branchLinks">
                            <p>
                                BRANCH LOCATIONS
                            </p>
                            <ul>
                                <li><a href="#">Adelaide </a></li>
                                <li><a href="#">Alice Springs </a></li>
                                <li><a href="#">Ballina </a></li>
                                <li><a href="#">Byron Bay </a></li>
                                <li><a href="#">Brisbane </a></li>
                                <li><a href="#">Broome </a></li>
                                <li><a href="#">Cairns </a></li>
                                <li><a href="#">Darwin </a></li>
                                <li><a href="#">Hobart </a></li>
                                <li><a href="#">Melbourne </a></li>
                                <li><a href="#">Perth </a></li>
                                <li><a href="#">Sydney </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footerMainContentRight">
                    <div class="footerHotDeals">
                        <span class="footerHotDealsTitle"><a href="/australia-travel/hot-offers">HOT OZ DEALS
                        </a></span><a href="/australia-travel/hot-offers" class="findOutMoreBtnFooter">View
                            All </a>
                        <div class="ms-webpart-chrome ms-webpart-chrome-fullWidth ">
                            <div webpartid="00000000-0000-0000-0000-000000000000" haspers="true" id="WebPartWPQ3"
                                width="100%" class="ms-WPBody " onlyformepart="true" allowdelete="false" style="">
                                <ul>
                                    <li><a href="http://dev.britz.com.au/promotions/Pages/bathurst.aspx">Britz for Bathurst</a></li><li>
                                        <a href="http://dev.britz.com.au/promotions/Pages/Britz-Early-Bird.aspx">Early Bird
                                            Savings</a></li><li><a href="http://dev.britz.com.au/promotions/Pages/Britz-Hot-Everyday-Rates.aspx">
                                                Hot Everyday Rates</a></li><li><a href="http://dev.britz.com.au/promotions/Pages/wildflower-adventure.aspx">
                                                    Wildflower Adventure</a></li></ul>
                                <div class="ms-clear">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footerLogos">
                        <div class="tQual">
                        </div>
                        <div class="tourismAustralia">
                        </div>
                        <div class="priceBeat">
                            <span class="footerLogosL1">WE WON'T BE </span><span class="footerLogosL2"><strong>BEATEN
                            </strong>ON PRICE </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footerBase">
                <div class="footerContact">
                    <div class="footerContactCall">
                        <span class="footerContactTitle"><a href="/ContactUs">Call free within NZ &amp; Worldwide
                        </a></span><span class="footerContactInfo"><a href="/ContactUs">0800 831 900 </a>
                        </span>
                    </div>
                    <div class="footerContactMessage">
                        <span class="footerContactTitle">EMAIL </span><span class="footerContactInfo"><a
                            href="/ContactUs">Send us a message </a></span>
                    </div>
                </div>
            </div>
            <div id="footerWrap">
                <!-- virgin start -->
                <div id="brandedFooter" class="body">
                </div>
                <!-- virgin end -->
                <div class="thl body">
                    <ul>
                        <li><a class="thl-logo" href="http://www.thlonline.com"><span class="thl"></span></a>
                        </li>
                        <li><a class="thllogos" href="http://www.keacampers.com"><span class="kea"></span></a>
                        </li>
                        <li><a class="thllogos" href="http://www.maui-rentals.com"><span class="maui"></span>
                        </a></li>
                        <li><a class="thllogos" href="http://www.britz.com"><span class="britz"></span></a>
                        </li>
                        <li><a class="thllogos" href="http://www.unitedcampervans.com"><span class="united">
                        </span></a></li>
                        <li><a class="thllogos" href="http://www.alphacampers.com"><span class="alpha"></span>
                        </a></li>
                        <li><a class="thllogos" href="http://www.mightycampers.com"><span class="mighty"></span>
                        </a></li>
                        <li><a class="thllogos" href="http://www.econocampers.com"><span class="econo"></span>
                        </a></li>
                    </ul>
                    <span class="privacy"><b>© Tourism Holidings Limited </b>
                        <br />
                        <a href="#">Privacy Policy </a></span>
                </div>
            </div>
        </div>
        <!-- ############### FOOTER ENDS ###############-->
    </div>
    <div id="DeltaFormDigest">
    </div>
    <script type="text/javascript">
//<![CDATA[

        WebForm_InitCallback(); var _spFormDigestRefreshInterval = 1440000; window.g_updateFormDigestPageLoaded = new Date(); window.g_updateFormDigestPageLoaded.setDate(window.g_updateFormDigestPageLoaded.getDate() - 5); var _fV4UI = true;
        function _RegisterWebPartPageCUI() {
            var initInfo = { editable: true, isEditMode: false, allowWebPartAdder: false, listId: "{4a82c6d3-aadc-4627-81f3-b015b8b40367}", itemId: 1, recycleBinEnabled: true, enableMinorVersioning: true, enableModeration: false, forceCheckout: true, rootFolderUrl: "\u002fcampervans\u002fPages", itemPermissions: { High: 2147483647, Low: 4294967295} };
            SP.Ribbon.WebPartComponent.registerWithPageManager(initInfo);
            var wpcomp = SP.Ribbon.WebPartComponent.get_instance();
            var hid;
            hid = document.getElementById("_wpSelected");
            if (hid != null) {
                var wpid = hid.value;
                if (wpid.length > 0) {
                    var zc = document.getElementById(wpid);
                    if (zc != null)
                        wpcomp.selectWebPart(zc, false);
                }
            }
            hid = document.getElementById("_wzSelected");
            if (hid != null) {
                var wzid = hid.value;
                if (wzid.length > 0) {
                    wpcomp.selectWebPartZone(null, wzid);
                }
            }
        };
        function __RegisterWebPartPageCUI() {
            ExecuteOrDelayUntilScriptLoaded(_RegisterWebPartPageCUI, "sp.ribbon.js");
        }
        _spBodyOnLoadFunctionNames.push("__RegisterWebPartPageCUI"); var __wpmExportWarning = 'This Web Part Page has been personalized. As a result, one or more Web Part properties may contain confidential information. Make sure the properties contain information that is safe for others to read. After exporting this Web Part, view properties in the Web Part description file (.WebPart) by using a text editor such as Microsoft Notepad.'; var __wpmCloseProviderWarning = 'You are about to close this Web Part.  It is currently providing data to other Web Parts, and these connections will be deleted if this Web Part is closed.  To close this Web Part, click OK.  To keep this Web Part, click Cancel.'; var __wpmDeleteWarning = 'You are about to permanently delete this Web Part.  Are you sure you want to do this?  To delete this Web Part, click OK.  To keep this Web Part, click Cancel.'; function ctl00_ctl34_g_428a7651_8ea2_4c53_932c_e511ad3c6f66_init() {
            EnsureScript('mediaplayer.js',
        typeof (mediaPlayer) != 'undefined' ? typeof (mediaPlayer.attachToMediaLinks) : 'undefined');
            ExecuteOrDelayUntilScriptLoaded(
        function () {
            if (Silverlight.isInstalled('2.0')) {
                mediaPlayer.createOverlayPlayer();
                mediaPlayer.attachToMediaLinks(document.getElementById('cbqwpctl00_ctl34_g_428a7651_8ea2_4c53_932c_e511ad3c6f66'), ["wmv", "wma", "avi", "mpg", "mp3", "mp4", "asf", "ogg", "ogv", "oga", "webm"], '');
            }
        }, 'mediaplayer.js');
        }
        if (_spBodyOnLoadFunctionNames != null) { _spBodyOnLoadFunctionNames.push('ctl00_ctl34_g_428a7651_8ea2_4c53_932c_e511ad3c6f66_init'); }
        function ctl00_ctl34_g_f18746c2_cb44_49bb_a6a5_0bfb195e75e9_init() {
            EnsureScript('mediaplayer.js',
        typeof (mediaPlayer) != 'undefined' ? typeof (mediaPlayer.attachToMediaLinks) : 'undefined');
            ExecuteOrDelayUntilScriptLoaded(
        function () {
            if (Silverlight.isInstalled('2.0')) {
                mediaPlayer.createOverlayPlayer();
                mediaPlayer.attachToMediaLinks(document.getElementById('cbqwpctl00_ctl34_g_f18746c2_cb44_49bb_a6a5_0bfb195e75e9'), ["wmv", "wma", "avi", "mpg", "mp3", "mp4", "asf", "ogg", "ogv", "oga", "webm"], '');
            }
        }, 'mediaplayer.js');
        }
        if (_spBodyOnLoadFunctionNames != null) { _spBodyOnLoadFunctionNames.push('ctl00_ctl34_g_f18746c2_cb44_49bb_a6a5_0bfb195e75e9_init'); }

        ExecuteOrDelayUntilScriptLoaded(
function () {
    var initInfo =
{
    itemPermMasks: { High: 2147483647, Low: 4294967295 },
    listPermMasks: { High: 2147483647, Low: 4294967295 },
    listId: "4a82c6d3-aadc-4627-81f3-b015b8b40367",
    itemId: 1,
    workflowsAssociated: false,
    editable: true,
    doNotShowProperties: false,
    enableVersioning: true
};
    SP.Ribbon.DocLibAspxPageComponent.registerWithPageManager(initInfo);
},
"sp.ribbon.js");
        var g_disableCheckoutInEditMode = false;
        var _spWebPermMasks = { High: 2147483647, Low: 4294967295 }; var _spPageStateActionControlId = 'ctl00_ctl42_PageStateActionButton'; function ctl00_g_552868ad_c69a_40eb_89cd_098929fd486c_init() {
            EnsureScript('mediaplayer.js',
        typeof (mediaPlayer) != 'undefined' ? typeof (mediaPlayer.attachToMediaLinks) : 'undefined');
            ExecuteOrDelayUntilScriptLoaded(
        function () {
            if (Silverlight.isInstalled('2.0')) {
                mediaPlayer.createOverlayPlayer();
                mediaPlayer.attachToMediaLinks(document.getElementById('cbqwpctl00_g_552868ad_c69a_40eb_89cd_098929fd486c'), ["wmv", "wma", "avi", "mpg", "mp3", "mp4", "asf", "ogg", "ogv", "oga", "webm"], '');
            }
        }, 'mediaplayer.js');
        }
        if (_spBodyOnLoadFunctionNames != null) { _spBodyOnLoadFunctionNames.push('ctl00_g_552868ad_c69a_40eb_89cd_098929fd486c_init'); }
        function ctl00_g_def54fc3_44c3_4798_a041_3f9ff2900e2b_init() {
            EnsureScript('mediaplayer.js',
        typeof (mediaPlayer) != 'undefined' ? typeof (mediaPlayer.attachToMediaLinks) : 'undefined');
            ExecuteOrDelayUntilScriptLoaded(
        function () {
            if (Silverlight.isInstalled('2.0')) {
                mediaPlayer.createOverlayPlayer();
                mediaPlayer.attachToMediaLinks(document.getElementById('cbqwpctl00_g_def54fc3_44c3_4798_a041_3f9ff2900e2b'), ["wmv", "wma", "avi", "mpg", "mp3", "mp4", "asf", "ogg", "ogv", "oga", "webm"], '');
            }
        }, 'mediaplayer.js');
        }
        if (_spBodyOnLoadFunctionNames != null) { _spBodyOnLoadFunctionNames.push('ctl00_g_def54fc3_44c3_4798_a041_3f9ff2900e2b_init'); }

        ExecuteOrDelayUntilScriptLoaded(
                function () {
                    Srch.ScriptApplicationManager.get_current().states = { "browserLanguage": 1033, "webUILanguageName": "en-US", "webDefaultLanguageName": "en-US", "contextUrl": "http://dev.britz.com.au/campervans", "showAdminDetails": true, "defaultPagesListName": "Pages", "isSPFSKU": false, "defaultQueryProperties": { "culture": 1033, "uiLanguage": 1033, "summaryLength": 180, "desiredSnippetLength": 90, "enableStemming": true, "enablePhonetic": false, "enableNicknames": false, "trimDuplicates": true, "bypassResultTypes": false, "enableInterleaving": true, "enableQueryRules": true, "processBestBets": true, "enableOrderingHitHighlightedProperty": false, "hitHighlightedMultivaluePropertyLimit": -1, "processPersonalFavorites": true} };
                    Srch.U.trace(null, 'SerializeToClient', 'ScriptApplicationManager state initialized.');
                }, 'Search.ClientControls.js'); var g_clientIdDeltaPlaceHolderMain = "DeltaPlaceHolderMain";
        var g_clientIdDeltaPlaceHolderUtilityContent = "DeltaPlaceHolderUtilityContent";

        theForm.oldSubmit = theForm.submit;
        theForm.submit = WebForm_SaveScrollPositionSubmit;

        theForm.oldOnSubmit = theForm.onsubmit;
        theForm.onsubmit = WebForm_SaveScrollPositionOnSubmit;

        var g_commandUIHandlers = { "name": "CommandHandlers", "attrs": {}, "children": [] };
        g_QuickLaunchControlIds.push("zz12_V4QuickLaunchMenu"); _spBodyOnLoadFunctionNames.push('QuickLaunchInitDroppable'); var g_zz12_V4QuickLaunchMenu = null; function init_zz12_V4QuickLaunchMenu() { if (g_zz12_V4QuickLaunchMenu == null) g_zz12_V4QuickLaunchMenu = $create(SP.UI.AspMenu, null, null, null, $get('zz12_V4QuickLaunchMenu')); } ExecuteOrDelayUntilScriptLoaded(init_zz12_V4QuickLaunchMenu, 'SP.Core.js');

        ExecuteOrDelayUntilScriptLoaded(
                        function () {
                            if ($isNull($find('ctl00_ctl46_csr'))) {
                                var sb = $create(Srch.SearchBox, { "delayLoadTemplateScripts": true, "messages": [], "queryGroupNames": ["Default"], "renderTemplateId": "~sitecollection/_catalogs/masterpage/Display Templates/Search/Control_SearchBox.js", "resultsPageAddress": "/aboutus/Pages/SearchResults.aspx", "serverInitialRender": true, "showDataErrors": true, "showQuerySuggestions": false, "states": {}, "tryInplaceQuery": false }, null, null, $get("ctl00_ctl46_csr"));
                                sb.activate('Search...', 'ctl00_ctl46_csr_sbox', 'ctl00_ctl46_csr_sboxdiv', 'ctl00_ctl46_csr_NavButton', 'ctl00_ctl46_csr_AutoCompList', 'ctl00_ctl46_csr_NavDropdownList', 'ctl00_ctl46_csr_SearchLink', 'ms-srch-sbprogress', 'ms-srch-sb-prompt ms-helperText');
                            }
                        }, 'Search.ClientControls.js');//]]>
    </script>
    </form>
    <span id="DeltaPlaceHolderUtilityContent"></span>
</body>
</html>