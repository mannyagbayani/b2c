﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BookNow.aspx.cs" Inherits="UnitTests_BookNow" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js" type="text/javascript"></script>
    <script src="jquery.tools.min.js" type="text/javascript"></script>   
    <link rel="stylesheet" href="http://jqueryui.com/themes/base/jquery.ui.all.css" />
    <link href="http://images.thl.com/css/b/jcal.css" type="text/css" rel="stylesheet" />
    <link href="css/booknow.css" type="text/css" rel="stylesheet" />
    <title>Book A THL Vehicle</title>
</head>
<body>
    <form id="form1" runat="server">   
        <input type="hidden" id="vh" name="vh" value="nzavb.4BB" />
        <input type="hidden" id="cc" name="cc" value="NZ" />
        <input type="hidden" id="brand" name="brand" value="b" />                
        <input type="hidden" id="sc" name="sc" value="rv" />
        <input type="hidden" id="vtype" name="vtype" value="rv" />        
        
        <input type="hidden" id="pd" name="pd" value="1" />
        <input type="hidden" id="pm" name="pm" value="8" />
        <input type="hidden" id="py" name="py" value="2011" />
        <input type="hidden" id="dd" name="dd" value="10" />
        <input type="hidden" id="dm" name="dm" value="8" />
        <input type="hidden" id="dy" name="dy" value="2011" />
        
        <input type="hidden" id="pt" name="pt" value="10:00" />
        <input type="hidden" id="dt" name="dt" value="15:00" />
        
        <div>
            <!-- tabs hide if not needed -->
            <ul class="navi" id="flowtabs">
	            <li><a href="#pickUpDate" id="t1" class="">Pick Up</a></li>
	            <li><a href="#DropOffDate" id="A1" class="">Drop Off</a></li>
	            <li><a href="#locations" id="t2" class="">Get Quote</a></li>	            	        
            </ul>
            <!-- paneles -->
            <div id="flowpanes">	    
	            <div class="items">		
                    <div id="pdPanel" class="panelTab">
                        <select id="pb" name="pb" class="locSelector">
                            <option value="-1">Pick Up Location</option>                            
                            <option value="AKL">Auckland airport</option>
                            <option value="AKX">Auckland city</option>
                            <option value="WLG">Wellington</option>
                            <option value="CHC">Christchurch airport</option>
                            <option value="ZQN">Queenstown</option>                           
                        </select>
                        <input type="text" id="from-date" class="datepicker" />
                        <span id="fromDateDiv" style="width:100px;height: 100px;"></span>                        
                    </div>
                    <div id="ddPanel">                    
                        <select id="db" name="db" class="locSelector">
                            <option value="-1">Drop Off Location</option>
                            <option value="AKL">Auckland airport</option>
                            <option value="AKX">Auckland city</option>
                            <option value="WLG">Wellington</option>
                            <option value="CHC">Christchurch airport</option>
                            <option value="ZQN">Queenstown</option>
                        </select>                        
                        <input type="text" id="to-date" class="datepicker" />
                        <span id="toDateDiv" style="width:100px;height: 100px;"></span>
                    </div>                          
                    <div id="bookNow">
                        <img src="http://images.thl.com/images/b/ajax-loader.gif">
		            </div>
		            <div id="bookNowConfirm">
                        <span>GET The <b class="reponseVehicle"></b> on these dates for just <b class="responsePrice">$0.00</b></span>
                        <span class="submitBtnSpan"></span>
		            </div>		    
	            </div>
            </div>
            <script type="text/javascript">
                $(function() {
                    $("#flowpanes").scrollable({ circular: true, mousewheel: true }).navigator({
                        navi: "#flowtabs",
                        naviItem: 'a',
                        activeClass: 'current',
                        history: true
                    });

                    $("#fromDateDiv").datepicker({
                        disabled: false,
                        dateFormat: 'dd-mm-yy',
                        //beforeShowDay: function(){} ,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        minDate: new Date(),
                        onSelect: function(dateText, inst) { $('#from-date').val(dateText); setDate(inst); validate(0) }
                    });

                    $("#toDateDiv").datepicker({
                        dateFormat: 'dd-mm-yy',                        
                        numberOfMonths: 1,
                        minDate: new Date(),
                        changeMonth: true,
                        changeYear: true,
                        onSelect: function(dateText, inst) { $('#to-date').val(dateText); setDate(inst); validate(1) }
                    });

                    $("#pb").change(function() { validate(0); });
                    $("#db").change(function() { validate(1); });                    

                });

                function setDate(inst) {
                    //alert(inst.selectedDay);
                    if (inst.id == "toDateDiv") {
                        $('#dd').val(inst.selectedDay);
                        $('#dm').val(inst.selectedMonth+1);
                        $('#dy').val(inst.selectedYear);                        
                    }
                    else {//from date changed                    
                        $('#pd').val(inst.selectedDay);
                        $('#pm').val(inst.selectedMonth + 1);
                        $('#py').val(inst.selectedYear);

                        $('#dd').val(inst.selectedDay);
                        $('#dm').val(inst.selectedMonth + 1);
                        $('#dy').val(inst.selectedYear);
                        $('#toDateDiv').datepicker("setDate", new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay));
                        $('#toDateDiv').datepicker("option", "minDate",  new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay));
                    }                  
                }

                function validate(state)
                {
                    if (state == 0) {//PickUp
                        if (state == 0 && $("#from-date").val() != "" && $('#pb').val() != '-1') {
                            setState(1);
                            $('#pdPanel').removeClass('required');                            
                        }
                        else {
                            $('#pdPanel').addClass('required');
                        }
                    } else if (state == 1) {//DropOff
                        if (state == 1 && $("#to-date").val() != "" && $('#db').val() != '-1') {
                            setState(2); //TODO: validate date range here
                            $('#ddPanel').removeClass('required');
                            submitQuote();
                        }
                        else
                            $('#ddPanel').addClass('required');
                    }
                }

                var fParams = "";
                
                function submitQuote(){
                    fParams = $('#form1').serialize().replace("&pb=-1", "") + "&na=1&nc=0&quote=true";
                    $.ajax({
                        url: "?" + fParams,
                        context: document.body,
                        success: function(res) {
                            if (res.total > 0) {
                                $('.responsePrice').text("$" + res.total);
                                $('.reponseVehicle').text(res.prdName);                                
                                $(".submitBtnSpan").html("<a href='http://local.britz.co.nz/bookingcontrols/Selection.aspx?" + fParams.replace("__VIEWSTATE","state") + "&cr='>Book Now</a>");
                                //console.log("http://local.britz.co.nz/bookingcontrols/?" + fParams.replace("__VIEWSTATE","state");//no need
                            }
                            setState(3);
                        }
                    });
                }                
                
                function setState(i) {
                   $("#flowpanes").data("scrollable").seekTo(i);            
                }
            </script>
        </div>   
    </form>
</body>
</html>