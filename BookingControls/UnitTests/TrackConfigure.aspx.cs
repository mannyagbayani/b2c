﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UnitTests_TrackConfigure : System.Web.UI.Page
{

    public string TrackerStr;

    protected void Page_Load(object sender, EventArgs e)
    {
        string[] vehicleCodes = new string[] { "PFMRS", "2B4WDBC", "2B4WDB", "6BB", "2BTSB", "5BB" };
        int randomVehicleIndex = new Random().Next(0, vehicleCodes.Length);

        TrackerStr = "pageTracker._trackEvent('SelectedVehicle', '" + vehicleCodes[randomVehicleIndex] + "');\n";

        //TrackerStr += "pageTracker._trackEvent('SelectedVehicle', '" + vehicleCodes[randomVehicleIndex] + "');\n";

    }
}
