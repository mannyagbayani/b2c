﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class UnitTests_LoadVehicleContent : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ApplicationData appData = ApplicationManager.GetApplicationData();
        Vehicle[] vehicles = appData.Vehicles;
        int numLoaded = ApplicationData.LoadVehiclesContent(ref vehicles);

        Response.Write("num loaded :" + numLoaded);
        foreach (Vehicle vehicle in vehicles)
        {
            if(vehicle.Content != null)
                Response.Write("<br />" + vehicle.Content.Description);
        }
    }
}