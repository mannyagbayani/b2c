﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Econo.aspx.cs" Inherits="UnitTests_Econo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en-NZ" xml:lang="en-NZ" xmlns="http://www.w3.org/1999/xhtml">
<head id="Header1">
    <title>Budget Camper Van Hire New Zealand Campervan Rental – Econo Campers NZ </title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta name="robots" content="index,follow" />
    <script type="text/javascript" src="http://www.econocampers.co.nz/Resources/JavaScript/ContegroScriptaculous/lib/prototype.js"></script>
    <script type="text/javascript" src="http://www.econocampers.co.nz/Resources/JavaScript/ContegroScriptaculous/src/scriptaculous.js"></script>
    <script type="text/javascript" src="http://www.econocampers.co.nz/Resources/JavaScript/BannerRandom.4.3.7.js"></script>
    <link href="http://www.econocampers.co.nz/WebResource.axd?d=VbCohrPvzfi7ETwgAiTJ3G5sEsDACQvZCtqQUyazm_NC7IscyBlDKZ3FOFBm8XGeQnvYoWvrWZK9a2Xe-wkZZHyEFbSlu6HJffUecx03U21tNNDyMgturVs2S3Wo5zuL-tUCeg2&amp;t=634768365830603750"
        type="text/css" rel="stylesheet" class="Telerik_stylesheet" />
    <link type="text/css" rel="stylesheet" href="http://www.econocampers.co.nz/WebResource.axd?d=b8lpWQoX_vDHRTBkiywZdPr998N2ESJLgLDaFJ0RiRnW1llpV2KvRgou6_NJpAM-0o8pUwThKrKctyJpLC_itUnggNkSG6Snia_U-434myTDYu0CaY6NbAgE6UHVLrj-6gWd3OSUR4NX13dl737Wqu3u8JM1&amp;amp;t=634768365822635000" />
    <link type="text/css" rel="stylesheet" href="http://www.econocampers.co.nz/Resources/RadControls/Menu/Skins/SlideHorizontal/styles.css" />
    <link href="http://www.econocampers.co.nz/Resources/StyleSheets/EconoCampers/DefaultTheme.css" type="text/css"
        rel="stylesheet" />
    <link href="http://www.econocampers.co.nz/Resources/StyleSheets/EconoCampers/Template.css" type="text/css" rel="stylesheet" />
    <link href="http://www.econocampers.co.nz/Resources/StyleSheets/EconoCampers/NavigationSlide.css" type="text/css"
        rel="stylesheet" />
    <link href="http://www.econocampers.co.nz/Resources/StyleSheets/EconoCampers/BannerRandom.css" type="text/css"
        rel="stylesheet" />
    <link href="http://www.econocampers.co.nz/Resources/StyleSheets/EconoCampers/Content.css" type="text/css" rel="stylesheet" />
    <link href="http://www.econocampers.co.nz/Resources/StyleSheets/EconoCampers/CustomCode.css" type="text/css" rel="stylesheet" />
    <link href="http://www.econocampers.co.nz/Resources/StyleSheets/EconoCampers/CategoryList.css" type="text/css"
        rel="stylesheet" />
    <link href="http://www.econocampers.co.nz/Resources/StyleSheets/EconoCampers/Banner.css" type="text/css" rel="stylesheet" />
    <link href="http://www.econocampers.co.nz/Resources/StyleSheets/EconoCampers/Navigation.css" type="text/css" rel="stylesheet" />
    <meta name="description" content="New Zealand’s best value budget camper van hire – 2-3 berth quality economy campervans, lots of free extras, bike hire. Auckland &amp; Christchurch depots" />
    <meta name="keywords" content="budget camper van hire zealand cheap campers economy campervan rentals auckland campers christchurch backpacker campervan rental nz campervan deals" />
</head>
<body>
    <form method="post" action="/" id="Form1" class="contegro_firefox16 contegro_firefox">
    <div>
        <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
        <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
    </div>
    <script type="text/javascript">
//<![CDATA[
        var theForm = document.forms['Form1'];
        if (!theForm) {
            theForm = document.Form1;
        }
        function __doPostBack(eventTarget, eventArgument) {
            if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
                theForm.__EVENTTARGET.value = eventTarget;
                theForm.__EVENTARGUMENT.value = eventArgument;
                theForm.submit();
            }
        }
//]]>
    </script>
    <script src="http://www.econocampers.co.nz/WebResource.axd?d=PfbI-AjfRFvntB5JHAsFKeVWyp_cucex7a146GlsDkPEVDihdaUisk23_3d4Qnh8SWjJ1PvkNnl2BQ-E1_EkmZFArSU1&amp;t=634605703982615616"
        type="text/javascript"></script>
    <script src="http://www.econocampers.co.nz/Resources/JavaScript/Common.js" type="text/javascript"></script>
    <script src="http://www.econocampers.co.nz/Resources/JavaScript/FireDefaultButtonFix.js" type="text/javascript"></script>
    <script src="http://www.econocampers.co.nz/ScriptResource.axd?d=Hza0YhRfpaUXWBOr3hPcLiJR7LXN_ZX1dEr94BwwWNUVM7wX4YSbNiobAaUrF1eII5NM02IMB1Q1XwlsoflPXMW4mrxHYqxs4xBs3AhY3YHdLq0cPx0BTfOyxoqs2fVBR5Sy6nxYSX_rLCKame0pNxD-_Yo1&amp;t=ffffffff8dc250fc"
        type="text/javascript"></script>
    <script src="http://www.econocampers.co.nz/ScriptResource.axd?d=FqLs9MoQXu7LXONg0lI9vRP2bIqtUbQdQ2DGj3rlZJMqa8P27OSSmr3hoBuTMgb7I77biGlrOUOCmxA3rUE0_1jM-RhVrPjmDfffId-7LsBcqUreFwb_rn3NIszK5k7P4oaerhfdWgi2bngxM6hdXdtEI_q73FJHP4OxbisYc8ibZu5q0&amp;t=ffffffff8dc250fc"
        type="text/javascript"></script>
    <script src="http://www.econocampers.co.nz/ScriptResource.axd?d=PV8ByooKFiWkjufLrRZFtUUYCe_ovVm-cZMWBTMd9pOIFWGcwkTrBJXnfGkhEdJ3loC73nBijDCpDXXYk-2XZCthuBso_6IRjwU45WpwdncZQmY5kc6WlTnkkwPYOoa7PQZmxA2&amp;t=a6b9165"
        type="text/javascript"></script>
    <script src="http://www.econocampers.co.nz/ScriptResource.axd?d=CPKQtI06kVdM0j8-WmBbVj7RAFHEgD0UW7lobvxHAE5iYCWf6GeML_r6z_3OXpm0qZnJY0JdS9Qjp-QBanOVd-a826qPUkL3v-5a38e4F64GqmgO0ysZLbQBr6hqzh83hX8Ecw2&amp;t=a6b9165"
        type="text/javascript"></script>
    <script src="http://www.econocampers.co.nz/ScriptResource.axd?d=o94IN0_dWnD5w5fLHKpr81vI4-Fgke8csEZRnSnn5UqXPFCoFCYGlbgfFFC23EaPmeJuJtVSaMtbHGVg_Uj2MaA0H6WjvJrDcQXTCH_5dH5aIriVbz6clChWaDniEJg5Lv02lg2&amp;t=a6b9165"
        type="text/javascript"></script>
    <script type="text/javascript">
//<![CDATA[
        Sys.WebForms.PageRequestManager._initialize('ScriptManager', document.getElementById('Form1'));
        Sys.WebForms.PageRequestManager.getInstance()._updateControls(['tAjaxManagerSU'], [], [], 90);
//]]>
    </script>
    <!-- 2011.2.920.35 -->
    <div id="AjaxManagerSU">
        <span id="AjaxManager" style="display: none;"></span>
    </div>
    <div id="LoadingPanel" style="display: none; height: 75px; width: 75px; width: 320px;
        position: absolute; z-index: 2000;">
        <img id="LoadingImage" src="http://www.econocampers.co.nz/Templates/EconoCampers/_images/default_loading.gif"
            alt="Loading..." style="border-width: 0px;" />
    </div>
    <!-- Home - 3Column -->
    <div id="outer">
        <div id="outerWrapper" class="home">
            <div id="cHWrapper">
                <div id="cH">
                    <div class="cLogo">
                        <a href="/default.aspx" title="Back to home page">
                            <img src="http://www.econocampers.co.nz/Templates/EconoCampers/_images/Logo.gif" alt="Econo Campers" style="display: block;
                                border: none" /></a></div>
                    <!-- cB = Container BANNER - CODE#BANNER - END -->
                    <div id="cB">
                        <div style="position: relative;">
                            <div class="Banner">
                                <a id="ctl03_cB_ctl00_ibBanner" class="BannerImage" href="/bookings--quotes-1/about-our-rates/22-free-items">
                                    <img src="http://www.econocampers.co.nz/images/EconoCampers/_Banners/BANNER-Header_HOME.gif" alt="Click here for your 22 free items"
                                        style="border-width: 0px;" /></a></div>
                        </div>
                    </div>
                    <!-- cB = Container BANNER - CODE#BANNER - END -->
                    <div class="clear">
                        &nbsp;</div>
                    <!-- cN = Container NAVIGATION - CODE#NAVIGATION - START -->
                    <div id="cN">
                        <div style="position: relative;">
                            <div id="ctl03_cN_ctl00_NavigationContent" class="Navigation">
                                <div id="ctl03_cN_ctl00_rmSlideNavigation" class="radmenu RadMenu_SlideHorizontal Nav">
                                    <!-- 4.2.0 -->
                                    <script type="text/javascript" src="http://www.econocampers.co.nz/WebResource.axd?d=c4fjCbJcUV_F77VZoXkkt0QW44gWXs-UZLScouzahbkJaBOyoCS8oJs__2RTbd-vcEFzs1Qg4yamXQHVCkwngKIAwLhuG49asw7eGv2omFhmKDwDee0e1C2CXBh4UHJrbGc6q3a7AicoEKn2nx7g9l0NWHU1&amp;t=634768365822635000"></script>
                                    <span id="ctl03_cN_ctl00_rmSlideNavigationStyleSheetHolder" style="display: none;">
                                    </span>
                                    <ul class="horizontal rootGroup">
                                        <li class="item first"><a href="/" id="ctl03_cN_ctl00_rmSlideNavigation_m0" class="link MenuSelected">
                                            <span class="text">Home</span></a></li><li class="item"><a href="/bookings--quotes-1/book-online"
                                                id="ctl03_cN_ctl00_rmSlideNavigation_m1" class="link"><span class="text">Bookings &amp;
                                                    Quotes</span></a><div class="slide">
                                                        <ul class="vertical group level1">
                                                            <li class="item first"><a href="/bookings--quotes-1/book-online" id="ctl03_cN_ctl00_rmSlideNavigation_m1_m0"
                                                                class="link"><span class="text">Book Online</span></a></li><li class="item"><a href="/bookings--quotes-1/about-our-rates"
                                                                    id="ctl03_cN_ctl00_rmSlideNavigation_m1_m1" class="link"><span class="text">About Our
                                                                        Rates</span></a></li><li class="item"><a href="/bookings--quotes-1/free-items-with-your-econo-rates"
                                                                            id="ctl03_cN_ctl00_rmSlideNavigation_m1_m2" class="link"><span class="text">Free Items
                                                                                with your Econo rates</span></a></li><li class="item"><a href="/Bookings--Quotes-1/About-Our-Rates/default.aspx#Bicycle-Hire"
                                                                                    id="ctl03_cN_ctl00_rmSlideNavigation_m1_m3" class="link"><span class="text">Bicycles
                                                                                        for hire</span></a></li><li class="item"><a href="/Bookings--Quotes-1/About-Our-Rates/default.aspx#Additional-hire"
                                                                                            id="ctl03_cN_ctl00_rmSlideNavigation_m1_m4" class="link"><span class="text">Additional
                                                                                                Items for Hire</span></a></li><li class="item"><a href="/bookings--quotes-1/about-insurance"
                                                                                                    id="ctl03_cN_ctl00_rmSlideNavigation_m1_m5" class="link"><span class="text">About Insurance</span></a></li><li
                                                                                                        class="item"><a href="/bookings--quotes-1/booking-conditions" id="ctl03_cN_ctl00_rmSlideNavigation_m1_m6"
                                                                                                            class="link"><span class="text">Booking Conditions</span></a></li><li class="item"><a
                                                                                                                href="/bookings--quotes-1/important-information" id="ctl03_cN_ctl00_rmSlideNavigation_m1_m7"
                                                                                                                class="link"><span class="text">Important Information</span></a></li><li class="item">
                                                                                                                    <a href="/bookings--quotes-1/living-equipment" id="ctl03_cN_ctl00_rmSlideNavigation_m1_m8"
                                                                                                                        class="link"><span class="text">Living Equipment</span></a></li><li class="item last">
                                                                                                                            <a href="/bookings--quotes-1/faqs" id="ctl03_cN_ctl00_rmSlideNavigation_m1_m9" class="link">
                                                                                                                                <span class="text">FAQs</span></a></li>
                                                        </ul>
                                                    </div>
                                            </li>
                                        <li class="item"><a href="/great-deals" id="ctl03_cN_ctl00_rmSlideNavigation_m2"
                                            class="link"><span class="text">Great Deals</span></a></li><li class="item"><a href="/our-vehicles"
                                                id="ctl03_cN_ctl00_rmSlideNavigation_m3" class="link"><span class="text">Our Vehicles</span></a><div
                                                    class="slide">
                                                    <ul class="vertical group level1">
                                                        <li class="item first"><a href="/our-vehicles/the-ace-23-berth-hi-top" id="ctl03_cN_ctl00_rmSlideNavigation_m3_m0"
                                                            class="link"><span class="text">The Ace 2/3 Berth Hi Top</span></a></li><li class="item last">
                                                                <a href="/our-vehicles/the-duke-2-berth-campervan" id="ctl03_cN_ctl00_rmSlideNavigation_m3_m1"
                                                                    class="link"><span class="text">'The Duke' 2 Berth Campervan</span></a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                        <li class="item"><a href="/depots-1/auckland" id="ctl03_cN_ctl00_rmSlideNavigation_m4"
                                            class="link"><span class="text">Depots</span></a><div class="slide">
                                                <ul class="vertical group level1">
                                                    <li class="item first"><a href="/depots-1/auckland" id="ctl03_cN_ctl00_rmSlideNavigation_m4_m0"
                                                        class="link"><span class="text">Auckland</span></a></li><li class="item last"><a
                                                            href="/depots-1/christchurch" id="ctl03_cN_ctl00_rmSlideNavigation_m4_m1" class="link">
                                                            <span class="text">Christchurch</span></a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="item"><a href="/pip--petes-page" id="ctl03_cN_ctl00_rmSlideNavigation_m5"
                                            class="link"><span class="text">Pip &amp; Pete's page</span></a></li><li class="item">
                                                <a href="/travel-info" id="ctl03_cN_ctl00_rmSlideNavigation_m6" class="link"><span
                                                    class="text">Travel Info</span></a><div class="slide">
                                                        <ul class="vertical group level1">
                                                            <li class="item first"><a href="/travel-info/tourism-radio" id="ctl03_cN_ctl00_rmSlideNavigation_m6_m0"
                                                                class="link"><span class="text">Tourism Radio</span></a></li><li class="item"><a
                                                                    href="/travel-info/holiday-grocery-boxes" id="ctl03_cN_ctl00_rmSlideNavigation_m6_m1"
                                                                    class="link"><span class="text">Holiday Grocery Boxes</span></a></li><li class="item">
                                                                        <a href="/travel-info/freedom-camping-our-way---love-nz" id="ctl03_cN_ctl00_rmSlideNavigation_m6_m2"
                                                                            class="link"><span class="text">Freedom Camping Our Way - Love NZ</span></a></li><li
                                                                                class="item last"><a href="/travel-info/other-links" id="ctl03_cN_ctl00_rmSlideNavigation_m6_m3"
                                                                                    class="link"><span class="text">Other Links</span></a></li>
                                                        </ul>
                                                    </div>
                                            </li>
                                        <li class="item"><a href="/campers-blog" id="ctl03_cN_ctl00_rmSlideNavigation_m7"
                                            class="link"><span class="text">Campers' Blog</span></a></li><li class="item last"><a
                                                href="/enquiries" id="ctl03_cN_ctl00_rmSlideNavigation_m8" class="link"><span class="text">
                                                    Enquiries</span></a></li>
                                    </ul>
                                    <input type="hidden" id="ctl03_cN_ctl00_rmSlideNavigation_Hidden" name="ctl03$cN$ctl00$rmSlideNavigation" /><script
                                        type="text/javascript">                                                                                                                                                    window["ctl03_cN_ctl00_rmSlideNavigation"] = RadMenu.Create("ctl03_cN_ctl00_rmSlideNavigation"); window["ctl03_cN_ctl00_rmSlideNavigation"].Initialize({ "Skin": "SlideHorizontal", "CausesValidation": false, "Enabled": true }, { "ctl03_cN_ctl00_rmSlideNavigation_m0": { "CssClass": "MenuSelected"} });</script>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- cN = Container NAVIGATION - CODE#NAVIGATION - END -->
                </div>
            </div>
            <div id="mainWrapper" class="layout3">
                <div id="cPCWrapper">
                    <div id="cTR">
                        <div style="position: relative;">
                            <div class="BannerRandom" style="position: relative;">
                                <img src="http://www.econocampers.co.nz/Images/EconoCampers/_Banners/RANDOM_Campervans/RANDOM-Campervan_04.jpg"
                                    id="ctl03_cTR_ctl00_ibBanner" alt="Book Me Now!" onclick="RandomBannerClickctl03_cTR_ctl00_ibBanner();"
                                    style="cursor: pointer;" /></div>
                        </div>
                    </div>
                    <!-- cPC = Container PRIMARYCONTAINER - CODE#PRIMARYCONTAINER - START -->
                    <div id="cPC" class="Content">
                        <div style="position: relative;">
                            <div id="ctl03_cPC_ctl00_HtmlContent" class="HTMLContent">
                                <p>
                                    <em><span style="color: #1f497d; font-size: 11px;"><em><span style="color: #1f497d;
                                        font-size: 11px;"></span></em></span></em>
                                </p>
                                <h1>
                                    Miles of Smiles with Econo Campers New Zealand</h1>
                                <h2>
                                    <strong>Absolutely New Zealand's best value, budget brand campervans!</strong></h2>
                                If&nbsp;you're looking for <strong>budget campervan hire in New Zealand</strong>,&nbsp;no-one
                                gives you&nbsp;more value for your money than Econo Campers.&nbsp;&nbsp;<span class="Normal">Look
                                    at&nbsp;just some of the inclusions in your&nbsp;rental rate when you hire a camper
                                    van from Econo Campers:</span>&nbsp;&nbsp;&nbsp;
                                <p>
                                    <table style="background-color: #a7b51e; width: 264px; height: 246px;" border="3"
                                        cellspacing="0" cellpadding="0" align="right">
                                        <tbody>
                                            <tr>
                                                <td style="text-align: center; background-color: #a7b51e; height: 2px; vertical-align: bottom;">
                                                    <h1>
                                                        <font size="2"><font size="2"><strong>HAPPY BIRTHDAY!<br>
                                                        </strong></font></font><strong><font size="2">Pip &amp; Pete's Birthday Special<br>
                                                        </font></strong><font size="2"><strong><font size="3">One Day Free For You<br>
                                                        </font></strong>Celebrating&nbsp;your birthday&nbsp;during your hire? Then tell&nbsp;us
                                                            the date when you book your vehicle, show us proof when you pick it up and this
                                                            great deal</font><font size="2"> </font><font size="2">means that day will
                                                                <br>
                                                            </font><font size="2">be <strong>absolutely free</strong>.</font></h1>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </p>
                                <span style="font-size: 13px;"><strong></strong></span>&nbsp; &nbsp;&nbsp;&nbsp;
                                &nbsp;
                                <ul>
                                    <li><span class="Normal">a well maintained, reliable campervan</span> </li>
                                    <li><strong>free</strong> comprehensive insurance with NZ$1,200 Bond.&nbsp; Find out
                                        <a href="/Modules/LSDocumentManager/DocumentDownload.aspx?DocumentId=283" title="Econo Campers insurance">
                                            <strong>how to reduce this insurance bond &nbsp;to&nbsp;only &nbsp;NZ$250</strong></a>.&nbsp;
                                    </li>
                                    <li><strong>free</strong> 0800 help line and 24 hour roadside assistance </li>
                                    <li><span class="Normal"><strong>free</strong> portable DVD player</span> </li>
                                    <li><span class="Normal"><strong>free</strong> transfer in a luxury mini coach between
                                        Airport and&nbsp;<a href="/depots-1/econo-campers-depots" title="Econo Campers Depots"><strong>Depots</strong></a></span><a
                                            href="default.aspx?MenuId=492&amp;PageId=448" title="Econo  Campers are available from Depots in Auckland &amp; Christchurch, New Zealand"
                                            shape="rect"></a> </li>
                                    <li><span class="Normal"><strong>free</strong> snow chains</span> </li>
                                    <li><strong>free</strong> picnic table and chairs </li>
                                    <li><span class="Normal"><strong>free</strong> tyre repair and/or replacement</span>
                                    </li>
                                    <li><span class="Normal"><strong>no</strong>&nbsp;extra driver fees</span> </li>
                                    <li><span class="Normal">And that's not all -&nbsp;there are too many to list,
                                        <br>
                                        so click here to see&nbsp;<a href="default.aspx?MenuId=551" title="Free Items with your Econo rates"
                                            shape="rect"><strong>more good things</strong><strong>&nbsp;</strong></a>included
                                        in your rates</span>&nbsp; </li>
                                </ul>
                                <p>
                                    <span class="Normal"></span>
                                </p>
                                <p>
                                    <span class="Normal">You&#8217;ll find there are absolutely <strong>no extra costs to
                                        pay on arrival </strong>and no hidden 'surprises' when you pick up your campervan
                                        from Econo Campers, New Zealand&#8217;s friendliest&nbsp;campervan rental company.&nbsp;&nbsp;
                                        And you'll agree</span>&nbsp;we're <strong>New Zealand&#8217;s best value budget&nbsp;campervans</strong>.&nbsp;&nbsp;&nbsp;&nbsp;No
                                    gimmicks, no extra charges, just great campers for a great price and great service.&nbsp;&nbsp;&nbsp;
                                    <a href="default.aspx?MenuId=551" title="Free Items with your Econo rates" shape="rect">
                                        <strong>Check out the full list of what we&nbsp;offer for FREE</strong></a>&nbsp;</p>
                                <p style="margin-right: 0px;" dir="ltr">
                                    Econo Campers can be hired from our&nbsp; <b><a href="default.aspx?MenuId=492&amp;PageId=448"
                                        title="Budget Econo Campers are available from depots in Auckland &amp; Christchurch, New Zealand"
                                        shape="rect"><span style="color: #333333;">Depots in Auckland and Christchurch</span></a></b><b><a
                                            href="default.aspx?MenuId=492&amp;PageId=448" title="Budget Econo Campers are available form depots in Auckland &amp; Christchurch, New Zealand"
                                            shape="rect"><span style="color: #333333;">.</span></a>&nbsp; </b>One way&nbsp;hires
                                    from Auckland to Christchurch or Christchurch to Auckland are our speciality.</p>
                                <p>
                                    Keep an eye out for our special offers, decide which <a href="default.aspx?MenuId=478"
                                        title="Great Deals from Econo Campers budget campervans in New Zealand" shape="rect">
                                        <strong>hot deal</strong></a> you'd like, then use our easy booking facility&nbsp;to
                                    <a href="/bookings--quotes-1/book-online" title="Book Online"><strong>book your campervan</strong></a>&nbsp;with
                                    Econo Campers now.&nbsp;&nbsp;</p>
                            </div>
                            <div class="Banner">
                                <a id="ctl03_cPC_ctl01_ibBanner" class="BannerImage" href="/bookings--quotes-1/book-online">
                                    <img src="http://www.econocampers.co.nz/images/EconoCampers/_Banners/BookaCamper.jpg" alt="Bookings &amp; Quotes"
                                        style="border-width: 0px;" /></a></div>
                            <div class="Banner">
                                <a id="ctl03_cPC_ctl02_ibBanner" class="BannerImage" href="/pip--petes-page">
                                    <img src="http://www.econocampers.co.nz/images/EconoCampers/_Banners/Pip-Pete_Banner.jpg" alt="Click to Pip and Pete's page"
                                        style="border-width: 0px;" /></a></div>
                        </div>
                    </div>
                    <!-- cPC = Container PRIMARYCONTAINER - CODE#PRIMARYCONTAINER - END -->
                </div>
                <!-- cRS = Container RIGHTSIDE - CODE#RIGHTSIDE - START -->
                <div id="cRS">
                    <div style="position: relative;">
                        <div id="ctl03_cRS_ctl00_CustomCodeHolder" class="CustomCode">
                            <table border="0" cellpadding="5" align="center">
                                <tbody>
                                    <tr>
                                        <td style="border-bottom: 0px solid; border-left: 0px solid; border-top: 0px solid;
                                            border-right: 0px solid;">
                                            <a href="/foreign-languages/french---francais" class="ApplyClass">
                                                <img width="24" height="24" style="border-bottom: 0px solid; border-left: 0px solid;
                                                    border-top: 0px solid; border-right: 0px solid;" src="http://www.econocampers.co.nz/images/EconoCampers/flags/France%20flag.gif"></a>
                                        </td>
                                        <td>
                                            <a href="/foreign-languages/german---deutsch">
                                                <img width="24" height="24" style="border-bottom: 0px solid; border-left: 0px solid;
                                                    border-top: 0px solid; border-right: 0px solid;" src="http://www.econocampers.co.nz/images/EconoCampers/flags/Germany%20flag.gif"></a>
                                        </td>
                                        <td>
                                            <a href="/foreign-languages/dutch---nederlands">
                                                <img width="24" height="24" style="border-bottom: 0px solid; border-left: 0px solid;
                                                    border-top: 0px solid; border-right: 0px solid;" src="http://www.econocampers.co.nz/images/EconoCampers/flags/Netherlands%20flag.gif"></a>
                                        </td>
                                        <td>
                                            <a href="/foreign-languages/italian---italiano">
                                                <img width="24" height="24" style="border-bottom: 0px solid; border-left: 0px solid;
                                                    border-top: 0px solid; border-right: 0px solid;" src="http://www.econocampers.co.nz/images/EconoCampers/flags/Italy%20flag.gif"></a>
                                        </td>
                                        <td>
                                            <a href="/foreign-languages/spanish---espanol">
                                                <img width="24" height="24" style="border-bottom: 0px solid; border-left: 0px solid;
                                                    border-top: 0px solid; border-right: 0px solid;" src="http://www.econocampers.co.nz/images/EconoCampers/flags/Spain%20flag.gif"></a>
                                        </td>
                                        <td>
                                            <a href="/foreign-languages/danish---danske">
                                                <img width="24" height="24" style="border-bottom: 0px solid; border-left: 0px solid;
                                                    border-top: 0px solid; border-right: 0px solid;" src="http://www.econocampers.co.nz/images/EconoCampers/flags/Denmark%20flag.gif"></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="Banner">
                            <a id="ctl03_cRS_ctl01_ibBanner" class="BannerImage" href="http://www.facebook.com/pages/Econo-Campers/137313376314817"
                                target="_blank">
                                <img src="http://www.econocampers.co.nz/images/EconoCampers/_Banners/Find%20us%20on%20Facebook.gif" alt="Vote in our Facebook Competition"
                                    style="border-width: 0px;" /></a></div>
                        <div id="ctl03_cRS_ctl02_CustomCodeHolder" class="CustomCode">
                            <div class="SpecialOffers">
                                <table cellspacing="0" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td style="background-image: url(/images/econocampers/specialoffers1.jpg); width: 249px;
                                                height: 346px; background-repeat: no-repeat;">
                                                <p>
  
<div id="bookingWidget"></div>
<script type="text/javascript">
    //widget starts
    function loadwgt() {
        var options = { "brand": "e", "vehicleType": "campers", "country": "NZ", "serviceUrl": "http://test.econocampers.co.nz", "cssBase": "http://images.thl.com/css/e/widget.css", "expandUI": true, "months": 1, "startExpanded": true, "horizontal": false, "width": 240, "height": 500, "affId": "" }; //options
        initTHLWidget(options);
    }
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.onreadystatechange = function () {
        if (this.readyState == 'complete' || this.readyState == 'loaded'/*IE8*/) loadwgt();
    }
    script.onload = loadwgt;
    script.src = 'http://test.britz.com.au/js/wdgt.js';
    head.appendChild(script);
    //widget ends
</script>    
                            
                                           
                                                </p>
                                                
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td>
                                                <a href="/great-deals/pip-and-pete-special-offer" title="Pip and Pete Special Offer">
                                                    <img width="249" height="54" title="Book Now" alt="Book Now.jpg" src="http://www.econocampers.co.nz/images/EconoCampers/SpecialOffers2.jpg"
                                                        border="0"></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="Banner">
                            <a id="ctl03_cRS_ctl03_ibBanner" class="BannerImage" href="/bookings--quotes-1/about-our-rates">
                                <img src="http://www.econocampers.co.nz/images/EconoCampers/_Banners/HireABike.jpg" alt="About Our Rates" style="border-width: 0px;" /></a></div>
                        <div class="CategoryList">
                            <div class="ListHolder">
                                <div id="ctl03_cRS_ctl04_CategoryListWrapper" class="LatestInfo">
                                    <div id="ctl03_cRS_ctl04_CustomTitle" class="LatestInfoBanner">
                                        <h1 class="ListCustomTitle">
                                            Latest Info</h1>
                                    </div>
                                    <div class="ListItem">
                                        <p id="ctl03_cRS_ctl04_ItemList_ctl00_ListItem_ListSummary" class="ListSummary">
                                            * Holiday Grocery Boxes - 10% discount.
                                            <br />
                                            * Free Tourism Radio Guide.<br />
                                            Both ONLY when you book through Econo Campers... <a href="/great-deals" rel="nofollow">
                                                More&raquo;</a></p>
                                        <div class="ListEndItem">
                                        </div>
                                    </div>
                                </div>
                                <div class="ListClear">
                                </div>
                            </div>
                        </div>
                        <div class="Banner">
                            <a id="ctl03_cRS_ctl05_ibBanner" class="BannerImage" href="/travel-info/freedom-camping-our-way---love-nz">
                                <img src="http://www.econocampers.co.nz/images/EconoCampers/_Banners/WhereCanICamp_banner.jpg" style="border-width: 0px;" /></a></div>
                        <div class="Banner">
                            <a id="ctl03_cRS_ctl06_ibBanner" class="BannerImage" href="/great-deals">
                                <img src="http://www.econocampers.co.nz/images/EconoCampers/_Banners/AmazingDeals.jpg" alt="Amazing Deals" style="border-width: 0px;" /></a></div>
                    </div>
                </div>
                <!-- cRS = Container RIGHTSIDE - CODE#RIGHTSIDE - END -->
                <div class="clear">
                    &nbsp;</div>
            </div>
            <!-- end [ #mainWrapper] -->
            <div id="cCWrapper">
                <div id="cF">
                    <div style="position: relative;">
                        <div id="ctl03_cF_ctl00_NavigationContent">
                            <div class='NavigationUnique'>
                                <span class='navunique'><a href='/search'>Search</a></span> <span class='navunique'>
                                    <a href='/site-map'>Site Map</a></span> <span class='navunique'><a href='/admin'>Login</a></span>
                            </div>
                        </div>
                    </div>
                </div>
                <table border="0" cellpadding="0" width="240" align="left">
                    <tbody>
                        <tr>
                            <td style="border: 0px solid;">
                                <a href="http://www.facebook.com/pages/Auckland/Econo-Campers/137313376314817" title="Follow us on Facebook"
                                    class="ApplyClass" target="_blank">
                                    <img width="57" height="50" width="57" height="50" title="Facebook" alt="Facebook.gif"
                                        src="http://www.unitedcampervans.co.nz/images/_Banners/Facebook.gif" /></a><a title=""
                                            originalattribute="" class="ApplyClass"></a>
                            </td>
                            <!-- <td><a href="http://twitter.com/UnitedCampers" title="Follow us on Twitter" target="_blank"><img width="50" height="50" width="50" height="50" title="Twitter" alt="Twitter.gif" src="http://www.unitedcampervans.co.nz/images/_Banners/Twitter.gif" /></a><a href="/default.aspx?MenuId=731" title="German"></a></td>
            <td><a href="http://www.youtube.com/user/UNITED351" title="View our videos on YouTube" target="_blank"><img width="55" height="50" width="55" height="50" title="YouTube" alt="YouTube.gif" src="http://www.unitedcampervans.co.nz/images/_Banners/YouTube.gif" /></a><a href="/default.aspx?MenuId=729" title="Dutch"></a></td>
            <td> <a href="http://www.camping.org.nz/" target="_blank"><img width="55" height="50" width="55" height="50" title="CampingOurWay" style="border: 0px solid;" alt="CampingOurWay-icon.jpg" src="http://www.unitedcampervans.co.nz/images/_Banners/CampingOurWay-icon.jpg" /></a></td> -->
                        </tr>
                    </tbody>
                </table>
                <div id="cC">
                    &copy; 2008 Econo Campers &nbsp;<strong>www.econocampers.co.nz</strong><br />
                    <a href="http://www.website.co.nz" title="website design" target="_blank">Website Design</a>
                    by Labyrinth Solutions &nbsp;|&nbsp; <a href="http://www.contegro.com" title="website content management"
                        target="_blank">Content Management</a> by Contegro</div>
            </div>
        </div>
        <!-- end [ #outerWrapper] -->
    </div>
    <div id="lblVisitorReporting">
        <script type="text/javascript">
            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
        </script>
        <script type="text/javascript">
            try {
                var pageTracker = _gat._getTracker("UA-1215402-1");
                pageTracker._trackPageview();
            } catch (err) { }</script>
    </div>
    <script type="text/javascript">
//<![CDATA[
        function RandomBannerClickctl03_cTR_ctl00_ibBanner() { window.location = '/bookings--quotes-1/book-online'; };

        var RandomBannerImagesctl03_cTR_ctl00_ibBanner = new Array('/Images/EconoCampers/_Banners/RANDOM_Campervans/RANDOM-Campervan_01.jpg', '/Images/EconoCampers/_Banners/RANDOM_Campervans/RANDOM-Campervan_02.jpg', '/Images/EconoCampers/_Banners/RANDOM_Campervans/RANDOM-Campervan_03.jpg', '/Images/EconoCampers/_Banners/RANDOM_Campervans/RANDOM-Campervan_04.jpg', '/Images/EconoCampers/_Banners/RANDOM_Campervans/RANDOM-Campervan_05.jpg', '/Images/EconoCampers/_Banners/RANDOM_Campervans/RANDOM-Campervan_06.jpg');

        RandomBannerPreload(RandomBannerImagesctl03_cTR_ctl00_ibBanner);

        function QueueRandomBannerctl03_cTR_ctl00_ibBanner() {
            setTimeout(function () { RandomBannerChangectl03_cTR_ctl00_ibBanner(); }, 5000);
        }

        function RandomBannerChangectl03_cTR_ctl00_ibBanner() {
            var ImageObject = prototypeJs('ctl03_cTR_ctl00_ibBanner');
            var NewImageObject = RandomBannerNewImage(ImageObject, RandomBannerImagesctl03_cTR_ctl00_ibBanner, 2);
            NewImageObject.setStyle({ cursor: 'pointer' });
            Event.observe(NewImageObject, 'click', function () { RandomBannerClickctl03_cTR_ctl00_ibBanner(); });
            new Effect.Appear(NewImageObject, { duration: 1.0, afterFinish: function () {
                RandomBannerRemoveSibling(prototypeJs('ctl03_cTR_ctl00_ibBanner'));
                QueueRandomBannerctl03_cTR_ctl00_ibBanner();
            }
            });
        }

        document.observe('dom:loaded', function () { QueueRandomBannerctl03_cTR_ctl00_ibBanner(); });

        Sys.Application.initialize();
        Sys.Application.add_init(function () {
            $create(Telerik.Web.UI.RadAjaxManager, { "_updatePanels": "", "ajaxSettings": [], "clientEvents": { OnRequestStart: "", OnResponseEnd: "" }, "defaultLoadingPanelID": "", "enableAJAX": true, "enableHistory": false, "links": [], "styles": [], "uniqueID": "AjaxManager", "updatePanelsRenderMode": 0 }, null, null, $get("AjaxManager"));
        });
        Sys.Application.add_init(function () {
            $create(Telerik.Web.UI.RadAjaxLoadingPanel, { "initialDelayTime": 0, "isSticky": true, "minDisplayTime": 0, "skin": "", "transparency": 0, "uniqueID": "LoadingPanel", "zIndex": 90000 }, null, null, $get("LoadingPanel"));
        });
//]]>
    </script>
    <div style='display: none;'>
    </div>
    <div style='display: none;'>
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJMTgwNTAyNzYzDxYCHgpNZW51SXRlbUlkAtQDFgRmD2QWBgICDxYCHgRUZXh0ZWQCBA8WAh8BZWQCBQ8WAh8BZGQCAQ8WBB4FY2xhc3MFI2NvbnRlZ3JvX2ZpcmVmb3gxNiBjb250ZWdyb19maXJlZm94HgZhY3Rpb24FAS8WAgIFDw8WAh4XRW5hYmxlQWpheFNraW5SZW5kZXJpbmdoZBYCAgEPDxYCHghJbWFnZVVybAUzL1RlbXBsYXRlcy9FY29ub0NhbXBlcnMvX2ltYWdlcy9kZWZhdWx0X2xvYWRpbmcuZ2lmZGQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgEFIGN0bDAzJGNOJGN0bDAwJHJtU2xpZGVOYXZpZ2F0aW9u+QLhuQrwCmBSsOst2Zh4MjY889g=" />
    </div>
    </form>
</body>
</html>
