﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="United.aspx.cs" Inherits="UnitTests_United" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-NZ" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-NZ">
<head id="Header1">
    <title>United Campervans New Zealand Campervan Rentals NZ Motorhomes </title>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="index,follow" name="robots">
    <script src="http://www.unitedcampervans.co.nz/Resources/JavaScript/ContegroScriptaculous/lib/prototype.js" type="text/javascript"></script>
    <script src="http://www.unitedcampervans.co.nz/Resources/JavaScript/ContegroUIUtils.js" type="text/javascript"></script>
    <link class="Telerik_stylesheet" rel="stylesheet" type="text/css" href="http://unitedcampervans.co.nz/WebResource.axd?d=VbCohrPvzfi7ETwgAiTJ3G5sEsDACQvZCtqQUyazm_NC7IscyBlDKZ3FOFBm8XGeQnvYoWvrWZK9a2Xe-wkZZHyEFbSlu6HJffUecx03U21tNNDyMgturVs2S3Wo5zuL-tUCeg2&amp;t=634768365830603750">
    <link href="http://www.unitedcampervans.co.nz/WebResource.axd?d=b8lpWQoX_vDHRTBkiywZdPr998N2ESJLgLDaFJ0RiRnW1llpV2KvRgou6_NJpAM-0o8pUwThKrKctyJpLC_itUnggNkSG6Snia_U-434myTDYu0CaY6NbAgE6UHVLrj-6gWd3OSUR4NX13dl737Wqu3u8JM1&amp;amp;t=634768365822635000"
        rel="stylesheet" type="text/css">
    <link href="http://www.unitedcampervans.co.nz/Resources/RadControls/Menu/Skins/SlideHorizontal/styles.css" rel="stylesheet"
        type="text/css">
    <link rel="stylesheet" type="text/css" href="http://www.unitedcampervans.co.nz/Resources/StyleSheets/MICRO-United/DefaultTheme.css">
    <link rel="stylesheet" type="text/css" href="http://www.unitedcampervans.co.nz/Resources/StyleSheets/MICRO-United/Template.css">
    <link rel="stylesheet" type="text/css" href="http://www.unitedcampervans.co.nz/Resources/StyleSheets/MICRO-United/NavigationSlide.css">
    <link rel="stylesheet" type="text/css" href="http://www.unitedcampervans.co.nz/Resources/StyleSheets/MICRO-United/Content.css">
    <link rel="stylesheet" type="text/css" href="http://www.unitedcampervans.co.nz/Resources/StyleSheets/MICRO-United/BannerRandom.css">
    <link rel="stylesheet" type="text/css" href="http://www.unitedcampervans.co.nz/Resources/StyleSheets/MICRO-United/Search2.css">
    <link rel="stylesheet" type="text/css" href="http://www.unitedcampervans.co.nz/Resources/StyleSheets/MICRO-United/Banner.css">
    <link rel="stylesheet" type="text/css" href="http://www.unitedcampervans.co.nz/Resources/StyleSheets/MICRO-United/Navigation.css">
    <link rel="stylesheet" type="text/css" href="http://www.unitedcampervans.co.nz/Resources/StyleSheets/MICRO-United/CustomCode.css">
    <meta content="Top quality campervans &amp; motorhomes for hire in New Zealand.  Hot Deals and Great Specials for campervan hire in NZ."
        name="description">
    <meta content="About United Campervans New Zealand Campervan Rentals" name="keywords">
    <script type="text/javascript" src="http://test.britz.com.au/js/wdgt.js"></script>
</head>
<body>
    <form class="contegro_firefox15 contegro_firefox" id="Form1" action="/about-us/company-profile"
    method="post">
    <div>
        <input type="hidden" value="" id="__EVENTTARGET" name="__EVENTTARGET">
        <input type="hidden" value="" id="__EVENTARGUMENT" name="__EVENTARGUMENT">
    </div>
    <script type="text/javascript">
        //&lt;![CDATA[
        var theForm = document.forms['Form1'];
        if (!theForm) {
            theForm = document.Form1;
        }
        function __doPostBack(eventTarget, eventArgument) {
            if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
                theForm.__EVENTTARGET.value = eventTarget;
                theForm.__EVENTARGUMENT.value = eventArgument;
                theForm.submit();
            }
        }
        //]]&gt;
</script>
    <script type="text/javascript" src="http://www.unitedcampervans.co.nz/WebResource.axd?d=PfbI-AjfRFvntB5JHAsFKeVWyp_cucex7a146GlsDkPEVDihdaUisk23_3d4Qnh8SWjJ1PvkNnl2BQ-E1_EkmZFArSU1&amp;t=634605703982615616"></script>
    <script type="text/javascript" src="http://www.unitedcampervans.co.nz/Resources/JavaScript/Common.js"></script>
    <script type="text/javascript" src="http://www.unitedcampervans.co.nz/Resources/JavaScript/FireDefaultButtonFix.js"></script>
    <script type="text/javascript" src="http://www.unitedcampervans.co.nz/ScriptResource.axd?d=Hza0YhRfpaUXWBOr3hPcLiJR7LXN_ZX1dEr94BwwWNUVM7wX4YSbNiobAaUrF1eII5NM02IMB1Q1XwlsoflPXMW4mrxHYqxs4xBs3AhY3YHdLq0cPx0BTfOyxoqs2fVBR5Sy6nxYSX_rLCKame0pNxD-_Yo1&amp;t=ffffffff8dc250fc"></script>
    <script type="text/javascript" src="http://www.unitedcampervans.co.nz/ScriptResource.axd?d=FqLs9MoQXu7LXONg0lI9vRP2bIqtUbQdQ2DGj3rlZJMqa8P27OSSmr3hoBuTMgb7I77biGlrOUOCmxA3rUE0_1jM-RhVrPjmDfffId-7LsBcqUreFwb_rn3NIszK5k7P4oaerhfdWgi2bngxM6hdXdtEI_q73FJHP4OxbisYc8ibZu5q0&amp;t=ffffffff8dc250fc"></script>
    <script type="text/javascript" src="http://www.unitedcampervans.co.nz/ScriptResource.axd?d=PV8ByooKFiWkjufLrRZFtUUYCe_ovVm-cZMWBTMd9pOIFWGcwkTrBJXnfGkhEdJ3loC73nBijDCpDXXYk-2XZCthuBso_6IRjwU45WpwdncZQmY5kc6WlTnkkwPYOoa7PQZmxA2&amp;t=a6b9165"></script>
    <script type="text/javascript" src="http://www.unitedcampervans.co.nz/ScriptResource.axd?d=CPKQtI06kVdM0j8-WmBbVj7RAFHEgD0UW7lobvxHAE5iYCWf6GeML_r6z_3OXpm0qZnJY0JdS9Qjp-QBanOVd-a826qPUkL3v-5a38e4F64GqmgO0ysZLbQBr6hqzh83hX8Ecw2&amp;t=a6b9165"></script>
    <script type="text/javascript" src="http://www.unitedcampervans.co.nz/ScriptResource.axd?d=o94IN0_dWnD5w5fLHKpr81vI4-Fgke8csEZRnSnn5UqXPFCoFCYGlbgfFFC23EaPmeJuJtVSaMtbHGVg_Uj2MaA0H6WjvJrDcQXTCH_5dH5aIriVbz6clChWaDniEJg5Lv02lg2&amp;t=a6b9165"></script>
    <script type="text/javascript">
        //&lt;![CDATA[
        Sys.WebForms.PageRequestManager._initialize('ScriptManager', document.getElementById('Form1'));
        Sys.WebForms.PageRequestManager.getInstance()._updateControls(['tAjaxManagerSU'], [], [], 90);
        //]]&gt;
</script>
    <!-- 2011.2.920.35 -->
    <div id="AjaxManagerSU" style="display: none;">
        <span style="display: none;" id="AjaxManager"></span>
    </div>
    <div style="display: none; height: 75px; width: 75px; width: 320px; position: absolute;
        z-index: 2000;" id="LoadingPanel">
        <img style="border-width: 0px;" alt="Loading..." src="http://www.unitedcampervans.co.nz/Templates/MICRO-United/_images/default_loading.gif"
            id="LoadingImage">
    </div>
    <!-- TEMPLATE - 2Column -->
    <div id="topWrapper">
        <div id="topWrapper2">
            <!-- cN = Container NAVIGATION - CODE#NAVIGATION - START -->
            <div id="cN">
                <div style="position: relative;">
                    <div class="Navigation" id="ctl03_cN_ctl00_NavigationContent">
                        <div class="radmenu RadMenu_SlideHorizontal Nav" id="ctl03_cN_ctl00_rmSlideNavigation">
                            <!-- 4.2.0 -->
                            <script src="http://www.unitedcampervans.co.nz/WebResource.axd?d=c4fjCbJcUV_F77VZoXkkt0QW44gWXs-UZLScouzahbkJaBOyoCS8oJs__2RTbd-vcEFzs1Qg4yamXQHVCkwngKIAwLhuG49asw7eGv2omFhmKDwDee0e1C2CXBh4UHJrbGc6q3a7AicoEKn2nx7g9l0NWHU1&amp;t=634768365822635000"
                                type="text/javascript"></script>
                            <span style="display: none;" id="ctl03_cN_ctl00_rmSlideNavigationStyleSheetHolder">
                            </span>
                            <ul class="horizontal rootGroup">
                                <li class="item first"><a class="link " id="ctl03_cN_ctl00_rmSlideNavigation_m0"
                                    href="/"><span class="text expandTop">Home</span></a><div class="slide" style="z-index: 10;">
                                        <ul class="vertical group level1" style="z-index: 10;">
                                            <li class="item first last"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m0_m0"
                                                href="http://www.unitedcampervans.co.nz/home/united-in-papeete-2012"><span class="text">United in Papeete 2012</span></a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="item"><a class="link SelectedParent1" id="ctl03_cN_ctl00_rmSlideNavigation_m1"
                                    href="http://www.unitedcampervans.co.nz/about-us/company-profile"><span class="text expandTop">About Us</span></a><div
                                        class="slide" style="z-index: 12;">
                                        <ul class="vertical group level1" style="z-index: 12;">
                                            <li class="item first"><a class="link MenuSelected" id="ctl03_cN_ctl00_rmSlideNavigation_m1_m0"
                                                href="http://www.unitedcampervans.co.nz/about-us/company-profile"><span class="text">Company Profile</span></a></li><li
                                                    class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m1_m1" href="http://www.unitedcampervans.co.nz/about-us/our-promise">
                                                        <span class="text">Our Promise</span></a></li><li class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m1_m2"
                                                            href="http://www.unitedcampervans.co.nz/about-us/brochureunitedandalphacampervans"><span class="text">Brochures</span></a></li><li
                                                                class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m1_m3" href="/"><span
                                                                    class="text">Eco-Friendly</span></a><div class="slide">
                                                                        <ul class="vertical group level2">
                                                                            <li class="item first"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m1_m3_m0"
                                                                                href="http://www.unitedcampervans.co.nz/about-us/eco-friendly/earthcheck"><span class="text">EarthCheck</span></a></li><li
                                                                                    class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m1_m3_m1" href="/about-us/eco-friendly/motuora-project">
                                                                                        <span class="text">Motuora Project</span></a></li><li class="item last"><a class="link"
                                                                                            id="ctl03_cN_ctl00_rmSlideNavigation_m1_m3_m2" href="http://www.unitedcampervans.co.nz/about-us/eco-friendly/jasons-care-codes">
                                                                                            <span class="text">Jason’s Care Codes</span></a></li>
                                                                        </ul>
                                                                    </div>
                                                            </li>
                                            <li class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m1_m4" href="http://www.unitedcampervans.co.nz/about-us/depots/auckland">
                                                <span class="text">Depots</span></a><div class="slide">
                                                    <ul class="vertical group level2">
                                                        <li class="item first"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m1_m4_m0"
                                                            href="http://www.unitedcampervans.co.nz/about-us/depots/auckland"><span class="text">Auckland</span></a><div class="slide">
                                                                <ul class="vertical group level3">
                                                                    <li class="item first last"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m1_m4_m0_m0"
                                                                        href="http://www.unitedcampervans.co.nz/Rates-and-Bookings/United-and-Alpha-Terms-and-Conditions/default.aspx#transfersauckland">
                                                                        <span class="text">Complimentary Transfers</span></a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        <li class="item last"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m1_m4_m1"
                                                            href="http://www.unitedcampervans.co.nz/about-us/depots/christchurch"><span class="text">Christchurch</span></a><div
                                                                class="slide">
                                                                <ul class="vertical group level3">
                                                                    <li class="item first last"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m1_m4_m1_m0"
                                                                        href="http://www.unitedcampervans.co.nz/Rates-and-Bookings/United-and-Alpha-Terms-and-Conditions/default.aspx#transferschristchurch">
                                                                        <span class="text">Complimentary Transfers</span></a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="item last"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m1_m5"
                                                href="http://www.unitedcampervans.co.nz/about-us/testimonials"><span class="text">Testimonials</span></a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="item"><a class="link " id="ctl03_cN_ctl00_rmSlideNavigation_m2" href="http://www.unitedcampervans.co.nz/united-campervans-special-offers">
                                    <span class="text expandTop">United Campervans Special Offers</span></a><div class="slide"
                                        style="z-index: 26;">
                                        <ul class="vertical group level1" style="z-index: 26;">
                                            <li class="item first"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m2_m0"
                                                href="http://www.unitedcampervans.co.nz/united-campervans-special-offers/300dollardiscount"><span class="text">TAKE NZ$300
                                                    OFF</span></a></li><li class="item last"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m2_m1"
                                                        href="http://www.unitedcampervans.co.nz/united-campervans-special-offers/freedom-days-with-united"><span class="text">
                                                            FREEDOM DAYS WITH UNITED</span></a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="item"><a class="link " id="ctl03_cN_ctl00_rmSlideNavigation_m3" href="/rates-and-bookings/unitedcampervansratesto2013">
                                    <span class="text expandTop">Rates &amp; Bookings</span></a><div class="slide" style="z-index: 29;">
                                        <ul class="vertical group level1" style="z-index: 29;">
                                            <li class="item first"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m3_m0"
                                                href="http://www.unitedcampervans.co.nz/rates-and-bookings/unitedcampervansratesto2013"><span class="text">Rates</span></a><div
                                                    class="slide">
                                                    <ul class="vertical group level2">
                                                        <li class="item first last"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m3_m0_m0"
                                                            href="http://www.unitedcampervans.co.nz/rates-and-bookings/unitedcampervansratesto2013/whats-included-in-the-rates">
                                                            <span class="text">What's included in the rates</span></a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m3_m1" href="/rates-and-bookings/bookings">
                                                <span class="text">Bookings</span></a></li><li class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m3_m2"
                                                    href="http://www.unitedcampervans.co.nz/rates-and-bookings/extras"><span class="text">Extra Items for Hire</span></a></li><li
                                                        class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m3_m3" href="/rates-and-bookings/payment">
                                                            <span class="text">Payment</span></a></li><li class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m3_m4"
                                                                href="http://www.unitedcampervans.co.nz/rates-and-bookings/cancellations-and-amendments"><span class="text">Cancellations
                                                                    &amp; Amendments</span></a></li><li class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m3_m5"
                                                                        href="http://www.unitedcampervans.co.nz/rates-and-bookings/group-travel"><span class="text">Group Travel</span></a></li><li
                                                                            class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m3_m6" href="/rates-and-bookings/bicycles-for-hire">
                                                                                <span class="text">Bicycles for Hire</span></a><div class="slide">
                                                                                    <ul class="vertical group level2">
                                                                                        <li class="item first last"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m3_m6_m0"
                                                                                            href="http://www.unitedcampervans.co.nz/rates-and-bookings/bicycles-for-hire/bicycle-itineraries"><span class="text">
                                                                                                Bicycle Itineraries</span></a></li>
                                                                                    </ul>
                                                                                </div>
                                                                        </li>
                                            <li class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m3_m7" href="http://www.unitedcampervans.co.nz/rates-and-bookings/faqs">
                                                <span class="text">FAQs</span></a></li><li class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m3_m8"
                                                    href="http://www.unitedcampervans.co.nz/rates-and-bookings/living-equipment"><span class="text">Living Equipment</span></a></li><li
                                                        class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m3_m9" href="http://www.unitedcampervans.co.nz/rates-and-bookings/united-and-alpha-terms-and-conditions">
                                                            <span class="text">United &amp; Alpha Terms and Conditions</span></a></li><li class="item last">
                                                                <a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m3_m10" href="http://www.unitedcampervans.co.nz/rates-and-bookings/insurance">
                                                                    <span class="text">Insurance</span></a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="item"><a class="link " id="ctl03_cN_ctl00_rmSlideNavigation_m4" href="http://www.unitedcampervans.co.nz/itineraries/freedom-tours">
                                    <span class="text expandTop">Itineraries</span></a><div class="slide" style="z-index: 43;">
                                        <ul class="vertical group level1" style="z-index: 43;">
                                            <li class="item first"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m4_m0"
                                                href="http://www.unitedcampervans.co.nz/itineraries/freedom-tours"><span class="text">Freedom Tours</span></a></li><li
                                                    class="item last"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m4_m1" href="http://www.unitedcampervans.co.nz/itineraries/itinerary-planning--booking">
                                                        <span class="text">Itinerary Planning &amp; Booking</span></a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="item"><a class="link " id="ctl03_cN_ctl00_rmSlideNavigation_m5" href="http://www.unitedcampervans.co.nz/motorhome-fleet">
                                    <span class="text expandTop">Motorhome Fleet</span></a><div class="slide" style="z-index: 46;">
                                        <ul class="vertical group level1" style="z-index: 46;">
                                            <li class="item first"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m5_m0"
                                                href="http://www.unitedcampervans.co.nz/motorhome-fleet/vehicle-comparison-table"><span class="text">Vehicle Comparison
                                                    Table</span></a></li><li class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m5_m1"
                                                        href="http://www.unitedcampervans.co.nz/motorhome-fleet/united-6-premier-motorhome"><span class="text">United 6 Premier
                                                            Motorhome</span></a><div class="slide">
                                                                <ul class="vertical group level2">
                                                                    <li class="item first last"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m5_m1_m0"
                                                                        href="http://www.unitedcampervans.co.nz/motorhome-fleet/united-6-premier-motorhome/vehicle-specifications-1"><span
                                                                            class="text">Vehicle Specifications</span></a></li>
                                                                </ul>
                                                            </div>
                                                    </li>
                                            <li class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m5_m2" href="http://www.unitedcampervans.co.nz/motorhome-fleet/united-4-premier-motorhome">
                                                <span class="text">United 4 Premier Motorhome</span></a><div class="slide">
                                                    <ul class="vertical group level2">
                                                        <li class="item first last"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m5_m2_m0"
                                                            href="http://www.unitedcampervans.co.nz/motorhome-fleet/united-4-premier-motorhome/vehicle-specifications"><span class="text">
                                                                Vehicle Specifications</span></a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m5_m3" href="http://www.unitedcampervans.co.nz/motorhome-fleet/united-21-premier-automatic">
                                                <span class="text">United 2+1 Premier Automatic</span></a><div class="slide">
                                                    <ul class="vertical group level2">
                                                        <li class="item first last"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m5_m3_m0"
                                                            href="http://www.unitedcampervans.co.nz/motorhome-fleet/united-21-premier-automatic/vehicle-specifications"><span
                                                                class="text">Vehicle Specifications</span></a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m5_m4" href="http://www.unitedcampervans.co.nz/motorhome-fleet/united-2-premier-motorhome">
                                                <span class="text">United 2 Premier Motorhome</span></a><div class="slide">
                                                    <ul class="vertical group level2">
                                                        <li class="item first last"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m5_m4_m0"
                                                            href="http://www.unitedcampervans.co.nz/motorhome-fleet/united-2-premier-motorhome/vehicle-specifications-1"><span
                                                                class="text">Vehicle Specifications</span></a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="item last"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m5_m5"
                                                target="_blank" href="http://www.unitedsalescentre.co.nz"><span class="text">CAMPERVANS
                                                    FOR SALE</span></a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="item"><a class="link " id="ctl03_cN_ctl00_rmSlideNavigation_m6" href="http://www.unitedcampervans.co.nz/travel-info/about-new-zealand">
                                    <span class="text expandTop">Travel Info</span></a><div class="slide" style="z-index: 57;">
                                        <ul class="vertical group level1" style="z-index: 57;">
                                            <li class="item first"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m6_m0"
                                                href="http://www.unitedcampervans.co.nz/travel-info/about-new-zealand"><span class="text">About New Zealand</span></a></li><li
                                                    class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m6_m1" href="http://www.unitedcampervans.co.nz/travel-info/driving-in-new-zealand">
                                                        <span class="text">Driving in New Zealand</span></a></li><li class="item"><a class="link"
                                                            id="ctl03_cN_ctl00_rmSlideNavigation_m6_m2" href="http://www.unitedcampervans.co.nz/travel-info/ferry-information_united-campervans">
                                                            <span class="text">Ferry Information</span></a><div class="slide">
                                                                <ul class="vertical group level2">
                                                                    <li class="item first last"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m6_m2_m0"
                                                                        href="http://www.unitedcampervans.co.nz/travel-info/ferry-information_united-campervans/interislander-timetable">
                                                                        <span class="text">Interislander Timetable</span></a></li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                            <li class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m6_m3" href="http://www.unitedcampervans.co.nz/travel-info/where-you-can-camp-1">
                                                <span class="text">Where you can camp</span></a></li><li class="item"><a class="link"
                                                    id="ctl03_cN_ctl00_rmSlideNavigation_m6_m4" href="http://www.unitedcampervans.co.nz/travel-info/sightseeing-activities">
                                                    <span class="text">Sightseeing Activities</span></a></li><li class="item"><a class="link"
                                                        id="ctl03_cN_ctl00_rmSlideNavigation_m6_m5" href="http://www.unitedcampervans.co.nz/travel-info/currency-calculator">
                                                        <span class="text">Currency Calculator</span></a></li><li class="item"><a class="link"
                                                            id="ctl03_cN_ctl00_rmSlideNavigation_m6_m6" href="http://www.unitedcampervans.co.nz/travel-info/distance-calculator">
                                                            <span class="text">Distance Calculator</span></a></li><li class="item"><a class="link"
                                                                id="ctl03_cN_ctl00_rmSlideNavigation_m6_m7" href="http://www.unitedcampervans.co.nz/travel-info/tourism-radio"><span
                                                                    class="text">Tourism Radio</span></a></li><li class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m6_m8"
                                                                        href="http://www.unitedcampervans.co.nz/travel-info/holiday-grocery-boxes"><span class="text">Holiday Grocery Boxes</span></a></li><li
                                                                            class="item"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m6_m9" href="http://www.unitedcampervans.co.nz/travel-info/useful-contacts">
                                                                                <span class="text">Useful Contacts</span></a></li><li class="item last"><a class="link"
                                                                                    id="ctl03_cN_ctl00_rmSlideNavigation_m6_m10" href="http://www.unitedcampervans.co.nz/travel-info/other-travel-links">
                                                                                    <span class="text">Other Travel Links</span></a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="item last"><a class="link " id="ctl03_cN_ctl00_rmSlideNavigation_m7" href="http://www.unitedcampervans.co.nz/contact-us">
                                    <span class="text expandTop">Contact Us</span></a><div class="slide" style="z-index: 70;">
                                        <ul class="vertical group level1" style="z-index: 70;">
                                            <li class="item first"><a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m7_m0"
                                                href="http://www.unitedcampervans.co.nz/contact-us/auckland"><span class="text">Auckland</span></a></li><li class="item">
                                                    <a class="link" id="ctl03_cN_ctl00_rmSlideNavigation_m7_m1" href="http://www.unitedcampervans.co.nz/contact-us/christchurch">
                                                        <span class="text">Christchurch</span></a></li><li class="item last"><a class="link"
                                                            id="ctl03_cN_ctl00_rmSlideNavigation_m7_m2" href="http://www.unitedcampervans.co.nz/contact-us"><span class="text">Enquiry
                                                                Form</span></a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                            <input type="hidden" name="ctl03$cN$ctl00$rmSlideNavigation" id="ctl03_cN_ctl00_rmSlideNavigation_Hidden"><script
                                type="text/javascript">                                                                                                                                          window["ctl03_cN_ctl00_rmSlideNavigation"] = RadMenu.Create("ctl03_cN_ctl00_rmSlideNavigation"); window["ctl03_cN_ctl00_rmSlideNavigation"].Initialize({ "Skin": "SlideHorizontal", "CausesValidation": false, "Enabled": true }, { "ctl03_cN_ctl00_rmSlideNavigation_m1": { "CssClass": "SelectedParent1" }, "ctl03_cN_ctl00_rmSlideNavigation_m1_m0": { "CssClass": "MenuSelected"} });</script>
                        </div>
                    </div>
                </div>
                <div class="clear">
                    &nbsp;</div>
            </div>
            <!-- cN = Container NAVIGATION - CODE#NAVIGATION - END -->
            <div id="cH">
                <div class="cLogo">
                    <a title="Back to home page" href="/default.aspx">
                        <img style="display: block; border: none" alt="United Campervans" src="http://www.unitedcampervans.co.nz/Templates/MICRO-United/_images/LogoU.jpg"></a></div>
                <div class="clear">
                    &nbsp;</div>
            </div>
        </div>
        <div id="outerWrapper">
            <div class="layout2" id="mainWrapper">
                <div id="cPCWrapper">
                    <!-- cPC = Container PRIMARYCONTAINER - CODE#PRIMARYCONTAINER - START -->
                    <div class="Content" id="cPC">
                        <div style="position: relative;">
                            <div class="Banner">
                            </div>
                            <div class="Breadcrumb" id="ctl03_cPC_ctl01_divBreadcrumbTrail">
                                <a class="BreadCrumb" href="http://www.unitedcampervans.co.nz/about-us/company-profile">About Us</a><a class="BreadCrumb"
                                    href="http://www.unitedcampervans.co.nz/about-us/company-profile">Company Profile</a></div>
                            <div class="HTMLContent" id="ctl03_cPC_ctl02_HtmlContent">
                                
                                
                                </p>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td valign="top" align="left" style="width: 20%;">
                                                <img width="83" height="100" src="http://www.unitedcampervans.co.nz/Images/_Banners/Logos/Qualmark logo 2011.jpg"
                                                    alt="Qualmark logo 2011.jpg" title="Qualmark logo 2011">
                                            </td>
                                            <td>
                                                <p>
                                                    Since its creation in 1991, the United brand has been synonymous with a standard
                                                    of product and service that has earned us an enviable international reputation from
                                                    travel industry partners and hirers alike.
                                                </p>
                                                <p>
                                                    This reputation has been recognised by two industry ’firsts’. In April 2003, United
                                                    Vehicle Rentals Ltd became the first motorhome company to receive New Zealand tourism’s
                                                    official quality standard, ‘Qualmark’, and in October 2004 we became the first rental
                                                    vehicle operator in the world to achieve the prestigious Green Globe/EarthCheck
                                                    Benchmarked Certification standard.</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <p>
                                </p>
                                <p class="Highlight-Top">
                                    EarthCheck</p>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td style="width: 20%;">
                                                <p>
                                                    <a target="_blank" title="Learn more about EarthCheck " href="http://http://www.earthcheck.org/">
                                                    </a>
                                                    <img width="88" height="130" src="http://www.unitedcampervans.co.nz/Images/EarthCheck_Gold_Certified company_2011.jpg"
                                                        alt="EarthCheck_Gold_Certified company_2011.jpg" title="EarthCheck Gold_Certified company_2011"><a
                                                            shape="rect" title="Green Globe" href="http://www.unitedcampervans.co.nz/"></a></p>
                                            </td>
                                            <td>
                                                <p>
                                                    EarthCheck &nbsp;is based on Agenda 21 and principles for Sustainable Development
                                                    endorsed by 182 Heads of State at the United Nations Rio de Janeiro Earth Summit
                                                    in 1992.</p>
                                                <p>
                                                    It provides companies, communities and consumers with a path to sustainable development
                                                    within the travel and tourism industry worldwide.</p>
                                                <p>
                                                    Our commitment to the EarthCheck programme is ongoing, and since our world first
                                                    in achieving Green Globe/EarthCheck Benchmarked status&nbsp;we have&nbsp;worked
                                                    towards and in 2011 achieved prestigious Gold Certified level.&nbsp; We currently
                                                    remain the only motorhome rental company in New Zealand to hold EarthCheck Gold
                                                    Certification.</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <p class="Highlight-Top">
                                    Motuora Island Reforestation Project</p>
                                <p>
                                    <strong><span style="color: #666666;"><a target="_self" shape="rect" title="Motuora Island Reforestation Project"
                                        href="http://www.unitedcampervans.co.nz/About-Us/Eco-Friendly/Motuora-Project/MenuId/154.aspx">
                                        <img complete="complete" src="http://unitedcampervans.co.nz/images/_Banners/Logos/LogoSml_Motuora.jpg" alt="Logo Motuora"
                                            title="Logo Motuora" style="margin: 10px; width: 104px; float: right; height: 94px;
                                            border: 0px solid;"></a></span></strong></p>
                                <p>
                                    We see it as our responsibility to minimise the impact our business and our vehicles
                                    have on the environment in New Zealand and globally, and to encourage our clients
                                    likewise. To this end, since 2006 United has been involved in the <a target="_self"
                                        shape="rect" title="Motuora Island Reforestation Project" href="http://unitedcampervans.co.nz/About-Us/Eco-Friendly/Motuora-Project/MenuId/154.aspx">
                                        Motuora Island Reforestation Project</a> in Auckland’s beautiful Hauraki Gulf,
                                    by sponsoring the annual planting of 30,000 native trees on the island.
                                </p>
                                <p>
                                    Motuora Island serves not only as a sanctuary for endangered birds, reptiles and
                                    insects, but also as a nursery where Kiwi chicks can safely grow to maturity before
                                    being released back into their original habitat on the mainland. Through this support,
                                    and a number of other initiatives, we acknowledge our environmental responsibility
                                    and take a giant step towards offsetting the negative impact of emissions from a
                                    fleet in excess of 430 vehicles.
                                </p>
                                <p>
                                    As a hirer of a United vehicle you will immediately contribute to a better environment;
                                    every motorhome hired from United&nbsp;means more trees are planted on Motuora Island.&nbsp;</p>
                                <p>
                                    Travel with the organisation that cares and you will soon discover that hiring a
                                    motorhome from&nbsp;a company large enough to supply the superior quality of vehicle
                                    you expect, yet small enough to deliver exceptional personal service and value,
                                    will make a real difference to planning the perfect holiday.
                                </p>
                                <p class="Detail-Link">
                                    We invite you to <a target="_self" shape="rect" title="Motorhome Bookings" href="http://unitedcampervans.co.nz/Rates--amp;-Bookings/Bookings/MenuId/167.aspx">
                                        book with United Vehicle Rentals</a> and look forward to meeting you in New
                                    Zealand.</p>
                            </div>
                        </div>
                    </div>
                    <!-- cPC = Container PRIMARYCONTAINER - CODE#PRIMARYCONTAINER - END -->
                </div>
                <!-- cRS = Container RIGHTSIDE - CODE#RIGHTSIDE - START -->
                <div id="cLS">
                    <table cellpadding="5" border="0" align="center">
                        <tbody>
                            <tr>
                                <td style="border: 0px solid;">
                                    <a class="ApplyClass" href="/default.aspx?MenuId=737">
                                        <img width="24" height="24" src="http://unitedcampervans.co.nz/images/France%20flag.png"></a>
                                </td>
                                <td>
                                    <a href="/default.aspx?MenuId=731">
                                        <img width="24" height="24" src="http://unitedcampervans.co.nz/images/Germany%20flag.png"></a>
                                </td>
                                <td>
                                    <a href="/default.aspx?MenuId=729">
                                        <img width="24" height="24" src="http://unitedcampervans.co.nz/images/Netherlands%20flag.png"></a>
                                </td>
                                <td>
                                    <a href="/default.aspx?MenuId=730">
                                        <img width="24" height="24" src="http://unitedcampervans.co.nz/images/Italy%20flag.png"></a>
                                </td>
                                <td>
                                    <a href="/default.aspx?MenuId=732">
                                        <img width="24" height="24" src="http://unitedcampervans.co.nz/images/Spain%20flag.png"></a>
                                </td>
                                <td>
                                    <a href="/default.aspx?MenuId=728">
                                        <img width="24" height="24" src="http://unitedcampervans.co.nz/images/Denmark%20flag.png"></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div style="position: relative;">
                        <div style="position: relative;" class="BannerRandom">
                        </div>
                        <div class="Navigation" id="ctl03_cLS_ctl01_NavigationContent">
                            <span class="NavBlock"><span class="navitem1"><a class="MenuSelected" href="http://unitedcampervans.co.nz/about-us/company-profile">
                                Company Profile</a></span> <span class="navitem1"><a href="http://unitedcampervans.co.nz/about-us/our-promise">Our
                                    Promise</a></span> <span class="navitem1"><a href="http://unitedcampervans.co.nz/about-us/brochureunitedandalphacampervans">
                                        Brochures</a></span> <span class="navitem1"><a href="/">Eco-Friendly</a></span>
                                <span class="navitem1"><a href="http://unitedcampervans.co.nz/about-us/depots/auckland">Depots</a></span> <span
                                    class="navitem1"><a href="http://unitedcampervans.co.nz/about-us/testimonials">Testimonials</a></span>
                            </span>
                        </div>
                        <div onkeypress="javascript:return WebForm_FireDefaultButton(event, 'ctl03_cLS_ctl02_SearchButton')"
                            class="Search" id="ctl03_cLS_ctl02_SearchPanel">
                            <div class="KeywordField" id="ctl03_cLS_ctl02_KeywordField">
                                <div class="FieldLabel" id="ctl03_cLS_ctl02_KeywordFieldLabel" style="display: none;">
                                    Search</div>
                                <div class="FormField">
                                    <input type="text" id="ctl03_cLS_ctl02_Keywords" name="ctl03$cLS$ctl02$Keywords"
                                        style="display: none;"><input type="text" id="ctl03_cLS_ctl02_Keywords_inlinelabel"
                                            name="ctl03$cLS$ctl02$Keywords_inlinelabel" class="InlineLabel"></div>
                            </div>
                            <div class="FormButtons">
                                <input type="image" style="border-width: 0px;" alt="Search" src="http://unitedcampervans.co.nz/Images/MICRO-United/button_go.gif"
                                    title="Search" id="ctl03_cLS_ctl02_SearchButton" name="ctl03$cLS$ctl02$SearchButton"></div>
                        </div>
                        <div class="Banner">
                            <a href="/" class="BannerImage" id="ctl03_cLS_ctl03_ibBanner">
                                <img style="border-width: 0px;" src="http://unitedcampervans.co.nz/images/_Banners/Advert-United.jpg"></a></div>
                        <div class="Banner" style="margin-left: -10px;">
                            <div id="bookingWidget" style="margin:20px 20px 0 0">
                                <div style="width: 240px; overflow: hidden;" id="frameContainer">
                                    <iframe width="240px" scrolling="no" height="500px" frameborder="0" src="http://local.unitedcampers/GenericWidget.aspx?vt=campers&amp;cr=undefined&amp;cc=NZ&amp;brand=u&amp;months=1&amp;cssb=http://images.thl.com/css/u/widget.css"
                                        style="height: 500px; width: 240px;" id="thlControl"></iframe>
                                </div>
                            </div>
                            <script type="text/javascript">
                                //widget starts
                                function loadwgt() {
                                    var options = { "brand": "u", "vehicleType": "campers", "country": "NZ", "serviceUrl": "http://test.unitedcampers.co.nz", "cssBase": "http://images.thl.com/css/u/widget.css", "expandUI": true, "months": 1, "startExpanded": true, "horizontal": false, "width": 240, "height": 500, "affId": "" }; //options
                                    initTHLWidget(options);
                                }
                                var head = document.getElementsByTagName('head')[0];
                                var script = document.createElement('script');
                                script.type = 'text/javascript';
                                script.onreadystatechange = function () {
                                    if (this.readyState == 'complete' || this.readyState == 'loaded'/*IE8*/) loadwgt();
                                }
                                script.onload = loadwgt;
                                script.src = 'http://test.britz.com.au/js/wdgt.js';
                                head.appendChild(script);
                                //widget ends
                            </script>
                        </div>
                    </div>
                </div>
                <!-- cRS = Container RIGHTSIDE - CODE#RIGHTSIDE - END -->
                <div class="clear">
                    &nbsp;</div>
            </div>
            <!-- end [ #mainWrapper] -->
            <!-- cT = Container TOP - CODE#TOP - START -->
            <div id="cT">
                <div style="position: relative;">
                    <div id="ctl03_cT_ctl00_NavigationContent">
                        <div class="NavigationUnique">
                            <span class="navunique"><a href="/">Home</a></span> <span class="navunique"><a href="/admin">
                                Login</a></span> <span class="navunique"><a href="/sitemap">Sitemap</a></span>
                            <span class="navunique"><a href="/search">Search</a></span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- cT = Container TOP - CODE#TOP - END -->
            <div class="cLogoQualmark">
                <a title="Qualmark" href="http://www.qualmark.co.nz">
                    <img style="display: block; border: none" alt="Qualmark" src="http://unitedcampervans.co.nz/Templates/MICRO-United/_images/Logo_qualmark.gif"></a></div>
            <div class="cLogoMotuora">
                <a title="Motuora Project" href="http://www.unitedcampervans.co.nz/About-Us/Eco-Friendly/Motuora-Project/MenuId/154.aspx">
                    <img style="display: block; border: none" alt="Motuora Project" src="http://unitedcampervans.co.nz/Templates/MICRO-United/_images/Logo_Motuora.gif"></a></div>
            <div class="cLogoGreen">
                <a title="Green Globe" href="http://www.unitedcampervans.co.nz/About-Us/Eco-Friendly/Green-Globe/MenuId/152.aspx ">
                    <img style="display: block; border: none" alt="Green Globe" src="http://unitedcampervans.co.nz/Templates/MICRO-United/_images/Logo_GreenGlobe.gif"></a></div>
            <!-- cF = Container FOOTER - CODE#FOOTER - START -->
            <div id="cF">
                <div style="position: relative;">
                    <div class="CustomCode" id="ctl03_cF_ctl00_CustomCodeHolder">
                        <p>
                        </p>
                        <table cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="border: 0px solid;">
                                        <a target="_blank" class="ApplyClass" title="Follow us on Facebook" href="http://www.facebook.com/pages/Auckland/United-Campervans/125805864113422">
                                            <img width="57" height="50" src="http://www.unitedcampervans.co.nz/images/_Banners/Facebook.gif"
                                                alt="Facebook.gif" title="Facebook"></a><a class="ApplyClass" title="" originalattribute=""></a>
                                    </td>
                                    <td>
                                        <a target="_blank" title="Follow us on Twitter" href="http://twitter.com/UnitedCampers">
                                            <img width="50" height="50" src="http://www.unitedcampervans.co.nz/images/_Banners/Twitter.gif"
                                                alt="Twitter.gif" title="Twitter"></a><a title="German" href="http://unitedcampervans.co.nz/foreign-languages/german---deutsch"></a>
                                    </td>
                                    <td>
                                        <a target="_blank" title="View our videos on YouTube" href="http://www.youtube.com/user/UNITED351">
                                            <img width="55" height="50" src="http://www.unitedcampervans.co.nz/images/_Banners/YouTube.gif"
                                                alt="YouTube.gif" title="YouTube"></a><a title="Dutch" href="http://unitedcampervans.co.nz/foreign-languages/dutch---nederlands"></a>
                                    </td>
                                    <td>
                                        <a target="_blank" href="http://www.camping.org.nz/">
                                            <img width="55" height="50" src="http://www.unitedcampervans.co.nz/images/_Banners/CampingOurWay-icon.jpg"
                                                alt="CampingOurWay-icon.jpg" style="border: 0px solid;" title="CampingOurWay"></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p>
                        </p>
                    </div>
                </div>
                <br>
                &copy; 2007 United Campervans &mdash; www.unitedcampervans.co.nz</div>
            <!-- cF = Container FOOTER - CODE#FOOTER - END -->
            <div id="cC">
                <a rel="nofollow" class="TargetBlank" target="_blank" title="website design" href="http://www.website.co.nz">
                    Website Design</a> by Labyrinth Solutions &nbsp;|&nbsp;<a rel="nofollow" class="TargetBlank"
                        target="_blank" title="website content management" href="http://www.contegro.com">Content
                        Management</a> by Contegro</div>
        </div>
        <!-- end [ #outerWrapper] -->
    </div>
    <!-- end [ #topWrapper] -->
    <div id="lblVisitorReporting">
        <script type="text/javascript">
            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
        <script type="text/javascript" src="http://www.google-analytics.com/ga.js"></script>
        <script type="text/javascript">
            var pageTracker = _gat._getTracker("UA-1215402-4");
            pageTracker._initData();
            pageTracker._trackPageview();
</script>
    </div>
    <script type="text/javascript">
        //&lt;![CDATA[
        ContegroUIUtils.makeTextboxLabelInLine('ctl03_cLS_ctl02_Keywords', 'ctl03_cLS_ctl02_KeywordFieldLabel'); Sys.Application.initialize();
        Sys.Application.add_init(function () {
            $create(Telerik.Web.UI.RadAjaxManager, { "_updatePanels": "", "ajaxSettings": [], "clientEvents": { OnRequestStart: "", OnResponseEnd: "" }, "defaultLoadingPanelID": "", "enableAJAX": true, "enableHistory": false, "links": [], "styles": [], "uniqueID": "AjaxManager", "updatePanelsRenderMode": 0 }, null, null, $get("AjaxManager"));
        });
        Sys.Application.add_init(function () {
            $create(Telerik.Web.UI.RadAjaxLoadingPanel, { "initialDelayTime": 0, "isSticky": true, "minDisplayTime": 0, "skin": "", "transparency": 0, "uniqueID": "LoadingPanel", "zIndex": 90000 }, null, null, $get("LoadingPanel"));
        });
        //]]&gt;
</script>
    <div style="display: none;">
    </div>
    <div style="display: none;">
        <input type="hidden" value="/wEPDwUJMTgwNTAyNzYzDxYCHgpNZW51SXRlbUlkApUBFgRmD2QWBgICDxYCHgRUZXh0ZWQCBA8WAh8BZWQCBQ8WAh8BZGQCAQ8WBB4FY2xhc3MFI2NvbnRlZ3JvX2ZpcmVmb3gxNSBjb250ZWdyb19maXJlZm94HgZhY3Rpb24FGS9hYm91dC11cy9jb21wYW55LXByb2ZpbGUWAgIFDw8WAh4XRW5hYmxlQWpheFNraW5SZW5kZXJpbmdoZBYCAgEPDxYCHghJbWFnZVVybAUzL1RlbXBsYXRlcy9NSUNSTy1Vbml0ZWQvX2ltYWdlcy9kZWZhdWx0X2xvYWRpbmcuZ2lmZGQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgIFIGN0bDAzJGNOJGN0bDAwJHJtU2xpZGVOYXZpZ2F0aW9uBRxjdGwwMyRjTFMkY3RsMDIkU2VhcmNoQnV0dG9uhxmdCHe76cvYOyNFFvff70nxJEw="
            id="__VIEWSTATE" name="__VIEWSTATE">
    </div>
    </form>
</body>
</html>
