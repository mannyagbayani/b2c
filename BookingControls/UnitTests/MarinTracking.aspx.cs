﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class UnitTests_MarinTracking : System.Web.UI.Page
{

    public String Out;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        string marinParams = Request.Params["marinParams"];
        TrackingDisplayer td = new TrackingDisplayer(new THLDomain("britz.com.au", THLBrands.Britz, CountryCode.AU,"B2CNZ",  false,"test-ua"), false);
        Out = td.RenderMarinTrackingPixelIMG(marinParams, MarinStep.SELECTION, 3100, "B63121", string.Empty);
    }
}