﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class UnitTests_Preload : System.Web.UI.Page
{
    public string HTTPPrefix, BrandChar, ParentSiteURL, VehicleImagePath, VehicleTypeStr, PreloaderImagePath;

    public string AssetsURL, BrandCountryAssetPath;
    public string VehicleImageConvension;
    AvailabilityRequest aRequest;
    public string debugSTR;

    public AvailabilityItem AItem;

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionData sessionData = SessionManager.GetSessionData();
        //TODO: verify Session availability to procceed from here
        aRequest = SessionManager.GetSessionData().AvailabilityRequestData;
        HTTPPrefix = ApplicationManager.HTTPPrefix;
       
        if (Request.Params["ac"] != null && Request.Params["ac"] != string.Empty)
            SessionManager.AgentCode = Request.Params["ac"].ToUpper();

        THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
        string brandStr = appDomain.BrandChar.ToString();

        if (appDomain == null)//dubug line
            appDomain = new THLDomain("secure.maui.co.nz", THLBrands.Maui, CountryCode.NZ, "B2CNZ", false, "uahere");//dubug line
        BrandChar = appDomain.BrandChar.ToString(); //(Request.Params["brand"] != null ? Request.Params["brand"] : "m");//TODO: this should be swapped by baseURL. 
        ParentSiteURL = appDomain.ParentSite;
        string countryCode = appDomain.Country.ToString().ToUpper();

        AssetsURL = HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];
        BrandCountryAssetPath = THLBrand.GetNameForBrand(appDomain.Brand) + "/" + AvailabilityHelper.GetCountryName(appDomain.Country); //"Britz/Australia"
        AItem = sessionData.AvailabilityResponseData.GetAvailabilityItemForVehicleCode(Request.Params["vCode"]);
        VehicleTypeStr = (aRequest.VehicleType == VehicleType.Car ? "Cars" : "Campervans");
        VehicleImageConvension = aRequest.CountryCode.ToString().ToLower() + AvailabilityHelper.GetAuroraVehicleStrForVehicleType(aRequest.VehicleType) + brandStr + "." + AItem.VehicleCode;
        VehicleImagePath = AssetsURL + "/CentralLibraryImages/" + BrandCountryAssetPath + "/" + VehicleTypeStr + "/" + VehicleImageConvension + "/" + VehicleImageConvension;
        PreloaderImagePath = AssetsURL + "/images/" +brandStr + "/tiles/ads";
             
    }



}
