﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AnalyticsTest.aspx.cs" Inherits="UnitTests_AnalyticsTest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Tracking Code Test</title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            jQuery(document).ready(function() { $('a.bookingLink').click(function() { pageTracker._link($(this).attr('href')); return false; }); });
        </script>
    </head>
    <body>
        <form id="form1" runat="server">
            <div>
                <a href="http://www.targetbookingsite.com" class='bookingLink'>Booking Link</a>
            </div>
        </form>
        <script type="text/javascript">
            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
            document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
        </script>
        <script type="text/javascript">
            var pageTracker = _gat._getTracker("UA-2XXXXX-1");
            pageTracker._setDomainName("none");
            pageTracker._setAllowLinker(true);
            pageTracker._trackPageview();
        </script>
    </body>
</html>