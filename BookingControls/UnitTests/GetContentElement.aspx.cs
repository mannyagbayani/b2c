﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class UnitTests_GetContentElement : System.Web.UI.Page
{

    public string DebugStr;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        ContentElement elm = ContentManager.GetLinkElementForBrand(THLBrands.Maui, VehicleType.Campervan, CountryCode.NZ, "ExtraHireItems", DateTime.Now, THLBrands.AIRNZ);
        DebugStr = elm.ToString();
    }
}
