﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;
using System.Text;

public partial class UnitTests_PromoTest : System.Web.UI.Page
{

    public string Brand { get; set; }
    public string PromoHeader, PromoText;
    protected void Page_Load(object sender, EventArgs e)
    {
        THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
        if (appDomain == null)//dubug line
            appDomain = new THLDomain("secure.maui.co.nz", THLBrands.Maui, CountryCode.NZ, "B2CNZ", false, "uahere");//dubug line
        Brand = appDomain.BrandChar.ToString(); //(Request.Params["brand"] != null ? Request.Params["brand"] : "m");//TODO: this should be swapped by baseURL. 
        if (!string.IsNullOrEmpty(Request.Params["brand"]))
            Brand = Request.Params["brand"];
        if (!string.IsNullOrEmpty(Request.Params["promo"]))
        { 
            string promoStr = Request.Params["promo"];
            string[] promoParams = promoStr.Split('|');
            PromoHeader = promoParams[0];
            StringBuilder sb = new StringBuilder();
            for(int i=1; i< promoParams.Length; i++)
            {
                sb.Append("<small>"+ promoParams[i] +"</small>");
            }
            PromoText = sb.ToString();
        }
    }
}