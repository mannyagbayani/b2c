﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AltAvailability.aspx.cs"
    Inherits="UnitTests_AltAvailability" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Alternative Availability</title>
    <link href="<%= AssetsURL %>/css/Base.css?v=1.5" type="text/css" rel="stylesheet" />
    <link href="<%= AssetsURL %>/css/<%= BrandChar %>/selection.css?v=1.7" type="text/css" rel="stylesheet" />   
    <link rel="stylesheet" type="text/css" media="all" href="http://images.thl.com/css/b/horizontal.css" />
    <script src="../js/jquery-1.3.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery.dimensions.min.js" type="text/javascript"></script>
    <script src="../js/jquery.tooltip.min.js" type="text/javascript"></script>
    <script src="../js/date.js" type="text/javascript"></script>
    <script src="../js/hCtrl.js?v=1.7" type="text/javascript"></script>
    <script src="../js/selection.js?v=1.7" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <link href="<%= AssetsURL %>/css/<%= BrandChar %>/jcal.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript">
        var $j = jQuery.noConflict();
        jQuery(document).ready(function () {//TODO: refactor int initAltAvail()
            //
            //unavailaible detail layer
            $j(".details").each(function () {
                var $container = $j(this);
                var $html = $j('<div class="DiscountedDaysPopUp bubbleBody"><div class="SmallContainer"><span class="UnavailableInfo"></span></div></div>');
                var $textSpan = $html.find('.UnavailableInfo');
                $j(this).after($html);
                $j(this).bind('mouseover', function () { initPopUp(this); });
                var template = $j("#UnavailableInfoTemplate").html();
                var divInfo = $container.parents(".OptionRow").find('.AltType span').text().toLowerCase();
                $textSpan.html(template.replace("{ROW}", divInfo));
            });

            //TODO: this div will need to be generated from backend
            $j(".UnavailableInfo").each(function () {
                var $div = $j(this);
                var template = $j("#UnavailableInfoTemplate").html();
                var divInfo = $div.parents(".OptionRow").find('.AltType span').text().toLowerCase();
                $div.html(template.replace("{ROW}", divInfo));
            });
            $j(".ShowPriceList").each(function () {
                //var $html = $j('.bubbleBody');//get html from relevant row
                //$j(this).after($html);
                $j(this).bind('mouseover', function () { initPopUp(this); });
            });

            $j("span.hireDates").each(function() {
                $j(this).bind('mouseover', function() { initPopUpV2(this,"popUpTxt"); });
            });

            $j("#currencySelector").change(function() {
                var elm = $j(this);
                code = elm.val();
                factor = elm.attr('rel');
                $j("div.AltPrice ul.Prices li.Fp").addClass('Hide'); //reset
                $j("div.AltPrice ul.Prices li." + code).removeClass('Hide');
            });

            $j("a.call").click(function() {
                //event.stopPropagation();
                NewWindow($j(this).attr('href'), 'vCode', '820', '500', 'yes', 'center');
                return false;
            });     
            
            
        });
    </script>    
    <style>
        #tooltip
        {
            background: none;
            border: 0px;
            padding: 0px;
            -moz-box-shadow: 5px 5px 5px #666;
            -webkit-box-shadow: 5px 5px 5px #666;
            box-shadow: 5px 5px 5px #666;
        }
        
        #alternate a {
            outline: 0;
        }       
        
    </style>
    <%= AnalyticsCode %>
</head>
<body id="alternate">
    <form name="form1" method="post" action="selection.aspx" id="form1">
    <%= DNAIMGTag %>
    <div id="vehicleAAContainer">
        <div id="catalogAAHeader">
            <span class="Image">
                <img src="<%= AssetsURL %>/images/b/logo.jpg" alt="logo" />
            </span>
            <span class="ContactUs">
                <img src="../images/b/tiles/header-24-7-image.jpg" alt="contact us 24 7" />
            </span>
        </div>
        <div id="b2c-steps">
            <span id="step-1" class="steps">
                <a href="<%= SelectionURL %>">Select</a>
            </span>
        </div>
        <div id="vechileCatalogAAContainer">
            <h2>Alternative Availability</h2>
            <div id="resultsHeader">
                <span class="CurrencyPanel"><small>Alternative Currency:</small>
                    <%= CurrencySelector %>
                </span>
            </div>          
            <div id="alternativeCatalogContainer">
                <div id="selectedVehiclePanel">
                    <h3><%= AItem.VehicleName %></h3>                     
                    <img id="selectedVImage" src="<%= VehicleImagePath %>-Cutaway-144.jpg" title="Package Code here" alt="Vehicle Image" /> 
                    <img id="selectedVIncs"  src="<%= VehicleImagePath %>-InclusionIcons.gif" title="Inclusions" alt="Vehicle Inclusions"/> 
                    <span class="reel">
                        <img id="selectedVReel" src="<%= VehicleImagePath %>-OverviewThumbs.png" alt="Vehicle Reel" />       
                    </span>
                </div>
                <!-- Generated Catalog View -->
                <%= OriginalSearchRow %>
                <div id="alternativeMessagePanel">
                    <span>Next best availability for the Vehicle Name</span>                    
                </div>
                <%= CurrentRow %>
                <div id="alternateContactPanel">
                    <span>Can't find what you are looking for?</span>                    
                </div>
            </div>           
        </div>
        <div id="footerContainer">
            <div id="footerWrapper">
                <span class="resText">contact us 24 7</span>
                <a href="<%= ParentSite %>/callus" class="freephone call" title="free phone">Free phone</a>
                <span class="searchall">searchall</span>
                <a href="<%= SelectionURL %>" class="returntosearch"  title="return to search">Return to search page</a>
                <a href="#" class="otherbrands" title="try THL's other brands">Try THL's other brands</a>
            </div>
        </div>
        <div id="disclaimer" title="disclaimer">
            <span>Please note: Rental prices quoted above are only valid at the time of booking.</span>
            <span>Alternative currency is based on approximate current exchange rates. All payments
                will be processed in NZD.</span>
        </div>
        <div id="footerLinksBottom">
            <ul id="footerLinksLogos">
                <li id="logos">
                    <a id="THLLogo" title="Tourism Holding Ltd" href="http://www.thlonline.com">THL</a>
                    <a id="MauiLogo" title="maui motorhomes &amp; car rental" href="http://www.maui-rentals.com">Maui</a>
                    <a id="BritzLogo" title="Britz" href="http://www.britz.com">Britz</a>                                
                    <a id="backpackerLogo" title="backpacker campervan &amp; car hire" href="http://www.backpackercampervans.com">backpackercampervans</a>
                    <a id="exploremoreLogo" title="Explore More" href="http://www.exploremore.co.nz">exploremore</a>
                    <a id="cimuroLogo" title="Ci Munro Builds Caravans, Motorhomes &amp; relocatable holiday homes" href="http://www.cimunro.co.nz">CiMunro</a>
                    <a id="roadbearlogo" title="Road Bear RV USA" href="http://www.roadbearrv.com">Road Bear RV USA</a>
                </li>
                <li id="copyright">
                    You are viewing <%= ParentSite.Replace("http://", "") %><br />                   
                    &copy; <span id="yearToDate">2011</span>
                    <script type="text/javascript">
                        var currentTime = new Date();
                        document.getElementById('yearToDate').innerHTML = currentTime.getFullYear();
                    </script>
                    Tourism Holdings Ltd
                    <strong><i>(thl)</i></strong>                  
                </li>
            </ul>
        </div>        
        <div style="display:none;">       
            <span id="UnavailableInfoTemplate">Sorry, we are unable to find any results for {ROW}</span>
        </div>     
        <!-- Debug Code Starts -->           
        <div>
            <%= debugSTR %>
        </div>
        <!-- Debug Code Ends -->
    </div>
    </form>
</body>
</html>
