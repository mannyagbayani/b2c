﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Preload.aspx.cs" Inherits="UnitTests_Preload" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Alternative Availability</title>
    <link href="<%= AssetsURL %>/css/Base.css?v=1.5" type="text/css" rel="stylesheet" />
    <link href="<%= AssetsURL %>/css/<%= BrandChar %>/selection.css?v=1.7" type="text/css" rel="stylesheet" />   
    <link rel="stylesheet" type="text/css" media="all" href="http://images.thl.com/css/b/horizontal.css" />
    <script src="../js/jquery-1.3.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery.dimensions.min.js" type="text/javascript"></script>
    <script src="../js/jquery.tooltip.min.js" type="text/javascript"></script>
    <script src="../js/date.js" type="text/javascript"></script>
    <script src="../js/hCtrl.js?v=1.7" type="text/javascript"></script>
    <script src="../js/selection.js?v=1.7" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>    
    
    <script src="http://jquery-ui.googlecode.com/svn/branches/labs/carousel/ui.carousel.js" type="text/javascript"></script>
    <style type="text/css">
	
		#carousel { position: relative; height: 160px; margin-left: 100px; padding: 0; }
		#carousel li { float: left; cursor: pointer; cursor: hand; list-style: none; margin: 0; padding: 0; width: 220px; height: 195px; }
		#carousel li img { height: 100%; width: 100%; }	
	
	</style>        
    <link href="<%= AssetsURL %>/css/<%= BrandChar %>/jcal.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript">

        var loadAttempts = 10; //define number of trys
        var redirectURL = "AltAvailability.aspx";
        var providedParams = "";
        var progressStarted = false;
        var pingFreq = 1000;
        var pingServiceUrl = "../dispatcher/LoadProgress.asmx/LoadHeavyMethod?loadType=GetAlternateOptions";

        function getStatus() {
            var uniqueId = Math.random();
            var url = pingServiceUrl + '?tid=' + uniqueId + "&" + providedParams + (progressStarted ? "&state=ping" : "");
            $.get(url, function(data) {
                progressStarted = true;
                if (data.indexOf("loaded...") == -1 && data.indexOf("NoMatch") == -1 && --loadAttempts > 0) {
                    $('#status').html(data);
                    $('#statusFill').width(data);
                    window.setTimeout("getStatus()", pingFreq);
                }
                else {
                    window.location = redirectURL; //alert(redirectURL);            
                    //alert('server data is ready');
                };
            });
        }
       
        
        //var $j = jQuery.noConflict();
        jQuery(document).ready(function () {//TODO: refactor int initAltAvail()
            //
            //unavailaible detail layer
            window.setTimeout(function() {
                $("#carousel").carousel({
                    animateByMouse: false,
                    pausable: false
                });
            }, $.browser.safari ? 100 : 0);

            getStatus();         
            
        });
    </script>    
    <style>
        #tooltip
        {
            background: none;
            border: 0px;
            padding: 0px;
            -moz-box-shadow: 5px 5px 5px #666;
            -webkit-box-shadow: 5px 5px 5px #666;
            box-shadow: 5px 5px 5px #666;
        }
    </style>
</head>
<body id="alternate">
    <form name="form1" method="post" action="selection.aspx" id="form1">
    <div id="vehicleAAContainer">
        <div id="catalogAAHeader">
            <span class="Image">
                <img src="<%= AssetsURL %>/images/b/logo.jpg" alt="logo" />
            </span>
            <span class="ContactUs">
                <img src="../images/b/tiles/header-24-7-image.jpg" alt="contact us 24 7" />
            </span>
        </div>
        <div id="b2c-steps">
            <span id="step-1" class="steps"></span>
        </div>
        <div id="vechileCatalogAAContainer">
            <h2>Alternative Availability</h2>                      
            <div id="alternativeCatalogContainer">
                <div id="selectedVehiclePanel">
                    <h3><%= AItem.VehicleName %></h3>                     
                    <img id="selectedVImage" src="<%= VehicleImagePath %>-Clearcut-144.jpg" title="Package Code here" alt="Vehicle Image" /> 
                    <img id="selectedVIncs"  src="<%= VehicleImagePath %>-InclusionIcons.gif" title="Inclusions" alt="Vehicle Inclusions"/> 
                    <span class="reel">
                        <img id="selectedVReel" src="<%= VehicleImagePath %>-OverviewThumbs.png" alt="Vehicle Reel" />       
                    </span>
                </div>
                <div id="preLoader" style="height: 400px;width:700px;float:left;clear:both;">
                    <ul id="carousel" style="float:left;clear:both;" >
	                    <li><img src="<%= PreloaderImagePath %>/1.png" alt="" /></li>		
	                    <li><img src="<%= PreloaderImagePath %>/2.png" alt="" /></li>		
	                    <li><img src="<%= PreloaderImagePath %>/3.png" alt="" /></li>		
	                    <li><img src="<%= PreloaderImagePath %>/4.png" alt="" /></li>		
	                    <li><img src="<%= PreloaderImagePath %>/5.png" alt="" /></li>		
	                    <li><img src="<%= PreloaderImagePath %>/6.png" alt="" /></li>		
	                    <li><img src="<%= PreloaderImagePath %>/7.png" alt="" /></li>		
	                    <li><img src="<%= PreloaderImagePath %>/8.png" alt="" /></li>
                    </ul>
                </div>
            </div>           
        </div>
        <div id="footerContainer">            
        </div>
        <div id="disclaimer" title="disclaimer">
            
        </div>
        <div id="footerLinksBottom">
            <ul id="footerLinksLogos">
                <li id="logos">
                    <a id="THLLogo" title="Tourism Holding Ltd" href="http://www.thlonline.com">THL</a>
                    <a id="MauiLogo" title="maui motorhomes &amp; car rental" href="http://www.maui-rentals.com">Maui</a>
                    <a id="BritzLogo" title="Britz" href="http://www.britz.com">Britz</a>                                
                    <a id="backpackerLogo" title="backpacker campervan &amp; car hire" href="http://www.backpackercampervans.com">backpackercampervans</a>
                    <a id="exploremoreLogo" title="Explore More" href="http://www.exploremore.co.nz">exploremore</a>
                    <a id="cimuroLogo" title="Ci Munro Builds Caravans, Motorhomes &amp; relocatable holiday homes" href="http://www.cimunro.co.nz">CiMunro</a>
                    <a id="roadbearlogo" title="Road Bear RV USA" href="http://www.roadbearrv.com">Road Bear RV USA</a>
                </li>
                <li id="copyright">
                    You are viewing www.britz.co.nz.<br />                   
                    &copy; <span id="yearToDate">2011</span>
                    <script type="text/javascript">
                        var currentTime = new Date();
                        document.getElementById('yearToDate').innerHTML = currentTime.getFullYear();
                    </script>
                    Tourism Holdings Ltd
                    <strong><i>(thl)</i></strong>                  
                </li>
            </ul>
        </div>        
        <div style="display:none;">       
            <span id="UnavailableInfoTemplate">Sorry, we are unable to find any results for {ROW}</span>
        </div>     
        <!-- Debug Code Starts -->           
        <div>
            <%= debugSTR %>
        </div>
        <!-- Debug Code Ends -->
    </div>
    </form>
</body>
</html>