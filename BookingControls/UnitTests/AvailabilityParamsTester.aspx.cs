﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace THL.Booking
{

    public partial class UnitTests_AvailabilityParamsTester : System.Web.UI.Page
    {
     
        public string results;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            AvailabilityRequest ar = new AvailabilityRequest();
            ar.LoadFromQueryStringParams(Request.QueryString);
            results = ar.ToString();
        }
    }
}