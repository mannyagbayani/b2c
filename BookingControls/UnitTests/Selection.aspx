﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Selection.aspx.cs" Inherits="UnitTests_Selection" %>

<!doctype html>
<html lang="en">
<head>
    <title>Selection sorting test</title>
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
    <link href="/unittests/css/b/Base.css?v=2.3" type="text/css" rel="stylesheet" />
       
    
    <link href="/unittests/css/b/selection.css?v=2.3" type="text/css"
        rel="stylesheet" />
    <link rel="stylesheet" type="text/css" media="all" href="https://secure.britz.com.au/css/b/horizontal.css?v=2.3" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript"></script>
    <script src="https://secure.britz.com.au/js/jquery.dimensions.min.js" type="text/javascript"></script>
    <script src="https://secure.britz.com.au/js/jquery.tooltip.min.js" type="text/javascript"></script>
    <script src="https://secure.britz.com.au/js/date.js" type="text/javascript"></script>
    <script src="https://secure.britz.com.au/js/hCtrl.js?v=2.3" type="text/javascript"></script>
    <script src="/unittests/js/selection.js?v=2.3" type="text/javascript"></script>
    <script src="https://secure.britz.com.au/js/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <link href="https://secure.britz.com.au/css/b/jcal.css?v=2.3" type="text/css" rel="stylesheet" />
    <script src="/js/ui.core.js" type="text/javascript"></script>
    <script src="/js/ui.carousel.js" type="text/javascript"></script>
    <script type="text/javascript">
        var ContentbaseUrl = 'https://secure.britz.com.au/';
        var assetsBase = 'https://secure.britz.com.au';
        var affCode = '';
        var crossSellUrl = 'https://secure.motorhomesandcars.com';
        var brandId = 'b';
        var ctrlParams = { pb: 'adl', db: 'bne', cr: 'AU', cc: 'AU', rf: '', vt: 'ac', vc: '', pd: '21-Nov-2012', pt: '10:00', dd: '12-Dec-2012', dt: '15:00', na: '1', nc: '0', pb: 'ADL', db: 'BNE', com: '', brand: 'b', debug: '' };
        var requestParams = { pb: 'adl', db: 'bne', cr: 'AU', cc: 'AU', rf: '', vt: 'av', vc: '', pd: '21-Nov-2012', pt: '10:00', dd: '12-Dec-2012', dt: '15:00', na: '1', nc: '0', pb: 'ADL', db: 'BNE', com: '', brand: 'b', debug: '' };
        var currencyStr = 'AUD';
        var defaultHours = { avOpen: '09:30', avClose: '17:30', acOpen: '10:30', acClose: '11:30' };
        var $j = jQuery.noConflict();
        jQuery(document).ready(function () {
            initSelectionPage();


            t = window.setTimeout(function () {
                if ($j("#staticLoader").is(':visible')) {
                    $j("#carousel").carousel({
                        animateByMouse: false,
                        pausable: true,
                        radius: 250,
                        animate: 0.004,
                        pauseSpeed: 0
                    });
                }
                else {
                    window.clearTimeout(t);
                }
            }, $j.browser.safari ? 100 : 0);

            //sorting
            isotope();



        });

        function isotope() {
            $j('#vehicleCatalog').isotope({
                animationEngine: 'jquery',
                getSortData: {
                    size: function ($elem) {
                        return parseFloat($elem.find('.size').text());
                    },
                    price: function ($elem) {
                        return $elem.find('.Prices li span').text();
                    }
                }
            });
        }

        function sort(selm) {
            //_gaq.push(['_trackEvent', 'Campervan', 'Availability', '05/12/21-2012/ADL/BNE', 5]);
            _gaq.push(['_trackEvent', 'Campervan', 'Sort', selm])
            var selm = $j(selm);
            stype = selm.attr('rel');
            var isAsc = selm.hasClass('asc');
            var sortName = stype;
            
            $j('#vehicleCatalog').isotope(
            {
            animationEngine: 'jquery',
            getSortData: {
                    size: function ($elem) {
                        return parseFloat($elem.find('.size').text());
                    }                    
            },
                sortBy: stype,
                sortAscending : isAsc
            });
            
            selm.toggleClass('asc');
            return false;
        }


    </script>
    <script type='text/javascript'>
        //async
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UB-2807195-1']);
        _gaq.push(['_setDomainName', '.britz.com.au']);
        _gaq.push(['_setAllowHash', false]);
        _gaq.push(['_trackPageview']);
        _gaq.push(['_trackEvent', 'Campervan', 'Availability', '05/12/21-2012/ADL/BNE', 5]);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <script type="text/javascript" src="http://isotope.metafizzy.co/jquery.isotope.min.js"></script>
</head>
<body class="selection">
    <form name="form1" method="post" action="Selection.aspx?cc=AU&amp;brand=b&amp;ac=&amp;sc=rv&amp;vtype=rv&amp;pc=&amp;na=1&amp;nc=0&amp;cr=AU&amp;pb=ADL&amp;pd=21&amp;pm=11&amp;py=2012&amp;pt=10%3a00&amp;db=BNE&amp;dd=12&amp;dm=12&amp;dy=2012&amp;dt=15%3a00&amp;vh=&amp;__utma=1.707163786.1308716401.1352404881.1352425868.178&amp;__utmz=1.1350345174.153.14.utmcsr%3duat.alphacampervans.co.nz%7cutmccn%3d(referral)%7cutmcmd%3dreferral%7cutmcct%3d%2fpages%2fwidgetcreator.aspx&amp;__utmc=1&amp;__utmb=1.10.9.1352427769280&amp;SID=354o3q45q31wde55yqz412yg&amp;pv=1.0"
    id="form1">
    <div>
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTUxNDkzNjU0NmRkxwj4E7EOjTvzGyoGa/Euw5UU+FQ=" />
    </div>
    <div id="thlCrossSellBanner">
        <!-- Start CrossSell Header, TODO: swap to Web Control -->
        <div id="crossSell">
            <ul class="crossSellNav">
                <li><span class="logo" id="Span1">Australia</span></li>
                <li class="maui"><a href="http://www.maui.com.au">Maui</a></li>
                <li class="britz"><a href="http://www.britz.com.au" class='current'>Britz</a></li>
                <li class="BP"><a href="http://www.backpackercampervans.com.au">Backpacker</a></li>
                <li class="EX"><a href="http://www.exploremore.com.au">Explore More</a></li>
            </ul>
            <div id="crossSellCountry">
                <ul class="crossSellNav">
                    <li><span class="country">New Zealand </span>
                        <ul class="crossSellNavDD">
                            <li class="maui"><a href="http://www.maui.co.nz">Maui</a></li>
                            <li class="britz"><a href="http://www.britz.co.nz">Britz</a></li>
                            <li class="BP"><a href="http://www.backpackercampervans.co.nz">Backpacker</a></li>
                            <li class="EX"><a href="http://www.exploremore.co.nz">Explore More</a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="crossSellNav">
                    <li><span class="country">USA</span>
                        <ul class="crossSellNavDD">
                            <li class="RBC"><a href="http://www.roadbearrv.com">Road Bear RV</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- End CrossSell Header -->
    </div>
    <div id="vehicleSelectionContainer" class="AU">
        <div id="catalogHeader">
            <span class="Image"><a href="http://www.britz.com.au">
                <img src="https://secure.britz.com.au/images/b/logo.jpg" alt="logo" />
            </a></span><a class="priceMatch" href="javascript:openPriceMatchWindow();">Price match
                guarantee</a> <span class="ContactInfo">Call us now - 24 hours a day, 7 days a week:<br />
                    Call Within AU Toll Free: 1 800 331 454 - Worldwide Toll Free: +800 200 80 801 <a
                        href="http://www.britz.com.au/contactus" target="_blank">contact us</a>
            </span>
        </div>
        <div id="bookingControlContainer">
            <div id="sslSeal">
                <script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=secure.britz.com.au&amp;size=L&amp;lang=en"></script>
            </div>
            <div id="HorizontalControlContainer">
                <script type="text/javascript">
                    var bookingControlParams = { pb: 'adl', db: 'bne', cr: 'AU', cc: 'AU', rf: '', vt: 'ac', vc: '', pd: '21-Nov-2012', pt: '10:00', dd: '12-Dec-2012', dt: '15:00', na: '1', nc: '0', pb: 'ADL', db: 'BNE', com: '', brand: 'b', debug: '' };
                    var ContentbaseUrl = 'https://';
                    var MAX_PASSENGERS = 6;
                    var psid = '354o3q45q31wde55yqz412yg';
                    var noa = 0, noc = 0;
                    var $j = jQuery.noConflict();
                    jQuery(document).ready(function () {
                        initControl();
                        var options = { height: 24, imgUrl: "/images/tiles/slow-loader.png", btnClass: "searchBtn", spinnerClass: "spinner" }
                        bindSpinner(options);
                    });         
                </script>
                <div id="bookingControl" class="  ">
                    <!-- Horizontal Booking Control -->
                    <div id="leftCol" class="CtrlCol">
                        <span class="Radios ">
                            <label for="av">
                                Motorhome</label>
                            <input type="radio" id="avRadio" value="av" name="vt" />
                            <label for="av">
                                Car</label>
                            <input type="radio" id="acRadio" value="ac" name="vt" />
                            <input type="hidden" id="radiosSwapped" name="radiosSwapped" />
                        </span><span class="required">Pick up</span> <span class="required">Drop off</span>
                        <span class="required CofR">Drivers Licence</span>
                    </div>
                    <div id="centreCol" class="CtrlCol">
                        <span class="Model"><small>Model:</small>
                            <select name='ctrlSearchBox_dropVehicleType' id='ctrlSearchBox_dropVehicleType' class='SearchBox_FullListBox'>
                                <option value=''>Search all</option>
                                <option value='auavb.2BB'>HiTop</option>
                                <option value='auavb.4BBXS'>Voyager</option>
                                <option value='auavb.2BTSB'>Elite</option>
                                <option value='auavb.2BTSBV'>Venturer</option>
                                <option value='auavb.4BB'>Explorer</option>
                                <option value='auavb.4BBJ'>Maverick</option>
                                <option value='auavb.5BB'>Escape</option>
                                <option value='auavb.6BB'>Frontier</option>
                                <option value='auavb.6BBR'>Renegade</option>
                                <option value='auavb.2B4WDBC'>Challenger 4WD</option>
                                <option value='auavb.2B4WDB'>Bushcamper 4WD</option>
                                <option value='auavb.PFMRS'>Safari 4WD</option>
                            </select>
                        </span><span class="Splash">
                            <select id='countryCode' name='countryCode'>
                                <option value=''>Select Country of Travel</option>
                                <option selected='selected' value='AU'>Australia</option>
                                <option value='NZ'>New Zealand</option>
                                <option value='US'>United States,United States,United States,United States,United States,United
                                    States,United States,United States,United States,United States,United States,United
                                    States,United States,United States,United States,United States,United States,United
                                    States,United States,United States,United States,United States,United States,United
                                    States,United States,United States,United States,United States,United States,United
                                    States,United States,United States,United States,United States,United States,United
                                    States,United States,United States,United States,United States,United States,United
                                    States,United States,United States,United States,United States,United States,United
                                    States,United States</option>
                                <option value='CA'>Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada,Canada</option>
                            </select>
                        </span><span class="Company">
                            <input type='hidden' id='ctrlSearchBox_dropVendor' value='b' />
                        </span><span class="TimeSpan">
                            <input size="15" id="ctrlSearchBox_calPickUp_DateText" name="ctrlSearchBox_calPickUp_DateText"
                                value="21-Nov-2012" type="text" autocomplete="off" />
                            <select class="SearchBox_TimeListBox" id="ctrlSearchBox_dropPickUpTime" name="ctrlSearchBox_dropPickUpTime">
                                <option value="08:00">08:00am</option>
                                <option value="08:30">08:30am</option>
                                <option value="09:00">09:00am</option>
                                <option value="09:30">09:30am</option>
                                <option value="10:00" selected="selected">10:00am</option>
                                <option value="10:30">10:30am</option>
                                <option value="11:00">11:00am</option>
                                <option value="11:30">11:30am</option>
                                <option value="12:00">12:00pm</option>
                                <option value="12:30">12:30pm</option>
                                <option value="13:00">13:00pm</option>
                                <option value="13:30">13:30pm</option>
                                <option value="14:00">14:00pm</option>
                                <option value="14:30">14:30pm</option>
                                <option value="15:00">15:00pm</option>
                                <option value="15:30">15:30pm</option>
                                <option value="16:00">16:00pm</option>
                                <option value="16:30">16:30pm</option>
                                <option value="17:00">17:00pm</option>
                            </select>
                        </span><span class="TimeSpan">
                            <input size="15" id="ctrlSearchBox_calDropOff_DateText" name="ctrlSearchBox_calDropOff_DateText"
                                value="12-Dec-2012" type="text" autocomplete="off" />
                            <select class="SearchBox_TimeListBox" id="ctrlSearchBox_dropDropOffTime" name="ctrlSearchBox_dropDropOffTime">
                                <option value="08:00">08:00am</option>
                                <option value="08:30">08:30am</option>
                                <option value="09:00">09:00am</option>
                                <option value="09:30">09:30am</option>
                                <option value="10:00">10:00am</option>
                                <option value="10:30">10:30am</option>
                                <option value="11:00">11:00am</option>
                                <option value="11:30">11:30am</option>
                                <option value="12:00">12:00pm</option>
                                <option value="12:30">12:30pm</option>
                                <option value="13:00">13:00pm</option>
                                <option value="13:30">13:30pm</option>
                                <option value="14:00">14:00pm</option>
                                <option value="14:30">14:30pm</option>
                                <option value="15:00" selected="selected">15:00pm</option>
                                <option value="15:30">15:30pm</option>
                                <option value="16:00">16:00pm</option>
                                <option value="16:30">16:30pm</option>
                                <option value="17:00">17:00pm</option>
                            </select>
                        </span><span class="TimeSpan CofR">
                            <select id="ctrlSearchBox_dropCountryOfResidence">
                                <option value="0" selected="selected">Please select</option>
                                <option value="AU">Australia</option>
                                <option value="CA">Canada</option>
                                <option value="DK">Denmark</option>
                                <option value="FR">France</option>
                                <option value="DE">Germany</option>
                                <option value="NL">Netherlands</option>
                                <option value="NZ">New Zealand</option>
                                <option value="SE">Sweden</option>
                                <option value="CH">Switzerland</option>
                                <option value="UK">United Kingdom</option>
                                <option value="US">United States Of America</option>
                                <option value="-" disabled="disabled">--------------------</option>
                                <option value="AF">Afghanistan</option>
                                <option value="AL">Albania</option>
                                <option value="DZ">Algeria</option>
                                <option value="AS">American Samoa</option>
                                <option value="AD">Andora</option>
                                <option value="AO">Angola</option>
                                <option value="AI">Anguilla</option>
                                <option value="AQ">Antarctica</option>
                                <option value="AG">Antigua & Barbados</option>
                                <option value="AR">Argentina</option>
                                <option value="AM">Armenia</option>
                                <option value="AW">Aruba</option>
                                <option value="AT">Austria</option>
                                <option value="AZ">Azerbaijan</option>
                                <option value="BS">Bahamas</option>
                                <option value="BH">Bahrain</option>
                                <option value="BD">Bangladesh</option>
                                <option value="BB">Barbados</option>
                                <option value="BY">Belarus</option>
                                <option value="BE">Belgium</option>
                                <option value="BZ">Belize</option>
                                <option value="BJ">Benin</option>
                                <option value="BM">Bermuda</option>
                                <option value="BO">Bolivia</option>
                                <option value="BA">Bosnia and Herzegovina</option>
                                <option value="BW">Botswana</option>
                                <option value="BV">Bouvet Island</option>
                                <option value="BR">Brazil</option>
                                <option value="IO">British Indian Ocean Territory</option>
                                <option value="BN">Brunei</option>
                                <option value="BG">Bulgaria</option>
                                <option value="BF">Burkina Faso</option>
                                <option value="BI">Burundi</option>
                                <option value="BT">Butan</option>
                                <option value="KH">Cambodia</option>
                                <option value="CM">Cameroon</option>
                                <option value="CV">Cape Verde</option>
                                <option value="KY">Cayman Islands</option>
                                <option value="CF">Cental African Republic</option>
                                <option value="TD">Chad</option>
                                <option value="CL">Chile</option>
                                <option value="CN">China</option>
                                <option value="CX">Christmas Island</option>
                                <option value="CC">Cocos (Keeling) Island</option>
                                <option value="CO">Colombia</option>
                                <option value="KM">Comoros</option>
                                <option value="CG">Congo</option>
                                <option value="CK">Cook Island</option>
                                <option value="CR">Costa Rica</option>
                                <option value="CI">Cote D'ivoire</option>
                                <option value="HR">Croatia</option>
                                <option value="CU">Cuba</option>
                                <option value="CY">Cyprus</option>
                                <option value="CZ">Czech Republic</option>
                                <option value="DJ">DjiboutiI</option>
                                <option value="DM">Dominica</option>
                                <option value="DO">Dominican Republic</option>
                                <option value="TP">East Timor</option>
                                <option value="EC">Ecuador</option>
                                <option value="EG">Egypt</option>
                                <option value="SV">El Salvador</option>
                                <option value="GQ">Equatorial Guinea</option>
                                <option value="ER">Eritrea</option>
                                <option value="EE">Estonia</option>
                                <option value="ET">Ethiopia</option>
                                <option value="FK">Falkland Islands</option>
                                <option value="FO">Faroe Islands</option>
                                <option value="FJ">Fiji</option>
                                <option value="FI">Finland</option>
                                <option value="FX">France, Metropolitan</option>
                                <option value="GF">French Guiana</option>
                                <option value="PF">French Polynesia</option>
                                <option value="TF">French Souther Territories</option>
                                <option value="GA">Gabon</option>
                                <option value="GM">Gambia</option>
                                <option value="GE">Georgia</option>
                                <option value="GH">Ghana</option>
                                <option value="GI">Gibraltar</option>
                                <option value="GR">Greece</option>
                                <option value="GL">Greenland</option>
                                <option value="GD">Grenada</option>
                                <option value="GP">Guadeloupe</option>
                                <option value="GU">Guam</option>
                                <option value="GT">Guatemala</option>
                                <option value="GN">Guinea</option>
                                <option value="GW">Guinea-Bissau</option>
                                <option value="GY">Guyana</option>
                                <option value="HT">Haiti</option>
                                <option value="HM">Heard and McDonald Islands</option>
                                <option value="HN">Honduras</option>
                                <option value="HK">Hong Kong</option>
                                <option value="HU">Hungary</option>
                                <option value="IS">Iceland</option>
                                <option value="IN">India</option>
                                <option value="ID">Indonesia</option>
                                <option value="IR">Iran</option>
                                <option value="IQ">Iraq</option>
                                <option value="IE">Ireland</option>
                                <option value="IL">Israel</option>
                                <option value="IT">Italy</option>
                                <option value="JM">Jamaica</option>
                                <option value="JP">Japan</option>
                                <option value="JO">Jordan</option>
                                <option value="KZ">Kazakhstan</option>
                                <option value="KE">Kenya</option>
                                <option value="KI">Kiribati</option>
                                <option value="KW">Kuwait</option>
                                <option value="KG">Kyrgyzstan</option>
                                <option value="LA">Lao Peoples Democratic Republic</option>
                                <option value="LV">Latvia</option>
                                <option value="LB">Lebanon</option>
                                <option value="LS">Lesotho</option>
                                <option value="LR">Liberia</option>
                                <option value="LY">Libya</option>
                                <option value="LI">Liechtenstein</option>
                                <option value="LT">Lithuania</option>
                                <option value="LU">Luxembourg</option>
                                <option value="MO">Macau</option>
                                <option value="MK">Macedonia</option>
                                <option value="MG">Madagascar</option>
                                <option value="MW">Malawi</option>
                                <option value="MY">Malaysia</option>
                                <option value="MV">Maldives</option>
                                <option value="ML">Mali</option>
                                <option value="MT">Malta</option>
                                <option value="MH">Marshall Islands</option>
                                <option value="MQ">Martinique</option>
                                <option value="MR">Mauritania</option>
                                <option value="MU">Mauritius</option>
                                <option value="YT">Mayotte</option>
                                <option value="MX">Mexico</option>
                                <option value="FM">Micronesia, Federated States of</option>
                                <option value="MD">Moldova</option>
                                <option value="MC">Monaco</option>
                                <option value="MN">Mongolia</option>
                                <option value="MS">Montserrat</option>
                                <option value="MA">Morocco</option>
                                <option value="MZ">Mozambique</option>
                                <option value="MM">Myanamar</option>
                                <option value="NA">Namibia</option>
                                <option value="NR">Nauru</option>
                                <option value="NP">Nepal</option>
                                <option value="AN">Netherlands Antilles</option>
                                <option value="NC">New Caledonia</option>
                                <option value="NI">Nicaragua</option>
                                <option value="NE">Niger</option>
                                <option value="NG">Nigeria</option>
                                <option value="NU">Niue</option>
                                <option value="NF">Norfolk Island</option>
                                <option value="KP">North Korea</option>
                                <option value="MP">Northern Mariana Islands</option>
                                <option value="NO">Norway</option>
                                <option value="OM">Oman</option>
                                <option value="PK">Pakistan</option>
                                <option value="PW">Palau</option>
                                <option value="PA">Panama</option>
                                <option value="PG">Papua New Guinea</option>
                                <option value="PY">Paraguay</option>
                                <option value="PE">Peru</option>
                                <option value="PH">Philippines</option>
                                <option value="PN">Pitcairn Islands</option>
                                <option value="PL">Poland</option>
                                <option value="PT">Portugal</option>
                                <option value="PR">Puerto Rico</option>
                                <option value="QA">Qatar</option>
                                <option value="RE">Reunion</option>
                                <option value="RO">Romania</option>
                                <option value="RU">Russian Federation</option>
                                <option value="RW">Rwanda</option>
                                <option value="SH">Saint Helena</option>
                                <option value="KN">Saint Kitts and Nevis</option>
                                <option value="LC">Saint Lucia</option>
                                <option value="PM">Saint Pierre and Miquelon</option>
                                <option value="VC">Saint Vincent and The Grenadines</option>
                                <option value="WS">Samoa</option>
                                <option value="SM">San Marino</option>
                                <option value="ST">Sao Tome and Principe</option>
                                <option value="SA">Saudi Arabia</option>
                                <option value="SN">Senegal</option>
                                <option value="SC">Seychelles</option>
                                <option value="SG">Singapore</option>
                                <option value="SK">Slovakia</option>
                                <option value="SI">Slovenia</option>
                                <option value="SB">Solomon Islands</option>
                                <option value="SO">Somalia</option>
                                <option value="ZA">South Africa</option>
                                <option value="GS">South Georgia</option>
                                <option value="KR">South Korea</option>
                                <option value="ES">Spain</option>
                                <option value="SL">Sri Lanka</option>
                                <option value="SD">Sudan</option>
                                <option value="SR">Suriname</option>
                                <option value="SJ">Svalbardand Jan Mayen Islands</option>
                                <option value="SY">Syria</option>
                                <option value="TI">Tahiti</option>
                                <option value="TW">Taiwan</option>
                                <option value="TJ">Tajikistan</option>
                                <option value="TZ">Tanzania</option>
                                <option value="TH">Thailand</option>
                                <option value="TG">Togo</option>
                                <option value="TK">Tokelau</option>
                                <option value="TO">Tonga</option>
                                <option value="TT">Trinidad and Tobago</option>
                                <option value="TN">Tunisia</option>
                                <option value="TR">Turkey</option>
                                <option value="TM">Turkmenistan</option>
                                <option value="TC">Turks and Caicos Islands</option>
                                <option value="TV">Tuvalu</option>
                                <option value="UG">Uganda</option>
                                <option value="UA">Ukraine</option>
                                <option value="AE">United Arab Emirates</option>
                                <option value="UY">Uruguay</option>
                                <option value="UM">US Minor Outlying Islands</option>
                                <option value="UZ">Uzbekistan</option>
                                <option value="VU">Vanuatu</option>
                                <option value="VA">Vatican City</option>
                                <option value="VE">Venezuala</option>
                                <option value="VN">Vietnam</option>
                                <option value="VG">Virgin Islands (GB)</option>
                                <option value="VI">Virgin Islands (US)</option>
                                <option value="WF">Wallis and Futuna Islands</option>
                                <option value="EH">Western Sahara</option>
                                <option value="YE">Yemen</option>
                                <option value="YU">Yugoslavia</option>
                                <option value="ZR">Zaire</option>
                                <option value="ZM">Zambia</option>
                                <option value="ZW">Zimbabwe</option>
                            </select>
                        </span>
                    </div>
                    <div id="rightCol" class="CtrlCol">
                        <span class="Passengers"><span class="LeftPass">
                            <select name='ctrlSearchBox_dropChildren' id='ctrlSearchBox_dropChildren' class='SearchBox_AdultsListBox'>
                                <option selected='selected' value='0'>Children</option>
                                <option value='1'>1 Child</option>
                                <option value='2'>2 Children</option>
                                <option value='3'>3 Children</option>
                                <option value='4'>4 Children</option>
                                <option value='5'>5 Children</option>
                            </select>
                        </span><span class="RightPass">
                            <select name='ctrlSearchBox_dropAdults' id='ctrlSearchBox_dropAdults' class='SearchBox_AdultsListBox'>
                                <option selected='selected' value='1'>Adults</option>
                                <option value='1'>1 Adult</option>
                                <option value='2'>2 Adults</option>
                                <option value='3'>3 Adults</option>
                                <option value='4'>4 Adults</option>
                                <option value='5'>5 Adults</option>
                                <option value='6'>6 Adults</option>
                            </select>
                        </span></span><span id="bottomRight"><span id="locationsPanel">
                            <select name="ctrlSearchBox_dropPickUpLocation" id="ctrlSearchBox_dropPickUpLocation"
                                class="SearchBox_FullListBox LocationDD">
                                <option value="ADL">Adelaide</option>
                                <option value="ASP">Alice Springs</option>
                                <option value="BYR">Ballina - Byron Bay</option>
                                <option value="BNE">Brisbane</option>
                                <option value="BME">Broome</option>
                                <option value="CNS">Cairns</option>
                                <option value="DRW">Darwin</option>
                                <option value="HBT">Hobart</option>
                                <option value="MEL">Melbourne</option>
                                <option value="PER">Perth</option>
                                <option value="SYD">Sydney</option>
                            </select>
                            <select name="ctrlSearchBox_dropDropOffLocation" id="ctrlSearchBox_dropDropOffLocation"
                                class="SearchBox_FullListBox LocationDD">
                                <option value="ADL">Adelaide</option>
                                <option value="ASP">Alice Springs</option>
                                <option value="BYR">Ballina - Byron Bay</option>
                                <option value="BNE">Brisbane</option>
                                <option value="BME">Broome</option>
                                <option value="CNS">Cairns</option>
                                <option value="DRW">Darwin</option>
                                <option value="HBT">Hobart</option>
                                <option value="MEL">Melbourne</option>
                                <option value="PER">Perth</option>
                                <option value="SYD">Sydney</option>
                            </select>
                        </span><a href="javascript:submitControl('','get')" class="searchBtn">Search</a>
                        </span><span id="ctrlValidationMsg">= Required</span>
                        <input type="hidden" name="controlTarget" id="controlTarget" value="" />
                        <input type="hidden" name="controlBrand" id="controlBrand" value="b" />
                        <input type="hidden" name="vehicleType" id="vehicleType" value="av" />
                        <input type="hidden" name="countryOfResidence" id="countryOfResidence" value="AU" />
                        <input type="hidden" name="pickUpLoc" id="pickUpLoc" value="ADL" />
                        <input type="hidden" name="dropOffLoc" id="dropOffLoc" value="BNE" />
                        <input type="hidden" name="vehicleCode" id="vehicleCode" value="" />
                        <input type="hidden" name="contryVehicleAvailability" id="contryVehicleAvailability"
                            value="Both" />
                    </div>
                    <div class="botCol">
                        <div class="crossSell">
                            <span class="CSellLbl">Campervan brand</span>
                            <select class="CSellOpt">
                                <option value="brand">Britz</option>
                                <option value="all">All campervan brands</option>
                            </select>
                            <span class="crossSellBrand"></span>
                        </div>
                        <a href="javascript:submitControl('','get')" class="searchBtn">Search</a> <span class="spinner">
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div id="breadCrumbs">
            <span class="Crumbs"><a href="http://www.britz.com.au" class="HomeLnk">Home</a> &gt;
                Select your vehicle </span>
            <ul id="stepsDisplayer">
                <li><span id="PaymentStep">3.Payment</span> </li>
                <li><span id="ConfigureStep">2.Configure</span> </li>
                <li><span id="SelectStep">1.Select</span> </li>
            </ul>
        </div>
        <div id="vehicleCatalogContainer">
            <style>
                h2 span { text-decoration: underline; color: Blue; cursor: pointer; }            
            </style>
            <h2>
                1. Select your vehicle, sort by:  <span rel="size" onclick="sort(this)" class="asc">size</span>|
                                        <span rel="price" onclick="sort(this)" class="asc">price</span>
            </h2>
            <div id="resultsHeader">
                <h3>
                    Search results below</h3>
                <span class="CurrencyPanel"><small>Alternative Currency:</small>
                    <select id='currencySelector'>
                        <option value='NZD' rel='1.0'>New Zealand Dollar (NZD)</option>
                        <option value='USD' rel='0.8154'>United States Dollar (USD)</option>
                        <option value='GBP' rel='0.51'>Pound Sterling (GBP)</option>
                        <option selected='selected' value='AUD' rel='0.782'>Australian Dollar (AUD)</option>
                        <option value='EUR' rel='0.6397'>Euro (EUR)</option>
                        <option value='JPY' rel='64.7629'>Japanese Yen (JPY)</option>
                        <option value='CAD' rel='0.8149'>Canadian Dollar (CAD)</option>
                        <option value='CHF' rel='0.7709'>Swiss Franc (CHF)</option>
                        <option value='XPF' rel='75.4028'>Central French Pacific Franc (XPF)</option>
                        <option value='DKK' rel='4.7782'>Danish Krone (DKK)</option>
                        <option value='FJD' rel='1.4404'>Fijian Dollar (FJD)</option>
                        <option value='HKD' rel='6.3257'>Hong Kong Dollar (HKD)</option>
                        <option value='INR' rel='44.3319'>Indian Rupee (INR)</option>
                        <option value='NOK' rel='4.6773'>Norwegian Krone (NOK)</option>
                        <option value='PKR' rel='78.2199'>Pakistan Rupee (PKR)</option>
                        <option value='PGK' rel='1.6409'>Papua New Guinean Kina (PGK)</option>
                        <option value='PHP' rel='33.4624'>Philippine Peso (PHP)</option>
                        <option value='SGD' rel='0.9985'>Singapore Dollar (SGD)</option>
                        <option value='SBD' rel='5.7578'>Solomon Islands Dollar (SBD)</option>
                        <option value='ZAR' rel='7.0814'>South African Rand (ZAR)</option>
                        <option value='LKR' rel='106.3093'>Sri Lankan Rupee (LKR)</option>
                        <option value='SEK' rel='5.4606'>Swedish Krona (SEK)</option>
                        <option value='THB' rel='24.6635'>Thai Baht (THB)</option>
                        <option value='TOP' rel='1.3562'>Tongan Pa'anga (TOP)</option>
                        <option value='VUV' rel='74.3528'>Vanuatu Vatu (VUV)</option>
                        <option value='WST' rel='1.8236'>Western Samoan Tala (WST)</option>
                    </select>
                </span>
            </div>
            <ul id='vehicleCatalog'>
                <li class='Collapse  element'>
                    <div class='VehicleItem'>
                        <img src='https://secure.britz.com.au/images/b/icons/rowLogo.gif' title='Britz' class='RowLogo PopUp' /><a
                            href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.PFMRS'
                            class='VehicleThumb PopUp'><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.PFMRS/AUavb.PFMRS-ClearCut-72.jpg'
                                border='0' title='PFMRS' /></a><div class='VehicleFeatures s5'><span class="size" style="display:none;">5</span>
                                    <a href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.PFMRS'
                                        class='PopUp'>Britz Safari 4WD</a><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.PFMRS/AUavb.PFMRS-InclusionIcons.gif'
                                            border='0' title='Britz' /></div>
                        <div class='PriceDescription'>
                            <a href='Configure.aspx?avp=ACE341F1A80145B8B033A8E559C4D506' class='Avail'>AVAILABLE
                                NOW</a><span class='promo'><big>Adventure All The Way</big><small>Book before 15 Nov
                                    2012</small><small>Travel 1 Sep 12 to 18 Dec 12</small></span></div>
                        <div class='TotalPrice' title='SPRINGG'>
                            <ul class='Prices'>
                                <li><span>AUD 2,607.60</span></li><li class='Fp Hide NZD'><span>NZD $ 3,334.53</span></li><li
                                    class='Fp Hide USD'><span>USD $ 2,718.97</span></li><li class='Fp Hide GBP'><span>GBP
                                        £ 1,700.61</span></li><li class='Fp Hide AUD'><span>AUD $ 2,607.60</span></li><li
                                            class='Fp Hide EUR'><span>EUR € 2,133.10</span></li><li class='Fp Hide JPY'><span>JPY
                                                ¥ 215,953.63</span></li><li class='Fp Hide CAD'><span>CAD $ 2,717.31</span></li><li
                                                    class='Fp Hide CHF'><span>CHF 2,570.59</span></li><li class='Fp Hide XPF'><span>XPF
                                                        251,432.66</span></li><li class='Fp Hide DKK'><span>DKK 15,933.04</span></li><li
                                                            class='Fp Hide FJD'><span>FJD 4,803.05</span></li><li class='Fp Hide HKD'><span>HKD
                                                                21,093.22</span></li><li class='Fp Hide INR'><span>INR 147,825.91</span></li><li
                                                                    class='Fp Hide NOK'><span>NOK 15,596.58</span></li><li class='Fp Hide PKR'><span>PKR
                                                                        260,826.36</span></li><li class='Fp Hide PGK'><span>PGK 5,471.63</span></li><li class='Fp Hide PHP'>
                                                                            <span>PHP 111,581.27</span></li><li class='Fp Hide SGD'><span>SGD $ 3,329.53</span></li><li
                                                                                class='Fp Hide SBD'><span>SBD 19,199.54</span></li><li class='Fp Hide ZAR'><span>ZAR
                                                                                    R 23,613.12</span></li><li class='Fp Hide LKR'><span>LKR 354,491.22</span></li><li
                                                                                        class='Fp Hide SEK'><span>SEK 18,208.52</span></li><li class='Fp Hide THB'><span>THB
                                                                                            ฿ 82,241.10</span></li><li class='Fp Hide TOP'><span>TOP 4,522.29</span></li><li
                                                                                                class='Fp Hide VUV'><span>VUV 247,931.41</span></li><li class='Fp Hide WST'><span>WST
                                                                                                    6,080.84</span></li></ul>
                            <a class='ShowPriceList'>Price Details</a></div>
                        <a href='Configure.aspx?avp=ACE341F1A80145B8B033A8E559C4D506' class='SelectBtn'>Select</a></div>
                    <div class='PriceDetailsList NoFreeDays'>
                        <table class='chargesTable'>
                            <thead>
                                <tr>
                                    <th class='RateTH'>
                                        Product Description
                                    </th>
                                    <th class='FreeTH'>
                                        <span>Free Days</span><span rel='freeDays' class='popup' style='display: none;'>[?]</span>
                                    </th>
                                    <th class='HireTH'>
                                        Hire Days
                                    </th>
                                    <th class='PerTH'>
                                        Per Day Price
                                    </th>
                                    <th class='DiscTH'>
                                        <span>Discounted Day Price</span><span rel='DiscountedDaysPopUp' class='popup' onmouseover='initPopUp(this)'>[?]</span>
                                    </th>
                                    <th class='TotalTH'>
                                        Total
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class='rt'>
                                        21-Nov-2012 to 12-Dec-2012
                                    </td>
                                    <td class='fd'>
                                    </td>
                                    <td class='hp'>
                                        22 day(s)
                                    </td>
                                    <td class='pdp'>
                                        $115.00
                                    </td>
                                    <td class='dpd'>
                                        $105.80
                                    </td>
                                    <td>
                                        $2327.60
                                    </td>
                                </tr>
                                <tr rel='ONEWAYAU'>
                                    <td colspan='4'>
                                        Oneway Fee
                                    </td>
                                    <td>
                                        $280.00
                                    </td>
                                </tr>
                                <tr class='total'>
                                    <td class='TotalTD' colspan='4'>
                                        Total
                                    </td>
                                    <td>
                                        $2,607.60
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class='DiscountedDaysPopUp bubbleBody'>
                            <h3>
                                Discounted Day Price</h3>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            Long Hire Discount
                                        </td>
                                        <td>
                                            8.00%
                                        </td>
                                    </tr>
                                    <tr class='total' rel='8.00'>
                                        <td>
                                            Total Discount
                                        </td>
                                        <td>
                                            8.00%
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <span class='note'>(off standard per day rates)</span></div>
                    </div>
                </li>
                <li class='Collapse element'>
                    <div class='VehicleItem'>
                        <img src='https://secure.britz.com.au/images/b/icons/rowLogo.gif' title='Britz' class='RowLogo PopUp' /><a
                            href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.2B4WDB'
                            class='VehicleThumb PopUp'><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.2B4WDB/AUavb.2B4WDB-ClearCut-72.jpg'
                                border='0' title='2B4WDB' /></a><div class='VehicleFeatures s2'><span class="size" style="display:none;">2</span>
                                    <a href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.2B4WDB'
                                        class='PopUp'>Britz Bushcamper 4WD</a><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.2B4WDB/AUavb.2B4WDB-InclusionIcons.gif'
                                            border='0' title='Britz' /></div>
                        <div class='PriceDescription'>
                            <a href='Configure.aspx?avp=27D8F92FC96A422893714D8DD6C8FEB5' class='Avail'>AVAILABLE
                                NOW</a><span class='promo'><big>Adventure All The Way</big><small>Book before 15 Nov
                                    2012</small><small>Travel 1 Sep 12 to 18 Dec 12</small></span></div>
                        <div class='TotalPrice' title='SPRINGG'>
                            <ul class='Prices'>
                                <li><span>AUD 3,295.76</span></li><li class='Fp Hide NZD'><span>NZD $ 4,214.53</span></li><li
                                    class='Fp Hide USD'><span>USD $ 3,436.53</span></li><li class='Fp Hide GBP'><span>GBP
                                        £ 2,149.41</span></li><li class='Fp Hide AUD'><span>AUD $ 3,295.76</span></li><li
                                            class='Fp Hide EUR'><span>EUR € 2,696.03</span></li><li class='Fp Hide JPY'><span>JPY
                                                ¥ 272,944.98</span></li><li class='Fp Hide CAD'><span>CAD $ 3,434.42</span></li><li
                                                    class='Fp Hide CHF'><span>CHF 3,248.98</span></li><li class='Fp Hide XPF'><span>XPF
                                                        317,787.13</span></li><li class='Fp Hide DKK'><span>DKK 20,137.85</span></li><li
                                                            class='Fp Hide FJD'><span>FJD 6,070.60</span></li><li class='Fp Hide HKD'><span>HKD
                                                                26,659.83</span></li><li class='Fp Hide INR'><span>INR 186,837.98</span></li><li
                                                                    class='Fp Hide NOK'><span>NOK 19,712.61</span></li><li class='Fp Hide PKR'><span>PKR
                                                                        329,659.87</span></li><li class='Fp Hide PGK'><span>PGK 6,915.62</span></li><li class='Fp Hide PHP'>
                                                                            <span>PHP 141,028.18</span></li><li class='Fp Hide SGD'><span>SGD $ 4,208.21</span></li><li
                                                                                class='Fp Hide SBD'><span>SBD 24,266.40</span></li><li class='Fp Hide ZAR'><span>ZAR
                                                                                    R 29,844.75</span></li><li class='Fp Hide LKR'><span>LKR 448,043.40</span></li><li
                                                                                        class='Fp Hide SEK'><span>SEK 23,013.85</span></li><li class='Fp Hide THB'><span>THB
                                                                                            ฿ 103,944.98</span></li><li class='Fp Hide TOP'><span>TOP 5,715.74</span></li><li
                                                                                                class='Fp Hide VUV'><span>VUV 313,361.87</span></li><li class='Fp Hide WST'><span>WST
                                                                                                    7,685.61</span></li></ul>
                            <a class='ShowPriceList'>Price Details</a></div>
                        <a href='Configure.aspx?avp=27D8F92FC96A422893714D8DD6C8FEB5' class='SelectBtn'>Select</a></div>
                    <div class='PriceDetailsList NoFreeDays'>
                        <table class='chargesTable'>
                            <thead>
                                <tr>
                                    <th class='RateTH'>
                                        Product Description
                                    </th>
                                    <th class='FreeTH'>
                                        <span>Free Days</span><span rel='freeDays' class='popup' style='display: none;'>[?]</span>
                                    </th>
                                    <th class='HireTH'>
                                        Hire Days
                                    </th>
                                    <th class='PerTH'>
                                        Per Day Price
                                    </th>
                                    <th class='DiscTH'>
                                        <span>Discounted Day Price</span><span rel='DiscountedDaysPopUp' class='popup' onmouseover='initPopUp(this)'>[?]</span>
                                    </th>
                                    <th class='TotalTH'>
                                        Total
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class='rt'>
                                        21-Nov-2012 to 12-Dec-2012
                                    </td>
                                    <td class='fd'>
                                    </td>
                                    <td class='hp'>
                                        22 day(s)
                                    </td>
                                    <td class='pdp'>
                                        $149.00
                                    </td>
                                    <td class='dpd'>
                                        $137.08
                                    </td>
                                    <td>
                                        $3015.76
                                    </td>
                                </tr>
                                <tr rel='ONEWAYAU'>
                                    <td colspan='4'>
                                        Oneway Fee
                                    </td>
                                    <td>
                                        $280.00
                                    </td>
                                </tr>
                                <tr class='total'>
                                    <td class='TotalTD' colspan='4'>
                                        Total
                                    </td>
                                    <td>
                                        $3,295.76
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class='DiscountedDaysPopUp bubbleBody'>
                            <h3>
                                Discounted Day Price</h3>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            Long Hire Discount
                                        </td>
                                        <td>
                                            8.00%
                                        </td>
                                    </tr>
                                    <tr class='total' rel='8.00'>
                                        <td>
                                            Total Discount
                                        </td>
                                        <td>
                                            8.00%
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <span class='note'>(off standard per day rates)</span></div>
                    </div>
                </li>
                <li class='Collapse element'>
                    <div class='VehicleItem'>
                        <img src='https://secure.britz.com.au/images/b/icons/rowLogo.gif' title='Britz' class='RowLogo PopUp' /><a
                            href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.2B4WDBC'
                            class='VehicleThumb PopUp'><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.2B4WDBC/AUavb.2B4WDBC-ClearCut-72.jpg'
                                border='0' title='2B4WDBC' /></a><div class='VehicleFeatures s2'><span class="size" style="display:none;">2</span>
                                    <a href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.2B4WDBC'
                                        class='PopUp'>Britz Challenger 4WD</a><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.2B4WDBC/AUavb.2B4WDBC-InclusionIcons.gif'
                                            border='0' title='Britz' /></div>
                        <div class='PriceDescription'>
                            <a href='Configure.aspx?avp=46B6E7BEC0C445289ACD57739C77AB88' class='Avail'>AVAILABLE
                                NOW</a><span class='promo'><big>Adventure All The Way</big><small>Book before 15 Nov
                                    2012</small><small>Travel 1 Sep 12 to 18 Dec 12</small></span></div>
                        <div class='TotalPrice' title='SPRINGG'>
                            <ul class='Prices'>
                                <li><span>AUD 3,295.76</span></li><li class='Fp Hide NZD'><span>NZD $ 4,214.53</span></li><li
                                    class='Fp Hide USD'><span>USD $ 3,436.53</span></li><li class='Fp Hide GBP'><span>GBP
                                        £ 2,149.41</span></li><li class='Fp Hide AUD'><span>AUD $ 3,295.76</span></li><li
                                            class='Fp Hide EUR'><span>EUR € 2,696.03</span></li><li class='Fp Hide JPY'><span>JPY
                                                ¥ 272,944.98</span></li><li class='Fp Hide CAD'><span>CAD $ 3,434.42</span></li><li
                                                    class='Fp Hide CHF'><span>CHF 3,248.98</span></li><li class='Fp Hide XPF'><span>XPF
                                                        317,787.13</span></li><li class='Fp Hide DKK'><span>DKK 20,137.85</span></li><li
                                                            class='Fp Hide FJD'><span>FJD 6,070.60</span></li><li class='Fp Hide HKD'><span>HKD
                                                                26,659.83</span></li><li class='Fp Hide INR'><span>INR 186,837.98</span></li><li
                                                                    class='Fp Hide NOK'><span>NOK 19,712.61</span></li><li class='Fp Hide PKR'><span>PKR
                                                                        329,659.87</span></li><li class='Fp Hide PGK'><span>PGK 6,915.62</span></li><li class='Fp Hide PHP'>
                                                                            <span>PHP 141,028.18</span></li><li class='Fp Hide SGD'><span>SGD $ 4,208.21</span></li><li
                                                                                class='Fp Hide SBD'><span>SBD 24,266.40</span></li><li class='Fp Hide ZAR'><span>ZAR
                                                                                    R 29,844.75</span></li><li class='Fp Hide LKR'><span>LKR 448,043.40</span></li><li
                                                                                        class='Fp Hide SEK'><span>SEK 23,013.85</span></li><li class='Fp Hide THB'><span>THB
                                                                                            ฿ 103,944.98</span></li><li class='Fp Hide TOP'><span>TOP 5,715.74</span></li><li
                                                                                                class='Fp Hide VUV'><span>VUV 313,361.87</span></li><li class='Fp Hide WST'><span>WST
                                                                                                    7,685.61</span></li></ul>
                            <a class='ShowPriceList'>Price Details</a></div>
                        <a href='Configure.aspx?avp=46B6E7BEC0C445289ACD57739C77AB88' class='SelectBtn'>Select</a></div>
                    <div class='PriceDetailsList NoFreeDays'>
                        <table class='chargesTable'>
                            <thead>
                                <tr>
                                    <th class='RateTH'>
                                        Product Description
                                    </th>
                                    <th class='FreeTH'>
                                        <span>Free Days</span><span rel='freeDays' class='popup' style='display: none;'>[?]</span>
                                    </th>
                                    <th class='HireTH'>
                                        Hire Days
                                    </th>
                                    <th class='PerTH'>
                                        Per Day Price
                                    </th>
                                    <th class='DiscTH'>
                                        <span>Discounted Day Price</span><span rel='DiscountedDaysPopUp' class='popup' onmouseover='initPopUp(this)'>[?]</span>
                                    </th>
                                    <th class='TotalTH'>
                                        Total
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class='rt'>
                                        21-Nov-2012 to 12-Dec-2012
                                    </td>
                                    <td class='fd'>
                                    </td>
                                    <td class='hp'>
                                        22 day(s)
                                    </td>
                                    <td class='pdp'>
                                        $149.00
                                    </td>
                                    <td class='dpd'>
                                        $137.08
                                    </td>
                                    <td>
                                        $3015.76
                                    </td>
                                </tr>
                                <tr rel='ONEWAYAU'>
                                    <td colspan='4'>
                                        Oneway Fee
                                    </td>
                                    <td>
                                        $280.00
                                    </td>
                                </tr>
                                <tr class='total'>
                                    <td class='TotalTD' colspan='4'>
                                        Total
                                    </td>
                                    <td>
                                        $3,295.76
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class='DiscountedDaysPopUp bubbleBody'>
                            <h3>
                                Discounted Day Price</h3>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            Long Hire Discount
                                        </td>
                                        <td>
                                            8.00%
                                        </td>
                                    </tr>
                                    <tr class='total' rel='8.00'>
                                        <td>
                                            Total Discount
                                        </td>
                                        <td>
                                            8.00%
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <span class='note'>(off standard per day rates)</span></div>
                    </div>
                </li>
                <li class='Collapse element'>
                    <div class='VehicleItem'>
                        <img src='https://secure.britz.com.au/images/b/icons/rowLogo.gif' title='Britz' class='RowLogo PopUp' /><a
                            href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.6BB'
                            class='VehicleThumb PopUp'><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.6BB/AUavb.6BB-ClearCut-72.jpg'
                                border='0' title='6BB' /></a><div class='VehicleFeatures s6'><span class="size" style="display:none;">6</span>
                                    <a href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.6BB'
                                        class='PopUp'>Britz Frontier</a><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.6BB/AUavb.6BB-InclusionIcons.gif'
                                            border='0' title='Britz' /></div>
                        <div class='PriceDescription'>
                            <a href='Configure.aspx?avp=65997AA9027047DDBE066B5CC52AF405' class='Avail'>AVAILABLE
                                NOW</a></div>
                        <div class='TotalPrice' title='DFLEXBG'>
                            <ul class='Prices'>
                                <li><span>AUD 4,732.80</span></li><li class='Fp Hide NZD'><span>NZD $ 6,052.17</span></li><li
                                    class='Fp Hide USD'><span>USD $ 4,934.94</span></li><li class='Fp Hide GBP'><span>GBP
                                        £ 3,086.61</span></li><li class='Fp Hide AUD'><span>AUD $ 4,732.80</span></li><li
                                            class='Fp Hide EUR'><span>EUR € 3,871.58</span></li><li class='Fp Hide JPY'><span>JPY
                                                ¥ 391,956.33</span></li><li class='Fp Hide CAD'><span>CAD $ 4,931.92</span></li><li
                                                    class='Fp Hide CHF'><span>CHF 4,665.62</span></li><li class='Fp Hide XPF'><span>XPF
                                                        456,350.86</span></li><li class='Fp Hide DKK'><span>DKK 28,918.50</span></li><li
                                                            class='Fp Hide FJD'><span>FJD 8,717.55</span></li><li class='Fp Hide HKD'><span>HKD
                                                                38,284.24</span></li><li class='Fp Hide INR'><span>INR 268,304.37</span></li><li
                                                                    class='Fp Hide NOK'><span>NOK 28,307.83</span></li><li class='Fp Hide PKR'><span>PKR
                                                                        473,400.44</span></li><li class='Fp Hide PGK'><span>PGK 9,931.01</span></li><li class='Fp Hide PHP'>
                                                                            <span>PHP 202,520.26</span></li><li class='Fp Hide SGD'><span>SGD $ 6,043.10</span></li><li
                                                                                class='Fp Hide SBD'><span>SBD 34,847.21</span></li><li class='Fp Hide ZAR'><span>ZAR
                                                                                    R 42,857.86</span></li><li class='Fp Hide LKR'><span>LKR 643,402.37</span></li><li
                                                                                        class='Fp Hide SEK'><span>SEK 33,048.50</span></li><li class='Fp Hide THB'><span>THB
                                                                                            ฿ 149,267.79</span></li><li class='Fp Hide TOP'><span>TOP 8,207.96</span></li><li
                                                                                                class='Fp Hide VUV'><span>VUV 449,996.08</span></li><li class='Fp Hide WST'><span>WST
                                                                                                    11,036.74</span></li></ul>
                            <a class='ShowPriceList'>Price Details</a></div>
                        <a href='Configure.aspx?avp=65997AA9027047DDBE066B5CC52AF405' class='SelectBtn'>Select</a></div>
                    <div class='PriceDetailsList NoFreeDays'>
                        <table class='chargesTable'>
                            <thead>
                                <tr>
                                    <th class='RateTH'>
                                        Product Description
                                    </th>
                                    <th class='FreeTH'>
                                        <span>Free Days</span><span rel='freeDays' class='popup' style='display: none;'>[?]</span>
                                    </th>
                                    <th class='HireTH'>
                                        Hire Days
                                    </th>
                                    <th class='PerTH'>
                                        Per Day Price
                                    </th>
                                    <th class='DiscTH'>
                                        <span>Discounted Day Price</span><span rel='DiscountedDaysPopUp' class='popup' onmouseover='initPopUp(this)'>[?]</span>
                                    </th>
                                    <th class='TotalTH'>
                                        Total
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class='rt'>
                                        21-Nov-2012 to 12-Dec-2012
                                    </td>
                                    <td class='fd'>
                                    </td>
                                    <td class='hp'>
                                        22 day(s)
                                    </td>
                                    <td class='pdp'>
                                        $220.00
                                    </td>
                                    <td class='dpd'>
                                        $202.40
                                    </td>
                                    <td>
                                        $4452.80
                                    </td>
                                </tr>
                                <tr rel='ONEWAYAU'>
                                    <td colspan='4'>
                                        Oneway Fee
                                    </td>
                                    <td>
                                        $280.00
                                    </td>
                                </tr>
                                <tr class='total'>
                                    <td class='TotalTD' colspan='4'>
                                        Total
                                    </td>
                                    <td>
                                        $4,732.80
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class='DiscountedDaysPopUp bubbleBody'>
                            <h3>
                                Discounted Day Price</h3>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            Long Hire Discount
                                        </td>
                                        <td>
                                            8.00%
                                        </td>
                                    </tr>
                                    <tr class='total' rel='8.00'>
                                        <td>
                                            Total Discount
                                        </td>
                                        <td>
                                            8.00%
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <span class='note'>(off standard per day rates)</span></div>
                    </div>
                </li>
                <li class='Collapse element'>
                    <div class='VehicleItem'>
                        <img src='https://secure.britz.com.au/images/b/icons/rowLogo.gif' title='Britz' class='RowLogo PopUp' /><a
                            href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.4BBJ'
                            class='VehicleThumb PopUp'><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.4BBJ/AUavb.4BBJ-ClearCut-72.jpg'
                                border='0' title='4BBJ' /></a><div class='VehicleFeatures s4'><span class="size" style="display:none;">4</span>
                                    <a href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.4BBJ'
                                        class='PopUp'>Britz Maverick</a><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.4BBJ/AUavb.4BBJ-InclusionIcons.gif'
                                            border='0' title='Britz' /></div>
                        <div class='PriceDescription'>
                            <a href='Configure.aspx?avp=EDDE352230D84490B0D50B16C30910E9' class='Avail'>AVAILABLE
                                NOW</a></div>
                        <div class='TotalPrice' title='DFLEXBG'>
                            <ul class='Prices'>
                                <li><span>AUD 4,854.24</span></li><li class='Fp Hide NZD'><span>NZD $ 6,207.47</span></li><li
                                    class='Fp Hide USD'><span>USD $ 5,061.57</span></li><li class='Fp Hide GBP'><span>GBP
                                        £ 3,165.81</span></li><li class='Fp Hide AUD'><span>AUD $ 4,854.24</span></li><li
                                            class='Fp Hide EUR'><span>EUR € 3,970.92</span></li><li class='Fp Hide JPY'><span>JPY
                                                ¥ 402,013.63</span></li><li class='Fp Hide CAD'><span>CAD $ 5,058.47</span></li><li
                                                    class='Fp Hide CHF'><span>CHF 4,785.34</span></li><li class='Fp Hide XPF'><span>XPF
                                                        468,060.47</span></li><li class='Fp Hide DKK'><span>DKK 29,660.52</span></li><li
                                                            class='Fp Hide FJD'><span>FJD 8,941.24</span></li><li class='Fp Hide HKD'><span>HKD
                                                                39,266.58</span></li><li class='Fp Hide INR'><span>INR 275,188.85</span></li><li
                                                                    class='Fp Hide NOK'><span>NOK 29,034.19</span></li><li class='Fp Hide PKR'><span>PKR
                                                                        485,547.53</span></li><li class='Fp Hide PGK'><span>PGK 10,185.83</span></li><li
                                                                            class='Fp Hide PHP'><span>PHP 207,716.78</span></li><li class='Fp Hide SGD'><span>SGD
                                                                                $ 6,198.16</span></li><li class='Fp Hide SBD'><span>SBD 35,741.36</span></li><li
                                                                                    class='Fp Hide ZAR'><span>ZAR R 43,957.56</span></li><li class='Fp Hide LKR'><span>LKR
                                                                                        659,911.58</span></li><li class='Fp Hide SEK'><span>SEK 33,896.50</span></li><li
                                                                                            class='Fp Hide THB'><span>THB ฿ 153,097.89</span></li><li class='Fp Hide TOP'><span>
                                                                                                TOP 8,418.57</span></li><li class='Fp Hide VUV'><span>VUV 461,542.63</span></li><li
                                                                                                    class='Fp Hide WST'><span>WST 11,319.94</span></li></ul>
                            <a class='ShowPriceList'>Price Details</a></div>
                        <a href='Configure.aspx?avp=EDDE352230D84490B0D50B16C30910E9' class='SelectBtn'>Select</a></div>
                    <div class='PriceDetailsList NoFreeDays'>
                        <table class='chargesTable'>
                            <thead>
                                <tr>
                                    <th class='RateTH'>
                                        Product Description
                                    </th>
                                    <th class='FreeTH'>
                                        <span>Free Days</span><span rel='freeDays' class='popup' style='display: none;'>[?]</span>
                                    </th>
                                    <th class='HireTH'>
                                        Hire Days
                                    </th>
                                    <th class='PerTH'>
                                        Per Day Price
                                    </th>
                                    <th class='DiscTH'>
                                        <span>Discounted Day Price</span><span rel='DiscountedDaysPopUp' class='popup' onmouseover='initPopUp(this)'>[?]</span>
                                    </th>
                                    <th class='TotalTH'>
                                        Total
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class='rt'>
                                        21-Nov-2012 to 12-Dec-2012
                                    </td>
                                    <td class='fd'>
                                    </td>
                                    <td class='hp'>
                                        22 day(s)
                                    </td>
                                    <td class='pdp'>
                                        $226.00
                                    </td>
                                    <td class='dpd'>
                                        $207.92
                                    </td>
                                    <td>
                                        $4574.24
                                    </td>
                                </tr>
                                <tr rel='ONEWAYAU'>
                                    <td colspan='4'>
                                        Oneway Fee
                                    </td>
                                    <td>
                                        $280.00
                                    </td>
                                </tr>
                                <tr class='total'>
                                    <td class='TotalTD' colspan='4'>
                                        Total
                                    </td>
                                    <td>
                                        $4,854.24
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class='DiscountedDaysPopUp bubbleBody'>
                            <h3>
                                Discounted Day Price</h3>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            Long Hire Discount
                                        </td>
                                        <td>
                                            8.00%
                                        </td>
                                    </tr>
                                    <tr class='total' rel='8.00'>
                                        <td>
                                            Total Discount
                                        </td>
                                        <td>
                                            8.00%
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <span class='note'>(off standard per day rates)</span></div>
                    </div>
                </li>
                <li class='Collapse element'>
                    <div class='VehicleItem'>
                        <img src='https://secure.britz.com.au/images/b/icons/rowLogo.gif' title='Britz' class='RowLogo' /><a
                            href='http://www.britz.co.nz/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.2BTSBV'
                            class='VehicleThumb PopUp'><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.2BTSBV/AUavb.2BTSBV-ClearCut-72.jpg'
                                border='0' title='2BTSBV' /></a><div class='VehicleFeatures s2'><span class="size" style="display:none;">2</span>
                                    <a href='http://www.britz.co.nz/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.2BTSBV'
                                        class='PopUp'>Britz Venturer</a><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.2BTSBV/AUavb.2BTSBV-InclusionIcons.gif'
                                            border='0' title='Britz' /></div>
                        <div class='Err'>
                            <span>Vehicle not available until 1st April 2013. Please try the Elite: 2BTSB.</span></div>
                        <div class='Limited'>
                            <div class='Top'>
                                <span><a href='javascript:preloadPage("vCode=2BTSBV&rType=GetAlternateOptions&Brand=b")'>
                                    Unavailable - Search Alternative Availability</a></span><span class='Contact'>Search
                                        online to find the following<br />
                                        alternative travel options for this vehicle:<br />
                                        <b>Reverse route, +/- 10 days, Shorter hire.</b></span></div>
                            <div class='AALink LeftLnk'>
                                <a class='aAvail' href='javascript:preloadPage("vCode=2BTSBV&rType=GetAlternateOptions&Brand=b")'>
                                    Search Alternate Availability</a></div>
                        </div>
                    </div>
                </li>
                <li class='Collapse element'>
                    <div class='VehicleItem'>
                        <img src='https://secure.britz.com.au/images/b/icons/rowLogo.gif' title='Britz' class='RowLogo' /><a
                            href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.2BTSB'
                            class='VehicleThumb PopUp'><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.2BTSB/AUavb.2BTSB-ClearCut-72.jpg'
                                border='0' title='2BTSB' /></a><div class='VehicleFeatures s2'><span class="size" style="display:none;">2</span>
                                    <a href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.2BTSB'
                                        class='PopUp'>Britz Elite</a><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.2BTSB/AUavb.2BTSB-InclusionIcons.gif'
                                            border='0' title='Britz' /></div>
                        <div class='Err'>
                            <span>Model on stopsell until 25th November 2012</span></div>
                        <div class='Limited'>
                            <div class='Top'>
                                <span><a href='javascript:preloadPage("vCode=2BTSB&rType=GetAlternateOptions&Brand=b")'>
                                    Unavailable - Search Alternative Availability</a></span><span class='Contact'>Search
                                        online to find the following<br />
                                        alternative travel options for this vehicle:<br />
                                        <b>Reverse route, +/- 10 days, Shorter hire.</b></span></div>
                            <div class='AALink LeftLnk'>
                                <a class='aAvail' href='javascript:preloadPage("vCode=2BTSB&rType=GetAlternateOptions&Brand=b")'>
                                    Search Alternate Availability</a></div>
                        </div>
                    </div>
                </li>
                <li class='Collapse element'>
                    <div class='VehicleItem'>
                        <img src='https://secure.britz.com.au/images/b/icons/rowLogo.gif' title='Britz' class='RowLogo' /><a
                            href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.5BB'
                            class='VehicleThumb PopUp'><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.5BB/AUavb.5BB-ClearCut-72.jpg'
                                border='0' title='5BB' /></a><div class='VehicleFeatures s5'><span class="size" style="display:none;">5</span>
                                    <a href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.5BB'
                                        class='PopUp'>Britz Escape</a><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.5BB/AUavb.5BB-InclusionIcons.gif'
                                            border='0' title='Britz' /></div>
                        <div class='Err'>
                            <span>Model on stopsell until 25th November 2012</span></div>
                        <div class='Limited'>
                            <div class='Top'>
                                <span><a href='javascript:preloadPage("vCode=5BB&rType=GetAlternateOptions&Brand=b")'>
                                    Unavailable - Search Alternative Availability</a></span><span class='Contact'>Search
                                        online to find the following<br />
                                        alternative travel options for this vehicle:<br />
                                        <b>Reverse route, +/- 10 days, Shorter hire.</b></span></div>
                            <div class='AALink LeftLnk'>
                                <a class='aAvail' href='javascript:preloadPage("vCode=5BB&rType=GetAlternateOptions&Brand=b")'>
                                    Search Alternate Availability</a></div>
                        </div>
                    </div>
                </li>
                <li class='Collapse element'>
                    <div class='VehicleItem'>
                        <img src='https://secure.britz.com.au/images/b/icons/rowLogo.gif' title='Britz' class='RowLogo' /><a
                            href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.4BB'
                            class='VehicleThumb PopUp'><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.4BB/AUavb.4BB-ClearCut-72.jpg'
                                border='0' title='4BB' /></a><div class='VehicleFeatures s4'><span class="size" style="display:none;">4</span>
                                    <a href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.4BB'
                                        class='PopUp'>Britz Explorer</a><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.4BB/AUavb.4BB-InclusionIcons.gif'
                                            border='0' title='Britz' /></div>
                        <div class='Err'>
                            <span>Message = DVASS: No vehicles available Detail = 10 Status = 1<br />
                                LIMITED AVAILABILITY</span></div>
                        <div class='Limited'>
                            <div class='Top'>
                                <span><a href='javascript:preloadPage("vCode=4BB&rType=GetAlternateOptions&Brand=b")'>
                                    Unavailable - Search Alternative Availability</a></span><span class='Contact'>Search
                                        online to find the following<br />
                                        alternative travel options for this vehicle:<br />
                                        <b>Reverse route, +/- 10 days, Shorter hire.</b></span></div>
                            <div class='AALink LeftLnk'>
                                <a class='aAvail' href='javascript:preloadPage("vCode=4BB&rType=GetAlternateOptions&Brand=b")'>
                                    Search Alternate Availability</a></div>
                        </div>
                    </div>
                </li>
                <li class='Collapse element'>
                    <div class='VehicleItem'>
                        <img src='https://secure.britz.com.au/images/b/icons/rowLogo.gif' title='Britz' class='RowLogo' /><a
                            href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.6BBR'
                            class='VehicleThumb PopUp'><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.6BBR/AUavb.6BBR-ClearCut-72.jpg'
                                border='0' title='6BBR' /></a><div class='VehicleFeatures s6'><span class="size" style="display:none;">4</span>
                                    <a href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.6BBR'
                                        class='PopUp'>Britz Renegade</a><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.6BBR/AUavb.6BBR-InclusionIcons.gif'
                                            border='0' title='Britz' /></div>
                        <div class='Err'>
                            <span>Message = DVASS: No vehicles available Detail = 10 Status = 1<br />
                                LIMITED AVAILABILITY</span></div>
                        <div class='Limited'>
                            <div class='Top'>
                                <span><a href='javascript:preloadPage("vCode=6BBR&rType=GetAlternateOptions&Brand=b")'>
                                    Unavailable - Search Alternative Availability</a></span><span class='Contact'>Search
                                        online to find the following<br />
                                        alternative travel options for this vehicle:<br />
                                        <b>Reverse route, +/- 10 days, Shorter hire.</b></span></div>
                            <div class='AALink LeftLnk'>
                                <a class='aAvail' href='javascript:preloadPage("vCode=6BBR&rType=GetAlternateOptions&Brand=b")'>
                                    Search Alternate Availability</a></div>
                        </div>
                    </div>
                </li>
                <li class='Collapse element'>
                    <div class='VehicleItem'>
                        <img src='https://secure.britz.com.au/images/b/icons/rowLogo.gif' title='Britz' class='RowLogo' /><a
                            href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.2BB'
                            class='VehicleThumb PopUp'><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.2BB/AUavb.2BB-ClearCut-72.jpg'
                                border='0' title='2BB' /></a><div class='VehicleFeatures s2'><span class="size" style="display:none;">2</span>
                                    <a href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.2BB'
                                        class='PopUp'>Britz HiTop</a><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.2BB/AUavb.2BB-InclusionIcons.gif'
                                            border='0' title='Britz' /></div>
                        <div class='Err'>
                            <span>Model on stopsell until 25th November 2012</span></div>
                        <div class='Limited'>
                            <div class='Top'>
                                <span><a href='javascript:preloadPage("vCode=2BB&rType=GetAlternateOptions&Brand=b")'>
                                    Unavailable - Search Alternative Availability</a></span><span class='Contact'>Search
                                        online to find the following<br />
                                        alternative travel options for this vehicle:<br />
                                        <b>Reverse route, +/- 10 days, Shorter hire.</b></span></div>
                            <div class='AALink LeftLnk'>
                                <a class='aAvail' href='javascript:preloadPage("vCode=2BB&rType=GetAlternateOptions&Brand=b")'>
                                    Search Alternate Availability</a></div>
                        </div>
                    </div>
                </li>
                <li class='Collapse element'>
                    <div class='VehicleItem'>
                        <img src='https://secure.britz.com.au/images/b/icons/rowLogo.gif' title='Britz' class='RowLogo' /><a
                            href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.4BBXS'
                            class='VehicleThumb PopUp'><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.4BBXS/AUavb.4BBXS-ClearCut-72.jpg'
                                border='0' title='4BBXS' /></a><div class='VehicleFeatures s4'><span class="size" style="display:none;">4</span>
                                    <a href='http://www.britz.com.au/campervanhire/pages/CampervanPopup.aspx?vehiclecode=auavb.4BBXS'
                                        class='PopUp'>Britz Voyager</a><img src='https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/AUavb.4BBXS/AUavb.4BBXS-InclusionIcons.gif'
                                            border='0' title='Britz' /></div>
                        <div class='Err'>
                            <span>Model on stopsell until 2nd December 2012</span></div>
                        <div class='Limited'>
                            <div class='Top'>
                                <span><a href='javascript:preloadPage("vCode=4BBXS&rType=GetAlternateOptions&Brand=b")'>
                                    Unavailable - Search Alternative Availability</a></span><span class='Contact'>Search
                                        online to find the following<br />
                                        alternative travel options for this vehicle:<br />
                                        <b>Reverse route, +/- 10 days, Shorter hire.</b></span></div>
                            <div class='AALink LeftLnk'>
                                <a class='aAvail' href='javascript:preloadPage("vCode=4BBXS&rType=GetAlternateOptions&Brand=b")'>
                                    Search Alternate Availability</a></div>
                        </div>
                    </div>
                </li>
            </ul>
            <div id="emptySelectionMsg">
                <span></span>
            </div>
        </div>
        <div id="staticLoader">
            <span>Reloading your search results... </span>
            <ul id="carousel">
                <li>
                    <img src="https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/SelectionPreloader/Ad1.png"
                        alt="" /></li>
                <li>
                    <img src="https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/SelectionPreloader/Ad2.png"
                        alt="" /></li>
                <li>
                    <img src="https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/SelectionPreloader/Ad3.png"
                        alt="" /></li>
                <li>
                    <img src="https://secure.britz.com.au/CentralLibraryImages/Britz/Australia/Campervans/SelectionPreloader/Ad4.png"
                        alt="" /></li>
            </ul>
        </div>
        <div id="disclaimer" title="disclaimer">
            <span>Please note: Rental prices quoted above are only valid at the time of booking.</span>
            <span>Alternative currency is based on approximate current exchange rates. All payments
                will be processed in AUD.</span>
        </div>
        <!-- Debug Code Starts -->
        <!-- Debug Code Ends -->
    </div>
    <img src='https://au.track.decideinteractive.com/n/45517/45523/BRITZ1A/x/e?domain=au.track.decideinteractive.com'
        width='1' height='1' border='0' />
    <!-- BEGIN: Marin Software Tracking Script -->
    <script type="text/javascript">
        var _mTrack = _mTrack || [];
        _mTrack.push(['addTrans', { items: [{ convType: 'britz-m1-au'}]}]);
        _mTrack.push(['processOrders']);
        (function () {
            var mClientId = '1624rrj16003';
            var mProto = ('https:' == document.location.protocol ? 'https://' : 'http://');
            var mHost = 'tracker.marinsm.com';
            var mt = document.createElement('script');
            mt.type = 'text/javascript';
            mt.async = true;
            mt.src = mProto + mHost + '/tracker/async/' + mClientId + '.js';
            var fscr = document.getElementsByTagName('script')[0];
            fscr.parentNode.insertBefore(mt, fscr);
        })(); 
    </script>
    <noscript>
        <img src="https://tracker.marinsm.com/tp?act=2&cid=1624rrj16003&script=no"></noscript>
    <!-- END: Copyright Marin Software -->
    </form>    
</body>
</html>
