﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Text;

public partial class UnitTests_TokenTester : System.Web.UI.Page
{    
    
    public string AgentToken { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        AgentToken = AgentKeyJSON(Request.Params["agentId"]);
    }

    /// <summary>
    /// Get the Agents Token in JSON format, TODO: refactor into B2BCrossSell Utils
    /// </summary>
    /// <param name="agentId"></param>
    /// <returns></returns>
    public static string AgentKeyJSON(string agentId)
    {
        try
        {
            if(string.IsNullOrEmpty(agentId))
                return "{\"token\":null}";
            string requestUrl = "http://local.alphacampervans.co.nz/webservices/setagenttoken.aspx?agentId=" + agentId;//from config
            HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception(String.Format(
                    "Server error (HTTP {0}: {1}).",
                    response.StatusCode,
                    response.StatusDescription));
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                string json = readStream.ReadToEnd();
                return json;
            }
        }
        catch (Exception ex)
        {
            THLDebug.LogError(ErrorTypes.ExternalProvider, "AgentKeyJSON", ex.Message, "{agentId:\""+ agentId +"\"}");
            return "{\"token\":null}"; 
        }    
    }
}