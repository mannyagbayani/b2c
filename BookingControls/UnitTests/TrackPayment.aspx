﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TrackPayment.aspx.cs" Inherits="UnitTests_TrackPayment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <a href="TrackConfirmation.aspx">Confirm This Booking</a>
    </div>
    </form>
    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        try {
            var pageTracker = _gat._getTracker("UA-16069586-1");
            pageTracker._setDomainName(".britz.com.au");
            pageTracker._setAllowLinker(true);
            pageTracker._trackPageview();
            
            <%= TrackerStr %>                
            
        } catch (err) { }
    </script>
</body>
</html>
