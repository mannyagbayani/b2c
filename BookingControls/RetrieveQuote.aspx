﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RetrieveQuote.aspx.cs" Inherits="RetrieveQuote" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Configure Quote Page</title>
    <%--<link href="<%= AssetsURL %>/css/Base.css?v=1.7" type="text/css" rel="stylesheet" />
    <link href="<%= AssetsURL %>/css/<%= brandStr %>/selection.css?v=1.7" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" media="all" href="<%= AssetsURL %>/css/<%= brandStr %>/horizontal.css" />
    <link href="<%= AssetsURL %>/css/jquery.ui.dialog.css" rel="stylesheet" type="text/css" />
    <link href="<%= AssetsURL %>/css/<%= brandStr %>/jcal.css" type="text/css" rel="stylesheet" />--%>

    <link href="<%= AssetsURL %>/css/Base.css?v=<%= Version %>" type="text/css" rel="stylesheet" />
    <link href="<%= AssetsURL %>/css/<%= BrandChar %>/configure.css" type="text/css" rel="stylesheet" />    
    <link href="<%= CSSLink %>?v=<%= Version %>" type="text/css" rel="stylesheet" />
    <link href="<%= AssetsURL %>/css/jquery.ui.dialog.css" rel="stylesheet" type="text/css"/>      
    <link href="<%= AssetsURL %>/css/<%= BrandChar %>/jcal.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript" src="<%= ContentURL %>js/jquery-1.3.1.min.js"></script>
    <script src="<%= ContentURL %>js/jquery-ui.min.js" type="text/javascript"></script>
    <%--<script type="text/javascript" src="<%= ContentURL %>js/jquery.dimensions.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.tooltip.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/date.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/hCtrl.js?v=1.7"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/selection.js?v=1.7"></script>
    --%>

    <%--<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>--%>
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.validate.js"></script> 
    <script src="/js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.dimensions.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.tooltip.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/date.js"></script>     
    <script type="text/javascript" src="<%= ContentURL %>js/hCtrl.js?v=<%= Version %>"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/selection.js?v=<%= Version %>"></script>
    <![if !IE]>
    <script type="text/javascript" src="<%= ContentURL %>js/enquire.min.js?v=<%= Version %>"></script>   
    <![endif]>

    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"> 

    <%= AnalyticsCode %>
    <%= OpenTag %>
</head>
<body>
    <form id="form1" runat="server" onsubmit="return serialiseQuote()">
        <%= DNAIMGTag %>
        <script type="text/javascript">
            //TODO: restore js inits initConfigureQuote()
            var logJS = true;            
            var $j = jQuery.noConflict();
            var siteInfo = <%= SiteInfoJSON %>;
            jQuery(document).ready(function() {
                initConfigureQuote(); 
            });           
        </script>
        <div id="header">
            <div class="headerContent">
                <div id="genericHeader">
                    <a href="/">
                        <img src="<%= AssetsURL %>/images/<%= BrandChar %>/icons/logo.png" alt="logo" />
                    </a>
                    <ul class="steps">
                        <li>
                            <span class="tab">1. <label>Select</label></span>
                        </li>
                        <li class="selected">
                            <span class="tab">2. <label>Extras</label></span>
                        </li>
                        <li>
                            <span class="tab">3. <label>Payment</label></span>
                        </li>
                    </ul>
                    <a class="contactUs" href="<%= ContactUsURL %>" target="_blank" ><span>Contact us <i class="icon-chevron-down"></i></span></a>
                    <a class="contactUs contactUsMobile" href="<%= ContactUsURL %>" target="_blank"><span><i class="icon-envelope"></i></span></a>
                </div>  
            </div>
        </div>

        <div id="vehicleConfigureContainer" class="quote cc<%= CountryCodeStr %>">
            <%--<div id="catalogHeader">
                <span class="Image">
                    <img src="<%= AssetsURL %>/images/<%= brandStr %>/logo.jpg" alt="logo" />
                </span>
                <a class="priceMatch" href="javascript:openPriceMatchWindow();">Price match guarantee</a>
                <span class="ContactInfo">
                    <%= ContactHeaderText %> or <a href="<%= ParentSiteURL %>/contactus" target="_blank">contact us</a>
                </span>
            </div>
            <div id="breadCrumbs">
                <span class="Crumbs">Quote
                </span>
                <ul id="stepsDisplayer">
                    <li>
                        <span id="PaymentStep">3.Payment</span>
                    </li>
                    <li>
                        <span id="ConfigureStep">2.Configure</span>
                    </li>
                    <li>
                        <span id="SelectStep">1.Select</span>
                    </li>
                </ul>
            </div>--%>
            <div id="vehicleCatalogContainer">
                <h2>Confirm your Quote</h2>
                <div id="configurePanel">
                    <div id="promptContainer" >
                        <div class="" id="promptTextArea">
                            <span>This new quote will expire on <%= quote.ExpiryDate.ToString("dd - MMM - yyyy")%>,
                            <%= DepositTxt %></span>
                        </div>
                        <input type="submit" value="Payment Details" />
                    </div>
                    <div id="topHeader">
                        <h3>
                                <a href="http://<%= VehicleContentLink %>" class="VehicleTitle PopUp">
                                    <%= VehicleName %>
                                </a>
                            </h3>
                        <span class="CurrencyPanel" style="visibility: hidden;">
                            <small>Alternative Currency:</small>
                            <%= CurrencySelector %>                        
                        </span>
                    </div>
                    <div id="vehicleTopPanel">
                        <div class="VehicleHeader">
                            
                            <h4>Travel details:&nbsp;</h4>
                            <div class="Locations">
                                <span><b>Pick up:</b> <%= PickUpLocationStr %>, <%= PickUpDateStr %></span>
                                <span><b>Drop off:</b> <%= DropOffLocationStr %>, <%= DropOffDateStr %></span>
                            </div>
                        </div>
                        <div class="VehicleImages">
                            <div class="Left">
                                <img src="<%= VehicleInclusionPath %>-InclusionIcons.gif" alt="<%= quote.Request.VehicleName %> inclusions" title="<%= quote.Request.VehicleName %> inclusions" />
                            </div>
                            <div class="Right">
                                <img src="<%= VehicleImagePath %>-ClearCut-144.jpg" title="<%= quote.Request.VehicleModel %>" alt="<%= quote.Request.VehicleModel %>" />
                            </div>
                        </div>
                    </div>
                    <%= FeesView %>
                    <%= INSView %>
                    <%= EHIView %>
                    <div id="ferryPanel" <%= (HasFerryProduct ? "" : "style='display:none;'") %>>
                        <span class="ferryDisc">Please note that <b>if your quote included a ferry crossing booking</b> (for your vehicle, or for yourself and/or others)</span>
                        <span class="ferryDisc">this has NOT been carried across to this booking confirmation. If you still wish to book a ferry crossing, <b>please re-add this to your booking now.</b><br />
                            <br />
                        </span>
                        <script type="text/javascript">ferryItems = <%= FerryJSON %>;</script>
                        <input type="hidden" id="ferryNote" name="ferryNote" />
                        <%= FerryView %>
                    </div>
                    <div id="configureUpdatePanel">
                        <span class="Total">
                            <label>Current total:</label>
                            <span class="Price">
                                <small id="totalAmount">0.00</small>
                                <small id="alternateCurrency">0.0</small>
                            </span>
                        </span>
                        <span class="TaxMsg <%= (quote.AdminFee > 0 ? "HasAdminFee" : "") %>">
                            <small>Total includes all taxes (GST)</small>
                            <big>Includes <%= quote.AdminFee %>% Admin Fee</big>
                        </span>
                        <span class="Submit">
                            <input type="submit" value="Payment Details" id="confirmQuoteSubmit" />
                            <a id="saveQuoteLnk" href="Selection.aspx?<%= SearchAgainURLParamsNoVehicle %>" title="Search Again">Search Again</a>
                            <input type="hidden" id="bookingSubmitted" name="bookingSubmitted" value="0" />
                        </span>
                    </div>
                </div>
            </div>
            <%--<div id="disclaimer">
                <span>Please note: Rental prices quoted above are only valid at the time of booking.</span>
            </div>--%>
            <!-- Debug Code Starts -->
            <%= debugHTML %>
        </div>

        <!-- footer starts -->
            <div id="footerContainer">
                <div id="footer">
                    <div class="top">
                        <div id="disclaimer">
                            <span>
                                <strong>Please note:</strong> Rentals prices quoted above are only vaild at the time of booking.
                            </span>
                            <span>Alternative currency is based on approximate current exchange rates. All payments will be processed in <%= Currency %>.</span>
                        </div>
                        <div id="footerlogo">
                            <a href="#">
                                <img src="<%= AssetsURL %>/images/<%= BrandChar %>/icons/foot_logo.png" alt="Worldwide Rentals - Motorhomes and Cars.com" />
                            </a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div id="footerWrap">
                    <div class="thl body">
                        <ul>
                            <li>
                                <a class="thl-logo" href="http://www.thlonline.com">
                                    <span class="thl"></span>
                                </a>
                            </li>
                            <li>
                                <a class="thllogos" href="http://www.keacampers.com">
                                    <span class="kea"></span>
                                </a>
                            </li>
                            <li>
                                <a class="thllogos" href="http://www.maui-rentals.com">
                                    <span class="maui"></span>
                                </a>
                            </li>
                            <li>
                                <a class="thllogos" href="http://www.britz.com">
                                    <span class="britz"></span>
                                </a>
                            </li>
                            <li>
                                <a class="thllogos" href="http://www.unitedcampervans.com">
                                    <span class="united"></span>
                                </a>
                            </li>
                            <li>
                                <a class="thllogos" href="http://www.alphacampers.com">
                                    <span class="alpha"></span>
                                </a>
                            </li>
                            <li>
                                <a class="thllogos" href="http://www.mightycampers.com">
                                    <span class="mighty"></span>
                                </a>
                            </li>
                        </ul>
                        <span class="privacy">
                            <b>&copy; Tourism Holidings Limited</b><br />&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Privacy Policy</a>
                        </span>
                    </div>
                </div>
            </div>
            <!-- footer ends -->
        <%= DNAIMGTag %>
    </form>
    <script>
        $j(document).ready(function () {
            if (navigator.userAgent.indexOf("MSIE") < 0) {//todo: >9
                enquire.register("screen and (max-width:481px)", {
                    match: function () {
                        $j('.sections').find('.sectionContent').css('display', 'none'); ;
                        // Mobile Accordion 
                        $j('.sections .sectionHeader').click(function () {
                            //$j(this).find('.openclose i').toggleClass('icon-minus icon-plus');
                            var iconSign = $j(this).find('.openclose i');

                            if (iconSign.hasClass('icon-minus')){
                                iconSign.removeClass('icon-minus').addClass('icon-plus');
                            }else if (iconSign.hasClass('icon-plus')){
                                iconSign.removeClass('icon-plus').addClass('icon-minus');
                            }

                            $j(this).next().slideToggle();
                            return false;
                        });
                    },
                    unmatch: function () {
                        $j('.sections').find('.sectionContent').css('display', 'block');
                        $j('.sections').find('.openclose i').removeClass('icon-minus').addClass('icon-plus');
                        $j('.sections .sectionHeader').off('click');
                    }
                });
            }
        });				
	</script>
    <%= RemarketingCode %>
</body>
</html>
