﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using THL.Booking;
using System.Collections.Specialized;

public partial class RetrieveQuote : System.Web.UI.Page
{
    public string AuroraRentalStr;
    public string FeesView;//MVC Ready
    public string EHIView;//MVC Ready
    public string INSView;//MVC Ready
    public string FerryView, FerryJSON;//MVC Ready
    public string RequestStr;
    public string SearchAgainURLParams, SearchAgainURLParamsNoVehicle ;
    public string AnalyticsCode, SiteInfoJSON;
    public string ContentURL, AssetsURL;
    public string HTTPPrefix;
    public string ParentSiteURL, SSLSeal, VehicleName;
    public string ContactHeaderText, CurrencySelector, BrandTitle, VehicleContentLink;
    public string PickUpLocationStr, PickUpDateStr, DropOffLocationStr, DropOffDateStr, VehicleImageConvension, vehicleBrandStr, VehicleChargeContentURL, VehicleImagePath, VehicleInclusionPath;
    public string CountryCodeStr;

    public string debugHTML;


    public string DepositTxt;

    public AuroraQuote quote;

    ApplicationData appData;

    public decimal AdminFee = 0.0m;

    public string BrandChar { get; set; }
    public string CSSLink { get; set; }
    public bool HasFerryProduct { get; set; }
    public string RemarketingCode { get; set; }
    public string OpenTag { get; set; }
    /// <summary>
    /// Marin Tracking Code
    /// </summary>
    public string DNAIMGTag { get; set; }

    /// <summary>
    /// Support release version from config for forcing static assets cache refresh.
    /// </summary>
    public string Version
    {
        get { return ApplicationManager.ReleaseVersion; }
        set { value = ApplicationManager.ReleaseVersion; }
    }

    public string ContactUsURL
    {
        get
        {
            THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
            string contactUrl = appDomain.ParentSite + "/contactus";
            switch (appDomain.ParentSite)
            {
                case "keacampers.co.nz":
                    contactUrl = "http://nzrentals.keacampers.com/en/contact/contact_us.aspx";
                    break;
                case "keacampers.com.au":
                    contactUrl = "http://nzrentals.keacampers.com/en/contact/contact_us.aspx";
                    break;
            }

            return contactUrl;
        }
    }

    public string Currency
    {
        //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
        //get
        //{
        //    THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
        //    return CurrencyManager.GetCurrencyForCountryCode(appDomain.Country.ToString()).ToString();
        //}
        get;
        set;
    }

    protected void Page_Load(object sender, EventArgs e)
    {       
        //-- init from configure
        bool secureMode = false;//TODO: move to config manager,, managed globally
        bool.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["SecureMode"], out secureMode);
        HTTPPrefix = ApplicationManager.HTTPPrefix;
        THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
        if (appDomain == null)//dubug line
            appDomain = new THLDomain("secure.maui.co.nz", THLBrands.Maui, CountryCode.NZ, "B2CNZ", false, "uahere");//dubug line
        BrandChar = appDomain.BrandChar.ToString();
        ParentSiteURL = appDomain.ParentSite;
        TrackingDisplayer trd = new TrackingDisplayer(appDomain, secureMode);//DecideDNA Tracking Img
        
        //DNAIMGTag = trd.RenderDNATrackingImg("configure");
        DNAIMGTag += trd.RenderMarinTrackingPixelIMG(SessionManager.MarinTrackingParams, MarinStep.SELECTION, 0.0m, string.Empty, string.Empty);

        SSLSeal = trd.GetSealID();
        AnalyticsCode = trd.RenderAnalsCode();//Analytics Tracking Code
        RemarketingCode = trd.RenderRemarketingSnippets();

        ContentURL = HTTPPrefix + appDomain.Name + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["BookingControlContentPath"];
        string countryCode = appDomain.Country.ToString().ToUpper();  //(Request.Params["cc"] != null ? Request.Params["cc"] : "NZ").ToUpper();   
        AssetsURL = HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];
        //-- init from configure ends        
        if (IsPostBack && (Request.Params["bookingSubmitted"] == "1") || Request.Params["bookingSubmitted"] == "2")//Move to payment step
        {            
            if (SessionManager.CurrentQuote != null)//must have the quote in session to submit
            {
                SessionData sessionData = SessionManager.GetSessionData();
                quote = SessionManager.CurrentQuote;                 
                AvailabilityRequest aReq = quote.Request;
                AuroraBooking submitedData = new AuroraBooking(quote.GetCustomerData(), quote.SelectedExtraHireItems);
                NameValueCollection fDates = groupFerryDates(Request.Params);
                quote.ModifyQuote(Request.Params, Request.Params["paxCounter"], fDates);
                bool submittedDataSuccess = sessionData.UpdateBookingData(submitedData);
                Response.Redirect("QuotePayment.aspx");
            }
            else 
            {
                Response.Redirect("Selection.aspx?msg=sessionExpired");            
            }            
        }
        else
        {
            string rentalId = Request.Params["rentalId"];
            if (rentalId != null && rentalId != string.Empty)
            {
                quote = new DataAccess().GetQuoteForRentalId(rentalId);
                if (quote == null) Response.Redirect("Selection.aspx?msg=bookingCompleted");                
                CountryCodeStr = quote.Request.CountryCode.ToString();

                try
                {
                    //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
                    Currency = CurrencyManager.GetCurrencyForCountryCode(quote.Request.CountryCode).ToString();
                }
                catch (Exception)
                {
                }

                SessionManager.CurrentQuote = quote;
                SessionManager.BookingStatus = BookingStatus.Created;
                if (quote.Status != QuoteStatus.Retrieved)
                    Response.Redirect("ExpiredQuote.aspx");
                AuroraRentalStr = new UserDataDisplayer().RenderCustomerDetails(quote.GetCustomerData());
                THLBrands requestBrand = THLBrand.GetBrandForString(quote.Request.Brand);
                ContactHeaderText = ContentManager.GetMessageForCode(THLBrand.GetBrandForString(BrandChar), appDomain.Country, "ContactText").Content + "<br />"
                                + ContentManager.GetMessageForCode(THLBrand.GetBrandForString(BrandChar), appDomain.Country, "DomesticPhone").Content + " - "
                                + ContentManager.GetMessageForCode(THLBrand.GetBrandForString(BrandChar), appDomain.Country, "InternationalPhone").Content;
                VehicleImageConvension = quote.Request.CountryCode.ToString().ToLower() + AvailabilityHelper.GetAuroraVehicleStrForVehicleType(quote.Request.VehicleType) + appDomain.BrandChar.ToString() + "." + quote.Request.VehicleModel;

                //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
                //Move to B2C will need to used branded images.
                VehicleImagePath = AvailabilityHelper.GetVehicleImageBasePath(quote.BookingData.CountryCode, THLBrand.GetBrandForString(quote.Request.Brand), quote.Request.VehicleType, quote.Request.VehicleModel, AssetsURL, false);
                VehicleInclusionPath = AvailabilityHelper.GetVehicleImageBasePath(quote.BookingData.CountryCode, THLBrand.GetBrandForString(quote.Request.Brand), quote.Request.VehicleType, quote.Request.VehicleModel, AssetsURL, false);

                appData = ApplicationManager.GetApplicationData();
                VehicleName = quote.VehicleName;
                PickUpLocationStr = quote.PickUpLoc;// appData.GetLocationStringForCtrlCode(quote.Request.PickUpBranch);  //appData.GetAddressForLocationCode(aRequest.PickUpBranch);
                DropOffLocationStr = quote.DropOffLoc;// appData.GetLocationStringForCtrlCode(quote.Request.DropOffBranch); //appData.GetAddressForLocationCode(aRequest.DropOffBranch);
                PickUpDateStr = quote.Request.PickUpDate.ToString("dddd, dd MMMM yyyy HH:mm tt");
                DropOffDateStr = quote.Request.DropOffDate.ToString("dddd, dd MMMM yyyy HH:mm tt");
                VehicleType contentResourceVehicleType = quote.Request.VehicleType;
                VehicleChargeContentURL = ContentManager.GetLinkElementForBrand(requestBrand, contentResourceVehicleType, quote.Request.CountryCode, "VehicleCharges", quote.Request.PickUpDate, requestBrand).Content;
                SiteInfoJSON = "{'brand':'" + BrandChar + "'}";

                FeesView = VehicleDisplayer.RenderQuoteVehicleCharges(quote.EnforcedFees, VehicleChargeContentURL, quote.RentalInfoNode, false, quote);//TODO: last param is false, add day price discount support with rate bands
                EHIView += ExtraHireItemsDisplayer.RenderExtraHireQuotePanel(quote.AvailableExtraHireItems, quote.SelectedExtraHireItems, quote.Request);
                INSView += InsuranceProductDisplayer.RenderInsuranceQuotePanel(quote.AvailableInsurance, quote.SelectedInsurance, quote.Request, quote.InclusiveSelected);//TODO: pass inclusive status

                // Below code was commented by nimesh and added if else part on 17th Sep 2014 to display message on ferry products
                //FerryProductDisplayer fpd = new FerryProductDisplayer(quote.FerryProducts, quote.Request.PickUpDate, quote.Request.DropOffDate, quote.CurrencyCode);
                //FerryView = fpd.RenderFerryPanel();
                //FerryJSON = fpd.RenderJSON();

                //if (BrandChar.Equals("b") || BrandChar.Equals("m"))
                //{
                //    //FerryProduct[] ferryProducts = pm.GetFerryProducts(avpId);
                //    FerryProductDisplayerBM fpd = new FerryProductDisplayerBM(quote.FerryProducts, quote.Request.PickUpDate, quote.Request.DropOffDate, quote.CurrencyCode);
                //    FerryView = fpd.RenderFerryPanel();
                //    FerryJSON = fpd.RenderJSON();
                //}
                //else
                //{
                    //FerryProduct[] ferryProducts = pm.GetFerryProducts(avpId);
                    FerryProductDisplayer fpd = new FerryProductDisplayer(quote.FerryProducts, quote.Request.PickUpDate, quote.Request.DropOffDate, quote.CurrencyCode);
                    FerryView = fpd.RenderFerryPanel();
                    FerryJSON = fpd.RenderJSON();
                //}

                // End - Below code was commented by nimesh and added if else part on 17th Sep 2014 to display message on ferry products
                HasFerryProduct = !string.IsNullOrEmpty(FerryView);

                SearchAgainURLParams = quote.Request.GenerateSelectionParamsURL(true);
                SearchAgainURLParamsNoVehicle = quote.Request.GenerateSelectionParamsURL(false);

                //--
                    VehicleContentLink = ContentManager.GetVehicleLink(requestBrand, quote.Request.CountryCode, quote.Request.VehicleModel, requestBrand);
                    if (VehicleContentLink != null && VehicleContentLink.Length > 0) VehicleContentLink = VehicleContentLink.Replace("&amp;", "&");
                //--

                DepositTxt = quote.RequiredDepositPer < 100.0m ? "To confirm your quote a " + (quote.RequiredDepositPer > 0 ? quote.RequiredDepositPer + "% " : "")+"deposit will be required, If you wish to change your travel date the prices may differ from the original quote." : "To confirm your quote full vehicle charges will be required, If you wish to change your travel date the prices may differ from the original quote.";
            }
            else
            {
                Response.Write("no rental ID provided");
                Response.End();
            }
        }

        OpenTag = AvailabilityHelper.LoadOpenTag(appDomain);       
    }

    /// <summary>
    /// Filter out ferry data into readable string collection
    /// </summary>
    /// <param name="requestParams"></param>
    /// <returns></returns>
    NameValueCollection groupFerryDates(NameValueCollection requestParams)
    {        
        NameValueCollection ferryDates = new NameValueCollection();
        if (!String.IsNullOrEmpty(requestParams["wlg_pct_Adult"]) &&  !requestParams["wlg_pct_Adult"].Equals("0"))
            ferryDates["wlg"] = requestParams["crossingDate_wlg_pct"] + " " + requestParams["crossingTime_wlg_pct"];
        if (!String.IsNullOrEmpty(requestParams["pct_wlg_Adult"]) && !requestParams["pct_wlg_Adult"].Equals("0"))
            ferryDates["pct"] = requestParams["crossingDate_pct_wlg"] + " " + requestParams["crossingTime_pct_wlg"];
        return ferryDates;
    }

}