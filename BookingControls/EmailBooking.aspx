﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmailBooking.aspx.cs" Inherits="EmailBooking" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Request Vehicle</title>
    <link href="<%= AssetsURL %>/css/Base.css" type="text/css" rel="stylesheet" />    
    <link href="<%= AssetsURL %>/css/calendar/calendar.css"rel="stylesheet" type="text/css" media="all" />
    <link href="<%= AssetsURL %>/css/Base.css" type="text/css" rel="stylesheet" />
    <link href="<%= AssetsURL %>/css/<%= brandStr %>/selection.css" type="text/css" rel="stylesheet" />    
    <script type="text/javascript" src="<%= ContentURL %>js/jquery-1.3.1.min.js"></script>        
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.validate.js"></script>
    <style type="text/css">
        label { width: 10em; float: left; }
        label.error { float: none; color: red; padding-left: .5em; vertical-align: top; }
    </style>    
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.dimensions.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.tooltip.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/date.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/calendar/utils.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/calendar/calendar.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/calendar/calendar-setup.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/calendar/lang/calendar-en.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/hCtrl.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/selection.js"></script>
    <%= AnalyticsCode %>
</head>
<body>
    <form id="emailForm" onsubmit="return submitEmailForm()" action="">
        <script type="text/javascript">
            var ContentbaseUrl = '<%= ContentURL %>';
            var DNATagHTML = "<%= DNAIMGTag %>";
            var $j = jQuery.noConflict();
            jQuery(document).ready(function() { initEmailBookingPage(); });
        </script>
        <div id="paymentContainer">
            <div id="catalogHeader">
                <span class="Image">
                    <img src="<%= AssetsURL %>/images/<%= brandStr %>/logo.jpg" alt="logo" />
                </span>                
            </div>           
            <div id="vehicleCatalogContainer">
            <h2 id="titleHeader">Check Availability</h2>
            <div id="paymentPanel">
                <div id="infoPanel" class="FormSection">
                    <div id="sealPanel">
                        <!--<script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=<%= SSLSeal %>"></script>-->
                        <script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=<%= SSLSeal %>&amp;size=L&amp;lang=en"></script>
                    </div>
                    <p>The vehicle you have selected has limited availability for the dates you have chosen due to high demand, or rates for this period may not yet be available online.</p>
                    <p>As we cannot confirm this vehicles availability online immediately please complete the below form and one of our reservations consultants will contact you within 24 hours to advise if we can accommodate your request.</p>    
                    <p>In the instance where the vehicle you have selected is not available, our reservations consultants can advise alternative options that best suit your plans.</p>
                </div>
                <div id="personalDetailsForm" class="FormSection">
                    <big>Personal details:</big>
                    <ul>
                        <li>
                            <p id="CustomerTitleSection">
                                <label for="customerTitleDD">Title:*</label>
                                <select id="customerTitleDD" name="customerTitleDD">
                                    <option value="0">Please Select</option>
                                    <option value="Mr">Mr</option>
	                                <option value="Mrs">Mrs</option>
	                                <option value="Ms">Ms</option>
	                                <option value="Miss">Miss</option>
	                                <option value="Master">Master</option>
	                                <option value="Dr">Dr</option>
                                </select>
                            </p>
                        </li>
                        <li>
                            <p>
                                <label for="customerFirstName">First Name:*</label>
                                <input id="customerFirstName" name="customerFirstName" minlength="2" class="required" />
                            </p>
                        </li>
                        <li>
                            <p>
                                <label for="customerLastName">Last Name:*</label>
                                <input id="customerLastName" name="customerLastName" minlength="2" class="required" />
                            </p>
                        </li>                        
                        <li>
                            <p>
                                <label for="emailAddressTxt">Email address:*</label>
                                <input id="emailAddressTxt" name="emailAddressTxt" class="required email"/>
                            </p>
                        </li>
                        <li>
                            <p>
                                <label for="confirmEmailAddressTxt">Confirm email:*</label>
                                <input id="confirmEmailAddressTxt" name="confirmEmailAddressTxt" class="required email" />
                            </p>
                        </li>
                        <li>
                            <p>
                                <label for="telephoneTxt1">Telephone:*</label>
                                <span class="PhoneNumber">
                                    <span class="number">
                                        <small>country code</small>
                                        <input id="telephoneTxt1" name="telephoneTxt1" size="10" class="required number"/>
                                    </span>                                    
                                    <span class="number">
                                        <small>area code</small>
                                        <input id="telephoneTxt2" name="telephoneTxt2" size="10" class="required number" />
                                    </span>
                                    <span class="number">
                                        <small>number</small>
                                        <input id="telephoneTxt3" name="telephoneTxt3" size="10" class="required number"/>
                                    </span>                                    
                                </span>
                            </p>
                        </li>                                            
                        <li>
                            <p id="contactSelector">
                                <label for="customerNoteTxt">Contact me via:*</label>
                                <input type="radio" name="contactMethod" id="email" value="email" checked="checked" />
                                <label for="email" class="title">Email</label>
                                <input type="radio" name="contactMethod" id="phone" value="phone" />
                                <label for="phone" class="title">Telephone</label>
                            </p>
                        </li>                                             
                    </ul>              
                </div>
                <div id="rentalsDetailsForm" class="FormSection">
                    <big>Rental details:</big>
                    <%= RentalRequestSummary %>
                    <span>Please advise other dates or vehicles you may be interested in below:</span>
                    <textarea cols="50" rows="5" id="otherRequests" name="otherRequests" onblur="this.value = this.value.replace(/[^a-zA-Z 0-9 \n]+/g,'')"></textarea>
                    <input type="hidden" id="vehicleCode" name="vehicleCode" value="<%= VehicleCode %>" />
                </div>
                <div id="bottomPanel" class="FormSection">                    
                    <input type="hidden" name="submitted" id="submitted" value="0" />
                    <input type="submit" value="send" />
                    <a href="Selection.aspx" >Cancel</a>
                    <label id="globalFormError">&nbsp</label>
                </div>
            </div>
            <div id="completedMessage" class="Hide">                
                <big>Availability request sent</big>
                <span>Thank you for your enquiry. Our reservations team will be in touch with you within 24 hours with your options.</span>
                <span>Please quote <b id="generatedBookingId"></b> for reference.</span><img id="imgHolder" width='1' height='1' />
            </div> 
            </div>
             <div id="disclaimer">
                <span>Please note: Rental prices quoted above are only valid at the time of booking.</span>
            </div>  
        </div>            
    </form>    
</body>
</html>
