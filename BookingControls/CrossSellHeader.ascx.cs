﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace THL.Booking
{

    public partial class CrossSellHeader : System.Web.UI.UserControl
    {

        public string CrossSellCountry, CurrentCountry;
        public string CrossSuffix;
        public string ReverseSuffix;
        public string ReverseCountryName;
        public THLBrands Brand;

        protected void Page_Load(object sender, EventArgs e)
        {
            switch (CrossSellCountry.ToUpper())
            { 
                case "NZ":
                    CrossSuffix = "co.nz";
                    ReverseSuffix = "com.au";
                    ReverseCountryName = "Australia";
                    CurrentCountry = "New Zealand";
                    break;
                case "AU":
                    CrossSuffix = "com.au";
                    ReverseSuffix = "co.nz";
                    ReverseCountryName = "New Zealand";
                    CurrentCountry = "Australia";
                    break;            
            }             
        }

        public bool IsCurrentBrand(string brandString)
        {
            THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
            return appDomain.Brand.Equals(THLBrand.GetBrandForString(brandString));
        }

    }
}