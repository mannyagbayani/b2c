﻿var ctrl = null;
var showSpinner = null;

Date.compareStrDate = function (dateStr1, dateStr2) {//dd-mm-yyyy 
    var dateParams1 = dateStr1.split('-');
    var dateParams2 = dateStr2.split('-');
    var date1 = Date.parse(dateParams1[2] + "-" + dateParams1[1] + "-" + dateParams1[0]);
    var date2 = Date.parse(dateParams2[2] + "-" + dateParams2[1] + "-" + dateParams2[0]);
    if (date1 > date2) return 1;
    else if (date1 < date2) return 2;
    return 0; //return 1 for first larger,2 for second and 0 for equal..    
};

/* B2C Controller */
function B2CController(params) {
    //var sParam = 
    //if(!sParam)
    this.params = params; //local storage point
    this.setParam('na', 1); //defaults point    
    this.setQueryParams();     
}

B2CController.prototype.toString = function () { 
    var results = "params:";
    for (var key in this.params) {
        var obj = this.params[key];
        results +=  key + " = " + obj + ",";
    }
    return results;
}

B2CController.prototype.store = function () {
    var ser = JSON.stringify(this.params);
    //$.jStorage.set("thlb2c", ser, { TTL: 3600000 });//period?
}

B2CController.prototype.restore = function () {
    var val = "";// $.jStorage.get("thlb2c", "");
    if (val != null && val != "")
        return val;
}

B2CController.prototype.setCtrlData = function (data) {
    this.params.data = data;
}

B2CController.prototype.setParam = function (name, val, uiElm) {
    for (var key in this.params) {
        if (key == name) {
            this.params[key] = val;
            if (typeof uiElm != 'undefined' && uiElm != null) {
                var elm = $('#' + uiElm);
                elm.val(val);
                elm.trigger('change');
            }
            this.store();
            return;
        }
    }
    this.params[name] = val;
    this.store();    
}

B2CController.prototype.getB2CUrl = function () {

    var targetUrl = this.params.engine + "?cc=" + this.params.cc + "&brand=" + this.params.brand + "&ac=" + this.params.ac + "&sc=" + (this.params.vt == "car" ? "ac" : "rv") + "&vtype=" + (this.params.vt == "car" ? "ac" : "rv") + "&pc=" + this.params.pc + "&na=" + this.params.na + "&nc=" + this.params.nc + "&cr=" + this.params.cr + "&pb=" + this.params.pb + "&pd=" + this.params.pd + "&pm=" + this.params.pm + "&py=" + this.params.py + "&pt=" + this.params.pt + "&db=" + this.params.db + "&dd=" + this.params.dd + "&dm=" + this.params.dm + "&dy=" + this.params.dy + "&dt=" + this.params.dt + "&vh=" + this.params.vh + "&pv=1.0" + "&promoCode=" + this.params.promocode;
    return targetUrl;
}

B2CController.prototype.validate = function () {//return array of missing UIs
    var missingParams = [];
    var required = [{ 'data': 'pb', 'ui': 'pickUpLoc' }, { 'data': 'db', 'ui': 'dropOffLoc' }, { 'data': 'pd', 'ui': 'pickUpDate' }, { 'data': 'dd', 'ui': 'dropOffDate'}];
    switch (this.params.brand) {//brand specific validation
        case 'b':
            required.push({ 'data': 'cr', 'ui': 'countrOfRes' });
        case 'm':
            required.push({ 'data': 'cr', 'ui': 'countrOfRes' });
    }
    for (var i = 0; i < required.length; i++) {
        if (typeof this.params[required[i].data] == 'undefined' || this.params[required[i].data] == '' || this.params[required[i].data] == 0)
            missingParams.push(required[i].ui)
    }
    return missingParams; //returns the missing UI ids.
}

B2CController.prototype.setQueryParams = function () {
    if (document.location.href.indexOf('?') > 0) {
        var params = {}, queries, temp, i, l;
        queryString = document.location.href.split('?')[1];
        queries = queryString.split("&");
        for (i = 0, l = queries.length; i < l; i++) {
            temp = queries[i].split('=');
            params[temp[0]] = temp[1];
        }
        this.setParam('pc', typeof params["pc"] != 'undefined' ? params["pc"] : "");
        this.setParam('ac', typeof params["ac"] != 'undefined' ? params["ac"] : ""); //biz point        
    }
    else {//set blank
        this.setParam('pc', '');
        this.setParam('ac', '');
    }
}

//experimental
B2CController.prototype.getAvailability = function () {
    //TODO: get params and construct new query url
    ///availability/nz/britz/akl/1000-05-09-2013/akl/1500-12-09-2013/na-1-nc-0-ac-MACCOM
    //jp the server
    //iterate on results and return num of avail..
}


function initWidget(params) {
    ctrl = new B2CController(params);
    //var initParams = params;     
    var bElm = $(params.target);
    pdDatePicker = $('#pickUpDate').datepicker({
        dateFormat: 'dd-mm-yy',
        numberOfMonths: 2,
        changeMonth: true,
        changeYear: false,
        minDate: new Date(),
        onSelect: function (dateText, inst) {
            var month = parseInt(inst.selectedMonth, 10) + 1;
            ctrl.setParam('pd', (inst.selectedDay < 10 ? "0" : "") + inst.selectedDay);
            ctrl.setParam('pm', (month < 10 ? "0" : "") + month);
            ctrl.setParam('py', inst.selectedYear);
            setRequired();
        },
        onChangeMonthYear: function (year, month, inst) {
            //changeMonthYear($(this), year, month, inst, $('#pickUpDate'));
        },
        onClose: function (dateText, inst) {
            var pickParams = dateText.split('-');
            var dropValParams = $('#dropOffDate').val().split('-');

            if (dropValParams.length != 3) $('#dropOffDate').val(dateText);
            if (Date.compareStrDate($("#pickUpDate").val(), $("#dropOffDate").val()) == 1) {
                $("#dropOffDate").val($("#pickUpDate").val());
                var drop = $("#dropOffDate").datepicker('getDate');
                ctrl.setParam('dd', drop.getDate())
                ctrl.setParam('dm', (drop.getMonth() < 10 ? "0" : "") + (drop.getMonth() + 1));
                ctrl.setParam('dy', drop.getFullYear());
            }
            
        }
    });
    ddDatePicker = $("#dropOffDate").datepicker({
        beforeShowDay: function (date) {
            var pickDate = new Date();
            if (ctrl.params.pd > 0)
                pickDate = new Date(ctrl.params.py, ctrl.params.pm - 1, ctrl.params.pd);
            var selected = (pickDate < date);
            return [selected, ''];
        },
        dateFormat: 'dd-mm-yy',
        numberOfMonths: 2,
        minDate: new Date(),
        changeMonth: true,
        changeYear: false,
        onSelect: function (dateText, inst) {
            var month = parseInt(inst.selectedMonth, 10) + 1;
            //setDateFromDatePicker($('#dropOffDate'), dateText, inst);
            ctrl.setParam('dd', (inst.selectedDay < 10 ? "0" : "") + inst.selectedDay);
            ctrl.setParam('dm', (month < 10 ? "0" : "") + month);
            ctrl.setParam('dy', inst.selectedYear);
            setRequired();
        },
        onChangeMonthYear: function (year, month, inst) {
            //changeMonthYear($(this), year, month, inst, $('#dropOffDate'));
        }
    });

    //bind ddowns
    $('.bookingControlForm select').change(function (e) {
        var elm = $(this);
        var paramName = elm.attr('data-b2c');
        var val = elm.val();
        ctrl.setParam(paramName, val);
        switch (elm.attr('id')) {//dd specifics, set dropoff
            case 'pickUpLoc':
                $('#dropOffLoc :nth-child(1)').prop('value', val);
                ctrl.setParam('db', val);
                break;
        }        
        if (typeof e.isTrigger == 'undefined' || !e.isTrigger)
            setRequired();
    });
    
    //vehicle type
    $('.vehicleTab').click(function (e) {
        $('.vehicleTab').removeClass('TabSelected');
        ctrl.setParam('vt', $(this).attr('data-vtype'));
        $(this).addClass('TabSelected');
        loadCtrl(ctrl.params.data);
        setRequired();
    });

    $('.StartSearchBtn').click(function (e) {
        e.preventDefault();
        var required = ctrl.validate();
        if (required.length > 0) {
            setRequired();
            $('.bookingControlForm .requiredBox').css('display', 'block');
        } else {
            $('.bookingControlForm .requiredBox').css('display', 'none');
            showSpinner(true);
            $('.StartSearchBtn .lbl').text('Searching..');
            window.location = ctrl.getB2CUrl();
        }        
    });

    //local storage point    
    $.ajax({
        //url: 'http://staging.britz.com.au/SiteCollectionDocuments/js/booking/testdata.js', //debug line
        url: '/GenericWidget.aspx?service=json&cb=loadCtrl',//TODO: from incoming params
        dataType: 'jsonp'
    });
    var options = { height: 24, imgUrl: "http://staging.britz.com.au/SiteCollectionImages/booking/slow-loader.png", btnClass: "StartSearchBtn", spinnerClass: "spinner" }
    bindSpinner(options);    
}

function setRequired() {
    $('.bookingControlForm select, .bookingControlForm input').removeClass('required');
    var req = ctrl.validate();
    for(var i=0; i< req.length; i++)
        $('#' + req[i]).addClass('required');
    return req.length;    
}

function loadCtrl(data) {
    ctrl.setCtrlData(data)
    //grab time from ui
    var selects = $('.bookingControlForm select.time');
    for(var i=0; i < selects.length; i++) {
        var elm = $(selects[i]);
        var paramName = elm.attr('data-b2c');
        ctrl.params[paramName] = elm.val();
    };
    ctrldata = ctrl.params.data;
    var branches = [];
    clearLocSelect(["pickUpLoc", "dropOffLoc", "vehicleModel"]);
    for (var i = 0; i < ctrldata.branches.length; i++) {
        var branch = ctrldata.branches[i]
        if (branch.vt == ctrl.params.vt && branch.cc == ctrl.params.cc && branch.brands.indexOf(ctrl.params.brand) > -1) {
            $("#pickUpLoc, #dropOffLoc").append("<option value=\"" + branch.code + "\">" + branch.name + "</option>");
            branches.push(branch);
        }
    }
    //load vehicles
    var vehicles = [];
    for (var i = 0; i < ctrldata.vehicles.length; i++) {
        var vehicle = ctrldata.vehicles[i];
        if (vehicle.vt == ctrl.params.vt && vehicle.brand == ctrl.params.brand && vehicle.cc == ctrl.params.cc) {
            $("#vehicleModel").append("<option value=\"" + vehicle.code + "\">" + vehicle.name + "</option>");
            vehicles.push(branch);
        }
    }
    syncUI(ctrl.params);                       
}

function clearLocSelect(elmIds) {
    for(var i=0; i < elmIds.length; i++)
        $("#" + elmIds[i] + " > option").each(function (i, el) {
            if (i == 0) return;
            $(el).remove();
    });    
}

function syncUI(params) {//restore the view state based on params, ui specific
    $('#pickUpLoc').val(params.pb);
    $('#dropOffLoc').val(params.db);
    //$('#vehicleModel').val(params.vt);
    $('#countrOfRes').val(params.cr);
    $('#pickUpDate').val((params.pd > 0) ? params.pd + '-' + params.pm + '-' + params.py : "Choose what date?");
    $('#dropOffDate').val((params.dd > 0) ? params.dd + '-' + params.dm + '-' + params.dy : "Choose what date?");
    $('#numberAdults').val(params.na);
    $('#pickUpTime').val(params.pt);
    $('#dropOffTime').val(params.dt);
    $('#numberChildren').val(params.nc);
    $('.bookingControlForm select').trigger('change');
}

function changeMonthYear(calendar, year, month, inst, hiddenObj) {
    var oldDate = calendar.datepicker("getDate");
    if (oldDate != null) {
        var newDate = new Date(year, month - 1, oldDate.getDate());
        if (newDate.getDate() != oldDate.getDate()) {
            var lastDay = 32 - new Date(year, month - 1, 32).getDate();
            newDate = new Date(year, month - 1, lastDay);
        }
        calendar.datepicker("setDate", newDate);        
    }
}


function setDateFromDatePicker(hiddenObj, dateText, inst) {
    $(hiddenObj).val(dateText);
    setDate(inst);
}

function setDate(inst) {
    var toDate = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
    var nextDay = new Date(toDate.getTime() + 1 * 86400000);
    $('#dropOffDate').datepicker("option", "minDate", nextDay);
}

function bindSpinner(options) {
    btn = $('.StartSearchBtn');
    spinner = $('.spinner');
    var spinnerButtons = $('.StartSearchBtn');
    var loadingTimer;
    var loadingFrame = 0;
    var animateLoading = function () {
        if (!spinner.is(':visible') && (spinner.css("display") != "inline")) {
            clearInterval(loadingTimer);
            return;
        }
        var imgHeight = 24;
        var frameCount = 8;
        spinner.css('background-image', "url(" + options.imgUrl + ")");
        spinner.css('background-position', "0 " + (loadingFrame * -imgHeight) + "px");
        spinner.css('width', imgHeight);
        loadingFrame = (loadingFrame + 1) % frameCount;
    };
    showSpinner = function (show) {
        clearInterval(loadingTimer);
        if (show)
            spinner.show();
        else
            spinner.hide();
        loadingTimer = setInterval(animateLoading, 50);
    };    
}