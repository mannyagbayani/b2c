﻿$(document).ready(function () {
    //rev:mia may 13, 2014 - injection of code to display Gross  and NET AMOUNT 
    //THL history: last day of niven  
    //1. set the default option (gross or net)
    var query = getUrlVars();
    var grDefOn = query['grDefOn'];
    var grossNetOpn = query['grossNetOpn'];
    var gno = query['gno'];  //has gross net option
    var curJson = query['json']; // contain the currency list
    var cty = query['cty']; // travel destination
    var cur = query['cursel']; //default currency
    var isGross;//     = query['IsGross'];
    //debugger;

    var curJsonObject = [];
    try {
        curJsonObject = JSON.parse(decodeURIComponent(curJson));
        isGross = query['IsGross'];
        //alert("isgross: " + isGross);
    }
    catch (ex) {
        alert(ex);
    }

    if (grossNetOpn != gno) grossNetOpn = gno;

    var HasCurrencyOption = query['hso']; //has currency option
    var HasGrossNetOptions = query['hgo']; //has gross option
    var displayOption = false;


    if (HasCurrencyOption == 'false')
        $("#spancurrencySelectorb2b").hide();
    else {
        $("#spancurrencySelectorb2b").show();

        if (curJsonObject.length - 1 != -1) {

            var ctyDesc = $("#currencySelector option[value='" + cty + "']").html();

            $('#currencySelectorb2b').append($('<option>', {
                rel: 1.0,
                value: cty,
            }).text(ctyDesc));
            //insert default country if travel
            
            $.each(curJsonObject, function (index, item) {
                //if country is equsl to FromCode then aadd
                if (cty == item.fc && item.tc != cty)  {
                    ctyDesc = $("#currencySelector option[value='" + item.tc + "']").html();
                    
                    $('#currencySelectorb2b').append($('<option>', {
                        rel: item.rt,
                        val: item.tc,
                        selected: (cur == item.tc)
                    }).text(ctyDesc));
                }


            });
           // $('#currencySelectorb2b').trigger("change");
        }

    }

   // $("#spanPriceHighlight").text((HasGrossNetOptions == 'true' ? "" : "Price highlighted below is NETT only"));
    if (HasGrossNetOptions == 'true') {
        $(".spanRadioAvailability").show();
        $(".lblDisplay").show();
        displayOption = true;
    }
    else {
        $(".spanRadioAvailability").hide();
        $(".lblDisplay").hide();
    }

    //alert("displayOption: " + displayOption);
    if (displayOption) {

        try {
            if (isGross != undefined) {
                grDefOn = isGross.toLowerCase();
            }

        }
        catch (ex) {
            alert(ex)
        }

        if (grossNetOpn == "G,N" && grDefOn == 'true') {
            //alert(1)
            $("#gnGross").prop('checked', true);
            $("#gnNett").prop('checked', false);
        }
        else if (grossNetOpn == "G,N" && grDefOn == 'false') {
            //alert(2)
            $("#gnGross").prop('checked', false);
            $("#gnNett").prop('checked', true);
        }
        else if (grossNetOpn == "G" && grDefOn == 'true') {
            //alert(3)
            $("#gnGross").prop('checked', true).attr('disabled', 'disabled')
            $("#gnNett").prop('checked', false).attr('disabled', 'disabled')
        }
        else if (grossNetOpn == "N" && grDefOn == 'true') {
            //alert(4)
            $("#gnNett").prop('checked', true).attr('disabled', 'disabled')
            $("#gnGross").prop('checked', false).attr('disabled', 'disabled');
        }
        else if (( grossNetOpn == "N" || grossNetOpn == "") && grDefOn == 'false') {
            //alert(5)
            $("#gnNett").prop('checked', true).attr('disabled', 'disabled')
            $("#gnGross").prop('checked', false).attr('disabled', 'disabled');
        }
        else{
        }


    }
    else {
        //alert(6)
        $("#gnGross").prop('checked', false);
        $("#gnNett").prop('checked', false);
        $("#spanPriceHighlight").hide();


    }
    
});

