﻿/* cal base starts */
var startDate;
var endDate;
var ONEDAY = 3600 * 24 * 1000;

function compareDates() {
    var pickupdateStr = $j('#ctrlSearchBox_calPickUp_DateText').val();
    var dropoffDateStr = $j('#ctrlSearchBox_calDropOff_DateText').val();
    if (!validatePeriod(pickupdateStr, dropoffDateStr)) return false;
    else return true;
}

function filterDates1(cal) {
    var date = cal.date;
    startDate = date.getTime();
    document.getElementById('ctrlSearchBox_dropPickUpTime').focus();

    if (!compareDates())//dropoffDate is after pickupdate, set dropoffDate to pickup 
        document.getElementById('ctrlSearchBox_calDropOff_DateText').value = document.getElementById('ctrlSearchBox_calPickUp_DateText').value;

    $j('#ctrlSearchBox_calPickUp_DateText').removeClass('required');
    $j("div.requiredMsg").remove();
}
function filterDates2(cal) {
    var date = cal.date;
    endDate = date.getTime();
    document.getElementById('ctrlSearchBox_dropDropOffTime').focus();
    if (document.getElementById('ctrlSearchBox_calDropOff_DateText').value == 'dd/mm/yyyy')
        document.getElementById('ctrlSearchBox_calDropOff_DateText').value = document.getElementById('ctrlSearchBox_calPickUp_DateText').value;
    $j('#ctrlSearchBox_calDropOff_DateText').removeClass('required');
    $j("div.requiredMsg").remove();
}

function disallowDateBefore(date) {
    date = date.getTime();
    var MINHIRE;
    MINHIRE = ONEDAY * -1;
    if ((startDate != null) && (date <= (startDate + MINHIRE))) {
        return true;
    }
    var now = new Date().getTime() + ONEDAY;
    if (date <= (now + MINHIRE)) {
        return true;
    }
    return false;
}

function disallowDateAfter(date) {
    date = date.getTime();
    var now = new Date().getTime();
    if (date < (now)) {
        return true;
    }
    return false;
}
/* cal base ends */


function beforeShowDayFunc(date) {
    var selected = ($j("#ctrlSearchBox_calPickUp_DateText").datepicker('getDate') < date);
    return [selected, ''];
}


function initControl() {
    $j("#bookingControl .TimeSpan input").focus(function () {
        $j(this).select();
    });

    $j("#ctrlSearchBox_calDropOff_DateText").datepicker({
        dateFormat: 'dd-M-yy',
        beforeShowDay: beforeShowDayFunc,
        numberOfMonths: 2,
        minDate: new Date(),
        onClose: function (dateText, inst) {

        }

    });

    $j("#ctrlSearchBox_calPickUp_DateText").datepicker({
        dateFormat: 'dd-M-yy',
        minDate: new Date(),
        numberOfMonths: 2,
        onClose: function (dateText, inst) {
            if (Date.parse($j("#ctrlSearchBox_calDropOff_DateText").val()) < Date.parse($j("#ctrlSearchBox_calPickUp_DateText").val())) {
                //alert($j("#ctrlSearchBox_calDropOff_DateText").val() < $j("#ctrlSearchBox_calPickUp_DateText").val());
                $j("#ctrlSearchBox_calDropOff_DateText").val($j("#ctrlSearchBox_calPickUp_DateText").val());
            }
        }
    });
    /* J Cal End */

    if (bookingControlParams.vc != null && bookingControlParams.vc != '')//set vcode
        $j("#ctrlSearchBox_dropVehicleType").val(bookingControlParams.vc);
    if ($j('#vehicleType').val() == 'ac' || $j('#vehicleType').val() == 'av')//set vcode
        $j("#" + $j('#vehicleType').val() + "Radio").attr('checked', 'checked');

    //bind events
    $j("#bookingControl input[name='vt']").click(function () {//bind expand

        $j('#vehicleType').val($j(this).val());

        $j('#radiosSwapped').val('true');

        submitControl("_self", "post");
    });

    $j("#countryCode").change(function () {//bind expand
        submitControl("_self", "post");
    });

    $j("#ctrlSearchBox_dropCountryOfResidence").change(function () {//bind expand
        $j('#countryOfResidence').val($j(this).val());
    });

    $j("select.LocationDD").change(function () {//bind locations
        if ($j(this).attr('id') == "ctrlSearchBox_dropPickUpLocation")
            $j("#pickUpLoc").attr('value', $j(this).val());
        else
            $j("#dropOffLoc").attr('value', $j(this).val());
    });

    $j("#ctrlSearchBox_dropVehicleType").change(function () {//bind vehicle code
        $j("#vehicleCode").attr('value', $j(this).val());
        $j("#ctrlSearchBox_dropAdults option:first-child").attr('selected', 'selected');
        $j("#ctrlSearchBox_dropChildren option:first-child").attr('selected', 'selected');
    });
    $j("#ctrlSearchBox_dropVehicleType").val($j("#vehicleCode").val());

    $j("#bookingControl input").change(function () {//bind change
        $j("#bookingControl input, #bookingControl select").removeClass("required");
        $j("#ctrlValidationMsg").removeClass("required");
    });

    $j("#bookingControl select").change(function () {//bind change
        $j("#bookingControl input, #bookingControl select").removeClass("required");
        $j("#ctrlValidationMsg").removeClass("required");
    });

    if ($j("#bookingControl").hasClass('SplashMode') && ($j('#countryCode').val() == '')) {//init splash mode
        $j('#bookingControl input, #bookingControl select').attr('disabled', 'disabled');
        $j('#countryCode').attr('disabled', '');
    }

    setCrossSellBookingControl();

    setCountryAvailability();

    if (requestParams != null)
        updateHorizontalCtrl(requestParams);

    /*if (ctrlParams.pt == '00:00' && ctrlParams.dt == '00:00' && requestParams.pt == '00:00' && requestParams.dt == '00:00') {
    $j("#ctrlSearchBox_dropPickUpTime").attr('value', '10:00');
    $j("#ctrlSearchBox_dropDropOffTime").attr('value', '15:00');
    }*/

    var cvtype = $j("#bookingControl #avRadio").attr('checked');
    if (!cvtype || (typeof affCode != 'undefined' && affCode != '')) {
        $j('#bookingControl').addClass('cars');
    }

    $j("#bookingControl .Radios input[type='radio']").click(function () {
        if ($j(this).val() == 'ac') {
            $j('#bookingControl').addClass('cars');
        }
        else
            $j('#bookingControl').removeClass('cars');
    });

}

function setCrossSellBookingControl($ctrl) {
    $j('.CSellOpt').change(function () {
        if ($j(this).val() == 'all')
            $j('.crossSellBrand').addClass('all')
        else
            $j('.crossSellBrand').removeClass('all')
    });
    $j('.CSellOpt').trigger('change');
}
function submitControl(targetUrl, method) {
    var cTarget = (targetUrl == "_self" ? "" : $j("#bookingControl #controlTarget").val());

    //cross sell
    var targetRequest = $j("#bookingControl .CSellOpt option:selected").val() == "all" ? "mac" : "current";
    var targetType = $j("#bookingControl #avRadio").attr('checked') ? "campervans" : "cars";

    if (targetRequest == "mac" && targetType == "campervans") {
        var params = getParams();
        params = params.replace('vh=', 'cvh=');
        params = params.replace('brand=', 'cbrand='); //.replace('vc=', 'brand=&cvc=');

        if (affCode != "")//pass ac to aggr
            params = params.replace('ac=', 'ac=' + affCode);

        cTarget = crossSellUrl + "?" + params + "&csell=1&SID=" + psid + "&";
        var spinnerSrc = assetsBase + '/images/' + brandId + '/icons/ajax-loader.gif';
        $j('body').css('background', 'none');
        $j('#thlCrossSellBanner').css('display', 'none');
        lightWeightLoader($j('#vehicleSelectionContainer'), "<div id='preLoaderPanel' class='CSell_" + (document.domain.indexOf('.nz') > 0 ? "NZ" : "AU") + "'><span><img src='" + spinnerSrc + "' alt='spinner'/></span></div>", cTarget);
        setTimeout(function () { window.location = cTarget; }, 1000);
    }
    //cross sell 

    var brandChar = $j("#bookingControl #controlBrand").val();
    if (method == "post") {
        document.forms[0].action = cTarget;
        document.forms[0].submit();
    } else if (method == "get") {
        var urlParams = getParams();
        if (urlParams == null || urlParams == '') return;

        if ($j('#controlTarget').val() == '') {
            document.forms[0].submit(); //submit to self on to target    
            return;
        }
        var url = cTarget + "?" + urlParams;
        window.location = url;
    }
}

function lightWeightLoader($containerEle, preloaderHtml, redirectURL) {
    $j('body').html(preloaderHtml)
    //setTimeout(function () { window.location = redirectURL; }, 1000);
}
function getParams() {
    var responseText = '';
    var pickupdateStr = $j('#ctrlSearchBox_calPickUp_DateText').val();
    var dropoffDateStr = $j('#ctrlSearchBox_calDropOff_DateText').val();
    if (!validatePeriod(pickupdateStr, dropoffDateStr)) {//failed onblur Date Range validation
        responseText = 'valid period';
        $j('#ctrlSearchBox_calPickUp_DateText').addClass("required");
        $j('#ctrlSearchBox_calDropOff_DateText').addClass("required");
    }
    var pickupdate = Date.parseExact(pickupdateStr, "dd-MMM-yyyy");
    var dropoffDate = Date.parseExact(dropoffDateStr, "dd-MMM-yyyy");
    var nc = $j("#ctrlSearchBox_dropChildren option:selected").val();
    var na = $j("#ctrlSearchBox_dropAdults option:selected").val();
    if (parseInt(na) < 1) {
        na = 1;
    }

    cr = $j("#ctrlSearchBox_dropCountryOfResidence option:selected").val();

    var brandId = $j('#controlBrand').val();

    if (cr == "NaN") cr = 0;

    cc = $j("#countryCode option:selected").val();

    if (parseInt(cr) == 0 && (brandId == 'u' || brandId == 'a' || brandId == 'e' || brandId == 'b' || brandId == 'p' || brandId == 'y' || (brandId == 'm' && cc == 'NZ' && $j("#vehicleType").val() == 'ac'))) { //todo: cleaup.
        responseText += " country of residence,";
        $j('#ctrlSearchBox_dropCountryOfResidence').addClass("required");
    }
    
    if (cc == '-1' || cc == '') {
        cc = "";
        $j("#countryCode").addClass("required");
    }
    if (responseText != '') {
        $j("#ctrlValidationMsg").addClass("required");
        if ($j('.spinner') != null) $j('.spinner').hide();
        return '';
    }
    vh = $j('#ctrlSearchBox_dropVehicleType').val();
    vh = (vh == -1 || vh == null ? "" : vh);

    vc = $j("#ctrlSearchBox_dropVendor").val();
    vc = getBPAEForAURCodes('brand', vc)

    ac = "";
    sc = ($j("#vehicleType").val() == 'ac' ? 'car' : 'rv');
    pc = "";
    ch = "";
    rf = ""; //$j('#agentCodeField').val();
    if (rf.length > 9) rf = "";
    pb = $j("#ctrlSearchBox_dropPickUpLocation option:selected").val();
    pd = pickupdate.getDate();
    pm = pickupdate.getMonth() + 1;
    py = pickupdate.getFullYear();
    pt = $j("#ctrlSearchBox_dropPickUpTime option:selected").val();
    db = $j("#ctrlSearchBox_dropDropOffLocation option:selected").val();
    dd = dropoffDate.getDate();
    dm = dropoffDate.getMonth() + 1;
    dy = dropoffDate.getFullYear();
    dt = $j("#ctrlSearchBox_dropDropOffTime option:selected").val(); ;
    vt = ($j("#vehicleType").val() == 'ac' ? 'car' : 'rv'); //legacy param

    pv = "1.0"; //param version(BPAE)

    var getvalues = '';
    if (responseText == '') {
        getvalues = "cc=" + cc + "&vc=" + vc + "&ac=" + ac + "&sc=" + sc + "&pc=" + pc + "&ch=" + ch + "&rf=" + rf + "&na=" + na + "&nc=" + nc + "&cr=" + cr + "&pb=" + pb + "&pd=" + pd + "&pm=" + pm + "&py=" + py + "&pt=" + pt + "&db=" + db + "&dd=" + dd + "&dm=" + dm + "&dy=" + dy + "&dt=" + dt + "&vtype=" + vt + "&vh=" + vh + "&pv=" + pv + "&brand=" + brandId;
        return getvalues;
    } else {
        var trimmed = responseText.replace(/,+$/, "");
        if($j('.spinner') != null) $j('.spinner').hide();
        return '';
    }
}

function validatePeriod(pickupdateStr, dropoffDateStr) {
    var pickUpDate = Date.parseExact(pickupdateStr, "dd-MMM-yyyy");
    var dropOffDate = Date.parseExact(dropoffDateStr, "dd-MMM-yyyy");
    if (pickupdateStr == null || dropoffDateStr == null || pickupdateStr == '' || dropoffDateStr == '' || pickupdateStr == 'dd-mmm-yyyy' || dropoffDateStr == 'dd-mmm-yyyy' || Date.parseExact(pickupdateStr, "dd-MMM-yyyy") == null || Date.parseExact(dropoffDateStr, "dd-MMM-yyyy") == null)
        return false;
    else {//range validation
        if (dropOffDate.compareTo(pickUpDate) >= 0)
            return true
        else
            return false;
    }
}

function setSelectedVehicle(vehicleCode, vehicleName) {
    $j('#bookingControl').removeClass('Aggregator');
    if ($j("#ctrlSearchBox_dropVehicleType").containsOption(vehicleCode)) {
        $j("#ctrlSearchBox_dropVehicleType").val(vehicleCode);
    } else {
        $j('#ctrlSearchBox_dropVehicleType').append("<option selected='selected' value=" + vehicleCode + ">" + vehicleName + "</option>");
        $j('#bookingControl').addClass('VehicleMode');
    }
}

function setCountryAvailability() {
    availabilityType = $j('#contryVehicleAvailability').val();
    if (availabilityType != "Both") {
        $j("span.Radios").css('visibility', 'hidden');
        $j(".CSell span.Radios").css('visibility', 'visible');
    }
}

//temporary mapper
function getBPAEForAURCodes(codeType, aurCode) {
    var bpaeCode = '';
    switch (aurCode) {
        case 'b':
            bpaeCode = "bz";
            break;
        case 'p':
            bpaeCode = "bk";
            break;
        case 'm':
            bpaeCode = "ma";
            break;
        case 'x':
            bpaeCode = "kx";
            break;
        case 'b':
            bpaeCode = "bz";
            break;
        case 'ap':
            bpaeCode = "ap";
            break;
        case 'cc':
            bpaeCode = "cc";
            break;
        case 'kc':
            bpaeCode = "kc";
            break;
        case 'ke':
            bpaeCode = "ke";
            break;
        case 'eg':
            bpaeCode = "ec";
            break;
        case 'ju':
            bpaeCode = "ju";
            break;
    }
    return bpaeCode;
}