﻿function initWidgetCreator(env) {
    $('#testCode').click(function (ev) {
        updateWidget();
        ev.preventDefault();
    });

    if (!IsIE8()) {
        $("#copyToCB").zclip({
            path: "/js/ZeroClipboard.swf",
            copy: function () { return $('#widgetSnippet').val(); }
        });
    } else
        $("#copyToCB").click(function () { alert("sorry, only supported on latest browsers");  });  ;


    $('#configureForm select, #configureForm input, #controlCodeArea textarea').change(function () {
        var elm = $(this);
        var options = getWdgtOptions();
        var eId = elm.attr('id');
        switch (eId) {
            case "countrySelector":
                options.country = elm.val();
                break;
            case "vehicleSelector":
                options.vehicleType = elm.val();
                break;
            case "brandSelector":
                options.brand = elm.val();
                break;
            case "affId":
                options.affId = elm.val();
                break;
            case "utm_campaign":
                options.utm_campaign = elm.val();
                break;
                // Added by Nimesh on 29th sep 2014
            //case "RemoveCrossLink":
            //    options.removeCrossLink = elm.is(':checked');
            //    break;
        }

        var brand = $('#brandSelector').val();
        var country = $('#countrySelector').val();

        /*Added by Nimesh on 29th Sep 2014 to handle cross link check box*/
        
        if (brand === "") {
            $('#RemoveCrossLink').attr("disabled", false);
            $('#utm_campaign').attr("disabled", !($('#RemoveCrossLink').is(':checked')));
            options.removeCrossLink = $('#RemoveCrossLink').is(':checked');
        }
        else {
            $('#RemoveCrossLink').attr("disabled", true);
            $('#utm_campaign').attr("disabled", false);
            options.removeCrossLink = false;
        }
        
        /*End - Added by Nimesh on 29th Sep 2014*/

        options.serviceUrl = getUrlForBrand(brand, $('#countrySelector').val(), env);
        var scr = $('#widgetSnippet').val();
        scr = scr.substr(0, scr.indexOf('var options =') + 14) + JSON.stringify(options) + scr.substring(scr.indexOf('//options') - 1, scr.length);
        $('#widgetSnippet').val(scr);

        if (brand == "" || country == "") {//cross-sell fork
            $('#vehicleSelector').val('campers');
            $('#vehicleSelector').trigger('change');
            $('#testCode').trigger('click');
        } else {
            //$('#vehicleSelector').attr('disabled', false);
        }

        //if ($("#brandSelector").val() == '') { $("#brandSelector").attr('disabled', (country == "" ? 'disabled' : false)) };
        if (country == '' && brand == '') $('#brandSelector').val('b');

        updateWidget();
    });
    updateWidget();//init widget
}

function getWdgtOptions() {
    var optionsStr = $('#widgetSnippet').val();
    optionsStr = optionsStr.substring(optionsStr.indexOf('var options = {') + 14, optionsStr.indexOf('//options') - 1);
    return jQuery.parseJSON(optionsStr);
}

function updateWidget() {
    var scr = $('#widgetSnippet').val();
    scr = scr.substr(scr.indexOf('//widget starts') + 15, scr.indexOf('//widget ends') - scr.indexOf('//widget starts') - 15);
    eval(scr);
}

function getUrlForBrand(brand, country, env) {
    var url = "http://";
    switch (brand) {
        case "m":
            url += env + ".maui";
            break;
        case "b":
            url += env + ".britz";
            break;
        case "y":
            url += env + ".mightycampers";
            break;
        case "u":
            url += env + ".unitedcampervans";
            country = "nz";
            break;
        case "a":
            url += env + ".alphacampervans";
            country = "nz";
            break;
        case "e":
            url += env + ".econocampers";
            country = "nz";
            break;
        //    // Added By Nimesh on 30 July 2014 for Kea Support
        //case "q":
        //    url += (country.toLowerCase() == "au" ? "aubookings.keacampers.com" : "nzbookings.keacampers.com");
        //    break;
        //    // End - Added By Nimesh on 30 July 2014 for Kea Support

    }
    // Added By Nimesh on 30 July 2014 for Kea Support
    //if (brand !== "q") {
        // End - Added By Nimesh on 30 July 2014 for Kea Support
        url = url + (country.toLowerCase() == "au" ? ".com.au" : ".co.nz");
        /*if(brand == "") always from mac */ url = "http://" + env + ".motorhomesandcars.com";//added cross-sell
    //}
    return url;
}

function IsIE8() {
     return (jQuery.browser["msie"] && jQuery.browser["version"] != "9.0");
}

 