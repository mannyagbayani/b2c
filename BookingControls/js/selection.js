﻿function showPriceList(elm) {//bind expand
    var parentElm = $j(elm).parent()[0];
    while (parentElm.tagName != "LI")
        parentElm = $j(parentElm).parent()[0];
    $j(parentElm).toggleClass("Collapse");
    if ($j(parentElm).hasClass("Collapse"))
        $j(elm).text("Price Details");
    else
        $j(elm).text("Hide Details");

    bindIsoTope($j('#vehicleCatalog'), $j('#sortPanel'), null); //sorting 
}



function initSelectionPage() {
    $j('#radiosSwapped').val("false");

    $j(".ShowPriceList").click(function () {//bind expand
        /*
        var parentElm = $j(this).parent()[0];
        while (parentElm.tagName != "LI")
            parentElm = $j(parentElm).parent()[0];
        $j(parentElm).toggleClass("Collapse");
        if($j(parentElm).hasClass("Collapse"))
            $j(this).text("Price Details");
        else
            $j(this).text("Hide Details");

        bindIsoTope($j('#vehicleCatalog'), $j('#sortPanel'), null); //sorting 
        */
    });

    $j("#currencySelector").change(function () {
        var elm = $j(this);
        code = elm.val();
        factor = elm.attr('rel');
        $j("div.TotalPrice ul.Prices li.Fp").addClass('Hide'); //reset
        $j("div.TotalPrice ul.Prices li." + code).removeClass('Hide');
    });

    $j("#currencySelector").val(currencyStr); //reset to local currency

    $j("a.PopUp").click(function (ev) {
        ev.preventDefault();
        var popWidth = ($j(this).attr('href').indexOf('keacampers') > 0 || $j(this).attr('href').indexOf('unitedcampervans') > 0 || $j(this).attr('href').indexOf('alphacampervans') > 0 || $j(this).attr('href').indexOf('econocampers') > 0) ? '980' : '820';
        NewWindow($j(this).attr('href'), 'vCode', (requestParams.brand == 'm' ? '720' : popWidth), '500', 'yes', 'center');
        return false;
    });

    if ($j('#vehicleCatalog a.VehicleThumb') != null && $j('#vehicleCatalog a.VehicleThumb').length > 0) $j('#emptySelectionMsg').css('display', 'none');//hide empty msg..

    if (brandId == 'c')
        $j('#catalogHeader').click(function () { NewWindow('http://www.motorhomesandcars.com/contactus', 'vCode', '820', '500', 'yes', 'center'); });

    if (document.location.href.indexOf('keacampers') > 0)//support SiteFinity redirects
        $j('.ContactInfo a, .CallLink .PopUp').each(function () { $j(this).attr('href', $j(this).attr('href') + ".aspx"); });
}

function initConfigurePage() {
    //set the control:
    $j('#ctrlSearchBox_dropVehicleType option').attr('value', availabilityItem.vCode);
    $j('#ctrlSearchBox_dropVehicleType option').attr("selected", "selected");

    if (requestParams.vt != null) {
        $j("#bookingControl #" + requestParams.vt + "Radio").attr("checked", "checked");
        $j("#vehicleType").val(requestParams.vt);
        if (!(requestParams.vt == 'ac')) $j('#vehicleConfigureContainer').addClass('Campervan');
    }

    if (requestParams.cr != null) { $j('#ctrlSearchBox_dropCountryOfResidence').val(requestParams.cr) };

    if ($j('#controlBrand').val() == "MACCOM") {
        $j('#controlBrand').val('c');
    }

    if ($j('#controlBrand').val() == "AIRNZ")
        $j('#controlBrand').val('z');

    //--end set ctrl

    $j('#bookingSubmitted').val('0');
    $j(".ShowPriceList").click(function (e) {//bind expand
        var parentElm = $j(this).parent()[0];
        while (parentElm.tagName != "DIV")
            parentElm = $j(parentElm).parent()[0];
        $j(parentElm).toggleClass("Collapse");
        if ($j(parentElm).hasClass("Collapse"))
            $j(this).text("Details");
        else
            $j(this).text("Close Details");
        e.stopPropagation();
        e.preventDefault();
    });

    if ($j.browser.safari) {
        $j("#hireItemsPanel select").blur(function () {//bind extra hire change
            updateExtraHiresSelection(this);
        });
    }
    else {
        $j("#hireItemsPanel select").change(function () {//bind extra hire change
            updateExtraHiresSelection(this);
        });
    }

    if ($j.browser.safari) {
        $j("#hireItemsPanel input").click(function () {//bind extra hire change
            updateExtraHiresSelection(this);
        });
    } else {
        $j("#hireItemsPanel input").click(function () {//bind extra hire change
            updateExtraHiresSelection(this);
        });
    }

    $j("#ferryPanel select").change(function () {//bind ferry select change
        updateFerrySelection(this);
    });
    $j("#ferryPanel input[type='checkbox']").attr('checked', false);//clear checked cboxs
    $j("#ferryPanel input").click(function () {//bind ferry CheckBox
        var elm = $j(this);
        if (elm.attr('checked'))//expand ferry panel
            $j("#ferryPanel table." + elm.attr('id')).removeClass('Hide');
        else {//hide and reset
            $j("#ferryPanel table." + elm.attr('id')).addClass('Hide');
            $j("#ferryPanel table." + elm.attr('id') + " select").val('0'); //reset ddowns
        }
        updateFerrySelection(elm);
    });
    $j("#insOptionsTable input, #allInclusiveDetails input").click(function () {//bind insurance CheckBox/Radio
        var elm = $j(this);
        updateIsuranceOption(elm);
    });
    $j("#insuranceAgeSelector input").click(function () {//bind insurance CheckBox/Radio
        var elm = $j(this);
        updateDriversAge(elm);
    });
    updateTotal();
    setER0ByAge(false);
    if (ferryItems.length == 0) {
        $j('#ferryPanel').addClass('Hide');
    }

    if ($j('#promptTextArea span').text().length < 5) $j('#promptTextArea').css('display', 'none'); //hide msg on empty.

    $j("a.PopUp").click(function (ev) {
        ev.preventDefault();
        var popWidth = ($j(this).attr('href').indexOf('keacampers') > 0 || $j(this).attr('href').indexOf('unitedcampervans') > 0 || $j(this).attr('href').indexOf('alphacampervans') > 0 || $j(this).attr('href').indexOf('econocampers') > 0 || $j(this).attr('href').indexOf('maui') > 0) ? '1010' : '600'; //TODO: refactor into config
        NewWindow($j(this).attr('href'), 'vCode', popWidth, '500', 'yes', 'center'); //mauiv2 wider popup
        return false;
    });

    //------restore form (on crumbs)
    $j("#hireItemsPanel input, #hireItemsPanel select").each(
        function (i) {
            if ($j(this).attr('id') != 'ehi_total') {
                //console.log($j(this).attr('id'));
                updateExtraHiresSelection(this);
            }
        });

    swapAgeSelector();

    $j("#currencySelector").change(function () {//bind currency swap
        var elm = $j(this);
        currencyCode = elm.val();
        factor = $j('#currencySelector :selected').attr('rel');
        var currentTotal = $j('#totalAmount').text().replace(/[^0-9.]+/g, '');
        if (currencyCode != currencyStr) {
            $j("#alternateCurrency").css('display', 'block');
            populateForeignTotal($j("#alternateCurrency"), currentTotal, currencyCode);
        } else {
            $j("#alternateCurrency").css('display', 'none');
        }
    });
    $j("#currencySelector").val(currencyStr); //reset to local currency    

    if (document.location.href.indexOf('keacampers') > 0)//support SiteFinity redirects
        $j('.ContactInfo a, .CallLink .PopUp').each(function () { $j(this).attr('href', $j(this).attr('href') + ".aspx"); });

}

function populateForeignTotal(targetDispElm, currentTotal, selectedCurrencyCode) {
    var pageNZRate = $j("#currencySelector option[value='" + currencyStr + "']").attr('rel');

    selectedCurrencyExRate = $j('#currencySelector :selected').attr('rel');
    factor = selectedCurrencyExRate / pageNZRate;
    var currentSym = curSymForCode(selectedCurrencyCode);
    targetDispElm.text("(" + CurrencyFormatted(currentTotal * factor, selectedCurrencyCode, 1).replace("$", currentSym) + ")");
}

function curSymForCode(code) {
    var curSym = { AUD: "$", CAD: "$", CNY: "", EUR: "€", GBP: "£", HKD: "$", JPY: "¥", SGD: "$", ZAR: "", KRW: "", CHF: "", USD: "$", NZD: "$", INR: "Rs", FJD: "$", SBD: "$", THB: "฿" };
    var symbol = eval('curSym.' + code);
    if (symbol == null || typeof symbol == 'undefined') symbol = "";
    return symbol;
}

function swapAgeSelector() {
    var hasAges = false;
    for (i = 0; i < insuranceProducts.length; i++) {
        if (insuranceProducts[i].minAge > 0 || insuranceProducts[i].maxAge > 0)
            hasAges = true;
    }
    if (hasAges) $j('#insuranceAgeSelector').addClass('Show');
}


function initPaymentPage() {
    if ($j('#bookingCompleted').val() == 1) window.location = 'Selection.aspx?errMsg=bookingCompleted' + (SiteInfo.isAggregator ? "&cc=" + SiteInfo.cot : "");
    //alert($j('#bookingCompleted').val());
    if (currentSurcharge != null && currentSurcharge == 0) {
        $j('#ccDesclaimerTxt').css('display', 'none');
        $j('#ccDesclaimerTxtBtm').css('display', 'none');
    }

    $j('#cardTypeDD').change(function () {//bind surcharge        
        var isKea = (document.domain.indexOf('keacampers.com') > 0);
        var elm = $j(this);
        var cardParams = elm.val().split(':');


        if (cardParams[0] == 'Amex' && cardParams[1] > 0.0 && !confirm('Payments by American Express incur a ' + cardParams[1] + '% surcharge. Do you wish to continue?')) {
            $j('#cardTypeDD option:eq(0)').attr("selected", "selected");
            return false;
        }
        var cardName = cardParams[0];
        var cardPercentage = cardParams[1];
        currentSurcharge = (cardParams[1] * 0.01);
        var calculatedSCharge = currentSurcharge * (vehicleCharges.depositAmount);
        var calculateDeposit = calculatedSCharge + vehicleCharges.depositAmount;
        var pickUpBalance = vehicleCharges.calculatedTotal - vehicleCharges.depositAmount;
        vehicleCharges.depositPercentage = cardParams[1];
        $j('#surchargeAmount').text(CurrencyFormatted(calculatedSCharge, '', 0));
        $j('#surchargePercentage').text(cardParams[1]);
        $j('.TotalDeposit, #totalPricesPanel .TotalDeposit').text(CurrencyFormatted(calculateDeposit, '', 0));
        $j('.PickUpBalance, #totalPricesPanel .PickUpBalance').text(CurrencyFormatted(pickUpBalance, '', 0));
        $j("#totalDepositValue").val(calculateDeposit);

        //added for verification number
        if (typeof cardParams[0] != 'undefined' && cardParams[0] != null) {
            $j('#cardVerify').val('');
            switch (cardParams[0]) {
                case "Visa":
                    //$j('#cardVerifyLbl').html("CVV2 Number:*");
                    $j('#cardVerify').attr('maxlength', 3);
                    break;
                case "Mastercard":
                    //$j('#cardVerifyLbl').html("CVC2 Number:*");
                    $j('#cardVerify').attr('maxlength', 3);
                    break;
                case "Amex":
                    //$j('#cardVerifyLbl').html("CID Number:*");
                    $j('#cardVerify').attr('maxlength', 4);
                    break;
            }
        }
        //end verification number
    });

    $j('#cardVerify').blur(function () {
        var elm = $j(this);
        if (elm.val().length < elm.attr('maxlength'))
            elm.val('');
    });

    $j('#cardTypeDD').val('').attr('disabled', '');//reset to first card and enable.

    var currentTime = new Date()
    var month = currentTime.getMonth() + 1
    month = (month < 10 ? "0" + month : month);
    $j('#cardExpiryMonthTxt').val(month);

    $j('#payTotalCB').click(function () {

        if ($j('#paymentForm').hasClass('quote')) {//quote fork 07022011
            payFullQuote(this);
            return;
        }
        if ($j(this).attr('checked')) {
            if (!confirm("The payment due today will be recalculated applying all applicable surcharges to the total value of your hire. Do you wish to continue?")) {
                $j(this).attr('checked', '')
                return false;
            }
            $j('#cardTypeDD').attr('disabled', 'disabled');
            $j('#paymentContainer').addClass('PayFull');

            //----121109
            var calculatedSCharge = currentSurcharge * (vehicleCharges.calculatedTotal); //121109
            var fullAmount = vehicleCharges.calculatedTotal + calculatedSCharge;
            //----

            $j('.TotalDeposit, #totalPricesPanel .TotalDeposit').text(CurrencyFormatted(fullAmount, '', 0));
            $j('.PickUpBalance, #totalPricesPanel .PickUpBalance').text(CurrencyFormatted(0, '', 0));
            $j("#totalDepositValue").val(fullAmount);

            $j('#surchargeAmount').text(CurrencyFormatted(calculatedSCharge, '', 0));
            $j(this).val('true');
            $j('.surchargePerDesc').text('total');
            $j('#sumlabel').text('Total charge (payable today)');

        } else { //restore top deposit only
            $j('#cardTypeDD').attr('disabled', '');
            $j('#paymentContainer').removeClass('PayFull');
            var dpstAmtWsrg = (currentSurcharge * vehicleCharges.depositAmount) + vehicleCharges.depositAmount;
            $j('#surchargeAmount').text(CurrencyFormatted(dpstAmtWsrg, '', 0));
            $j('.TotalDeposit, #totalPricesPanel .TotalDeposit').text(CurrencyFormatted(dpstAmtWsrg, '', 0));
            $j('.PickUpBalance, #totalPricesPanel .PickUpBalance').text(CurrencyFormatted(vehicleCharges.calculatedTotal - vehicleCharges.depositAmount, '', 0));
            $j("#totalDepositValue").val(dpstAmtWsrg);
            var calculatedSCharge = currentSurcharge * (vehicleCharges.depositAmount);
            $j('#surchargeAmount').text(CurrencyFormatted(calculatedSCharge, '', 0));
            $j(this).val('false');
            $j('.surchargePerDesc').text('deposit');
            $j('#sumlabel').text('Deposit total (payable today)');
        }
    });


    $j('#payTotalCB').attr('checked', '');//reset on post backs

    $j("#emailAddressTxt, #confirmEmailAddressTxt").change(function () {
        $j("#confirmEmailErr").remove();
    });

    $j("#paymentForm").validate({
        errorPlacement: function (error, element) {
            if (element.attr("name") == "cardExpiryMonthTxt" || element.attr("name") == "cardExpiryYearTxt") {
                $j('#cardExpirySpan label.error').remove();
                error.insertAfter("#cardExpiryYearTxt");
            }
            else if (element.attr("name") == "telephoneTxt1" || element.attr("name") == "telephoneTxt2" || element.attr("name") == "telephoneTxt3") {
                $j('.PhoneNumber label.error').remove();
                error.insertAfter("#telephoneTxt3");
            }
            else
                error.insertAfter(element);
        }
    });

    $j("a.PopUp").click(function (event) {
        event.stopPropagation();
        if ($j(this).attr('href').indexOf('keacampers') > 0) {//added for kea
            window.open($j(this).attr('href'));
            return false;
        }
        NewWindow($j(this).attr('href'), 'vCode', '600', '500', 'yes', 'center');
        return false;
    });

    if (document.location.href.indexOf('keacampers') > 0)//support SiteFinity redirects
        $j('.ContactInfo a').each(function () { $j(this).attr('href', $j(this).attr('href') + ".aspx"); });
}

function initEmailBookingPage() {
    $j("#customerTitleDD").click(function () {
        $j("#customerTitleDD").css('background-color', '');
    });
    $j("#emailForm").validate({
        errorPlacement: function (error, element) {
            if (element.attr("name") == "telephoneTxt1" || element.attr("name") == "telephoneTxt2" || element.attr("name") == "telephoneTxt3") {
                $j('.PhoneNumber label.error').remove();
                error.insertAfter("#telephoneTxt3");
            }
            else
                error.insertAfter(element);
        }
    });
}

function initPopUp(elm) {
    var elm = $j(elm);
    var parentElm = elm.parent()[0];
    while (parentElm.tagName != "DIV")
        parentElm = $j(parentElm).parent()[0];
    if (!elm.hasClass('Loaded')) {
        elm.addClass('Loaded');
        elm.tooltip({
            track: true,
            delay: 0,
            showURL: false,
            showBody: " - ",
            extraClass: "bubble",
            fixPNG: true,
            opacity: 0.95,
            left: -120,
            bodyHandler: function () {
                return $j("<div class='bubbleBody'>" + /*$j("#popup_" + $j(this).attr('rel')).html()*/$j(".DiscountedDaysPopUp", parentElm).html() + "</div>");
            }
        });
        elm.trigger('mouseover');
    }
}



function initPopUpV2(elm, popUpContentClass) {//TODO: going forward using this as its generic popup functionality
    var elm = $j(elm);
    var parentElm = elm.parent()[0];
    while (parentElm.tagName != "DIV")
        parentElm = $j(parentElm).parent()[0];
    if (!elm.hasClass('Loaded')) {
        elm.addClass('Loaded');
        elm.tooltip({
            track: true,
            delay: 0,
            showURL: false,
            showBody: " - ",
            /*extraClass: "bubble",*/
            fixPNG: true,
            /*opacity: 1.0,*/
            left: -120,
            bodyHandler: function () {
                return $j("<div class='bubbleBody'>" + $j("." + popUpContentClass, parentElm).html() + "</div>");
            }
        });

        elm.trigger('mouseover');
    }
}



function updateDriversAge(elm) {
    if (elm.val() == '18+') {
        $j('#insOptionsTable').addClass('Under21');
        $j('#insOptionsTable').removeClass('Over21');
        setER0ByAge(true);
    }
    else {
        $j('#insOptionsTable').removeClass('Under21');
        $j('#insOptionsTable').addClass('Over21');
        setER0ByAge(false);
    }
}

function setER0ByAge(under21) {//set the relevant ER0
    if (typeof insuranceProducts == 'undefined') return;
    $j('#insOptionsTable td.total em').remove();//remove pricing label elm    
    for (var i = 0; i < insuranceProducts.length; i++) {
        insuranceProducts[i].numItems = 0;
        if (insuranceProducts[i].code.split('_')[0] == 'ER0') {//found ER0           
            insuranceProducts[i].numItems = ((under21 && insuranceProducts[i].minAge == 18) || (!under21 && insuranceProducts[i].minAge == 21)) ? 1 : 0;
            jQuery.each($j('#insOptionsTable tr input'), function () {
                if ($j(this).attr('id') == "ipi_" + insuranceProducts[i].code) {
                    if ((under21 && insuranceProducts[i].minAge == 18) || (!under21 && insuranceProducts[i].minAge == 21) || (insuranceProducts[i].minAge == 0)) {
                        $j(this).attr('checked', 'checked'); //biz logic in front-end                        
                    }
                }
            });
        }
    }
    updateTotal();
}

function updateIsuranceOption(elm) {
    if ($j(elm).attr('type') == 'checkbox') {//this is a <no worries> exp
        var selectedinsuranceElm = null; //init
        for (var i = 0; i < insuranceProducts.length; i++) {
            if ("ipi_" + insuranceProducts[i].id == elm.attr('value')) {
                if (elm.attr('checked'))
                    insuranceProducts[i].numItems = 1;
                else
                    insuranceProducts[i].numItems = 0;
                selectedinsuranceElm = insuranceProducts[i];
            }
            else {
                //insuranceProducts[i].numItems = 0;
            }
        }
        setRowTotal(elm, selectedinsuranceElm.charge, 'insurance');
        updateTotal();
        return;
    }
    else {
        $j("#insOptionsTable INPUT[type='checkbox']").attr('checked', false);
    }
    $j('#allInclusivePkg').attr('checked', false);//untick the bonus pack. 
    $j('#allInclusiveDetails').addClass('Hide');

    var selectedinsuranceElm = null;//init
    for (var i = 0; i < insuranceProducts.length; i++) {
        if ("ipi_" + insuranceProducts[i].id == elm.attr('value')) {
            insuranceProducts[i].numItems = 1;
            selectedinsuranceElm = insuranceProducts[i];
        }
        else {
            insuranceProducts[i].numItems = 0;
        }
    }
    setRowTotal(elm, selectedinsuranceElm.charge, 'insurance');
    setProductDependencies(elm);
    updateTotal();
}

function setRowTotal(elm, totalValue, rowType) {//Set Total within UI Row
    if (rowType == "extraHire") {
        totalValue = (totalValue > 0 ? totalValue : "");
        parentElm = $j(elm.parent()[0]);
        var trElm = $j(parentElm).parent()[0];
        $j('.total', trElm).html((totalValue > 0) ? "<em>" + CurrencyFormatted(totalValue, currencyStr, 10) + "</em>" : "");
    } else if (rowType == "insurance") {
        totalValue = (totalValue > 0 ? totalValue : "");
        parentElm = $j(elm.parent()[0]);
        var trElm = $j(parentElm).parent()[0];
        $j('.total', trElm).html((totalValue > 0) ? "<em>" + CurrencyFormatted(totalValue, currencyStr, 10) + "</em>" : "");
        jQuery.each($j('#insOptionsTable tr'), function () {
            if (!$j('.cb input', $j(this)).attr('checked'))
                $j('.total', $j(this)).text('');
        });
    }
}

function disableInsCbs() {
    $j("#insOptionsTable INPUT[type='checkbox']").attr('disabled', 'disabled');
}

function setProductDependencies(selectedElm) {
    //iterate through all items and check if selected item key exists
    // if exists and has NOTDISPLAY value untick and disable    
    //disableInsCbs();
    var selectedElmId = selectedElm.attr('id').split('_')[1];
    for (var i = 0; i < insuranceProducts.length; i++) {
        //found the selected meta and it has not display items in it
        if (insuranceProducts[i].requiredActions != null && insuranceProducts[i].requiredActions.length > 0) {//has required actions
            for (var j = 0; j < insuranceProducts[i].requiredActions.length; j++) {//for each action
                var currentParams = insuranceProducts[i].requiredActions[j].split(':');
                if (currentParams[0] == selectedElmId && currentParams[1] == 'DISPLAYWHEN') {//has display action
                    $j("#ipi_" + insuranceProducts[i].code).attr('disabled', false);
                }
            }
        }
    }
}

function renderInclusive(elm, avpId) {
    var cbElm = $j(elm);
    var selected = (cbElm.attr('checked') == true || cbElm.attr('checked') == "checked")
    $j('#hasBonusPack').val(selected ? 1 : 0);
    document.forms[0].submit();
    disableForm($j('#form1'), true);
}

function setInclusiveSelected(selected) {//force the inclusive insurance option if selected
    $j('#insOptionsTable tr.IsInclusive input').attr('checked', selected ? 'checked' : '');
}

function updateFerrySelection(elm) {
    var selectedElm = $j(elm);
    var elmType = elm.tagName;
    var selected = $j(selectedElm);
    var amount = 0;
    var checkboxes = $j('#ferryPanel input'); //get all vehicles
    for (var i = 0; i < checkboxes.length; i++) {
        var check = $j(checkboxes[i]);
        if (check.attr('checked')) {
            amount += parseFloat(check.val());
        }
    }
    //ferryItems = {pct_wlg:{selected:false,vehicle:209.00,Adult:75.00,Child:40.00,Infant:0.00},wlg_pct:{selected:false,Adult:75.00,Child:40.00,Infant:0.00,},total:0};
    var ddowns = $j('#ferryPanel tr.pax td select'); //get all pax

    //reset the hidden containers
    $j("#ferryPanel #paxCounter").val('');

    for (var i = 0; i < ddowns.length; i++) {
        var dd = $j(ddowns[i]);
        var idParams = dd.attr('id').split('_');
        var valStr = "ferryItems." + idParams[0] + "_" + idParams[1] + "." + idParams[2];
        var currentItemPrice = parseFloat(eval(valStr));
        var ddownVal = parseFloat(dd.val());
        if (ddownVal > 0)
            amount += ddownVal * currentItemPrice;
        var parentTR = dd;
        while (parentTR.tagName != "TR")
            parentTR = $j(parentTR).parent()[0];
        $j('.total', $j(parentTR)).text((ddownVal > 0) ? CurrencyFormatted(ddownVal * currentItemPrice, currencyStr, 2) : '');

        //get and set the relevant hidden container
        var currentPrdId = dd.attr('rel');
        var hiddenInput = $j("#ferryPanel #paxCounter");
        var currentVal = hiddenInput.val();
        hiddenInput.val(currentVal + idParams[0] + "_" + idParams[1] + "_" + idParams[2] + "_" + currentPrdId + ":" + ddownVal + ",");
        //end update hidden        
    }
    ferryItems.total = amount;



    if (typeof vCharges == "undefined") updateTotal(); //Added for Modify Quote
    else updateQuoteTotal();

}

function updatetotalForCar() {
    //$j('#paymentContainer').addClass('PayFull');

    //var calculatedSCharge = currentSurcharge * calculatedTotal;

    //var fullAmount = calculatedTotal + calculatedSCharge;

    ////logMe(fullAmount, "payFullQuote");
    ////----
    //$j('.TotalDeposit, #totalPricesPanel .TotalDeposit').text(CurrencyFormatted(fullAmount, '', 0));
    //$j('.PickUpBalance, #totalPricesPanel .PickUpBalance').text(CurrencyFormatted(0, '', 0));
    //$j("#totalDepositValue").val(fullAmount);
    //$j('#surchargeAmount').text(CurrencyFormatted(calculatedSCharge, '', 0));
    //$j('#payTotalCB').val('true');
    //$j('.surchargePerDesc').text('total');
    //$j('#sumlabel').text('Total charge (payable today)');
    ////$j('#payTotalCB').val('false');
    $j('#payTotalPanel').css('display', 'none');
}
function updateTotal() {
    var totalAmount = availabilityItem.vehicleTotal; //Vehicle   
    totalAmount = totalAmount - oneWayFeeWaiver;

    var hiredays = availabilityItem.hirePeriod;
    for (var i = 0; i < insuranceProducts.length; i++) {//Add Insurance
        if (insuranceProducts[i].numItems == 1)
            totalAmount += insuranceProducts[i].charge  //TODO: is insuranceProducts.MaxCharge less..
    }
    for (var i = 0; i < extraHireItems.length; i++) {//add extra hires
        if (extraHireItems[i].numItems > 0)
            totalAmount += calculateExtraHireTotal(extraHireItems[i], hiredays);
    }
    if (ferryItems.total != null && ferryItems.total > 0)
        totalAmount += ferryItems.total;//add ferry totals..

    //--add admin fee
    var totalWithAdmin = 0;

    if (vehicleCharges.bonusPackSelected != 1) {
        totalWithAdmin = totalAmount * (1 + (availabilityItem.adminFee / 100));
    } else {
        totalWithAdmin = totalAmount;
    }

    totalWithAdminStr = CurrencyFormatted(totalWithAdmin, currencyStr, 10);
    $j('#totalAmount').text(totalWithAdminStr);

    try {//update foreign currency 220110
        currencyCode = $j("#currencySelector").val();
        populateForeignTotal($j("#alternateCurrency"), totalWithAdmin, currencyCode);
    }
    catch (e) { }

}

function calculateExtraHireTotal(itemElm, hiredays) {
    return (itemElm.charge);
}

function updateExtraHiresSelection(elm) {
    var selectedElm = $j(elm);
    var elmType = elm.tagName;
    var selected = $j(selectedElm);
    var itemId = selected.attr('id').split('_')[1];
    var selectedElmData = getHireItemById(itemId);
    var selectedUOM = selectedElmData.uom;
    var selectedMaxCharge = selectedElmData.maxCharge;
    if (selectedElmData == null)
        return;//no data match
    var amount = 0;
    if (elmType == "INPUT") {
        amount = (selected.attr('checked') ? 1 : 0);
    } else {//<SELECT>
        amount = selected.val();
    }
    selectedElmData.numItems = amount;
    var numDays = (selectedUOM == 'Day') ? availabilityItem.hirePeriod : 1;
    var perUnitTotalCap = (numDays * selectedElmData.dailyRate > selectedMaxCharge && selectedMaxCharge > 0) ? selectedMaxCharge : numDays * selectedElmData.dailyRate;
    var totalCalCharge = perUnitTotalCap * amount;
    totalCalCharge = (selectedElmData.grossAmt * amount);
    selectedElmData.charge = totalCalCharge;
    setRowTotal(selectedElm, selectedElmData.charge, 'extraHire');
    updateTotal();
}

function getHireItemById(itemId) {
    for (i = 0; i < extraHireItems.length; i++)
        if (extraHireItems[i].id == itemId)
            return extraHireItems[i];
    return null;
}

function serializeBookingInfo() {
    var selectedProducts = [];
    for (var i = 0; i < extraHireItems.length; i++)
        if (extraHireItems[i].numItems > 0)
            selectedProducts.push(extraHireItems[i]);
    for (var i = 0; i < insuranceProducts.length; i++)
        if (insuranceProducts[i].numItems > 0)
            selectedProducts.push(insuranceProducts[i]);
    if ($j('#bookingSubmitted').val() != 2)
        $j('#bookingSubmitted').val(1);//TODO: if all   
    return true;
}

function serializeHireItems() {
    var inputElm = $j('#ehi_total');
    var serValue = '';
    if (extraHireItems.length > 0) {
        serValue = "[";
        for (i = 0; i < extraHireItems.length; i++) {
            serValue += "{id:'" + extraHireItems[i].id + "',amount:" + extraHireItems[i].numItems + ",totalPrice:" + extraHireItems[i].charge + "},";
        }
        serValue = serValue.substr(0, serValue.length - 1) + "]";
    }
    inputElm.val(serValue);
}

function submitPaymentForm() {

    if($j == undefined)
    var $j = jQuery.noConflict();
    var webmethod = $j('#paymentForm').hasClass('quote') ? "ConfirmAQuote" : "MakePayment";


    if (!localCardValidation()) return false;

    var validated = false;
    var selSlot = false;
    /* Added by Nimesh on 21st Aug 2014 to validate if slot is selected*/
    var $k = $j("<label>").text('Date:');

    if (jQuery('.timeslots.available').length > 0) {
        if (!(jQuery('.timeslots.available.selected').length === 1)) {
            var error = $j("<label id='selectionError' style='color:red;margin-left: 5px;'>Please select time slot</label>");
            error.insertBefore('#pickuptimes');
            var position = jQuery('#pickuptimes').position();
            scroll(0, position.TopPosition);
            return false;
        }
        else {
            $j('#selectionError').remove();
            selSlot = true;
        }
    }
    /* End Added by Nimesh on 21st Aug 2014 to validate if slot is selected*/
    if ($j("#emailAddressTxt").val() != $j("#confirmEmailAddressTxt").val()) {
        var error = $j("<label id='confirmEmailErr' style='color:red;margin-left: 5px;'>Emails do not match</label>");
        $j('#confirmEmailErr').remove();
        error.insertAfter("#confirmEmailAddressTxt");
        return false;
    } else {
        $j('#confirmEmailErr').remove();
    }

    if (!$j('#acceptedTerms').attr('checked')) {
        validated = false;
        $j('#acceptedTerms').addClass('required');
    } else {
        validated = true;
        $j('#acceptedTerms').removeClass('required');
    }
    if (validated && ($j('#paymentForm').valid())) {
        var formParams = "{meta:1";
        $j('#paymentForm').find("input[type='checkbox'], input[type='text'], input[type='hidden'], input[type='password'], option[selected], textarea").each(
            function () {
                formParams += "," + (this.name || this.id || this.parentNode.name || this.parentNode.id) + ":'" + this.value + "'";
            });
        // Nimesh Added this for time slot on 22nd Aug 2014
        if (selSlot) {
            formParams += ", selectedSlot:'";
            jQuery('.timeslots.available.selected').find('.time.left').each(function () {
                formParams += this.innerText;
            });
            formParams += " : ";
            jQuery('.timeslots.available.selected').find('.time.right').each(function () {
                formParams += this.innerText + "'";
            });
            var slotId = jQuery('.timeslots.available.selected').attr('id').replace('slot', '');
            formParams += ",slotId:'" + slotId + "'";
            //formParams += ",selectedSlot:'11:00 am : 04:30 pm'";

        }
        else
            formParams += ", selectedSlot:'', slotId:''";
        // End Nimesh Added this for time slot on 22nd Aug 2014
        formParams += "}"
        var serviceURL = ContentbaseUrl + "/webservices/CreateBooking.asmx/" + webmethod; //TODO: from config
        $j('#footerPanel').addClass("Submitting");

        $j('#cardNumberTxt').val('');

        enableForm('paymentForm', false);

        $j('#globalFormError').removeClass('Error');
        $j.ajax({
            type: "POST",
            cache: false,
            contentType: "application/json; charset=utf-8",
            url: serviceURL,
            data: formParams,
            dataType: "json",
            success: function (data, textStatus) {
                var response = eval(data);
                if (response.d.indexOf('rror') > 0) {
                    var errorParams = response.d.split(':');
                    if (errorParams[1] == "Session Expired")
                        window.location = "Selection.aspx?msg=sessionExpired";
                    $j('#globalFormError').addClass('Error');
                    $j('#footerPanel').removeClass("Submitting");
                    $j('#globalFormError').text(errorParams[1]);
                    $j('#cardTypeDD').attr('disabled', '');
                    enableForm('paymentForm', true);
                } else {
                    if (response.d.indexOf('Id:') > 0) {
                        var bId = response.d.split(':')[1];
                        $j('#bookingCompleted').val('1');
                        if (bId == 0) {//added for quote case
                            bId = $j('#bookingId').val();
                            window.location = ContentbaseUrl + "/Confirmation.aspx?bookingId=" + bId + "&quote=1";
                        }
                        else
                            window.location = ContentbaseUrl + "/Confirmation.aspx?bookingId=" + bId;
                    }
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                enableForm(false);
                $j('#cardTypeDD').attr('disabled', '');
                $j('#globalFormError').addClass('Error');
                $j('#footerPanel').removeClass("Submitting");

                enableForm('paymentForm', true);

                this; // the options for this ajax request
            }
        });
    }
    return false;
}

function localCardValidation() {
    var cardNumberFDig = $j('#cardNumberTxt').val().substr(0, 1);
    var cardTypeElm = $j('#cardTypeDD');
    var cardParams = cardTypeElm.val().split(':');
    if ((cardParams[0] == 'Amex' && cardNumberFDig != 3) || (cardParams[0] == 'Visa' && cardNumberFDig != 4) || (cardParams[0] == 'Mastercard' && cardNumberFDig != 5)) {
        alert('invalid card type');
        return false;
    } else
        return true;
}

function enableForm(formId, enable) {
    $j('#' + formId).find("input[type='checkbox'], input[type='text'], select, textarea").attr('disabled', (enable ? '' : 'disabled'));
}

function submitEmailForm() {
    if ($j("#customerTitleDD").val() == '0') {//<select>s here 
        $j("#customerTitleDD").css('background-color', 'red');
        return false;
    }
    if ($j('#submitted').val() == '1' || !$j('#emailForm').valid()) {//Do not submit twice or non valid forms..
        return false;
    }

    if ($j("#emailAddressTxt").val() != $j("#confirmEmailAddressTxt").val()) {
        var error = $j("<label id='confirmEmailErr' style='color:red;margin-left: 5px;'>Emails do not match</label>");
        $j('#confirmEmailErr').remove();
        error.insertAfter("#confirmEmailAddressTxt");
        return false;
    } else {
        $j('#confirmEmailErr').remove();
    }

    var formParams = "{meta:1";
    $j('#emailForm').find("input[checked='checked'], input[type='text'], input[type='radio'],input[type='hidden'], input[type='password'], option[selected], textarea").each(
            function () {
                formParams += "," + (this.name || this.id || this.parentNode.name || this.parentNode.id) + ":'" + this.value + "'";
            });
    formParams += "}"
    var serviceURL = ContentbaseUrl + "/webservices/CreateBooking.asmx/BookingRequest"; //TODO: from config
    $j('#bottomPanel').addClass("Submitting");
    $j('#globalFormError').removeClass('Error');
    $j('#submitted').val('1');
    enableForm('emailForm', false);
    $j.ajax({
        type: "POST",
        cache: false,
        contentType: "application/json; charset=utf-8",
        url: serviceURL,
        data: formParams,
        dataType: "json",
        success: function (data, textStatus) {
            var response = eval(data);
            if (response.d.indexOf('rror') > 0) {
                var errorParams = response.d.split(':');
                $j('#globalFormError').addClass('Error');
                $j('#bottomPanel').removeClass("Submitting");
                $j('#globalFormError').text(errorParams[1]);
                $j('#submitted').val('0');
                enableForm('emailForm', true);
            } else {
                if (response.d.indexOf('Id:') > 0) {
                    var bId = response.d.split(':')[1];
                    $j('#generatedBookingId').text(bId);
                    swapFormMsg(bId);
                }
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            enableForm('emailForm', true);
            $j('#globalFormError').addClass('Error');
            $j('#bottomPanel').removeClass("Submitting");
            window.location = "Selection.aspx?msg=sessionExpired";
            this; // the options for this ajax request
        }
    });
    return false;
}

function swapFormMsg(bId) {
    var compImgHTML = DNATagHTML.substr(10, DNATagHTML.indexOf('.com') + 4) + "&trans=" + bId + "' width='1' height='1' border=0' />";
    //alert(DNATagHTML.substr(10, DNATagHTML.indexOf('.com') + 4) + "&trans=" + bId);    
    $j('#paymentPanel').addClass('Hide');
    $j('#completedMessage').removeClass('Hide');
    var i = new Image();
    i.src = DNATagHTML + "trans=" + bId;
}

function disableForm(formElm, enable) {
    formElm.find("input, select, textarea").attr('disabled', enable);
}

function CurrencyFormatted(amount, currencyCode, pricePadding) {
    var i = parseFloat(amount);
    if (isNaN(i)) { i = 0.00; }
    var minus = '';
    if (i < 0) { minus = '-'; }
    i = Math.abs(i);
    i = parseInt((i + .005) * 100);
    i = i / 100;
    s = new String(i);
    if (s.indexOf('.') < 0) { s += '.00'; }
    if (s.indexOf('.') == (s.length - 2)) { s += '0'; }
    s = minus + s; //s is now Neutral Decimal
    var regEx = /(\d+)(\d{3})/;
    while (regEx.test(s)) {
        s = s.replace(regEx, '$1' + ',' + '$2');
    }
    var padding = "";
    for (var i = 0; i < (pricePadding - s.length) ; i++)
        padding = padding + " ";
    s = currencyCode + " $" + padding + s;
    return s;
}

function updateHorizontalCtrl(initParams) {
    //var requestParams = {pb:'adl',db:'adl',cr:'AU',cc:'AU',rf:'',vt:'av',vc:'',pd:'22-Oct-2009',pt:'10:00',dd:'18-Nov-2009',dt:'10:00',na:'1',nc:'0',pb:'ADL',db:'ADL',com:'',brand:'z',debug:''};
    //$j("#ctrlSearchBox_dropCountryOfResidence").val(requestParams.cr);
    if (requestParams.cr != null) {
        $j("#ctrlSearchBox_dropCountryOfResidence").val(requestParams.cr);
    }
    $j("#ctrlSearchBox_dropPickUpLocation").val(requestParams.pb);
    $j("#ctrlSearchBox_dropDropOffLocation").val(requestParams.db);
    $j("#ctrlSearchBox_dropAdults").val(requestParams.na);
    $j("#ctrlSearchBox_dropChildren").val(requestParams.nc);
    $j("#ctrlSearchBox_dropVehicleType").val(requestParams.vc);
    $j("#ctrlSearchBox_dropPickUpTime").val(requestParams.pt);
    $j("#ctrlSearchBox_dropDropOffTime").val(requestParams.dt);
    $j('#ctrlSearchBox_dropVendor').val(requestParams.com);
    $j('#ctrlSearchBox_calPickUp_DateText').val(requestParams.pd);
    $j('#ctrlSearchBox_calDropOff_DateText').val(requestParams.dd);
}

function bindPopUps(aElm) {
    aElm.click(function () {
        NewWindow($j(this).attr('href'), 'vCode', '600', '500', 'yes', 'center');
        return false;
    });
}

var win = null;

function NewWindow(mypage, myname, w, h, scroll, pos) {

    if (pos == "random") { LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100; TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100; }
    if (pos == "center") { LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100; TopPosition = (screen.height) ? (screen.height - h) / 2 : 100; }
    else if ((pos != "center" && pos != "random") || pos == null) { LeftPosition = 0; TopPosition = 20 }
    settings = 'width=' + w + ',height=' + h + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=1,location=no,directories=no,status=yes,menubar=no,toolbar=no,resizable=yes';
    win = window.open(mypage, myname, settings);
    win.focus();
}

function selectionPreLoader(enabled) {
    //    var msgPanel = $j('#emptySelectionMsg');
    //    if (enabled) {
    //        msgPanel.html();//preloader + msg
    //    } else {
    //        msgPanel.html();//remove preloader 
    //    }
}


function saveQuote() {
    var selectedProducts = [];
    for (var i = 0; i < extraHireItems.length; i++)
        if (extraHireItems[i].numItems > 0)
            selectedProducts.push(extraHireItems[i]);
    for (var i = 0; i < insuranceProducts.length; i++)
        if (insuranceProducts[i].numItems > 0)
            selectedProducts.push(insuranceProducts[i]);
    $j('#bookingSubmitted').val(2); //quote flag
    $j('#form1').submit();

}

function submitQuoteForm() {
    var validated = false;
    if ($j("#emailAddressTxt").val() != $j("#confirmEmailAddressTxt").val()) {
        var error = $j("<label id='confirmEmailErr' style='color:red;margin-left: 5px;'>Emails do not match</label>");
        $j('#confirmEmailErr').remove();
        error.insertAfter("#confirmEmailAddressTxt");
        return false;
    } else {
        $j('#confirmEmailErr').remove();
    }


    validated = true;
    //$j('#acceptedTerms').removeClass('required');

    if (validated && ($j('#paymentForm').valid())) {
        var formParams = "{meta:1";
        $j('#paymentForm').find("input[type='checkbox'], input[type='text'], input[type='hidden'], input[type='password'], option[selected], textarea").each(
            function () {
                formParams += "," + (this.name || this.id || this.parentNode.name || this.parentNode.id) + ":'" + this.value + "'";
            });
        formParams += "}"
        var serviceURL = ContentbaseUrl + "/webservices/CreateBooking.asmx/MakeQuote"; //TODO: from config
        $j('#footerPanel').addClass("Submitting");

        enableForm('paymentForm', false);

        $j('#globalFormError').removeClass('Error');
        $j.ajax({
            type: "POST",
            cache: false,
            contentType: "application/json; charset=utf-8",
            url: serviceURL,
            data: formParams,
            dataType: "json",
            success: function (data, textStatus) {
                var response = eval(data);
                if (response.d.indexOf('rror') > 0) {
                    var errorParams = response.d.split(':');
                    if (errorParams[1] == "Session Expired")
                        window.location = "Selection.aspx?msg=sessionExpired";
                    $j('#globalFormError').addClass('Error');
                    $j('#footerPanel').removeClass("Submitting");
                    $j('#globalFormError').text(errorParams[1]);
                    $j('#cardTypeDD').attr('disabled', '');
                    enableForm('paymentForm', true);
                } else {
                    if (response.d.indexOf('Id:') > 0) {
                        var bId = response.d.split(':')[1];
                        $j('#bookingCompleted').val('1');
                        window.location = ContentbaseUrl + "QuoteConfirmation.aspx?bookingId=" + bId;
                    }
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                enableForm(false);
                $j('#cardTypeDD').attr('disabled', '');
                $j('#globalFormError').addClass('Error');
                $j('#footerPanel').removeClass("Submitting");

                enableForm('paymentForm', true);

                this; // the options for this ajax request
            }
        });
    }
    return false;
}

/* PriceMatch, refactor into CMS */
function openPriceMatchWindow() {
    var brandDomain = document.domain;
    var popUpUrl = "http://" + document.domain.replace("secure", "www") + "/price-guarantee-popup";
    NewWindow(popUpUrl, 'vCode', '820', '600', 'yes', 'center');
}

/* Retrieve and Configure Quote support  */

function initConfigureQuote() {

    $j('#ferryPanel select').val('0');

    //--Insurance Starts
    for (var i = 0; i < insuranceOptions.selected.length; i++) {
        $j('#ipi_' + insuranceOptions.selected[i]).attr('checked', 'checked');
    }


    $j("#insOptionsTable input[type='radio']").click(function () {//bind insurance CheckBox/Radio event mgmt
        var elm = $j(this);
        depParams = elm.attr('rel').split(' ');
        var currentElmCode = depParams[0];
        $j("#insOptionsTable tr.content").each(
            function (i) {
                var currentTr = $j(this);
                if (currentTr.hasClass(currentElmCode)) currentTr.css('display', '');
                else currentTr.css('display', 'none');
            });
        var insCboxes = $j("#insOptionsTable input[type='checkbox']")
        for (var i = 0; i < insCboxes.length; i++) {
            var cbElm = $j(insCboxes);
            cbElm.attr("checked", "");
            if (cbElm.attr('rel').indexOf(depParams[0]) > 0) cbElm.attr('disabled', '');
            else cbElm.attr('disabled', 'disabled');
        }

        var exMore = ((new String(document.location)).indexOf("xplore") > -1);
        if (exMore) {
            if ($j(this).attr('rel').indexOf('ER2') > -1) $j("#insOptionsTable input[type='checkbox']").attr('disabled', '');
            else $j("#insOptionsTable input[type='checkbox']").attr('disabled', 'disabled');
        }


        updateVtotal();
    });

    $j("#insOptionsTable input").click(function () {
        updateQuoteInsurance();
    });

    updateQuoteInsurance();

    $j("#insOptionsTable input[type='radio']").each(function (i) {
        //alert('');
        if ($j(this).attr("rel") == "ER2" && $j(this).attr("checked"))
            $j("#insOptionsTable input[type='checkbox']").attr('disabled', '')
    });

    $j("#insuranceAgeSelector [name='ageRadio']").each(function (i) {
        if ($j(this).val() == "18+" && $j('#insOptionsTable').hasClass("Under21"))
            $j(this).attr("checked", "checked");
        if ($j(this).val() == "21+" && $j('#insOptionsTable').hasClass("Over21"))
            $j(this).attr("checked", "checked");
    });

    $j("#insOptionsTable input[type='checkbox']").each(function (i) {
        if ($j(this).hasClass("Under21") && $j('#insOptionsTable').hasClass("Over21"))
            $j(this).attr("checked", "");
        if ($j(this).hasClass("Over21") && $j('#insOptionsTable').hasClass("Under21"))
            $j(this).attr("checked", "");
    });

    $j("#insuranceAgeSelector").click(function () {
        $j('#insOptionsTable input').attr('checked', ''); $j("#insOptionsTable input[type='checkbox']").attr('disabled', 'disabled'); $j('#insOptionsTable td.total em').html('');

        //invalidate the selected insurance (age changed)
        $j("#insOptionsTable input[rel='ER0']").trigger('click');
        insuranceOptions.selected = [];
        updateQuoteInsurance();

    });

    //--Insurance Ends

    //--Extra Hires Starts    
    updateQuoteHires();
    $j('#hireItemsPanel table tr input, #hireItemsPanel table tr select').change(function () {
        updateQuoteHires();
    });
    /*
    if($j('#insOptionsTable').hasClass('Over21'))//history back fix
        $j('#insOptionsTable tr input.Under21').attr('checked','');
    else
        $j('#insOptionsTable tr input.Over21').attr('checked','');
    */
    $j("#insOptionsTable input[type='checkbox']").each(function (i) {
        if ($j(this).attr('checked')) $j(this).attr('disabled', '');
    })
    //--Extra Hires ends


}

function updateQuoteHires() {
    if (window.console && window.console.firebug) console.log("----");
    var ehiTotal = 0;
    $j('#hireItemsPanel table tr input, #hireItemsPanel table tr select').each(function (i) {
        var selectedElm = $j(this);
        var elmType = this.tagName;
        var itemId = selectedElm.attr('id').split('_')[1];
        var hireData = null;
        for (i = 0; i < availableHires.length; i++)
            if (availableHires[i].id == itemId) hireData = availableHires[i];
        var qty = 0;
        if (elmType == "INPUT") qty = (selectedElm.attr('checked') || selectedElm.attr('checked') == "checked" ? 1 : 0);
        else qty = selectedElm.val();
        var totalValue = (hireData.uom == "Day" || hireData.uom == "H24") ? qty * hireData.prc * selectedHires.hirePeriod : qty * hireData.prc;
        totalValue = (totalValue > hireData.max && hireData.max > 0) ? hireData.max : totalValue;
        var currencyStr = selectedHires.cur;
        var trElm = $j(this).parent()[0];
        while (trElm.tagName != "TR") trElm = $j(trElm).parent()[0];
        $j('.total', trElm).html((totalValue > 0) ? "<em>" + CurrencyFormatted(totalValue, currencyStr, 10) + "</em>" : "");
        ehiTotal += totalValue;
    });
    selectedHires.total = ehiTotal;
    updateQuoteTotal();
    //logMe(ehiTotal, "upQtHire");
}

function updateQuoteInsurance() {
    if (vCharges.loadedAdmin > 0) {
        vCharges.admin = vCharges.loadedAdmin;
        $j("span.TaxMsg").addClass("HasAdminFee");
    }
    var totalIns = 0;
    insuranceOptions.selected = [];
    $j("#insOptionsTable tr td.total").html('');
    $j("#insOptionsTable input").each(function () {
        currencyStr = insuranceOptions.cur;
        var selectedElm = $j(this);
        var itemId = selectedElm.attr('id').split('_')[1];
        if (selectedElm.attr('checked') || selectedElm.attr('checked') == "checked") {
            var trElm = $j(this).parent()[0];
            while (trElm.tagName != "TR") trElm = $j(trElm).parent()[0];
            for (var j = 0; j < vCharges.fees.length; j++) vCharges.fees[j].waive = 0; //reset fee waivers
            $j('table.chargesTable tr td.pr').each(
                function (i) {
                    var elm = $j(this);
                    elm.text(elm.attr('rel'));
                });
            for (i = 0; i < optionalInsOptions.length; i++) {//search in possible options
                totalValue = optionalInsOptions[i].grsAmt;
                if (optionalInsOptions[i].sap == itemId) {
                    $j('.total', trElm).html((totalValue > 0) ? "<em>" + CurrencyFormatted(totalValue, currencyStr, 10) + "</em>" : "");
                    totalIns += totalValue;
                    insuranceOptions.total = totalIns;
                    insuranceOptions.selected.push(itemId);
                    //set waved flags                    
                    if (optionalInsOptions[i].waive != '') {
                        for (var j = 0; j < vCharges.fees.length; j++) {
                            if (optionalInsOptions[i].waive.indexOf(vCharges.fees[j].name) > -1) {
                                vCharges.fees[j].waive = 1;

                                if (vCharges.fees[j].name == "STADMIN") {//biz, waive admin
                                    vCharges.admin = 0;
                                    $j("span.TaxMsg").removeClass("HasAdminFee");
                                }
                                $j('table.chargesTable tr.' + vCharges.fees[j].name + ' td.pr').text('included'); //TODO: reveres
                            }
                        }
                    }
                    logMe(totalIns, "upQtIns");
                }
            }
        }
    });
    updateVtotal();//TODO: test
    updateQuoteTotal();

    $j("#currencySelector").change(function () {//bind currency swap
        var elm = $j(this);
        currencyCode = elm.val();
        factor = $j('#currencySelector :selected').attr('rel');
        var currentTotal = $j('#totalAmount').text().replace(/[^0-9.]+/g, '');
        if (currencyCode != currencyStr) {
            $j("#alternateCurrency").css('display', 'block');
            populateForeignTotal($j("#alternateCurrency"), currentTotal, currencyCode);
        } else {
            $j("#alternateCurrency").css('display', 'none');
        }
    });

    //-ferry
    $j("#ferryPanel select").change(function () {//bind ferry select change
        updateFerrySelection(this);
    });
    $j("#ferryPanel input[type='checkbox']").attr('checked', ''); //clear checked cboxs
    $j("#ferryPanel input").click(function () {//bind ferry CheckBox
        var elm = $j(this);
        if (elm.attr('checked'))//expand ferry panel
        {
            $j("#ferryPanel table." + elm.attr('id')).removeClass('Hide');

            $j('.pax select', "#ferryPanel table." + elm.attr('id')).each(//force a driver
                function (i) {
                    var selElm = $j(this);
                    if (selElm.attr('id').indexOf('Adult') > 0)
                        selElm.val(1);
                }
            );
        }
        else {//hide and reset
            $j("#ferryPanel table." + elm.attr('id')).addClass('Hide');
            $j("#ferryPanel table." + elm.attr('id') + " select").val('0'); //reset ddowns
        }
        updateFerrySelection(elm);
    });

    //-ferry

    //UI
    $j(".ShowPriceList").click(function (e) {//bind expand
        var parentElm = $j(this).parent()[0];
        while (parentElm.tagName != "DIV")
            parentElm = $j(parentElm).parent()[0];
        $j(parentElm).toggleClass("Collapse");
        if ($j(parentElm).hasClass("Collapse"))
            $j(this).text("Details");
        else
            $j(this).text("Close Details");
        e.stopPropagation();
        e.preventDefault();
    });
    $j("a.PopUp").click(function (ev) {
        ev.preventDefault();

        if ($j(this).attr('href').indexOf('keacampers') > 0) {//added for kea
            window.open($j(this).attr('href'));
            return false;
        }

        NewWindow($j(this).attr('href'), 'vCode', (siteInfo.brand == 'm' ? '720' : '600'), '500', 'yes', 'center'); //mauiv2 wider popup
        return false;
    });

    $j('bookingSubmitted').val('0');
    //UI
    $j("#insuranceAgeSelector input").click(function () {//bind insurance CheckBox/Radio
        var elm = $j(this);
        updateDriversAge(elm);
    });
}

function updateVtotal() {
    //TODO: subtract waived fee vCharges.vTotal -= with per<->total fork
    vCharges.vTotal = vCharges.vBase;
    for (var i = 0; i < vCharges.fees.length; i++) {
        if (vCharges.fees[i].waive && vCharges.fees[i].isPer != 1)
            vCharges.vTotal -= vCharges.fees[i].amount;
    }
    $j('#ChargeHeader span.Total small').html(CurrencyFormatted(vCharges.vTotal, vCharges.cur, 10));
}

function updateQuoteTotal() {
    currencyStr = vCharges.cur;
    var totalValue = parseFloat(vCharges.vTotal) + parseFloat(insuranceOptions.total) + parseFloat(selectedHires.total); //TODO: add ferry here
    if (typeof ferryItems != 'undefined' && ferryItems != null && ferryItems.total > 0) totalValue += ferryItems.total;
    perComponent = vCharges.admin > 0 ? 1 + parseFloat(vCharges.admin) / 100 : 1;
    logMe(totalValue, "upQtTo");
    $j('#totalAmount').html(CurrencyFormatted(totalValue * perComponent, currencyStr, 10));
}

function serialiseQuote() {
    $j('#bookingSubmitted').val('1');
    return true;
}

function payFullQuote(cbElm) {
    if ($j(cbElm).attr('checked')) {
        if (!confirm("The payment due today will be recalculated applying all applicable surcharges to the total value of your hire. Do you wish to continue?")) {
            $j(cbElm).attr('checked', '')
            return false;
        }
        $j('#cardTypeDD').attr('disabled', 'disabled');
        $j('#paymentContainer').addClass('PayFull');

        var calculatedSCharge = currentSurcharge * calculatedTotal;

        var fullAmount = calculatedTotal + calculatedSCharge;

        //logMe(fullAmount, "payFullQuote");
        //----
        $j('.TotalDeposit, #totalPricesPanel .TotalDeposit').text(CurrencyFormatted(fullAmount, '', 0));
        $j('.PickUpBalance, #totalPricesPanel .PickUpBalance').text(CurrencyFormatted(0, '', 0));
        $j("#totalDepositValue").val(fullAmount);
        $j('#surchargeAmount').text(CurrencyFormatted(calculatedSCharge, '', 0));
        $j(cbElm).val('true');
        $j('.surchargePerDesc').text('total');
        $j('#sumlabel').text('Total charge (payable today)');

    } else { //restore top deposit only            
        $j('#cardTypeDD').attr('disabled', '');
        $j('#paymentContainer').removeClass('PayFull');
        var dpstAmtWsrg = (currentSurcharge * vehicleCharges.depositAmount) + vehicleCharges.depositAmount;
        $j('#surchargeAmount').text(CurrencyFormatted(dpstAmtWsrg, '', 0));
        $j('.TotalDeposit, #totalPricesPanel .TotalDeposit').text(CurrencyFormatted(dpstAmtWsrg, '', 0));
        $j('.PickUpBalance, #totalPricesPanel .PickUpBalance').text(CurrencyFormatted(vehicleCharges.calculatedTotal - vehicleCharges.depositAmount, '', 0));
        $j("#totalDepositValue").val(dpstAmtWsrg);
        var calculatedSCharge = currentSurcharge * (vehicleCharges.depositAmount);
        $j('#surchargeAmount').text(CurrencyFormatted(calculatedSCharge, '', 0));
        $j(cbElm).val('false');
        $j('.surchargePerDesc').text('deposit');
        $j('#sumlabel').text('Deposit total (payable today)');
    }
}


function preloadPage(params) {
    var preloaderIframe = $j('<iframe src="Preloader.aspx?' + params + '" width="100%" height="100%" frameborder="0" class="preloaderIframe" />');
    $j('body').append(preloaderIframe);
    //force reload to avoid the browser caching of iframe content
    setCookie('history', 1, 0, '', '', false);
    preloaderIframe.attr('src', preloaderIframe.attr('src'));
    preloaderIframe.load(function () {
        $j('body form').hide();
        preloaderIframe.css('visibility', 'visible');
    })
}

function logMe(str, pty) { if (window.console && window.console.firebug /*&& logJS*/) console.log(str + " at " + pty); }

function setCookie(name, value, expires, path, domain, secure) {
    // set time, it's in milliseconds
    var today = new Date();
    today.setTime(today.getTime());

    /*
    if the expires variable is set, make the correct
    expires time, the current script below will set
    it for x number of days, to make it for hours,
    delete * 24, for minutes, delete * 60 * 24
    */
    if (expires) {
        expires = expires * 1000 * 60 * 60 * 24;
    }
    var expires_date = new Date(today.getTime() + (expires));
    document.cookie = name + "=" + escape(value) + ((expires) ? ";expires=" + expires_date.toGMTString() : "") + ((path) ? ";path=" + path : "") + ((domain) ? ";domain=" + domain : "") + ((secure) ? ";secure" : "");
}


// this fixes an issue with the old method, ambiguous values
// with this test document.cookie.indexOf( name + "=" );
function getCookie(check_name) {
    // first we'll split this cookie up into name/value pairs
    // note: document.cookie only returns name=value, not the other components
    var a_all_cookies = document.cookie.split(';');
    var a_temp_cookie = '';
    var cookie_name = '';
    var cookie_value = '';
    var b_cookie_found = false; // set boolean t/f default f

    for (i = 0; i < a_all_cookies.length; i++) {
        // now we'll split apart each name=value pair
        a_temp_cookie = a_all_cookies[i].split('=');


        // and trim left/right whitespace while we're at it
        cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

        // if the extracted name matches passed check_name
        if (cookie_name == check_name) {
            b_cookie_found = true;
            // we need to handle case where cookie has no value but exists (no = sign, that is):
            if (a_temp_cookie.length > 1) {
                cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g, ''));
            }
            // note that in cases where cookie is initialized but no value, null is returned
            return cookie_value;
            break;
        }
        a_temp_cookie = null;
        cookie_name = '';
    }
    if (!b_cookie_found) {
        return null;
    }
}

// this deletes the cookie when called
function deleteCookie(name, path, domain) {
    if (getCookie(name)) {
        setCookie(name, 0, -1, path, domain, true);
    }
}


function bindSpinner(options) {
    btn = $j('.' + options.btnClass);
    spinner = $j('.' + options.spinnerClass);

    var spinnerButtons = $j('.' + options.btnClass);
    var loadingTimer;
    var loadingFrame = 0;
    var animateLoading = function () {
        if (!spinner.is(':visible') && (spinner.css("display") != "inline")) {
            clearInterval(loadingTimer);
            return;
        }
        var imgHeight = 24;
        var frameCount = 8;

        spinner.css('background-image', "url(" + options.imgUrl + ")");
        spinner.css('background-position', "0 " + (loadingFrame * -imgHeight) + "px");
        spinner.css('width', imgHeight);

        loadingFrame = (loadingFrame + 1) % frameCount;
    };
    var showSpinner = function () {
        clearInterval(loadingTimer);
        spinner.show();
        loadingTimer = setInterval(animateLoading, 50);
    };
    spinnerButtons.click(function () {
        //spinnerButtons.hide();
        //btn.hide();
        showSpinner();
        return true;
    });
}

//added for sorting and filtering availability requests
var sortingBinded = false;
function bindIsoTope(catalogElm, ctrlElm, options) {
    if (catalogElm.length > 0) {
        catalogElm.isotope({
            animationEngine: 'jquery',
            getSortData: {
                size: function ($elem) {
                    return parseFloat($elem.find('.VehicleFeatures').attr('data-vsize'));
                },
                price: function ($elem) {
                    return parseFloat($elem.find('.VehicleFeatures').attr('data-vprice'));
                }
            }
        });
    }
    if (!sortingBinded) {
        ctrlElm.find('select').change(function (e) {
            var actionType = $j(this).hasClass('sort') ? 'Sort' : 'Filter';
            var sortParams = $j('#sortBy').val().split('_');
            var sortBy = sortParams[0];
            var isAsc = sortParams[1] == 'a';
            /*if (sortBy == "price") {
                $j('#filterBy').val('avail')
            }*/
            var filterBy = ($j('#filterBy').val() != "all" ? "." + $j('#filterBy').val() : "");

            _gaq.push(['_trackEvent', (requestParams.vt == 'av' ? 'Campervan' : 'Car'), actionType, sortBy + (isAsc ? " ascending" : " descending")])
            $j('#vehicleCatalog').isotope({
                animationEngine: 'jquery',
                getSortData: {
                    size: function ($elem) {
                        return parseFloat($elem.find('.VehicleFeatures').attr('data-vsize'));
                    },
                    price: function ($elem) {
                        return parseFloat($elem.find('.VehicleFeatures').attr('data-vprice'));
                    }
                },
                sortBy: sortBy,
                sortAscending: isAsc,
                filter: filterBy
            });
            return false;
        });
        sortingBinded = true;
    }
}

function getFieldsPrice(fieldIds) {//automated tester helper
    var sum = 0;
    for (var i = 0; i < fieldIds.length; i++) {
        $j(fieldIds[i]).each(function () {
            var cur = $j(this).text();
            cur = cur.replace(/[^0-9\.]+/g, '');
            sum += parseFloat(cur);
        });
    }
    return sum;
}