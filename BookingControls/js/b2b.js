﻿var xhrPool = [];
var availInProg = false;//loading data semaphore
var aggregator = true;

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}


function initCS(initParams) {    
    loadLocations();
    updateOpeningHours('nz','rv', ctrlParams.openHours);
    
    if (typeof rqstParams.vc != 'undefined' && rqstParams.vc.indexOf('.') > 0) rqstParams.vc = rqstParams.vc.split('.')[1];//normalize
    var valAdults = rqstParams.na;
    var valChildren = rqstParams.nc;
    PaxSelection(valAdults, valChildren)
   
    
    $('#sortPanel .btns label').click(function () {

            

        var elm = $(this);
        var parentSelector = elm.parent();
        while (!parentSelector.hasClass('selector')) parentSelector = parentSelector.parent();
        $(this).siblings().removeClass('selected');
        elm.addClass('selected');

        var actionType = parentSelector.attr('id').split('_')[0];
        var actionCol = parentSelector.attr('id').split('_')[1];
        if (actionType == "filter") {
            
            filterBy = '';
            $(".btns").children(".selected").each(function (index) {
                var tmpFilter = $(this).attr('data-filter').split(',').length;
                if (tmpFilter === 1) 
                    filterBy += $(this).attr('data-filter')
            }
           );
        }
        else {
            sortBy = actionCol;
            isAsc = (elm.attr('data-filter') == "asc");
        }
        
        var availability = $("#filter_availability span.btns label.selected").attr("data-filter");
        var sleepselected = $("#filter_sleeps span.btns label.selected").attr("data-filter");
        var toiletselected = $("#filter_toilet span.btns label.selected").attr("data-filter");
        var wheelselected = $("#filter_wheel span.btns label.selected").attr("data-filter");

        if (sleepselected.indexOf('.s') != -1)
        {
            var startwith = sleepselected.charAt(sleepselected.length - 1);
            startwith = parseInt(startwith) + 1
            for(startwith; startwith <= 6; startwith++)
            {
                if (toiletselected.indexOf(',') != -1) toiletselected = '';
                if (wheelselected.indexOf(',') != -1) wheelselected = '';

                filterBy += ",.s" + startwith + toiletselected + wheelselected + availability;
            }
        }
       
        //console.log(filterBy);
        isoTope();
    });
    

    $('#countryOfTravel').val(rqst.params.cc);

    if(rqst.params.cur)
        $('#currencySelector').val(rqst.params.cur.toUpperCase());
    
    //combo bind:
    if(domain.agg)
        $(".combo").combobox({change: function( event, ui ) { } });
    else
        $('#bookingControl select, #sortSelector').change(function(ev) { ddChangeOnSelect(this); });//ui is the element
    
    
    //datepicker
    initDatePicker(rqst.params);  
        
    $('#vehicleInfoSelector span').click(function(e){
        $('#vehicleInfoSelector span').removeClass('active');
        $(this).addClass('active');
        if($(this).hasClass('show'))
            $('#vehicleCatalog .vehicle').removeClass('col');
        else
            $('#vehicleCatalog .vehicle').addClass('col')
        isoTope();
    })

    $('#bookingControl .ui-combobox-input, #currencyPanel .ui-combobox-input').focus(function() { $(this).select(); });

    //only relevant brands
    $('#vehicleCatalogHead .logos li span').each(function(i, el) {
        var elm = $(el);
        if (!initParams.request.includedBrand(elm.attr('rel')))
            elm.css('display','none');
    });
    $('#vehicleCatalog .brand').each(function(i, el) {
        var elm = $(el);
        if (!initParams.request.includedBrand(elm.attr('id').split('_')[0]))
            elm.css('display','none');
    });
    //only relevant brands ends
    rqst.updateLabels($('#pickUpTxt'), $('#dropOffTxt'));
    initParams.request.query();//request
    $('.vehicle .status, .vehicle .spc, .vehicle .prc').tooltip({content: function() { return "<span>" + $(this).attr('title') + "</span>" ;}});    
    
    $('.vehicle').click(function(ev) { //handle click for collapse
        if(!aggregator) return;
        var elm = $(this);
        elm.toggleClass('col');
        isoTope();   
     });

     $('#bookingControl .head').click(function(ev) { toggleCtrlView();});
     $('#sortPanel .header .left small').click(function(ev) { toggleSort();});

     $('.vehicle span.inf').click(function(e) {
        var elm = $(this);
        if(!elm.parent('.vehicle').hasClass('col')) {
            e.stopPropagation();
            popUp(elm.attr('rel'));        
        }
     });

    $('.dateTimeRowOptions i').click(function(ev) { 
        $(this).parent().find('input').datepicker('show');
    });
    
    //bind sorting
    //sortChange("sleeps_asc");
    $('#selectorPanel .ui-combobox-input').click(function(e){
        $('#selectorPanel .ui-button').trigger('click');    
    });
    
    if(!domain.agg) {
        var brandName = brandForChar(domain.brand);
        brandName = brandName.charAt(0).toUpperCase() + brandName.slice(1);
        $('#csellSelect option[value="current"]').text(brandName);
    
        $('.vprice .head .right').click(function(e) {//expand price
            e.stopPropagation();
            var el = $(this);
            var row = el.closest('.vprice').toggleClass('expand');
            isoTope();
        });

        $('#currencySelector').change(function(e) {
            var elm = $(this);
            var rate = elm.find('option:selected').attr('rel')
            forex(elm.val(), rate);
            if(rate != 1)
                $('#vehicleCatalog').addClass('forex');
            else 
                $('#vehicleCatalog').removeClass('forex');
            //rqst.params.cur = val.toLowerCase();            
        });

       

        $('.vfoot .alt a').click(function loadAltAvail(e) {
            var elm = $(this);
            vcode = elm.parents('div.element').attr('id').split('_')[1];
            if(rqst) 
            {
                var url = rqst.getAltAvailURL(vcode);
                window.location = url;
            }
        });

        //mobile menus
        $('.contactUs.mobileMenu').click(function() {
            $('ul.steps').slideToggle();
            $(this).find('span.glyphicon').toggleClass('glyphicon-remove glyphicon-align-justify');
            $(this).find('i').toggleClass('icon-minus icon-align-justify');
        });
                
        $("#csellSelect").change(function() {
            if($(this).val() == "all") {
                        $(this).parent().addClass('allbrands');
            } else {
                        $(this).parent().removeClass('allbrands');
            }
        });
        //mobile menus end

    }

    //gross nett start
    // rev:mia may 13, 2014 - injection of code to display Gross  and NET AMOUNT
    $('[name=grsnett]').click(function (e) {
        
        var elm = $(this);
        var url = document.location.href;
        url = url.split('IsGross')[0];
        url = url + "&IsGross=" + ((elm.val() == 'gross') ? "True" : "False");

        $("#availabilityFilter input, #availabilityFilter select").attr("disabled", true);

        window.location= url;
    })
    
    //gross nett
    
    // rev:mia may 13, 2014 - injection of code to display Gross  and NET AMOUNT
    $('#currencySelectorb2b').change(function (e) {
        

        var elm = $(this);
        var rate = elm.find('option:selected').attr('rel')
        
        forexb2b(elm.val(), rate);
        if (rate != 1)
            $('#vehicleCatalog').addClass('forex');
        else
            $('#vehicleCatalog').removeClass('forex');
        //rqst.params.cur = val.toLowerCase();            
    });


    
    
}

function sortChange(sortVal){
    var sortParams = sortVal.split('_');
    sortBy = sortParams[0];
    isAsc = sortParams[1] == "asc";
    //alert(sortBy + ":" +  isAsc )
    isoTope();
}

function toggleCtrlView(collapse) {//pass collapse <true> to force close or <false> to reopen    
    var ctrl = $('#bookingControl');
    if(collapse) ctrl.removeClass('col');        
    if(ctrl.hasClass('col')) {         
        $('#bookingControl .body').show('fast');            
        $('#bookingControl .head .close small').text('Close Search');
    } else { 
        $('#bookingControl .head .close small').text('Update Search');
        $('#bookingControl .body').hide('fast');        
    }
    ctrl.toggleClass('col');    
}

function toggleSort(){
    var panel = $('#sortPanel');
    var stateTxt = $('#sortPanel .left small em');
    if(panel.hasClass('col')) {
        panel.find('.body').show('fast');
        stateTxt.text('Hide');
    } else {
        panel.find('.body').hide('fast');
        stateTxt.text('Show');
    }
    panel.toggleClass('col');
}

var initiailRequest = true;

//load from browser cache
function updateRequest(pageParams, refresh) {
    
    if($.jStorage.get("thlRqst") != null && $.jStorage.get("thlRqst").cc != pageParams.cc)//MaC reload on diff contry
        $.jStorage.set("thlRqst", pageParams, {TTL: 12000000});//reset lstorage
	    
    if(initiailRequest && $.jStorage.get("thlRqst") != null && $.jStorage.get("thlState") == "configure") {  //back btn
       var pParams = $.jStorage.get("thlRqst");
        $.jStorage.set("thlState","select");
        //console.log('back case');
        initiailRequest = false;        
        //$.jStorage.set("thlRqst", pageParams, {TTL: 12000000});
        return  pParams;       
    } else { //load from url         
        //console.log('GET case');
        initiailRequest = false;
        $.jStorage.set("thlRqst", pageParams, {TTL: 12000000});
        return pageParams;
    }  
}

function loadLocations(){
    var cc = rqst.params.cc
    $(ctrlParams.branches).each(function(i, el) {
        if ((el.vt == "campervan") && el.cc == cc) {
            $('select.location').append($('<option>', {
                value: el.code.toLowerCase()
            }).text(el.name));
        }
    });
    $('#pickUpSelector').val(initParams.pb);
    $('#dropOffSelector').val(initParams.db);   
    $('#dlSelector').val(initParams.cr);
    $('#adultSelector').val(initParams.na);
    $('#childSelector').val(initParams.nc);
}

function isoTope() {     
    $('.element').each(function(i,el) { 
        var elm = $(el);       
        elm.removeClass('hideElm');                 
    });    
    
    //TODO: check for first timer
    
    $('#vehicleCatalog .brand').isotope('reloadItems').isotope({
        animationEngine: 'css',
        transformsEnabled: false,
        getSortData: {
           sleeps: function ($elem) {
               if ($elem.is('.emptyVehicle')) {
                    return -1;
                }
                return $elem.attr('data-sleeps');
            },
            price: function ($elem) {
                if ($elem.is('.emptyVehicle')) {
                    return -1;
                }
                return parseFloat($elem.attr('data-price'));
            },
            wheel: function ($elem) {
                if ($elem.is('.emptyVehicle')) {
                    return -1;
                }
                return $elem.attr('data-wdr');
            },
            length: function ($elem) {
                if ($elem.is('.emptyVehicle')) {
                    return -1;
                }
                return parseFloat($elem.attr('data-length'));
            },
        },
        sortBy: sortBy, 
        sortAscending: isAsc,
        filter: filterBy
    });
    
    $('.element').each(function(i,el) { 
        var elm = $(el);
        if(elm.css('opacity') == '0') elm.addClass('hideElm');
        else elm.removeClass('hideElm');                 
    });
    
}

function forex(cur, rate){           
    //console.log("forex says: cur:"+ cur + ", rate:" + rate); 
    if(rqst.params.cc == "au") {
        var factor = $("#currencySelector option[value='AUD']").attr('rel');
        factor = rate / factor;        
        rate = factor;
    }

    $("#vehicleCatalog .forex .cur").text(cur.toUpperCase());
    $("#vehicleCatalog .forex .prc").each(function (i, el) {
        var elm = $(el);
        var raw = elm.attr('data-base');
        var baseCur = elm.attr('data-cur');
        if(raw>0 && cur != baseCur)
            elm.text( formatMoney(raw*rate));
        else if (cur == baseCur && raw>0)
            elm.text(formatMoney(raw));
    });
}


// rev:mia may 13, 2014 - injection of code to display Gross  and NET AMOUNT
function HasGrossInUrl()
{
    return (url.indexOf("IsGross") != -1 ? true : false)
}

function DefaultToAlternativeCurrency(data_pickup, other_currency, element) {

    var altCurrencyValue;
    other_currency = other_currency.toUpperCase();

    if (other_currency.indexOf("NOT") == -1 && other_currency.indexOf("UNAVAILABLE") == -1)
    {
        altCurrencyValue = other_currency.split(" ")[0].replace("$", "").replace(",", "")
        //altCurrencyValue = "$" + formatMoney(altCurrencyValue) + " (agent)<br />$" + formatMoney(data_pickup) + " (pickup)"
        altCurrencyValue = "$" + formatMoney(altCurrencyValue) + " <br />$" + formatMoney(data_pickup) + " (pickup)"
        element.html(altCurrencyValue);
    }
}

function forexb2b(cur, rate) {

    $("#vehicleCatalog .cur").text(cur.toUpperCase());
    $("#vehicleCatalog .prc").each(function (i, el) {
        var destinationCurrency = "<span style='font-size:0.8em;margin-bottom: 0px;text-transform: capitalize;'>" + window.location.href.substring(window.location.href.indexOf("/b2b/")).substr(5, 2)  + "</span>"

        var elm = $(el);
        var raw = elm.attr('data-agent')//elm.attr('data-base');
        var dataAgent = elm.attr('data-agent'); 
        var baseCur = elm.attr('data-cur');
        var dataPickup = elm.attr('data-pickup');
        var cangetgross = elm.attr('data-cangetgross');
        var packagecode = elm.attr('data-package');
        
        var display = (cangetgross == "Yes" || cangetgross == "No" ? true : false);
        if (cangetgross == undefined) {
            cangetgross = "No";
            display = true;
        }

        var result = "0.00";
        cur = cur.toUpperCase();
        baseCur = baseCur.toUpperCase();
        //var vehicle = elm.closest('div').find('h4').text()
        //console.log(vehicle + ', raw: ' + raw + ', dataAgent: ' + dataAgent + ', cur: ' + cur + ', baseCur: ' + baseCur + ', dataPickup: ' + dataPickup + ', cangetgross: ' + cangetgross + ', packagecode: ' + packagecode)

        

        if (raw > 0 && cur != baseCur) {
            result = formatMoney(raw * rate);
        }
        else if (cur == baseCur && raw > 0) {
            result = formatMoney(raw);
        }
        else {
            display = false;
        }

        //console.log("forexb2b cur : " + cur + ", basecur : " + baseCur + ", raw :" + raw + ", resul: " + result);
        if (display == true) {
            
            var currentSym = $("#currencySelectorb2b option:selected").val();
            if (currentSym == undefined) {
                currentSym = cur;
                if (currentSym == undefined) {
                    currentSym = destinationCurrency
                }
            }
            currentSym = curSymForCode(currentSym.toUpperCase());

            

            //cur = gbp  to baseCur = aud
            var curtemp = $("#currencySelectorb2b option[value != '" + cur + "']").eq(0).val();
            if (raw > 0 && cur != baseCur) {
                if ($("#currencySelectorb2b option").length > 1) {
                    baseCur = curtemp;
                }
                elm.attr('title', formatMoney(raw) + " " + baseCur + " " + packagecode);
            }
            else if (cur == baseCur && raw > 0) {
                if ($("#currencySelectorb2b option").length > 1) {
                    var ratetemp = $("#currencySelectorb2b option[value != '" + cur + "']").eq(0).attr("rel");
                    elm.attr('title', formatMoney(raw * ratetemp) + " " + curtemp + " " + packagecode);
                }
                else {
                    elm.attr('title', formatMoney(raw * rate) + " " + cur + " " + packagecode);
                }
                destinationCurrency = "";
            }

            if (cangetgross == "Yes") {
                //elm.html("<span class=''> " + currentSym + result + " (agent)</span><br />" + destinationCurrency + "$" + formatMoney(dataPickup) + " (supplier)");
                elm.html("<span class=''> " + currentSym + result + " </span><br />");
            }
            else if (cangetgross == "No") {
                //elm.html("<span class='bluehighlight'> " + currentSym + result + " (agent)</span><br />" + destinationCurrency + "$" + formatMoney(dataPickup) + " (supplier)");
                elm.html("<span class='bluehighlight'> " + currentSym + result + " </span><br />");
            }

        }

        
    });
   // rqst.query();
}
//rev:mia end


function tgl(el) {
    var elm = $(el);
    var parentSelector = elm.parent();
    while (!parentSelector.hasClass('vehicle')) parentSelector = parentSelector.parent();
    parentSelector.toggleClass('col');
    isoTope();
}

function beforeShowDayFunc(date) {
    var selected = ($("#pickUpTimeTxt").datepicker('getDate') < date);
    return [selected, ''];
}

function initDatePicker() {
    var targetFrom = "pickUpTimeTxt";
    rqst.updateDatePicker($('#pickUpTimeTxt'), $('#dropOffTimeTxt'));
    
    pdDatePicker = $('#pickUpTimeTxt').datepicker({
        dateFormat: 'M dd, yy',
        //beforeShowDay: function(){} ,
        numberOfMonths: 1,
        changeMonth: true,
        changeYear: false,
        minDate: new Date(),
        onSelect: function (dateText, inst) {
            var month = parseInt(inst.selectedMonth,10) + 1;
            rqst.params.pd = (inst.selectedDay < 10 ? "0" : "") + inst.selectedDay + "-" + (month < 10 ? "0" : "") + month + "-" + inst.selectedYear;
            setDateFromDatePicker($('#pickUpTimeTxt'), dateText, inst);
            //rqst.updateLabels($('#pickUpTxt'), $('#dropOffTxt'));
        },
        onChangeMonthYear: function (year, month, inst) {
            //changeMonthYear($(this), year, month, inst, $('#pickUpTimeTxt'));
        },
        onClose: function (dateText, inst) {
            if (Date.parse($("#dropOffTimeTxt").val()) < Date.parse($("#pickUpTimeTxt").val())) {                
                $("#dropOffTimeTxt").val($("#pickUpTimeTxt").val());
                var drop = $("#dropOffTimeTxt").datepicker('getDate');
                rqst.params.dd = drop.getDate() + "-" + (drop.getMonth() < 10 ? "0" : "")  + (drop.getMonth()+1) + "-" + drop.getFullYear();
            }
        }
    });
    ddDatePicker = $("#dropOffTimeTxt").datepicker({
        beforeShowDay: beforeShowDayFunc,
        dateFormat: 'M dd, yy',
        numberOfMonths: 1,
        minDate: new Date(),
        changeMonth: true,
        changeYear: false,
        onSelect: function (dateText, inst) {
            var month = parseInt(inst.selectedMonth,10) + 1;
            setDateFromDatePicker($('#dropOffTimeTxt'), dateText, inst);
            rqst.params.dd = (inst.selectedDay < 10 ? "0" : "") + inst.selectedDay + "-" + (month < 10 ? "0" : "") + month + "-" + inst.selectedYear;            
        },
        onChangeMonthYear: function (year, month, inst) {
            //changeMonthYear($(this), year, month, inst, $('#dropOffTimeTxt'));
        }
    });
}

function changeMonthYear(calendar, year, month, inst, hiddenObj) {
    var oldDate = calendar.datepicker("getDate");
    if (oldDate != null) {
        var newDate = new Date(year, month - 1, oldDate.getDate());
        if (newDate.getDate() != oldDate.getDate()) {
            var lastDay = 32 - new Date(year, month - 1, 32).getDate();
            newDate = new Date(year, month - 1, lastDay);
        }
        calendar.datepicker("setDate", newDate);
        setDateFromDatePicker(hiddenObj, inst.input.val(), inst);
    }
}

function setDateFromDatePicker(hiddenObj, dateText, inst) {
    $(hiddenObj).val(dateText);
    setDate(inst);    
}

function setDate(inst) {
    var toDate = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
	var nextDay = new Date(toDate.getTime() + 1 * 86400000);
    $('#dropOffTxt').datepicker("option", "minDate", nextDay);
}

function submitSearch() {    
    
    if((rqst.params.cr == '' || rqst.params.cr == -1) && !domain.agg){
        $('#dlSelector').addClass('req');
        return;
    }
    
    if($('body').hasClass('promo')){
        $('body').removeClass('promo');
        isoTope();
    }
    $('#vehicleCatalog .brand .vehicle').addClass('loading');
    updateRequest(rqst.params,true);//update local cache
    rqst.updateLabels($('#pickUpTxt'), $('#dropOffTxt'));
    
    if($('#csellSelect').val() == 'all') {
        window.location = rqst.getCrossSellUrl();
        return;
    }
    
    rqst.query();
    toggleCtrlView(false);
    isoTope();
}

function showDiscTable(elm) {
    var elm = $(elm);
    var priceDetailsDiv = elm.parents('.PriceDetailsList')[0]; 
    $(priceDetailsDiv).find('.discountTable').removeClass('hide');
    elm.remove();
}

//native no combo
function ddChangeOnSelect(elm)
{
    var elm = $(elm);
    ddId = $(elm).attr('id');
    var val = elm.val().toLowerCase()
    $('.req').removeClass('req');
    switch(ddId)
    {
        case 'dlSelector':
            rqst.params.cr = val;
            break;
        case 'pickUpSelector':
            rqst.params.pb = val;
            rqst.params.pbLbl = $("#"+ ddId +" option:selected").text();
            break;
        case 'dropOffSelector':
            rqst.params.db = val;
            rqst.params.dbLbl = $("#"+ ddId +" option:selected").text()
            break;
        case 'pickTimeSelector':
            rqst.params.pt = val;
            break;
        case 'dropTimeSelector':
            rqst.params.dt = val;
            break;
        case 'adultSelector':
            rqst.params.na = val;
            PaxSelection(val, $("#childSelector").val())
            break;
        case 'childSelector':
            rqst.params.nc = val;
            PaxSelection($("#adultSelector").val(), val);
            break;
        case 'currencySelector':
            forex(val, $("#"+ ddId +" option:selected").attr('rel'));
            rqst.params.cur = val.toLowerCase();
            break;
        case 'sortSelector':
            sortChange(val);
            break;
        case 'countryOfTravel':
            window.location = "/" + val + "/select" + (rqst.params.ac != null ? "?ac="+ rqst.params.ac : "") ;
            break;
    }
}

function ddChange(event, ui) {//handle the combo event on thl cols
    var elm = $(ui.item.option.parentElement);
    ddId = $(elm).attr('id');
    var val = elm.val().toLowerCase()
    $('.req').removeClass('req');
    switch(ddId)
    {
        case 'dlSelector':
            rqst.params.cr = val;
            break;
        case 'pickUpSelector':
            rqst.params.pb = val;
            rqst.params.pbLbl = $("#"+ ddId +" option:selected").text();
            break;
        case 'dropOffSelector':
            rqst.params.db = val;
            rqst.params.dbLbl = $("#"+ ddId +" option:selected").text()
            break;
        case 'pickTimeSelector':
            rqst.params.pt = val;
            break;
        case 'dropTimeSelector':
            rqst.params.dt = val;
            break;
        case 'adultSelector':
            rqst.params.na = val;
            PaxSelection(val, $("#childSelector").val())
            break;
        case 'childSelector':
            rqst.params.nc = val;
            PaxSelection($("#adultSelector").val(), val);
            break;
        case 'currencySelector':
            forex(val, $("#"+ ddId +" option:selected").attr('rel'));
            rqst.params.cur = val.toLowerCase();
            break;
        case 'sortSelector':
            sortChange(val);
            break;
        case 'countryOfTravel':
            window.location = "/" + val + "/select" + (rqst.params.ac != null ? "?ac="+ rqst.params.ac : "") ;
            break;
    }
    //rqst.updateLabels($('#pickUpTxt'), $('#dropOffTxt'));
}

function updateOpeningHours(cCode, vType, openHours) {    
    cCode = cCode.toLowerCase();
    vType = (vType == "rv" || vType == "av") ? "av" : "ac";
    var selectedPt = rqst.params.pt, selectedDt = rqst.params.dt;
    $('select.timeSelector').each(function () {
        
        var dd = $(this);
        for (i = 0; i < openHours.length; i++) {
            if (openHours[i].vt == vType && openHours[i].cc == cCode) {
                var html = '';
                var defaultParams = openHours[i].df.split('-');
                for (j = openHours[i].pt; j <= openHours[i].dt; j++) {
                    //if(!(j==7 && vType == "av"))
                    html += "<option value='" + (j < 10 ? "0" : "") + j + "00' " + ((j == 10 && dd.attr('id') == "pickTimeSelector") || (j == 15 && dd.attr('id') == "dropTimeSelector")  ? "selected='selected'" : "") + "   >" + (j < 10 ? "0" : "") + j + ":00 " + (j < 12 ? "AM" : "PM") + "</option>";
                    if(!(j==17 && vType == "av"))
                        html += "<option value='" + (j < 10 ? "0" : "") + j + "30'>" + (j < 10 ? "0" : "") + j + ":30 " + (j < 12 ? "AM" : "PM") + "</option>";
                }
                dd.html(html);
            }
        }
        
    })
    $('#pickTimeSelector').val(selectedPt);
    $('#dropTimeSelector').val(selectedDt);
}

/* Request Object */
function B2CRequest(params) { this.params = params; }

B2CRequest.prototype.toString = function () {
    var results = "params:";
    for (var key in this.params) {
        var obj = this.params[key];
        results +=  key + " = " + obj + ",";
    }
    return results;
} 

B2CRequest.prototype.getRequestUrl = function(queryBrand) {
    var reqParams   = this.params;
    var query       = getUrlVars();
    var cur         = query['cur']; 

    //rev:mia may 19, 2014 - injection of code to display Gross  and NET AMOUNT 
    var hasIsGrossInUrl = document.location.href.indexOf('IsGross') != -1;
    var url = "/availability/" + reqParams.cc + "/" + queryBrand + "/" + reqParams.pb + "/" + reqParams.pt + "-" + reqParams.pd + "/" + reqParams.db + "/" + reqParams.dt + "-" + reqParams.dd + "/" + "na-" + reqParams.na + "-nc-" + reqParams.nc + "-ac-" + reqParams.ac + (reqParams.cr != -1 && reqParams.cr != 0 ? "-cr-" + reqParams.cr : "") + (typeof reqParams.pc != "undefined" && reqParams.pc != "" ? "-pc-" + reqParams.pc : "") + "-isGross-"// + (document.location.href.indexOf('IsGross=True') != -1 ? 'true' : 'false');
    if (hasIsGrossInUrl == true)
        url = url + (document.location.href.indexOf('IsGross=True') != -1 ? 'true' : 'false');
    else {
        var grDefOn = query['grDefOn'];
        url = url + (grDefOn == 'false' ? 'false' : 'true');
    }

    //alert(url)
    return url;
}

B2CRequest.prototype.getCrossSellUrl = function() {
    var pickDayParams = this.params.pd.split('-');
    var dropDayParams = this.params.dd.split('-'); 
    var reqParams = this.params;
    //debugger;
    var url = "https://secure.motorhomesandcars.com/" + reqParams.cc + "/select?utm_campaign=CrossLink&sc=rv&vtype=rv&pc=&na="+ reqParams.na +"&nc="+ reqParams.nc +"&brand=&cc=" + reqParams.cc + "&pb=" + reqParams.pb + "&pd=" + pickDayParams[0] + "&pm=" + pickDayParams[1] + "&py=" + pickDayParams[2] + "&pt="+ this.formatTime(this.params.pt) +"&db=" + reqParams.db + "&dd="+ dropDayParams[0] +"&dm="+ dropDayParams[1] +"&dy="+ dropDayParams[2] +"&dt="+ this.formatTime(this.params.dt) +"&cr=" + this.params.cr +"&vh=-1&pv=1.0";
    return url;
}


B2CRequest.prototype.getMinPax = function() {
    return parseInt(this.params.na) + parseInt(this.params.nc);
}

B2CRequest.prototype.getContactLnk = function() {
    var lnk;
    if(document.domain.indexOf('motorhomesandcars') > -1)//no conventions
        lnk = "http://www.motorhomesandcars.com/Pages/contact-us.aspx"
    else if (document.domain.indexOf('keacampers') > -1)
        lnk = "http://aurentals.keacampers.com/en/contact.aspx";
    else lnk = "http://www." + document.domain.split(".").slice(1).join(".") + "/contact-us";
    return lnk;
}

B2CRequest.prototype.getConfigureURL = function(vCode) {
    var pickDayParams = this.params.pd.split('-');
    var dropDayParams = this.params.dd.split('-');
    //rev:mia 26June2014 B2BMAC
    var isGross = ($("#gnGross").prop('checked') == true ? 'true' : 'false');
    var url = "/Selection.aspx?cc=" + this.params.cc.toUpperCase() + "&brand=&sc=rv&utm_campaign=CrossLink&vtype=rv&pc=&na=" + this.params.na + "&nc=" + this.params.nc + "&cr=" + this.params.cr + "&pb=" + this.params.pb + "&pd=" + pickDayParams[0] + "&pm=" + pickDayParams[1] + "&py=" + pickDayParams[2] + "&pt=" + this.formatTime(this.params.pt) + "&db=" + this.params.db + "&dd=" + dropDayParams[0] + "&dm=" + dropDayParams[1] + "&dy=" + dropDayParams[2] + "&dt=" + this.formatTime(this.params.dt) + "&vh=" + vCode + "&ac=" + this.params.ac +  "&isGross=" + isGross + "&pv=1.0";
    return url;
}

B2CRequest.prototype.getAltAvailURL = function(vCode) {
    brand = domain.brand;//todo: refactor
    return "/preloader.aspx?vCode="+ vCode +"&rType=GetAlternateOptions&Brand=" + brand;
}

B2CRequest.prototype.formatTime = function(timeStr) {
    return timeStr.substring(0,2) + ":" + timeStr.substring(2,5);
}

B2CRequest.prototype.getCharForBrand = function(name){
    var brand = '';
    switch(name)
    {
        case 'kea':
            brand = 'q';
            break;
        case 'maui':
            brand = 'm';
            break;
        case 'united':
            brand = 'u';
            break;
        case 'britz':
            brand = 'b';
            break;
        case 'alpha':
            brand = 'a';
            break;
        case 'mighty':
            brand = 'y';
            break;
    }
    return brand;
}


B2CRequest.prototype.updateLabels = function(pickLbl, dropLbl) {
    
    //if(this.params.pbLbl == "No Address" || this.params.dbLbl == "No Address"){
        rqst.params.pbLbl = $("#pickUpSelector option:selected").text();
        rqst.params.dbLbl = $("#dropOffSelector option:selected").text()
    //}//TODO: fix at source   
    
    var p = new Date(); var d = new Date();
    var pdParams = this.params.pd.split('-');
    var ddParams = this.params.dd.split('-');
    p.setFullYear(pdParams[2], pdParams[1]-1, pdParams[0]);
    d.setFullYear(ddParams[2], ddParams[1]-1, ddParams[0]);
    var weekday=new Array(7);//lang support here
    weekday[0]="Sunday";
    weekday[1]="Monday";
    weekday[2]="Tuesday";
    weekday[3]="Wednesday";
    weekday[4]="Thursday";
    weekday[5]="Friday";
    weekday[6]="Saturday";
    pickLbl.html(this.params.pbLbl + " - " + p.toDateString() + " at " + this.formatTime(this.params.pt));
    dropLbl.html(this.params.dbLbl + " - " + d.toDateString() + " at " + this.formatTime(this.params.dt));
    this.hirePeriod = Math.round((d.getTime() - p.getTime())/86400000);
    $('#travelPeriod').html((this.hirePeriod +1)+ " days");
}

B2CRequest.prototype.updateDatePicker = function(pickTxt, dropTxt) {
    var p = new Date(); var d = new Date()
    var pdParams = this.params.pd.split('-');
    var ddParams = this.params.dd.split('-');
    p.setFullYear(pdParams[2], pdParams[1]-1, pdParams[0]);
    d.setFullYear(ddParams[2], ddParams[1]-1, ddParams[0]);
    var month= new Array(7);//lang support here
    month[0]="Jan";
    month[1]="Feb";
    month[2]="Mar";
    month[3]="Apr";
    month[4]="May";
    month[5]="Jun";
    month[6]="Jul";
    month[7]="Aug";
    month[8]="Sep";
    month[9]="Oct";
    month[10]="Nov";
    month[11]="Dec";
    pickTxt.val(month[p.getMonth()] + " " + p.getDate() + ", " + p.getUTCFullYear());
    dropTxt.val(month[d.getMonth()] + " " + d.getDate() + ", " + p.getUTCFullYear());
}

B2CRequest.prototype.includedBrand = function(brand) {
    for(var i=0; i< this.params.queryBrands.length; i++)
        if(this.params.queryBrands[i] == brand) return true;
    return false;
}


function contactUs() {
    var lnk = rqst.getContactLnk();
    document.location = lnk;
}

var loadCount = 0;

// rev:mia may 13, 2014 - injection of code to display Gross  and NET AMOUNT
function b2bRedirect(data) {
    var elm = $('.book .update');
    elm.removeClass('update');
    if (data.avp) {
        var query = getUrlVars();
        var cur = query['cur'];
        var rate = $('#currencySelectorb2b option:selected').attr("rel");
        var currate = $('#currencySelectorb2b option:selected').val();
        var def = window.location.href.substring(window.location.href.indexOf("/b2b/")).substr(5, 2) + "D"
        var curOff = "";
        if (rate == undefined)
        {
            rate = 1.0
            curOff = "&curOff=" + def
        }
        if (currate == undefined) currate = def
        if (cur != currate)   cur = currate;

        var location = agt.base + '/content/configure.aspx?enc=0&avp=' + data.avp;
        location = location + "&rate=" + rate;
        location = location + "&cur=" + cur;
        var json = query['json'];
        if (json != undefined) {
            var b2cparam = window.location.href.substring(window.location.href.indexOf("/b2b/") );
            location += "&b2bAll=true&b2bqry=" + b2cparam
        }
        //console.log(location);
        //var url = (window.location != window.parent.location) ? document.referrer : document.location;
       
        //window.parent.location = location + curOff;
        top.location = location + curOff
    }
    else {
        switch (data.msg) {
            case "Session Expired":
                alert("Your login session has expired please log back in.");
                //window.parent.location = agt.base + "/content/agentLogin.aspx";
                top.location = agt.base + "/content/agentLogin.aspx";
                break;
            case "Error":
                alert("unexpected error, please log back in.");
                //window.parent.location = agt.base + "/content/agentLogin.aspx";

                break;
        }
    }
}

B2CRequest.prototype.query = function () {
    // $("#availabilityFilter input, #availabilityFilter select").attr("disabled", true);
    $("#availabilityFilter").attr("disabled", true);
    $("#spanPriceHighlight").text("");
    xhrPool = []; availInProg = true;
    var queryBrands = domain.agg ? this.params.queryBrands : [brandForChar(domain.brand)];
    var baseCurTemp = "";
    var destinationCurrency = "<span style='font-size:0.8em;margin-bottom: 0px;text-transform: capitalize;>" + window.location.href.substring(window.location.href.indexOf("/b2b/")).substr(5, 2) +  "</span>$"
    aggregator = domain.agg;
    loadCount = queryBrands.length;
    var hasAvailable = 0;
    for(var i=0; i < queryBrands.length; i++)
    {
       
        var options = {
            url: this.getRequestUrl(queryBrands[i]) ,
            data: "{'type':'json'}",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'post',
            beforeSend: function () { /* */ },
            success: function (json) {
                loadCount--;
                var bookables = 0;
                var items = eval(json.items);
                for (var i = 0; i < items.length; i++) {
                    
                    
                    var elm = $('#v_' + items[i].code);
                    //rev:mia 19may2015 - if box has a data-price then dont overwrite it.
                    if (elm.attr("data-price") != '999999') {
                        //debugger;
                        //console.log("DUPLICATE with price " + items[i].code + '--' + items[i].pkg + '--' + items[i].avp)
                        continue
                    }
                    //console.log("with price " + items[i].code + '--' + items[i].pkg + '--' + items[i].avp)

                    var priceNode = elm.find('.prc');

                    //rev:mia june 9 2014 - B2BMAC
                    var cangetgross = items[i].cangetgross;
                    

                    var thumbImg = elm.find('.tmb img');
                    elm.find('.vehicle').removeClass('loading');
                    if (items[i].price > 0) {
                        hasAvailable++;
                        //console.log("with price " + items[i].code + '--' + items[i].pkg)
                        //if (items[i].code == "4BBT") debugger;
                        //if (items[i].code == "6BBT") debugger;
                        bookables++;
                        
                        //var vehicle = elm.closest('div').find('h4').text()
                       // console.log(vehicle)

                        var currentSym = $("#currencySelectorb2b option:selected").val();
                        if (currentSym == undefined) {
                            currentSym = priceNode.attr('data-cur');
                            if (currentSym == undefined) {
                                currentSym = window.location.href.substring(window.location.href.indexOf("/b2b/")).substr(5, 2) + "D"
                            }
                        }
                        currentSym = curSymForCode(currentSym.toUpperCase());


                        ////rev:mia june 9 2014 - B2BMAC
                        //if (cangetgross == "Yes") {
                        //    priceNode.html("<span class=''> " + currentSym + formatMoney(items[i].agent) + " (agent)</span><br />" + destinationCurrency + formatMoney(items[i].pickup) + " (supplier)");
                        //}
                        //else
                        //    priceNode.html("<span class='bluehighlight'> " + currentSym + formatMoney(items[i].agent) + " (agent)</span><br />" + destinationCurrency + formatMoney(items[i].pickup) + " (supplier)");
                            

                        priceNode.attr('data-agent', items[i].agent);
                        priceNode.attr('data-pickup', items[i].pickup);
                        //priceNode.attr('title', formatMoney(items[i].price / agt.rt) + " " + agt.cur);
                        //rev:mia june 9 2014 - B2BMAC
                        priceNode.attr('data-cangetgross', cangetgross);
                        
                        //console.log('data-cangetgross: ' + cangetgross + " , code : " + items[i].code);
                        priceNode.attr('data-package', items[i].pkg);

                        thumbImg.attr('title', items[i].pkg);
                        
                        priceNode.attr('data-base',items[i].price.toFixed(2));                        
                        elm.addClass('avail');
                        elm.removeClass('noavail');
                        elm.attr('data-price', items[i].price);
                        elm.attr('rel', items[i].price);  
                        elm.find('.det').html(items[i].bands);   
                        var bookLink = elm.find('.book');
                        bookLink.attr('data-avp', items[i].avp);

                       
                        

                        //rev:mia june 19 2014 - B2BMAC - today in history: Birthday of Jose P. Rizal, Philippines National Hero
                        var raw = items[i].price.toFixed(2);
                        var baseCur = priceNode.attr('data-cur');
                        var cur = $("#currencySelectorb2b option:selected").val();
                       
                        try
                        {
                            baseCur = baseCur.toUpperCase();
                            if (cur != undefined)
                                cur = cur.toUpperCase();
                            else {
                                cur = baseCur
                                baseCurTemp = baseCur
                            }
                        }
                        catch (error) {
                            //console.log("ERROR title: " + items[i].pkg + ", price :" + items[i].price + " , code: " + items[i].code + ", rate: " + agt.rt + ", base:  " + baseCur + ", cur: " + cur);
                        }
                        

                        //cur = gbp  to baseCur = aud
                        if (raw > 0 && cur != baseCur) {
                            priceNode.attr('title', formatMoney(items[i].price) + " " + baseCur + " " + items[i].pkg);
                        }
                        else if (cur == baseCur && raw > 0) {
                            priceNode.attr('title', formatMoney(items[i].price * agt.rt) + " " + agt.cur + " " + items[i].pkg);
                            destinationCurrency = "$";
                        }

                        //rev:mia june 9 2014 - B2BMAC
                        if (cangetgross == "Yes") {
                            //  priceNode.html("<span class=''> " + currentSym + formatMoney(items[i].agent) + " (agent)</span><br />" + destinationCurrency + formatMoney(items[i].pickup) + " (supplier)");
                            priceNode.html("<span class=''> " + currentSym + formatMoney(items[i].agent) + " </span><br />");
                        }
                        else {
                            $("#spanPriceHighlight").text("Price highlighted below is NETT only");
                            //priceNode.html("<span class='bluehighlight'> " + currentSym + formatMoney(items[i].agent) + " (agent)</span><br />" + destinationCurrency + formatMoney(items[i].pickup) + " (supplier)");
                            priceNode.html("<span class='bluehighlight'> " + currentSym + formatMoney(items[i].agent) + " </span><br />");
                        }



                        bookLink.attr('data-code', items[i].code).click(function(ev) { 
                            ev.stopPropagation();
                            var elm = $(this);                            
                            //abort all ongoings rqsts
                            //if(availInProg) {
                            //    alert('Results are still loading...please wait');
                            //    return;
                            //}
                            
                            //var url = agt.base + '/content/setsession.aspx?' + rqst.getConfigureURL(elm.attr('data-code')).split('Selection.aspx?')[1].replace('brand=', 'brand=' + bookLink.attr('data-brand')) + "&cb=b2bRedirect";
                            var url = agt.base + '/content/setsession.aspx?' + rqst.getConfigureURL(elm.attr('data-code')).split('Selection.aspx?')[1].replace('brand=', 'brand=' + elm.attr('data-brand')) + "&cb=b2bRedirect";
                            
                            elm.addClass('update');
                            $.ajax({
                                url: url,
                                dataType: 'jsonp',
                                success: function (data) {
                                    /*handled by callback */
                                   // alert(data)
                                },
                                error: function (error)
                                {
                                   // alert(error)
                                }
	 	                    });
                        });
                        if(items[i].promo != "n/a") {
                            elm.addClass('promo');
                            elm.find('.spc').attr('title',items[i].promo);
                            elm.find('.vfoot .spcDet').html(items[i].promo.replace("<big>","<big>HOT DEAL - "));//added for promo
                        } else
                            elm.removeClass('promo'); 
                        elm.removeClass('tac');                                                                                   
                    }
                    else {//no price            
                        //console.log("with no price " + items[i].code + '----' + items[i].pkg)
                        //if(domain.agg) {//mac
                            var errMsg = items[i].err != '' ? items[i].err : "Unavailable";
                            if (items[i].err == "TrueTrue") errMsg = "Unavailable";

                            if(errMsg.indexOf('DISP24') > -1) errMsg = "Request less then 24 hours"
                            if(errMsg != "Unavailable" && errMsg != "LIMITED AVAILABILITY") 
                                elm.addClass('tac');
                            else
                                elm.removeClass('tac');
                            
                            priceNode.html("<span class='prcMsg'>" + errMsg + "</span>");
                            priceNode.attr('title', (errMsg != '' ?  errMsg : items[i].pkg));
                            elm.removeClass('avail');
                            elm.addClass('noavail');
                            elm.attr('data-price', 99999);//max out
                            elm.attr('rel', 99999);//test
                            priceNode.attr('data-base',0);
                            var statElm = domain.agg ? elm.find('.status') : elm.find('.prcMsg');
                            statElm.removeClass('lead0'); 
                            statElm.attr('title', 'Unavailable');                       
                            statElm.html('Unavailable');
                            elm.removeClass('promo');
                            if(!domain.agg) { 
                                errMsg = brandedNoAvailMsg(items[i].err,items[i].brmsg);
                                if(errMsg.indexOf('less24') > -1) statElm.addClass('lead0');
                                statElm.html(errMsg);                                
                            }                    
                    }
                    test1A(items[i]);
                }
              
                if(loadCount == 0) 
                {
                    $('#vehicleCatalog .vehicle').each(function(i, el) { 
                        var elm = $(el);
                        var parentElm = elm.parent();
                        if(elm.hasClass('loading')) {
                            elm.removeClass('loading');
                            parentElm.removeClass('avail');
                            parentElm.removeClass('promo');
                            parentElm.addClass('noavail');
                            var priceNode = elm.find('.prc');
                            priceNode.attr('data-base',0);
                            var sleeps = parseInt(parentElm.attr('data-sleeps'));                            
                            var statusNode = elm.find('.status');
                            if(rqst.getMinPax() > sleeps) {//enc rqst 
                                var maxPaxMsg = "This vehicle cannot sleep the number of travellers you have specified";
                                priceNode.html("<span class='prcMsg'>"+ maxPaxMsg +"</span>");
                                priceNode.attr('title', maxPaxMsg);
                                statusNode.attr('title', 'Unavailable'); 
                                statusNode.html('Unavailable');
                                parentElm.addClass('tac');                            
                                parentElm.find('.vstatus .prcMsg').html('<span class="unavail">' + maxPaxMsg + '</span>');
                            }
                            else {
                                priceNode.text("Unavailable");
                                priceNode.attr('title', "Unavailable");
                                statusNode.attr('title', ''); 
                                statusNode.html('');
                                parentElm.find('.vstatus .prcMsg').html('<span class="unavail">Unavailable</span>');//not it reply
                            }
                            parentElm.attr('data-price', 99999);//max out 
                            parentElm.attr('rel', 99999 );                       
                        }
                    });
                    availInProg = false;
                    //filterBy = ".avail"
                    if (hasAvailable > 0)
                        filterBy = ".avail"
                    else
                    {
                        $("#filter_availability span.btns label").removeClass("selected")
                        $("#filter_availability span.btns label:first-child").addClass("selected")
                    }

                    
                    
                }
                if (!availInProg)
                {
                    $("#availabilityFilter").removeAttr("disabled");
                }

                var trkImg = $('#trackImg');
                if(trkImg.length ==0) {
                    trkImg = $(json.bi);
                    $('body').append(trkImg);
                }
                else {
                    trkImg.remove();
                    $('body').append(json.bi);
                }
                //trackGAEvt('Availability',trkImg.attr('src').split('ev=')[1]);
                forex($("#currencySelector option:selected").val(), $("#currencySelector option:selected").attr('rel'));//forex

                try
                {
                    var curtemp = $("#currencySelectorb2b option:selected").val()
                    var reltemp = $("#currencySelectorb2b option:selected").attr('rel')
                    if(curtemp != undefined)
                        forexb2b(curtemp, reltemp);
                    else
                        forexb2b(baseCurTemp.toUpperCase(), 1)
                }
                catch (ex) {
                    alert(ex);
                }

                sortChange("price_asc"); 
                if (bookables == 0) { $('#bookingControl .body').css('display', 'block'); $('#bookingControl').removeClass('col');}
                //isoTope();
            },
            error: function (XMLHttpRequest, textStatus, error) {
                loadCount--;                
            },
            timeout: function () {
                loadCount--;                                
            }
        };
        var xhr = $.ajax(options);
        xhrPool.push(xhr);
    }
  
}

function test1A(item){
    if(rqstParams.vc != '' && rqstParams.vc == item.code)//tag
    {
        if(item.avp != '')
            window.location = '/configure.aspx?avp=' + item.avp;

        else {
            var vElm = $('#v_' + item.code);
            vElm.addClass('1a');
            vElm.find('.alternate').html('<span class="1aMsg">The Vehicle you have chosen has limited availability for the dates selected<br />please choose other vehicles</span>')
            vElm.attr('data-price', 1);
            vElm.attr('rel', 1 );
            rqstParams.vc = "";
        }
    }    
}

function brandedNoAvailMsg(msg,brMsg){
    var display = msg;
    if(msg.indexOf('DISP24HR') > -1)
        display = '<span class="less24">Travel is within 24 hours - <a href="javascript:contactUs()">Contact Us</a>. Let our 24hr res team find a vehicle for you. We can search all the big brands:<br />Kea, Maui, Britz, Mighty, United, Alpha</span>';
    else if (msg != "" && msg != "LIMITED AVAILABILITY")
        display = '<span class="unavail">' + msg + '</span>';
    else if((msg == "" &&  brMsg != "") || msg == "LIMITED AVAILABILITY")
        display = '<span class="unavail">Unavailable</span><span class="alternate"><b>Search Alternate Availability</b><br />Search online to find the following alternative travel options for this vehicle: <b>Reverse route, +/- 10 days, Shorter hire.</b></span>';
    return display;
}

function showVInfo(url,elm) {
    if(!$(elm).parent('.vehicle').hasClass('col'))
        popUp(url);
    return false;
}

function popUp(url){
    //$('#modalBox').append($("<iframe frameborder='0' width='880px'/>").attr("src", url)).dialog({width:'900px'});
     NewWindow(url, 'vCode', '980', '500', 'yes', 'center');
}

var win = null;

function NewWindow(mypage, myname, w, h, scroll, pos) {//revisit   
    if (pos == "random") { LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - w)) : 100; TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - h) - 75)) : 100; }
    if (pos == "center") { LeftPosition = (screen.width) ? (screen.width - w) / 2 : 100; TopPosition = (screen.height) ? (screen.height - h) / 2 : 100; }
    else if ((pos != "center" && pos != "random") || pos == null) { LeftPosition = 0; TopPosition = 20 }
    settings = 'width=' + w + ',height=' + h + ',top=' + TopPosition + ',left=' + LeftPosition + ',scrollbars=1,location=no,directories=no,status=yes,menubar=no,toolbar=no,resizable=yes';
    win = window.open(mypage, myname, settings);
    win.focus();
}

function formatMoney(no) {
    try
    {
        var d = '.'; var t = ',';
        var n = no, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
        var result = s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        return result;
    }
    catch (error) {
        //console.error("result: " + error);
    }

    
}

function trackGAEvt(action, params) {
    if(_gaq)
        _gaq.push(['_trackEvent','Campervan', action, params, 0]);
}

function brandForChar(char){
    var brand = '';
    switch(char)
    {
        case 'q':
            brand = 'kea';
            break;
        case 'm':
            brand = 'maui';
            break;
        case 'u':
            brand = 'united';
            break;
        case 'b':
            brand = 'britz';
            break;
        case 'a':
            brand = 'alpha';
            break;
        case 'y':
            brand = 'mighty';
            break;
    }
    return brand;
}

function curSymForCode(code) {
    var curSym = { AUD: "$", CAD: "$", CNY: "", EUR: "€", GBP: "£", HKD: "$", JPY: "¥", SGD: "$", ZAR: "", KRW: "", CHF: "", USD: "$", NZD: "$", INR: "Rs", FJD: "$", SBD: "$", THB: "฿" };
    var symbol = eval('curSym.' + code);
    if (symbol == null || typeof symbol == 'undefined') symbol = "";
    return symbol;
}

//rev:mia 16April2015 - Total combined pax selected should auto select the sleeps filter so it only displays vehicles that apply
function PaxSelection(valAdults, valChildren) {
    //debugger
    $("#filter_sleeps .btns label.selected").removeClass("selected");
    var elem;
    var totalpax = parseInt(valAdults) + parseInt(valChildren);
    if (totalpax > 6 || (valAdults == 1 && valChildren == 0)) {
        elem = $("#filter_sleeps .btns label[data-filter='.s2, .s3, .s4, .s5, .s6']").addClass("selected")
    }
    else
        elem = $("#filter_sleeps .btns label[data-filter='.s" + totalpax + "']").addClass("selected")


    var actionType = "filter";
    var actionCol = "sleeps";

    if (actionType == "filter") {
        filterBy = '';
        $("#filter_sleeps .btns").children(".selected").each(function (index) {
            var tmpFilter = $(this).attr('data-filter').split(',').length;
            if (tmpFilter === 1)
                filterBy += $(this).attr('data-filter');
        }
       );
    }
    else {
        sortBy = actionCol;
        isAsc = (elm.attr('data-filter') == "asc");
    }
    isoTope();

}
