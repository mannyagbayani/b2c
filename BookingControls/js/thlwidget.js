﻿//THL Booking Widget v1.0
var thlOptions = {}; //TODO: define defaults
function initTHLWidget(options) {
    thlOptions = options;

    window.onload = function () {
        if (!thlOptions.expandUI)
            document.getElementById("bookingWidget").style.position = "absolute"; //param
        document.getElementById("bookingWidget").innerHTML = "<div id='widgetHead'>" + thlOptions.headerTxt + "</div><div id='frameContainer' style='display:none; width: " + 480 + "px;overflow:hidden;' style='background-color:transparent;'></div>";
        document.getElementById("widgetHead").onclick = triggerWidget;

        if (thlOptions.startExpanded) triggerWidget(thlOptions);
    }
}

function triggerWidget(thlOptions) {

    if (document.getElementById("bookingWidget").className != "loaded") {
        document.getElementById("bookingWidget").className = "loaded";
        document.getElementById("frameContainer").innerHTML = "<iframe id='thlControl' style='height:600px;width:" + thlOptions.width + "px;' width='" + thlOptions.width + "px' scrolling='no' frameborder='0' height='" + thlOptions.height + "px' src='" + thlOptions.serviceUrl + "/" + (thlOptions.horizontal ? "horizontal" : "") + "widget.aspx?vt=" + thlOptions.vehicleType + "&cr=" + thlOptions.driversLicense + "&cc=" + thlOptions.country + "&brand=" + thlOptions.brand + "&months=" + thlOptions.months + ((typeof thlOptions.vehicleCode !== "undefined") ? "&vc=" + thlOptions.vehicleCode : "") + "&cssb=" + thlOptions.cssBase + ((typeof thlOptions.utm_campaign !== "undefined") ? "&utm_campaign=" + thlOptions.utm_campaign : "") + "' style='overflow:hidden;'></iframe>";
    }
    document.getElementById("frameContainer").style.display = (document.getElementById("frameContainer").style.display == "block" ? "none" : "block");
}

/* Date Picker Starts */

function beforeShowDayFunc(date) {
    var selected = ($("#ctrlSearchBox_calPickUp_DateText").datepicker('getDate') < date);
    return [selected, ''];
}

function initDates(params) {
    //JQuery calendar starts
    $("#bookingControl .TimeSpan input").focus(function () {
        $(this).select();
    });

    $("#ctrlSearchBox_calDropOff_DateText").datepicker({
        dateFormat: 'dd-M-yy',
        beforeShowDay: beforeShowDayFunc,
        minDate: new Date(),
        numberOfMonths: ctrlUI.numMonth,
        onClose: function (dateText, inst) {
            if (Date.parse($("#ctrlSearchBox_calDropOff_DateText").val()) < Date.parse($("#ctrlSearchBox_calPickUp_DateText").val())) {

            }
        }
    });

    $("#ctrlSearchBox_calPickUp_DateText").datepicker({
        dateFormat: 'dd-M-yy',
        minDate: new Date(),
        numberOfMonths: ctrlUI.numMonth,
        onClose: function (dateText, inst) {
            if (Date.parse($("#ctrlSearchBox_calDropOff_DateText").val()) < Date.parse($("#ctrlSearchBox_calPickUp_DateText").val())) {
                $("#ctrlSearchBox_calDropOff_DateText").val($("#ctrlSearchBox_calPickUp_DateText").val());
            }
        }
    });
    //JQuery calendar ends 
    
    

}

var startDate;
var endDate;
var ONEDAY = 3600 * 24 * 1000;

function filterDates1(cal) {
    var date = cal.date;
    startDate = date.getTime();
    document.getElementById('ctrlSearchBox_dropPickUpTime').focus();
    document.getElementById('ctrlSearchBox_calDropOff_DateText').value = document.getElementById('ctrlSearchBox_calPickUp_DateText').value;
    $('#ctrlSearchBox_calPickUp_DateText').removeClass('required');
    $("div.requiredMsg").remove();
}
function filterDates2(cal) {
    var date = cal.date;
    endDate = date.getTime();
    document.getElementById('ctrlSearchBox_dropDropOffTime').focus();
    if (document.getElementById('ctrlSearchBox_calDropOff_DateText').value == 'dd/mm/yyyy')
        document.getElementById('ctrlSearchBox_calDropOff_DateText').value = document.getElementById('ctrlSearchBox_calPickUp_DateText').value;
    $('#ctrlSearchBox_calDropOff_DateText').removeClass('required');
    $("div.requiredMsg").remove();
}

function disallowDateBefore(date) {
    date = date.getTime();
    var MINHIRE;
    MINHIRE = ONEDAY * -1;
    if ((startDate != null) && (date <= (startDate + MINHIRE))) {
        return true;
    }
    var now = new Date().getTime() + ONEDAY;
    if (date <= (now + MINHIRE)) {
        return true;
    }
    return false;
}
function disallowDateAfter(date) {
    date = date.getTime();
    var now = new Date().getTime();
    if (date < (now)) {
        return true;
    }
    return false;
}
/* Date Picker ends */

function updateCtrl() {
    branchSelectFactory($('#ctrlSearchBox_dropDropOffLocation'));
    branchSelectFactory($('#ctrlSearchBox_dropPickUpLocation'));
    vehicleSelectFactory($('#ctrlSearchBox_dropVehicleType'));
    $('#ddCountryOfTravel').val(ctrlParams.state.cc);

    if (ctrlParams.state.cc != "none") $('li.CountryOfTravel').css('display', 'none');

    if (ctrlParams.state.type == "car") {
        $('#vehicleTabs a').removeClass('on');
        $('#carsTab').addClass('on');
    }

}

function vehicleSelectFactory(elm) {
    var vehicles = ctrlParams.vehicles;
    //alert(vehicles.length);
    elm.html("<option value='-1'>Search all vehicles</option>");
    for (var i = 0; i < vehicles.length; i++) {
        if (ctrlParams.state.cc == vehicles[i].cc && ctrlParams.state.type == vehicles[i].vt && vehicles[i].brand == ctrlParams.state.brand) {
            elm.append("<option value='" + vehicles[i].code + "' " + ((ctrlParams.state.model == vehicles[i].code) ? "selected='selected'" : "") + " >" + vehicles[i].name + "</option>");
        }
    }
}

function branchSelectFactory(elm) {
    var branches = ctrlParams.branches;
    elm.html("<option value=''>At which location?</option>");
    for (var i = 0; i < branches.length; i++) {
        if (ctrlParams.state.cc == branches[i].cc && (ctrlParams.state.type == branches[i].vt || branches[i].vt == "both") && (branches[i].brands.indexOf(ctrlParams.state.brand) != -1))
            elm.append("<option value='" + branches[i].code + "'>" + branches[i].name + "</option>");
    }
}

function submitWidget(SSLRedirect) {
    var targetPrefix = ((('https:' == document.location.protocol) || (typeof SSLRedirect == 'undefined'/* backward comp */)) ? 'https://' : 'http://')
    
    if (!$('#form1').valid()) {
       
        return false;
    }
    else {
        //Added by Nimesh on 29h Sep 2014 for Include Cross Link
        var removeCrossLink = false;
        if (document.URL.indexOf('removeCrossLink') > -1)
            removeCrossLink = document.URL.split('removeCrossLink=')[1].split('&')[0];
        //End - Added by Nimesh on 29h Sep 2014 for Include Cross Link
        var responseText = '';
        var cc = ctrlParams.state.cc.toUpperCase();  //$("#countryOfTravel").val();
        var na = $('#ctrlSearchBox_dropAdults').val();
        var nc = $('#ctrlSearchBox_dropChildren').val();
        //------------ date time -----------------
        var pickupdateStr = $('#ctrlSearchBox_calPickUp_DateText').val();
        var dropoffDateStr = $('#ctrlSearchBox_calDropOff_DateText').val();

        if (!validatePeriod(pickupdateStr, dropoffDateStr)) {//failed onblur Date Range validation
            $('input.dateField').addClass('error');
            //promptField($j('#ctrlSearchBox_calPickUp_DateText'), "Required");
        }
        var pickupdate = Date.parseExact(pickupdateStr, "dd-MMM-yyyy");
        var dropoffDate = Date.parseExact(dropoffDateStr, "dd-MMM-yyyy");
        pb = $("#ctrlSearchBox_dropPickUpLocation option:selected").val();
        pd = pickupdate.getDate();
        pm = pickupdate.getMonth() + 1;
        py = pickupdate.getFullYear();
        pt = $("#ctrlSearchBox_dropPickUpTime option:selected").val();
        db = $("#ctrlSearchBox_dropDropOffLocation option:selected").val();
        dd = dropoffDate.getDate();
        dm = dropoffDate.getMonth() + 1;
        dy = dropoffDate.getFullYear();
        dt = $("#ctrlSearchBox_dropDropOffTime option:selected").val();
        //------------ date time ends -----------------    
        cr = $("#ctrlSearchBox_dropCountryOfResidence option:selected").val();
        if (parseInt(cr) == 0) {
            //responseText += " country of residence,";
            //promptField($('#ctrlSearchBox_dropCountryOfResidence'), "Required");
        }
        if (responseText != '') {
           
            return '';
        }
        pb = $("#ctrlSearchBox_dropPickUpLocation option:selected").val();
        db = $("#ctrlSearchBox_dropDropOffLocation option:selected").val();
        var legacyType = (ctrlParams.state.type == 'car') ? "car" : "rv";

        var vc = $('#ctrlSearchBox_dropVehicleType').val();
        var targetUrl = targetPrefix + /*document.domain*/getDestForBrand(ctrlParams.state.brand, cc) + "/Selection.aspx"; 
        
        var ac = (typeof ctrlParams.state.agent == 'undefined' || ctrlParams.state.agent == null ? "" : ctrlParams.state.agent);
        //Following line was commented and included in if condition by Nimesh on 29th Sep 2014 for includecrosslink
        ////var queryParams = "?cc=" + cc + "&brand="+ ctrlParams.state.brand +"&ac=" + ac +"&sc=" + legacyType + "&vtype=" + legacyType + "&pc=&na=" + na + "&nc=" + nc + "&cr=" + cr + "&pb=" + pb + "&pd=" + pd + "&pm=" + pm + "&py=" + py + "&pt=" + pt + "&db=" + db + "&dd=" + dd + "&dm=" + dm + "&dy=" + dy + "&dt=" + dt + "&vh=" + vc + /*"&vt=" + (legacyType == "car" ? "ac" : "") +*/"&pv=1.0";
        //var queryParams = "?cc=" + cc + (ctrlParams.state.brand != "" ? "&brand=" + ctrlParams.state.brand : "&utm_campaign=CrossLink&brand=" ) + "&ac=" + ac + "&sc=" + legacyType + "&vtype=" + legacyType + "&pc=&na=" + na + "&nc=" + nc + "&cr=" + cr + "&pb=" + pb + "&pd=" + pd + "&pm=" + pm + "&py=" + py + "&pt=" + pt + "&db=" + db + "&dd=" + dd + "&dm=" + dm + "&dy=" + dy + "&dt=" + dt + "&vh=" + vc + "&vc=" + vc + /*"&vt=" + (legacyType == "car" ? "ac" : "") +*/"&pv=1.0";
        var queryParams = "";
        if (ctrlParams.state.brand === "") {
            if (removeCrossLink === "true")
                queryParams = "?cc=" + cc + (ctrlParams.state.brand != "" ? "&brand=" + ctrlParams.state.brand : "&brand=") + "&ac=" + ac + "&sc=" + legacyType + "&vtype=" + legacyType + "&pc=&na=" + na + "&nc=" + nc + "&cr=" + cr + "&pb=" + pb + "&pd=" + pd + "&pm=" + pm + "&py=" + py + "&pt=" + pt + "&db=" + db + "&dd=" + dd + "&dm=" + dm + "&dy=" + dy + "&dt=" + dt + "&vh=" + vc + "&vc=" + vc + /*"&vt=" + (legacyType == "car" ? "ac" : "") +*/"&pv=1.0";
            else
                queryParams = "?cc=" + cc + (ctrlParams.state.brand != "" ? "&brand=" + ctrlParams.state.brand : "&utm_campaign=CrossLink&brand=") + "&ac=" + ac + "&sc=" + legacyType + "&vtype=" + legacyType + "&pc=&na=" + na + "&nc=" + nc + "&cr=" + cr + "&pb=" + pb + "&pd=" + pd + "&pm=" + pm + "&py=" + py + "&pt=" + pt + "&db=" + db + "&dd=" + dd + "&dm=" + dm + "&dy=" + dy + "&dt=" + dt + "&vh=" + vc + "&vc=" + vc + /*"&vt=" + (legacyType == "car" ? "ac" : "") +*/"&pv=1.0";
        }
        else
            queryParams = "?cc=" + cc + (ctrlParams.state.brand != "" ? "&brand=" + ctrlParams.state.brand : "&utm_campaign=CrossLink&brand=") + "&ac=" + ac + "&sc=" + legacyType + "&vtype=" + legacyType + "&pc=&na=" + na + "&nc=" + nc + "&cr=" + cr + "&pb=" + pb + "&pd=" + pd + "&pm=" + pm + "&py=" + py + "&pt=" + pt + "&db=" + db + "&dd=" + dd + "&dm=" + dm + "&dy=" + dy + "&dt=" + dt + "&vh=" + vc + "&vc=" + vc + /*"&vt=" + (legacyType == "car" ? "ac" : "") +*/"&pv=1.0";
        //End - Following line was commented and included in if condition by Nimesh on 29th Sep 2014 for includecrosslink
        if (ctrlParams.state.brand == "" || ctrlParams.state.brand == "a") {//migration point, refactor once completed.
            queryParams = "/" + cc + "/select" + queryParams;
            targetUrl = targetUrl.split('/Selection.aspx')[0].replace('http:','https:');
        }

        var docParams = document.URL
        // IF condition Added by Nimesh On 29th Sep
        if(removeCrossLink === "true" || ctrlParams.state.brand !== "")
        if (document.URL.indexOf('utm_campaign') > -1)
            queryParams += "&utm_campaign=" + document.URL.split('utm_campaign=')[1];

        //window.parent.location = targetUrl + queryParams;
        //window.open(targetUrl + queryParams, 'THL Search');
        window.open(targetUrl + queryParams);
    }
}

function validatePeriod(pickupdateStr, dropoffDateStr) {
    var pickUpDate = Date.parseExact(pickupdateStr, "dd-MMM-yyyy");
    var dropOffDate = Date.parseExact(dropoffDateStr, "dd-MMM-yyyy");
    if (pickupdateStr == null || dropoffDateStr == null || pickupdateStr == '' || dropoffDateStr == '' || pickupdateStr == 'dd-mmm-yyyy' || dropoffDateStr == 'dd-mmm-yyyy' || Date.parseExact(pickupdateStr, "dd-MMM-yyyy") == null || Date.parseExact(dropoffDateStr, "dd-MMM-yyyy") == null)
        return false;
    else {//range validation
        if (dropOffDate.compareTo(pickUpDate) >= 0)
            return true
        else
            return false;
    }
}


function bindSpinner(options) {
    btn = $('#searchButton');
    spinner = $('.spinner');
    var spinnerButtons = $('#searchButton');
    var loadingTimer;
    var loadingFrame = 0;
    var animateLoading = function () {
        if (!spinner.is(':visible') && (spinner.css("display") != "inline")) {
            clearInterval(loadingTimer);
            return;
        }
        var imgHeight = 24;
        var frameCount = 8;

        spinner.css('background-image', "url(" + options.imgUrl + ")");
        spinner.css('background-position', "0 " + (loadingFrame * -imgHeight) + "px");
        spinner.css('width', imgHeight);

        loadingFrame = (loadingFrame + 1) % frameCount;
    };
    var showSpinner = function () {
        clearInterval(loadingTimer);
        spinner.show();
        loadingTimer = setInterval(animateLoading, 50);
    };
    spinnerButtons.click(function () {
        if ($('#ctrlSearchBox_divContent input.error, #ctrlSearchBox_divContent select.error').length == 0) 
            //showSpinner();            
        return true;
    });
}

function swapCtry(elm) {
    var ctr = $(elm).val();
    ctrlParams.state.cc = ctr;
    updateCtrl();
}

function getDestForBrand(brand, country) {
    var dest = 'motorhomesandcars.com';
    brand = brand.toLowerCase(); country = country.toLowerCase();
    var destinations = [
        { 'b': 'bau', 'u': 'britz.com.au' },
        { 'b': 'bnz','u': 'britz.co.nz'},
        { 'b': 'mau','u': 'maui.com.au'},
        { 'b': 'mnz','u': 'maui.co.nz'},
        { 'b': 'yau','u': 'mightycampers.com.au'},
        { 'b': 'ynz','u': 'mightycampers.co.nz'},
        { 'b': 'qau','u': 'keacampers.com.au'},
        { 'b': 'qnz','u': 'keacampers.co.nz'},
        { 'b': 'unz','u': 'unitedcampervans.co.nz'},
        { 'b': 'anz','u': 'alphacampervans.co.nz'},
        { 'b': 'eau','u': 'econocampers.co.nz' },
        { 'b': 'au','u':  'motorhomesandcars.com'},
        { 'b': 'nz','u': 'motorhomesandcars.com'}
    ];
    for(i=0; i< destinations.length; i++) if (destinations[i].b == brand + country) dest = env + "." + destinations[i].u;
    //add kea exc
    // added by nimesh on 23rd Aug 2014 to take care of kea url in widget
    if (dest.indexOf("keacampers") !== -1) {
        dest = (env + "." + country + "bookings.keacampers.com").toLowerCase().replace('secure.','');
    }
    // end added by nimesh on 23rd Aug 2014 to take care of kea url in widget
    return dest;
}