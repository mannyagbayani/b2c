﻿var ctrl = null;
var showSpinner = null;
var debugOn = true;

Date.compareStrDate = function (dateStr1, dateStr2) {//dd-mm-yyyy 
    var dateParams1 = dateStr1.split('-');
    var dateParams2 = dateStr2.split('-');
    var date1 = Date.parse(dateParams1[2] + "-" + dateParams1[1] + "-" + dateParams1[0]);
    var date2 = Date.parse(dateParams2[2] + "-" + dateParams2[1] + "-" + dateParams2[0]);
    if (date1 > date2) return 1;
    else if (date1 < date2) return 2;
    return 0; //return 1 for first larger,2 for second and 0 for equal..    
};

Date.toB2CStr = function (date) {
    //this is a lang point
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    return months[date.getMonth()] + " " + date.getDate() + "," + date.getFullYear();
}

/* B2C Controller */
function B2CController(params) {
    this.params = params; //local storage point
    this.setParam('na', 1); //defaults point    
    this.setParam('nc', 0);
    this.setQueryParams();
}

B2CController.prototype.toString = function () {
    var results = "params:";
    for (var key in this.params) {
        var obj = this.params[key];
        results += key + " = " + obj + ",";
    }
    return results;
}

B2CController.prototype.store = function () {
    var ser = JSON.stringify(this.params);
    //$.jStorage.set("thlb2c", ser, { TTL: 3600000 });//period?
}

B2CController.prototype.restore = function () {
    var val = $.jStorage.get("thlb2c", "");
    if (val != null && val != "")
        return val;
}

B2CController.prototype.setCtrlData = function (data) {
    this.params.data = data;
}

B2CController.prototype.setParam = function (name, val, uiElm) {    
    for (var key in this.params) {
        if (key == name) {
            this.params[key] = val;
            //if (typeof uiElm != 'undefined' && uiElm != null) {
            //    var elm = $('#' + uiElm);
            //    elm.val(val);
            //    elm.trigger('change');
            //}
            //this.store();
            logEvt(this.toString());
            return;
        }
    }
    this.params[name] = val;
    //this.store();
    logEvt(this.toString());
}

B2CController.prototype.getB2CUrl = function () {
    var pdParams = this.params.pd.split('-'), ddParams = this.params.dd.split('-');
    $('.calanderTime select').trigger('change');//load time
    var targetUrl = this.params.engine + "/" + this.params.cc + "/select/" + "?utm_campaign=CrossLink&ac=" + this.params.ac + "&sc=rv&pc=" + /*this.params.pc +*/"&na=" + this.params.na + "&nc=" + this.params.nc + "&cr=" + this.params.cr + "&pb=" + this.params.pb + "&pd=" + pdParams[0] + "&pm=" + pdParams[1] + "&py=" + pdParams[2] + "&pt=" + this.params.pt + "&db=" + this.params.db + "&dd=" + ddParams[0] + "&dm=" + ddParams[1] + "&dy=" + ddParams[2] + "&dt=" + this.params.dt + "&vh=";
    return targetUrl;
}

B2CController.prototype.validate = function () {//return array of missing UIs
    var missingParams = [];
    var required = [{ 'data': 'pb', 'ui': 'pickUpLoc', 'msg': 'Pickup Location' }, { 'data': 'db', 'ui': 'dropOffLoc', 'msg': 'Drop off Location' }, { 'data': 'pd', 'ui': 'pickUpDate', 'msg': 'Pickup date' }, { 'data': 'dd', 'ui': 'dropOffDate', 'msg': 'Dropoff Date' }, { 'data': 'cr', 'ui': 'driverLic', 'msg': 'Drivers License'}];
    for (var i = 0; i < required.length; i++) {
        if (typeof this.params[required[i].data] == 'undefined' || this.params[required[i].data] == '' || this.params[required[i].data] == 0)
            missingParams.push(required[i])
    }
    return missingParams; //returns the missing UI ids.
}

B2CController.prototype.setQueryParams = function () {
    if (document.location.href.indexOf('?') > 0) {
        var params = {}, queries, temp, i, l;
        queryString = document.location.href.split('?')[1];
        queries = queryString.split("&");
        for (i = 0, l = queries.length; i < l; i++) {
            temp = queries[i].split('=');
            params[temp[0]] = temp[1];
        }
        this.setParam('pc', typeof params["pc"] != 'undefined' ? params["pc"] : "");
        this.setParam('ac', typeof params["ac"] != 'undefined' ? params["ac"] : ""); //biz point        
    }
    else {//set blank
        this.setParam('pc', '');
        this.setParam('ac', '');
    }
}

function initWidget(params) {
    params = $.jStorage.get("thlb2c") ?  JSON.parse($.jStorage.get("thlb2c")) : { 'ac': 'MACCOM', 'cc': 'nz', 'engine' : 'https://secure.motorhomesandcars.com','cr':''};
    ctrl = new B2CController(params);
    $.ajax({
        url: 'http://staging.data.thlonline.com/data/Branches.aspx?format=json&cb=loadCtrl',
        dataType: 'jsonp'
    });
    pdDatePicker = $('#pickUpDate').datepicker({
        dateFormat: 'M dd, yy',
        numberOfMonths: 2,
        changeMonth: true,
        changeYear: false,
        minDate: new Date(),
        onSelect: function (dateText, inst) {
            var month = parseInt(inst.selectedMonth, 10) + 1;
            ctrl.setParam('pd', (inst.selectedDay < 10 ? "0" : "") + inst.selectedDay + "-" + (month < 10 ? "0" : "") + month + "-" + inst.selectedYear, $('#pickUpDate'));
            $(this).parent().removeClass('required');
        },        
        onClose: function (dateText, inst) {
            if (!ctrl.params.dd || Date.parse($("#dropOffDate").val()) < Date.parse($("#pickUpDate").val())) {
                var toDate = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
                var drop = new Date(toDate.getTime() + 3 /* from config?*/ * 86400000);
                $("#dropOffDate").val(Date.toB2CStr(drop));
                $("#dropOffDate").parent().removeClass('required');
                ctrl.setParam('dd', drop.getDate() + "-" + (drop.getMonth() < 10 ? "0" : "") + (drop.getMonth() + 1) + "-" + drop.getFullYear(), $("#dropOffDate"));
            }
        }
    });

    ddDatePicker = $("#dropOffDate").datepicker({
        beforeShowDay: beforeShowDayFunc,
        dateFormat: 'M dd, yy',
        numberOfMonths: 2,
        minDate: new Date(),
        changeMonth: true,
        changeYear: false,
        onSelect: function (dateText, inst) {
            var month = parseInt(inst.selectedMonth, 10) + 1;
            ctrl.setParam('dd', (inst.selectedDay < 10 ? "0" : "") + inst.selectedDay + "-" + (month < 10 ? "0" : "") + month + "-" + inst.selectedYear, $("#dropOffDate"));
            $(this).parent().removeClass('required');
        },
        onChangeMonthYear: function (year, month, inst) {
            //changeMonthYear($(this), year, month, inst, $('#dropOffTimeTxt'));
        }
    });
    
    //bind ddowns
    $('#bookingControl select').change(function (e) {
        var elm = $(this);
        var paramName = elm.attr('data-b2c');
        var val = elm.val();
        ctrl.setParam(paramName, val, $(this));

        switch (elm.attr('id')) {//dd specifics, set dropoff
            case 'pickUpLoc':
                $('#dropOffLoc :nth-child(1)').prop('value', val);
                ctrl.setParam('db', val);
                break;
        }
        //if (typeof e.isTrigger == 'undefined' || !e.isTrigger)
        //    setRequired();
        $(this).parent().removeClass('required');
    });

    $('#countryOfTravel').change(function (ev) {
        var elm = $(this);
        loadCtrl(null, { cc: elm.val(), 'vt': 'camper' });
        $('.loc').each(function(i,el) {
            var elm = $(el);
            elm.parent().find('.customSelectInner').text(elm.parent().find('select option')[0].text); //$()
        });
        
    });

    $('#updateSearch').click(function (e) {
        e.preventDefault();
        var required = ctrl.validate();
        if (required.length > 0) {
            var requiredMsg = "";
            for (var i = 0; i < required.length; i++) {
                requiredMsg += required[i].msg + (i < required.length - 1 ? "," : "");
                $('#' + required[i].ui).parent().addClass('required');
            }
            logEvt(requiredMsg);
            //setRequired();
            $('.bookingControlForm .requiredBox').css('display', 'block');
        } else {
            uiLoading(true);
            logEvt(ctrl.getB2CUrl());
            ctrl.store();
            window.location = ctrl.getB2CUrl();
        }
    });


    $('.ui-icon-trianSgle-1-s').click(function (ev) {
        logEvt($(this).prev());
        $(this).prev().datepicker("show");
    });
    
        
    var options = { height: 20, width: 20, imgUrl: "/SiteCollectionImages/booking/ajax-loader.png", btnClass: "submit", spinnerClass: "spinner" }
    bindSpinner(options);

    return;
}

function setRequired() {
    $('.bookingControlForm select, .bookingControlForm input').removeClass('required');
    var req = ctrl.validate();
    for (var i = 0; i < req.length; i++)
        $('#' + req[i]).addClass('required');
    return req.length;
}

function setLocs(data, options) {
    var vType = (options.vt + options.cc).toLowerCase();
    //console.log(vType);
    clearLocSelect(["pickUpLoc", "dropOffLoc"]);
    for (var i = 0; i < data.length; i++) {
        var branch = data[i];
        if (vType == branch.vt.toLowerCase()) {
            $("#pickUpLoc, #dropOffLoc").append("<option value=\"" + branch.code + "\">" + branch.title + "</option>");
        }
    }
}

function loadCtrl(data, options) {
    if (!options) options = { cc: 'nz', 'vt': 'camper' };//first timer
    if (!ctrl.data) {
        ctrl.data = {};
        ctrl.data.branches = data;
    }
    setLocs(ctrl.data.branches, options);

    $('#pickUpLoc').change(function () {
        var dropElm = $('#dropOffLoc');
        $('#dropOffLoc option[0]').attr('selected', 'selected');
        dropElm.next().find('.customSelectInner').text('Same as Pick Up');
    });


    return; //fornow
    /*
    //grab time from ui
    var selects = $('.bookingControlForm select.time');
    for (var i = 0; i < selects.length; i++) {
        var elm = $(selects[i]);
        var paramName = elm.attr('data-b2c');
        ctrl.params[paramName] = elm.val();
    };
    ctrldata = ctrl.params.data;
    var branches = [];
    clearLocSelect(["pickUpLoc", "dropOffLoc", "vehicleModel"]);
    for (var i = 0; i < ctrldata.branches.length; i++) {
        var branch = ctrldata.branches[i]
        if (branch.vt == ctrl.params.vt && branch.cc == ctrl.params.cc && branch.brands.indexOf(ctrl.params.brand) > -1) {
            $("#pickUpLoc, #dropOffLoc").append("<option value=\"" + branch.code + "\">" + branch.name + "</option>");
            branches.push(branch);
        }
    }    
    //syncUI(ctrl.params);
    */
}

function clearLocSelect(elmIds) {
    for (var i = 0; i < elmIds.length; i++)
        $("#" + elmIds[i] + " > option").each(function (i, el) {
            if (i == 0) return;
            $(el).remove();
        });
}

function syncUI(params) {//restore the view state based on params, ui specific
    $('#pickUpLoc').val(params.pb);
    $('#dropOffLoc').val(params.db);
    //$('#vehicleModel').val(params.vt);
    $('#countrOfRes').val(params.cr);
    $('#pickUpDate').val((params.pd > 0) ? params.pd + '-' + params.pm + '-' + params.py : "Choose what date?");
    $('#dropOffDate').val((params.dd > 0) ? params.dd + '-' + params.dm + '-' + params.dy : "Choose what date?");
    $('#numberAdults').val(params.na);
    $('#pickUpTime').val(params.pt);
    $('#dropOffTime').val(params.dt);
    $('#numberChildren').val(params.nc);
    $('.bookingControlForm select').trigger('change');
}

function changeMonthYear(calendar, year, month, inst, hiddenObj) {
    var oldDate = calendar.datepicker("getDate");
    if (oldDate != null) {
        var newDate = new Date(year, month - 1, oldDate.getDate());
        if (newDate.getDate() != oldDate.getDate()) {
            var lastDay = 32 - new Date(year, month - 1, 32).getDate();
            newDate = new Date(year, month - 1, lastDay);
        }
        calendar.datepicker("setDate", newDate);
    }
}

function setDate(inst) {
    var toDate = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
    var nextDay = new Date(toDate.getTime() + 3 * 86400000);
    //TODO: add drop set into controller
    $('#dropOffDate').datepicker("option", "minDate", nextDay);
    pickFormated = $("#dropOffDate").datepicker({ dateFormat: 'M dd, yy' }).val();
    $('#dropOffDate').val(pickFormated);
}

function bindSpinner(options) {
    btn = $('.submit');
    spinner = $('.spinner');
    var spinnerButtons = $('.submit');
    var loadingTimer;
    var loadingFrame = 0;
    var animateLoading = function () {
        if (!spinner.is(':visible') && (spinner.css("display") != "inline")) {
            clearInterval(loadingTimer);
            return;
        }
        var imgHeight = 24;
        var frameCount = 8;
        spinner.css('background-image', "url(" + options.imgUrl + ")");
        spinner.css('background-position', "0 " + (loadingFrame * -imgHeight) + "px");
        spinner.css('width', imgHeight);
        loadingFrame = (loadingFrame + 1) % frameCount;
    };
    showSpinner = function (show) {
        clearInterval(loadingTimer);
        if (show)
            spinner.show();
        else
            spinner.hide();
        loadingTimer = setInterval(animateLoading, 50);
    };
}

function beforeShowDayFunc(date) {
    var selected = ($("#pickUpDate").datepicker('getDate') < date);
    return [selected, ''];
}

function uiLoading(loading) {
    showSpinner(loading);
    if (loading) {
        $('#updateSearch').addClass('loading');
    } else {
        $('#updateSearch').removeClass('loading')
    }
}

function logEvt(evt) {
    if (debugOn) console.log(evt);
}