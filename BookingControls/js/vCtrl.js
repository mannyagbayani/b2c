﻿//Booking Lib 0.8
function initControl(params) {
    Zapatec.Calendar.setup({
        inputField: "ctrlSearchBox_calDropOff_DateText",
        button: "ctrlSearchBox_calDropOff_DateText",
        weekNumbers: false,
        showOthers: false,
        electric: false,
        disableFunc: disallowDateBefore,
        onUpdate: filterDates2,
        singleClick: true,
        ifFormat: "%d-%b-%Y",
        align: "BC",
        numberMonths: 2,
        timeInterval: 5,
        canType: true,
        showsTime: false
    });
    Zapatec.Calendar.setup({
        inputField: "ctrlSearchBox_calPickUp_DateText",
        button: "ctrlSearchBox_calPickUp_DateText",
        weekNumbers: false,
        showOthers: false,
        electric: false,
        disableFunc: disallowDateAfter,
        onUpdate: filterDates1,
        singleClick: true,
        ifFormat: "%d-%b-%Y",
        align: "BC",
        numberMonths: 2,
        timeInterval: 5,
        canType: true,
        showsTime: false
    });
    //bind validator
    if (cr != null && cr != '') {//set cor
        var corOption = $j("#ctrlSearchBox_dropCountryOfResidence").val(cr);
    }
    //set dates
    document.getElementById('ctrlSearchBox_calPickUp_DateText').value = pudate;
    document.getElementById('ctrlSearchBox_calDropOff_DateText').value = dod;
    if (noa != 0 && $j('#ctrlSearchBox_dropAdults option:last').val() >= noa) {
        $j('#ctrlSearchBox_dropAdults').val(noa);
    }
    if (noc != 0 && $j('#ctrlSearchBox_dropChildren option:last').val() >= noc) {
        $j('#ctrlSearchBox_dropChildren').val(noc);
    }
    $j("select").change(function() {
        $j("div.requiredMsg").remove();
        $j(this).removeClass('required');
    });
    cot = $j("#countryOfTravel").val();
    if (cot != null && cot != '') {//set cor
        $j("#ddCountryOfTravel").val(cot);
    }    
}

var startDate;
var endDate;
var ONEDAY = 3600 * 24 * 1000;

function filterDates1(cal) {
    var date = cal.date;
    startDate = date.getTime();
    document.getElementById('ctrlSearchBox_dropPickUpTime').focus();
    document.getElementById('ctrlSearchBox_calDropOff_DateText').value = document.getElementById('ctrlSearchBox_calPickUp_DateText').value;
    $j('#ctrlSearchBox_calPickUp_DateText').removeClass('required');
    $j("div.requiredMsg").remove();
}
function filterDates2(cal) {
    var date = cal.date;
    endDate = date.getTime();
    document.getElementById('ctrlSearchBox_dropDropOffTime').focus();
    if (document.getElementById('ctrlSearchBox_calDropOff_DateText').value == 'dd/mm/yyyy')
        document.getElementById('ctrlSearchBox_calDropOff_DateText').value = document.getElementById('ctrlSearchBox_calPickUp_DateText').value;
    $j('#ctrlSearchBox_calDropOff_DateText').removeClass('required');
    $j("div.requiredMsg").remove();
}

function disallowDateBefore(date) {
    date = date.getTime();
    var MINHIRE;
    MINHIRE = ONEDAY * -1;
    if ((startDate != null) && (date <= (startDate + MINHIRE))) {
        return true;
    }
    var now = new Date().getTime() + ONEDAY;
    if (date <= (now + MINHIRE)) {
        return true;
    }
    return false;
}
function disallowDateAfter(date) {
    date = date.getTime();
    var now = new Date().getTime();
    if (date < (now)) {
        return true;
    }
    return false;
}

function swapTabs(selected) {
    if ($j('#ddCountryOfTravel').val() == '-1') return;
    pudate = document.getElementById('ctrlSearchBox_calPickUp_DateText').value;
    dod = document.getElementById('ctrlSearchBox_calDropOff_DateText').value;
    noa = document.getElementById('ctrlSearchBox_dropAdults').value;
    noc = document.getElementById('ctrlSearchBox_dropChildren').value;    
    pul = $j('#ctrlSearchBox_dropPickUpLocation').val();
    dol = $j('#ctrlSearchBox_dropDropOffLocation').val();    
    cr = $j('#ctrlSearchBox_dropCountryOfResidence').val();
    vh = $j('#vehicleCode').val();
    var url = CallBackURL + "/bookingcontrolpanel.aspx?vt=" + selected + "&cc=" + cot + "&vh=" + vh + "&pul=" + pul + "&dol=" + dol + "&brand=" + brandStr;
    $j('#ctrlSearchBox_up1').load(url, initControl);
}

function enablePanel(selectElm) {
    var cc = $j(selectElm).val();
    if (cc == '-1') cc = '';
    var vt = $j('#vehicleType').val();
    var url = CallBackURL + "/bookingcontrolpanel.aspx?vt=" + vt + "&cc=" + cc + "&brand=" + brandStr;
    $j('#ctrlSearchBox_up1').load(url, initControl);
}

function getParams() {
    var responseText = '';
    var pickupdateStr = $j('#ctrlSearchBox_calPickUp_DateText').val();
    var dropoffDateStr = $j('#ctrlSearchBox_calDropOff_DateText').val();

    if ($j('#ddCountryOfTravel').val() == '-1') {
        responseText = 'country of travel';
        promptField($j('#ddCountryOfTravel'), "Required");
        return '';
    }

    if (!validatePeriod(pickupdateStr, dropoffDateStr)) {//failed onblur Date Range validation
        responseText = 'valid period';
        promptField($j('#ctrlSearchBox_calPickUp_DateText'), "Required");
    }
    var pickupdate = Date.parseExact(pickupdateStr, "dd-MMM-yyyy");
    var dropoffDate = Date.parseExact(dropoffDateStr, "dd-MMM-yyyy");

    var nc = $j("#ctrlSearchBox_dropChildren option:selected").val();
    var na = $j("#ctrlSearchBox_dropAdults option:selected").val();
    if (parseInt(na) == 0) {
        responseText += " number of travellers,";
        promptField($j('#ctrlSearchBox_dropAdults'), "Required");
    }

    MAX_PASSENGERS = parseInt($j('#maxTravellers').val(), 10);
    if ((parseInt(na) + parseInt(nc)) > MAX_PASSENGERS) {
        promptField($j('#ctrlSearchBox_dropAdults'), "Total travellers cannot exceed " + MAX_PASSENGERS);
        $j('#ctrlSearchBox_dropChildren').addClass("required");
        responseText += " Total travellers cannot exceed " + MAX_PASSENGERS + ",";
    }

    cr = $j("#ctrlSearchBox_dropCountryOfResidence option:selected").val();
    if (parseInt(cr) == 0) {
        responseText += " country of residence,";
        promptField($j('#ctrlSearchBox_dropCountryOfResidence'), "Required");
    }
    if (responseText != '') return '';
    cc = cot; 
    vc = 'bk';//TODO: swap from config
    ac = "";

    sc = (getSelectedType() == "av" ? "rv" : "car");
    vt = sc;
    
    pc = "";
    ch = "";
    rf = "";
    nc = $j("#ctrlSearchBox_dropChildren option:selected").val();
    pb = $j("#ctrlSearchBox_dropPickUpLocation option:selected").val();
    pd = pickupdate.getDate();
    pm = pickupdate.getMonth() + 1;
    py = pickupdate.getFullYear();
    pt = $j("#ctrlSearchBox_dropPickUpTime option:selected").val();
    db = $j("#ctrlSearchBox_dropDropOffLocation option:selected").val();
    dd = dropoffDate.getDate();
    dm = dropoffDate.getMonth() + 1;
    dy = dropoffDate.getFullYear();
    dt = $j("#ctrlSearchBox_dropDropOffTime option:selected").val();
    
    vh = $j("#ctrlSearchBox_dropVehicleType option:selected").val();
    var getvalues = '';
    if (responseText == '') {
        getvalues = "cc=" + cc + "&vc=" + vc + "&ac=" + ac + "&sc=" + sc + "&pc=" + pc + "&ch=" + ch + "&rf=" + rf + "&na=" + na + "&nc=" + nc + "&cr=" + cr + "&pb=" + pb + "&pd=" + pd + "&pm=" + pm + "&py=" + py + "&pt=" + pt + "&db=" + db + "&dd=" + dd + "&dm=" + dm + "&dy=" + dy + "&dt=" + dt + "&vt=" + vt + "&vh=" + vh;
        return getvalues;
    } else {
        var trimmed = responseText.replace(/,+$/, "");
        return '';
    }
}

function validatePeriod(pickupdateStr, dropoffDateStr) {
    var pickUpDate = Date.parseExact(pickupdateStr, "dd-MMM-yyyy");
    var dropOffDate = Date.parseExact(dropoffDateStr, "dd-MMM-yyyy");
    if (pickupdateStr == null || dropoffDateStr == null || pickupdateStr == '' || dropoffDateStr == '' || pickupdateStr == 'dd-mmm-yyyy' || dropoffDateStr == 'dd-mmm-yyyy' || Date.parseExact(pickupdateStr, "dd-MMM-yyyy") == null || Date.parseExact(dropoffDateStr, "dd-MMM-yyyy") == null)
        return false;
    else {//range validation
        if (dropOffDate.compareTo(pickUpDate) >= 0)
            return true
        else
            return false;
    }
}

function getSelectedType() {
    return ($j('#vehicleTabs li a.on').attr('id') == "campersTab" ? "av" : "ac");   
}

function submitForm() {
    var baseUrl = TargetUrl;
    var params = getParams()
    if (params == '')
        return;
    else
        window.location = baseUrl + "?" + params;
}

//validation
function promptField(fieldElm, msg) {
    var offsetx = 8;
    var offsety = (jQuery.browser.msie ? 0 : 0);
    var top = fieldElm.position().top;
    var left = fieldElm.position().left;
    var width = fieldElm.width();
    var height = fieldElm.height();
    fieldElm.addClass("required");

    var ctrlWidthCss = $j('ul.SearchBox_TableWrapper').css('width');
    var ctrlWidth = parseInt(ctrlWidthCss.substr(0, ctrlWidthCss.indexOf('px')), 10);    

    var styleStr = "top:" + (top + offsety) + "px; left:" + (dxr + ctrlWidth) + "px;height:" + (height + 6) + "px;";
    $j("<div class='requiredMsg' style='" + styleStr + "'><span class='arrow' style='width:10px;height:" + 20 + "px;'></span><span class='text' style='height:" + 18 + "px;'>" + msg + "&nbsp;</span></div>").appendTo('#bookingcontrol');
}

function bindResize() {
    $j(window).bind('resize', function() {
        var newDxr = $j('#bookingcontrol').position().left;
        var delta = (parseInt(dxr, 10) - parseInt(newDxr, 10));
        dxr = newDxr;
        jQuery.each($j('div.requiredMsg'), function(i, val) {
            $j(this).css("left", (parseInt($j(this).css("left")) - delta));
        });
    });
}

function setSelectedVehicle(vehicleCode, vehicleName) {
    if ($j("#ctrlSearchBox_dropVehicleType").containsOption(vehicleCode)) {
        $j("#ctrlSearchBox_dropVehicleType").val(vehicleCode);
    } else {
        $j('#ctrlSearchBox_dropVehicleType').append("<option selected='selected' value=" + vehicleCode + ">" + vehicleName + "</option>");
        $j('#bookingControl').addClass('VehicleMode');
    }
}