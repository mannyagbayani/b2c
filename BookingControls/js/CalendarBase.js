﻿var startDate;
var endDate;
var ONEDAY = 3600 * 24 * 1000;

function filterDates1(cal) {
    var date = cal.date;
    startDate = date.getTime();
    document.getElementById('ctrlSearchBox_dropPickUpTime').focus();
    document.getElementById('ctrlSearchBox_calDropOff_DateText').value = document.getElementById('ctrlSearchBox_calPickUp_DateText').value;
    $j('#ctrlSearchBox_calPickUp_DateText').removeClass('required');
    $j("div.requiredMsg").remove();
}
function filterDates2(cal) {
    var date = cal.date;
    endDate = date.getTime();
    document.getElementById('ctrlSearchBox_dropDropOffTime').focus();
    if (document.getElementById('ctrlSearchBox_calDropOff_DateText').value == 'dd/mm/yyyy')
        document.getElementById('ctrlSearchBox_calDropOff_DateText').value = document.getElementById('ctrlSearchBox_calPickUp_DateText').value;
    $j('#ctrlSearchBox_calDropOff_DateText').removeClass('required');
    $j("div.requiredMsg").remove();
}

function disallowDateBefore(date) {
    date = date.getTime();
    var MINHIRE ;
    MINHIRE = ONEDAY * -1;
    if ((startDate != null) && (date <= (startDate + MINHIRE))) {
        return true;
    }
    var now = new Date().getTime() + ONEDAY;
    if (date <= (now + MINHIRE)) {
        return true;
    }
    return false;
}
function disallowDateAfter(date) {
    date = date.getTime();
    var now = new Date().getTime();
    if (date < (now)) {
        return true;
    }
    return false;
}