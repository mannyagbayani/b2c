﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using THL.Booking;

public partial class QuoteConfirmation : System.Web.UI.Page
{

    public string debugURL;

    public string ContentURL, AssetsURL, BookingId;
    public string BrandChar, BrandNameStr;
    public string CustomerDetails, RentalSummary, CustomerEMail;

    public string TotalConversionAmount, ConversionId, ConversionCode;

    public string HTTPPrefix;

    public string ChargeSummaryPanel, PaymentSummaryPanel;

    public string DNAIMGTag, AnalyticsCode;

    public bool OnRequestMode = false;

    public string SSLSeal, ParentSiteURL;

    public string ContactHeaderText, WorldWidePhone, DomesticPhone;

    public string PaymentHeader;

    public string BlankConfirmation = string.Empty;

    public AvailabilityItem AvailItem;
    public AuroraBooking BookingItem;
    public AvailabilityRequest ReqItem;

    public bool HasAdminFee = false;

    public string RequestQueryParams;

    public string VehicleName = string.Empty;

    public string PassengersStr, PickUpStr, DropOffStr, CurrencyStr;

    public string XaxisConfirmation;
    public string RemarketingCode { get; set; }
    THLDomain appDomain;
    public string OpenTag { get; set; }
    /// <summary>
    /// Support release version from config for forcing static assets cache refresh.
    /// </summary>
    public string Version
    {
        get { return ApplicationManager.ReleaseVersion; }
        set { value = ApplicationManager.ReleaseVersion; }
    }

    public string ContactUsURL
    {
        get
        {
            THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
            string contactUrl = appDomain.ParentSite + "/contactus";
            switch (appDomain.ParentSite)
            {
                case "keacampers.co.nz":
                    contactUrl = "http://nzrentals.keacampers.com/en/contact/contact_us.aspx";
                    break;
                case "keacampers.com.au":
                    contactUrl = "http://nzrentals.keacampers.com/en/contact/contact_us.aspx";
                    break;
            }
            //Following condition was added by Nimesh on 24th Sep 2014 to redirect contact us for auropcarb2c
            if (SessionManager.AgentCode != null)
            if (SessionManager.AgentCode.ToLower().Equals("europcb2c"))
                contactUrl = "http://www.europcar.com.au/campervan/enquiry-form";
            return contactUrl;
        }
    }

    public string Currency
    {
        //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
        //get
        //{
        //    appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
        //    return CurrencyManager.GetCurrencyForCountryCode(appDomain.Country.ToString()).ToString();
        //}
        get;
        set;
    }

    public string CSSLink { get; set; }    

    protected void Page_Load(object sender, EventArgs e)
    {
        OpenTag = string.Empty;
        try
        {
            bool secureMode = false;//TODO: move to config manager
            bool.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["SecureMode"], out secureMode);

            HTTPPrefix = ApplicationManager.HTTPPrefix;

            THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
            if (appDomain == null)//dubug line
                appDomain = new THLDomain("secure.maui.co.nz", THLBrands.Maui, CountryCode.NZ, "B2CNZ", false, "uahere");//dubug line
            BrandChar = appDomain.BrandChar.ToString(); //(Request.Params["brand"] != null ? Request.Params["brand"] : "m");//TODO: this should be swapped by baseURL. 
            
            //Added by nimesh below on 25/07/2014 by nimesh as it was not picking up agent code
            string agentCode = "mac";

            if (!string.IsNullOrEmpty(SessionManager.AgentCode))
            {
                string acCode = SessionManager.AgentCode.ToLower();
                BrandChar = /*Request.Params["ac"].ToLower();//*/ AgentTheme.CSSPath(acCode, BrandChar);
                //agentCode = SessionManager.AgentCode.ToLower();

            }
            else
            {
                agentCode = appDomain.AgentCode;
            }
            // end Added by nimesh on 25/07/2014

            ParentSiteURL = appDomain.ParentSite + "/contactus";
            //Following condition was added by Nimesh on 24th Sep 2014 to redirect contact us for auropcarb2c
            if (SessionManager.AgentCode != null)
            if (SessionManager.AgentCode.ToLower().Equals("europcb2c"))
                ParentSiteURL = "http://www.europcar.com.au/campervan/enquiry-form";
            
            WorldWidePhone = ContentManager.GetMessageForCode(THLBrand.GetBrandForString(BrandChar), appDomain.Country, "InternationalPhone").Content;
            DomesticPhone = ContentManager.GetMessageForCode(THLBrand.GetBrandForString(BrandChar), appDomain.Country, "DomesticPhone").Content;

            ContactHeaderText = ContentManager.GetMessageForCode(THLBrand.GetBrandForString(BrandChar), appDomain.Country, "ContactText").Content + "<br />"
                                + DomesticPhone + " - "
                                + WorldWidePhone;

            ContentURL = HTTPPrefix + appDomain.Name + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["BookingControlContentPath"];
            string countryCode = appDomain.Country.ToString().ToUpper();

            BrandNameStr = THLBrand.GetNameForBrand(appDomain.Brand) + " " + AvailabilityHelper.GetCountryName(appDomain.Country);

            SessionData sessionData = SessionManager.GetSessionData();

            if (!sessionData.HasBookingData || !sessionData.HasRequestData || !sessionData.HasResponseData)//protect UI on expired session
                Response.Redirect("Selection.aspx?msg=sessionExpired");

            AssetsURL = HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];

            BookingId = (Request.Params["bookingId"] != null ? Request.Params["bookingId"] : null);
            if (BookingId != null)
            {                
                if (sessionData.HasRequestData && sessionData.HasResponseData && sessionData.HasBookingData)//has session data with a booking in it, TODO: verify booking stage and match ID with BookingId
                {
                    ApplicationData appData = ApplicationManager.GetApplicationData();
                    BookingItem = sessionData.BookingData;

                    OnRequestMode = (BookingItem.AvailabilityType != AvailabilityItemType.Available);
                    PaymentHeader = (OnRequestMode ? "Booking Request Submitted" : "Booking Completed");

                    ReqItem = sessionData.AvailabilityRequestData;
                    AvailItem = sessionData.AvailabilityResponseData.GetAvailabilityItem(BookingItem.AVPID);

                    VehicleName = (AvailItem != null ? AvailItem.getVehicleNameFromPackage() : string.Empty);

                    if ((appDomain.Brand == THLBrands.AIRNZ || appDomain.Brand == THLBrands.MAC) && !OnRequestMode)
                    {
                        BrandNameStr = THLBrand.GetNameForBrand(AvailItem.Brand) + " " + AvailabilityHelper.GetCountryName(appDomain.Country);
                        ParentSiteURL = ApplicationManager.GetParentSiteForBrand(AvailItem.Brand, ReqItem.CountryCode) + "/contactus";
                        //Following condition was added by Nimesh on 24th Sep 2014 to redirect contact us for auropcarb2c
                        if (SessionManager.AgentCode != null)
                        if (SessionManager.AgentCode.ToLower().Equals("europcb2c"))
                            ParentSiteURL = "http://www.europcar.com.au/campervan/enquiry-form";
                        
                    }

                    Customer customer = BookingItem.PaymentInformation.Customer;
                    UserDataDisplayer usd = new UserDataDisplayer();
                    CustomerDetails = usd.RenderCustomerDetails(customer);

                    PassengersStr = ReqItem.NumberOfAdults + " adult(s)" + (ReqItem.NumberOfChildren > 0 ? " and " + ReqItem.NumberOfChildren + " children" : string.Empty);
                    PickUpStr = ReqItem.PickUpDate.ToString("dddd, dd MMMM yyyy HH:mm tt") + "<br />" + BookingItem.PickUpStr;
                    DropOffStr = ReqItem.DropOffDate.ToString("dddd, dd MMMM yyyy HH:mm tt") + "<br />" + BookingItem.DropOffStr;
                    
                    //CurrencyStr = appDomain.Country.ToString().ToUpper() + "D";// QuoteConfirmationMode ? quote.CurrencyCode : sessionData.AvailabilityResponseData.Currency.ToString();
                    CurrencyStr = (ReqItem.CountryCode == CountryCode.NZ ? "NZD" : "AUD"); 

                    //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
                    Currency = CurrencyStr;

                    CustomerEMail = customer.EMail;

                    ChargeSummaryPanel = RenderRentalConfirmationChargeSummary(BookingItem, AvailItem);

                    if (OnRequestMode)
                        PaymentSummaryPanel = "<div>Full payment is required on confirmation of your booking.</div>";
                    else
                        PaymentSummaryPanel = RenderPaymentDetailsConfirmation(BookingItem, AvailItem);

                    //DecideDNA Tracking Img, TODO(added 12/07): add currency (CurrencyStr) : 
                    string transactionParams = "value=" + getTotalAmount(BookingItem, AvailItem).ToString("F") + "&trans=" + BookingId + "&currency=" + CurrencyStr;
                    TrackingDisplayer trd = new TrackingDisplayer(appDomain, secureMode);
                    DNAIMGTag = trd.RenderDNATrackingImg("quoteConfirmation", transactionParams);

                    //marin tracking
                    string currencyStr = string.Empty;
                    if (appDomain.IsAggregator)
                        currencyStr = (ReqItem.CountryCode.Equals(CountryCode.AU) ? "AUD" : "NZD");
                    DNAIMGTag += trd.RenderMarinTrackingPixelIMG(SessionManager.MarinTrackingParams, MarinStep.QUOTECONF, getTotalAmount(BookingItem, AvailItem), BookingId, currencyStr);
                    
                    string pickUpBranchStr = (appData.GetBranchForCode(ReqItem.PickUpBranch) != null ? appData.GetBranchForCode(ReqItem.PickUpBranch).ZoneName : ReqItem.PickUpBranch);
                                        
                    SSLSeal = trd.GetSealID();
                    TrackingData adWordsElm = trd.GetAdWordsElement();
                    ConversionId = (adWordsElm != null && adWordsElm.ID != null && adWordsElm.ID.Length > 0 ? adWordsElm.ID : "0");
                    RemarketingCode = trd.RenderRemarketingSnippets();

                    //Analytics Tracking Code with save Quote Event Attached
                    int totalBookingRounded = PaymentHelper.DecimalToInt(totalAmountWithAdditionals, "BookingtoAnalyticsEvent");
                    AnalyticsEventTrackingItem[] quoteEvents = { new AnalyticsEventTrackingItem(ReqItem.VehicleType.ToString(), "Save Quote", BookingId, totalBookingRounded) };
                    AnalyticsCode = trd.RenderAnalsCode(quoteEvents);

                    XaxisConfirmation = RenderXaxisConfirmation(appDomain, "quote", totalBookingRounded, BookingId);
                    
                    
                    //------------------------------------------------------
                                       
                    if (adWordsElm != null && adWordsElm.ID.Length > 0)
                    {
                        TotalConversionAmount = "";// getTotalAmount(BookingItem, AvailItem).ToString("N2");
                        ConversionId = (adWordsElm.ID != null && adWordsElm.ID.Length > 0 ? adWordsElm.ID : "0");
                        ConversionCode = adWordsElm.Code;
                    }
                    RequestQueryParams = ReqItem.GenerateSelectionParamsURL(false);//Search Again Functionality,added 30/07/2010
                }
            }
            else
            {
                Response.Redirect("Selection.aspx?msg=sessionExpired");
            }

            //-------------------------------- Debug Zone -------------------------
            debugURL = (Request.Params["debugOn"] != null && Request.Params["debugOn"] == "true" ? "unittests/showbookinginfo.aspx?ts=" + DateTime.Now.Millisecond : string.Empty);
            //-------------------------------- Debug Zone -------------------------

            OpenTag = AvailabilityHelper.LoadOpenTag(appDomain);
        }
        catch (Exception ex)
        {
            string ErrMsg = ex.Message;
            BookingId = (Request.Params["bookingId"] != null ? Request.Params["bookingId"] : string.Empty);
            if (BookingId.Length > 0)
            {
                BlankConfirmation = "BlankConfirmation";
                bool secureMode = false;//TODO: move to config manager
                bool.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["SecureMode"], out secureMode);
                THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
                HTTPPrefix = ApplicationManager.HTTPPrefix;
                ContentURL = HTTPPrefix + appDomain.Name + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["BookingControlContentPath"];
                AssetsURL = HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];
                ApplicationData appData = ApplicationManager.GetApplicationData();
                if (appDomain == null)//dubug line
                    appDomain = new THLDomain("secure.maui.co.nz", THLBrands.Maui, CountryCode.NZ, "B2CNZ", false, "uahere");//dubug line
                BrandChar = appDomain.BrandChar.ToString(); //(Request.Params["brand"] != null ? Request.Params["brand"] : "m");//TODO: this should be swapped by baseURL. 
                ParentSiteURL = appDomain.ParentSite + "/contactus";
                //Following condition was added by Nimesh on 24th Sep 2014 to redirect contact us for auropcarb2c
                if (SessionManager.AgentCode != null)
                if (SessionManager.AgentCode.ToLower().Equals("europcb2c"))
                    ParentSiteURL = "http://www.europcar.com.au/campervan/enquiry-form";
                if ((appDomain.Brand == THLBrands.AIRNZ || appDomain.Brand == THLBrands.MAC) && !OnRequestMode)
                {
                    //BrandNameStr = THLBrand.GetNameForBrand(AvailItem.Brand) + " " + AvailabilityHelper.GetCountryName(appDomain.Country);
                    //ParentSiteURL = ApplicationManager.GetParentSiteForBrand(AvailItem.Brand, ReqItem.CountryCode);
                }
                PassengersStr = string.Empty;
                PickUpStr = string.Empty;
                DropOffStr = string.Empty;
                CurrencyStr = string.Empty;
                CustomerEMail = " Your Email Address";
                ChargeSummaryPanel = "this booking is done!!";
                //DecideDNA Tracking Img
                string transactionParams = "value=" + "0" + "&trans=" + BookingId;
                TrackingDisplayer trd = new TrackingDisplayer(appDomain, secureMode);

                //marin tracking
                DNAIMGTag += trd.RenderMarinTrackingPixelIMG(SessionManager.MarinTrackingParams, MarinStep.QUOTECONF, 0.0m, string.Empty, string.Empty);
        
                //Analytics Tracking Code
                TrackingData td = ApplicationManager.GetTrackingElementForDomainEvent(appDomain.Name, "GoogleAnalytics");
                AnalyticsCode = ""; //avoid multiple triggers.
                SSLSeal = trd.GetSealID();
                TotalConversionAmount = "";
                ConversionId = "0";
                ConversionCode = "";
            }
            else
            {
                Response.Redirect("Selection.aspx?msg=sessionExpired");
            }
        }

        
    }




    /// <summary>
    /// Internal calc for google adWords
    /// </summary>
    /// <param name="bookingData"></param>
    /// <param name="availabilityRow"></param>
    /// <returns></returns>
    decimal getTotalPayment(AuroraBooking bookingData, AvailabilityItem availabilityRow)
    {
        decimal surchargeAmount = bookingData.PaymentInformation.PaymentSurchargeAmount;

        decimal totalAmountWithAdditionals = (availabilityRow.EstimatedTotal + bookingData.GetTotalNonVehicleCharges(ProductType.All));
        if (bookingData.BonusPackSelected && bookingData.InclusivePack != null)
            totalAmountWithAdditionals += bookingData.InclusivePack.GrossAmount;

        decimal adminFeeComponent = (bookingData.AdminFee / 100) * totalAmountWithAdditionals;

        if (bookingData.AdminFee > 0 && !bookingData.BonusPackSelected)
        {//admin fee
            totalAmountWithAdditionals += adminFeeComponent;
            HasAdminFee = true;
        }
        return totalAmountWithAdditionals;
    }

    decimal totalAmountWithAdditionals;

    string RenderRentalConfirmationChargeSummary(AuroraBooking bookingData, AvailabilityItem availabilityRow)
    {
        string currencyStr = CurrencyStr;//CurrencyManager.GetCurrencyForCountryCode(bookingData.CountryCode).ToString();
        decimal surchargeAmount = bookingData.PaymentInformation.PaymentSurchargeAmount;

        totalAmountWithAdditionals = (availabilityRow.EstimatedTotal + bookingData.GetTotalNonVehicleCharges(ProductType.All));
        if (bookingData.BonusPackSelected && bookingData.InclusivePack != null)
            totalAmountWithAdditionals += bookingData.InclusivePack.GrossAmount;

        //031109
        decimal oneWayFeeWaiver = (bookingData.WaiveOneWayFee && bookingData.OneWayFeeComponent > 0 ? bookingData.OneWayFeeComponent : 0.0m);
        if (bookingData.BonusPackSelected)
            totalAmountWithAdditionals -= oneWayFeeWaiver;
        //031109
        
        decimal adminFeeComponent = (bookingData.AdminFee / 100) * totalAmountWithAdditionals;

        if (bookingData.AdminFee > 0 && !bookingData.BonusPackSelected)
        {//admin fee
            totalAmountWithAdditionals += adminFeeComponent;
            HasAdminFee = true;
        }


        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //---------------------
        int colSpans = 4;
        bool hasFreeDays = false;
        AvailabilityItemChargeRateBand[] availabilityRateBands = availabilityRow.GetRateBands();
        AvailabilityItemChargeDiscount[] aidc = availabilityRow.GetDiscounts();
        bool hasDiscount = false;
        colSpans = (hasDiscount || hasFreeDays ? colSpans : --colSpans);
        if (availabilityRateBands != null && availabilityRateBands.Length > 0)
            foreach (AvailabilityItemChargeRateBand chargeRateRow in availabilityRateBands) //order?
                if (chargeRateRow.IncludesFreeDay > 0)
                    hasFreeDays = true;
        sb.Append("<table class='chargesTable'>");
        sb.Append("<tbody>");

        decimal longHireDiscount = availabilityRow.GetLongHireDiscount();
        decimal totalCharge = availabilityRow.GetTotalPrice();
        if (availabilityRateBands != null)//has several rate bands
        {
            foreach (AvailabilityItemChargeRateBand chargeRateRow in availabilityRateBands)
            {
                string fromDateStr = chargeRateRow.FromDate.ToString("dd-MMM-yyyy");
                string toDateStr = chargeRateRow.ToDate.ToString("dd-MMM-yyyy");
                sb.Append("<tr>");
                sb.Append("<td class='rt'>Vehicle Charge - Rate Period " + fromDateStr + " to " + toDateStr + "</td>");
                sb.Append("<td class='rp'>" + chargeRateRow.HirePeriod + " day(s) x " + currencyStr + "  $" + chargeRateRow.DiscountedRate + " per day</td>");
                sb.Append("<td class='cur'>" + currencyStr + "</td>");
                sb.Append("<td class='price'>$" + chargeRateRow.GrossAmount.ToString("F") + "</td>");
                sb.Append("</tr>");
            }
        }
        else //single rate band, TODO: revisit formating
        {
            AvailabilityItemChargeRow vehicleRow = availabilityRow.GetVehicleChargeRow();
            sb.Append("<tr>");
            sb.Append("<td class='rt'>" + vehicleRow.ProductName + "</td>");
            sb.Append("<td class='rp'>" + vehicleRow.HirePeriod + " day(s) x " + currencyStr + " $" + vehicleRow.AverageDailyRate + " per day</td>");
            sb.Append("<td class='cur'>" + currencyStr + "</td>");
            sb.Append("<td class='price'>$" + vehicleRow.ProductPrice + "</td>");
            sb.Append("</tr>");
        }

        AvailabilityItemChargeRow[] aCharges = availabilityRow.GetNonVehicleCharges();
        if (aCharges != null && aCharges.Length > 0)
        {
            foreach (AvailabilityItemChargeRow charge in aCharges)
            {
                if (charge.IsBonusPack) continue;


                if (charge.AdminFee > 0)
                {
                    if (!bookingData.BonusPackSelected)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>" + charge.AdminFee + "% Administration fee </td>");
                        sb.Append("<td></td>");
                        sb.Append("<td class='cur'>" + currencyStr + "</td>");
                        sb.Append("<td class='price'>$" + adminFeeComponent.ToString("F") + "</td>");
                        sb.Append("</tr>");
                    }
                    continue;
                }

                if (charge.HirePeriod > 1)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + charge.ProductName + "</td>");
                    sb.Append("<td class='hp'>" + charge.HirePeriod + " day(s)</td>");
                    sb.Append("<td class='pdp'>$" + charge.AverageDailyRate.ToString("F") + "</td>");
                    sb.Append("<td class='cur'>" + currencyStr + "</td>");
                    sb.Append("<td class='price'>$" + charge.ProductPrice + "</td>");
                    sb.Append("</tr>");
                }
                else
                {
                    sb.Append("<tr rel='" + charge.ProductCode + "'>");
                    sb.Append("<td colspan='2'>" + charge.ProductName + "</td>");
                    sb.Append("<td class='cur'>" + currencyStr + "</td>");
                    sb.Append("<td class='price'>" + (charge.ProductPrice == 0m ? "included" : "$" + charge.ProductPrice.ToString("F")) + "</td>");
                    sb.Append("</tr>");
                }
            }
        }

        //Add Additional Hire Items
        AuroraProduct[] additionalProducts = bookingData.GetBookingProducts(ProductType.All);
        if (additionalProducts != null && additionalProducts.Length > 0)
        {
            foreach (AuroraProduct product in additionalProducts)
            {
                sb.Append("<tr>");
                sb.Append("<td class='title'>" + product.SelectedAmount + " x " + product.Name + "</td>");
                sb.Append("<td class='desc'>" + product.SelectedAmount + " @ " + currencyStr + " $ " + product.GrossAmount.ToString("N2") + " each</td>");
                sb.Append("<td class='cur'>" + currencyStr + "</td>");
                sb.Append("<td class='price'>$" + (product.SelectedAmount * product.GrossAmount).ToString("N2") + "</td>");
                sb.Append("</tr>");
            }
        }

        //TODO: include bpack: if (bookingData.BonusPackSelected) ;
        if (bookingData.BonusPackSelected)//removed 01/10
        {
            //InsuranceProduct inclusivePack = bookingData.InclusivePack;
            //sb.Append("<tr>");
            //sb.Append("<td class='title'>1 x " + inclusivePack.Name + "</td>");
            //sb.Append("<td class='desc'>" + bookingData.HirePeriod + " day(s) @" + currencyStr + " $ " + inclusivePack.DailyRate.ToString("N2") + " each</td>");
            //sb.Append("<td class='cur'>" + currencyStr + "</td>");
            //sb.Append("<td class='price'>$" + inclusivePack.GrossAmount.ToString("N2") + "</td>");
            //sb.Append("</tr>");
        }


        decimal totalAmountWithAdditionalsAndSCharge = totalAmountWithAdditionals + surchargeAmount;
        decimal totalDepositWithSCharge = (surchargeAmount + availabilityRow.DepositAmount);
        decimal finalDuePickUp = (totalAmountWithAdditionals - availabilityRow.DepositAmount);

        sb.Append("<tr class='total'>");
        sb.Append("<td class='title'>");
        sb.Append("<small>Total</small>");
        sb.Append("</td>");
        sb.Append("<td class='desc'>" + /*(HasAdminFee ? "(includes " + bookingData.AdminFee + "% admin fee)" : string.Empty) +*/ "</td>");
        sb.Append("<td class='cur'><small>" + currencyStr + "</small></td>");
        sb.Append("<td class='price'><small>$" + totalAmountWithAdditionals.ToString("N2") + "</small></td>");//Total whole for booking
        sb.Append("</tr>");
        sb.Append("</tbody>");
        sb.Append("</table>");

        string result = sb.ToString();
        return result;
    }

    string RenderPaymentDetailsConfirmation(AuroraBooking bookingData, AvailabilityItem availabilityRow)
    {
        string currencyStr = CurrencyStr;//CurrencyManager.GetCurrencyForCountryCode(bookingData.CountryCode).ToString();
        decimal surchargeAmount = bookingData.PaymentInformation.PaymentSurchargeAmount;

        decimal totalAmountWithAdditionals = (availabilityRow.EstimatedTotal + bookingData.GetTotalNonVehicleCharges(ProductType.All));
        if (bookingData.BonusPackSelected && bookingData.InclusivePack != null)
            totalAmountWithAdditionals += bookingData.InclusivePack.GrossAmount;

        //031109
        decimal oneWayFeeWaiver = (bookingData.WaiveOneWayFee && bookingData.OneWayFeeComponent > 0 && bookingData.BonusPackSelected ? bookingData.OneWayFeeComponent : 0.0m);
        totalAmountWithAdditionals -= oneWayFeeWaiver;
        //031109

        decimal adminFeeComponent = (bookingData.AdminFee / 100) * totalAmountWithAdditionals;

        if (bookingData.AdminFee > 0 && !bookingData.BonusPackSelected)
        {//admin fee
            totalAmountWithAdditionals += adminFeeComponent;
            HasAdminFee = true;
        }


        //decimal surchargeAmount = bookingData.PaymentInformation.PaymentSurchargeAmount;
        //decimal totalAmountWithAdditionals = (availabilityRow.EstimatedTotal + bookingData.GetTotalNonVehicleCharges(ProductType.All));
        //if (bookingData.BonusPackSelected && bookingData.InclusivePack != null)
        //    totalAmountWithAdditionals += bookingData.InclusivePack.GrossAmount;
        //if (bookingData.AdminFee > 0)
        //{//admin fee
        //    totalAmountWithAdditionals += (bookingData.AdminFee / 100) * totalAmountWithAdditionals;
        //    HasAdminFee = true;
        //}
        //decimal totalAmountWithAdditionalsAndSCharge = totalAmountWithAdditionals + surchargeAmount;
        //decimal totalDepositWithSCharge = (surchargeAmount + availabilityRow.DepositAmount);
        decimal finalDuePickUp = (totalAmountWithAdditionals - availabilityRow.DepositAmount);

        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append("<table>");
        if (!bookingData.PayedFull)//has a deposit component
        {
            sb.Append("<tr>");
            sb.Append("<td class='rt'>Deposit</td>");
            sb.Append("<td class='rp'>20% of Vehicle Charge</td>");
            sb.Append("<td class='cur'>" + CurrencyStr + "</td>");
            sb.Append("<td class='price'>$" + availabilityRow.DepositAmount + "</td>");
            sb.Append("</tr>");
        }
        else
        {
            sb.Append("<tr>");
            sb.Append("<td class='total'>Total Payment</td>");
            sb.Append("<td class='rp'></td>");
            sb.Append("<td class='cur'>" + CurrencyStr + "</td>");
            sb.Append("<td class='price'>$" + totalAmountWithAdditionals.ToString("F") + "</td>");
            sb.Append("</tr>");
        }
        //bookingData.PaymentInformation.PaymentSurchargeAmount.ToString("F")
        sb.Append("<tr>");
        sb.Append("<td class='rt'>Credit Card Surcharge</td>");
        sb.Append("<td class='rp'>" + surchargeAmount.ToString("F") + "% of " + (bookingData.PayedFull ? "Total Price" : "Deposit") + "</td>");
        sb.Append("<td class='cur'>" + CurrencyStr + "</td>");
        sb.Append("<td class='price'>$" + (bookingData.PayedFull ? (totalAmountWithAdditionals * (surchargeAmount / 100)).ToString("F") : (availabilityRow.DepositAmount * (surchargeAmount / 100)).ToString("F")) + "</td>");
        sb.Append("</tr>");

        sb.Append("<tr class='total'>");
        sb.Append("<td colspan='2'>Total Paid</td>");
        sb.Append("<td class='cur'>" + CurrencyStr + "</td>");
        sb.Append("<td class='price'>$" + bookingData.PaymentInformation.PaymentAmount.ToString("F") + "</td>");
        sb.Append("</tr>");

        if (!bookingData.PayedFull)
        {
            sb.Append("<tr>");
            sb.Append("<td colspan='2'>Final balance (payable on pick-up)</td>");
            sb.Append("<td class='cur'>" + CurrencyStr + "</td>");
            sb.Append("<td class='price'>$" + finalDuePickUp.ToString("F") + "</td>");
            sb.Append("</tr>");
            sb.Append("<tr><td colspan='3'>If paying final balance on pick up by credit card, the credit card surcharge will apply.</td></tr>");
        }
        sb.Append("</table>");
        return sb.ToString();
    }

    string RenderContactText(THLDomain domain)
    {
        StringBuilder sb = new StringBuilder();
        return sb.ToString();
    }


    decimal getTotalAmount(AuroraBooking bookingData, AvailabilityItem availabilityRow)
    {
        decimal totalAmountWithAdditionals = (availabilityRow.EstimatedTotal + bookingData.GetTotalNonVehicleCharges(ProductType.All));
        if (bookingData.BonusPackSelected && bookingData.InclusivePack != null)
            totalAmountWithAdditionals += bookingData.InclusivePack.GrossAmount;

        if (bookingData.AdminFee > 0)
        {//admin fee
            totalAmountWithAdditionals += (bookingData.AdminFee / 100) * totalAmountWithAdditionals;
            HasAdminFee = true;
        }
        return totalAmountWithAdditionals;
    }


    /// <summary>
    /// Render Confirmation tracker for Xaxis Transaction completion
    /// </summary>
    /// <param name="domain">the domain this snippet is generating</param>
    /// <param name="step">quote or booking</param>
    /// <returns></returns> 
    public static string RenderXaxisConfirmation(THLDomain domain, string step, decimal orderValue, string bookingId)
    {

        if (domain.ParentSite != "http://www.britz.co.nz" && domain.ParentSite != "http://www.britz.com.au")
            return string.Empty;

        string clientID = string.Empty;
        if (!string.IsNullOrEmpty(step) && step.ToLower().Equals("quote"))
            clientID = (domain.ParentSite == "http://www.britz.co.nz" ? "160626" : "160625");
        else
            clientID = (domain.ParentSite == "http://www.britz.co.nz" ? "159370" : "159366");

        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append(@"
            <!-- Post Click Tracking Location: THL - Britz " + domain.Country + @" THL - Britz - Saved Quotes AU -->
            <script type=""text/javascript"">
            <!--
                var dd = new Date();
                var ord = Math.round(Math.abs(Math.sin(dd.getTime()))*1000000000)%10000000;
                var fd_pct_src = new String(""<scr""+""ipt src=\""https://adsfac.net/pct_mx.asp?L=" + clientID + @"&source=js&udp1=" + bookingId + @"&ta=" + orderValue + @"&ord=""+ord+""\"" type=\""text/javascript\""></scr""+""ipt>"");
                document.write(fd_pct_src);
            -->
            </script>
            <noscript>
                <iframe frameborder=""0"" width=""0"" height=""0"" src=""https://adsfac.net/pct_mx.asp?L=" + clientID + @"&udp1=" + bookingId + @"&ta=" + orderValue + @"&source=if""></iframe>
            </noscript>
        ");
        return sb.ToString();
    }


}
