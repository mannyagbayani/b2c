﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class THLGenericWidget : System.Web.UI.Page
{
    public string JSONParams;

    public bool HasModel = false;

    AvailabilityRequest availabilityRequest;

    public int numMonths;

    public string CountryOfTravel;

    public string CSSLocations;

    public string AnalyticsCode;

    public bool SplashMode { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        SplashMode = string.IsNullOrEmpty(Request.Params["cc"]) || string.IsNullOrEmpty(Request.Params["brand"]);

        availabilityRequest = AvailabilityHelper.GetAvailabilityRequestForWidgetParams(Request.Params);
        THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
        ApplicationData appData = ApplicationManager.GetApplicationData();
        Branch[] branches = appData.GetWidgetBranches();
        Vehicle[] vehicles = appData.Vehicles;
        
        numMonths = 2;
        int.TryParse(Request.Params["months"], out numMonths);

        if(!string.IsNullOrEmpty(Request.Params["vc"]))
        {
            HasModel = true;
        }

        CountryOfTravel = appDomain.Country.ToString();

        CSSLocations = !string.IsNullOrEmpty(Request.Params["cssb"]) ? Request.Params["cssb"] : "/css/GenericWidget.css";

        JSONParams = AvailabilityHelper.GetWidgetJSONForRequest(availabilityRequest, branches, vehicles);
        
        //Handle External Requests
        if(!string.IsNullOrEmpty(Request.Params["service"]) && Request.Params["service"].ToLower().Equals("json"))
        {
            if (!string.IsNullOrEmpty(Request.Params["cb"]))
                JSONParams = Request.Params["cb"] + "(" + JSONParams + ")";
            Response.ContentType = "application/json";
            Response.Write(JSONParams);
            Response.End();
        }
        else if (!string.IsNullOrEmpty(Request.Params["service"]) && Request.Params["service"].ToLower().Equals("xml"))
        {
            Response.ContentType = "text/xml";
            Response.Write(AvailabilityHelper.GetWidgetXMLForRequest(availabilityRequest, branches, vehicles).Replace("&","&amp;"));
            Response.End();
        }


        //Render Analytics Segment for THL sourced sites
        if (string.IsNullOrEmpty(availabilityRequest.AgentCode))
        {
            AnalyticsEventTrackingItem[] availTrackingItems = new AnalyticsEventTrackingItem[0] { };
            TrackingDisplayer trd = new TrackingDisplayer(appDomain, true/*secureMode*/);
            if (availTrackingItems != null && availTrackingItems.Length > 0 && appDomain.Brand != THLBrands.AIRNZ)
                AnalyticsCode = trd.RenderAnalsCode(availTrackingItems);
            else
                AnalyticsCode = trd.RenderAnalsCode();
        }

    }
}