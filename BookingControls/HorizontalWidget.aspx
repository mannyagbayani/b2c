﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HorizontalWidget.aspx.cs" Inherits="THLHorizontalWidget" %>
<!DOCTYPE html>
<html>
<head>
    <title>THL Horizontal Booking Widget</title>
    <link rel="stylesheet" type="text/css" media="all" href="/css/y/widget_h.css" />
    <script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="/js/jquery.validate-1.9.0.js"></script>
    <script type="text/javascript" src="/js/jquery-ui-1.8.0.min.js"></script>
    <script type="text/javascript" src="/js/date.js"></script>
    <script type="text/javascript" src="/js/thlwidget.js"></script>
    <style type="text/css">
        li.VehicleModel.Selected { background-color: #fff;}        
        label.error { display: none !important; } /* possible styling point */
        input.error, select.error { background-color: #ffe57f;  }
		select, input { font-size: 13px !important; }
    </style>    
    <script type="text/javascript">
        var ctrlParams = <%= JSONParams %>; 
        var ctrlUI = {"numMonth": <%= numMonths %>};               
        jQuery(document).ready(function () {            
            $("#form1").validate({
		        errorLabelContainer: $("#form1 div.error")
	        });            
            
            $("#form1 select, #form1 input").change(function() { $(this).removeClass('error'); });

            initDates();
            updateCtrl();
            $("#vehicleTabs a").click(function(e){
                $("#vehicleTabs a").removeClass('on');
                var elm = $(this);
                elm.addClass('on');
                ctrlParams.state.type = (elm.hasClass("cars") ? "car" : "campervan");
                updateCtrl();
                e.stopPropagation();
                e.preventDefault();
            });         
        });              
    </script> 
    <%= AnalyticsCode %>      
</head>
<body>
    <form id="form1" runat="server" onsubmit="return false">
    <div id="ctrlSearchBox_up1">
    <div class="SearchBox_Wrapper" id="bookingcontrol">
    	
        <div id="quoteTitle">
        	<img src="/images/y/quotetop.png" width="488" height="86" alt="Get a Quote" />
        </div>
    
        <div class="SearchBox_Tabs">
            <ul id="vehicleTabs">
                <li><a id="campersTab" class="on campers" href="#"><span>BOOK A CAMPER</span></a></li>
                <li><a id="carsTab" class="cars" href="#"><span>BOOK A CAR</span></a></li>
            </ul>
        </div>
        <div class="SearchBox_Content" id="ctrlSearchBox_divContent">
            <ul class="SearchBox_TableWrapper">
                <li class="CountryOfTravel">
                    <p>
                        <label>Country of travel:</label>                       
                        <select class="SearchBox_FullListBox" id="ddCountryOfTravel" onchange="javascript:enablePanel(this);" name="ddCountryOfTravel">
                            <option value="-1" selected="selected">Please select</option>
                            <option value="au">Australia</option>
                            <option value="nz">New Zealand</option>
                        </select>
                        <input type="hidden" id="countryOfTravel" value="<%= CountryOfTravel %>" />                      
                    </p>
                </li>               
                <li class="VehicleModel <% = (HasModel ? "Selected" : "") %>">
                    <p>
                        <label>1. VEHICLE</label>
                        <select name='ctrlSearchBox_dropVehicleType' id='ctrlSearchBox_dropVehicleType' class='SearchBox_FullListBox'><option value=''>Which vehicle would you like?</option></select>
                        <input type="hidden" id="vehicleType" value="rv" />
                        <input type="hidden" id="vehicleCode" value="" />
                    </p>
                </li>
                <li class="Pax">
                    <p>
                        <label>2. TRAVELLERS</label>
                    </p>
                    <p>
                    	
                        <select name='ctrlSearchBox_dropAdults' id='ctrlSearchBox_dropAdults' class='SearchBox_AdultsListBox required'>
                            <option selected='selected' value='1'>Adults</option>
                            <option value='1'>1 Adult</option>
                            <option value='2'>2 Adults</option>
                            <option value='3'>3 Adults</option>
                            <option value='4'>4 Adults</option>                            
                        </select>
                        <select name='ctrlSearchBox_dropChildren' id='ctrlSearchBox_dropChildren' class='SearchBox_AdultsListBox'>
                            <option selected='selected' value='0'>Children</option>
                            <option value='1'>1 Child</option>
                            <option value='2'>2 Children</option>
                            <option value='3'>3 Children</option>
                            <option value='4'>4 Children</option>                           
                        </select>
                        <input type="hidden" id="maxTravellers" value="6" />
                    </p>
                </li>
                <li class="Locations">
                    <div class="left">
                        <label>3. Pick up</label>
                        <p>
                            <select name="ctrlSearchBox_dropPickUpLocation" id="ctrlSearchBox_dropPickUpLocation" class="LocationDD required">
                                <option value="">Where would you like to pick it up?</option>
                            </select>
                        </p>                
                        <p>
                        	<span style="width:215px; float:left;">
                            	<span class="plil">ON WHAT DATE?</span> <input  type="text" onfocus="this.select();" class="SearchBox_CalTextBox dateField" id="ctrlSearchBox_calPickUp_DateText" value="dd/mm/yyyy" name="ctrlSearchBox$calPickUp$DateText" />
                            </span>
                            <span style="width:215px; float:right;">
                                <span class="plil">AT WHAT TIME?</span> <select class="SearchBox_TimeListBox" id="ctrlSearchBox_dropPickUpTime" name="ctrlSearchBox_dropPickUpTime">
                                    <option value="08:00">08:00am</option>
                                    <option value="08:30">08:30am</option>
                                    <option value="09:00">09:00am</option>
                                    <option value="09:30">09:30am</option>
                                    <option value="10:00" selected="selected">10:00am</option>
                                    <option value="10:30">10:30am</option>
                                    <option value="11:00">11:00am</option>
                                    <option value="11:30">11:30am</option>
                                    <option value="12:00">12:00pm</option>
                                    <option value="12:30">12:30pm</option>
                                    <option value="13:00">13:00pm</option>
                                    <option value="13:30">13:30pm</option>
                                    <option value="14:00">14:00pm</option>
                                    <option value="14:30">14:30pm</option>
                                    <option value="15:00">15:00pm</option>
                                    <option value="15:30">15:30pm</option>
                                    <option value="16:00">16:00pm</option>
                                    <option value="16:30">16:30pm</option>
                                    <option value="17:00">17:00pm</option>
                                </select> 
                            </span>                                          
                        </p>
                    </div>
                    <div class="right">                    
                        <label>4. Drop off</label>
                        <p>
                            <select name="ctrlSearchBox_dropDropOffLocation" id="ctrlSearchBox_dropDropOffLocation" class="LocationDD required">
                                <option value="">Where would you like to drop it off?</option>
                            </select>
                        </p>
                        <p>    
                        	<span style="width:215px; float:left;">
                            	<span class="plil">ON WHAT DATE?</span><input  type="text" onfocus="this.select();" class="SearchBox_CalTextBox dateField" id="ctrlSearchBox_calDropOff_DateText" value="dd/mm/yyyy" name="ctrlSearchBox$calDropOff$DateText" />
                            </span>
                            <span style="width:215px; float:right;">
                            	<span class="plil">AT WHAT TIME?</span> 
                                <select class="SearchBox_TimeListBox" id="ctrlSearchBox_dropDropOffTime" name="ctrlSearchBox_dropDropOffTime">
                                    <option value="08:00">08:00am</option>
                                    <option value="08:30">08:30am</option>
                                    <option value="09:00" >09:00am</option>
                                    <option value="09:30">09:30am</option>
                                    <option value="10:00">10:00am</option>
                                    <option value="10:30">10:30am</option>
                                    <option value="11:00">11:00am</option>
                                    <option value="11:30">11:30am</option>
                                    <option value="12:00">12:00pm</option>
                                    <option value="12:30">12:30pm</option>
                                    <option value="13:00">13:00pm</option>
                                    <option value="13:30">13:30pm</option>
                                    <option value="14:00">14:00pm</option>
                                    <option value="14:30">14:30pm</option>
                                    <option value="15:00" selected="selected">15:00pm</option>
                                    <option value="15:30">15:30pm</option>
                                    <option value="16:00">16:00pm</option>
                                    <option value="16:30">16:30pm</option>
                                    <option value="17:00">17:00pm</option>
                                </select>  
                            </span>                                  
                        </p>
                    </div>
                </li>
                <li>
                    <p id="ctrlSearchBox_tblrCountryOfResidence required">
                        <label class="SearchBoxLabel" id="ctrlSearchBox_lblCountryOfResidence">Driver's License:</label>
                        <select  class="SearchBox_FullListBox required" id="ctrlSearchBox_dropCountryOfResidence" name="ctrlSearchBox$dropCountryOfResidence">
                            <option value="" selected="selected">Where is your driver's licence from?</option>
                            <option value="AU">Australia</option>
                            <option value="CA">Canada</option>
                            <option value="DK">Denmark</option>
                            <option value="FR">France</option>
                            <option value="DE">Germany</option>
                            <option value="NL">Netherlands</option>
                            <option value="NZ">New Zealand</option>
                            <option value="SE">Sweden</option>
                            <option value="CH">Switzerland</option>
                            <option value="UK">United Kingdom</option>
                            <option value="US">United States Of America</option>
                            <option value="" disabled="disabled">--------------------</option>
                            <option value="AF">Afghanistan</option>
                            <option value="AL">Albania</option>
                            <option value="DZ">Algeria</option>
                            <option value="AS">American Samoa</option>
                            <option value="AD">Andora</option>
                            <option value="AO">Angola</option>
                            <option value="AI">Anguilla</option>
                            <option value="AQ">Antarctica</option>
                            <option value="AG">Antigua & Barbados</option>
                            <option value="AR">Argentina</option>
                            <option value="AM">Armenia</option>
                            <option value="AW">Aruba</option>
                            <option value="AT">Austria</option>
                            <option value="AZ">Azerbaijan</option>
                            <option value="BS">Bahamas</option>
                            <option value="BH">Bahrain</option>
                            <option value="BD">Bangladesh</option>
                            <option value="BB">Barbados</option>
                            <option value="BY">Belarus</option>
                            <option value="BE">Belgium</option>
                            <option value="BZ">Belize</option>
                            <option value="BJ">Benin</option>
                            <option value="BM">Bermuda</option>
                            <option value="BO">Bolivia</option>
                            <option value="BA">Bosnia and Herzegovina</option>
                            <option value="BW">Botswana</option>
                            <option value="BV">Bouvet Island</option>
                            <option value="BR">Brazil</option>
                            <option value="IO">British Indian Ocean Territory</option>
                            <option value="BN">Brunei</option>
                            <option value="BG">Bulgaria</option>
                            <option value="BF">Burkina Faso</option>
                            <option value="BI">Burundi</option>
                            <option value="BT">Butan</option>
                            <option value="KH">Cambodia</option>
                            <option value="CM">Cameroon</option>
                            <option value="CV">Cape Verde</option>
                            <option value="KY">Cayman Islands</option>
                            <option value="CF">Cental African Republic</option>
                            <option value="TD">Chad</option>
                            <option value="CL">Chile</option>
                            <option value="CN">China</option>
                            <option value="CX">Christmas Island</option>
                            <option value="CC">Cocos (Keeling) Island</option>
                            <option value="CO">Colombia</option>
                            <option value="KM">Comoros</option>
                            <option value="CG">Congo</option>
                            <option value="CK">Cook Island</option>
                            <option value="CR">Costa Rica</option>
                            <option value="CI">Cote D'ivoire</option>
                            <option value="HR">Croatia</option>
                            <option value="CU">Cuba</option>
                            <option value="CY">Cyprus</option>
                            <option value="CZ">Czech Republic</option>
                            <option value="DJ">DjiboutiI</option>
                            <option value="DM">Dominica</option>
                            <option value="DO">Dominican Republic</option>
                            <option value="TP">East Timor</option>
                            <option value="EC">Ecuador</option>
                            <option value="EG">Egypt</option>
                            <option value="SV">El Salvador</option>
                            <option value="GQ">Equatorial Guinea</option>
                            <option value="ER">Eritrea</option>
                            <option value="EE">Estonia</option>
                            <option value="ET">Ethiopia</option>
                            <option value="FK">Falkland Islands</option>
                            <option value="FO">Faroe Islands</option>
                            <option value="FJ">Fiji</option>
                            <option value="FI">Finland</option>
                            <option value="FX">France, Metropolitan</option>
                            <option value="GF">French Guiana</option>
                            <option value="PF">French Polynesia</option>
                            <option value="TF">French Souther Territories</option>
                            <option value="GA">Gabon</option>
                            <option value="GM">Gambia</option>
                            <option value="GE">Georgia</option>
                            <option value="GH">Ghana</option>
                            <option value="GI">Gibraltar</option>
                            <option value="GR">Greece</option>
                            <option value="GL">Greenland</option>
                            <option value="GD">Grenada</option>
                            <option value="GP">Guadeloupe</option>
                            <option value="GU">Guam</option>
                            <option value="GT">Guatemala</option>
                            <option value="GN">Guinea</option>
                            <option value="GW">Guinea-Bissau</option>
                            <option value="GY">Guyana</option>
                            <option value="HT">Haiti</option>
                            <option value="HM">Heard and McDonald Islands</option>
                            <option value="HN">Honduras</option>
                            <option value="HK">Hong Kong</option>
                            <option value="HU">Hungary</option>
                            <option value="IS">Iceland</option>
                            <option value="IN">India</option>
                            <option value="ID">Indonesia</option>
                            <option value="IR">Iran</option>
                            <option value="IQ">Iraq</option>
                            <option value="IE">Ireland</option>
                            <option value="IL">Israel</option>
                            <option value="IT">Italy</option>
                            <option value="JM">Jamaica</option>
                            <option value="JP">Japan</option>
                            <option value="JO">Jordan</option>
                            <option value="KZ">Kazakhstan</option>
                            <option value="KE">Kenya</option>
                            <option value="KI">Kiribati</option>
                            <option value="KW">Kuwait</option>
                            <option value="KG">Kyrgyzstan</option>
                            <option value="LA">Lao Peoples Democratic Republic</option>
                            <option value="LV">Latvia</option>
                            <option value="LB">Lebanon</option>
                            <option value="LS">Lesotho</option>
                            <option value="LR">Liberia</option>
                            <option value="LY">Libya</option>
                            <option value="LI">Liechtenstein</option>
                            <option value="LT">Lithuania</option>
                            <option value="LU">Luxembourg</option>
                            <option value="MO">Macau</option>
                            <option value="MK">Macedonia</option>
                            <option value="MG">Madagascar</option>
                            <option value="MW">Malawi</option>
                            <option value="MY">Malaysia</option>
                            <option value="MV">Maldives</option>
                            <option value="ML">Mali</option>
                            <option value="MT">Malta</option>
                            <option value="MH">Marshall Islands</option>
                            <option value="MQ">Martinique</option>
                            <option value="MR">Mauritania</option>
                            <option value="MU">Mauritius</option>
                            <option value="YT">Mayotte</option>
                            <option value="MX">Mexico</option>
                            <option value="FM">Micronesia, Federated States of</option>
                            <option value="MD">Moldova</option>
                            <option value="MC">Monaco</option>
                            <option value="MN">Mongolia</option>
                            <option value="MS">Montserrat</option>
                            <option value="MA">Morocco</option>
                            <option value="MZ">Mozambique</option>
                            <option value="MM">Myanamar</option>
                            <option value="NA">Namibia</option>
                            <option value="NR">Nauru</option>
                            <option value="NP">Nepal</option>
                            <option value="AN">Netherlands Antilles</option>
                            <option value="NC">New Caledonia</option>
                            <option value="NI">Nicaragua</option>
                            <option value="NE">Niger</option>
                            <option value="NG">Nigeria</option>
                            <option value="NU">Niue</option>
                            <option value="NF">Norfolk Island</option>
                            <option value="KP">North Korea</option>
                            <option value="MP">Northern Mariana Islands</option>
                            <option value="NO">Norway</option>
                            <option value="OM">Oman</option>
                            <option value="PK">Pakistan</option>
                            <option value="PW">Palau</option>
                            <option value="PA">Panama</option>
                            <option value="PG">Papua New Guinea</option>
                            <option value="PY">Paraguay</option>
                            <option value="PE">Peru</option>
                            <option value="PH">Philippines</option>
                            <option value="PN">Pitcairn Islands</option>
                            <option value="PL">Poland</option>
                            <option value="PT">Portugal</option>
                            <option value="PR">Puerto Rico</option>
                            <option value="QA">Qatar</option>
                            <option value="RE">Reunion</option>
                            <option value="RO">Romania</option>
                            <option value="RU">Russian Federation</option>
                            <option value="RW">Rwanda</option>
                            <option value="SH">Saint Helena</option>
                            <option value="KN">Saint Kitts and Nevis</option>
                            <option value="LC">Saint Lucia</option>
                            <option value="PM">Saint Pierre and Miquelon</option>
                            <option value="VC">Saint Vincent and The Grenadines</option>
                            <option value="WS">Samoa</option>
                            <option value="SM">San Marino</option>
                            <option value="ST">Sao Tome and Principe</option>
                            <option value="SA">Saudi Arabia</option>
                            <option value="SN">Senegal</option>
                            <option value="SC">Seychelles</option>
                            <option value="SG">Singapore</option>
                            <option value="SK">Slovakia</option>
                            <option value="SI">Slovenia</option>
                            <option value="SB">Solomon Islands</option>
                            <option value="SO">Somalia</option>
                            <option value="ZA">South Africa</option>
                            <option value="GS">South Georgia</option>
                            <option value="KR">South Korea</option>
                            <option value="ES">Spain</option>
                            <option value="SL">Sri Lanka</option>
                            <option value="SD">Sudan</option>
                            <option value="SR">Suriname</option>
                            <option value="SJ">Svalbardand Jan Mayen Islands</option>
                            <option value="SY">Syria</option>
                            <option value="TI">Tahiti</option>
                            <option value="TW">Taiwan</option>
                            <option value="TJ">Tajikistan</option>
                            <option value="TZ">Tanzania</option>
                            <option value="TH">Thailand</option>
                            <option value="TG">Togo</option>
                            <option value="TK">Tokelau</option>
                            <option value="TO">Tonga</option>
                            <option value="TT">Trinidad and Tobago</option>
                            <option value="TN">Tunisia</option>
                            <option value="TR">Turkey</option>
                            <option value="TM">Turkmenistan</option>
                            <option value="TC">Turks and Caicos Islands</option>
                            <option value="TV">Tuvalu</option>
                            <option value="UG">Uganda</option>
                            <option value="UA">Ukraine</option>
                            <option value="AE">United Arab Emirates</option>
                            <option value="UY">Uruguay</option>
                            <option value="UM">US Minor Outlying Islands</option>
                            <option value="UZ">Uzbekistan</option>
                            <option value="VU">Vanuatu</option>
                            <option value="VA">Vatican City</option>
                            <option value="VE">Venezuala</option>
                            <option value="VN">Vietnam</option>
                            <option value="VG">Virgin Islands (GB)</option>
                            <option value="VI">Virgin Islands (US)</option>
                            <option value="WF">Wallis and Futuna Islands</option>
                            <option value="EH">Western Sahara</option>
                            <option value="YE">Yemen</option>
                            <option value="YU">Yugoslavia</option>
                            <option value="ZR">Zaire</option>
                            <option value="ZM">Zambia</option>
                            <option value="ZW">Zimbabwe</option>
                        </select>
                    </p>
                </li>               
                <li>
                    <p>
                        <a onclick="javascript:submitWidget();" id="searchButton">Get a Quote</a>
                    </p>
                </li>
            </ul>            
        </div>
    </div>
</div>
    </form>
</body>
</html>