﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;
using System.Collections.Specialized;

public partial class webservices_SetAgentToken : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        NameValueCollection tokenCache;
        string agentId = Request.Params["agentId"];
        if (!string.IsNullOrEmpty(agentId))
        {
            if (Cache["agentTokens"] == null ) 
                tokenCache = new NameValueCollection();
            else tokenCache = (NameValueCollection)(Cache["agentTokens"]);        
            string token = string.Empty;
            if (!tokenCache.AllKeys.Contains(agentId))
                tokenCache.Set(agentId, Guid.NewGuid().ToString());
            else
                token = tokenCache.Get(agentId);
            Cache["agentTokens"] = tokenCache;
            token =  tokenCache.Get(agentId);

            string JSONResponse = @"{""token"":""" + token + @"""}";
            Response.ContentType = "application/json";
            Response.Write(JSONResponse);
            Response.End();
        }
    }
}