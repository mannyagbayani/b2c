﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using THL.Booking;
using System.Text;

public partial class webservices_Availability : System.Web.UI.Page
{

    public string JSONData { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!string.IsNullOrEmpty(Request.Params["service"]) && Request.Params["service"].ToLower().Equals("json"))
        //{


        string JSONResponse = string.Empty;
        //Check if there is an async request for this request signiture.
        //if yes and completed: return it, flush it.
        //if yes and pending: return its status and elapsed time
        // if no: fork it and return status            
        //for now, phase one:
        AvailabilityRequest request = GetAvailabilityRequestForAsyncParams(Request.Params);

        if (request.CountryOfResidence == null)
            request.CountryOfResidence = string.Empty;

        SessionManager.GetSessionData().AvailabilityRequestData = request;

        //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
        if (SessionManager.GetSessionData().AvailabilityResponseData == null)
        {
            SessionManager.GetSessionData().AvailabilityResponseData = new AvailabilityResponse(string.Empty);
            SessionManager.GetSessionData().AvailabilityResponseData.SetAvailabilityItems(new AvailabilityItem[0]);
        }

        AvailabilityResponse response = new DataAccess().GetAvailability(request);

        //only if branded should do this
        THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);

        if (!appDomain.IsAggregator)
        {
            SessionManager.GetSessionData().AvailabilityResponseData = response;
        }
        else
        {
            //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
            SessionManager.GetSessionData().AvailabilityResponseData.AppendAvailabilityItems(response.GetAvailabilityItems(string.Empty));
        }

        THLDebug.LogError(ErrorTypes.Application, 
            "Configure.Page_Load.SessionManager.GetSessionData().AvailabilityResponseData.GetAvailabilityItems().Length",
            SessionManager.GetSessionData().AvailabilityResponseData.GetAvailabilityItems(string.Empty).Length.ToString(), 
            "{}");

        AvailabilityItem[] responseItems = response.GetAvailabilityItems(string.Empty);
        StringBuilder sb = new StringBuilder();

        string trackingResponse = AvailabilityHelper.GetTrackingImgForAvailResponse(response, request).Replace("\"", "\\\"");

        foreach (AvailabilityItem item in responseItems)
        {
            sb.Append(GetAvailabilityItemJSON(item) + ",");
        }
        JSONResponse = @"{""items"":[" + sb.ToString().TrimEnd(',') + @"],""debug"":""" + Request.Params["extraParams"] +/*request.ToJSON() + */ @""",""bi"":""" + trackingResponse + @"""}";
        Response.ContentType = "application/json";
        Response.Write(JSONResponse);
        Response.End();
        //}


    }

    /// <summary>
    /// Generate and Availability Requert object of the async script request, refactor into AvailbilityHelper
    /// </summary>
    /// <param name="requestParams"></param>
    /// <returns></returns>
    public static AvailabilityRequest GetAvailabilityRequestForAsyncParams(NameValueCollection queryParams)
    {
        AvailabilityRequest request = new AvailabilityRequest();
        int na = 1; int nc = 0;//default

        if (!string.IsNullOrEmpty(queryParams["extraParams"]))
        {
            string[] extraParams = queryParams["extraParams"].Split('-');
            for (int i = 0; i < extraParams.Length; i = i + 2)
            {
                string paramValue = extraParams[i + 1];
                switch (extraParams[i].ToLower())
                {
                    case "na":
                        int.TryParse(paramValue, out na);
                        request.NumberOfAdults = na;
                        break;
                    case "nc":
                        int.TryParse(paramValue, out nc);
                        request.NumberOfChildren = nc;
                        break;
                    case "cr":
                        request.CountryOfResidence = paramValue.ToUpper();
                        break;
                    case "pc":
                        request.PackageCode = paramValue.ToUpper();
                        break;
                    case "ac":
                        SessionManager.AgentCode = paramValue;
                        break;
                    case "isgross":
                        request.IsGross = paramValue.ToLower().Equals("true");
                        break;
                    
                       
                }
            }
        }

        //http://local.britz.com.au/selection.aspx?company=thl&pickUpLoc=bne&pickUpDate=1030-01-03-2013&dropOffLoc=syd&dropOffDate=1500-15-03-2013&extraParams=dsfsdf

        string pickUpTimeStr = string.Empty, dropOfTimeStr = string.Empty;
        foreach (string key in queryParams.AllKeys)
        {
            if (key == null || key == string.Empty)
                continue;
            string normalizedKey = (key != null ? key.ToLower() : string.Empty);
            string normalizedValue = queryParams[key].ToLower();
            string value = queryParams[key];
            switch (key)
            {
                case "company":
                    request.Brand = GetBrandCharForURLName(normalizedValue);
                    break;
                case "country"://country code                        
                    switch (normalizedValue)
                    {
                        case "nz":
                            request.CountryCode = CountryCode.NZ;
                            break;
                        case "au":
                            request.CountryCode = CountryCode.AU;
                            break;
                        case "us":
                            request.CountryCode = CountryCode.US;
                            break;
                        default:
                            request.CountryCode = CountryCode.NONE;
                            break;
                    }
                    break;
                case "ac"://agent code
                    request.AgentCode = normalizedValue;
                    break;
                case "IsVan":
                    //bool isVan = true;//mine from url
                    request.VehicleType = VehicleType.Campervan;
                    break;
                case "adults":
                    int numberOfAdults = 0;
                    int.TryParse(normalizedValue, out numberOfAdults);
                    request.NumberOfAdults = numberOfAdults;
                    break;
                case "NoOfChildren":
                    int numberOfChildren = 0;
                    int.TryParse(normalizedValue, out numberOfChildren);
                    request.NumberOfChildren = numberOfChildren;
                    break;
                case "CountryOfResidence"://Country of Residence
                    request.CountryOfResidence = normalizedValue;
                    break;
                case "pickUpLoc"://PickUp Branch
                    request.PickUpBranch = normalizedValue;
                    break;
                case "dropOffLoc":
                    request.DropOffBranch = normalizedValue;
                    break;
                case "pickUpDate":
                    pickUpTimeStr = normalizedValue;
                    break;
                case "dropOffDate":
                    dropOfTimeStr = normalizedValue;
                    break;
                case "promoCode":
                    request.PromoCode = normalizedValue;
                    break;
                    
                /*
                case "VehicleCode":
                    vehicleModel = value;
                    break;                
                case "CompanyCode":
                    companyCode = value;
                    break;                
                case "ChannelCode":
                    channelCode = value;
                    break;   
                
                case "PackageCode":
                    packageCode = value;
                    break;  
                 */
            }
        }
        System.Globalization.CultureInfo provider = System.Globalization.CultureInfo.InvariantCulture;
        string format = "Hmm-dd-MM-yyyy";
        request.PickUpDate = DateTime.ParseExact(pickUpTimeStr, format, provider);
        request.DropOffDate = DateTime.ParseExact(dropOfTimeStr, format, provider);
        /*
        if (channelCode != null && channelCode != string.Empty)
            _affiliate = ApplicationManager.GetAffeliateForCode(channelCode);
        */

        request.VehicleType = VehicleType.Campervan;//TODO: get from url i.e. /britz/campervans/
        return request;
    }

    public static string GetBrandCharForURLName(string brandName)
    {
        string returnChar = string.Empty;
        switch (brandName.ToLower())
        {
            case "britz":
                returnChar = "b";
                break;
            case "maui":
                returnChar = "m";
                break;
            case "mighty":
                returnChar = "y";
                break;
            case "kea":
                returnChar = "q";
                break;
            case "united":
                returnChar = "u";
                break;
            case "alpha":
                returnChar = "a";
                break;
            case "econo":
                returnChar = "e";
                break;
        }
        return returnChar;
    }

    /// <summary>
    /// Render JSON for avaiability item, used by the script service for asyn call,TODO: refactor into Availability Response 
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public static string GetAvailabilityItemJSON(AvailabilityItem item)
    {

        //AvailabilityResponseDisplayer ard = new AvailabilityResponseDisplayer();
        string rateTable = (item.AvailabilityType == AvailabilityItemType.Available ? RenderPriceDetails(item).Replace("\"", "\\\"").Replace("'", "\\\"").Replace("\n", "") : string.Empty);
        StringBuilder sb = new StringBuilder();
        sb.Append(@"{""price"":" + item.EstimatedTotal.ToString("F") + @",");
        sb.Append(@"""agent"":" + item.PayableToAgent.ToString("F") + @",");//B2B
        sb.Append(@"""pickup"":" + item.PayableAtPickUp.ToString("F") + @",");//B2B
        sb.Append(@"""code"":""" + item.VehicleCode + @""",");
        sb.Append(@"""avp"":""" + item.AVPID + @""",");
        sb.Append(@"""pkg"":""" + item.PackageCode + @""",");
        sb.Append(@"""promo"":""" + (!string.IsNullOrEmpty(item.PromoText) ? item.PromoText : "n/a") + @""",");
        sb.Append(@"""bands"":""" + rateTable + @""",");
        //rev:mia june 9 2014 - B2BMAC
        sb.Append(@"""cangetgross"":""" + item.CanGetGross + @""",");

        string err = "";// ((item.ErrorMessage != null && !item.ErrorMessage.Contains("LIMITED AVAILABILITY"))) ? item.ErrorMessage : "LIMITED AVAILABILITY";
        //rev:mia june 3 2015 - B2BMAC
        if (item.ErrorMessage != null)
        {
            if (item.ErrorMessage.Contains("LIMITED AVAILABILITY"))
                err = "LIMITED AVAILABILITY";
            else
                err = item.ErrorMessage;
        }

        sb.Append(@"""err"":""" + err + @""",""brmsg"":""" + item.BlockingRuleMessage + @"""");
        //More properties here..
        sb.Append("}");
        return sb.ToString();
    }


    /// <summary>
    /// Render Price Details Table for a given Availability Row Object, TODO: refactor into MVC displayer once 4.5
    /// </summary>
    /// <param name="availabilityRow"></param>
    /// <returns></returns>
    public static string RenderPriceDetails(AvailabilityItem availabilityRow)
    {
        int colSpans = 3;

        StringBuilder sb = new StringBuilder();
        bool hasFreeDays = false;
        AvailabilityItemChargeRateBand[] availabilityRateBands = availabilityRow.GetRateBands();
        AvailabilityItemChargeDiscount[] aidc = availabilityRow.GetDiscounts();
        bool hasDiscount = false, hasPercentageDiscount = false;

        foreach (AvailabilityItemChargeDiscount discount in aidc)
        {
            //if (discount.IsPercentage) hasDiscount = true;// 04/08/09 only consider percentage discount ,TODO: dive into all ratebands           
            if (discount.IsPercentage) hasPercentageDiscount = true;
        }

        hasDiscount = AvailabilityHelper.HasDiscountedDays(availabilityRateBands);


        colSpans = (hasDiscount || hasFreeDays ? colSpans : --colSpans);

        if (availabilityRateBands != null && availabilityRateBands.Length > 0)
            foreach (AvailabilityItemChargeRateBand chargeRateRow in availabilityRateBands) //order?
                if (chargeRateRow.IncludesFreeDay > 0)
                    hasFreeDays = true;
        sb.Append("<div class=\"PriceDetailsList " + (hasFreeDays ? string.Empty : "NoFreeDays") + (hasDiscount ? string.Empty : " NoDiscount") + "\">");

        //vehicle content from central repository, future implementation could leverage this for branded blurb
        //VehicleContent vehicleContent = null ;// ApplicationManager.GetApplicationData().GetContentForVehicle(availabilityRow.VehicleCode);
        //if (vehicleContent != null && !string.IsNullOrEmpty(vehicleContent.Description))
        //    sb.Append("<div class='vinfo' style='font-size: 0.7em;margin: 0 0 5px 20px;display:none;'>\"" + ApplicationManager.GetApplicationData().GetContentForVehicle(availabilityRow.VehicleCode).Description + "\"</div>");

        sb.Append("<table class=\"chargesTable\">");
        sb.Append("<thead>");
        sb.Append("<tr>");
        sb.Append("<th class='rate'>Product Description</th>");

        if (hasFreeDays)
            sb.Append("<th class='FreeTH'><span>Free Days</span><span rel='freeDays' class='popup' style='display:none;'>[?]</span></th>");
        sb.Append("<th class='days'>Hire Days</th>");
        sb.Append("<th class='per'>Per Day Price</th>");
        sb.Append(!hasDiscount ? "" : "<th class='disc' " + (hasDiscount ? "" : "style='display:none;'") + "><span>Discounted Day Price</span>" + (hasPercentageDiscount ? "<span rel='DiscountedDaysPopUp' class='popup' onclick='showDiscTable(this)'>[?]</span>" : string.Empty) + "</th>");
        sb.Append("<th class='total'>Total</th>");
        sb.Append("</tr>");
        sb.Append("</thead>");
        sb.Append("<tbody>");

        bool displayTotal = false;
        decimal longHireDiscount = availabilityRow.GetLongHireDiscount();
        decimal totalCharge = availabilityRow.GetTotalPrice();

        if (availabilityRateBands != null)//has several rate bands
        {
            if (availabilityRateBands.Length > 1)
                displayTotal = true;
            foreach (AvailabilityItemChargeRateBand chargeRateRow in availabilityRateBands)
            {
                string freeDaysStr = string.Empty;
                if (chargeRateRow.IncludesFreeDay > 0)
                {
                    freeDaysStr = chargeRateRow.IncludesFreeDay.ToString();
                    hasFreeDays = true;
                }
                string fromDateStr = chargeRateRow.FromDate.ToString("dd-MMM-yyyy");
                string toDateStr = chargeRateRow.ToDate.ToString("dd-MMM-yyyy");
                sb.Append("<tr>");
                sb.Append("<td class='rt'>" + fromDateStr + " to " + toDateStr + "</td>");
                if (hasFreeDays)
                    sb.Append("<td class='fd'>" + freeDaysStr + "</td>");
                sb.Append("<td class='hp'>" + chargeRateRow.HirePeriod + " day(s)</td>");
                sb.Append("<td class='pdp'>$" + chargeRateRow.OriginalRate.ToString("F") + "</td>");
                sb.Append(!hasDiscount ? "" : "<td class='dpd' " + (hasDiscount ? "" : "style='display:none;'") + ">$" + chargeRateRow.DiscountedRate.ToString("F") + "</td>");
                sb.Append("<td>$" + chargeRateRow.GrossAmount.ToString("F") + "</td>");
                sb.Append("</tr>");
            }
        }
        else //single rate band, TODO: revisit formating
        {
            AvailabilityItemChargeRow vehicleRow = availabilityRow.GetVehicleChargeRow();
            sb.Append("<tr>");
            sb.Append("<td class='rt'>" + vehicleRow.ProductName + "</td>");
            sb.Append("<td class='fd'>" + /* free days? */  "</td>");
            sb.Append("<td class='hp'>" + vehicleRow.HirePeriod + " day(s)</td>");
            sb.Append("<td class='pdp'>" + /*chargeRateRow.OriginalRate.ToString("F") +*/ "</td>");
            sb.Append("<td class='dpd' " /*+ (hasDiscount ? "" : "style='display:none;'") + ">$" + chargeRateRow.DiscountedRate.ToString("F")*/ + "</td>");
            sb.Append("<td>$" + vehicleRow.ProductPrice + "</td>");
            sb.Append("</tr>");
        }

        AvailabilityItemChargeRow[] aCharges = availabilityRow.GetNonVehicleCharges();
        if (aCharges != null && aCharges.Length > 0)
        {
            displayTotal = true;
            foreach (AvailabilityItemChargeRow charge in aCharges)
            {
                if (charge.IsBonusPack || charge.AdminFee > 0) continue;

                if (charge.HirePeriod > 1)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + charge.ProductName + "</td>");
                    sb.Append("<td class='hp'>" + charge.HirePeriod + " day(s)</td>");
                    sb.Append("<td class='pdp'>$" + charge.AverageDailyRate.ToString("F") + "</td>");
                    if (hasDiscount) sb.Append("<td class='pdp'>" + "&nbsp;" + "</td>");
                    if (hasFreeDays) sb.Append("<td class='dpd'>" + "&nbsp;" + "</td>");
                    sb.Append("<td>" + (charge.ProductPrice > 0 ? "$" + charge.ProductPrice : "included") + "</td>");
                    sb.Append("</tr>");
                }
                else
                {
                    sb.Append("<tr rel='" + charge.ProductCode + "'>");
                    sb.Append("<td colspan='" + (colSpans + (hasFreeDays ? 1 : 0)) + "'>" + charge.ProductName + "</td>");
                    sb.Append("<td>" + (charge.ProductPrice == 0m ? "included" : "$" + charge.ProductPrice.ToString("F")) + "</td>");
                    sb.Append("</tr>");
                }
            }

        }


        decimal adminFeeFactor = 1.0m;
        if (availabilityRow.AdminFee > 0 && availabilityRow.AdminFee > 0)
        {
            adminFeeFactor = 1.0m + (availabilityRow.AdminFee / 100);
            sb.Append("<tr>");
            sb.Append("<td colspan='" + (colSpans + (hasFreeDays ? 1 : 0)) + "'>" + (availabilityRow.AdminFee).ToString("F") + "% Administration fee</td>");
            sb.Append("<td>$" + (availabilityRow.EstimatedTotal * (availabilityRow.AdminFee / 100)).ToString("F") + "</td>");
            sb.Append("</tr>");
        }

        if (displayTotal)
        {
            colSpans = 3 + (hasDiscount ? 1 : 0) + (hasFreeDays ? 1 : 0);
            sb.Append("<tr class='total'>");
            sb.Append("<td class='TotalTD' colspan='" + colSpans + "'>Total</td>");
            sb.Append("<td>$" + (availabilityRow.EstimatedTotal * adminFeeFactor).ToString("N") + "</td>");
            sb.Append("</tr>");
        }

        sb.Append("</tbody>");
        sb.Append("</table>");
        sb.Append(getDiscountPopUp(availabilityRow));
        sb.Append("</div>");
        string result = sb.ToString();
        return result;
    }

    /// <summary>
    /// Discount PopUps 
    /// </summary>
    /// <param name="availabilityRow"></param>
    /// <returns></returns>
    private static string getDiscountPopUp(AvailabilityItem availabilityRow)
    {
        AvailabilityItemChargeDiscount[] aidc = availabilityRow.GetDiscounts();
        if (aidc == null || aidc.Length == 0)
            return null;
        else
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div>");
            sb.Append(@"<table class=""discountTable hide"">");
            sb.Append(@"<thead><th class=""desc"">Your discounted Day Price Includes</th><th class=""total"">Total</th></thead>");
            sb.Append("<tbody>");
            decimal totalDis = 0m;
            string discountUnitDisplay = string.Empty;
            foreach (AvailabilityItemChargeDiscount aid in aidc)
            {
                if (aid.IsPercentage)
                {
                    totalDis += aid.DisPercentage;//TODO: Condider summing discount totals on non percentage as well
                    discountUnitDisplay = aid.DisPercentage + "%";
                }
                else
                {
                    discountUnitDisplay = "$" + aid.DisPercentage;
                    continue;
                }
                sb.Append("<tr>");
                sb.Append("<td>" + aid.CodDesc + "</td>");
                sb.Append("<td>" + discountUnitDisplay + "</td>");
                sb.Append("</tr>");
            }
            sb.Append("<tr class='total' rel='" + totalDis + "'>");
            sb.Append("<td>Total Discount</td>");
            sb.Append("<td>" + totalDis + "%</td>");
            sb.Append("</tr>");
            sb.Append("</tbody>");
            sb.Append("</table>");
            //sb.Append("<span class='note'>(off standard per day rates)</span>");
            sb.Append("</div>");
            return sb.ToString();
        }
    }




}