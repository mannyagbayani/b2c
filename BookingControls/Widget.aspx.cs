﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class THLWidget : System.Web.UI.Page
{
    public string JSONParams;

    public bool HasModel = false;

    AvailabilityRequest availabilityRequest;

    public int numMonths;

    public string CountryOfTravel;

    public string ContentBase;

    public string AnalyticsCode;

    protected void Page_Load(object sender, EventArgs e)
    {
        availabilityRequest = AvailabilityHelper.GetAvailabilityRequestForWidgetParams(Request.Params);
        THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
        ApplicationData appData = ApplicationManager.GetApplicationData();
        Branch[] branches = appData.GetWidgetBranches();
        Vehicle[] vehicles = appData.Vehicles;
        
        numMonths = 2;
        int.TryParse(Request.Params["months"], out numMonths);

        if(!string.IsNullOrEmpty(Request.Params["vc"]))
        {
            HasModel = true;
        }

        CountryOfTravel = appDomain.Country.ToString();

        ContentBase = !string.IsNullOrEmpty(Request.Params["cssb"]) ? Request.Params["cssb"] : appDomain.ParentSite + "/SiteCollectionDocuments/BookingControls";

        JSONParams = AvailabilityHelper.GetWidgetJSONForRequest(availabilityRequest, branches, vehicles);
        
        //Handle External Requests
        if(!string.IsNullOrEmpty(Request.Params["service"]) && Request.Params["service"].ToLower().Equals("json"))
        {
            Response.ContentType = "application/json";
            Response.Write(JSONParams);
            Response.End();
        }
        else if (!string.IsNullOrEmpty(Request.Params["service"]) && Request.Params["service"].ToLower().Equals("xml"))
        {
            Response.ContentType = "text/xml";
            Response.Write(AvailabilityHelper.GetWidgetXMLForRequest(availabilityRequest, branches, vehicles));
            Response.End();
        }


        //Render Analytics Segment
        AnalyticsEventTrackingItem[] availTrackingItems = new AnalyticsEventTrackingItem[0] { }; 
        TrackingDisplayer trd = new TrackingDisplayer(appDomain, true/*secureMode*/);
        if (availTrackingItems != null && availTrackingItems.Length > 0 && appDomain.Brand != THLBrands.AIRNZ)
            AnalyticsCode = trd.RenderAnalsCode(availTrackingItems);
        else
            AnalyticsCode = trd.RenderAnalsCode();


    }
}