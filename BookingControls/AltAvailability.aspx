﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AltAvailability.aspx.cs"
    Inherits="AltAvailability" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Alternative Availability</title>
    <link href="<%= AssetsURL %>/css/Base.css?v=1.8" type="text/css" rel="stylesheet" />
    <link href="<%= AssetsURL %>/css/<%= BrandChar %>/selection.css?v=1.8" type="text/css" rel="stylesheet" />   
    <script src="js/jquery-1.3.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.dimensions.min.js" type="text/javascript"></script>
    <script src="js/jquery.tooltip.min.js" type="text/javascript"></script>
    <script src="js/date.js" type="text/javascript"></script>
    <script src="js/hCtrl.js?v=1.8" type="text/javascript"></script>
    <script src="js/selection.js?v=1.8" type="text/javascript"></script>
    <script src="js/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <link href="<%= AssetsURL %>/css/<%= BrandChar %>/jcal.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript">
        var $j = jQuery.noConflict();
        jQuery(document).ready(function() {//TODO: refactor int initAltAvail()
            //
            //unavailaible detail layer
            $j(".details").each(function() {
                var $container = $j(this);
                var $html = $j('<div class="DiscountedDaysPopUp bubbleBody"><div class="SmallContainer"><span class="UnavailableInfo"></span></div></div>');
                var $textSpan = $html.find('.UnavailableInfo');
                $j(this).after($html);
                $j(this).bind('mouseover', function() { initPopUp(this); });
                var template = $j("#UnavailableInfoTemplate").html();
                var divInfo = $container.parents(".OptionRow").find('.AltType span').text().toLowerCase();
                $textSpan.html(template.replace("{ROW}", divInfo));
            });

            //TODO: this json poterntialy be generated from backend
            var unAvailMsg = { 'MoveForward': 'moved your pick up day up to 10 days later', 'MoveBackwards': 'moved your pick up day up to 10 days earlier', 'SwitchLocation': 'reversed your travel route', 'PickUpBased': 'shortened your stay by up to 10 days, keeping your pick up date the same', 'DropOffBased': 'shortened your stay by up to 10 days, keeping your drop off date the same', 'OneWaySwitchLocation': 'returned  the vehicle to your pick up location' };

            $j(".UnavailableInfo").each(function() {
                var $div = $j(this);
                var template = $j("#UnavailableInfoTemplate").html();
                var divInfo = eval("unAvailMsg." + $div.parents(".OptionRow").find('.AltType').attr('rel'));
                $div.html(template.replace("{ROW}", divInfo));
            });
            $j(".ShowPriceList").each(function() {
                //var $html = $j('.bubbleBody');//get html from relevant row
                //$j(this).after($html);
                $j(this).bind('mouseover', function() { initPopUp(this); });
            });

            $j("span.hireDates").each(function() {
                $j(this).bind('mouseover', function() { initPopUpV2(this, "popUpTxt"); });
            });

            $j("#currencySelector").change(function() {
                var elm = $j(this);
                code = elm.val();
                factor = elm.attr('rel');
                $j("div.AltPrice ul.Prices li.Fp").addClass('Hide'); //reset
                $j("div.AltPrice ul.Prices li." + code).removeClass('Hide');
            });

            $j("a.call").click(function() {
                if (document.domain.indexOf("airnz") > 0) {//air nz call
                    deleteCookie('history', '', '');
                    document.location = $j('#step-1 a').attr('href');
                }
                else
                    NewWindow($j(this).attr('href'), 'vCode', '820', '500', 'yes', 'center');
                return false;
            });


            $j(".AltRow a.AltSelect").click(function() {
                var gaEventPrm = $j(this).attr('rel');
                _gaq.push(['_trackEvent', '<%= VType %>', 'AlternativeAvailabilityOption', gaEventPrm]);
            });

            if ($j(".AltTime").length == 1) {
                $j("#alternativeCatalogContainer").addClass("unavailable");
                $j("#footerContainer").addClass("unavailable");
            }

            $j("#selectedVehiclePanel img, #selectedVehiclePanel h3").click(function() {
                //event.stopPropagation();
                NewWindow('http://<%= VehicleContentLink %>', 'vCode', '820', '500', 'yes', 'center');
                return false;
            });
        });
    </script>    
    <style>
        #tooltip
        {
            background: none;
            border: 0px;
            padding: 0px;
            -moz-box-shadow: 5px 5px 5px #666;
            -webkit-box-shadow: 5px 5px 5px #666;
            box-shadow: 5px 5px 5px #666;
        }
        
        #alternate a {
            outline: 0;
        }      
        
        #selectedVehiclePanel img, #selectedVehiclePanel h3   {
            cursor: pointer;
        }
        
    </style>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <%= AnalyticsCode %>
    <%= OpenTag %>
</head>
<body id="alternate">
    <form name="form1" method="post" action="selection.aspx" id="form1">
    <%= DNAIMGTag %>
    <div id="vehicleAAContainer">
        <div id="catalogAAHeader">
            <span class="Image">
                <img src="<%= AssetsURL %>/images/<%= BrandChar %>/logo.png" alt="logo" />
            </span>
            <span class="ContactUs">
                <img src="<%= AssetsURL %>/images/<%= BrandChar %>/tiles/header-24-7-image.jpg" alt="contact us 24 7" />
                <a href="<%= ParentSite %>/callus" class="freephone call" title="free phone">Free phone</a>
            </span>
            </div>
        <div id="b2c-steps">
            <span id="step-1" class="steps">
                <a href="<%= SelectionURL %>">Select</a>
            </span>
        </div>
        <div id="vechileCatalogAAContainer">
            <div class="returntoseachBreadCrumb"><a href="<%= SelectionURL %>" title="return to search">&lt; Return to search results</a></div>
            <h2>Alternative Availability</h2>
            <div id="resultsHeader">
                <span class="CurrencyPanel"><small>Alternative Currency:</small>
                    <%= CurrencySelector %>
                </span>
            </div>          
            <div id="alternativeCatalogContainer">
                <div id="selectedVehiclePanel">
                    <h3><%= VehicleTitle %></h3>                     
                    <img id="selectedVImage" src="<%= VehicleImagePath %>-Clearcut-144.jpg" title="<%= AItem.VehicleName %>" alt="Vehicle Image" /> 
                    <img id="selectedVIncs"  src="<%= InclImagePath %>-InclusionIcons.gif" title="Inclusions" alt="Vehicle Inclusions"/> 
                    <% if (THL.Booking.ApplicationManager.GetDomainForURL(Request.Url.Host).IsAggregator)
                       { %>
                    <img id="selectedVLogo"  src="<%= LogoImagePath %>" title="Logo" alt="Brand Logo"/>
                    <%} %>
                    <span class="reel">
                        <img id="selectedVReel" src="<%= VehicleImagePath %>-OverviewThumbs.png" alt="Vehicle Reel" />       </span>
                </div>
                <!-- Generated Catalog View -->
                <%= OriginalSearchRow %>
                <div id="alternativeMessagePanel">
                    <span>Next best availability for the <%= AItem.VehicleName %></span>                    
                </div>
                <div class="rightHandBanner">
                    <img src="<%= AssetsURL %>/images/<%= BrandChar %>/tiles/rightHand-24-7-reservations-title.gif" alt="contact us 24/7" />
                    <a href="<%= ParentSite %>/callus" class="freephone call" title="free phone">Free phone</a>
                </div>
                <%= CurrentRow %>
                <div id="alternateContactPanel">
                    <span>Can't find what you are looking for?</span>
                </div>
            </div>           
        </div>
        <div id="footerContainer">
            <div id="footerWrapper">
                <span class="resText">contact us 24 7</span>
                <a href="<%= ParentSite %>/callus" class="freephone call" title="free phone">Free phone</a>
                <span class="searchall">searchall</span>
                <a href="<%= SelectionURL %>" class="returntosearch"  title="return to search">Return to search page</a>
                <a href="#" class="otherbrands" title="try THL's other brands">Try THL's other brands</a>
            </div>
        </div>
        <div id="disclaimer" title="disclaimer">
            <span>Please note: Rental prices quoted above are only valid at the time of booking.</span>
            <span>Alternative currency is based on approximate current exchange rates. All payments
                will be processed in <%= defaultCurrency %>.</span>
        </div>
        <div id="footerLinksBottom">
            <ul id="footerLinksLogos">
                <li id="logos">
                    <a id="THLLogo" title="Tourism Holding Ltd" href="http://www.thlonline.com">THL</a>
                    <a id="MauiLogo" title="maui motorhomes &amp; car rental" href="http://www.maui-rentals.com">Maui</a>
                    <a id="BritzLogo" title="Britz" href="http://www.britz.com">Britz</a>                                
                    <a id="mightyLogo" title="Mighty Campers &amp; car hire" href="http://www.mightycampers.com">Mightycampers</a>
                    <a id="roadbearlogo" title="Road Bear RV USA" href="http://www.roadbearrv.com">Road Bear RV USA</a>
                </li>
                <li id="copyright">
                    You are viewing <%= ParentSite.Replace("http://", "") %><br />                   
                    &copy; <span id="yearToDate">2011</span>
                    <script type="text/javascript">
                        var currentTime = new Date();
                        document.getElementById('yearToDate').innerHTML = currentTime.getFullYear();
                    </script>
                    Tourism Holdings Ltd
                    <strong><i>(thl)</i></strong>                  
                </li>
            </ul>
        </div>        
        <div style="display:none;">
            <span id="UnavailableInfoTemplate">We checked to see if the vehicle was available if you {ROW}, but there were still days that were unavailable</span>
        </div>
        <!-- Debug Code Starts -->           
        <div>
            <%= debugSTR %>
        </div>
        <!-- Debug Code Ends -->
    </div>
    </form>
    <%= RemarketingCode %> 
</body>
</html>
