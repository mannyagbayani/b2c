﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Confirmation.aspx.cs" Inherits="Confirmation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Confirmation Page</title>
    <link href="<%= AssetsURL %>/css/Base.css" type="text/css" rel="stylesheet" />
    <link href="<%= AssetsURL %>/css/<%= ThemeBaseStr %>/configure.css?v=2.4" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/js/promo.js"></script>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!--AnalyticsCode-->
    <%= AnalyticsCode %>
    <!--UniversalVariableCode-->
    
        <%= UniversalVariableCode %>
    
    <!--OpenTag-->
    <%= OpenTag %>
    <%=DebugInfo %>
</head>
<body>
    <form id="paymentForm" action="" class="<%= (QuoteConfirmationMode ? "quote" : "") %>">
        <%= DNAIMGTag %>
        <div id="header">
            <div class="headerContent">
                <div id="genericHeader">
                    <%--<a href="/">--%>
                    <%--<a href="<%= ImageURL %>">--%>
                    <a href="#">
                        <img src="<%= AssetsURL %>/images/<%= brandStr %>/icons/logo.png" alt="logo" />
                    </a>
                    <ul class="steps">
                        <li>
                            <span class="tab">1.
                                <label>Select</label></span>
                        </li>
                        <li>
                            <span class="tab">2.
                                <label>Payment</label></span>
                        </li>
                        <li class="selected">
                            <span class="tab">3.
                                <label>Confirm</label></span>
                        </li>
                    </ul>
                    <a class="contactUs" href="<%= ContactUsURL %>" target="_blank"><span>Contact us <i class="icon-chevron-down"></i></span></a>
                    <a class="contactUs contactUsMobile" href="<%= ContactUsURL %>" target="_blank"><span><i class="icon-envelope"></i></span></a>
                </div>
            </div>
        </div>
        <div id="paymentContainer" class="<%= BlankConfirmation %>">
            <div id="vehicleCatalogContainer">
                <h2><%= PaymentHeader %></h2>
                <div id="paymentPanel" class="Confirmation">
                    <div id="confirmationPanel">
                        <div id="ConfText" class="Section" style="display: <%= !OnRequestMode ? "block" : "none" %>">
                            <p>
                                Thank you for booking with <%= BrandNameStr %>. Below are the details of your booking. Please take a moment to check that all the details are correct.
                            </p>
                            <p>
                                An e-mail has been sent confirming your booking to: <%= CustomerEMail %>
                            </p>
                            <p>
                                Please note <%= BrandNameStr %> do not issue any further documentation for online bookings.
                            </p>
                            <p>
                                Please print the confirmation below or from the e-mail confirming your booking to present on collection of your vehicle.
                            </p>
                            <p>
                                Your booking reference is: <big><%= BookingId %></big>
                            </p>
                            <p class="promoContainer"></p>
                        </div>
                        <div id="onRequest" class="Section" style="display: <%= OnRequestMode ? "block" : "none" %>;">
                            <p>Thank you for submitting a booking request with <%= BrandNameStr %>! </p>
                            <p>We will now ensure that the vehicle and options you have requested are still available. We will then email you a detailed booking confirmation voucher and purchase receipt within 24 hours.</p>
                            <p>Payment will only be processed once a vehicle has been confirmed for your booking. If your first choice is no longer available, we will offer an alternative option for your consideration.</p>
                            <p>Your booking reference is: <big><%= BookingId %></big></p>
                            <p class="promoContainer"></p>
                        </div>
                        <div id="printLinkDiv" class="Section">
                            <div id="sealPanel">
                                <!--<script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=<%= SSLSeal %>"></script>
                            <script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=<%= SSLSeal %>&amp;size=L&amp;lang=en"></script>-->
                            </div>
                            <a href="#" onclick="window.print();" id="printItinLink">Print Itinerary</a>
                        </div>
                    </div>
                    <div id="yourDetails" class="Section">
                        <h2>Your details</h2>
                        <%= CustomerDetails %>
                    </div>
                    <div id="travelDetails" class="Section">
                        <h2>Travel details</h2>
                        <table>
                            <tr>
                                <td class="tc">Vehicle:</td>
                                <td><%= VehicleName %></td>
                            </tr>
                            <tr>
                                <td class="tc">Passengers:</td>
                                <td><%= PassengersStr %></td>
                            </tr>
                            <tr>
                                <td class="tc">Pick up:</td>
                                <td><%= PickUpStr %></td>
                            </tr>
                            <tr>
                                <td class="tc">Drop off:</td>
                                <td><%= DropOffStr  %></td>
                            </tr>
                        </table>
                    </div>
                    <div id="rentalSum" class="Section">
                        <h2>Rental charge summary</h2>
                        <%= ChargeSummaryPanel %>
                    </div>
                    <div id="paymentDet" class="Section">
                        <h2>Payment details</h2>
                        <%= PaymentSummaryPanel %>
                    </div>
                    <div id="contactUs" class="Section">
                        <h2>Contact us</h2>
                        <div>
                            If you need to contact us for any reason, the details are: 
                        </div>
                        <table>
                            <!-- TODO: from Brand Content Managed Data -->
                            <tbody>
                                <tr>
                                    <td><%= DomesticPhone %></td>
                                </tr>
                                <tr>
                                    <td><%= WorldWidePhone %></td>
                                </tr>
                                <tr>
                                    <td><a class="contactUsLink" target="_blank" href="<%= ParentSiteURL %>">contact us</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer starts -->
        <div id="footerContainer">
            <div id="footer">
                <div class="top">
                    <div id="disclaimer">
                        <span>
                            <strong>Please note:</strong> Rentals prices quoted above are only vaild at the time of booking.
                        </span>
                        <span>Alternative currency is based on approximate current exchange rates. All payments will be processed in <%= Currency %>.</span>
                    </div>
                    <div id="footerlogo">
                        <a href="#">
                        <%--<a href="<%= ImageURL %>">--%>
                            <img src="<%= AssetsURL %>/images/<%= brandStr %>/icons/foot_logo.png" alt="Worldwide Rentals - Motorhomes" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div id="footerWrap">
                <div class="thl body">
                    <ul>
                        <li>
                            <a class="thl-logo" href="http://www.thlonline.com">
                                <span class="thl"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.keacampers.com">
                                <span class="kea"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.maui-rentals.com">
                                <span class="maui"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.britz.com">
                                <span class="britz"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.unitedcampervans.com">
                                <span class="united"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.alphacampers.com">
                                <span class="alpha"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.mightycampers.com">
                                <span class="mighty"></span>
                            </a>
                        </li>
                    </ul>
                    <span class="privacy">
                        <b>&copy; Tourism Holidings Limited</b><br />
                        &nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Privacy Policy</a>
                    </span>
                </div>
            </div>
        </div>
        <!-- footer ends -->
        <!-- Google Code for Online Confirmation Conversion Page -->
        <script type="text/javascript">
            <!--
            var google_conversion_id = <%= ConversionId %> ;

               

            if(google_conversion_id > 0)
            {            
                var google_conversion_language = "en_GB";
                var google_conversion_format = "1";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "<%= ConversionCode %>";
                    var google_conversion_value = 0;
                }
            //if (<%= TotalConversionAmount %>) {
            //    google_conversion_value = 0;
            //}
            //-->
        </script>
        <script type="text/javascript" src="<%= HTTPPrefix %>www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
            <div style="display: inline;">
                <img height="1" width="1" style="border-style: none;" alt="" src="<%= HTTPPrefix %>www.googleadservices.com/pagead/conversion/<%= ConversionId %>/?value=<%= TotalConversionAmount %>&amp;label=<%= ConversionCode %>&amp;guid=ON&amp;script=0" />
            </div>
        </noscript>
        <%= XaxisConfirmation %>
    </form>
    <%= RemarketingCode %>
</body>
</html>
