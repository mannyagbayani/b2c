﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="QuoteConfirmation.aspx.cs" Inherits="QuoteConfirmation" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Quote Confirmation Page</title>    
    <%--<link href="<%= AssetsURL %>/css/Base.css" type="text/css" rel="stylesheet" />    
    <link href="<%= AssetsURL %>/css/<%= brandStr %>/selection.css" type="text/css" rel="stylesheet" />           
    <style type="text/css">
        label { width: 10em; float: left; }
        label.error { float: none; color: red; padding-left: .5em; vertical-align: top; }       
    </style> --%>
    <link href="<%= AssetsURL %>/css/Base.css?v=<%= Version %>" type="text/css" rel="stylesheet" />
    <link href="<%= AssetsURL %>/css/<%= BrandChar %>/configure.css" type="text/css" rel="stylesheet" />    
    <link href="<%= CSSLink %>?v=<%= Version %>" type="text/css" rel="stylesheet" />
    <link href="<%= AssetsURL %>/css/jquery.ui.dialog.css" rel="stylesheet" type="text/css"/>      
    <link href="<%= AssetsURL %>/css/<%= BrandChar %>/jcal.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript" src="<%= ContentURL %>js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.validate.js"></script>  
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.dimensions.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.tooltip.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/date.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/hCtrl.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/selection.js"></script>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"> 
    <%= AnalyticsCode %>
    <%= OpenTag %>

</head>
<body>
    <form id="paymentForm" action="">
        <%= DNAIMGTag %>
        <script type="text/javascript">
            jQuery(document).ready(function() { /*initConfirmationPage();*/ });
        </script>
        <div id="header">
            <div class="headerContent">
                <div id="genericHeader">
                    <%--<a href="/">--%>
                    <a href="#">
                        <img src="<%= AssetsURL %>/images/<%= BrandChar %>/icons/logo.png" alt="logo" />
                    </a>
                    <ul class="steps">
                        <li>
                            <span class="tab">1.
                                <label>Select</label></span>
                        </li>
                        <li>
                            <span class="tab">2.
                                <label>Extras</label></span>
                        </li>
                        <li class="selected">
                            <span class="tab">3.
                                <label>Saved</label></span>
                        </li>
                    </ul>
                    <a class="contactUs" href="<%= ContactUsURL %>" target="_blank"><span>Contact us <i class="icon-chevron-down"></i></span></a>
                    <a class="contactUs contactUsMobile" href="<%= ContactUsURL %>" target="_blank"><span><i class="icon-envelope"></i></span></a>
                </div>
            </div>
        </div>
        <div id="paymentContainer" class="<%= BlankConfirmation %> quote">
            <%--<div id="catalogHeader">
                <span class="Image">
                    <img src="<%= AssetsURL %>/images/<%= brandStr %>/logo.jpg" alt="logo" />
                </span>
                <span class="ContactInfo">
                    <%= ContactHeaderText %> or <a target="_blank"  href="<%= ParentSiteURL %>/contactus" >contact us</a>
                </span>
            </div>           
            <div id="breadCrumbs">
                <span class="Crumbs">
                    <a href="<%= ParentSiteURL %>" class="HomeLnk">Home</a>&nbsp;&gt;&nbsp;Booking Completed
                </span>
                <ul id="stepsDisplayer">
                    <li>
                        <span id="PaymentStep">3.Payment</span>
                    </li>
                    <li>
                        <span id="ConfigureStep">2.Configure</span>
                    </li>
                    <li>
                        <span id="SelectStep">1.Select</span>
                    </li>
                </ul>
            </div>--%> 
            <div id="vehicleCatalogContainer">
            <h2>Quote Saved</h2>
            <div id="paymentPanel" class="Confirmation">
                <div id="confirmationPanel">
                    <div id="ConfText" class="Section" style="display:<%= !OnRequestMode ? "block" : "none" %>" >
                        <p>
                            Below are the details of your saved quote. Please take a moment to check that all the details are correct.
                        </p>
                        <p>
                            An e-mail has been sent confirming your quote to: <%= CustomerEMail %>
                        </p>
                        <p>
                            The price quoted will be valid for 1 week from today. Your quote can be confirmed by phoning reservations or replying to the quote e-mail.
                        </p>
                        <p>
                            Please note vehicle availability will be re-checked before your booking can be confirmed.
                        </p>
                        <p>
                            Your quote reference is: <big><%= BookingId %></big>
                        </p>
                        
                        <p>
                            You can retrieve your quote and confirm your booking online by clicking on the link provided in your e-mail or by contacting our 24/7 reservation team.
                        </p>                        
                    </div>                  
                    <div id="printQuoteDiv" class="Section">
                        <div id="sealPanel">
                            <!--<script type="text/javascript" src="https://seal.thawte.com/getthawteseal?host_name=<%= SSLSeal %>&amp;size=L&amp;lang=en"></script>-->
                        </div>
                        <a href="#" onclick="window.print();" id="printQuoteLnk">Print Quote</a>
                        <a href="Selection.aspx?<%= RequestQueryParams %>" id="searchAgainLnk">Search Again</a>
                    </div>                    
                </div>
                <div id="yourDetails" class="Section">
                    <h2>Your details</h2>
                    <%= CustomerDetails %> 
                </div>
                <div id="travelDetails" class="Section">
                    <h2>Travel details</h2>
                    <table>
                        <tr>
                            <td class="tc">Vehicle:</td>
                            <td><%= VehicleName %></td>
                        </tr>
                        <tr>
                            <td class="tc">Passengers:</td>
                            <td><%= PassengersStr %></td>
                        </tr>
                        <tr>
                            <td class="tc">Pick up:</td>
                            <td><%= PickUpStr %></td>
                        </tr>
                        <tr>
                            <td class="tc">Drop off:</td>
                            <td><%= DropOffStr  %></td>
                        </tr>
                    </table>
                </div>
                <div id="rentalSum" class="Section">                   
                    <h2>Rental charge summary</h2>
                     <%= ChargeSummaryPanel %>
                    
                </div>
                <div id="paymentDet" class="Section">
                    <h2>Payment details</h2>
                    <%= PaymentSummaryPanel %>
                </div>
                <div id="contactUs" class="Section">
                    <h2>Contact us</h2>
                    <div>
                        If you need to contact us for any reason, the details are: 
                    </div>
                    <table><!-- TODO: from Brand Content Managed Data -->
                        <tbody>
                            <tr>
                                <td><%= DomesticPhone %></td>
                            </tr>                            
                            <tr>
                                <td><%= WorldWidePhone %></td>
                            </tr>                            
                            <tr>
                                <td><a class="contactUsLink" target="_blank"  href="<%= ParentSiteURL %>" >contact us</a></td>
                            </tr>
                        </tbody>                    
                    </table>                   
                </div>                
            </div>
        </div>
        <%--<div id="disclaimer">
            <span>Please note: Rental prices quoted above are only valid at the time of booking.</span>
        </div>--%>
    </div>
    <!-- Google Code for Online Confirmation Conversion Page -->
    <script type="text/javascript">
        <!--
        var google_conversion_id = <%= ConversionId %>;
        if(google_conversion_id > 0)
        {            
            var google_conversion_language = "en_GB";
            var google_conversion_format = "1";
            var google_conversion_color = "ffffff";
            var google_conversion_label = "<%= ConversionCode %>";
            var google_conversion_value = 0;
        }
        //if (<%= TotalConversionAmount %>) {
        //    google_conversion_value = 0;
        //}
        //-->
    </script>
    <script type="text/javascript" src="<%= HTTPPrefix %>www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="<%= HTTPPrefix %>www.googleadservices.com/pagead/conversion/<%= ConversionId %>/?value=<%= TotalConversionAmount %>&amp;label=<%= ConversionCode %>&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>

        <!-- footer starts -->
        <div id="footerContainer">
            <div id="footer">
                <div class="top">
                    <div id="disclaimer">
                        <span>
                            <strong>Please note:</strong> Rentals prices quoted above are only vaild at the time of booking.
                        </span>
                        <span>Alternative currency is based on approximate current exchange rates. All payments will be processed in <%= Currency %>.</span>
                    </div>
                    <div id="footerlogo">
                        <a href="#">
                            <img src="<%= AssetsURL %>/images/<%= BrandChar %>/icons/foot_logo.png" alt="Worldwide Rentals - Motorhomes" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div id="footerWrap">
                <div class="thl body">
                    <ul>
                        <li>
                            <a class="thl-logo" href="http://www.thlonline.com">
                                <span class="thl"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.keacampers.com">
                                <span class="kea"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.maui-rentals.com">
                                <span class="maui"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.britz.com">
                                <span class="britz"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.unitedcampervans.com">
                                <span class="united"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.alphacampers.com">
                                <span class="alpha"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.mightycampers.com">
                                <span class="mighty"></span>
                            </a>
                        </li>
                    </ul>
                    <span class="privacy">
                        <b>&copy; Tourism Holidings Limited</b><br />
                        &nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Privacy Policy</a>
                    </span>
                </div>
            </div>
        </div>
        <!-- footer ends -->
    <%= XaxisConfirmation %>   
    </form>   
    <%= RemarketingCode %> 
</body>
</html>