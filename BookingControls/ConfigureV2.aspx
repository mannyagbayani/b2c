﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ConfigureV2.aspx.cs" Inherits="ConfigureV2" %>

<%@ Register Src="HorizontalControl.ascx" TagName="HorizontalControl" TagPrefix="thl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Configure Vehicle Page</title>
    <link href="<%= AssetsURL %>/css/Base.css?v=<%= Version %>" type="text/css" rel="stylesheet" />
    <link href="<%= CSSLink %>?v=<%= Version %>" type="text/css" rel="stylesheet" />
    <!--<script type="text/javascript" src="<%= ContentURL %>js/jquery-1.3.1.min.js"></script>-->
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <script src="/js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.dimensions.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.tooltip.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/date.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/hCtrl.js?v=<%= Version %>"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/selection.js?v=<%= Version %>"></script>

    <![if !IE]>
    <script type="text/javascript" src="<%= ContentURL %>js/enquire.min.js?v=<%= Version %>"></script>
    <![endif]>

    <link href="<%= AssetsURL %>/css/jquery.ui.dialog.css" rel="stylesheet" type="text/css" />
    <link href="<%= AssetsURL %>/css/<%= brandStr %>/jcal.css" type="text/css" rel="stylesheet" />

    <style type="text/css">
        .QuestText
        {
            height: 22px;
            float: left;
            overflow: hidden;
            /*text-decoration: none;*/
            /*color: black !important;*/
            text-decoration:underline;
            line-height:22px;
        }

            .QuestText:hover
            {
                /*color: #3366CC !important;*/
                /*text-decoration:none;*/
            }
    </style>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <%= AnalyticsCode %>
    <%= OpenTag %>
</head>
<body>
    <form id="form1" runat="server" onsubmit="return serializeBookingInfo()">
        <%= DNAIMGTag %>
        <script type="text/javascript">
            var ContentbaseUrl = '<%= ContentURL %>';
            var currencyStr = '<%=  CurrencyStr %>';
            var vehicleCharges = <%= VehicleJSON %>;
            var extraHireItems = <%= ExtraHireItemsJSON %>;
            var insuranceProducts = <%= InsuranceProductJSON %>;
            var ferryItems = <%= FerryJSON %>;
            var availabilityItem = <%= AvailabilityJSON %>;            
            var allInclusive = <%= AllInclusiveJSON %>;
            var oneWayFeeWaiver = <%= OneWayFee %>;
            var $j = jQuery.noConflict();
            var requestParams = <%= AvailRequestJSON %>;
            jQuery(document).ready(function() { 
                initConfigurePage(); 
                $j("#bottomRight a").click(function() {
                    _gaq.push(['_trackEvent','UIEvent','ConfigureBookingControlSubmit']);                
                });
                
                $j('#insurancePanel h4').text('Accident Liability & Security Deposit:');
                $j('#insurancePanel #insOptionsTable tr td.price').each(function(){ if($j(this).html() == "Included") $j(this).html("");});
            
            });
        </script>
        <div id="header">
            <div class="headerContent">
                <div id="genericHeader">
                    <%--<a href="/">--%>
                    <%--<a href="<%= ImageURL %>">--%>
                    <a href="#">
                        <img src="<%= AssetsURL %>/images/<%= BrandChar %>/icons/logo.png" alt="logo" />
                    </a>
                    <ul class="steps">
                        <li>
                            <span class="tab">1.
                                <label>Select</label></span>
                        </li>
                        <li class="selected">
                            <span class="tab">2.
                                <label>Extras</label></span>
                        </li>
                        <li>
                            <span class="tab">3.
                                <label>Payment</label></span>
                        </li>
                    </ul>
                    <a class="contactUs" href="<%= ContactUsURL %>" target="_blank"><span>Contact us <i class="icon-chevron-down"></i></span></a>
                    <a class="contactUs contactUsMobile" href="<%= ContactUsURL %>" target="_blank"><span><i class="icon-envelope"></i></span></a>
                </div>
            </div>
        </div>
        <div id="vehicleConfigureContainer">
            <div id="vehicleCatalogContainer">
                <h2>2. Configure your vehicle</h2>
                <div id="promptTextArea" class='<%= HasMsg ? "" : "Hide" %>'>
                    <span><%= VehicleDetailsText %></span>
                </div>
                <div id="configurePanel">
                    <div id="topHeader">
                        <h3>
                            <a class='VehicleTitle PopUp' href='http://<%= VehicleContentLink %>'>
                                <%= BrandTitle %> <%= SelectedVehicle.Name %>
                            </a>
                        </h3>
                        <span class="CurrencyPanel">
                            <small>Alternative Currency:</small>
                            <%= CurrencySelector %> 
                        </span>
                    </div>
                    <div id="vehicleTopPanel">
                        <div class="VehicleHeader">
                            <h4>Travel details:&nbsp;</h4>
                            <div class="Locations">
                                <span><b>Pick up:</b> <%= PickUpLocationStr %>, <%= PickUpDateStr %></span>
                                <span><b>Drop off:</b> <%= DropOffLocationStr%>, <%= DropOffDateStr %></span>
                            </div>
                        </div>
                        <div class="VehicleImages">
                            <div class="Left">
                                <img src="<%= VehicleInclusionPath %>-InclusionIcons.gif" alt="<%= SelectedVehicle.Name %> inclusions" title="<%= SelectedVehicle.Name %> inclusions" />
                            </div>
                            <div class="Right">
                                <img src="<%= VehicleImagePath %>-ClearCut-144.jpg" title="<%= SelectedVehicle.Code %>" alt="<%= SelectedVehicle.Code %>" />
                            </div>
                        </div>
                    </div>
                    <div id="vehicleChargeDetails" class="sections">
                        <div class="sectionHeader">
                            <h4>Vehicle charges:&nbsp;</h4>
                            <a href="http://<%= VehicleChargeContentURL %>" class="Quest PopUp">?</a>
                            <span class="openclose"><i class="icon-plus"></i></span>
                        </div>
                        <div class="sectionContent">
                            <span id="ChargeHeader">
                                <span class="Title">
                                    <small>Vehicle Charge</small>
                                    <a href="#details" class="ShowPriceList">Close Details</a>
                                </span>
                                <span class="Total">
                                    <small><%= VehiclePriceStr %></small>
                                </span>
                            </span>
                            <%= PriceDetailsDiv %>
                            <%= AllInclusivePanel %>
                        </div>
                    </div>
                    <%= InusranceOptionsStr %>
                    <div id="hireItemsPanel" class="sections">
                        <%= ExtraHiresTable %>
                    </div>
                    <div id="ferryPanel" class="sections">
                        <%= FerryPanel %>
                        <input type="hidden" id="ferryInfo" name="ferryInfo" />
                    </div>
                    <div id="configureUpdatePanel">
                        <span class="Total">
                            <label>Current total:</label>
                            <span class="Price">
                                <small id="totalAmount">0.00</small>
                                <small id="alternateCurrency">0.0</small>
                            </span>
                        </span>
                        <span class="TaxMsg <%= (AdminFee > 0 && !hasBonusPack ? "HasAdminFee" : "") %>">
                            <small>Total includes all taxes (GST)</small>
                            <big>Includes <%= AdminFee %>% Admin Fee</big>
                        </span>
                        <span class="Submit">
                            <a class="BackBtn" href="javascript:history.back();">Back</a>
                            <input type="submit" value="Payment Details" />
                            <a id="saveQuoteLnk" href="javascript:saveQuote();" title="Save your quote">Save Quote</a>
                            <input type="hidden" id="bookingSubmitted" name="bookingSubmitted" value="0" />
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer starts -->
        <div id="footerContainer">
            <div id="footer">
                <div class="top">
                    <div id="disclaimer">
                        <span>
                            <strong>Please note:</strong> Rentals prices quoted above are only vaild at the time of booking.
                        </span>
                        <span>Alternative currency is based on approximate current exchange rates. All payments will be processed in <%= Currency %>.</span>
                    </div>
                    <div id="footerlogo">
                        <a href="#">

                            <img src="<%= AssetsURL %>/images/<%= BrandChar %>/icons/foot_logo.png" alt="Worldwide Rentals - Motorhomes and Cars.com" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div id="footerWrap">
                <div class="thl body">
                    <ul>
                        <li>
                            <a class="thl-logo" href="http://www.thlonline.com">
                                <span class="thl"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.keacampers.com">
                                <span class="kea"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.maui-rentals.com">
                                <span class="maui"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.britz.com">
                                <span class="britz"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.unitedcampervans.com">
                                <span class="united"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.alphacampers.com">
                                <span class="alpha"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.mightycampers.com">
                                <span class="mighty"></span>
                            </a>
                        </li>
                    </ul>
                    <span class="privacy">
                        <b>&copy; Tourism Holidings Limited</b><br />
                        &nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Privacy Policy</a>
                    </span>
                </div>
            </div>
        </div>
        <!-- footer ends -->
        <!-- Debug Code Starts -->
        <%= debugHTML %>
        <!-- Debug Code Ends -->
        <input type="hidden" id="vehicleBrand" name="vehicleBrand" value="<%= VehicleBrand %>" />
        <input type="hidden" id="ctrlCOR" name="ctrlCOR" value="<%= CTRLCoR %>" />
    </form>
    <script>
        $j(document).ready(function () {
            if (navigator.userAgent.indexOf("MSIE") < 0) {//todo: >9
                enquire.register("screen and (max-width:481px)", {
                    match: function () {
                        $j('.sections').find('.sectionContent').css('display', 'none'); ;
                        // Mobile Accordion 
                        $j('.sections .sectionHeader').on('click', function () {
                            $j(this).find('.openclose i').toggleClass('icon-minus icon-plus');
                            $j(this).next().slideToggle();
                            return false;
                        });
                    },
                    unmatch: function () {
                        $j('.sections').find('.sectionContent').css('display', 'block');
                        $j('.sections').find('.openclose i').removeClass('icon-minus').addClass('icon-plus');
                        $j('.sections .sectionHeader').off('click');
                    }
                });
            }
        });				
    </script>
    <%= RemarketingCode %>
</body>
</html>
