﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class EmailBooking : System.Web.UI.Page
{

    public string ContentURL, AssetsURL;
    public string brandStr, VehicleCode;   
    public string RentalRequestSummary;

    public string DNAIMGTag, AnalyticsCode, SSLSeal;

    public string HTTPPrefix;

    ApplicationData appData;

    public string debugURL;
    
    protected void Page_Load(object sender, EventArgs e)
    {

            bool secureMode = false;//TODO: move to config manager
            bool.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["SecureMode"], out secureMode);

            HTTPPrefix = ApplicationManager.HTTPPrefix;   

            THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
            if (appDomain == null)//dubug line
                appDomain = new THLDomain("secure.maui.co.nz", THLBrands.Maui, CountryCode.NZ, "B2CNZ", false, "uahere");//dubug line
            brandStr = appDomain.BrandChar.ToString(); //(Request.Params["brand"] != null ? Request.Params["brand"] : "m");//TODO: this should be swapped by baseURL. 
            ContentURL = HTTPPrefix + appDomain.Name + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["BookingControlContentPath"];
            string countryCode = appDomain.Country.ToString().ToUpper();  //(Request.Params["cc"] != null ? Request.Params["cc"] : "NZ").ToUpper();   

            //DecideDNA Tracking Img
            TrackingDisplayer trd = new TrackingDisplayer(appDomain, secureMode);
            DNAIMGTag = trd.RenderDNATrackingImgSource("limitedAvailability" , "");

            //Analytics Tracking Code
            AnalyticsCode = trd.RenderAnalsCode();

            SSLSeal = trd.GetSealID();            

            //ContentURL = System.Web.Configuration.WebConfigurationManager.AppSettings["BookingControlContentURL"];
            AssetsURL = HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];
            SessionData sessionData = SessionManager.GetSessionData();
            appData = ApplicationManager.GetApplicationData();
            //brandStr = (Request.Params["brand"] != null ? Request.Params["brand"] : "m");//TODO: this should be swapped by baseURL/Session
            string vehicleCode = Request.Params["vCode"];
        
            if (sessionData.HasRequestData && sessionData.HasResponseData && vehicleCode != null)
            {
                VehicleCode = vehicleCode;
                AvailabilityRequest aReq = sessionData.AvailabilityRequestData;
                AvailabilityResponse aRes = sessionData.AvailabilityResponseData;
                CurrencyManager curMan = new CurrencyManager(CurrencyConversionProvider.BNZ, CurrencyCode.NZD);//from Application Cache
                AvailabilityResponseDisplayer ard = new AvailabilityResponseDisplayer(aRes, aReq, curMan, AssetsURL, appDomain.Brand);
                RentalRequestSummary = ard.RenderRequestSummary(aReq, vehicleCode);
            }
    }
}
