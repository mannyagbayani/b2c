﻿using System;
using System.Xml;
using System.Collections.Specialized;
using System.Text;
using System.Diagnostics;
using THL.Booking;

public enum ErrorTypes {
    Application=0,
    Database=1,
    ExternalProvider=2,
    Presentation=3
}

public enum EventTypes {
    DBRequest=0,
    CacheRequest=1,
    ClearCache=2,
    TimeMeasurement=3,
    MTierStatus=4,
    HTTPRequest
}

/// <summary>
/// Debug and logging functionality for the THL mapping platform
/// </summary>
public class THLDebug
{

    public static bool ErrorLogOn = Convert.ToBoolean(System.Web.Configuration.WebConfigurationManager.AppSettings["ErrorLoging"]);
    public static bool EventLogOn = Convert.ToBoolean(System.Web.Configuration.WebConfigurationManager.AppSettings["EventLoging"]);
    //PaymentLogOn  was added by nimesh on 25th Sep 2014 to check paymentloging flag from config
    public static bool PaymentLogOn = Convert.ToBoolean(System.Web.Configuration.WebConfigurationManager.AppSettings["PaymentLoging"]);
    public static bool BookingLogOn = Convert.ToBoolean(System.Web.Configuration.WebConfigurationManager.AppSettings["BookingLoging"]);

    static string logPath;
    static string eventPath;
    
    static THLDebug()
	{
        logPath = System.Web.Configuration.WebConfigurationManager.AppSettings["logPath"];
        eventPath = System.Web.Configuration.WebConfigurationManager.AppSettings["eventPath"];
	}

    public static bool LogError(ErrorTypes errType, string errMethod, string errStr, string errParams) 
    {
        if (ErrorLogOn == false) return false;
                
        EventLog ev = new EventLog("Application");	    
	    ev.Source = "B2C";
        
        if (!EventLog.SourceExists("B2C"))
            EventLog.CreateEventSource("B2C", "B2C");
		
        try
		{
			ev.WriteEntry(errType + ":" + errMethod + ":" + errStr, EventLogEntryType.Warning, DateTime.Now.Millisecond, 2);
            return true;//If that fails will write to FS log
		}
        catch(Exception ex)
        {
            string eventWriteError = ex.Message;
        }
		ev = null;
                    
        System.IO.StreamWriter sw = System.IO.File.AppendText(logPath + "bookings.log");
        try
        {
            string logLine = DateTime.Now + ", " + errType + ", " + errMethod + ", " + errStr + ", " + errParams;
            sw.WriteLine(logLine);
            return true;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return false;            
        }  
        finally
        {
            sw.Close();
        }              
    }
    
    public static bool LogXML(string fileStamp, XmlNode xmlNode)
    {
        if (EventLogOn == false) return false;
        System.IO.StreamWriter sw = System.IO.File.AppendText(eventPath + fileStamp + ".xml");
        try
        {
            sw.WriteLine(xmlNode.OuterXml);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
            sw.Close();
        }    
    
    }
    
    public static bool LogXML(string fileStamp, XmlDocument xmldocument)
    {
        if (EventLogOn == false) return false;
        System.IO.StreamWriter sw = System.IO.File.AppendText(eventPath + fileStamp + ".xml");
        try
        {            
            sw.WriteLine(xmldocument.OuterXml);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
            sw.Close();
        }    
    }

    public static bool LogBookingXML(string fileStamp, XmlDocument xmldocument)
    {
        if (BookingLogOn == false) return false;
        System.IO.StreamWriter sw = System.IO.File.AppendText(eventPath + fileStamp + ".xml");
        try
        {
            if (xmldocument.SelectSingleNode("//CreditCardDetails/CreditCardNumber") != null)
            {
                string ccNumber = xmldocument.SelectSingleNode("//CreditCardDetails/CreditCardNumber").InnerText;
                string maskCCNumber = "xxxx-xxxx-xxxx-" + ccNumber.Substring(ccNumber.Length - 4, 4);//mask:
                sw.WriteLine(xmldocument.OuterXml.Replace(ccNumber, maskCCNumber));
            }
            else//no CC (Quote) 
            {
                sw.WriteLine(xmldocument.OuterXml);
            }
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
            sw.Close();
        }
    }

    public static bool LogBookingXML(string fileStamp, XmlNode xmlNode)
    {
        if (BookingLogOn == false) return false;
        System.IO.StreamWriter sw = System.IO.File.AppendText(eventPath + fileStamp + ".xml");
        try
        {
            if (xmlNode.SelectSingleNode("//CreditCardDetails/CreditCardNumber") != null)
            {
                string ccNumber = xmlNode.SelectSingleNode("//CreditCardDetails/CreditCardNumber").InnerText;
                string maskCCNumber = "xxxx-xxxx-xxxx-" + ccNumber.Substring(ccNumber.Length - 4, 4);//mask:
                sw.WriteLine(xmlNode.OuterXml.Replace(ccNumber, maskCCNumber));
            }
            else
                sw.WriteLine(xmlNode.OuterXml);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
            sw.Close();
        }

    }

    public static bool LogEvent(EventTypes evType, string evParams)
    {
        if (EventLogOn == false) return false;
        System.IO.StreamWriter sw = System.IO.File.AppendText(eventPath + "bookings.evtx");
        try
        {
            string logLine = DateTime.Now + ", " + evType + ", " + evParams;
            sw.WriteLine(logLine);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
            sw.Close();
        }
    }

    /// <summary>
    /// Log Quote information 
    /// TODO: define log file path
    /// </summary>
    /// <param name="fileStamp"></param>
    /// <param name="xmldocument"></param>
    /// <returns>write success status</returns>
    public static bool LogQuoteInfo(string fileStamp, XmlDocument quoteXml, XmlDocument packageXml)
    {
        if (BookingLogOn == false) return false;
        System.IO.StreamWriter sw = System.IO.File.CreateText(eventPath+ "/quote/" + fileStamp + ".xml");
        try
        {
            sw.WriteLine("<quoteData>");
            sw.WriteLine(quoteXml.OuterXml);
            sw.Write(packageXml.OuterXml);
            sw.WriteLine("</quoteData>");
            return true;
        }
        catch (Exception ex)
        {
            string msg = ex.Message;
            //TODO: possiblly raise event
            return false;
        }
        finally
        {
            sw.Close();
        }
    }
    
    public static int GetObjectKBSize(object obj) {
        return System.Runtime.InteropServices.Marshal.SizeOf(obj);    
    }

    /// <summary>
    /// Quote a Name Value Collection to JSON 
    /// </summary>
    /// <param name="nvc"></param>
    /// <returns></returns>
    public static string DisplayNameValueParams(NameValueCollection nvc)
    {
        StringBuilder sb = new StringBuilder("{");
        foreach (string key in nvc.AllKeys)
        {
            sb.Append(key + ":" + nvc[key] + ",");
        }
        return sb.ToString().TrimEnd(',') + "}";
    }


    public static string DisplayApplicationData(ApplicationData appData)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<h2>Tracking Elements</h2>");
        sb.Append("<table>");
        foreach (TrackingData td in appData.TrackingElements)
        {
            sb.Append("<tr>");
            sb.Append("<td>"+ td.Code +"</td><td>"+ td.Domain +"</td><td>"+ td.Event +"</td><td>"+ td.ID +"</td>");
            sb.Append("<tr>");
        }
        sb.Append("</table>");
        //TODO: add possible additional data quoting


        ContentElement[] cacheElements = ApplicationManager.GetContentElements();
        sb.Append("<h2>Content Resources Elements</h2>");
        sb.Append("<table>");
        foreach (ContentElement elm in cacheElements)
        {
            sb.Append("<tr>");
            sb.Append("<td>" + elm.Brand + "</td><td>" + elm.VehicleType + "</td><td>" + elm.Code + "</td><td>" + elm.Country + "</td><td>" + elm.Content + "</td><td>" + elm.ContentType + "</td><td>" + elm.StartTime + "</td><td>" + elm.EndTime + "</td>");
            sb.Append("<tr>");            
        }


        cacheElements = ApplicationManager.GetMACContentElements();
        sb.Append("<h2>MaC Content Resources Elements</h2>");
        sb.Append("<table>");
        foreach (ContentElement elm in cacheElements)
        {
            sb.Append("<tr>");
            sb.Append("<td>" + elm.Brand + "</td><td>" + elm.VehicleType + "</td><td>" + elm.Code + "</td><td>" + elm.Country + "</td><td>" + elm.Content + "</td><td>" + elm.ContentType + "</td><td>" + elm.StartTime + "</td><td>" + elm.EndTime + "</td>");
            sb.Append("<tr>");
        }
        sb.Append("</table>");
        //Vehicle[] macVehicles =  ApplicationManager.GetMacVehicles();
        sb.Append("<br />Currency Exchange Age: " + appData.CurrencyXML.SelectSingleNode("/content").Attributes["validdate"].Value);      
        
        
        return sb.ToString();
    }


}