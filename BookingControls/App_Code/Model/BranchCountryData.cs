﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace THL.Booking {

    /// <summary>
    /// Summary description for BranchCountryData
    /// </summary>
    public class BranchCountryData
    {
        public string CountryCode;
        
        public string CarsOpenHour;
        public string CarsCloseHour;
        public string CarsDefaultHour;

        public string CampersOpenHour;
        public string CampersCloseHour;
        public string CampersDefaultHour;
        
        public int MaxAdultsCamper;
        public int MaxChildrenCamper;
        public int MaxAdultsCar;
        public int MaxChildrenCar;
        public int MaxPassengersCar;
        public int MaxPassengersCamper;
        
        /// <summary>
        /// Manage Vehicle Availability accross country's branches
        /// </summary>
        private VehicleType vehicleAvailability = VehicleType.None;
        public VehicleType VehicleAvailability
        {
            get { return vehicleAvailability; } 
        }

        public void AddAvailability(VehicleType availability) 
        {
            if (vehicleAvailability == VehicleType.None)
                vehicleAvailability = availability;
            else
                vehicleAvailability = (vehicleAvailability == availability ? availability : VehicleType.Both);            
        }


        public BranchCountryData()
	    {
    		
	    }


        public bool SetPassengerInfo(XmlNode countryDataNode) {
            //try
            int.TryParse(countryDataNode.Attributes["maxAdultsCamper"].Value, out MaxAdultsCamper);
            int.TryParse(countryDataNode.Attributes["maxChildrenCamper"].Value, out MaxChildrenCamper);
            int.TryParse(countryDataNode.Attributes["maxAdultsCar"].Value, out MaxAdultsCar);
            int.TryParse(countryDataNode.Attributes["maxChildrenCar"].Value, out MaxChildrenCar);
            int.TryParse(countryDataNode.Attributes["totalCamper"].Value, out MaxPassengersCamper);
            int.TryParse(countryDataNode.Attributes["totalCar"].Value, out MaxPassengersCar);
            //catch for false
            return true;//TODO: exp handle    
        }

        public bool SetBranchTimes(XmlNode dataNode) {
            //try
            //{
                foreach (XmlNode timeNode in dataNode.SelectNodes("timeRange"))
                {
                    if (timeNode.Attributes["vehicle"].Value == "ac")
                    {
                        CarsOpenHour = timeNode.Attributes["open"].Value;
                        CarsCloseHour = timeNode.Attributes["close"].Value;
                        CarsDefaultHour = timeNode.Attributes["default"].Value;
                    }
                    else
                    {
                        CampersOpenHour = timeNode.Attributes["open"].Value;
                        CampersCloseHour = timeNode.Attributes["close"].Value;
                        CampersDefaultHour = timeNode.Attributes["default"].Value;
                    }
                }
                return true;//success
            //} catch()//return false failed to load opening times..
        }
    }
}