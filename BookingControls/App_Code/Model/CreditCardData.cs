﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace THL.Booking
{

    public enum CreditCardStatus
    { 
        Authorised = 0,
        Charged = 1
    }
    
    public enum CreditCardType
    { 
        NONE = 0,
        VISA = 1,
        MASTERCARD = 2,
        AMEX = 3,
        DINERS = 4
    }

    /// <summary>
    /// CreditCard Information and Approval Data
    /// </summary>
    public class CreditCardData
    {               
        public static CreditCardType GetCardTypeForString(string cardString)
        {
            cardString = cardString.ToUpper();
            CreditCardType cardType = CreditCardType.NONE;
            switch (cardString)
            { 
                case "VISA":
                    cardType = CreditCardType.VISA;
                    break;
                case "MASTERCARD":
                    cardType = CreditCardType.MASTERCARD;
                    break;    
                case "AMEX":
                    cardType = CreditCardType.AMEX;
                    break;
                case "DINERS" :
                    cardType = CreditCardType.DINERS;
                    break;
            }
            return cardType;
        }
        
        /// <summary>
        /// Validates the card number (using the Luhn Algorithm) consider additional levels(Expiry,Name)
        /// </summary>
        public bool IsValid
        {
            get {
                //if (!PaymentHelper.ValidateCard(_cardType, _cardNumber)) {
                //    validationErrorMassage = "C/C card validation";
                //    return false;
                //} //no need for check sum card validation
                if ((_customer.FirstName != null && _customer.FirstName.Length > 0) && (_customer.LastName != null && _customer.LastName.Length > 0))
                {
                    validationErrorMassage = string.Empty;
                    return true;
                }
                else
                {
                    validationErrorMassage = "Customer Name validation";
                    return false;
                }
            }
        }

        /// <summary>
        /// Generate a Booking Reference to pass forward to DPS for identifying the record
        /// for future accounting, may need revisiting 
        /// </summary>
        /// <returns></returns>
        public string GeneratedBookingReference()
        {
            string generatedReference = _customer.FirstName + "," + _customer.LastName;
            
            // Added by Nimesh on 29th Apr 2015
            // ref : https://thlonline.atlassian.net/browse/DESK-3257 ,
            //     https://thlonline.atlassian.net/browse/DESK-3256
            if(!AdditionalReference.Trim().Equals(string.Empty))
                generatedReference += "," + AdditionalReference;
            // End Added by Nimesh on 29th Apr 2015
            return generatedReference;
        }
        
        string validationErrorMassage;
        /// <summary>
        /// User Friendly Message on failed card validation 
        /// </summary>
        public string ValidationErrorMessage
        {
            get { return validationErrorMassage; }
        }

        /// <summary>
        /// The Cards Type (Credit Company)
        /// </summary>
        CreditCardType _cardType;
        public CreditCardType CardType
        {
            get { return _cardType;  }
            set { _cardType = value; }
        }
        
        /// <summary>
        /// Status of this Purchase
        /// </summary>
        CreditCardStatus _status;
        public CreditCardStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        string _cardNumber;
        /// <summary>
        /// The Cards Number
        /// </summary>
        public string CardNumber
        {
            get { return _cardNumber; }
        }

        
        string _cardHoldersName;
        public string CardHolderName
        {
            get { return _cardHoldersName; }
        }

        string _expiryDate;
        /// <summary>
        /// Card Expiry date
        /// </summary>
        public string ExpiryDate
        {
            get { return _expiryDate; }
        }

        /// <summary>
        /// Format For an Aurora Note
        /// </summary>
        public string FormattedExpiryDate
        {
            get
            {
                return _expiryDate.Insert(2, "/");
            }
        }


        string _approvalReference;
        /// <summary>
        /// Credit broker approval reference
        /// </summary>
        public string ApprovalReference
        {
            get { return _approvalReference; }
            set { _approvalReference = value; }
        }
        
        decimal _paymentAmount;
        /// <summary>
        /// Payment Amount 
        /// </summary>
        public decimal PaymentAmount
        {
            get { return _paymentAmount; }            
        }

        decimal _paymentSurchargeAmount;
        /// <summary>
        /// Card Surcharge Pecentage on Purchase
        /// </summary>
        public decimal PaymentSurchargeAmount
        {
            get { return _paymentSurchargeAmount; }
            set { _paymentSurchargeAmount = value; }
        }
        
        string _dpsAuthCode;
        /// <summary>
        /// DPS Generated Autgh code for the transaction
        /// </summary>
        public string DPSAuthCode
        {
            get { return _dpsAuthCode; }
            set { _dpsAuthCode = value; }
        }

        string _dpsTxnReference;
        /// <summary>
        /// DPS Transaction reference ID
        /// </summary>
        public string DPSTransactionreference
        {
            get { return _dpsTxnReference; }
            set { _dpsTxnReference = value; }
        }

        /// <summary>
        /// CVV,CVC,CID
        /// </summary>
        public string VerificationCode
        {
            get;
            set;
        }


        Customer _customer;
        /// <summary>
        /// Customers Information
        /// </summary>
        public Customer Customer
        {
            get { return _customer; }
        }
                
        public CreditCardData(Customer customer, CreditCardType cardType, string cardNumber, string cardVerification,  string cardHolderName ,string expiryDate, decimal paymentAmount)
        {
            _customer = customer;
            _cardHoldersName = cardHolderName;
            _cardType = cardType;
            _cardNumber = cardNumber;
            _expiryDate = expiryDate;
            _paymentAmount = paymentAmount;
            VerificationCode = cardVerification;//added for CVV support
        }

        // Added by Nimesh on 29th Apr 2015
        // ref : https://thlonline.atlassian.net/browse/DESK-3257 ,
        //     https://thlonline.atlassian.net/browse/DESK-3256
        public string AdditionalReference { get; set; }
        // End Added by Nimesh on 29th Apr 2015
    }
}