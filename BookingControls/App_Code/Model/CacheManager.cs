﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace THL.Booking
{       
    /// <summary>
    /// Application Level Cache Manager for Booking
    /// </summary>
    public class CacheManager
    {
        /// <summary>
        /// THL Native Brands
        /// </summary>
        THLBrand[] brands;

        /// <summary>
        /// Aggregator Sites(Used for MAC,AirNZ)
        /// </summary>
        AggregatorBrand[] aggregatorCompanies;
        public AggregatorBrand GetAggregator(string aggregatorStr)
        {
            string aggregatorName = string.Empty;
            switch(aggregatorStr)
            {
                case "z": 
                    aggregatorName = "airnz";
                    break;
                case "c":
                    aggregatorName = "mac";
                    break;
            }
            foreach (AggregatorBrand agb in aggregatorCompanies)
                if (agb.AggregatorName.ToUpper().Equals(aggregatorName.ToUpper()))
                    return agb;
            return null;
        }
        
        /// <summary>
        /// Construct the Cache Manager with all brands info
        /// </summary>
        /// <param name="dataPathStr"></param>
        public CacheManager(string dataPathStr, bool useLocalResource)
        {
            //load b,p,b,x brands into the collection, TODO: condition brandchars from web.config here.
            char[] brandsChars = new char[9] {'b','p','m','x','y', 'q', 'u', 'a', 'e'};
            List<THLBrand> brandsList = new List<THLBrand>();           
            foreach (char brandChar in brandsChars)
            {
                string brandInfoPath = (useLocalResource ? dataPathStr + "\\" + brandChar + ".xml"  :  dataPathStr + "/" + brandChar + ".xml");
                brandsList.Add(new THLBrand(brandInfoPath, THLBrand.GetBrandForString(brandChar.ToString()), useLocalResource));                
            }
            brands = brandsList.ToArray();

            //load external companies(using mac xml), TODO: fork according to web.config here and add airnz
            List<AggregatorBrand> aggregatorList = new List<AggregatorBrand>(); 
            
            string macInfoPath = (useLocalResource ? dataPathStr + "\\" + "c" + ".xml" : dataPathStr + "/" + "mac" + ".xml");
            AggregatorBrand mac = new AggregatorBrand("MAC");
            mac.LoadFromXML(macInfoPath, useLocalResource);//load locally.
            mac.Brand = THLBrands.MAC;
            aggregatorList.Add(mac);            
        
            //-- add air nz here..
            string airnzInfoPath = (useLocalResource ? dataPathStr + "\\" + "z" + ".xml" : dataPathStr + "/" + "z" + ".xml");
            AggregatorBrand airnz = new AggregatorBrand("airnz");
            airnz.LoadFromXML(airnzInfoPath, useLocalResource);//load locally.
            airnz.Brand = THLBrands.AIRNZ;
            aggregatorList.Add(airnz);
            //---end add air nz here..

            aggregatorCompanies = aggregatorList.ToArray();
        } 
        
        public THLBrand GetBrand(THLBrands _brand) {
            foreach (THLBrand brand in brands) {
                if (brand.BrandType == _brand)
                    return brand;
            }
            return null;        
        }
        
        public Branch[] GetBranchesForBrand(THLBrands _brand) {
            if (_brand == THLBrands.AIRNZ || _brand == THLBrands.MAC)
            {
                return getAggregatorForBrandType(_brand).Branches;//return MAC for now TODO fork by name
            }
            else
            {
                foreach (THLBrand brand in brands)
                {
                    if (brand.BrandType == _brand)
                        return brand.Branches;
                }
            }
            return null;
        }
        
        public BranchCountryData GetCountryData(string countryCode, string brandCode) { 
            //TODO: add brands as they come BP for now
            return brands[0].GetCountryData(countryCode);        
        }

        public BranchCountryData GetCountryData(string countryCode, THLBrands _brand)
        {
            if (_brand == THLBrands.AIRNZ || _brand == THLBrands.MAC)            {
                
                return getAggregatorForBrandType(_brand).GetCountryData(countryCode);//return MAC for now
            }
            else
            {
                foreach (THLBrand brand in brands)
                {
                    if (brand.BrandType == _brand)
                        return brand.GetCountryData(countryCode);
                }
            }
            return null;
        }

        /// <summary>
        /// Get a cached aggregator instance for a supplied THLBrand
        /// </summary>
        /// <param name="brand"></param>
        /// <returns></returns>
        AggregatorBrand getAggregatorForBrandType(THLBrands brand)
        {
            foreach (AggregatorBrand aggr in aggregatorCompanies)
                if (aggr.Brand == brand)
                    return aggr;
            return null;
        }

    }
}