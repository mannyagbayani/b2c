﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;

namespace THL.Booking
{
    public enum CustomerTitle
    {
        Mr = 0,
        Mrs = 1,
        Ms = 2,
        Miss = 3,
        Master = 4,
        Dr = 5,
        None = 6
    }
    
    public enum ContactMethod
    { 
        None = 0,
        Email = 1,
        Phone = 2,
        Post = 3
    }

    /// <summary>
    /// Customer Information 
    /// </summary>
    public class Customer
    {

        
        
        /// <summary>
        /// Convert to CustomerTitle Type from String 
        /// </summary>
        /// <param name="titleString"></param>
        /// <returns>CustomerTitle</returns>
        public static CustomerTitle GetTitleTypeForString(string titleString)
        {
            CustomerTitle titleType = CustomerTitle.None;
            switch (titleString)
            { 
                case("Mr") :
                    titleType = CustomerTitle.Mr;
                    break;               
                case("Mrs") :
                    titleType = CustomerTitle.Mrs;
                    break;                
                case("Ms") :
                    titleType = CustomerTitle.Ms;
                    break;           
                case("Miss") :
                    titleType = CustomerTitle.Miss;
                    break;                
                case("Master") :
                    titleType = CustomerTitle.Master;
                    break;                
                case("Dr") :
                    titleType = CustomerTitle.Dr;
                    break;
            }
            return titleType;
        }

        /// <summary>
        /// Map User preferred Selected Contact Method string to Enum
        /// </summary>
        /// <param name="contactMethodString"></param>
        /// <returns></returns>
        public static ContactMethod GetContactMethodTypeForString(string contactMethodString)
        {
            ContactMethod selectedMethod = ContactMethod.None;
            switch (contactMethodString.ToUpper())
            { 
                case "PHONE":
                    selectedMethod = ContactMethod.Phone;
                    break;
                case "EMAIL":
                    selectedMethod = ContactMethod.Email;
                    break;
                case "POST":
                    selectedMethod = ContactMethod.Post;
                    break;           
            }
            return selectedMethod;
        }

        ContactMethod _contactMethod;

        /// <summary>
        /// The Preferred Communication method selected by the user
        /// </summary>
        public ContactMethod CommunicationMethod {
            get { return _contactMethod; }
            set { _contactMethod = value; }            
        }
        
        private string _agentMembershipNumber;//Added fro NRMA loyalty support
        /// <summary>
        /// Agent Loyalty/Membership ID
        /// </summary>
        public string AgentMembershipNumber
        {
            get { return _agentMembershipNumber;  }
            set { _agentMembershipNumber = value; }        
        }
        
        
        public string FullName
        {
            get { return (_title + " " + _firstName + " " + _lastName); }
        }
        
        CustomerTitle _title;
        public CustomerTitle Title 
        {
            get { return _title;  }
        }

        string _firstName;
        public string FirstName
        {
            get { return ( string.IsNullOrEmpty(_firstName) ? "n/a" : _firstName); }//n/a has been added for direct agents 31/12/11
        }

        string _lastName;
        public string LastName
        {
            get { return _lastName; }
        }

        string _email;
        public string EMail 
        {
            get { return _email; }
        }

        string _countryOfresidenceCode;
        public string CountryOfresidenceCode
        {
            get { return _countryOfresidenceCode; }
            set { _countryOfresidenceCode = value; }        
        }
        
        string _phoneNumber;
        public string PhoneNumber 
        {//consider: string formatedPhone = (_phoneNumber.StartsWith("+") ? string.Empty : "+") + Regex.Replace(_phoneNumber, @"\D", string.Empty);
            get { return _phoneNumber; }
        }

        /// <summary>
        /// Possible User Query Note
        /// </summary>
        string _note;
        public string Note 
        {
            get { return _note; }
            set { _note = value;  }
        }
        
        public Customer(CustomerTitle title, string firstName, string lastName, string email, string phoneNumber)
        {
            //TODO: add validation if needed
            _title = title;
            _firstName = firstName;
            _lastName = lastName;
            _email = email;
            _phoneNumber = phoneNumber;
        }
        
        /// <summary>
        /// Convert Customer data to XML
        /// </summary>
        /// <returns></returns>
        public string ToXML()
        {
            string formatedPhone = (_phoneNumber.StartsWith("+") ? string.Empty : "+") + Regex.Replace(_phoneNumber, @"\D", string.Empty);
            StringBuilder sb = new StringBuilder();
            sb.Append("<CustomerData>");
            sb.Append("<Title>"+ _title +"</Title>");
            sb.Append("<FirstName>"+ _firstName +"</FirstName>");
            sb.Append("<LastName>"+ _lastName +"</LastName>");
            sb.Append("<Email>"+ _email +"</Email>");
            sb.Append("<PhoneNumber>"+ formatedPhone +"</PhoneNumber>");
            sb.Append("<CountryOfResidence>"+ _countryOfresidenceCode +"</CountryOfResidence>");
            sb.Append("<PreferredModeOfCommunication>" + _contactMethod.ToString().ToUpper() + "</PreferredModeOfCommunication>");            
            sb.Append("</CustomerData>");
            return sb.ToString();
        }
    }
}