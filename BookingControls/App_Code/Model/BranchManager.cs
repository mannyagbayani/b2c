﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text;


public enum BranchType {
    PickUp = 0,
    DropOff = 1
}

public enum PassengerType { 
    Adult = 0,
    Child = 1
}

namespace THL.Booking
{
    /// <summary>
    /// Manage banches query functionality
    /// </summary>
    public class BranchManager
    {
        
        private Branch[] _branches;

        public BranchManager(Branch[] branches)
        {
            _branches = branches;
        }
               
               
    }
}