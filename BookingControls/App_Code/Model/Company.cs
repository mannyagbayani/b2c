﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace THL.Booking
{
    /// <summary>
    /// Company Functionality Structure
    /// </summary>
    public class Company
    {

        string companyName;
        public string CompanyName {
            get { return companyName;  }             
        }
        
        string companyCode;
        public string CompanyCode {
            get { return companyCode; }
        }

        string countryCode;
        public string CountryCode
        {
            get { return countryCode; }
        }

        VehicleType companyType;
        public VehicleType CompanyType
        {
            get { return companyType; }
        }
        
        public Company(string _companyName, string _companyCode, string _countryCode, string _companyType)
        {
            companyName = _companyName;
            companyCode = _companyCode;
            countryCode = _countryCode;
            VehicleType cType = VehicleType.Both;//defa
            switch(_companyType.ToUpper()) 
            {
                case "AV":
                    cType = VehicleType.Campervan;
                    break;
                case "AC":
                    cType = VehicleType.Car;
                    break;
                case "BOTH":
                    cType = VehicleType.Both;
                    break;
            }
            companyType = cType;            
        }        
    }
}