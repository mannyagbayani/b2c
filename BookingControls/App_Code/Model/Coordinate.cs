﻿using System;

/// <summary>
/// Defines a Geo Coordinate , Later point for projection calculation
/// </summary>
public class Coordinate
{
    string name;
    public string Name { get { return name; } set { name = value; } }

    int id;
    public int ID { get { return id; } set { id = value; } }

    decimal lon;
    public decimal Lon { get { return lon; } }
    
    decimal lat;
    public decimal Lat { get { return lat; } }
    
    public Coordinate(decimal _lon, decimal _lat )
	{
        lon = _lon;
        lat = _lat;
	}

    public Coordinate() { 
    
    }
}