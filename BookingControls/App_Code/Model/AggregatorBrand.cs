﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace THL.Booking
{
    public enum Aggregators 
    { 
        MotorHomesAndCars = 0,
        AirNZ = 1
    }
    
    /// <summary>
    /// AggregatorBrand Object used for AirNZ, MAC 
    /// </summary>
    public class AggregatorBrand
    {

        Company[] companies;

        Branch[] branches;
        public Branch[] Branches
        {
            get { return branches;  }
        }

        THLBrands _aggregatorBrand;
        public THLBrands Brand
        {
            get { return _aggregatorBrand;  }
            set { _aggregatorBrand = value; }
        }            

        /// <summary>
        /// Return Aggregators companies for a given country
        /// </summary>
        /// <param name="countryCode"></param>
        /// <param name="companyType"></param>
        /// <returns></returns>
        public Company[] GetCompaniesForCountry(string countryCode, VehicleType companyType)
        { 
            List<Company> cmpList = new List<Company>();
            foreach (Company currentCmp in companies)
            { 
                if (currentCmp.CountryCode.Equals(countryCode) && (currentCmp.CompanyType == companyType || currentCmp.CompanyType == VehicleType.Both))//considet enum
                    cmpList.Add(currentCmp);
            }
            return cmpList.ToArray();
        }
        

        BranchCountryData[] _countriesData = null;
        public BranchCountryData GetCountryData(string countryCode)
        {
            foreach (BranchCountryData country in _countriesData)
                if (country.CountryCode == countryCode)
                    return country;
            return null;
        }

        private Aggregators aggregatorType;

        string aggregatorName;
        public string AggregatorName
        {
            get { return aggregatorName; }
        }
        
        public AggregatorBrand(string _aggregatorName)
        {
            aggregatorName = _aggregatorName;            
        }

        public bool LoadFromXML(string brandXmlPath, bool useLocalResource)
        {
            //TODO: exp handle and return false on failure            
            XmlDocument xml = new XmlDocument();            
            if (useLocalResource)
                xml.Load(brandXmlPath); //fs      
            else
                xml.LoadXml(new DataAccess().GetStringForURL(brandXmlPath));//http
            XmlNodeList countries = xml.SelectNodes("countries/country");
            List<Company> companiesList = new List<Company>();
            List<Branch> branchesList = new List<Branch>();
             
            string currentCountryCode = string.Empty;
            foreach (XmlNode country in countries)//itterate thorough countries
            {
                currentCountryCode = country.Attributes["code"].Value;
                XmlNode openingNode = country.SelectSingleNode("openHours");
                BranchCountryData countryData = new BranchCountryData();
                if (countryData.SetBranchTimes(openingNode))
                {
                    countryData.CountryCode = currentCountryCode;
                    countryData.SetPassengerInfo(country.SelectSingleNode("passengerInfo"));
                    //addCountryData(countryData);
                } //else failed to init the country data for node xml node currentCountryCode, get countries max passenger info
                //load companies
                XmlNodeList companiesXml = country.SelectNodes("companies/company");
                string comName, comCode, comType;
                foreach (XmlNode companyXml in companiesXml)
                {
                    comName = companyXml.Attributes["name"].Value;
                    comCode = companyXml.Attributes["id"].Value;
                    comType = companyXml.Attributes["type"].Value;
                    companiesList.Add(new Company(comName, comCode, currentCountryCode, comType));                    
                }
                //load branches
                XmlNodeList branchesXml = country.SelectNodes("branches/branch");
                string branchName, branchCode;
                foreach (XmlNode branchXml in branchesXml)
                {
                    Branch curBranch = new Branch();
                    branchName = branchXml.Attributes["name"].Value;
                    branchCode = branchXml.Attributes["value"].Value;//TODO: consider adding type
                    curBranch.Name = branchName;
                    curBranch.BranchCode = branchCode;
                    curBranch.CountryCode = currentCountryCode;
                    
                    VehicleType vt;
                    switch (branchXml.Attributes["type"].Value)
                    {
                        case "av":
                            vt = VehicleType.Campervan;
                            break;
                        case "ac":
                            vt = VehicleType.Car;
                            break;
                        case "all":
                            vt = VehicleType.Both;
                            break;
                        default:
                            vt = VehicleType.Both;
                            //TODO: log no service type branch and handle here
                            break;
                    }
                    curBranch.VehicleTypeAvailability = vt;
                    countryData.AddAvailability(vt);                    
                    branchesList.Add(curBranch);
                }
                addCountryData(countryData);
            }
            companies = companiesList.ToArray();
            branches = branchesList.ToArray();
            return true;
        }
                
        void addCountryData(BranchCountryData countryData)
        {
            if (_countriesData == null)
                _countriesData = new BranchCountryData[] { countryData };
            else
            {
                List<BranchCountryData> list = new List<BranchCountryData>(_countriesData);
                list.Add(countryData);
                _countriesData = list.ToArray();
            }
        }
    }
}