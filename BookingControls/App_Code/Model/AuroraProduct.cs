﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace THL.Booking
{
    public enum ProductType {
        Insurance = 1,
        ExtraHireItem = 2,
        FerryCrossing = 3,
        All = 4
    }

    public enum UOMType {
        None=0,
        Day =1,
        H24 = 2
    }
        
    /// <summary>
    /// Non Vehicle Aurora Product Base Class Representation 
    /// </summary>
    public class AuroraProduct
    {

        protected int selectedAmount;
        public int SelectedAmount
        {
            get { return selectedAmount; }
            set { selectedAmount = value; }
        }
        
        protected ProductType productType;
        public ProductType Type 
        {
            get { return productType; }
            set { productType = value; }
        }

        protected string code;
        public string Code
        {
            get { return code; }
        }

        protected string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        protected decimal dailyRate;
        public decimal DailyRate
        {
            get { return dailyRate; }
        }

        protected UOMType uom;
        public UOMType UOM
        {
            get { return uom; }
        }

        protected decimal grossAmount;
        public decimal GrossAmount
        {
            get { return grossAmount; }
            set { grossAmount = value; }
        }

        protected decimal maxCharge;
        public decimal MaxCharge
        {
            get { return maxCharge; }
        }

        protected decimal hirePeriod;
        public decimal HirePeriod
        {
            get { return hirePeriod; }
            set { hirePeriod = value; }
        }

        protected string sapId;
        public string SapId
        {
            get { return sapId; }
        }

        protected int displayOrder;
        public int DisplayOrder {
            get { return displayOrder; }
        }

        protected bool promptQuantity;
        public bool PromptQuantity 
        {
            get { return promptQuantity;  } 
        }

        protected int minAge;
        public int MinAge {
            get { return minAge;  }
        }

        protected int maxAge;
        public int MaxAge
        {
            get { return maxAge; }
        }

        protected string productInfo;
        public string ProductInfo 
        {
            get { return productInfo; }
            set { productInfo = value; }
        }

        protected string note;
        public string Note
        {
            get { return note; }
            set { note = value; }
        }

        public string BpdId { 
            get; 
            set; 
        }


        public AuroraProduct(string _code, string _name, decimal _dailyRate, UOMType _uom, decimal _grossAmt, decimal _maxCharge, string _sapId, int _displayOrder, bool _promptForQty, int _minAge, int _maxAge)
        {
            productType = ProductType.ExtraHireItem;
            code = _code;
            name = _name;
            dailyRate = _dailyRate;
            uom = _uom;
            grossAmount = _grossAmt;
            maxCharge = _maxCharge;
            sapId = _sapId;
            displayOrder = _displayOrder;
            promptQuantity = _promptForQty;
            minAge = _minAge;
            maxAge = _maxAge;
        }

        public AuroraProduct()
        { 
        }

        /// <summary>
        /// Serialize Aurora Product Data into JSON/String format
        /// </summary>
        /// <returns>JSON String</returns>
        public override string ToString()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("{");
                sb.Append("productType:"+ productType);
                sb.Append(",code:"+ code);
                sb.Append(",name:"+ name);
                sb.Append(",dailyRate:"+ dailyRate); 
                sb.Append(",uom:"+ uom);
                sb.Append(",grossAmount:"+ grossAmount) ;
                sb.Append(",maxCharge:"+ maxCharge);
                sb.Append(",sapId:"+sapId);
                sb.Append(",displayOrder:"+displayOrder);
                sb.Append(",promptQuantity:"+promptQuantity);
                sb.Append(",minAge:"+minAge);
                sb.Append(",maxAge:" + maxAge);
                sb.Append(",note:'" + note + "'");
                sb.Append("}");
                return sb.ToString();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                //TODO: log failed serialization event.
                return "failed to serialize product object";
            }
        }



    }
}