﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using System.Threading;
using System.Runtime.Remoting.Messaging;
using THL.Booking;
using System.Collections.Specialized;

/// <summary>
/// Summary description for LoadProgress
/// </summary>
[WebService(Namespace = "http://bookings.thlonline.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class LoadProgress : System.Web.Services.WebService {

    delegate void ProcessTask(string sid, string taskType, int timeoutInSeconds, string param);

    public LoadProgress () {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }


    /// <summary>
    /// Enumeration of Load Status Type
    /// </summary>
    public enum LoadStatusType
    {
        //initialize state
        Init,
        //thread in progress
        Pending,
        //thread timed out
        Timedout,
        //thread finished
        Completed,
        //thread failed
        Failed
    }


    /// <summary>
    /// web method for asynchronously calling a heavy slow function
    /// by forking a thread on the server using delegation
    /// it can be used for webserivce call, database access, remote functions, etc
    /// </summary>
    /// <param name="loadType"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public string LoadSlowFunction(string sid, string taskType, int timeoutInSeconds, string param)
    {
        LoadStatusType currentLoadStatus = GetloadStatus(sid);
        if (SessionManager.GetSessionData() != null && SessionManager.GetSessionData().HasRequestData)
        {
            switch (currentLoadStatus)
            {
                case LoadStatusType.Init:
                    SetRequestData(sid, SessionManager.GetSessionData().AvailabilityRequestData);
                    ClearSessionData(sid, taskType);
                    ProcessTask wsCall = new ProcessTask(TaskOnStart);
                    //Fork a thread to run the webservice /slow method call asynchrously on server
                    IAsyncResult ar = wsCall.BeginInvoke(sid, taskType, timeoutInSeconds, param, new AsyncCallback(TaskOnCompletion), wsCall);
                    SetLoadStatus(sid, LoadStatusType.Pending);
                    break;
                case LoadStatusType.Pending:
                    currentLoadStatus = LoadStatusType.Pending;
                    break;
                case LoadStatusType.Timedout:
                    currentLoadStatus = LoadStatusType.Timedout;
                    ClearCache(sid);
                    break;
                case LoadStatusType.Completed:
                    MoveDataToSession(sid, taskType);
                    ClearCache(sid);
                    break;
                case LoadStatusType.Failed:
                    ClearCache(sid);
                    break;
                default:
                    SetLoadStatus(sid, LoadStatusType.Failed);
                    break;
            }
        }
        else
        {
            currentLoadStatus = LoadStatusType.Timedout;
        }
        return GetStatusInString(currentLoadStatus);
    }

    public void ClearCache(string sid)
    {
        HttpRuntime.Cache.Remove(sid + "-LoadStatus");
        HttpRuntime.Cache.Remove(sid + "-RequestData");
        HttpRuntime.Cache.Remove(sid + "-ResponseData");
    }

    public void SetCacheObject(string sid, string cacheType, object value)
    {
        HttpRuntime.Cache[sid + "-" + cacheType] = value;
    }

    public object GetCacheObject(string sid, string cacheType)
    {
        return HttpRuntime.Cache[sid + "-" + cacheType];
    }

    public void SetLoadStatus(string sid, LoadStatusType loadStatus)
    {
        SetCacheObject(sid, "LoadStatus", loadStatus);
    }

    public LoadStatusType GetloadStatus(string sid)
    {
        if (GetCacheObject(sid, "LoadStatus") == null)
            return LoadStatusType.Init;
        return (LoadStatusType)GetCacheObject(sid, "LoadStatus");
    }

    public void SetRequestData(string sid, AvailabilityRequest requestData)
    {
        SetCacheObject(sid, "RequestData", requestData);
    }

    public AvailabilityRequest GetRequestData(string sid)
    {
        if (GetCacheObject(sid, "RequestData") == null)
            return null;
        return (AvailabilityRequest)GetCacheObject(sid, "RequestData");
    }
    public void SetResponseData(string sid, object responseData)
    {
        SetCacheObject(sid, "ResponseData", responseData);
    }
    public void MoveDataToSession(string sid, string taskType)
    {
        Session[taskType+sid] = GetCacheObject(sid, "ResponseData");
    }
    public void ClearSessionData(string sid, string taskType)
    {
        Session[taskType+sid] = null;
    }
    /// <summary>
    /// Method to convert enumeration type into string format for 
    /// ease of front end handling
    /// </summary>
    /// <param name="status"></param>
    /// <returns></returns>
    public string GetStatusInString(LoadStatusType status)
    {
        switch (status)
        {
            case LoadStatusType.Init:
                return "initialised";
            case LoadStatusType.Pending:
                return "pending";
            case LoadStatusType.Completed:
                return "success";
            case LoadStatusType.Timedout:
                return "timedout";
            case LoadStatusType.Failed:
                return "failed";
            default:
                return "no type maching";
        }
    }

    public NameValueCollection ConvertStringToCollection(string param)
    {
        NameValueCollection result = new NameValueCollection();
        string[] parameters = param.Split('~');
        foreach (string s in parameters)
        {
            try
            {
                result.Add(s.Split('|')[0], s.Split('|')[1]);
            }
            catch { }
        }
        return result;
    }

    public void TaskOnStart(string sid, string taskType, int timeout, string param)
    {
        Thread threadToKill = null;
        //fork another thread to monitor the timeout
        Action a = () =>
        {
            threadToKill = Thread.CurrentThread;
            NameValueCollection parameters = ConvertStringToCollection(param);
            AvailabilityRequest aRequest = GetRequestData(sid);
            try
            {
                if (taskType == "GetAlternateOptions")
                {
                    //int i = 0, j = 0;
                    //int b = i / j;
                    aRequest.VehicleModel = parameters["vCode"];
                    aRequest.Brand = parameters["Brand"].ToUpper();
                    AlternativeAvailabilityItem[] alternativeItems = (new DataAccess().GetAlternateAvailability(aRequest));
                    SetResponseData(sid, alternativeItems);
                    SetLoadStatus(sid, LoadStatusType.Completed);
                }
            }
            catch(Exception ex)
            {
                THLDebug.LogError(ErrorTypes.ExternalProvider, "PreLoader", ex.Message,"");
                SetLoadStatus(sid, LoadStatusType.Failed);
            }
        };
        IAsyncResult r = a.BeginInvoke(null, null);
        if (r.AsyncWaitHandle.WaitOne(timeout * 1000, true))
        {
            a.EndInvoke(r);
        }
        else
        {
            SetLoadStatus(sid, LoadStatusType.Timedout);
        }
    }

    public void TaskOnCompletion(IAsyncResult ar)
    {
        string result = ar.AsyncState.ToString();
    }
}

