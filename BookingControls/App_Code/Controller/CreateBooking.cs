﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;


namespace THL.Booking
{
    /// <summary>
    /// Generate Booking Service against the Stored Session Booking and Supplied User and Payment Information  
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    
     
    [System.Web.Script.Services.ScriptService]
    public class CreateBooking : System.Web.Services.WebService
    {

        public CreateBooking()             
        {
            
                
        }

        [WebMethod(Description = "Make A Booking Request with supplied form data", EnableSession = true)]
        public string BookingRequest(string customerTitleDD,
                                     string customerFirstName,
                                     string customerLastName,
                                     string emailAddressTxt,
                                     string telephoneTxt1,
                                     string telephoneTxt2,
                                     string telephoneTxt3,
                                     /*string countryOfResidenceDD, used from session */
                                     string otherRequests,
                                     string contactMethod,
                                     string vehicleCode
        )
        {
            //{meta:1,customerTitleDD:'-1',customerFirstName:'csdfsdf',customerLastName:'',emailAddressTxt:'asdas@adas.net',confirmEmailAddressTxt:'asdas@adas.net',telephoneTxt1:'21',telephoneTxt2:'212',telephoneTxt3:'123435',countryOfResidenceDD:'AU',otherRequests:''}
            Customer customerData = new Customer(Customer.GetTitleTypeForString(customerTitleDD), customerFirstName, customerLastName, emailAddressTxt, telephoneTxt1 + telephoneTxt2 + telephoneTxt3);

            SessionData sessionData = SessionManager.GetSessionData();

            customerData.CommunicationMethod = Customer.GetContactMethodTypeForString(contactMethod);
            customerData.CountryOfresidenceCode = sessionData.AvailabilityRequestData.CountryOfResidence.ToUpper();

            

            ApplicationData appData = ApplicationManager.GetApplicationData();                        
            if (sessionData.HasRequestData && sessionData.HasResponseData)
            {
                PaymentManager pm = new PaymentManager();
                AvailabilityRequest aReq = sessionData.AvailabilityRequestData;
                                
                AvailabilityItem aResItem = sessionData.AvailabilityResponseData.GetAvailabilityItemForVehicleCode(vehicleCode);
                ///TODO: get the request 
                customerData.Note = otherRequests + "\n(" + aResItem.ErrorMessage + "," + aResItem.BlockingRuleMessage + ")";
                
                string generatedAuroraBookingId = pm.CreateBookingWithoutPayment(customerData, aReq, aResItem);
                return "bookingId:" + generatedAuroraBookingId;
            }
            else {
                return "error:session expired";            
            }            
        }
          
        [WebMethod(Description="Make A Payment against a stored booking", EnableSession=true)]
        public string MakePayment(string cardTypeDD,
            string cardNumberTxt,
            string cardExpiryMonthTxt,
            string cardVerify,
            string cardExpiryYearTxt,
            string cardHolderName,
            string customerTitleDD,
            string customerFirstName,
            string customerLastName,
            string emailAddressTxt,
            string telephoneTxt1,
            string telephoneTxt2,
            string telephoneTxt3,
            string customerAgeDD,
            string customerNoteTxt,
            string totalDepositValue,
            string payTotalCB,
            string loyaltyCartTxt,
            string selectedSlot, 
            string slotId
            )
        {
            string responseText = string.Empty;

            try
            {
                SessionData sessionData = SessionManager.GetSessionData();
                AuroraBooking bookingData = sessionData.BookingData;
                if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.MTierStatus, "{BookingStatus:" + SessionManager.BookingStatus + "}");//Debug Line

                if (SessionManager.BookingStatus == BookingStatus.PaymentSuccessful || SessionManager.BookingStatus == BookingStatus.BookingSuccessful || SessionManager.BookingStatus == BookingStatus.Proccessing)
                {
                    return "error:" + getErrorMessageForBookingStatus(SessionManager.BookingStatus);//TODO: consider forking on both options.
                }
                else
                    SessionManager.BookingStatus = BookingStatus.Proccessing;

                decimal paymentAmount = 0.0m;
                bool hasAmount = decimal.TryParse(totalDepositValue, out paymentAmount);

                if (bookingData != null && hasAmount)
                {
                    // Added by Nimesh on 21st Jan for selected slot
                    bookingData.SelectedSlot = selectedSlot;
                    bookingData.SlotId = slotId;
                    // End Added by Nimesh on 21st Jan for selected slot

                    paymentAmount = System.Math.Round(paymentAmount, 2);

                    string phoneStr = telephoneTxt1 + telephoneTxt2 + telephoneTxt3;
                    bool phoneNumberValid = PaymentHelper.ValidatePhoneNumber(ref phoneStr);

                    bookingData.BookingNote = customerNoteTxt;
                    bookingData.PayedFull = (payTotalCB == "true");

                    string cardTypeStr = cardTypeDD.Split(':')[0];
                    string cardSurchargeStr = cardTypeDD.Split(':')[1];

                    string expiryDateStr = cardExpiryMonthTxt + cardExpiryYearTxt;

                    Customer customerData = new Customer(Customer.GetTitleTypeForString(customerTitleDD), customerFirstName, customerLastName, emailAddressTxt, phoneStr);
                    customerData.CountryOfresidenceCode = sessionData.AvailabilityRequestData.CountryOfResidence.ToUpper();
                    
                    customerData.AgentMembershipNumber = (loyaltyCartTxt != "0" ? loyaltyCartTxt : string.Empty);

                   
                    CreditCardData ccData = new CreditCardData(customerData, CreditCardData.GetCardTypeForString(cardTypeStr), cardNumberTxt, cardVerify, cardHolderName, expiryDateStr, paymentAmount);
                    decimal surchargeAmount = 0m;
                    decimal.TryParse(cardSurchargeStr, out surchargeAmount);
                    ccData.PaymentSurchargeAmount = surchargeAmount;
                    ccData.CardType = CreditCardData.GetCardTypeForString(cardTypeStr);

                    if (ccData.IsValid)//TODO: fill IsValid
                    {
                        bookingData.AddPaymentInfo(ccData);
                        decimal totalPrice = bookingData.GetBookingPriceTotal();
                        PaymentManager pm = new PaymentManager();
                        //string maskCCNumber = "xxxx-xxxx-xxxx-" + _paymentInfo.CardNumber.ToString().Substring(_paymentInfo.CardNumber.ToString().Length - 4, 4);//mask:

                        Transaction paymentTransaction = pm.CreateBookingWithPayment(ref bookingData, PaymentProvider.DPS, "paymentCodeHere", paymentAmount);

                        if (paymentTransaction == null)
                        {
                            string dpsResponse = "error:" + pm.DPSErrorMessages;
                            return dpsResponse;
                        }
                        else
                        {
                            return "bookingId:" + paymentTransaction.AuroraBookingId;
                        }
                    }
                    else
                    {
                        responseText = ccData.ValidationErrorMessage;
                        SessionManager.BookingStatus = BookingStatus.Created;
                        return "error:failed Validation on " + ccData.ValidationErrorMessage;
                    }
                }
                else
                {
                    return "error:Session Expired";//TODO: informa protocol, no booking for session inform presentation tier.
                }
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Application, "CreateBooking.MakePayment", "{error:" + ex.Message + "}", "paymentFailed");
                return "error:appError";
            }
        }


        //Submit a Quote to Aurora
        [WebMethod(Description = "Make A Quote against a stored booking request", EnableSession = true)]
        public string MakeQuote(
            string customerTitleDD,
            string customerFirstName,
            string customerLastName,
            string emailAddressTxt,
            string telephoneTxt1,
            string telephoneTxt2,
            string telephoneTxt3,
            string customerAgeDD,
            string customerNoteTxt           
            )
        {
            string responseText = string.Empty;

            try
            {
                SessionData sessionData = SessionManager.GetSessionData();
                AuroraBooking bookingData = sessionData.BookingData;
                
                
                
                if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.MTierStatus, "{BookingStatus:" + SessionManager.BookingStatus + "}");//Debug Line

                if (SessionManager.BookingStatus == BookingStatus.PaymentSuccessful || SessionManager.BookingStatus == BookingStatus.BookingSuccessful || SessionManager.BookingStatus == BookingStatus.Proccessing)
                {
                    return "error:" + getErrorMessageForBookingStatus(SessionManager.BookingStatus);//TODO: consider forking on both options.
                }
                else
                    SessionManager.BookingStatus = BookingStatus.Proccessing;

                decimal paymentAmount = 0.0m;
                paymentAmount = bookingData.GetBookingPriceTotal();

                bool hasAmount = true;//a quote allways have a value, possible refactoring point

                if (bookingData != null && hasAmount)
                {
                    paymentAmount = System.Math.Round(paymentAmount, 2);
                    string phoneStr = telephoneTxt1 + telephoneTxt2 + telephoneTxt3;
                    bool phoneNumberValid = PaymentHelper.ValidatePhoneNumber(ref phoneStr);
                    bookingData.BookingNote = customerNoteTxt;
                    bookingData.PayedFull = false;
                                        
                    Customer customerData = new Customer(Customer.GetTitleTypeForString(customerTitleDD), customerFirstName, customerLastName, emailAddressTxt, phoneStr);
                    customerData.CountryOfresidenceCode = sessionData.AvailabilityRequestData.CountryOfResidence.ToUpper();

                    CreditCardData ccData = new CreditCardData(customerData, CreditCardType.NONE, null, null, null, null, 0m);
                    bookingData.AddPaymentInfo(ccData);

                    PaymentManager pm = new PaymentManager();
                    string quoteId =  pm.CreateQuote(ref bookingData, 0);
                    return quoteId;
                }
                else
                {
                    return "error:Session Expired";//TODO: inform a protocol, no booking for session inform presentation tier.
                } 
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Application, "CreateBooking.MakePayment", "{error:" + ex.Message + "}", "paymentFailed");
                return "error:failed to create quote, please try again";
            }
        } 


        [WebMethod(Description = "Submit an Existing Quote as Payment", EnableSession = true)]
        public string ConfirmAQuote(string cardTypeDD,
            string cardNumberTxt,
            string cardVerify,
            string cardExpiryMonthTxt,
            string cardExpiryYearTxt,
            string cardHolderName,
            string customerTitleDD,
            string customerFirstName,
            string customerLastName,
            string emailAddressTxt,
            /*
            string telephoneTxt1,
            string telephoneTxt2,
            string telephoneTxt3,
            */
            string customerAgeDD,
            string customerNoteTxt,
            /*string totalDepositValue,*/
            string payTotalCB,
            /*,string loyaltyCartTxt*/
            string totalDepositValue,
            string selectedSlot,
            string slotId
            )
        {
            string responseText = string.Empty;

            try
            {
                string loyaltyCartTxt = "0";//stub
                
                AuroraQuote quote = SessionManager.CurrentQuote;
                if (quote != null)
                {                   
                    AuroraBooking bookingData = quote.BookingData;

                    Customer customerDataUpdate = new Customer(Customer.GetTitleTypeForString(customerTitleDD), customerFirstName, customerLastName, emailAddressTxt,string.Empty);
                    bookingData.CustomerInfo = customerDataUpdate;
                    //bookingData.CustomerInfo = customerData;
                    
                    if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.MTierStatus, "{BookingStatus:" + SessionManager.BookingStatus + "}");//Debug Line                    
                    if (SessionManager.BookingStatus == BookingStatus.PaymentSuccessful || SessionManager.BookingStatus == BookingStatus.Proccessing)
                    {
                        return "error:" + getErrorMessageForBookingStatus(SessionManager.BookingStatus);//TODO: consider forking on both options.
                    }
                    else 
                        SessionManager.BookingStatus = BookingStatus.Proccessing;
                    
                    decimal submitedAmount = 0.0m;
                    bool hasAmount =  decimal.TryParse(totalDepositValue, out submitedAmount);
                    decimal paymentAmount = submitedAmount > quote.GetRequiredDeposit() ? submitedAmount : quote.GetRequiredDeposit();//Greater between required deposit and submitted full amount 
                    
                    if (bookingData != null && hasAmount)
                    {
                        // Added by Nimesh on 21st Jan for selected slot
                        bookingData.SelectedSlot = selectedSlot;
                        bookingData.SlotId = slotId;
                        // End Added by Nimesh on 21st Jan for selected slot
                        decimal surchargeAmount = 0m;

                        string cardSurchargeStr = cardTypeDD.Split(':')[1];
                        decimal.TryParse(cardSurchargeStr, out surchargeAmount);
                        
                        paymentAmount = System.Math.Round(paymentAmount, 2);
                        
                        string phoneStr = string.Empty;// telephoneTxt1 + telephoneTxt2 + telephoneTxt3;
                        bool phoneNumberValid = PaymentHelper.ValidatePhoneNumber(ref phoneStr);

                        bookingData.BookingNote = customerNoteTxt;
                        bookingData.PayedFull = (payTotalCB == "true");

                        string cardTypeStr = cardTypeDD.Split(':')[0];
                        
                        string expiryDateStr = cardExpiryMonthTxt + cardExpiryYearTxt;

                        Customer customerData = bookingData.CustomerInfo;
                        
                        CreditCardData ccData = new CreditCardData(customerData, CreditCardData.GetCardTypeForString(cardTypeStr), cardNumberTxt, cardVerify, cardHolderName, expiryDateStr, paymentAmount);
                        
                        ccData.PaymentSurchargeAmount = surchargeAmount;
                        ccData.CardType = CreditCardData.GetCardTypeForString(cardTypeStr);
                        
                        if (ccData.IsValid)//TODO: fill IsValid
                        {                           
                                                      
                            bookingData.AddPaymentInfo(ccData);
                            decimal totalPrice = quote.GetRequiredDeposit(); //bookingData.GetBookingPriceTotal();
                            PaymentManager pm = new PaymentManager();

                            quote.FerryNote = customerNoteTxt + " " + quote.FerryNote;

                            Transaction paymentTransaction = pm.ConfirmQuoteWithPayment(quote, ref bookingData, PaymentProvider.DPS, "paymentCodeHere", paymentAmount);
                            
                            if (paymentTransaction == null)
                            {
                                string dpsResponse = "error:" + pm.DPSErrorMessages;
                                return dpsResponse;
                            }
                            else
                            {
                                return "bookingId:" + paymentTransaction.AuroraBookingId;
                            }
                        }
                        else
                        {
                            responseText = ccData.ValidationErrorMessage;
                            SessionManager.BookingStatus = BookingStatus.Created;
                            return "error:failed Validation on " + ccData.ValidationErrorMessage;
                        }
                    }
                    else
                        return "error:Session Expired";//failed to load session quote
                }
                else
                {
                    return "error:Session Expired";//TODO: informa protocol, no booking for session inform presentation tier.
                }
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Application, "CreateBooking.MakePayment", "{error:" + ex.Message + "}", "paymentFailed");
                return "error:appError";
            }
        }














        /// <summary>
        /// generate an booking status text according to the booking status
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        private string getErrorMessageForBookingStatus(BookingStatus status) 
        {
            string msg = string.Empty;
            switch (status)
            { 
                case BookingStatus.Proccessing:
                    msg = "Please Wait while we process your request";
                    break;
                case BookingStatus.PaymentSuccessful:
                    msg = "Payment Completed, Booking in progress";
                    break;
                case BookingStatus.BookingSuccessful:
                    msg = "This booking has been completed.";
                    break;
            }           
            return msg;
        }
    }
}