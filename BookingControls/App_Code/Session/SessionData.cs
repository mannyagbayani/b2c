﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace THL.Booking
{
    /// <summary>
    /// Represents Session information for a booking user
    /// </summary>
    public class SessionData
    {
        
        private bool hasRequestData;
        public bool HasRequestData
        {
            get { return (availabilityRequestData != null); }//consoider validation of object
        }

        private bool hasResponseData;
        public bool HasResponseData
        {
            get { return (availabilityResponseData != null); }//consoider validation of Response object
        }
        
        AvailabilityRequest availabilityRequestData;
        public AvailabilityRequest AvailabilityRequestData {
            get { return availabilityRequestData; }//consoider validation of Requset object
            set 
            {
                availabilityRequestData = value;
                hasRequestData = true;
            }
        } 

        AvailabilityResponse availabilityResponseData;
        public AvailabilityResponse AvailabilityResponseData {
            get { return availabilityResponseData; }
            set { availabilityResponseData = value; }
        }

        DateTime creationTime;
        public DateTime CreationTime {
            get { return creationTime; }
        }
        
        AuroraBooking _bookingData;
        public bool UpdateBookingData(AuroraBooking bookingData)
        {
            //TODO: merge into existig data, currently overright
            _bookingData = bookingData;            
            return true;//TODO: exp handle
        }

        public AuroraBooking BookingData {
            get { return _bookingData;  }
        }

        public bool HasBookingData { 
            get { return (_bookingData != null); /*TODO: add data vaerfication here..*/ }
        }
        
        public SessionData(DateTime createdOn)
        {
            creationTime = createdOn;
        }

        public CreditCardElement[] availableCards;
        public CreditCardElement[] AvailableCards
        {
            get { return availableCards; }
            set { availableCards = value; }
        }

        public bool HasSurecharge()
        { 
            bool hasSurcharge = false;
            foreach(CreditCardElement cardInfo in availableCards)
            {
                if (cardInfo.Surcharge > 0)
                    return true;
            }
            return hasSurcharge;
        }

        //refactoring 25/01/10
        public bool HasCountryOfResidence = false;
        string countryOfResidence = "AU";//test here string.Empty;        
        public string CountryOfResidence {
            get { return countryOfResidence; }
            set { 
                countryOfResidence = value.ToUpper();//TODO: consider ISO validation here
                HasCountryOfResidence = true;
            }
        }
        //end refactoring 25/01/10
    }
}