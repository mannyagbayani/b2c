﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.Xml;
using System.Web;

namespace THL.Booking {
    
    public enum CountryCode
    {
        NONE = 0,
        AU = 1,
        NZ = 2,
        US = 3,
        CA = 4
    }
 
    /// <summary>
    /// Availability Requests Parameters Structure
    /// </summary>
    public class AvailabilityRequest
    {

        string debugStr;
        
        /// <summary>
        /// Country of Customer origin ISO 3166 Code
         /// </summary>
        CountryCode countryCode;
        public CountryCode CountryCode 
        {
            get { return countryCode; }
            set { countryCode = value; }
        }

        /// <summary>
        /// User By Aggregator sites requests to define a company to filter by
        /// </summary>
        string companyCode;
        public string CompanyCode
        {
            get { return companyCode; }
            set { companyCode = value; }
        }

        /// <summary>
        /// Added for B2B Requests
        /// </summary>
        public bool IsGross { get; set; }

        /// <summary>
        /// Vehicle Type (Camper/Car/Both)
        /// </summary>
        VehicleType vehicleType;
        public VehicleType VehicleType {
            get { return vehicleType; }
            set { vehicleType = value; }
        }

        string vehicleModel;
        public string VehicleModel    
        {
            get { return vehicleModel; }
            set { vehicleModel = value; }        
        }

        DateTime pickUpDate;
        public DateTime PickUpDate 
        {
            get { return pickUpDate; }
            set { pickUpDate = value; }
        }

        DateTime dropOffDate;
        public DateTime DropOffDate
        {
            get { return dropOffDate; }
            set { dropOffDate = value; }
        }

        string pickUpBranch;
        public string PickUpBranch
        {
            get { return pickUpBranch; }
            set { pickUpBranch = value; }
        }

        string dropOffBranch;
        public string DropOffBranch
        {
            get { return dropOffBranch; }
            set { dropOffBranch = value; }
        }

        string countryOfResidence;
        public string CountryOfResidence
        {
            get { return countryOfResidence; }
            set { countryOfResidence = value; }
        }

        int numberOfAdults;
        public int NumberOfAdults
        {
            get { return numberOfAdults; }
            set { numberOfAdults = value; }
        }

        int numberOfChildren;
        public int NumberOfChildren
        {
            get { return numberOfChildren; }
            set { numberOfChildren = value; }
        }


        string agentCode;
        public string AgentCode {
            get { return agentCode; }
            set { agentCode = value; }
        }

        // Added for PromoCode support by Nimesh on 16th Oct 2014
        private string promoCode;

        public string PromoCode
        {
            get { return promoCode; }
            set { promoCode = value; }
        }
        // End Added for PromoCode support by Nimesh on 16th Oct 2014
        /// <summary>
        /// Calculate the hire period of this request
        /// </summary>
        /// <returns></returns>
        public int GetHirePeriod()
        {
            return (dropOffDate - pickUpDate).Days +1;        
        }


        /// <summary>
        /// The requesting Brand (Site)
        /// </summary>
        string brand;
        public string Brand
        {
            get {
                if ((agentCode == "AIRNZB2C") && brand == string.Empty) return "AIRNZ";
                else if ((agentCode == "MACCOM") && brand == string.Empty) return "MACCOM";                             
                return brand; 
            }
            set { brand = value; }
        }

        string channelCode;
        public string ChannelCode
        {
            get { return channelCode; }
            set { channelCode = value; }
        }

        /// <summary>
        /// Allow forcing a particular PAckage for the Request
        /// </summary>
        string packageCode;
        public string PackageCode
        {
            get { return packageCode; }
            set { packageCode = value; }        
        }


        public AvailabilityRequest()
	    {
            countryCode = CountryCode.NONE;//default to nz, TODO: define defaults
	    }


        public string VehicleName;//added to support Vehicle Specific Quotes, TODO: encapsulate 

        public bool Init(/*VehicleType currentType from radio */) {
            
            //test lines consider default population..
            //vehicleType = currentType;
            //countryOfResidence = string.Empty;
            //pickUpBranch = string.Empty;
            //dropOffBranch = string.Empty;
            //test ends
            //Load Defaults
            return true;
        }

        public bool LoadFromAuroraParams(NameValueCollection queryParams)
        {
            string pickUpTimeStr = string.Empty, dropOfTimeStr = string.Empty;
            foreach (string key in queryParams.AllKeys)
            {
                if (key == null || key == string.Empty)
                    continue;
                string normalizedKey = (key != null ? key.ToLower() : string.Empty);
                string normalizedValue = queryParams[key].ToLower();
                string value = queryParams[key];
                switch (key)
                {
                    case "Brand":
                        brand = normalizedValue;
                        break;                   
                    case "CountryCode"://country code                        
                        switch (normalizedValue)
                        { 
                            case "nz":
                                countryCode = CountryCode.NZ;
                                break;
                            case "au":
                                countryCode = CountryCode.AU;
                                break;
                            case "us":
                                countryCode = CountryCode.US;
                                break;
                            case "ca":
                                countryCode = CountryCode.CA;
                                break;
                            default:
                                countryCode = CountryCode.NONE;
                                break;
                        }                       
                        break;
                    case "ac"://agent code
                        agentCode = normalizedValue;
                        break;
                    case "IsVan":
                        bool isVan;
                        vehicleType = VehicleType.Both;
                        if(bool.TryParse(normalizedValue, out isVan)) { 
                            if (isVan)
                            vehicleType = VehicleType.Campervan;
                        else 
                            vehicleType = VehicleType.Car;
                        }
                        break;
                    case "NoOfAdults":
                        numberOfAdults = 0;
                        int.TryParse(normalizedValue, out numberOfAdults);
                        break;
                    case "NoOfChildren":
                        numberOfChildren = 0;
                        int.TryParse(normalizedValue, out numberOfChildren);
                        break;
                    case "CountryOfResidence"://Country of Residence
                        countryOfResidence = normalizedValue;
                        break;
                    case "CheckOutZoneCode"://PickUp Branch
                        pickUpBranch = normalizedValue;
                        break;
                    case "CheckInZoneCode":
                        dropOffBranch = normalizedValue;
                        break;
                    case "CheckOutDateTime":
                        pickUpTimeStr = normalizedValue;
                        break;
                    case "CheckInDateTime":
                        dropOfTimeStr = normalizedValue;
                        break;
                    case "VehicleCode":
                        vehicleModel = value;
                        break;
                    case "CompanyCode":
                        companyCode = value;
                        break;
                    case "ChannelCode":
                        channelCode = value;
                        break;
                    case "PackageCode":
                        packageCode = value;
                        break;                       
                }
            }
            System.Globalization.CultureInfo provider = System.Globalization.CultureInfo.InvariantCulture;
            string format = "dd-MMM-yyyy HH:mm";
            PickUpDate = DateTime.ParseExact(pickUpTimeStr, format, provider);
            DropOffDate = DateTime.ParseExact(dropOfTimeStr, format, provider);

            if (channelCode != null && channelCode != string.Empty)
                _affiliate = ApplicationManager.GetAffeliateForCode(channelCode);
            
            return true;
        }

        Affiliate _affiliate;
        public Affiliate RequestingAffiliate
        {
            get { return _affiliate; }
        }

        /// <summary>
        /// Initialise the Availability Request Object out of Query String Params
        /// </summary>
        /// <param name="url">incoming url</param>
        /// <returns></returns>
        public bool LoadFromQueryStringParams(NameValueCollection queryParams) {
            //match: ?cc=AU&vc=&ac=AIRNZB2C&sc=rv&pc=&ch=AIRNZ&rf=&na=2&nc=2&cr=NZ&pb=BNE&pd=1&pm=6&py=2009&pt=10:30&db=ADL&dd=12&dm=7&dy=2009&dt=11:30&vt=1
            int pickUpDay = 0 , pickUpMonth = 0, pickUpYear = 0, dropOffDay=0, dropOffMonth=0, dropOffYear=0;
            string pickUpTimeStr = string.Empty, dropOfTimeStr = string.Empty;
            foreach(string key in queryParams.AllKeys ) {
                if (key == null || key == string.Empty) 
                    continue;
                string normalizedKey = (key != null ? key.ToLower() : string.Empty);
                string normalizedValue = queryParams[key].ToLower();
                string value = queryParams[key];
                switch(key) {
                    case "Brand":
                        brand = normalizedValue;
                        break;                    
                    case "brand":
                        brand = normalizedValue;
                        break;
                    case "cc"://country code                        
                        if (normalizedValue == "nz")
                            countryCode = CountryCode.NZ;
                        if (normalizedValue == "au")
                            countryCode = CountryCode.AU;
                        break;
                    case "ac"://agent code
                        agentCode = normalizedValue;
                        break;
                    case "vt":
                        vehicleType = VehicleType.Both;
                        if (normalizedValue == "rv" || normalizedValue == "av")
                            vehicleType = VehicleType.Campervan;
                        else if (normalizedValue == "car" || normalizedValue == "ac")
                            vehicleType = VehicleType.Car;
                        break;
                    case "na":
                        numberOfAdults = 0;
                        int.TryParse(normalizedValue, out numberOfAdults);
                        break;
                    case "nc":
                        numberOfChildren = 0;
                        int.TryParse(normalizedValue, out numberOfChildren);
                        break;
                    case "cr"://Country of Residence
                        countryOfResidence = normalizedValue;
                        break;
                    case "pb"://PickUp Branch
                        pickUpBranch = normalizedValue;
                        break;
                    case "db":
                        dropOffBranch = normalizedValue;
                        break;
                    case "pt":                        
                        pickUpTimeStr = normalizedValue;
                        break;
                    case "pd":
                        int.TryParse(normalizedValue, out pickUpDay);
                        break;
                    case "pm":
                        int.TryParse(normalizedValue, out pickUpMonth);
                        break;
                    case "py":
                        int.TryParse(normalizedValue, out pickUpYear);
                        break;
                    case "dd":
                        int.TryParse(normalizedValue, out dropOffDay);
                        break;
                    case "dm":
                        int.TryParse(normalizedValue, out dropOffMonth);
                        break;
                    case "dy":
                        int.TryParse(normalizedValue, out dropOffYear);
                        break;
                    case "dt":
                        dropOfTimeStr = normalizedValue;
                        break;
                    case "vc"://replaces vh
                        vehicleModel = value;
                        break;
                    case "promoCode":
                        PromoCode = value;
                        break;
                }
            }
            PickUpDate  = DateTimeHelper.GetDateTimeForQueryParams(pickUpDay, pickUpMonth, pickUpYear, pickUpTimeStr);
            DropOffDate = DateTimeHelper.GetDateTimeForQueryParams(dropOffDay, dropOffMonth, dropOffYear,dropOfTimeStr);
            //debugStr = PickUpDate.ToString();
            return true;
        }

        /// <summary>
        /// Debug Displayer
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            StringBuilder sb = new StringBuilder();            
            //HTML Format
            sb.Append("<br />Pick Up Branch :" + pickUpBranch );
            sb.Append("<br />Drop Off Branch :" + dropOffBranch);
            sb.Append("<br />Country of Residence :" + countryOfResidence);
            sb.Append("<br />Country Code :" + CountryCode);
            sb.Append("<br />Agent Code :" + agentCode);
            sb.Append("<br />Vehicle Type :" + vehicleType);
            sb.Append("<br />Vehicle Code :" + VehicleModel);
            sb.Append("<br />Pick Up Date :" + pickUpDate);
            sb.Append("<br />Drop Off Date :" + dropOffDate);
            sb.Append("<br />Number of Adults :" + numberOfAdults);
            sb.Append("<br />Number of Children :" + numberOfChildren);
            sb.Append("<br />Requesting brand :" + brand);
            return sb.ToString();
        }

        public string ToJSON() {
            StringBuilder sb = new StringBuilder("{");
            sb.Append("pb:'" + pickUpBranch  + "',");
            sb.Append("db:'" + dropOffBranch + "',");
            sb.Append("cr:'" + (countryOfResidence != null ? countryOfResidence.ToUpper() : "")  + "',");
            sb.Append("cc:'" + CountryCode.ToString().ToUpper() + "',");
            sb.Append("rf:'" + agentCode + "',");
            sb.Append("vt:'" + (vehicleType == VehicleType.Campervan ? "av" : "ac" ) + "',");
            sb.Append("vc:'" + VehicleModel + "',");
            sb.Append("pd:'" + (pickUpDate.Equals(new DateTime()) ? "dd-mmm-yyyy" : pickUpDate.ToString("dd-MMM-yyyy")) + "',");
            sb.Append("pt:'" + pickUpDate.ToString("HH:mm") + "',");
            sb.Append("dd:'" + (dropOffDate.Equals(new DateTime()) ? "dd-mmm-yyyy" : dropOffDate.ToString("dd-MMM-yyyy"))+ "',");
            sb.Append("dt:'" + dropOffDate.ToString("HH:mm") + "',");
            sb.Append("na:'" + numberOfAdults + "',");
            sb.Append("nc:'" + numberOfChildren + "',");
            sb.Append("vh:'" + vehicleModel + "',");
            sb.Append("pb:'" + (pickUpBranch != null  ? pickUpBranch.ToUpper()  : "") +"',");
            sb.Append("db:'" + (dropOffBranch != null ? dropOffBranch.ToUpper() : "") + "',");
            sb.Append("com:'" + (companyCode != null ? companyCode : "") + "',");            
            sb.Append("brand:'" + (brand != null ? brand : string.Empty) + "',");
            // Added for PromoCode support by Nimesh on 16th Oct 2014
            sb.Append("promocode:'" + (PromoCode != null ? PromoCode : string.Empty) + "',");
            // End Added for PromoCode support by Nimesh on 16th Oct 2014
            sb.Append("debug:'" + debugStr + "'");
            sb.Append("}");
            return sb.ToString();
        }

        /// <summary>
        /// Format the Availability Request for Web Service request
        /// </summary>
        /// <returns>GET Params String</returns>
        public string GenerateAuroraRequestURL() {          
            
            string agentcode = "B2C" + countryCode;

            //if (brand == "z") agentcode = "AIRNZB2C"; //TODO: invoke THLDomain query here + MAC..
            //if (brand == "c") agentcode = ApplicationManager.GetDomainForURL(System.Web.HttpContext.Current.Request.Url.Host).AgentCode;//TODO and test for all
            //replaced by:
            if (ApplicationManager.GetDomainForURL(System.Web.HttpContext.Current.Request.Url.Host).IsAggregator)
            {
                agentcode = ApplicationManager.GetDomainForURL(System.Web.HttpContext.Current.Request.Url.Host).AgentCode;
            }
            
            if (SessionManager.AgentCode != null && SessionManager.AgentCode != string.Empty) agentcode = SessionManager.AgentCode;
                        
            if (brand == "z" || brand == "c") brand = string.Empty;

            brand = (companyCode != null && companyCode.Length > 0 ? companyCode : brand);//if aggregator pass relevant brand forward

            string vCodeStr = vehicleModel;
            if(vehicleModel != null && vehicleModel.IndexOf(".") >0)//TODO: define required state
                vCodeStr = vehicleModel.Split('.')[1];            
            
            string isTestMode = System.Web.Configuration.WebConfigurationManager.AppSettings["IsTestMode"];
            string AgnisInclusiveProduct = "true";


            //rev:mia July 3, 2014 - change to TRUE if its coming from B2B
            //utm_campaign=CrossLink,&hso=true&hgo=true&gno
            try
            {
                string urlReferrer = System.Web.HttpContext.Current.Request.UrlReferrer.OriginalString;
                if (urlReferrer.IndexOf("utm_campaign=CrossLink") != -1
                    && urlReferrer.IndexOf("&hso") != -1
                    && urlReferrer.IndexOf("&hgo") != -1
                    && urlReferrer.IndexOf("&gno") != -1
                    && urlReferrer.IndexOf("&json") != -1)
                {
                    isTestMode = "true";
                    AgnisInclusiveProduct = "false";
                }
            }
            catch (Exception exUrlReferrer)
            {
                isTestMode = "false";
            }
            
            
            StringBuilder sb = new StringBuilder();

            //if (brand.ToUpper().Equals("U") || brand.ToUpper().Equals("A") || brand.ToUpper().Equals("E") || (brand.ToUpper().Equals("Q") && countryCode.Equals(CountryCode.NZ)))
            //    sb.Append("Brand=Y&");//this is a united debug line, remove once brand loaded to back-end
            //else
                sb.Append("Brand="+ brand.ToUpper() +"&"); //TODO: get the availability requests brands collection..
            
            sb.Append("CountryCode="+ countryCode +"&");
            sb.Append("VehicleCode=" + vCodeStr + "&");
            sb.Append("CheckOutZoneCode="+ pickUpBranch +"&");
            sb.Append("CheckOutDateTime=" + pickUpDate.ToString("dd-MMM-yyyy HH:mm") + "&");
            sb.Append("CheckInZoneCode="+ dropOffBranch +"&");
            sb.Append("CheckInDateTime=" + dropOffDate.ToString("dd-MMM-yyyy HH:mm") + "&");
            sb.Append("NoOfAdults="+ numberOfAdults +"&");
            sb.Append("NoOfChildren="+ numberOfChildren +"&");
            sb.Append("AgentCode=" + (agentcode != null ? agentcode.Replace(",", "")  : string.Empty) + "&");
            sb.Append("PackageCode=" + ( packageCode != null ? packageCode.Replace(",", "") : string.Empty) + "&");
            sb.Append("IsVan="+ (vehicleType == VehicleType.Campervan ? "True" : "False") +"&");
            sb.Append("IsBestBuy=True&");
            sb.Append("CountryOfResidence="+ (countryOfResidence != null ? countryOfResidence.ToUpper() : string.Empty) +"&");
            sb.Append("IsTestMode="+ isTestMode);
            sb.Append("&IsGross=" + IsGross);
            sb.Append("&AgnisInclusiveProduct=" + AgnisInclusiveProduct);
            //sb.Append("&sPromoCode=" + HttpUtility.UrlEncode(PromoCode));
            // Added for PromoCode support by Nimesh on 16th Oct 2014
            sb.Append("&sPromoCode=" + PromoCode);
            // End Added for PromoCode support by Nimesh on 16th Oct 2014
            return sb.ToString();   
        }

        private string safeQueryString(string promoCode)
        {
            return promoCode.Replace("%", "%2B");
        }
                
        /// <summary>
        /// In dev, Construct a query string (B2C internal) out of the existing Availability Request 
        /// </summary>
        /// <returns>Serialized query Params string</returns>
        public string GenerateSelectionParamsURL(bool includeVehicle)
        {
            StringBuilder queryParamsSB = new StringBuilder();
            AvailabilityRequest request = new AvailabilityRequest();//TODO: use requestor
            //TODO: construct request params
            //cc=AU&vc=&ac=&sc=rv&pc=&ch=&rf=&na=1&nc=0&cr=&pb=ADL&pd=18&pm=8&py=2010&pt=10:00&db=ADL&dd=23&dm=9&dy=2010&dt=15:00&vtype=rv&vh=2BMA&pv=1.0&brand=m
            queryParamsSB.Append("cc="+ CountryCode +"&");
            queryParamsSB.Append("na=" + "1"/* consider passing pax forward if any */  + "&");
            queryParamsSB.Append("cr=" + countryOfResidence  + "&");
            queryParamsSB.Append("sc="+ (vehicleType == VehicleType.Campervan ? "rv" : "ac") +"&");
            if(includeVehicle) queryParamsSB.Append("vh=" + vehicleModel + "&");
            queryParamsSB.Append("pb="+ pickUpBranch +"&");
            queryParamsSB.Append("pd="+ pickUpDate.ToString("dd") +"&");
            queryParamsSB.Append("pm=" + pickUpDate.ToString("MM") + "&");
            queryParamsSB.Append("py=" + pickUpDate.ToString("yyyy") + "&");
            queryParamsSB.Append("pt=" + pickUpDate.ToString("HH:mm") + "&");
            queryParamsSB.Append("db="+ dropOffBranch +"&");
            queryParamsSB.Append("dd="+ dropOffDate.ToString("dd") +"&");
            queryParamsSB.Append("dm=" + dropOffDate.ToString("MM") + "&");
            queryParamsSB.Append("dy=" + dropOffDate.ToString("yyyy") +  "&");
            queryParamsSB.Append("dt="+ dropOffDate.ToString("HH:mm") + "&");
            queryParamsSB.Append("vtype=" + (vehicleType == VehicleType.Campervan ? "rv" : "ac") + "&");
            
            queryParamsSB.Append("pv=1.0&");
            queryParamsSB.Append("brand="+ brand );
            return queryParamsSB.ToString();
        }


        //Added for Rerieve Quote functionality 22/12/2010
        /// <summary>
        /// Initiate Availbility request objject from a supplied quote xml node from Aurora
        /// </summary>
        /// <param name="quoteRentalInfoNode">RetrieveBooking/RentalInfo node</param>
        /// <returns>Success status of object initialization state</returns>
        public bool LoadFromQuoteRentalInfo(XmlNode quoteRentalInfoNode)
        {
            try
            {
                //get the domain context 
                THLDomain currentDomain = ApplicationManager.GetDomainForURL(System.Web.HttpContext.Current.Request.Url.Host);                                    
                DateTime PickUpDate, DropOffDate;
                string pickUpTimeStr = string.Empty, dropOffTimeStr = string.Empty;
                brand = THLBrand.GetBrandChar(currentDomain.Brand).ToString();            
                countryCode = currentDomain.Country;
                agentCode = quoteRentalInfoNode.SelectSingleNode("//RentalInfo/Rental").Attributes["AgentCode"].Value;
                                
                vehicleType = quoteRentalInfoNode.SelectSingleNode("//RentalInfo/Rental").Attributes["VehicleClass"].Value.Equals("AV") ? VehicleType.Campervan : VehicleType.Car;
                //vehicleType = VehicleType.Campervan;//TODO: define this context.
                

                //Changed By Nimesh on 29th July 2014 as no of adults and children are hardcoded, populating data from xml
                //numberOfAdults = 0;// n/a at service data level
                //numberOfChildren = 0;// n/a at service data level
                ////NumberOfAdults
                if (!(quoteRentalInfoNode.SelectSingleNode("//RentalInfo/Rental").Attributes["NumberOfAdults"] == null))
                    int.TryParse(quoteRentalInfoNode.SelectSingleNode("//RentalInfo/Rental").Attributes["NumberOfAdults"].Value, out numberOfAdults);
                if (!(quoteRentalInfoNode.SelectSingleNode("//RentalInfo/Rental").Attributes["NumberOfChildren"] == null))
                    int.TryParse(quoteRentalInfoNode.SelectSingleNode("//RentalInfo/Rental").Attributes["NumberOfChildren"].Value, out numberOfChildren);
                //End - Changed By Nimesh on 29th July 2014 as no of adults and children are hardcoded, populating data from xml

                countryOfResidence = string.Empty;// n/a at service data level
                pickUpBranch = quoteRentalInfoNode.SelectSingleNode("//RentalInfo/Rental").Attributes["PickupLocCode"].Value;;
                dropOffBranch = quoteRentalInfoNode.SelectSingleNode("//RentalInfo/Rental").Attributes["DropOffLocCode"].Value;;
                pickUpTimeStr = quoteRentalInfoNode.SelectSingleNode("//RentalInfo/Rental").Attributes["PickupDateTime"].Value;
                dropOffTimeStr = quoteRentalInfoNode.SelectSingleNode("//RentalInfo/Rental").Attributes["DropOffDateTime"].Value;
                DateTime.TryParse(pickUpTimeStr, out pickUpDate);
                DateTime.TryParse(dropOffTimeStr, out dropOffDate);
                vehicleModel = quoteRentalInfoNode.SelectSingleNode("//RentalInfo/Rental").Attributes["ProductCode"].Value;

                try
                {
                    System.Globalization.CultureInfo provider = System.Globalization.CultureInfo.InvariantCulture;
                    string format = "dd/MM/yyyy HH:mm:ss";
                    pickUpDate = DateTime.ParseExact(pickUpTimeStr, format, provider);
                    dropOffDate = DateTime.ParseExact(dropOffTimeStr, format, provider);
                }
                catch (Exception ex)
                {
                    string msg = "failed to load AvailabilityRequest DateTimes : " + ex.Message;
                    THLDebug.LogError(ErrorTypes.Application, "AvailabilityRequest:LoadFromQuoteRentalInfo", msg, string.Empty);                    
                }                
                return true;
            }
            catch (Exception ex)
            {
                string msg = "failed to load AvailabilityRequest object from quote XML : " + ex.Message;
                THLDebug.LogError(ErrorTypes.Application, "AvailabilityRequest:LoadFromQuoteRentalInfo", msg, string.Empty);
                return false;
            }        
        }
    }    
}