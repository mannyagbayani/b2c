﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;


namespace THL.Booking
{

    public enum AlternativeAvailabilityType {
        NormalBased = 0,
        MoveForward = 1,
        MoveBackwards = 2,
        SwitchLocation = 3,
        PickUpBased = 4,
        DropOffBased = 5,       
        OneWaySwitchLocation = 6,
        OriginalSearch = 7
    }

    /// <summary>
    /// Represent An Altenative Availability Item for a selected availability response
    /// </summary>
    /// 
    public class AlternativeAvailabilityItem : AvailabilityItem
    {

        AlternativeAvailabilityType _type;

        public AlternativeAvailabilityType Type {
            get { return _type; }
        }


        public DateTime PickUpDate, DropOffDate;//TODO: encapsulate
        public string PickUpLoc, DropOffLoc;//TODO: encapsulate
        public string PickUpLocCode, DropOffLocCode;//TODO: encapsulate
        
        public int HirePeriod;//TODO: encapsulate

        public string debugStr;

        /// <summary>
        /// Added to support front end GA reporting
        /// </summary>
        public int SortOrder;

        public AlternativeAvailabilityItem(AlternativeAvailabilityType type)
        {
            //init from recieved WS node
            _type = type;            
        }

        public string ToString() 
        {
            return "{'alternateType':'" + _type.ToString() + "'}";
        }

        public AlternativeAvailabilityItem(XmlNode auroraNode, string _vehicleCode)
        {
            //TODO: parse and process
            debugStr = auroraNode.InnerXml;
            loadFromXML(auroraNode);
            HirePeriod = (DropOffDate - PickUpDate).Days+1;
            vehicleCode = _vehicleCode;
        }


        /// <summary>
        /// Parse Aurora Alternate Availability XML node into B2C Object
        /// </summary>
        /// <param name="avalabilityRowXmlNode"></param>
        /// <returns>Success Status</returns>
        private bool loadFromXML(XmlNode avalabilityRowXmlNode)
        {
            //TODO: exp handle
            bool success = true;            
            //string avp, packageCode, packageDesc, brandCodeStr, packageComment, packageTCUrl, packageInfoURL,cki,cko;
            bool isLumpSum;
            decimal depositAmount = 0, depositPercentage = 0, estimatedTotal = 0; ;                
            _type = AlternativeAvailabilityItem.GetAlternativeTypeForAuroraCode(avalabilityRowXmlNode.Attributes["searchPattern"] != null ? avalabilityRowXmlNode.Attributes["searchPattern"].Value : string.Empty);
            XmlNode packageNode = avalabilityRowXmlNode.SelectSingleNode("Package");//load package Info
            if (packageNode.Attributes["AvpId"] == null) {
                AvailabilityType = AvailabilityItemType.BlockingRule; 
                Brand = THLBrand.GetBrandForString(packageNode.Attributes["BrandCode"].Value);//enable this instead: ai.Brand = packageNode.Attributes["BrandCode"].Value;
                XmlNode errorsNode = avalabilityRowXmlNode.SelectSingleNode("Errors");//load package Info
                BlockingRuleMessage = (errorsNode.SelectSingleNode("BlockingRuleMessage")==null)?"N/A":errorsNode.SelectSingleNode("BlockingRuleMessage").Attributes["Message"].Value;
                ErrorMessage = (errorsNode.SelectSingleNode("ErrorMessage")==null)? "N/A":errorsNode.SelectSingleNode("ErrorMessage").Attributes["Message"].Value;
                XmlNode chargeCollection = avalabilityRowXmlNode.SelectSingleNode("Charge/Det");//load Charges
                    if (chargeCollection != null && chargeCollection.Attributes["PrdCode"].Value != null)
                        VehicleCode =  chargeCollection.Attributes["PrdCode"].Value;
                    if (chargeCollection != null && chargeCollection.Attributes["PrdName"].Value != null)
                        VehicleName = chargeCollection.Attributes["PrdName"].Value;                   
                }
            else
            {
                int hirePeriod = 0;
                if(packageNode.Attributes["HirePeriod"] != null)
                    int.TryParse(packageNode.Attributes["HirePeriod"].Value, out hirePeriod);
                HirePeriod = hirePeriod; 

                AvailabilityType = AvailabilityItemType.Available;
                AVPId = packageNode.Attributes["AvpId"].Value;
                Brand = THLBrand.GetBrandForString(packageNode.Attributes["BrandCode"].Value);
                //VehicleCode = packageNode.Attributes[""].Value;
                PackageCode  = packageNode.Attributes["PkgCode"].Value;
                PackageInfoText = packageNode.Attributes["PkgInfoURL"].Value;
                Brand = THLBrand.GetBrandForString(packageNode.Attributes["BrandCode"].Value);
                Note = packageNode.Attributes["PkgComment"].Value;
                TandCLink = packageNode.Attributes["PkgTCURL"].Value;
                //packageInfoURL = packageNode.Attributes["PkgInfoURL"].Value;
                isLumpSum = (packageNode.Attributes["IsLumpSum"].Value.Equals("Yes") ? true : false);
                decimal.TryParse(packageNode.Attributes["EstimatedTotal"].Value, out estimatedTotal);
                EstimatedTotal = estimatedTotal;
                //get pickUp/DropOff locations
                XmlNode locationXml = avalabilityRowXmlNode.SelectSingleNode("Location");//load Charges
                
                // catch (log and set success = false                
                success = decimal.TryParse(packageNode.Attributes["DepositAmt"].Value, out depositAmount);
                if (packageNode.Attributes["DepositPercentage"] != null)//TODO define optionals here
                    success = decimal.TryParse(packageNode.Attributes["DepositPercentage"].Value.Replace('%', ' '), out depositPercentage);

                DepositPercentage = depositPercentage;
                DepositAmount = depositAmount;
                //nested try?
                XmlNodeList chargeCollection = avalabilityRowXmlNode.SelectNodes("Charge/Det");
                decimal dailyAvgRate = 0;
                List<AvailabilityItemChargeRow> aicList = new List<AvailabilityItemChargeRow>();                    
                string vehicleMsg = string.Empty;
                bool hasBonusPack = false;
                decimal adminFee = 0.0m;

                foreach (XmlNode chargeXml in chargeCollection)
                {// Each Charge <Det>                                        
                    decimal.TryParse(chargeXml.Attributes["AvgRate"].Value, out dailyAvgRate);
                    AvailabilityItemChargeRow aic = new AvailabilityItemChargeRow();
                    aic.LoadFromXml(chargeXml);
                    aicList.Add(aic);
                    if (aic.AdminFee > 0) adminFee = aic.AdminFee;
                    if (aic.IsBonusPack) hasBonusPack = true;
                    if (chargeXml.Attributes["ProductInfo"] != null && chargeXml.Attributes["ProductInfo"].Value != string.Empty)
                        vehicleMsg = chargeXml.Attributes["ProductInfo"].Value;
                }
                AItemChargeCollection = aicList.ToArray();
                AdminFee = adminFee;
                DisplayPromoTile = (packageNode.Attributes["DispTile"] != null && packageNode.Attributes["DispTile"].Value == "Yes");
                DiplayLoyaltyCard = (packageNode.Attributes["DispLoylCrdFld"] != null && packageNode.Attributes["DispLoylCrdFld"].Value == "Yes");
                XmlNode onewayFeeXMLNode = avalabilityRowXmlNode.SelectSingleNode("Charge/Det[@PrdCode='ONEWAYAU']");//TODO: get this not by code from Aurora
                if(onewayFeeXMLNode == null)
                    onewayFeeXMLNode = avalabilityRowXmlNode.SelectSingleNode("Charge/Det[@PrdCode='ONEWAYNZ']");//TODO: get this not by code from Aurora                   
                if (onewayFeeXMLNode == null)
                    onewayFeeXMLNode = avalabilityRowXmlNode.SelectSingleNode("Charge/Det[@PrdCode='ONEWAYE']");//TODO: get this not by code from Aurora
                if (onewayFeeXMLNode == null)
                    onewayFeeXMLNode = avalabilityRowXmlNode.SelectSingleNode("Charge/Det[@PrdCode='ONEWQ']");//TODO: get this not by code from Aurora                   
                decimal onewayFee = 0.0m; 
                if (onewayFeeXMLNode != null && onewayFeeXMLNode.Attributes["Price"] != null)
                    decimal.TryParse(onewayFeeXMLNode.Attributes["Price"].Value, out onewayFee);                    
                PickUpLoc = packageNode.Attributes["PickupLoc"].Value;
                DropOffLoc = packageNode.Attributes["DropOffLoc"].Value;

                PickUpLocCode = packageNode.Attributes["PickupLocCode"].Value;
                DropOffLocCode = packageNode.Attributes["DropOffLocCode"].Value;

                try
                {
                    string pickUpTimeStr = packageNode.Attributes["PickupDate"].Value;
                    string dropOffTimeStr = packageNode.Attributes["DropOffDate"].Value;                                             
                    System.Globalization.CultureInfo provider = System.Globalization.CultureInfo.InvariantCulture;
                    string format = "MMM d yyyy h:mmtt";
                    PickUpDate= DateTime.ParseExact(pickUpTimeStr.Replace("  "," "), format, provider);
                    DropOffDate = DateTime.ParseExact(dropOffTimeStr.Replace("  ", " "), format, provider);
                }
                catch (Exception ex)
                {
                    string msg = "failed to load AvailabilityRequest DateTimes : " + ex.Message;
                    THLDebug.LogError(ErrorTypes.Application, "AlternateAvailability:loadFromXML", msg, string.Empty);
                }
                OneWayFeeComponent = onewayFee;
                AvailabilityType = AvailabilityItemType.Available;
                XmlNode errorsNode = avalabilityRowXmlNode.SelectSingleNode("Errors");//load package Info
                IncludesBonusPack = hasBonusPack;
                VehicleMsgText = vehicleMsg;
                EstimatedTotal = estimatedTotal;                   
            }
            return success ;//on successful parse return true        
        }

        
        //AvailabilityItemChargeRow

        /// <summary>
        /// Refactor into static helper on AvailabilityResponseDisaplayer
        /// </summary>
        /// <param name="aType"></param>
        /// <returns></returns>
        public static AlternativeAvailabilityType GetAlternativeTypeForAuroraCode(string aType)
        {            
            AlternativeAvailabilityType responseType = AlternativeAvailabilityType.NormalBased;//default?
            switch (aType)
            {
                case "DropOffBased":
                    responseType = AlternativeAvailabilityType.DropOffBased;                    
                    break;
                case "PickUpBased": 
                    responseType = AlternativeAvailabilityType.PickUpBased;                    
                    break;
                case "SwitchLocation":
                    responseType = AlternativeAvailabilityType.SwitchLocation;                    
                    break;
                case "MoveForward": 
                    responseType = AlternativeAvailabilityType.MoveForward;                    
                    break;
                case "MoveBackWard":
                    responseType = AlternativeAvailabilityType.MoveBackwards;                    
                    break;                
                case "OriginalSearch":
                    responseType = AlternativeAvailabilityType.OriginalSearch;                    
                    break;
                case "OnewaySwitchLocationOnPickup":
                    responseType = AlternativeAvailabilityType.OneWaySwitchLocation;
                    break;
                default:
                    responseType = AlternativeAvailabilityType.NormalBased;
                    break;
            }
            return responseType;
        }


    }
}