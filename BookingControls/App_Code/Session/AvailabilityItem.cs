﻿//rev:mia june 9 2014 - B2BMAC - addition of CanGetGross to provide a color-driven price text similar in b2b
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace THL.Booking
{
    public enum AvailabilityItemType
    {
        Available = 0,
        OnRequest = 1,                        
        BlockingRule = 2,
        BlockingRuleTaC = 3,
        SystemMessage = 4,
        NoRates = 5
    } 
    
    /// <summary>
    /// Availability Item Class representing Package available by Aurora within Availability response
    /// </summary>
    public class AvailabilityItem
    {
        //rev:mia june 9 2014 - B2BMAC
        string _canGetGross;
        public string CanGetGross
        {
            get { return _canGetGross; }
            set { _canGetGross = value; }
        }


        string _agentCode;
        public string AgentCode
        {
            get { return _agentCode; }
            set { _agentCode = value; }
        }

        /// <summary>
        /// Added to support specials tile within Selection Catalog
        /// </summary>
        public bool DisplayPromoTile;

        /// <summary>
        /// Promo Text for this Package
        /// </summary>
        string promoText;
        public string PromoText 
        {
            get { return promoText; }
            set {
                if (!string.IsNullOrEmpty(value) && value.Contains('|'))
                {
                    string[] promoTxtParams = value.Split('|');
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<big>" + promoTxtParams[0] + "</big>");
                    for (int i = 1; i < promoTxtParams.Length; i++)
                        sb.Append("<small>" + promoTxtParams[i] + "</small>");
                    promoText = sb.ToString();
                }
                else
                    promoText = string.Empty;
            }
        }


        /// <summary>
        /// NRMA Loyalty card required
        /// TODO: refactor and encapsulate for future loyalty member functionality
        /// </summary>
        public bool DiplayLoyaltyCard;//21/06/10

        string blockingRuleMessage;
        public string BlockingRuleMessage
        {
            get { return blockingRuleMessage; }
            set { blockingRuleMessage = value; }
        }

        string errorMessage;
        public string ErrorMessage
        {
            get { return errorMessage; }
            set { errorMessage = value; }
        }
        
        public decimal AdminFee = 0.0m;

        string note;
        /// <summary>
        /// Note Element, used for Inclusive Item Details
        /// </summary>
        public string Note
        {
            get { return note; }
            set { note = value; }
        }
        
        AvailabilityItemType availabilityType;
        public AvailabilityItemType AvailabilityType
        {
            get { return availabilityType;  }
            set { availabilityType = value; }        
        }

        /// <summary>
        /// Identify the Availability Row in Aurora
        /// </summary>
        protected string AVPId;
       
        /// <summary>
        /// Return the Availability Item AVP ID
        /// </summary>
        /// <returns></returns>
        public string AVPID {
            get { return AVPId; } 
        }


        public decimal OneWayFeeComponent;

        /// <summary>
        /// Total Charges for the Item Charges collection
        /// </summary>
        private decimal estimatedTotal;

        public decimal EstimatedTotal
        {
            get { return estimatedTotal; }
            set { estimatedTotal = value; }
        }

        /// <summary>
        /// Aurora Package Code
        /// </summary>
        private string packageCode;

        public string PackageCode
        {
            get { return packageCode; }
            set { packageCode = value; }
        }

        /// <summary>
        /// Aurora Package Description
        /// </summary>
        private string packageDescription;

        /// <summary>
        /// Package stuff generated comment
        /// </summary>
        private string packageComment;
                
        private decimal avgRate;
        public decimal AvgRate
        {
            get { return avgRate; }
        }

        /// <summary>
        /// Terms and Conditions Content Link for an available package
        /// </summary>        
        private string packageTermConsUrl;
        /// <summary>
        /// Terms and Conditions Link for this Package
        /// </summary>
        public string TandCLink
        {
            get { return packageTermConsUrl; }
            set { packageTermConsUrl = value; }
        }

        
        /// <summary>
        /// Info URL for package points to a content managed page in the parent size 
        /// </summary>
        private string packageInfoUrl;

        public string PackageInfoText
        {
            get { return packageInfoUrl; }
            set { packageInfoUrl = value; }
        }


        /// <summary>
        /// Required deposit amount for package
        /// </summary>
        private decimal depositAmount;
        public decimal DepositAmount 
        {
            get { return depositAmount;  }
            set { depositAmount = value; }
        }
        
        /// <summary>
        /// required Deposit Percentage for package
        /// </summary>
        private decimal depositPercentage;
        public decimal DepositPercentage
        {
            get { return depositPercentage; }
            set { depositPercentage = value; }
        }

        private bool isLumpSum;
        
        /// <summary>
        /// THL Brand for this Item
        /// </summary>
        private THLBrands brand;
        
        public THLBrands Brand 
        {
            get { return brand;  }
            set { brand = value; }
        }
        
        /// <summary>
        /// Pick Up location
        /// </summary>
        private string ckoLocation;

        /// <summary>
        /// Drop Off Location
        /// </summary>
        private string ckiLocation;


        protected string vehicleCode;
        public string VehicleCode
        {
            get { return vehicleCode; }
            set { vehicleCode = value; }
        }

        //-- Start Added for CrossSell Support on B2B
        public decimal PayableToAgent { get; set; }
        public decimal PayableAtPickUp { get; set; }
        //-- End Added for CrossSell Support on B2B

        /// <summary>
        /// Support retrieval of Vehicle Name from the Package Information
        /// </summary>
        /// <returns></returns>
        public string getVehicleNameFromPackage()
        {

            if (AItemChargeCollection != null && AItemChargeCollection.Length > 0)
            {
                foreach (AvailabilityItemChargeRow aci in AItemChargeCollection)
                {
                    if (!string.IsNullOrEmpty(aci.ProductClass) && (aci.ProductClass.ToUpper().Equals("AV") || aci.ProductClass.ToUpper().Equals("AC")))
                        return aci.ProductName;
                }
            }
            return string.Empty;
        }


        public Vehicle GetVehicle() {
            
            string vName = vehicleName;
            //Added to handle AltAvail scenarios with empty 
            if (String.IsNullOrEmpty(vehicleName) && AItemChargeCollection != null && AItemChargeCollection.Length > 0)
                vName = getVehicleNameFromPackage();         
            
            
            
            Vehicle vehicle = new Vehicle(vName, VehicleType.None, vehicleCode, 0, string.Empty);           
            
            return vehicle;       
        }
                
        private string getVehicleCode()
        {
            foreach (AvailabilityItemChargeRow aRow in AItemChargeCollection)
            {
                if (aRow.IsVehicle)
                    return aRow.ProductCode;
            }
            return string.Empty;//TODO: inform caller
        }
                
        protected AvailabilityItemChargeRow[] AItemChargeCollection;
        
        public AvailabilityItemChargeRow[] GetNonVehicleCharges()
        {
            List<AvailabilityItemChargeRow> aicrc = new List<AvailabilityItemChargeRow>();
            foreach (AvailabilityItemChargeRow aicr in AItemChargeCollection)
            {
                if (aicr.ProductClass != "AV" && aicr.ProductClass != "AC")
                    aicrc.Add(aicr);
            }
            return aicrc.ToArray();
        }
        
        public AvailabilityItemChargeDiscount[] GetDiscounts() {
            List<AvailabilityItemChargeDiscount> discountedRatesList = new List<AvailabilityItemChargeDiscount>();
            if(AItemChargeCollection == null || AItemChargeCollection.Length == 0)
                return null;
            else
            {
                foreach (AvailabilityItemChargeRow aicr in AItemChargeCollection)
                {
                    if (aicr.DiscountCollection != null && aicr.DiscountCollection.Length > 0)
                        discountedRatesList.AddRange(aicr.DiscountCollection);
                }
                return discountedRatesList.ToArray();
            }
        }
        
        public AvailabilityItemChargeRow GetBonusPackCharge() {
            foreach (AvailabilityItemChargeRow aicr in AItemChargeCollection)
                if (aicr.IsBonusPack == true)
                    return aicr;
            return null;
        }

        public string BonusPackName 
        {
            get
            {
                foreach (AvailabilityItemChargeRow aicr in AItemChargeCollection)
                    if (aicr.IsBonusPack == true)
                        return aicr.ProductName;
                return string.Empty;
            }
        }

        public string BonusPackCode
        {
            get
            {
                foreach (AvailabilityItemChargeRow aicr in AItemChargeCollection)
                    if (aicr.IsBonusPack == true)
                        return aicr.ProductCode;
                return string.Empty;
            }
        }

        
        /// <summary>
        /// Returned Text to display within Configure top Panel
        /// </summary>
        public string VehicleMsgText;
        
        /// <summary>
        /// Get The Total Saving on This availability Item (collected from all Charges).
        /// </summary>
        /// <returns></returns>
        public decimal GetTotalSaving() {
            decimal totalSaving = 0m;
            foreach (AvailabilityItemChargeRow aiCharge in AItemChargeCollection)
                totalSaving += aiCharge.TotalSavings;
            return totalSaving;
        }
        
        /// <summary>
        /// Get All Rate Band Rows for the Availablity Item
        /// </summary>
        /// <returns></returns>
        public AvailabilityItemChargeRateBand[] GetRateBands() {
            foreach(AvailabilityItemChargeRow aiCharge in AItemChargeCollection) 
            {
                if (aiCharge.RateBandCollection != null) {
                    return aiCharge.RateBandCollection;
                }
            }
            return null;        
        }

        /// <summary>
        /// Return the Vehigle Charge Row for this Availability
        /// </summary>
        /// <returns></returns>
        public AvailabilityItemChargeRow GetVehicleChargeRow() 
        {
            foreach (AvailabilityItemChargeRow aiCharge in AItemChargeCollection)
            {
                if (aiCharge.IsVehicle)
                {
                    return aiCharge;
                }
            }
            return null;        
        }
        
        public AvailabilityItemChargeRow[] GetLocationFees()
        {
            //TODO: filter out for <Type="LOCFE">, TODO: once defined add fees enum here
            List<AvailabilityItemChargeRow> locFees = new List<AvailabilityItemChargeRow>();
            foreach (AvailabilityItemChargeRow aicr in AItemChargeCollection)
            {
                if (aicr.ProductType.Equals("LOCFE"))//TODO: swap by class attr (CR/FE)
                {
                    locFees.Add(aicr);
                }
            }
            return locFees.ToArray();
        }
        
        private string vehicleName;
        public string VehicleName {
            get { return vehicleName; }
            set { vehicleName = value; }
        }


        private bool includesBonusPack;
        public bool IncludesBonusPack
        {
            get { return includesBonusPack; }
            set { includesBonusPack = value; }
        }

        private bool waiveInclusiveOneWayFee;
        public bool WaiveInclusiveOneWayFee
        {
            get { return waiveInclusiveOneWayFee; }
            set { waiveInclusiveOneWayFee = value; }
        }

        
        public AvailabilityItem(string _avpId, string _packageCode, string _packageDesc, string _brandCodeStr, string _packageComment, string _packageTCUrl, string _packageInfoURL, decimal _depositAmount, decimal _depositPercentage, bool _isLumpSum, string _cko, string _cki, decimal _avgDailyRate, AvailabilityItemChargeRow[] _aiChargeRow)
        {
            AVPId = _avpId;
            packageCode = _packageCode;
            packageDescription = _packageDesc;
            brand = THLBrand.GetBrandForString(_brandCodeStr);
            packageComment = _packageComment;
            packageTermConsUrl = _packageTCUrl;
            packageInfoUrl = _packageInfoURL;
            depositAmount = _depositAmount;
            depositPercentage = _depositPercentage;
            isLumpSum = _isLumpSum;
            ckoLocation = _cko;
            ckiLocation = _cki;
            avgRate = _avgDailyRate;
            AItemChargeCollection = _aiChargeRow;
            vehicleName = getVehicleName();
            vehicleCode = getVehicleCode();
        }
                
        public AvailabilityItem()
        { 
            //used for non available rows
        }

        private string getVehicleName()
        {
            foreach (AvailabilityItemChargeRow aiCharge in AItemChargeCollection)
            {
                if (aiCharge.ProductClass == "AV" || aiCharge.ProductClass == "AC")//found vehicle component(optional use IsVehicle)
                {
                    return aiCharge.ProductName;
                }
            }
            return String.Empty;
        }

        /// <summary>
        /// Get the Availability Items Long Hire Discount
        /// </summary>
        /// <returns></returns>
        public decimal GetLongHireDiscount() {
            foreach (AvailabilityItemChargeRow aiCharge in AItemChargeCollection)
            {
                if (aiCharge.DiscountCollection != null && aiCharge.ProductClass == "AV" || aiCharge.ProductClass == "AC")//found vehicle component
                {//has a discount component
                    foreach (AvailabilityItemChargeDiscount discount in aiCharge.DiscountCollection)
                    {
                        if (discount.CodCode == "LHD")
                            return discount.DisPercentage;
                    }
                }
            }
            return 0m;        
        }
                
        private decimal totalCharge = 0;
        public decimal GetTotalPrice() {
            if (totalCharge > 0) return totalCharge;//save the round trip.
            foreach (AvailabilityItemChargeRow aiCharge in AItemChargeCollection)
            {
                if (aiCharge.ProductClass == "AV" || aiCharge.ProductClass == "AC")//found vehicle component
                {
                    return aiCharge.ProductPrice ;
                }
            }
            return 0m;   
        }

        /// <summary>
        /// Sum Hire Period from all Charge Collection
        /// </summary>
        /// <returns></returns>
        public int GetHirePeriod() 
        {
            int hirePeriod = 0 ;
            foreach (AvailabilityItemChargeRow aicr in AItemChargeCollection)
            {
                if (aicr.HirePeriod > 0 && aicr.RateBandCollection != null && aicr.RateBandCollection.Length > 0)
                    hirePeriod += aicr.HirePeriod;
            }
            return hirePeriod;
        }

        /// <summary>
        /// Tag if this entry has been released to UI
        /// </summary>
        public bool DeliveredToFront
        {
            get;
            set;
        }

    }
}