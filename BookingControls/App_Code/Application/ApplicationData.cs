﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Collections.Specialized;


namespace THL.Booking
{

    /// <summary>
    /// This is a Global Configuration and Application wide information that should later be read from Aurora MasterXML
    /// or Application Level common data
    /// 
    /// </summary>
    public class ApplicationData
    {

        Branch[] branches;
        City[] cities;
        THLDomain[] domains;
        Vehicle[] vehicles;
        Vehicle[] macVehicles;
        DPSAccount[] dpsAccounts;
        TrackingData[] trackingDataElements;
        Affiliate[] affiliates;
        
        /// <summary>
        /// Added For Widget Data Support
        /// </summary>
        Branch[] WidgetBranches;
        bool loadWidgetData()
        {
            try
            {
                string[] brandStrs = { "m", "b", "y", "u", "a", "e", "q" };
                string[] countries = { "NZ", "AU" };

                Dictionary<string, Branch> BranchesList = new Dictionary<string, Branch>();
                THLDebug.LogEvent(EventTypes.MTierStatus, "{'state':'entering widget load of branches'}");

                foreach (string brandStr in brandStrs)
                {
                    THLBrands currentBrand = THLBrand.GetBrandForString(brandStr);
                    string filename = System.Web.Configuration.WebConfigurationManager.AppSettings["LocalDataPath"] + "\\" + brandStr + ".xml";
                    DataAccess da = new DataAccess();
                    XmlDocument xmlDoc = da.GetXMLFromFile(filename);
                    XmlNodeList nodeList = xmlDoc.SelectNodes("//branch ");

                    foreach (XmlNode node in nodeList)
                    {
                        string currentCode = node.Attributes["value"].Value;

                        Branch current = null;
                        if (BranchesList.ContainsKey(currentCode))
                        {
                            current = BranchesList[currentCode];
                            List<THLBrands> brands = new List<THLBrands>(current.brands);
                            if (!brands.Contains<THLBrands>(currentBrand))
                                brands.Add(currentBrand);
                            current.brands = brands.ToArray();
                        }
                        else
                        {
                            current = new Branch();
                            current.Name = node.Attributes["name"].Value;
                            current.BranchCode = currentCode;
                            current.country = AvailabilityHelper.GetCountryCodeForString(node.ParentNode.ParentNode.Attributes["code"].Value);
                            switch (node.Attributes["type"].Value)
                            {
                                case "av":
                                    current.VehicleTypeAvailability = VehicleType.Campervan;
                                    break;
                                case "ac":
                                    current.VehicleTypeAvailability = VehicleType.Car;
                                    break;
                                case "both":
                                    current.VehicleTypeAvailability = VehicleType.Both;
                                    break;
                            }
                            current.brands = new THLBrands[] { currentBrand };
                            BranchesList.Add(currentCode, current);
                        }
                    }
                }

                WidgetBranches = BranchesList.Values.ToArray();
                THLDebug.LogEvent(EventTypes.MTierStatus, "{'state':'loaded " + (WidgetBranches != null ? WidgetBranches.Length : 0) + " widget load of branches ended..'}");
                return true;
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Application, "ApplicationData.loadWidgetData()", "{'err':'" + ex.Message + "'}", string.Empty);
                return false;
            }
        }



        /// <summary>
        /// Added For Widget Support, TODO: add biz logic for serving vehicle type if required
        /// </summary>
        /// <returns></returns>
        public Branch[] GetWidgetBranches()
        {
            //Possible Filtering and passed conditions here..
            return WidgetBranches;
        }


        public Branch[] GetBranchesForBrand(THLBrands brand)
        {
            List<Branch> branchesList = new List<Branch>();
            foreach (Branch branch in branches)
            {
                for (int i = 0; i < branch.brands.Length; i++)
                {
                    if (branch.brands[i].Equals(brand))
                        branchesList.Add(branch);
                }
            }
            return branchesList.ToArray();
        }


        public TrackingData[] TrackingElements
        {
            get { return trackingDataElements; }
        }

        public NameValueCollection BPAEMapper;//Map Old Controls Branches to Aurora Codes

        public NameValueCollection CtrlBranchLabels;//added for supporting branch labels in configure/payment(150110)  

        /// <summary>
        /// Return a Cache Affeliate for a provided code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public Affiliate GetAffiliateForCode(string code)
        {
            foreach (Affiliate affl in affiliates)
                if (affl.Code.Equals(code)) return affl;
            return null;
        }

        public DPSAccount GetDPSAccountForAgentCode(string agentCode)
        {
            foreach (DPSAccount dpsAccount in dpsAccounts)
                if (dpsAccount.AgentAccount.Equals(agentCode))
                    return dpsAccount;
            return null;
        }

        public Vehicle[] MacVehicles
        {
            get { return macVehicles; }
        }

        ContentElement[] contentElements;

        ContentElement[] macContentElements;
        public ContentElement[] MACContentElements
        {
            get { return macContentElements; }
        }

        public ContentElement[] ContentElements
        {
            get { return contentElements; }
        }

        public Vehicle[] Vehicles
        {
            get { return vehicles; }
        }

        /// <summary>
        /// Return a Cached Vehicle
        /// </summary>
        /// <param name="vehicleCode"></param>
        /// <returns></returns>
        public Vehicle GetVehicleForCode(string vehicleCode, THLBrand brand)
        {
            foreach (Vehicle vehicle in vehicles)
                if (vehicle.Code == vehicleCode)
                    return vehicle;
            return null;
        }

        public THLDomain[] Domains
        {
            get { return domains; }
        }

        XmlDocument currencyXML;

        public XmlDocument CurrencyXML
        {
            get { return currencyXML; }
        }

        bool loadMasterXML()
        {
            List<Branch> branchList = new List<Branch>();
            List<City> cityList = new List<City>();
            try
            {
                string masterXmlURL = System.Web.Configuration.WebConfigurationManager.AppSettings["MasterXMLPath"];
                string[] countries = { "NZ", "AU" };

                List<Affiliate> affiliatesList = new List<Affiliate>();

                foreach (string country in countries)
                {
                    string paramsStr = "CountryCode=" + country + "&CompanyCode=&BrandCode=&VehicleCode=";
                    string requestURL = masterXmlURL + "?" + paramsStr;
                    XmlDocument masterDocXml = new XmlDocument();
                    THLDebug.LogEvent(EventTypes.CacheRequest, "{'requested':'" + requestURL + "'}");

                    try
                    {
                        masterDocXml.Load(requestURL);
                        //string localFilePath = System.Web.Configuration.WebConfigurationManager.AppSettings["LocalDataPath"] + "/master.xml";
                        //masterDocXml.Load(localFilePath);
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        THLDebug.LogError(ErrorTypes.ExternalProvider, "ApplicationData.loadMasterXML", msg, "{paramStr:'failed to load master xml:" + paramsStr + "'}");
                        string localFilePath = System.Web.Configuration.WebConfigurationManager.AppSettings["LocalDataPath"] + "/master.xml";
                        masterDocXml.Load(localFilePath);
                        THLDebug.LogEvent(EventTypes.CacheRequest, "{'loadedLocalMaster':'" + localFilePath + "'}");
                    }

                    XmlNodeList branchNodes = masterDocXml.SelectNodes("//branch");
                    branchList.AddRange(loadBranches(branchNodes));
                    XmlNodeList citiesNodes = masterDocXml.SelectNodes("//city");
                    cityList.AddRange(loadCities(citiesNodes));

                    //Add Affiliates 
                    XmlNodeList affiliatesNodes = masterDocXml.SelectNodes("//agent");
                    foreach (XmlNode affNode in affiliatesNodes)
                    {
                        Affiliate currentAffl = new Affiliate(affNode);
                        if (currentAffl != null)
                            affiliatesList.Add(currentAffl);
                    }
                }
                branches = branchList.ToArray();
                cities = cityList.ToArray();
                affiliates = affiliatesList.ToArray();

                return (branches.Length > 0 && cities.Length > 0 && affiliates.Length > 0);

            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                THLDebug.LogError(ErrorTypes.Application, "ApplicationData.loadMasterXML", errMsg, "{}");
                return false;
            }
        }

        public ApplicationData()
        {
            bool branchesAndCitiesLoaded = false, currencyLoaded = false, domainsLoaded = false, vehiclesLoaded = false, contentElmLoaded = false, dPSAccountsLoaded = false, trackingElementsLoaded = false, totalLoad = true, widgetLoaded = false;
            try
            {
                branchesAndCitiesLoaded = loadMasterXML();
                currencyLoaded = loadCurrencyData();
                domainsLoaded = loadDomainsDefinitions();
                vehiclesLoaded = loadVehicles();
                contentElmLoaded = loadContentElements();
                dPSAccountsLoaded = loadDPSAccounts();
                trackingElementsLoaded = loadTrackingElements();
                widgetLoaded = loadWidgetData();


            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                totalLoad = false;
            }

            string cacheLoadStatus = "{CacheLoadStatus:'branchesAndCitiesLoaded:" + branchesAndCitiesLoaded + "  currencyLoadSuccess:" + currencyLoaded + ", domainsLoaded:" + domainsLoaded + ", vehiclesLoaded:" + vehiclesLoaded + ", contentElmLoaded: " + contentElmLoaded + ",dpsLoaded:'" + dPSAccountsLoaded + "', trackingCodesLoaded:" + trackingElementsLoaded + ", widgetLoaded:" + widgetLoaded + ", totalLoad:" + totalLoad + "'}";

            THLDebug.LogEvent(EventTypes.MTierStatus, cacheLoadStatus);
            if (!branchesAndCitiesLoaded || !currencyLoaded || !domainsLoaded || !vehiclesLoaded || !contentElmLoaded || !dPSAccountsLoaded || !trackingElementsLoaded)
                THLDebug.LogError(ErrorTypes.Application, "ApplicationData().const", "app cache failue", cacheLoadStatus);

            //TODO: log error if any is false;
        }

        bool loadAffiliates()
        {
            DataAccess dal = new DataAccess();
            //affiliates = dal.
            return true;//TODO:
        }

        bool loadTrackingElements()
        {
            DataAccess da = new DataAccess();
            trackingDataElements = da.GetTrackingDataElements(domains);
            return (trackingDataElements != null && trackingDataElements.Length > 0);
        }

        /// <summary>
        /// Initialize the DS accounts collection
        /// </summary>
        /// <returns>Success Status</returns>
        bool loadDPSAccounts()
        {
            string filename = System.Web.Configuration.WebConfigurationManager.AppSettings["DPSData"];
            DataAccess da = new DataAccess();
            dpsAccounts = da.GetDPSAccounts();
            return (dpsAccounts.Length > 0);
        }

        bool loadDomainsDefinitions()
        {
            string domainXmlPath = string.Empty;
            try
            {
                domainXmlPath = System.Web.Configuration.WebConfigurationManager.AppSettings["LocalDataPath"] + "\\domains.xml";
                XmlDocument domainXmlDoc = new XmlDocument();
                domainXmlDoc.Load(domainXmlPath);
                XmlNodeList domainNodes = domainXmlDoc.SelectNodes("//domain");
                List<THLDomain> domainsList = new List<THLDomain>();
                foreach (XmlNode xmlDomainNode in domainNodes)
                {
                    bool isAggregator = false;
                    bool.TryParse(xmlDomainNode.Attributes["aggregator"].Value, out isAggregator);
                    string name = xmlDomainNode.Attributes["name"].Value;
                    string brandStr = xmlDomainNode.Attributes["brand"].Value;
                    string countryStr = xmlDomainNode.Attributes["country"].Value;
                    string agentCode = xmlDomainNode.Attributes["agentcode"].Value;
                    string uaCode = xmlDomainNode.Attributes["ua"].Value;

                    string openTag = string.Empty;
                    try
                    {
                        openTag = xmlDomainNode.Attributes["openTag"].Value;
                    }
                    catch (Exception)
                    {

                        //throw;
                    }

                    THLBrands brand = THLBrand.GetBrandForString(brandStr);
                    CountryCode country = AvailabilityHelper.GetCountryCodeForString(countryStr);
                    THLDomain domain = new THLDomain(name, brand, country, agentCode, isAggregator, uaCode, openTag);

                    //Added to support Engine Versioning
                    decimal version = 1.0m;
                    if (xmlDomainNode.Attributes["version"] != null)
                        decimal.TryParse(xmlDomainNode.Attributes["version"].Value, out version);
                    domain.EngineVersion = version;

                    domainsList.Add(domain);
                }
                domains = domainsList.ToArray();

            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Application, "ApplicationData.loadDomainsDefinitions", "{'loadPath':'" + domainXmlPath + "'}", "{'err':'" + ex.Message + "'}");
                return false;
            }
            return true;
        }

        /// <summary>
        /// Load Converssion rates of the External Provider
        /// </summary>
        /// <returns>Success Status</returns>
        bool loadCurrencyData()
        {
            string currenctConvertorURL = string.Empty;
            try
            {
                //--- Load Currency Converssion XML
                currenctConvertorURL = System.Web.Configuration.WebConfigurationManager.AppSettings["CurrencyConversionURL"];
                DataAccess da = new DataAccess();
                string stringXml = da.GetStringForURL(currenctConvertorURL);
                XmlDocument responseXML = new XmlDocument();
                if (stringXml == null || stringXml == string.Empty)
                {
                    string localCurrencyFile = System.Web.Configuration.WebConfigurationManager.AppSettings["LocalDataPath"] + "\\" + System.Web.Configuration.WebConfigurationManager.AppSettings["LocalCurrencyFile"];
                    responseXML = da.GetXMLFromFile(localCurrencyFile);
                }
                else
                {
                    responseXML.LoadXml(stringXml);
                }
                currencyXML = responseXML;
                return true;
            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;
                THLDebug.LogError(ErrorTypes.ExternalProvider, "ApplicationData.loadCurrencyData", errorMsg, "{url:'" + currenctConvertorURL + "'}");
                return false;
            }
        }

        bool loadVehicles()
        {
            try
            {
                string[] brandStrs = { "m", "b", "p", "x", "c", "y", "q", "u", "a", "e" };
                string[] countries = { "NZ", "AU", "US" };

                BPAEMapper = new NameValueCollection();
                CtrlBranchLabels = new NameValueCollection();

                List<Vehicle> vehiclesList = new List<Vehicle>(), macVehiclesList = new List<Vehicle>();
                foreach (string countryCode in countries)
                {
                    foreach (string brandStr in brandStrs)
                    {
                        THLBrands currentBrand = THLBrand.GetBrandForString(brandStr);
                        string filename = System.Web.Configuration.WebConfigurationManager.AppSettings["LocalDataPath"] + "\\" + brandStr + ".xml";
                        DataAccess da = new DataAccess();
                        XmlDocument xmlDoc = da.GetXMLFromFile(filename);


                        if (currentBrand == THLBrands.MAC)
                        {
                            //<companies><company id="B" name="Britz" type="both" owner="THL"><vehicles>                            
                            XmlNodeList linkElements = xmlDoc.SelectNodes("//country[@code='" + countryCode + "']/companies/company/vehicles/vehicle");
                            foreach (XmlNode vehicleNode in linkElements)
                            {
                                //added 280110
                                THLBrands vehicleBrand = THLBrand.GetBrandForString(vehicleNode.ParentNode.ParentNode.Attributes["id"].Value);
                                //added 280110

                                macVehiclesList.Add(Vehicle.FromXML(vehicleNode, countryCode, vehicleBrand/*replaced : currentBrand */));
                            }
                        }
                        else
                        {
                            XmlNodeList linkElements = xmlDoc.SelectNodes("//country[@code='" + countryCode + "']/vehicles/vehicle");
                            foreach (XmlNode vehicleNode in linkElements)
                            {
                                vehiclesList.Add(Vehicle.FromXML(vehicleNode, countryCode, currentBrand));
                                System.Diagnostics.Debug.WriteLine(vehicleNode.OuterXml);
                            }
                        }


                        //Create Mapper Dic: BPAE -> Aurora
                        foreach (XmlNode branchNode in xmlDoc.SelectNodes("//branches/branch"))
                        {
                            if (branchNode.Attributes["BPAE"] != null && branchNode.Attributes["value"] != null)
                                BPAEMapper.Set(branchNode.Attributes["BPAE"].Value, branchNode.Attributes["value"].Value);


                            CtrlBranchLabels.Set(branchNode.Attributes["value"].Value, branchNode.Attributes["name"].Value);

                        }
                    }
                }
                vehicles = vehiclesList.ToArray();

                //load vehicle content from central repository
                int numLoaded = ApplicationData.LoadVehiclesContent(ref vehicles);//TODO: enable once ready, log num as event

                macVehicles = macVehiclesList.ToArray();
                numLoaded = ApplicationData.LoadVehiclesContent(ref macVehicles);//TODO: enable once ready, log num as event

                return (vehicles.Length > 0);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                //TODO: log point.
                return false;
            }
        }

        bool loadContentElements()
        {
            List<ContentElement> contentElmList = new List<ContentElement>();
            //Nimesh
            //ContentElement tmpContentElm = new ContentElement();
            try
            {
                string[] brandStrs = { "m", "b", "p", "x", "c", "y", "q", "u", "a", "e" };
                string[] countries = { "NZ", "AU"/*, "US"*/ };
                List<ContentElement> elmentsList = new List<ContentElement>(), macElementsList = new List<ContentElement>();
                foreach (string brandStr in brandStrs)
                {
                    THLBrands brand = THLBrand.GetBrandForString(brandStr);
                    string filename = System.Web.Configuration.WebConfigurationManager.AppSettings["LocalDataPath"] + "\\" + brandStr + ".xml";
                    DataAccess da = new DataAccess();
                    XmlDocument xmlDoc = da.GetXMLFromFile(filename);
                    
                    foreach (string countryCode in countries)
                    {
                        XmlNodeList linkElements = xmlDoc.SelectNodes("//country[@code='" + countryCode + "']/contentResources/links/link");
                        foreach (XmlNode linkNode in linkElements)
                        {
                            ContentElement contentElm = getLinkForXMLNode(linkNode, ContentType.Link);
                            contentElm.Brand = brand;
                            contentElm.Country = AvailabilityHelper.GetCountryCodeForString(countryCode);
                            if (brand == THLBrands.MAC)
                                macElementsList.Add(contentElm);
                            else
                                elmentsList.Add(contentElm);
                            // Nimesh
                            //tmpContentElm = contentElm;
                        }

                        XmlNodeList messageElements = xmlDoc.SelectNodes("//country[@code='" + countryCode + "']/contentResources/messages/message");
                        foreach (XmlNode messageNode in messageElements)
                        {
                            ContentElement contentElm = getMessageForXMLNode(messageNode, ContentType.HTML);

                            contentElm.Brand = brand;
                            contentElm.Country = AvailabilityHelper.GetCountryCodeForString(countryCode);
                            if (brand == THLBrands.MAC)
                                macElementsList.Add(contentElm);
                            else
                                elmentsList.Add(contentElm);
                        }
                    }
                }
                contentElements = elmentsList.ToArray();


                //-------------------------------------------------------------------------------------
                //---add mac support starts here (21/12/10)
                macElementsList = new List<ContentElement>();
                string macXmlfilename = System.Web.Configuration.WebConfigurationManager.AppSettings["LocalDataPath"] + "\\c.xml";
                DataAccess dal = new DataAccess();
                XmlDocument maCxmlDoc = dal.GetXMLFromFile(macXmlfilename);
                XmlNodeList macLinkElements = maCxmlDoc.SelectNodes("//company/links/link");
                foreach (XmlNode linkNode in macLinkElements)
                {
                    ContentElement macContentElm = getLinkForXMLNode(linkNode, ContentType.Link);//  new ContentElement();

                    XmlNode parentCompanyNode = linkNode.ParentNode.ParentNode;
                    XmlNode parentCountryNode = parentCompanyNode.ParentNode.ParentNode;

                    macContentElm.Brand = THLBrand.GetBrandForString(parentCompanyNode.Attributes["id"].Value.ToLower());
                    macContentElm.Country = AvailabilityHelper.GetCountryCodeForString(parentCountryNode.Attributes["code"].Value.ToLower());

                    macElementsList.Add(macContentElm);
                }
                macContentElements = macElementsList.ToArray();
                //---add mac support ends (21/12/10)
                //------------------------------------------------------------------------------------------

                return true;
            }
            catch (Exception ex)
            {
                string exMsg = ex.Message;
                //TODO: log point
                return false;
            }

        }

        /// <summary>
        /// Parse a content Element XML Node
        /// </summary>
        /// <param name="xmlNode">The Content Context to parse(Link/HTML)</param>
        /// <returns></returns>
        static ContentElement getLinkForXMLNode(XmlNode xmlNode, ContentType context)
        {
            System.Globalization.CultureInfo provider = System.Globalization.CultureInfo.InvariantCulture;
            string format = "dd/MM/yy";

            ContentElement contentElm = new ContentElement();
            contentElm.ContentType = ContentType.Link;
            //<link code="FerryCrossing" URL="http://www.maui.co.nz/ferry-ma-NZ.aspx"/>
            if (xmlNode.Attributes["code"] != null)
                contentElm.Code = xmlNode.Attributes["code"].Value;
            if (xmlNode.Attributes["url"] != null)
                contentElm.Content = (xmlNode.Attributes["url"].Value != null ? xmlNode.Attributes["url"].Value.Replace("http://", string.Empty) : string.Empty);
            contentElm.VehicleType = VehicleType.Both;
            if (xmlNode.Attributes["type"] != null)
            {
                switch (xmlNode.Attributes["type"].Value)
                {
                    case "ac":
                        contentElm.VehicleType = VehicleType.Car;
                        break;
                    case "av":
                        contentElm.VehicleType = VehicleType.Campervan;
                        break;
                }
            }
            contentElm.StartTime = (xmlNode.Attributes["startDate"] != null ? DateTime.ParseExact(xmlNode.Attributes["startDate"].Value, format, provider) : DateTime.ParseExact("01/01/00", format, provider));
            contentElm.EndTime = (xmlNode.Attributes["endDate"] != null ? DateTime.ParseExact(xmlNode.Attributes["endDate"].Value, format, provider) : DateTime.ParseExact("01/01/2050", "dd/MM/yyyy", provider));
            return contentElm;
        }

        static ContentElement getMessageForXMLNode(XmlNode xmlNode, ContentType context)
        {
            ContentElement contentElm = new ContentElement();
            contentElm.ContentType = ContentType.HTML;
            if (xmlNode.Attributes["code"] != null)
                contentElm.Code = xmlNode.Attributes["code"].Value;
            if (xmlNode != null && xmlNode.InnerText != null && xmlNode.InnerText.Length > 0)
                contentElm.Content = xmlNode.InnerText;
            return contentElm;
        }


        public string GetCardNameForCode(string cardCode)
        {
            //TODO: should this come from Aurora?
            string cardName = string.Empty;
            switch (cardCode)
            {
                case "Visa":
                    cardName = "Visa";
                    break;
                case "Mastercard":
                    cardName = "Mastercard";
                    break;
                case "Diners":
                    cardName = "Diners";
                    break;
                case "Amex":
                    cardName = "American Express";
                    break;
            }
            return cardName;
        }

        /// <summary>
        /// Deprecated
        /// </summary>
        /// <param name="cardType"></param>
        /// <returns></returns>
        public decimal GetSurchargeForCard(CreditCardType cardType)
        {
            //TODO: from BackEnd
            decimal surcharge = 0.0m;
            switch (cardType)
            {
                case CreditCardType.VISA:
                    surcharge = 2.0m;
                    break;
                case CreditCardType.MASTERCARD:
                    surcharge = 2.0m;
                    break;
                case CreditCardType.AMEX:
                    surcharge = 2.0m;
                    break;
            }
            return surcharge;
        }

        /// <summary>
        /// Return a Cached Tracking Element for provided Doamin and Event
        /// </summary>
        /// <param name="domainName"></param>
        /// <param name="eventName"></param>
        /// <returns></returns>
        public TrackingData GetTrackingElement(string domainName, string eventName)
        {
            foreach (TrackingData trackingElm in trackingDataElements)
            {
                if (trackingElm.Event.Equals(eventName) && trackingElm.Domain.Equals(domainName))
                    return trackingElm;
            }
            return null;//no match..
        }

        public Branch GetBranchForCode(string branchCode)
        {
            
            if(! string.IsNullOrEmpty(branchCode))
            {
                string normalizedBranch = branchCode.ToLower();
            //var nzCount = branches.Select(b => b.CountryCode.Equals("NZ")).FirstOrDefault();
            //if (! nzCount)
            //{
            //    //loadMasterXML();
            //    HttpContext.Current.Application["ApplicationData"] = new ApplicationData();
            //}
            foreach (Branch branch in branches)
                if (branch.BranchCode.ToLower().Equals(normalizedBranch) || branch.ZoneCode.ToLower().Equals(normalizedBranch))//TODO: refine query
                    return branch;
            }
            return null;
        }

        /// <summary>
        /// Return AppCache Address for a provided location CODE 
        /// </summary>
        /// <param name="locationCode"></param>
        /// <returns></returns>
        public string GetAddressForLocationCode(string locationCode)
        {
            try
            {
                Branch branchMatch = GetBranchForCode(locationCode);
                if (branchMatch != null)
                    return branchMatch.ZoneName;
                City cityMatch = GetCityForCode(locationCode);
                if (cityMatch != null)
                    return cityMatch.Name;
                return "No Address";//TODO: formalize
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Application, "ApplicationData:GetAddressForLocationCode", ex.Message, "{locationCode:" + locationCode + "}");
                return "No Address";
            }
        }

        /// <summary>
        /// Map a booking control code to its readable location string
        /// </summary>
        /// <param name="ctrlCode"></param>
        /// <returns></returns>
        public string GetLocationStringForCtrlCode(string ctrlCode)
        {
            try
            {
                string normCode = (ctrlCode != null ? ctrlCode.ToUpper() : string.Empty);
                if (CtrlBranchLabels != null && CtrlBranchLabels[normCode] != null)
                    return CtrlBranchLabels[normCode];
                else
                    return "n/a";
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Application, "ApplicationData:GetLocationStringForCtrlCode", ex.Message, "{ctrlCode:" + ctrlCode + "}");
                return "n/a";
            }
        }

        public City GetCityForCode(string cityCode)
        {
            string normalizedCityCode = cityCode.ToLower();
            foreach (City city in cities)
                if (city.Code.ToLower().Equals(normalizedCityCode))
                    return city;
            return null;
        }

        Branch[] loadBranches(XmlNodeList branchNodes)
        {
            List<Branch> branchList = new List<Branch>();
            foreach (XmlNode branchNode in branchNodes)
            {//process: <branch code="ATC" address1="Thrifty Office" address2="23 Hindley Street" phoneNumber="+61 1300 367 227" brandList="B M P"/>
                XmlNode zoneNode = branchNode.ParentNode;
                XmlNode cityNode = zoneNode.ParentNode.ParentNode;
                XmlNode countryNode = cityNode.ParentNode.ParentNode;
                Branch currentBranch = new Branch();
                currentBranch.BranchCode = branchNode.Attributes["code"].Value;
                currentBranch.Address = string.Empty;// branchNode.Attributes["address1"].Value; //removed 20/10/09 (passed on confirm booking).
                currentBranch.Address2 = string.Empty;// branchNode.Attributes["address2"].Value;
                currentBranch.PhoneNumber = branchNode.Attributes["phoneNumber"].Value;
                currentBranch.City = cityNode.Attributes["code"].Value;
                currentBranch.ZoneCode = zoneNode.Attributes["value"].Value;
                currentBranch.ZoneName = zoneNode.Attributes["name"].Value;
                currentBranch.CountryCode = countryNode.Attributes["code"].Value;
                currentBranch.VehicleTypeAvailability = (zoneNode.Attributes["type"].Value.Equals("AC") ? VehicleType.Car : VehicleType.Campervan);
                string[] brandCharList = branchNode.Attributes["brandList"].Value.Split(' ');
                List<THLBrands> brandList = new List<THLBrands>();
                foreach (string brandChar in brandCharList)
                {
                    brandList.Add(THLBrand.GetBrandForString(brandChar));
                }
                currentBranch.brands = brandList.ToArray();
                branchList.Add(currentBranch);
            }
            return branchList.ToArray();
        }

        City[] loadCities(XmlNodeList cityNodes)
        {
            List<City> citiesList = new List<City>();
            foreach (XmlNode cityNode in cityNodes)
            {
                City current = new City();
                if (current.LoadFromXml(cityNode))
                    citiesList.Add(current);//success parse
            }
            return citiesList.ToArray();
        }

        /// <summary>
        /// Populate the given vehicle list with content
        /// TODO: Exp handle
        /// </summary>
        /// <param name="vehicles"></param>
        /// <returns></returns>
        public static int LoadVehiclesContent(ref Vehicle[] vehicles)
        {
            int updatedNodes = 0;
            XmlDocument vehicleXml = new XmlDocument();
            try
            {

                string xmlStr = new DataAccess().GetStringForURL(ApplicationManager.VehicleContentFeedURL);
                vehicleXml.LoadXml(xmlStr);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                THLDebug.LogError(ErrorTypes.ExternalProvider, "ApplicationData.LoadVehiclesContent", msg, "{'url':'" + ApplicationManager.VehicleContentFeedURL + "'");
                return 0;// no vehicles updated as feed retrieval failed
            }

            if (vehicleXml != null && vehicleXml.SelectNodes("//vehicle") != null && vehicleXml.SelectNodes("//vehicle").Count > 0)
            {
                foreach (Vehicle vehicle in vehicles)
                {
                    //if (vehicle.Name.Equals("Nimesh"))
                    //{
                    //    VehicleContent vc = new VehicleContent();
                    //    vc.Description = "Nimesh Test Vehicle";
                    //    vc.Sleeps = 6;
                    //    vehicle.Content = vc;
                    //    continue;
                    //}
                    string currentCode = !string.IsNullOrEmpty(vehicle.Code) ? vehicle.Code.ToLower() : string.Empty;
                    XmlNode matchNode = vehicleXml.SelectSingleNode("//vehicle[@code='" + currentCode + "']");
                    if (matchNode != null)
                    {
                        VehicleContent contentElm = new VehicleContent(matchNode);
                        vehicle.Content = contentElm;
                        updatedNodes++;
                    }
                }
            }
            return updatedNodes;
        }

        /// <summary>
        /// Match Vehicle Content for Code
        /// TODO: Exp handle
        /// </summary>
        /// <param name="vehicleCode"></param>
        /// <returns></returns>
        public VehicleContent GetContentForVehicle(string vehicleCode)
        {
            for (int i = 0; i < Vehicles.Length; i++)
            {
                string currentCode = Vehicles[i].Code.ToLower().Split('.')[1];
                if (vehicleCode.ToLower().Equals(currentCode))
                    return (Vehicles[i].Content != null) ? Vehicles[i].Content : new VehicleContent();
            }
            return new VehicleContent();
        }
    }
}