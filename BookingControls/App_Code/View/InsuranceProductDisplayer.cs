﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace THL.Booking
{
    /// <summary>
    /// Displayer Functionality for Insurance Product
    /// </summary>
    public class InsuranceProductDisplayer
    {
        InsuranceProduct[] insuranceProductOptions;
        AvailabilityItem availabilityItem;
        AvailabilityRequest _availabilityrequest;
        string currencyCode;
        int hirePeriod;


        public string ContainerBrand;


        public InsuranceProductDisplayer(InsuranceProduct[] _insuranceProductOptions, AvailabilityItem _availabilityItem, string _currencyCode, AvailabilityRequest availabilityRequest)
        {
            availabilityItem = _availabilityItem;
            hirePeriod = availabilityItem.GetHirePeriod();
            insuranceProductOptions = _insuranceProductOptions;
            currencyCode = _currencyCode;
            _availabilityrequest = availabilityRequest;
        }

        public static string renderAgeSelector(InsuranceProduct[] products)
        {
            int minAge = 18, maxAge = 21;
            foreach (InsuranceProduct product in products)
            {
                if (product.MinAge > minAge) minAge = product.MinAge;
                if (product.MaxAge > maxAge) maxAge = product.MaxAge;
            }

            string leftMsg = (minAge == 18 ? "18-21" : "less then 25");
            string rightMsg = (maxAge == 21 ? "21+" : "25+"); 
            
            StringBuilder sb = new StringBuilder();
            sb.Append(@"
                <span id='insuranceAgeSelector' class='AgeSelector'>Age: <input type='radio' name='ageRadio' value='18+'/>" + leftMsg + "<input type='radio' name='ageRadio' value='21+' checked='checked'/>"+ rightMsg +"</span>");
            return sb.ToString();

        }

        //Added to support responsive 
        public string RenderResponsive(bool isInclusiveSelected)
        {
            VehicleType contentResourceVehicleType = _availabilityrequest.VehicleType;
            if (availabilityItem.VehicleCode.ToLower().Contains("pfmr")) contentResourceVehicleType = VehicleType.Campervan;//Added exception 08/01/10 for PFMR

            string insuranceHeaderURL = ContentManager.GetLinkElementForBrand(availabilityItem.Brand, contentResourceVehicleType, _availabilityrequest.CountryCode, "InsuranceOptions", _availabilityrequest.PickUpDate, THLBrands.AIRNZ).Content;
            string ageHeaderURL = ContentManager.GetLinkElementForBrand(availabilityItem.Brand, contentResourceVehicleType, _availabilityrequest.CountryCode, "InsuranceAge", _availabilityrequest.PickUpDate, THLBrands.AIRNZ).Content;

            StringBuilder sb = new StringBuilder();
            sb.Append(@"
        <div id=""insurancePanel"" " + (isInclusiveSelected ? "class=\"InclusiveOn sections\"" : "class=\"sections\"") + @">
            <div class=""sectionHeader"">
                <h4>Reduce your Accident Liability & Security Deposit:&nbsp;</h4>
                <a href=""http://" + insuranceHeaderURL + @""" class=""Quest PopUp"">?</a>                
                <a style=""display:none;"" href=""http://" + ageHeaderURL + @""" class=""Quest PopUp"">?</a>
                <span id=""inclusiveSelectedTxt"">" + "The maximum excess reduction option is included in the inclusive package you have selected above" + @"</span>
                <span class=""openclose""><i class=""icon-plus""></i></span>
            </div>
            <div class=""sectionContent"">
                " + InsuranceProductDisplayer.renderAgeSelector(insuranceProductOptions) + @"
                <table id=""insOptionsTable"" class=""Over21"">");
            foreach (InsuranceProduct insuranceProduct in insuranceProductOptions)
            {
                bool lessThen21 = (insuranceProduct.MinAge == 21 || insuranceProduct.MinAge == 18);
                bool hasRequiredActions = (insuranceProduct.RequiredActions != null);
                bool isInclusive = insuranceProduct.IsInclusive;
                bool perDayPrice = insuranceProduct.UOM == UOMType.Day;

                // commented by nimesh
//                sb.Append(@"
//                    <tr rel=""" + insuranceProduct.Code + @""" class=""" + (insuranceProduct.IsInclusive ? " IsInclusive " : string.Empty) + (lessThen21 ? " Under21" : " Over21") + @""">
//                        <td class='cb'>
//                            <input id='ipi_" + insuranceProduct.Code + "' " + /*(hasRequiredActions ? "disabled='disabled'" : string.Empty) +*/  " type='" + (hasRequiredActions && !isInclusive ? "checkbox" : "radio") + "' name='insuranceOption' value='ipi_" + insuranceProduct.SapId + "' " + ((isInclusiveSelected && isInclusive) ? "checked='checked' " : string.Empty) + @" />
//                        </td>
//                        <td class='desc'><small>" + insuranceProduct.Name + "</small></td>");
                // end commented by nimesh
                // added by nimesh to fetch link for insurance product
                sb.Append(@"
                    <tr rel=""" + insuranceProduct.Code + @""" class=""" + (insuranceProduct.IsInclusive ? " IsInclusive " : string.Empty) + (lessThen21 ? " Under21" : " Over21") + @""">
                        <td class='cb'>
                            <input id='ipi_" + insuranceProduct.Code + "' " + /*(hasRequiredActions ? "disabled='disabled'" : string.Empty) +*/  " type='" + (hasRequiredActions && !isInclusive ? "checkbox" : "radio") + "' name='insuranceOption' value='ipi_" + insuranceProduct.SapId + "' " + ((isInclusiveSelected && isInclusive) ? "checked='checked' " : string.Empty) + @" />
                        </td>
                        <td class='desc'><a class='QuestText PopUp' href='http://" + insuranceHeaderURL + "'>" + insuranceProduct.Name + "</a><a class='Quest PopUp' href='http://" + insuranceHeaderURL + "'>?</a></td>");
                //end added by nimesh to fetch link for insurance product
                if (perDayPrice)
                    sb.Append(@"
                        <td class=""days"">" + (isInclusive ? "Included" : hirePeriod + " day(s)") + "</td>");
                else
                    sb.Append(@"
                        <td class=""days"">" + (isInclusive ? "Included" : "&nbsp;") + "</td>");
                string priceStr = (isInclusive ? "Included" : "$ " + insuranceProduct.DailyRate.ToString()) + (perDayPrice ? " per day" : " each");
                if (insuranceProduct.DailyRate == 0) priceStr = "Included";//inclusive insurance, set no price
                sb.Append(@"
                        <td class=""price"">" + priceStr + @"</td>
                        <td class=""total""></td>
                    </tr>");
            }
            sb.Append(@"
                </table>
                <span>Your drivers license and credit card will be required for the security bond or imprint when you collect your vehicle.</span>
            </div>    
        </div>");
            return sb.ToString();
        }

        public string Render(bool isInclusiveSelected)
        {

            VehicleType contentResourceVehicleType = _availabilityrequest.VehicleType;
            if (availabilityItem.VehicleCode.ToLower().Contains("pfmr")) contentResourceVehicleType = VehicleType.Campervan;//Added exception 08/01/10 for PFMR

            string insuranceHeaderURL = ContentManager.GetLinkElementForBrand(availabilityItem.Brand, contentResourceVehicleType, _availabilityrequest.CountryCode, "InsuranceOptions", _availabilityrequest.PickUpDate, THLBrands.AIRNZ).Content;
            string ageHeaderURL = ContentManager.GetLinkElementForBrand(availabilityItem.Brand, contentResourceVehicleType, _availabilityrequest.CountryCode, "InsuranceAge", _availabilityrequest.PickUpDate, THLBrands.AIRNZ).Content;
            
            StringBuilder sb = new StringBuilder();
            sb.Append("<div id='insurancePanel' " + (isInclusiveSelected ? "class='InclusiveOn section'" : "class='section'") + ">");
            sb.Append("<h4>Reduce your Accident Liability & Security Deposit:&nbsp;</h4>");
            sb.Append("<a href='http://" + insuranceHeaderURL + "' class='Quest PopUp'>?</a>");
            
            //sb.Append("<span id='insuranceAgeSelector' class='AgeSelector'>Age: <input type='radio' name='ageRadio' value='18+'/>less then 25<input type='radio' name='ageRadio' value='21+' checked='checked'/> 25+ </span>");
            sb.Append(InsuranceProductDisplayer.renderAgeSelector(insuranceProductOptions));

            sb.Append("<a style='display:none;' href='http://" + ageHeaderURL + "' class='Quest PopUp'>?</a>");
            sb.Append("<span id='inclusiveSelectedTxt'>" + "The maximum excess reduction option is included in the inclusive package you have selected above" + "</span>");
            sb.Append("<table id='insOptionsTable' class='Over21'>");
            foreach (InsuranceProduct insuranceProduct in insuranceProductOptions)
            {
                bool lessThen21 = (insuranceProduct.MinAge == 21 || insuranceProduct.MinAge == 18);
                bool hasRequiredActions = (insuranceProduct.RequiredActions != null);
                bool isInclusive = insuranceProduct.IsInclusive;
                bool perDayPrice = insuranceProduct.UOM == UOMType.Day ;
                sb.Append("<tr rel='" + insuranceProduct.Code + "' class='" + (insuranceProduct.IsInclusive ? " IsInclusive " : string.Empty) + (lessThen21 ? " Under21" : " Over21") + "'>");
                sb.Append("<td class='cb'><input id='ipi_" + insuranceProduct.Code + "' " + (hasRequiredActions ? "disabled='disabled'" : string.Empty) + " type='" + (hasRequiredActions && !isInclusive ? "checkbox" : "radio") + "' name='insuranceOption' value='ipi_" + insuranceProduct.SapId + "' "+ ((isInclusiveSelected && isInclusive) ? "checked='checked' " : string.Empty ) +" /></td>");
                sb.Append("<td class='desc'><small>"+ insuranceProduct.Name +"</small></td>");
                
                if(perDayPrice)
                    sb.Append("<td class='days'>" + (isInclusive ? "Included" : hirePeriod + " day(s)") + "</td>");
                else
                    sb.Append("<td class='days'>" + (isInclusive ? "Included" : "&nbsp;") + "</td>");

                string priceStr = (isInclusive ? "Included" : "$ " + insuranceProduct.DailyRate.ToString()) + (perDayPrice ? " per day" : " each");
                if (insuranceProduct.DailyRate == 0) priceStr = "Included";//inclusive insurance, set no price

                sb.Append("<td class='price'>" + priceStr + "</td>");
                sb.Append("<td class='total'></td>");
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            sb.Append("<span>Your drivers license and credit card will be required for the security bond or imprint when you collect your vehicle.</span>");
            sb.Append("</div>");
            return sb.ToString();
        }

        List<string> getSelectedInsuranceItemIds(bool isInclusiveSelected) 
        { 
            List<string> selectedIdsList = new List<string>();
            int i=0;//currenttlly default to returning the first item or the inclusive one if selected.. 
            foreach (InsuranceProduct item in insuranceProductOptions)
            {
                if ((item.IsInclusive && isInclusiveSelected))
                    selectedIdsList.Add(item.Code);
                i++;
            }
            return selectedIdsList;
        }
                
        public string RenderJSON(bool isInclusiveSelected)
        {
            List<string> selectedIDsList = getSelectedInsuranceItemIds(isInclusiveSelected);            
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            foreach (InsuranceProduct item in insuranceProductOptions)
            {
                bool setSelected = selectedIDsList.Contains(item.Code) || (isInclusiveSelected && item.IsInclusive) ;                
                sb.Append("{");
                sb.Append("type:'ins',id:'" + item.SapId + "',name:'" + item.Name + "', code:'"+ item.Code + "',maxCharge:" + item.MaxCharge + ", dailyRate :" + item.DailyRate + ",minAge:" + item.MinAge + ",maxAge:" + item.MaxAge + ", promptQuant: '" + item.PromptQuantity + "', numItems:"+ (setSelected ? "1" : "0") +", charge: "+ item.GrossAmount +", maxCharge:"+ item.MaxCharge );
                if (item.RequiredActions != null && item.RequiredActions.Count > 0) 
                {//if has required Actions render:
                    sb.Append(",requiredActions:[");
                    string requiredActionStr = string.Empty;
                    foreach (string key in item.RequiredActions)
                        requiredActionStr += "'"+ key +":"+ item.RequiredActions[key] +"',";
                    sb.Append(requiredActionStr.TrimEnd(','));
                    sb.Append("]");
                }
                sb.Append("},");
            }
            string result = sb.ToString().TrimEnd(',') + "]";
            return result;
        }
        
        InsuranceProduct getBonusPackProduct() {
            foreach (InsuranceProduct currentInsurance in insuranceProductOptions)
                if (currentInsurance.IsInclusive)
                    return currentInsurance;
            return null;
        }

        public string RenderAllInlusivePanel(string avpId, AvailabilityItem bonusPackItem, bool hasBonusPackAvailable, string packageName, decimal dailyRate, decimal totalPrice, bool hasDieselRecoveryPrice, string vCode, PackageManager pm)//TODO: consider refactoring of Package Properties
        {

            InsuranceProduct[] incAddOns = new InsuranceProduct[] { };// swap with: pm.GetInclusiveAddOns();

            //PFMR workaround
            VehicleType contentResourceVehicleType = _availabilityrequest.VehicleType;
            if (vCode.ToLower().Contains("pfmr")) contentResourceVehicleType = VehicleType.Campervan;//Added exception 08/01/10 for PFMR
                       
            string dtrURL = ContentManager.GetLinkElementForBrand(availabilityItem.Brand, contentResourceVehicleType, _availabilityrequest.CountryCode, "DTRLink", _availabilityrequest.PickUpDate, THLBrands.AIRNZ).Content;
            string inclusiveURL = ContentManager.GetLinkElementForBrand(availabilityItem.Brand, contentResourceVehicleType, _availabilityrequest.CountryCode, "AllInclusive", _availabilityrequest.PickUpDate, THLBrands.AIRNZ).Content;
            
            InsuranceProduct bonusPackOption = getBonusPackProduct();
            StringBuilder sb = new StringBuilder();
            bool bonusPackSelected = (bonusPackItem != null);
            string bounusPackDayPrice = (hasBonusPackAvailable && bonusPackOption != null ? dailyRate.ToString() : string.Empty);

            string bonusPackName = (hasBonusPackAvailable && bonusPackOption != null ? packageName : string.Empty);

            //string dieselRecovery = dieselRecoveryPrice.ToString("F");
            decimal bonusPackTotal = (hasBonusPackAvailable && bonusPackSelected && bonusPackOption != null) ? totalPrice : 0;            
            
            sb.Append("<div id='allInclusivePanel'>");  
            sb.Append("<table>");
            sb.Append("<tbody>");

            if (hasDieselRecoveryPrice)
            {
                sb.Append("<tr>");
                sb.Append("<td class='cb'>");
                sb.Append("</td>");
                sb.Append("<td class='desc'>");
                sb.Append("<small>" + pm.RoadChargeTitle  +"</small><a class='Quest PopUp' href='http://" + dtrURL + "'>?</a>");
                sb.Append("</td>");
                sb.Append("<td class='days'>");
                sb.Append("</td>");
                sb.Append("<td class='pr'>");
                sb.Append("<small>Paid on vehicle return</small>");
                sb.Append("</td>");
                sb.Append("<td class='note'>");
                sb.Append("<small></small>");
                sb.Append("</td>");
                sb.Append("</tr>");
            }

            sb.Append("<tr"+ (hasBonusPackAvailable ? string.Empty : " style='display:none;'") +">");
            sb.Append("<td class='cb'>");
            sb.Append("<input type='checkbox' id='allInclusivePkg' "+ (bonusPackSelected ? "checked='checked' " : string.Empty) +" onClick=\"renderInclusive(this,'" + avpId + "')\"/>");
            sb.Append("<input type='hidden' id='hasBonusPack' name='hasBonusPack' value='" + (bonusPackSelected ? "1" : "0") + "' />");
            sb.Append("</td>");
            sb.Append("<td class='desc'>");
            sb.Append("<small>" + bonusPackName + "</small><a class='Quest PopUp' href='http://" + inclusiveURL + "'>?</a>");
            sb.Append("</td>");
            sb.Append("<td class='days'>");
            sb.Append("<small>" + hirePeriod + " days(s)</small>");
            sb.Append("</td>");
            sb.Append("<td class='pr'>");
            sb.Append("<small>$" + bounusPackDayPrice + " per day</small>");
            sb.Append("</td>");
            sb.Append("<td class='total'>");
            sb.Append("<small>" + (bonusPackSelected ? currencyCode + " $ " + String.Format("{0:N2}",bonusPackTotal) : string.Empty)  + "</small>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</tbody></table>");
            sb.Append("<div id='allInclusiveDetails'>" + (bonusPackSelected ? getBonusPackPanel(bonusPackItem, incAddOns) : string.Empty) + "</div>");
            sb.Append("</div>");           
            return sb.ToString();
        }

        string getBonusPackPanel(AvailabilityItem ai, InsuranceProduct[] incAddOns)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            sb.Append("<tr>");
            sb.Append("<td colspan='4'>" + getNoteHTML(ai.Note) + "</td>");
            sb.Append("</tr>");

            //-- added for Inc AddOns
            /*
            foreach (InsuranceProduct ins in incAddOns)
                sb.Append(@"
<tr rel=""" + ins.Code +@""">
    <td class=""cb"">
        <input type=""checkbox"" value=""ipi_" + ins.SapId + @""" name=""insuranceOption"" id=""ipi_" + ins.Code + @""" />
    </td>
    <td class=""desc""><small>"+ ins.Name +@"</small></td>
    <td class=""days"">&nbsp;</td>
    <td class=""price"">$ "+ ins.GrossAmount + @" each</td><td class=""total""></td>
</tr>
                ");           
           */ 
            //-- added for Inc AddOns

            sb.Append("</table>");
            return sb.ToString();
        }

        /// <summary>
        /// Convert Aurora Note (* separated) to HTML List
        /// </summary>
        /// <param name="noteString"></param>
        /// <returns></returns>
        string getNoteHTML(string noteString)
        {
            StringBuilder sb = new StringBuilder("<ul>");
            string[] noteParams = noteString.Split('*');
            foreach (string noteParam in noteParams)
            {
                sb.Append("<li>"+ noteParam +"</li>");
            }
            sb.Append("</ul>");
            return sb.ToString();
        }
                
        public string RenderAllInlusiveJSON()
        {
            string bounusPackDayPrice = "0.0";
            StringBuilder sb = new StringBuilder();
            sb.Append("{bonusPack:" + bounusPackDayPrice + "}");
            return sb.ToString();
        } 
   
        /// <summary>
        /// Serialize insurance product into JSON format
        /// </summary>
        /// <param name="insProduct"></param>
        /// <returns></returns>
        public static string SerializeInsuranceProduct(InsuranceProduct insProduct)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{product:" + insProduct.ToString() );
            sb.Append(",isInclusive:" + insProduct.IsInclusive);
            
            if (insProduct.RequiredActions != null && insProduct.RequiredActions.Count > 0) 
            {
                sb.Append(",dependancies:[");
                foreach (string key in insProduct.RequiredActions.AllKeys)
                    sb.Append(key + ":" + insProduct.RequiredActions[key] + ",");
                sb.Append("]");                
            }
            sb.Append("}"); 
            return sb.ToString();
        }

        /// <summary>
        /// Used for retrieve quote functionality within the ConfigureQuote View 
        /// </summary>
        /// <param name="inclusiveSelected"></param>
        /// <returns>HTML Representation of Configure Quote Insurance panel</returns>
        public static string RenderInsuranceQuotePanel(InsuranceProduct[] insuranceProductOptions, InsuranceProduct[] selectedOptions, AvailabilityRequest availabilityRequest, bool isInclusiveSelected)
        {
            THLBrands currentBrand = THLBrand.GetBrandForString(availabilityRequest.Brand);
            VehicleType contentResourceVehicleType = availabilityRequest.VehicleType;
            if (availabilityRequest.VehicleModel.ToLower().Contains("pfmr")) contentResourceVehicleType = VehicleType.Campervan;//TODO: verify this line Added exception 08/01/10 for PFMR

            string insuranceHeaderURL = ContentManager.GetLinkElementForBrand(currentBrand, contentResourceVehicleType, availabilityRequest.CountryCode, "InsuranceOptions", availabilityRequest.PickUpDate, THLBrands.AIRNZ).Content;
            string ageHeaderURL = ContentManager.GetLinkElementForBrand(currentBrand, contentResourceVehicleType, availabilityRequest.CountryCode, "InsuranceAge", availabilityRequest.PickUpDate, THLBrands.AIRNZ).Content;
            string inclusiveURL = ContentManager.GetLinkElementForBrand(currentBrand, contentResourceVehicleType, availabilityRequest.CountryCode, "AllInclusive", availabilityRequest.PickUpDate, THLBrands.AIRNZ).Content;
            string currencyCode = CurrencyManager.GetCurrencyForCountryCode(availabilityRequest.CountryCode).ToString();

            decimal hirePeriod = (selectedOptions != null && selectedOptions.Length > 0) ? selectedOptions[0].HirePeriod : availabilityRequest.GetHirePeriod();//This resolve no hire period returns on ER0
            
            string availableOptionsJSON = "[";
            foreach (InsuranceProduct ins in insuranceProductOptions)
                availableOptionsJSON += "{'sap':'" + ins.SapId + "', 'uom':'" + ins.UOM + "','grsAmt':" + ins.GrossAmount + ",'waive':" + (String.IsNullOrEmpty(ins.WaivedProducts) ?  "''" : "'" + ins.WaivedProducts + "'" )+ "},";
            availableOptionsJSON = availableOptionsJSON.TrimEnd(',');
            availableOptionsJSON += "]";

            StringBuilder sb = new StringBuilder();

            decimal totalInsAmount = 0.0m;
            
            string sapCollection = "[";

            bool under21 = false;

            foreach (InsuranceProduct insOption in selectedOptions)
            {
                totalInsAmount += insOption.GrossAmount;
                sapCollection += "'" + insOption.SapId + "',";
                if (insOption.MinAge == 21 || insOption.MinAge == 18) under21 = true;

            }
            sapCollection = sapCollection.TrimEnd(',') + "]";
            sb.Append(@"
            <script>
                var insuranceOptions = {'cur':'" + currencyCode + @"','total': " + totalInsAmount + @" ,'selected':" + sapCollection + @"};
                var optionalInsOptions = " + availableOptionsJSON + @";

                //
                jQuery(document).ready(
                    function() {
                        $j(insuranceOptions.selected).each(function(i,el) { $j('#ipi_' + el).attr('checked',true) ; });      
                });    
                //
 
            </script>
            ");            
            // ----------- Render Insurance JSON ends --------

            if (isInclusiveSelected)
            {
                InsuranceProduct inclusiveItem = selectedOptions[0];//TODO: verify biz rule for only one insurance when bonus pack
                sb.Append(@"
            <div id='insurancePanel' class='sections'>
                <div id='allInclusivePanel'>
                    <div id='allInclusiveDetails'>
                        <table style='background-color:inherit;'>
                            <tbody>
                                <tr>
                                    <td class='cb'>
                                        <input type='hidden' value='1' name='hasBonusPack' id='hasBonusPack'>
                                    </td>
                                    <td class='desc'>
                                        <small>" + inclusiveItem.Name +@"</small>
                                        <a href='http://"+ inclusiveURL + @"' class='Quest PopUp'>?</a>
                                    </td>
                                    <td class='days'>
                                        <small>" + hirePeriod  +@" days(s)</small>
                                    </td>
                                    <td class='pr'>
                                        <small>$"+ String.Format("{0:N2}",inclusiveItem.DailyRate) +@" per day</small>
                                    </td>
                                    <td class='total'>
                                        <small>" + currencyCode + " " + String.Format("{0:N2}", hirePeriod * inclusiveItem.DailyRate) + @"</small>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <ul>
                ");
                string[] noteParams = selectedOptions[0].Note.Split('*');
                foreach (string noteParam in noteParams)
                    sb.Append("<li>" + noteParam + "</li>");
                sb.Append("</ul>");
                sb.Append("</td></tr></tbody></table>");
                sb.Append("<span id='inclusiveQuoteSelectedTxt'>" + "The maximum excess reduction option is included in the inclusive package you have selected above" + "</span>");
                sb.Append("<input style='display:none;' id='ipi_" + selectedOptions[0].SapId + "' type='radio' name='insuranceOption' value='ipi_" + selectedOptions[0].SapId + "' checked='checked' />");

                sb.Append("</div></div></div>");
            }
            else
            {
                sb.Append(@"
            <div id='insurancePanel'  class='sections'>
<div class='sectionHeader'>
                        <h4>Reduce your Accident Liability & Security Deposit:&nbsp;</h4>
                        <a href='http://" + insuranceHeaderURL + @"' class='Quest PopUp'>?</a>
                        <span class='openclose'><i class='icon-plus'></i></span>
</div>
<div class='sectionContent'>" + renderAgeSelector(insuranceProductOptions) 
                  /* +"<span id='insuranceAgeSelector' class='AgeSelector'>Age: <input type='radio' name='ageRadio' value='18+' " + (under21 ? "checked='checked'" : "") + @"/>less then 25<input type='radio' name='ageRadio' value='21+' " + (under21 ? "" : "checked='checked'") + @" /> 25+ </span>"*/ 
                  + @"<a style='display:none;' href='" + ageHeaderURL + @" class='Quest PopUp'>?</a>
                <span id='inclusiveSelectedTxt'>The maximum excess reduction option is included in the inclusive package you have selected above</span>
                <table id='insOptionsTable' class='" + (under21 ? "Under21" : "Over21") 
                + @"'>");

                
                //dig out the isInclusive option
                List<InsuranceProduct> insOptionsList = new List<InsuranceProduct>(insuranceProductOptions);
                InsuranceProduct inclusiveItem =  insOptionsList.Find(delegate(InsuranceProduct ins) {return ins.IsInclusive == true; });

                if (inclusiveItem != null)
                {
                    InsuranceProduct insuranceProduct = inclusiveItem;
                    bool perDayPrice = insuranceProduct.UOM == UOMType.Day;
                    bool lessThen21 = (insuranceProduct.MinAge == 21);
                    sb.Append("<tr class='" + /*(insuranceProduct.IsInclusive ? " IsInclusive " : string.Empty) +*/ (lessThen21 ? " Under21" : " Over21") + "'>");
                    sb.Append("<td class='cb'><input id='ipi_" + insuranceProduct.SapId + "'  type='radio' name='insuranceOption' value='ipi_" + insuranceProduct.SapId + "' rel='"+ insuranceProduct.Code +"' /></td>");
                    sb.Append("<td class='desc'><small style='float:left;'>" + insuranceProduct.Name + "</small><a href='http://" + inclusiveURL + @"' class='Quest PopUp'>?</a>" + "</td>");

                    if (perDayPrice)
                        sb.Append("<td class='days'>" + hirePeriod + " day(s)" + "</td>");
                    else
                        sb.Append("<td class='days'>" + "&nbsp;" + "</td>");

                    string priceStr = "$ " + insuranceProduct.DailyRate.ToString() + (perDayPrice ? " per day" : " each");
                    if (insuranceProduct.DailyRate == 0) priceStr = "Included";//inclusive insurance, set no price

                    sb.Append("<td class='price'>" + priceStr + "</td>");
                    sb.Append("<td class='total'></td>");
                    sb.Append("</tr>");
                    sb.Append("<tr class=' content " + insuranceProduct.Code + "' style='display:none;'><td colspan='4'><ul style='display: block; margin: 10px;'>");
                    string[] noteParams = insuranceProduct.Note.Split('*');
                    foreach (string noteParam in noteParams)
                        sb.Append("<li>" + noteParam + "</li>");
                    sb.Append("</ul>");                        
                    sb.Append("</td></tr>");
                }

                foreach (InsuranceProduct insuranceProduct in insuranceProductOptions)
                {
                    bool lessThen21 = (insuranceProduct.MinAge == 21 || insuranceProduct.MinAge == 18);
                    bool hasRequiredActions = (insuranceProduct.RequiredActions != null && insuranceProduct.RequiredActions.Count > 0);
                    bool isInclusive = insuranceProduct.IsInclusive;

                    if (isInclusive) continue;//implemented above 

                    bool perDayPrice = insuranceProduct.UOM == UOMType.Day;
                    sb.Append("<tr class='" + /*(insuranceProduct.IsInclusive ? " IsInclusive " : string.Empty) +*/ (lessThen21 ? " Under21" : " Over21") + "'>");
                    sb.Append("<td class='cb'><input id='ipi_" + insuranceProduct.SapId + "' " + (hasRequiredActions ? "disabled='disabled'" : string.Empty) + " type='" + (hasRequiredActions && !isInclusive ? "checkbox" : "radio") + "' name='insuranceOption' value='ipi_" + insuranceProduct.SapId + "' " + ((insuranceProduct.Rating == 0 && !isInclusiveSelected) ? "checked='checked' " : string.Empty) + " rel='" + insuranceProduct.Code + getDependanciesCodesForInsurance(insuranceProduct, ' ') + "' class='"+ (insuranceProduct.MinAge == 18 ? "Under21" : "Over21")  +"'/></td>");
                    sb.Append("<td class='desc'><small>" + insuranceProduct.Name + "</small></td>");

                    if (perDayPrice)
                        sb.Append("<td class='days'>" +  hirePeriod + " day(s)" + "</td>");
                    else
                        sb.Append("<td class='days'>" +  "&nbsp;" + "</td>");

                    string priceStr =  "$ " + insuranceProduct.DailyRate.ToString() + (perDayPrice ? " per day" : " each");
                    if (insuranceProduct.DailyRate == 0) priceStr = "Included";//inclusive insurance, set no price

                    sb.Append("<td class='price'>" + priceStr + "</td>");
                    sb.Append("<td class='total'></td>");
                    sb.Append("</tr>");                    
                }

                sb.Append(@"
                </table>
                <span>Your drivers license and credit card will be required for the security bond or imprint when you collect your vehicle.</span>
            </div>
</div>
            ");
            }
            return sb.ToString();
        }

        public static string getDependanciesCodesForInsurance(InsuranceProduct insuranceProduct, char separator)
        {
            if (insuranceProduct.RequiredActions != null && insuranceProduct.RequiredActions.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (string keyStr in insuranceProduct.RequiredActions.Keys)
                    sb.Append(separator + keyStr);//support only DISPLAYWHEN
                return sb.ToString();
            }
            else return string.Empty;
        }
    }
}