﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace THL.Booking
{
    /// <summary>
    /// Displayer Functionality for Ferry Products
    /// </summary>
    public class FerryProductDisplayerBM
    {

        DateTime pickUpDate;
        DateTime dropOffDate;

        string currencyCode;
        
        FerryProduct[] ferryProductCollection;
        bool hasProducts = false;
        public FerryProductDisplayerBM(FerryProduct[] _products, DateTime _pickUpDate, DateTime _dropOffDate, string _currencyCode)
        {
            ferryProductCollection = _products;
            pickUpDate = _pickUpDate;
            dropOffDate = _dropOffDate;
            hasProducts = (_products != null && _products.Length > 0);
            currencyCode = _currencyCode;
        }

        public string RenderFerryPanel() { 
            StringBuilder sb = new StringBuilder();
            if (hasProducts)
            {
                sb.Append("<h4>Ferry crossing:</h4>");
                sb.Append("<input type='hidden' id='paxCounter' name='paxCounter' />");
                string[] directions = { "wlg_pct", "pct_wlg" };
                string[] directionTitles = { "Ferry - Wellington to Picton (North to South)", "Ferry - Picton to Wellington (South to North) - For travel 05 Jan 2015 - 15 Jan 2015 from Piction to Wellington,<br> reservation team will contact you to confirm availability and apply FREE ferry offer" };
                for(int i=0; i< 2; i++) 
                {
                    sb.Append(renderOneWay(directions[i], directionTitles[i]));
                }
            }
            return sb.ToString();
        }
        
        public string RenderJSON()
        {
            FerryProduct paxItem = null;
            FerryProduct vehItem = null;
            if (ferryProductCollection != null && ferryProductCollection.Length > 0)
            {
                foreach (FerryProduct current in ferryProductCollection)
                {
                    if(current.Code.Contains("PAX"))
                        paxItem = current;
                    else if(current.Code.Contains("VEH"))
                        vehItem = current;               
                }
                if (paxItem == null || vehItem == null)
                    return string.Empty;//not enough info for generating panel, TODO: inform presentation layer.
                PersonRateInfo[] paxRates = paxItem.FerryData.PersonRateCollection;
                StringBuilder sb = new StringBuilder();
                sb.Append("{");
                string ratesJSON = string.Empty;
                string ratesSapIds = string.Empty;
                foreach (PersonRateInfo personRate in paxRates)
                {
                    ratesJSON += personRate.PersonInfoType.ToString() + ":" + personRate.Rate + ",";
                    ratesSapIds += "," + personRate.PersonInfoType.ToString() + ":'" + personRate.SapId + "'";
                }
                ratesJSON = ratesJSON.TrimEnd(',');
                sb.Append("pct_wlg:{selected:false,vehicle:" + vehItem.DailyRate + "," + ratesJSON + "},");
                sb.Append("wlg_pct:{selected:false," + ratesJSON + "},");
                sb.Append("total:0,");
                sb.Append("sapIds:{pct_wlg:'" + vehItem.SapId + "', wlg_pct:'" + vehItem.SapId + "'" + ratesSapIds + "}");
                sb.Append("}");
                return sb.ToString();
            }
            else
                return "[]";                
        }
        
        string renderOneWay(string directionId, string directionTitle) {
            FerryProduct paxItem = null;
            FerryProduct vehItem = null;
            foreach(FerryProduct current in ferryProductCollection) {
                if(current.Code.Contains("PAX"))           
                    paxItem = current;
                else if(current.Code.Contains("VEH"))
                    vehItem = current;               
            }
            if(paxItem == null || vehItem == null)
                return string.Empty;//not enough info for generating panel, TODO: inform presentation layer.
              
            
            StringBuilder sb = new StringBuilder();            
            sb.Append("<table class='vehicleTable'>");
            sb.Append("<tr>");
            sb.Append("<td class='cb' style='width: 80px'>");
            sb.Append("<input type='checkbox' value='" + vehItem.DailyRate + "' id='" + directionId + "_" + vehItem.SapId + "' name='" + directionId + "_" + vehItem.SapId + "' />");
            sb.Append("</td>");
            sb.Append("<td class='desc' colspan='2'>");
            sb.Append("<small style='margin-left:0px;'>" + directionTitle + "</small>");
            sb.Append("</td>");
            sb.Append("<td class='total' style='width: 110px'>");
            sb.Append("<small>"+ currencyCode + " $ " + vehItem.DailyRate + "</small>");
            sb.Append("</td>");            
            
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<table class='" + directionId + "_" + vehItem.SapId + " Hide'>");
            sb.Append("<tr>");
            sb.Append("<td class='cb'></td>");
            sb.Append("<td class='desc'><small>Date of crossing:</small></td>");
            sb.Append("<td class='time' colspan='2'>");
            sb.Append(getCrossingDropDown(directionId));
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td class='cb'></td>");
            sb.Append("<td class='desc'><small>Preferred time:</small></td>");
            sb.Append("<td class='time' colspan='2'>");
            sb.Append(getCrossingTimeDropDown(directionId));
            sb.Append("</td>");
            sb.Append("</tr>");                      
            PersonRateInfo[] paxRates = paxItem.FerryData.PersonRateCollection;
            foreach(PersonRateInfo personRate in paxRates)
                sb.Append(getPersonRow(personRate, directionId));
            sb.Append("<tr>");
            sb.Append("<td class='cb'></td>");
            sb.Append("<td colspan='3'>");
            sb.Append("<small>* Actual departure time is subject to ferry timetables.</small>");
            sb.Append("</td>");                       
            sb.Append("</tr>");
            sb.Append("</table>");            
            return sb.ToString();
        }

        private string getPersonRow(PersonRateInfo personRate, string directionId) 
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<tr class='pax'>");
            sb.Append("<td class='cb'></td>");
            sb.Append("<td class='desc'>");
            sb.Append(getPaxNumberDropDown(directionId, personRate));
            sb.Append("<small>"+ personRate.PersonInfoType.ToString() +"</small>");                           
            sb.Append("</td>");
            sb.Append("<td class='price'>");
            sb.Append("<small>$"+ personRate.Rate + " each</small>");
            sb.Append("</td>");
            sb.Append("<td class='total'>");
            sb.Append("</td>");            
            sb.Append("</tr>");                       
            
            return sb.ToString();     
        }

        //decimal geVehicleRate(FerryProduct ferryProd)
        //{
        //    decimal vehicleRate = 0;
        //    ferryProd.DailyRate
        //}

        private string getCrossingDropDown(string directionId) 
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<select id='crossingDate_" + directionId + "' name='crossingDate_" + directionId + "'>");
            DateTime currentDate = pickUpDate;
            while (currentDate <= dropOffDate) 
            {
                sb.Append("<option value='"+ currentDate.ToString("ddMMyyyy") +"'>"+ currentDate.ToString("dd MMM") +"</option>");
                currentDate = currentDate.AddDays(1);
            }           
            sb.Append("</select>"); 
            return sb.ToString();
        }

        private string getCrossingTimeDropDown(string directionId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<select id='crossingTime_" + directionId + "' name='crossingTime_" + directionId + "'>");
            int currentTime = 9;//TODO: from config?
            while (currentTime < 22)
            {
                sb.Append("<option value='" + currentTime  + ":00'>" + currentTime + ":00</option>");
                currentTime++;
            }
            sb.Append("</select>");
            return sb.ToString();
        }

        private string getPaxNumberDropDown(string directionId, PersonRateInfo personRate)
        {
            PersonType personType = personRate.PersonInfoType;
            int maxPass = 8, currentPass = -1;
            StringBuilder sb = new StringBuilder();
            sb.Append("<select id='" + directionId + "_" + personType + "' name='" + directionId + "_" + personType + "' rel='"+ personRate.PersonId +"'>");            
            while(currentPass++ < maxPass)
                sb.Append("<option value='"+ currentPass +"'>"+ currentPass +"</option>");
            sb.Append("</select>");
            return sb.ToString();
        }
    }   
}