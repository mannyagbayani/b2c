﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Collections.Specialized;

namespace THL.Booking
{

    /// <summary>
    /// Render User Information UI
    /// </summary>
    public class UserDataDisplayer
    {
        public UserDataDisplayer()
        {

        }

        /// <summary>
        /// return xhtml for country of origin drop down
        /// consider html id as input param
        /// </summary>
        /// <param name="defaultCountryCode">iso country code str for default value</param>
        /// <returns>xhtml string</returns>
        public string GetCountryOfOriginDD(string defaultCountryCode)
        {
            string xhtml = "";
            return xhtml;
        }

        /// <summary>
        /// TODO:
        /// </summary>
        /// <param name="maxAdults"></param>
        /// <returns></returns>
        public string GetNumberOfAdultsDD(string maxAdults)
        {
            return string.Empty;
        }

        /// <summary>
        /// TODO:
        /// </summary>
        /// <param name="maxAdults"></param>
        /// <returns></returns>
        public string GetNumberOfChildrensDD(string maxAdults)
        {
            return string.Empty;
        }

        static NameValueCollection countries;

        /// <summary>
        /// TODO: get From App Level Config
        /// </summary>
        static UserDataDisplayer()
        {
            countries = new NameValueCollection();
            countries.Add("AU", "Australia");
            countries.Add("NZ", "New Zealand");
        }

        /// <summary>
        /// Renders Country of Travel (Used For all Booking Controls)
        /// </summary>
        /// <param name="defaultCountryCode"></param>
        /// <param name="brandType"></param>
        /// <returns></returns>
        public static string GetCountryOfTravelDD(string defaultCountryCode, THLBrands brandType)
        {

            if (brandType == THLBrands.MAC) {//MAC Stub TODO: revisit 
                countries.Add("US", "United States");
                countries.Add("CA", "Canada");
                //countries.Add("SA", "Southern Africa");
            }
                        
            StringBuilder sb = new StringBuilder();
            sb.Append("<select id='countryCode' name='countryCode'>");
            sb.Append("<option value='' " + (defaultCountryCode == "NONE" ? "selected='selected'" : "") + "  >Select Country of Travel</option>");
            foreach (string key in countries.AllKeys)
            {
                sb.Append("<option " + (key == defaultCountryCode ? "selected='selected'" : "") + " value='" + key + "'>" + countries[key] + "</option>");
            }
            
            sb.Append("</select>");
            return sb.ToString();
        }

        /// <summary>
        /// Renders Credit Card HTML Drop Down (Payment Page)
        /// </summary>
        /// <param name="brand"></param>
        /// <returns></returns>
        public string RenderCCDropDown(CreditCardElement[] availableCards, string brandStr)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<select id='cardTypeDD' name='cardTypeDD'>");
            foreach (CreditCardElement cardElement in availableCards)
            {
                sb.Append("<option value='"+ cardElement.Code +":"+ cardElement.Surcharge +"'>"+ cardElement.Name +"</option>");
            }           
            sb.Append("</select>");
            return sb.ToString();
        }
    
        /// <summary>
        /// Render Customers Details (Used within the Confirmation Page)
        /// </summary>
        /// <param name="customerData"></param>
        /// <returns></returns>
        public string RenderCustomerDetails(Customer customerData)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            sb.Append("<tr>");
            sb.Append("<td class='tc'>First name:</td>");
            sb.Append("<td>"+ customerData.FirstName +"</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td class='tc'>Surname:</td>");
            sb.Append("<td>"+ customerData.LastName +"</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td class='tc'>Telephone:</td>");
            sb.Append("<td>"+ customerData.PhoneNumber +"</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td class='tc'>Email:</td>");
            sb.Append("<td>"+ customerData.EMail +"</td>");
            sb.Append("</tr>");
            if (!string.IsNullOrEmpty(customerData.CountryOfresidenceCode) && customerData.CountryOfresidenceCode.Length == 2)
            {
                sb.Append("<tr>");
                sb.Append("<td class='tc'>Country:</td>");
                sb.Append("<td>" + customerData.CountryOfresidenceCode + "</td>");
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            return sb.ToString();           
        }
    }
}