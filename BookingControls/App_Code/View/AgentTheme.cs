﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace THL.Booking
{
    /// <summary>
    /// Support for Themed Views for 3rd Party agents 
    /// </summary>
    public class AgentTheme
    {
        /// <summary>
        /// Aurora Code for agent
        /// </summary>
        public string AgentCode { get; set; }

        /// <summary>
        /// The promo markup for this agents internal landing
        /// </summary>
        public string PromoHTML { get; set; }

        /// <summary>
        /// The Footer markup for this agent
        /// </summary>
        public string FooterHTML { get; set; }

        /// <summary>
        /// The Header markup for this agent
        /// </summary>
        public string HeaderHTML { get; set; }

        public string RemoteCSS { get; set; }
        
        public AgentTheme(string agentCode)
        {
            PromoHTML = string.Empty;
            AgentCode = agentCode.ToLower();
            try
            {
                string agentContentFile = System.Web.Configuration.WebConfigurationManager.AppSettings["LocalDataPath"] + "\\agents\\" + AgentCode + ".xml";
                if (System.IO.File.Exists(agentContentFile))
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load(agentContentFile);
                    RemoteCSS = xDoc.SelectSingleNode("//externalCSS").InnerText;
                    PromoHTML = xDoc.SelectSingleNode("//frontPromo").InnerXml;
                    FooterHTML = xDoc.SelectSingleNode("//footer").InnerXml;
                    //HeaderHTML = xDoc.SelectSingleNode("//header").InnerXml;
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;

            }
        }

        /// <summary>
        /// Return the extended style sheet path for 3rd party agent,
        /// TODO: add CDN future support and turn into static to save FS call
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string CSSPath(string requestAgent, string brandStr)
        {
            string cssThemeBase = brandStr;//default to none for no agent theme (or no agent passed)  

            try
            {
                if (!string.IsNullOrEmpty(requestAgent))
                {
                    string path = HttpContext.Current.Server.MapPath("/css/" + brandStr + "/" + requestAgent);
                    cssThemeBase = System.IO.Directory.Exists(path) ? brandStr + "/" + requestAgent : brandStr;
                }
            }
            catch (Exception ex) {
                THLDebug.LogError(ErrorTypes.Application, "AgentThemeView.CSSPath", ex.Message, "{'agentCode':'" + requestAgent + "','brand':'" + brandStr + "'}");
            }
            return cssThemeBase;
        }
    }
}