﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace THL.Booking
{    
    /// <summary>
    /// HTML Displayer Functionality for Branch Information
    /// </summary>
    public class BranchDisplayer
    {
        Branch _branch;

        Branch[] _branches;
        public Branch[] Branches {
            get {
                return _branches; 
            }    
        }
        
        public BranchDisplayer(Branch branch)
        {
            _branch = branch;
        }

        public BranchDisplayer(Branch[] branches)
        {
            _branches = branches;
        }
        
        public BranchDisplayer() { 
        
        }

        /// <summary>
        /// Display Time DropDown for according to selected Country and Vehicle Type 
        /// </summary>
        /// <param name="countryData">Country Specific Params</param>
        /// <param name="vehicleType">Required Vehicle Type</param>
        /// <param name="htmlId">Control HTML ID</param>
        /// <returns>XHTML Select Element</returns>
        public string GetOpeningHoursDD(BranchCountryData countryData, VehicleType vehicleType, string selectedTime, string htmlId) {
            
            //selectedTime = (selectedTime != null && selectedTime != string.Empty ? selectedTime : "15:00");
            
            StringBuilder sb = new StringBuilder();
            sb.Append("<select class=\"SearchBox_TimeListBox\" id=\"" + htmlId + "\" name=\"" + htmlId + "\">");
            if (countryData == null)
            {
                sb.Append("</select>");
                return sb.ToString();
            }
            else
            {   
                string[] openParams  = (vehicleType == VehicleType.Campervan ? countryData.CampersOpenHour : countryData.CarsOpenHour).Split(':');
                string[] closeParams = (vehicleType == VehicleType.Car ? countryData.CarsCloseHour : countryData.CampersCloseHour).Split(':');
                int openHour = 0, openMinute = 0, closeHour = 0, closeMinute =0;
                int.TryParse(openParams[0], out openHour);
                int.TryParse(openParams[1], out openMinute);
                int.TryParse(closeParams[0], out closeHour);
                int.TryParse(closeParams[1], out closeMinute);
                decimal startHour = openHour, endHour = closeHour;
                if (openMinute > 0) startHour = startHour + 0.5m;
                if (closeMinute > 0) endHour = endHour + 0.5m;
                decimal currentOptionTime = startHour;               
                while (currentOptionTime <= endHour)
                {
                    string timeStr = (Math.Floor(currentOptionTime) < 10 ? "0" : "") + Math.Floor(currentOptionTime) + ":" + (currentOptionTime - Math.Floor(currentOptionTime) > 0 ? "30" : "00");
                    sb.Append("<option value=\"" + timeStr + "\"" + (selectedTime == timeStr ? " selected=\"selected\"" : string.Empty) + ">" + timeStr + (currentOptionTime < 12 ? "am" : "pm") + "</option>");
                    currentOptionTime += 0.5m;
                }
                sb.Append("</select>");
                return sb.ToString();
            }
        }
        
        public string GetVehiclesDDForBranch(VehicleType vehicleType, THLBrand brand, string vehicleCode, string countryCode)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<select name='ctrlSearchBox_dropVehicleType' id='ctrlSearchBox_dropVehicleType' class='SearchBox_FullListBox'>");
            Vehicle[] vehicles = brand.GetVehiclesForCountry(countryCode);
            sb.Append("<option value=''>Search all</option>");
            foreach (Vehicle vehicle in vehicles) {
                if (countryCode != null && vehicle.Type == vehicleType)
                    sb.Append("<option value='" + vehicle.Code + "' " + (vehicleCode != null && vehicleCode.ToUpper() == vehicle.Code.ToUpper() ? "selected='selected'" : string.Empty) + ">" + vehicle.Name + "</option>"); //options = "<option value='aucvbk.BP2BB'>Backpacker Breezer</option><option value='aucvbk.BP2BHT'>Backpacker Nomad</option><option value='aucvbk.BP4BS'>Backpacker Warrior</option><option value='aucvbk.BP2BTS'>Backpacker Wanderer</option><option value='aucvbk.BP4WD'>Backpacker Off-Roader 4WD</option>";
            }
            sb.Append("</select>");
            return sb.ToString();
        }
        
        /// <summary>
        /// Return DropDown Text for a given Brand and Country  
        /// </summary>
        /// <param name="brandCode">brand code string TODO: convert ot enum</param>
        /// <param name="countryCode">country code string TODO: convert ot enum<</param>
        /// <returns></returns>
        public string GetBranchDDForBrand(string countryCode, string vehicleType, string ctrlId, BranchType bt, string selectedBranchCode)
        {
            VehicleType vtype = (vehicleType == "rv" || vehicleType == "av" ? VehicleType.Campervan : VehicleType.Car);
            countryCode = countryCode.ToUpper();
            StringBuilder sb = new StringBuilder();
            sb.Append("<select name=\"" + ctrlId + "\" id=\"" + ctrlId + "\" class=\"SearchBox_FullListBox LocationDD\">");
            StringBuilder options = new StringBuilder();
            foreach (Branch branch in _branches)
            {
                if (countryCode != null && branch.CountryCode == countryCode && !(branch.PickUpOnly == true && bt == BranchType.DropOff) && (branch.VehicleTypeAvailability == VehicleType.Both || vtype == branch.VehicleTypeAvailability))
                    sb.Append("<option value=\"" + branch.BranchCode + "\""+(selectedBranchCode.Equals(branch.BranchCode) ? " selected=\"selected\"" : "" ) +">" + branch.Name + "</option>");
            }
            sb.Append("</select>");
            return sb.ToString();
        }

        public string GetPassengerDD(PassengerType passengerType, int maxPassengers, string htmlId) { 
            string passengerSingularStr = (passengerType == PassengerType.Adult ? "Adult" : "Child");
            string passengerMultipleStr = (passengerType == PassengerType.Adult ? "Adults" : "Children");
            StringBuilder sb= new StringBuilder();
            sb.Append("<select name='"+ htmlId +"' id='"+ htmlId +"' class='SearchBox_AdultsListBox'>");
            sb.Append("<option selected='selected' value='" + (passengerType ==  PassengerType.Adult ? "1" : "0") +"'>" + passengerMultipleStr + "</option>");
            sb.Append("<option value='1'>1 "+ passengerSingularStr +"</option>");
            for(int i = 2 ; i<= maxPassengers; i++)
                sb.Append("<option value='" + i + "'>" + i + " " + passengerMultipleStr + "</option>");
            sb.Append("</select>");                            
            return sb.ToString();          
        }
    }
}