﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace THL.Booking
{

     /// <summary>
    /// Availability Response Displayer Functionality
    /// </summary>
    public class AvailabilityResponseDisplayer
    {
        
        AvailabilityResponse availabilityResponse;
        AvailabilityRequest availabilityRequest;

        string currencyStr;       

        string assetsURL;

        public string ContactPhoneText;

        /// <summary>
        /// Currency Conversion Functionality
        /// </summary>
        CurrencyManager currencyManager;

        THLBrands _containerBrand;

        /// <summary>
        /// Construct Availability Displayer using a given Aurora Reponse Object and Currency Conversion Manager
        /// </summary>
        /// <param name="_availabilityResponse"></param>
        /// <param name="_currencyManager"></param>
        public AvailabilityResponseDisplayer(AvailabilityResponse _availabilityResponse, AvailabilityRequest _availabilityRequest, CurrencyManager _currencyManager, string _assetsURL, THLBrands containerBrand)
        {
            availabilityRequest = _availabilityRequest;
            availabilityResponse = _availabilityResponse;
            currencyManager = _currencyManager;
            currencyStr = currencyManager.DefaultCurrency.ToString();
            assetsURL = _assetsURL;
            _containerBrand = containerBrand;            
        }
        
        /// <summary>
        /// Added to support content content top site support
        /// </summary>
        public THLDomain AppDomain { get; set; }


        /// <summary>
        /// Used for flat content generation
        /// </summary>
        public AvailabilityResponseDisplayer() 
        { 
        
        }
        
        /// <summary>
        /// Render Availability Catalog 
        /// </summary>
        /// <returns></returns>
        public string RenderAvailabilityVehicleCatalog(string selectedCode)
        {
            AvailabilityItem[] sortedItems = availabilityResponse.GetAvailabilityItems("price");
            
            bool step1a = false;
            if (selectedCode != null && selectedCode != string.Empty && sortedItems.Length != 0 && sortedItems[0].BlockingRuleMessage != null )//step 1a resort and pop selectedCode to top
                step1a = true;                    
            
            StringBuilder sb = new StringBuilder();
            sb.Append("<ul id='vehicleCatalog'>");
            foreach (AvailabilityItem ai in sortedItems)
            {                
                //added for sorting
                bool available = (ai.AvailabilityType == AvailabilityItemType.Available || ai.AvailabilityType == AvailabilityItemType.OnRequest);
                sb.Append("<li class='Collapse element" + (available ? " avail" : " noavail") + "'>");               
                if(selectedCode != null)
                    selectedCode = (selectedCode.Split('.').Length > 1 ? selectedCode.Split('.')[1] : selectedCode.Split('.')[0]) ;

                if (step1a && ai.VehicleCode.Equals(selectedCode))
                {//render 1A
                    sb.Append(render1aMsg(RenderAvailabilityRow(ai, _containerBrand), ai.VehicleName));
                    step1a = false;
                }
                else
                    sb.Append(RenderAvailabilityRow(ai, _containerBrand));
                sb.Append("</li>");
            }
            sb.Append("</ul>");
            return sb.ToString();
        }

        string render1aMsg(string wrappedHTML, string vehicleName)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div class='Step1A'>The vehicle you have chosen has limited availability for the dates selected.</div>");//TODO: content manage
            sb.Append("<span class='Step1AWrap'>");
            sb.Append(wrappedHTML);
            sb.Append("</span>");
            sb.Append("<div class='Step1A'>Other vehicles to select from:</div>");//TODO: content manage
            return sb.ToString();
        }

        /// <summary>
        /// Return vehicle size for its vehicel code
        /// TODO: this should be refactor into a manager that returns vehicle features for code (for filtering and sorting functionality)
        /// </summary>
        /// <param name="vehicleCode"></param>
        /// <returns>max pax for the requested vehicel</returns>
        public static int getVehicleSize(string vehicleCode)
        {
            int size = 6;
            try
            {

                string firstChar = !string.IsNullOrEmpty(vehicleCode) ? vehicleCode.Substring(0, 1) : "6";
                int.TryParse(vehicleCode.Substring(0, 1), out size);
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Application, "AvailabilityResponseDisplayer.getVehicleSize", ex.Message, "{vehicleCode:'"+ vehicleCode +"'}");
            }
            return size;
        }

        /// <summary>
        /// Render a Single Availability Row (Selection Page Catalog)
        /// </summary>
        /// <param name="availabilityRow"></param>
        /// <returns></returns>
        string RenderAvailabilityRow(AvailabilityItem availabilityRow, THLBrands containerSite)
        {
            //if (availabilityRow.ErrorMessage == "NOT A VALID PICKUP AND DROP OFF LOCATION FOR THIS VEHICLE")//added support for vtype ctrl swapping 150110
            //    return string.Empty;

            decimal adminFeeFactor = 1.0m;//use for adding Admin Fee if applicable
            if (availabilityRow.AdminFee > 0)
                adminFeeFactor = 1.0m + (availabilityRow.AdminFee / 100);

            string httpPrefix = "http://";//content pages are always HTTP

            string vehicleContentLink = ContentManager.GetVehicleLink(availabilityRow.Brand, availabilityRequest.CountryCode, availabilityRow.VehicleCode, _containerBrand);

            char brandChar = THLBrand.GetBrandChar(availabilityRow.Brand);
            char containerBrandChar = THLBrand.GetBrandChar(_containerBrand);

            string vehicleName = availabilityRow.VehicleName;
            string vehicleCode = availabilityRow.VehicleCode;
            string packageName = availabilityRow.PackageCode;
            string vehicleTypeStr = (availabilityRequest.VehicleType == VehicleType.Car ? "Cars" : "Campervans");

            int vehicleSize = getVehicleSize(vehicleCode);

            string dataSection = "data-vsize='" + vehicleSize + 
                                 "' data-vprice='"+ (availabilityRow.EstimatedTotal > 0.0m ? availabilityRow.EstimatedTotal : 99999.00m) /* todo: biz logic on no rates */  +
                                 "' data-avail=" + ((availabilityRow.AvailabilityType == AvailabilityItemType.Available || availabilityRow.AvailabilityType == AvailabilityItemType.OnRequest) ? "'true' " : "'false' ");
            ;

            //Image Convention
            THLDomain appDomain = ApplicationManager.GetDomainForURL(HttpContext.Current.Request.Url.Host);

            string vehicleImagePath = AvailabilityHelper.GetVehicleImageBasePath(availabilityRequest.CountryCode, availabilityRow.Brand, availabilityRequest.VehicleType, vehicleCode, assetsURL, false);
            string inclusionImagePath = AvailabilityHelper.GetVehicleImageBasePath(availabilityRequest.CountryCode, availabilityRow.Brand, availabilityRequest.VehicleType, vehicleCode, assetsURL, true);

            string vehicleInclusionImage = String.Format("<img src='{0}-InclusionIcons.gif' border='0' title='{1}' />", inclusionImagePath, availabilityRow.Brand);
            string vehicleClearCut72Image = String.Format("<img src='{0}-ClearCut-72.jpg' border='0' title='{1}' />", vehicleImagePath, vehicleCode);
            string vehicleClearCut144Image = String.Format("<img src='{0}-ClearCut-144.jpg' border='0' title='{1}' />", vehicleImagePath, vehicleCode);
            string noAvailMsg = "Search online to find the following<br/>alternative travel options for this vehicle:<br /><b>Reverse route, +/- 10 days, Shorter hire.</b>";
            string noAvailLink = "javascript:preloadPage(\"vCode=" + vehicleCode + "&rType=GetAlternateOptions&Brand=" + brandChar + "\")";
            string noAvailTitle = "Unavailable - Search Alternative Availability";
            bool shortLead = false;
            if (!string.IsNullOrEmpty(availabilityRow.ErrorMessage) && availabilityRow.ErrorMessage.Contains("DISP24HR"))//swap above if short leadtime detected
            {//less then 24hr lead
                noAvailMsg = "Let our 24hr res team find a vehicle for you<br />We can search all the big brands:<br /><b>Maui, Britz, Mighty</b>";
                noAvailTitle = (containerSite == THLBrands.AIRNZ ? "Bookings cannot be made within 24 hours" : "Travel is within 24 hours - Contact Us");
                noAvailLink = AppDomain.ParentSite + "/callus";
                //TODO: consider this : ContentManager.GetMessageForCode(THLBrand.GetBrandForString(brandStr), appDomain.Country, "CrossSale").Content for cross sell
                shortLead = true;//phone and no alt avail
            }
            else if (!string.IsNullOrEmpty(availabilityRow.ErrorMessage) && availabilityRow.ErrorMessage.Contains("DISPRateCalc"))
            {//weekly mantenance case
                noAvailMsg = "Let our 24hr res team find a vehicle for you<br />We can search all the big brands:<br /><b>Maui, Britz, Mighty</b>";
                noAvailTitle = "We are undergoing scheduled maintenance, will be back 12.15 NZST";
                noAvailLink = AppDomain.ParentSite + "/callus";
                //TODO: consider this : ContentManager.GetMessageForCode(THLBrand.GetBrandForString(brandStr), appDomain.Country, "CrossSale").Content for cross sell
                shortLead = true;//phone and no alt avail            
            }

            //add additional tokens here

            StringBuilder sb = new StringBuilder();
            if (availabilityRow.AvailabilityType == AvailabilityItemType.Available || availabilityRow.AvailabilityType == AvailabilityItemType.OnRequest)
            {
                sb.Append("<div class='VehicleItem'>");
                sb.Append("<img src='" + assetsURL + "/images/" + brandChar + "/icons/rowLogo.gif' title='" + availabilityRow.Brand + "' class='RowLogo PopUp' />");
                sb.Append("<a href='" + httpPrefix + vehicleContentLink + "' class='VehicleThumb PopUp'>");
                sb.Append(vehicleClearCut72Image);
                sb.Append("</a>");
                sb.Append("<div class='VehicleFeatures' " + dataSection + ">");
                sb.Append("<a href='" + httpPrefix + vehicleContentLink + "' class='PopUp'>" + THLBrand.GetNameForBrand(availabilityRow.Brand) + " " + vehicleName + "</a>");
                sb.Append(vehicleInclusionImage);
                sb.Append("</div>");
                sb.Append("<div class='PriceDescription'>");
                sb.Append("<a href='Configure.aspx?avp=" + availabilityRow.AVPID + "' class='" + (availabilityRow.AvailabilityType == AvailabilityItemType.Available ? "Avail" : "OnRequest") + "'>" + (availabilityRow.AvailabilityType == AvailabilityItemType.Available ? "AVAILABLE NOW" : "ON REQUEST") + "</a>");
                                
                //TODO: swap the <img> with content coming from WS
                
                if (availabilityRow.DisplayPromoTile && string.IsNullOrEmpty(availabilityRow.PromoText))
                {
                        sb.Append("<span><img src='" + assetsURL + "/CentralLibraryImages/" + THLBrand.GetNameForBrand(availabilityRow.Brand) + "/Promotions/" + availabilityRow.PackageCode + ".png' title='Promotion' /></span>");//TODO: file convention on img src for Promos goes here                
                }
                else if (!string.IsNullOrEmpty(availabilityRow.PromoText))
                {
                    sb.Append("<span class='promo'>" + availabilityRow.PromoText + "</span>");
                }

                sb.Append("</div>");
                sb.Append("<div class='TotalPrice' title='" + packageName + "'>");
                sb.Append(getForeignListElm(availabilityRow.EstimatedTotal * adminFeeFactor));
                sb.Append("<a class='ShowPriceList' onclick='showPriceList(this)'>Price Details</a>");
                sb.Append("</div>");
                sb.Append("<a href='Configure.aspx?avp=" + availabilityRow.AVPID + "' class='SelectBtn'>Select</a>");
                sb.Append("</div>");
                sb.Append(RenderPriceDetails(availabilityRow));
            }
            else
            {
                bool isBlocking = false;
                if (availabilityRow.AvailabilityType == AvailabilityItemType.BlockingRuleTaC)
                {
                    isBlocking = true;
                }

                string errMsg = availabilityRow.BlockingRuleMessage + availabilityRow.ErrorMessage;
                sb.Append("<div class='VehicleItem " + (isBlocking ? "Blocking" : string.Empty) + "'>");
                sb.Append("<img src='" + assetsURL + "/images/" + brandChar + "/icons/rowLogo.gif' title='" + availabilityRow.Brand + "' class='RowLogo' />");
                sb.Append("<a href='" + httpPrefix + vehicleContentLink + "' class='VehicleThumb PopUp'>");
                sb.Append(vehicleClearCut72Image);
                sb.Append("</a>");
                sb.Append("<div class='VehicleFeatures' " + dataSection + ">");
                sb.Append("<a href='" + httpPrefix + vehicleContentLink + "' class='PopUp'>" + THLBrand.GetNameForBrand(availabilityRow.Brand) + " " + vehicleName + "</a>");
                sb.Append(vehicleInclusionImage);
                sb.Append("</div>");
                sb.Append("<div class='Err'><span>" + errMsg + "</span></div>");
                sb.Append("<div class='Limited'><div class='Top'><span><a href='" + noAvailLink + "' "+ (shortLead ? "class='PopUp'" : string.Empty) +">" + noAvailTitle + "</a></span><span class='Contact'>" + noAvailMsg + "</span></div>");
                
                if (containerSite == THLBrands.AIRNZ)//add to support airnz exception on callus funcionality
                    sb.Append("<div class='EmailLink LeftLnk'><a class='PopUp' href='EmailBooking.aspx?vCode=" + vehicleCode + "'>E-mail your request to reservations</a></div>");
                
                if(shortLead && !(containerSite == THLBrands.AIRNZ))//AirNZ no Contact Exception
                    sb.Append("<div class='CallLink LeftLnk'><a href='" + AppDomain.ParentSite + "/callus' class='PopUp'>Call us now for free</a></div>");
                else if (!shortLead)
                    sb.Append("<div class='AALink LeftLnk'><a class='aAvail' href='" + noAvailLink + "'>Search Alternate Availability</a></div>");
                else if (containerSite == THLBrands.AIRNZ)
                    sb.Append("<div class='CallLink LeftLnk'><a href='#' class='searchAgain'>Search again</a></div>");


                sb.Append("</div>");
                sb.Append("</div>");
            }
            return sb.ToString();
        }
        
        public string RenderAvailabilityRowJSON(AvailabilityItem availabilityRow, decimal extraHireSum)
        {
            StringBuilder sb = new StringBuilder();            
            if (availabilityRow.AvailabilityType == AvailabilityItemType.Available || availabilityRow.AvailabilityType == AvailabilityItemType.OnRequest)
            {
                sb.Append("{");
                sb.Append("vCode: '" + availabilityRow.VehicleCode + "',");
                sb.Append("vName: '" + availabilityRow.VehicleName + "',");
                sb.Append("hirePeriod: " + availabilityRow.GetHirePeriod()+ ",");
                sb.Append("vehicleTotal: " + (availabilityRow.EstimatedTotal + extraHireSum)+ ",");
                sb.Append("depositPercentage : " + availabilityRow.DepositPercentage + ",");
                sb.Append("adminFee : " + availabilityRow.AdminFee + ",");
                sb.Append("depositAmount : " + availabilityRow.DepositAmount);
                sb.Append("}");
            }
            return sb.ToString();
        }

        /// <summary>
        /// Rende JSON Data of a specified Availability Row Identified by AVPID
        /// </summary>
        /// <param name="avpId"></param>
        /// <returns></returns>
        public string RenderAvailabilityRowJSON(string avpId, AuroraBooking bookingData)
        {
            AvailabilityItem availabilityRow = availabilityResponse.GetAvailabilityItem(avpId);
            decimal calculatedTotal = (bookingData != null ? availabilityRow.EstimatedTotal + bookingData.GetTotalNonVehicleCharges(ProductType.All) : availabilityRow.EstimatedTotal) ;
            if (bookingData.BonusPackSelected && bookingData.InclusivePack != null)
                calculatedTotal += bookingData.InclusivePack.GrossAmount;

            decimal adminFee = (!bookingData.BonusPackSelected ? availabilityRow.AdminFee : 0.0m);

            //031109
            decimal oneWayFeeWaiver = (bookingData.WaiveOneWayFee && bookingData.OneWayFeeComponent > 0 ? bookingData.OneWayFeeComponent : 0.0m);
            if(bookingData.BonusPackSelected)
                calculatedTotal -= oneWayFeeWaiver;
            //031109

            StringBuilder sb = new StringBuilder();
            if (availabilityRow.AvailabilityType == AvailabilityItemType.Available || availabilityRow.AvailabilityType == AvailabilityItemType.OnRequest)
            {
                sb.Append("{");
                sb.Append("vCode: '" + availabilityRow.VehicleCode + "',");
                sb.Append("vName: '" + availabilityRow.VehicleName + "',");
                sb.Append("hirePeriod: " + availabilityRow.GetHirePeriod() + ",");
                sb.Append("vehicleTotal: " + availabilityRow.EstimatedTotal + ",");
                sb.Append("depositPercentage : " + availabilityRow.DepositPercentage + ",");
                sb.Append("depositAmount : " + availabilityRow.DepositAmount + ",");
                sb.Append("adminFee : " + adminFee + ",");
                sb.Append("calculatedTotal : " + (calculatedTotal + ((adminFee/100)* calculatedTotal)).ToString("F"));
                sb.Append("}");
            }
            return sb.ToString();
        }
        
        /// <summary>
        /// Forign Currencies HTML List for a given Price
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        string getForeignListElm(decimal price)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<ul class='Prices'>");           
            sb.Append("<li>");
            sb.Append("<span>"+ currencyStr +" " + String.Format("{0:N2}",price) + "</span>");
            sb.Append("</li>");
            foreach (Currency currency in currencyManager.CurrencyRates)
            {
                //if AUD price should be 
                //if (currencyManager.DefaultCurrency == CurrencyCode.AUD) price = 
                decimal currentPriceNZD = price;
                
                if (availabilityRequest.CountryCode != CountryCode.NZ)
                    currentPriceNZD = currencyManager.ConvertToNZD(availabilityRequest.CountryCode, price);


                sb.Append("<li class='Fp Hide " + currency.CountryCode + "'>");
                sb.Append("<span>" + currency.CountryCode + " " + PaymentHelper.GetCurrencySymbolForISO(currency.CountryCode) + " " + String.Format("{0:N2}", (currentPriceNZD * currency.IndicativeRate)) + "</span>");
                sb.Append("</li>");
            }          
            sb.Append("</ul>");           
            return sb.ToString();
        }

        /// <summary>
        /// Render Price Details Table for a given Availability Row Object
        /// </summary>
        /// <param name="availabilityRow"></param>
        /// <returns></returns>
        public string RenderPriceDetails(AvailabilityItem availabilityRow) 
        {
            int colSpans = 4;
            
            StringBuilder sb = new StringBuilder();           
            bool hasFreeDays = false;
            AvailabilityItemChargeRateBand[] availabilityRateBands = availabilityRow.GetRateBands();
            AvailabilityItemChargeDiscount[] aidc = availabilityRow.GetDiscounts();
            bool hasDiscount = false, hasPercentageDiscount = false;

            foreach (AvailabilityItemChargeDiscount discount in aidc)
            {
                //if (discount.IsPercentage) hasDiscount = true;// 04/08/09 only consider percentage discount ,TODO: dive into all ratebands           
                if (discount.IsPercentage) hasPercentageDiscount = true;
            }

            hasDiscount = AvailabilityHelper.HasDiscountedDays(availabilityRateBands);


            colSpans = (hasDiscount || hasFreeDays ? colSpans : --colSpans);
                     
            if(availabilityRateBands != null && availabilityRateBands.Length > 0)
                foreach (AvailabilityItemChargeRateBand chargeRateRow in availabilityRateBands) //order?
                    if (chargeRateRow.IncludesFreeDay > 0)
                        hasFreeDays = true;
            sb.Append("<div class='PriceDetailsList "+ (hasFreeDays ? string.Empty : "NoFreeDays" ) + (hasDiscount ? string.Empty : " NoDiscount") + "'>");

            //vehicle content from central repository
            VehicleContent vehicleContent = ApplicationManager.GetApplicationData().GetContentForVehicle(availabilityRow.VehicleCode);
            if(!string.IsNullOrEmpty(vehicleContent.Description))
                sb.Append("<div class='vinfo' style='font-size: 0.7em;margin: 0 0 5px 20px;display:none;'>\"" + ApplicationManager.GetApplicationData().GetContentForVehicle(availabilityRow.VehicleCode).Description + "\"</div>"); 
                        
            sb.Append("<table class='chargesTable'>");
            sb.Append("<thead>");
            sb.Append("<tr>");
            sb.Append("<th class='RateTH'>Product Description</th>");
            sb.Append("<th class='FreeTH'><span>Free Days</span><span rel='freeDays' class='popup' style='display:none;'>[?]</span></th>");
            sb.Append("<th class='HireTH'>Hire Days</th>");
            sb.Append("<th class='PerTH'>Per Day Price</th>");
            sb.Append(!hasDiscount ? "" : "<th class='DiscTH' " + (hasDiscount ? "" : "style='display:none;'") + "><span>Discounted Day Price</span>" + (hasPercentageDiscount ? "<span rel='DiscountedDaysPopUp' class='popup' onmouseover='initPopUp(this)'>[?]</span>" : string.Empty) + "</th>");
            sb.Append("<th class='TotalTH'>Total</th>");         
            sb.Append("</tr>");
            sb.Append("</thead>");
            sb.Append("<tbody>");           
                                        
            bool displayTotal = false;
            decimal longHireDiscount = availabilityRow.GetLongHireDiscount();
            decimal totalCharge = availabilityRow.GetTotalPrice();

            if (availabilityRateBands != null)//has several rate bands
            {
                if (availabilityRateBands.Length > 1)
                    displayTotal = true;
                foreach (AvailabilityItemChargeRateBand chargeRateRow in availabilityRateBands)
                {
                    string freeDaysStr = string.Empty;
                    if (chargeRateRow.IncludesFreeDay > 0)
                    {
                        freeDaysStr = chargeRateRow.IncludesFreeDay.ToString();
                        hasFreeDays = true;
                    }
                    string fromDateStr = chargeRateRow.FromDate.ToString("dd-MMM-yyyy");
                    string toDateStr = chargeRateRow.ToDate.ToString("dd-MMM-yyyy");
                    sb.Append("<tr>");
                    sb.Append("<td class='rt'>" + fromDateStr + " to " + toDateStr + "</td>");
                    sb.Append("<td class='fd'>" + freeDaysStr + "</td>");
                    sb.Append("<td class='hp'>" + chargeRateRow.HirePeriod + " day(s)</td>");
                    sb.Append("<td class='pdp'>$" + chargeRateRow.OriginalRate.ToString("F") + "</td>");
                    sb.Append(!hasDiscount ? "" : "<td class='dpd' " + (hasDiscount ? "" : "style='display:none;'") + ">$" + chargeRateRow.DiscountedRate.ToString("F") + "</td>");
                    sb.Append("<td>$" + chargeRateRow.GrossAmount.ToString("F") + "</td>");
                    sb.Append("</tr>");
                }
            }
            else //single rate band, TODO: revisit formating
            {
                AvailabilityItemChargeRow vehicleRow = availabilityRow.GetVehicleChargeRow();
                sb.Append("<tr>");
                sb.Append("<td class='rt'>" + vehicleRow.ProductName + "</td>");
                sb.Append("<td class='fd'>" + /* free days? */  "</td>");
                sb.Append("<td class='hp'>" + vehicleRow.HirePeriod + " day(s)</td>");
                sb.Append("<td class='pdp'>" + /*chargeRateRow.OriginalRate.ToString("F") +*/ "</td>");
                sb.Append("<td class='dpd' " /*+ (hasDiscount ? "" : "style='display:none;'") + ">$" + chargeRateRow.DiscountedRate.ToString("F")*/ + "</td>");
                sb.Append("<td>$" + vehicleRow.ProductPrice + "</td>");
                sb.Append("</tr>");            
            }
            
            AvailabilityItemChargeRow[] aCharges = availabilityRow.GetNonVehicleCharges();
            if (aCharges != null && aCharges.Length > 0)
            {
                displayTotal = true;
                foreach(AvailabilityItemChargeRow charge in aCharges)
                {
                    if (charge.IsBonusPack || charge.AdminFee > 0) continue;
                    
                    if (charge.HirePeriod > 1)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>" + charge.ProductName + "</td>");
                        sb.Append("<td class='hp'>" + charge.HirePeriod + " day(s)</td>");
                        sb.Append("<td class='pdp'>$" + charge.AverageDailyRate.ToString("F") + "</td>");
                        if (hasDiscount) sb.Append("<td class='pdp'>" + "&nbsp;" + "</td>");
                        if (hasFreeDays) sb.Append("<td class='dpd'>" + "&nbsp;" + "</td>");
                        sb.Append("<td>" + (charge.ProductPrice>0 ? "$"+charge.ProductPrice : "included")  + "</td>");
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr rel='" + charge.ProductCode + "'>");
                        sb.Append("<td colspan='" + (colSpans + (hasFreeDays ? 1 : 0)) + "'>" + charge.ProductName + "</td>");
                        sb.Append("<td>" + (charge.ProductPrice == 0m ? "included" : "$" + charge.ProductPrice.ToString("F")) + "</td>");
                        sb.Append("</tr>");
                    }
                }                 
                                           
            }


            decimal adminFeeFactor = 1.0m;
            if (availabilityRow.AdminFee > 0 && availabilityRow.AdminFee > 0)
            {
                adminFeeFactor = 1.0m + (availabilityRow.AdminFee / 100);
                sb.Append("<tr>");
                sb.Append("<td colspan='" + (colSpans + (hasFreeDays ? 1 : 0)) + "'>" + (availabilityRow.AdminFee).ToString("F") + "% Administration fee</td>");
                sb.Append("<td>$" + (availabilityRow.EstimatedTotal * (availabilityRow.AdminFee / 100)).ToString("F") + "</td>");
                sb.Append("</tr>");
            }

            if (displayTotal)
            {
                colSpans = 3 + (hasDiscount ? 1 : 0) + (hasFreeDays ? 1 : 0);
                sb.Append("<tr class='total'>");
                sb.Append("<td class='TotalTD' colspan='" + colSpans + "'>Total</td>");
                sb.Append("<td>$" + (availabilityRow.EstimatedTotal* adminFeeFactor ).ToString("N") + "</td>");
                sb.Append("</tr>");
            }

            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append(getDiscountPopUp(availabilityRow));            
            sb.Append("</div>");
            string result = sb.ToString();            
            return result;
        }


        /// <summary>
        /// Render Price Details Table for a given Availability Row Object inside the Configure Page Context
        /// </summary>
        /// <param name="availabilityRow"></param>
        /// <returns></returns>
        public string RenderConfigurePagePriceDetails(AvailabilityItem availabilityRow)
        {
            int colSpans = 4;

            StringBuilder sb = new StringBuilder();
            bool hasFreeDays = false;
            AvailabilityItemChargeRateBand[] availabilityRateBands = availabilityRow.GetRateBands();
            AvailabilityItemChargeDiscount[] aidc = availabilityRow.GetDiscounts();
            bool hasDiscount = false, hasPercentageDiscount = false;

            foreach (AvailabilityItemChargeDiscount discount in aidc)
            {
                //if (discount.IsPercentage) hasDiscount = true;// 04/08/09 only consider percentage discount ,TODO: dive into all ratebands           
                if (discount.IsPercentage) hasPercentageDiscount = true;
            }

            hasDiscount = AvailabilityHelper.HasDiscountedDays(availabilityRateBands);


            colSpans = (hasDiscount || hasFreeDays ? colSpans : --colSpans);

            if (availabilityRateBands != null && availabilityRateBands.Length > 0)
                foreach (AvailabilityItemChargeRateBand chargeRateRow in availabilityRateBands) //order?
                    if (chargeRateRow.IncludesFreeDay > 0)
                        hasFreeDays = true;
            sb.Append("<div class='PriceDetailsList " + (hasFreeDays ? string.Empty : "NoFreeDays") + (hasDiscount ? string.Empty : " NoDiscount") + "'>");
            sb.Append("<table class='chargesTable'>");
            sb.Append("<thead>");
            sb.Append("<tr>");
            sb.Append("<th class='RateTH'>Product Description</th>");
            sb.Append("<th class='FreeTH'><span>Free Days</span><span rel='freeDays' class='popup'>[?]</span></th>");
            sb.Append("<th class='HireTH'>Hire Days</th>");
            sb.Append("<th class='PerTH'>Per Day Price</th>");
            sb.Append(!hasDiscount ? "" : "<th class='DiscTH' " + (hasDiscount ? "" : "style='display:none;'") + "><span>Discounted Day Price</span>" + (hasPercentageDiscount ? "<span rel='DiscountedDaysPopUp' class='popup' onmouseover='initPopUp(this)'>[?]</span>" : string.Empty) + "</th>");
            sb.Append("<th class='TotalTH'>Total</th>");
            sb.Append("</tr>");
            sb.Append("</thead>");
            sb.Append("<tbody>");

            bool displayTotal = false;
            decimal longHireDiscount = availabilityRow.GetLongHireDiscount();
            decimal totalCharge = availabilityRow.GetTotalPrice();

            if (availabilityRateBands != null)//has several rate bands
            {
                if (availabilityRateBands.Length > 1)
                    displayTotal = true;
                foreach (AvailabilityItemChargeRateBand chargeRateRow in availabilityRateBands)
                {
                    string freeDaysStr = string.Empty;
                    if (chargeRateRow.IncludesFreeDay > 0)
                    {
                        freeDaysStr = chargeRateRow.IncludesFreeDay.ToString();
                        hasFreeDays = true;
                    }
                    string fromDateStr = chargeRateRow.FromDate.ToString("dd-MMM-yyyy");
                    string toDateStr = chargeRateRow.ToDate.ToString("dd-MMM-yyyy");
                    sb.Append("<tr>");
                    sb.Append("<td class='rt'>" + fromDateStr + " to " + toDateStr + "</td>");
                    sb.Append("<td class='fd'>" + freeDaysStr + "</td>");
                    sb.Append("<td class='hp'>" + chargeRateRow.HirePeriod + " day(s)</td>");
                    sb.Append("<td class='pdp'>$" + chargeRateRow.OriginalRate.ToString("F") + "</td>");
                    sb.Append(!hasDiscount ? "" : "<td class='dpd' " + (hasDiscount ? "" : "style='display:none;'") + ">$" + chargeRateRow.DiscountedRate.ToString("F") + "</td>");
                    sb.Append("<td>$" + chargeRateRow.GrossAmount.ToString("F") + "</td>");
                    sb.Append("</tr>");
                }
            }
            else //single rate band, TODO: revisit formating
            {
                AvailabilityItemChargeRow vehicleRow = availabilityRow.GetVehicleChargeRow();
                sb.Append("<tr>");
                sb.Append("<td class='rt'>" + vehicleRow.ProductName + "</td>");
                sb.Append("<td class='fd'>" + /* free days? */  "</td>");
                sb.Append("<td class='hp'>" + vehicleRow.HirePeriod + " day(s)</td>");
                sb.Append("<td class='pdp'>" + /*chargeRateRow.OriginalRate.ToString("F") +*/ "</td>");
                sb.Append("<td class='dpd' " /*+ (hasDiscount ? "" : "style='display:none;'") + ">$" + chargeRateRow.DiscountedRate.ToString("F")*/ + "</td>");
                sb.Append("<td>$" + vehicleRow.ProductPrice + "</td>");
                sb.Append("</tr>");
            }

            //if (availabilityRow.AdminFee > 0 && availabilityRow.AdminFee > 0) //removed 201009
            //    sb.Append("<tr><td>" + availabilityRow.AdminFee.ToString("F") + "% Administration fee will apply</td></tr>");

            AvailabilityItemChargeRow[] aCharges = availabilityRow.GetNonVehicleCharges();
            if (aCharges != null && aCharges.Length > 0)
            {
                displayTotal = true;
                foreach (AvailabilityItemChargeRow charge in aCharges)
                {
                    if (charge.IsBonusPack || charge.AdminFee > 0) continue;

                    if (charge.HirePeriod > 1)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>" + charge.ProductName + "</td>");
                        sb.Append("<td class='hp'>" + charge.HirePeriod + " day(s)</td>");
                        sb.Append("<td class='pdp'>$" + charge.AverageDailyRate.ToString("F") + "</td>");
                        if (hasDiscount) sb.Append("<td class='pdp'>" + "&nbsp;" + "</td>");
                        if (hasFreeDays) sb.Append("<td class='dpd'>" + "&nbsp;" + "</td>");
                        sb.Append("<td>" + (charge.ProductPrice > 0 ? "$" + charge.ProductPrice : "included") + "</td>");
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.Append("<tr rel='" + charge.ProductCode + "'>");
                        sb.Append("<td colspan='" + (colSpans + (hasFreeDays ? 1 : 0)) + "'>" + charge.ProductName + "</td>");
                        sb.Append("<td>" + (charge.ProductPrice == 0m ? "included" : "$" + charge.ProductPrice.ToString("F")) + "</td>");
                        sb.Append("</tr>");
                    }
                }

            }
            if (displayTotal)
            {
                colSpans = 3 + (hasDiscount ? 1 : 0) + (hasFreeDays ? 1 : 0);
                sb.Append("<tr class='total'>");
                sb.Append("<td class='TotalTD' colspan='" + colSpans + "'>Total</td>");
                sb.Append("<td>$" + availabilityRow.EstimatedTotal.ToString("N") + "</td>");
                sb.Append("</tr>");
            }

            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append(getDiscountPopUp(availabilityRow));
            sb.Append("</div>");
            string result = sb.ToString();
            return result;
        }
        
        /// <summary>
        /// Discount PopUps 
        /// </summary>
        /// <param name="availabilityRow"></param>
        /// <returns></returns>
        private string getDiscountPopUp(AvailabilityItem availabilityRow) 
        {
            AvailabilityItemChargeDiscount[] aidc = availabilityRow.GetDiscounts();
            if (aidc == null || aidc.Length == 0)
                return null;
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<div class='DiscountedDaysPopUp bubbleBody'>");
                sb.Append("<h3>Discounted Day Price</h3>");
                sb.Append("<table>");
                sb.Append("<tbody>");
                decimal totalDis = 0m;
                string discountUnitDisplay = string.Empty;
                foreach (AvailabilityItemChargeDiscount aid in aidc)
                {
                    if (aid.IsPercentage)
                    {
                        totalDis += aid.DisPercentage;//TODO: Condider summing discount totals on non percentage as well
                        discountUnitDisplay = aid.DisPercentage + "%";
                    }
                    else 
                    {
                        discountUnitDisplay = "$" + aid.DisPercentage;
                        continue;
                    }
                    sb.Append("<tr>");
                    sb.Append("<td>" + aid.CodDesc + "</td>");
                    sb.Append("<td>" + discountUnitDisplay + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("<tr class='total' rel='" + totalDis + "'>");
                sb.Append("<td>Total Discount</td>");
                sb.Append("<td>" + totalDis + "%</td>");
                sb.Append("</tr>");
                sb.Append("</tbody>");
                sb.Append("</table>");
                sb.Append("<span class='note'>(off standard per day rates)</span>");
                sb.Append("</div>");               
                return sb.ToString();
            }
        }
        
        /// <summary>
        /// Render Currency Conversion Selector DropDown
        /// </summary>
        /// <param name="defaultCurrency"></param>
        /// <returns></returns>
        public string RenderCurrencyDropDown(CurrencyCode defaultCurrency)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<select id='currencySelector'>");
            //sb.Append("<option " + (defaultCurrency.Equals("NZD") ? "selected='selected'" : string.Empty) + " value='NZD' rel='1'>New Zealand(NZD)</option>");
            foreach (Currency currency in currencyManager.CurrencyRates)
            {
                sb.Append("<option " + (currency.CountryCode.Contains(defaultCurrency.ToString()) ? "selected='selected'" : string.Empty) + " value='" + currency.CountryCode + "' rel='" + currency.IndicativeRate + "'>" + currency.CountryName + " (" + currency.CountryCode + ")</option>");
            }
            sb.Append("</select>");
            return sb.ToString();
        }
        
        /// <summary>
        /// Payment/ Confirmation page display functionality
        /// </summary>
        /// <param name="bookingData"></param>
        /// <param name="avpId"></param>
        /// <returns></returns>
        public string RenderRentalSummary(AuroraBooking bookingData, string avpId, decimal defaultSurcharge, bool hasSurcharge)
        {

            
            //--- Aded for RWC2011 support remove once over --------------------------
            //DateTime rwcStarts = new DateTime(2011, 9, 1);
            //DateTime rwcEnds = new DateTime(2011, 11, 01);
            bool AddRWCMsg = false;// (bookingData.CountryCode == CountryCode.NZ) && (bookingData.CheckOutTime >= rwcStarts && bookingData.CheckOutTime <= rwcEnds);
            //------------ RWC2011 support ends --------------------------------------
                         
            
            StringBuilder sb = new StringBuilder();
            AvailabilityItem availabilityRow = availabilityResponse.GetAvailabilityItem(avpId);
            //decimal defaultSurcharge = (bookingData.Brand == THLBrands.ExploreMore ? 0 :  2.0m);//TODO: from config
            
            AvailabilityItemChargeRateBand[] availabilityRateBands = availabilityRow.GetRateBands();
            AvailabilityItemChargeDiscount[] aidc = availabilityRow.GetDiscounts();
            //bool hasDiscount = false, hasPercentageDiscount = false;
            bool hasFreeDays = false;

            decimal surchargeAmount = (availabilityRow.DepositAmount * defaultSurcharge);
                        
            sb.Append("<table class='ChargeTable'>");
            sb.Append("<tbody>");
            
            if (availabilityRateBands != null && availabilityRateBands.Length > 0)//add rate bands if any
            {
                foreach (AvailabilityItemChargeRateBand chargeRateRow in availabilityRateBands) //order?
                {
                    string freeDaysStr = string.Empty;
                    if (chargeRateRow.IncludesFreeDay > 0)
                    {
                        freeDaysStr = chargeRateRow.IncludesFreeDay.ToString();
                        hasFreeDays = true;
                    }
                    string fromDateStr = chargeRateRow.FromDate.ToString("dd-MMM-yyyy");
                    string toDateStr = chargeRateRow.ToDate.ToString("dd-MMM-yyyy");
                    sb.Append("<tr>");
                    sb.Append("<td class='title'>Vehicle - Rate Period " + fromDateStr + " to " + toDateStr + "</td>");
                    sb.Append("<td class='desc'>" + chargeRateRow.HirePeriod + " Day(s) x " + currencyStr + " $ " + chargeRateRow.DiscountedRate + "</td>");
                    sb.Append("<td class='cur'>" + currencyStr + "</td>");
                    sb.Append("<td class='price'>$" + chargeRateRow.GrossAmount.ToString("F") + "</td>");
                    sb.Append("</tr>");
                }
            }
            else //single rate band, TODO: revisit formating
            {
                AvailabilityItemChargeRow vehicleRow = availabilityRow.GetVehicleChargeRow();
                sb.Append("<tr>");
                sb.Append("<td lass='title'>" + vehicleRow.ProductName + "</td>");
                sb.Append("<td class='desc'>Included in rate</td>");
                sb.Append("<td class='cur'>" + currencyStr + "</td>");
                sb.Append("<td class='price'>$" + vehicleRow.ProductPrice.ToString("F") + "</td>");
                sb.Append("</tr>");
            }

            //Add Additional Hire Items
            AuroraProduct[] additionalProducts = bookingData.GetBookingProducts(ProductType.All);
                        
            AvailabilityItemChargeRow[] aCharges = availabilityRow.GetNonVehicleCharges();
            if (aCharges != null && aCharges.Length > 0)
            {
                foreach (AvailabilityItemChargeRow charge in aCharges)
                {
                    if (charge.IsBonusPack || charge.AdminFee > 0)
                        continue;
                    else
                    {
                        //----------------LOCFE Fee waved?
                        if ((charge.ProductCode.Contains("ONEW") /*||*/    ) && bookingData.WaiveOneWayFee && bookingData.BonusPackSelected)
                            charge.ProductPrice = 0;                       
                        //------------------------------
                        
                        sb.Append("<tr>");
                        sb.Append("<td class='title'>" + charge.ProductName + "</td>");
                        sb.Append("<td class='desc'>" + (charge.ProductPrice > 0 ? " $" + charge.ProductPrice.ToString("N2") : "Included in rate ") + "</td>");
                        sb.Append("<td class='cur'>" + currencyStr + "</td>");
                        sb.Append("<td class='price'>$" + charge.ProductPrice.ToString("F") + "</td>");
                        sb.Append("</tr>");
                    }
                }
            }
            
            if (additionalProducts != null && additionalProducts.Length > 0)
            {
                foreach (AuroraProduct product in additionalProducts)
                {
                    sb.Append("<tr>");
                    sb.Append("<td class='title'>"+ product.SelectedAmount + " x " + product.Name + "</td>");
                    sb.Append("<td class='desc'>" + product.SelectedAmount + " @ " + currencyStr + " $ " + product.GrossAmount.ToString("N2") + " each</td>");
                    sb.Append("<td class='cur'>" + currencyStr + "</td>");
                    sb.Append("<td class='price'>$" + (product.SelectedAmount * product.GrossAmount).ToString("N2") + "</td>");
                    sb.Append("</tr>");
                }
            }
            
            decimal totalAmountWithAdditionals = (availabilityRow.EstimatedTotal + bookingData.GetTotalNonVehicleCharges(ProductType.All));
            if (bookingData.BonusPackSelected && bookingData.InclusivePack != null)
                totalAmountWithAdditionals += bookingData.InclusivePack.GrossAmount;


            //031109------------
            if (bookingData.OneWayFeeComponent > 0 && bookingData.WaiveOneWayFee && bookingData.BonusPackSelected){
                totalAmountWithAdditionals -= bookingData.OneWayFeeComponent;
            } 
            //------------------

            if ( bookingData.AdminFee > 0 && !bookingData.BonusPackSelected)//Admin Fee added 02/10/09
                totalAmountWithAdditionals = totalAmountWithAdditionals + (totalAmountWithAdditionals * (bookingData.AdminFee / 100));

            decimal totalAmountWithAdditionalsAndSCharge = totalAmountWithAdditionals + totalAmountWithAdditionals * (defaultSurcharge / 100);
            decimal totalDepositWithSCharge = (surchargeAmount + availabilityRow.DepositAmount);
            decimal finalDuePickUp = (totalAmountWithAdditionals - availabilityRow.DepositAmount);
            
            if (availabilityRow.AdminFee > 0 && !bookingData.BonusPackSelected)
            {
                decimal totalPay = (availabilityRow.EstimatedTotal + bookingData.GetTotalNonVehicleCharges(ProductType.All));
                decimal adminFee = totalPay * (availabilityRow.AdminFee / 100);
                sb.Append("<tr><td class='title'>" + availabilityRow.AdminFee.ToString("F") + "% Administration fee</td>");
                sb.Append("<td class='desc'></td>");
                sb.Append("<td class='cur'>" + currencyStr + "</td>");
                sb.Append("<td class='price'>$" + adminFee.ToString("F") + "</td>");
                sb.Append("</tr>");
            }

            sb.Append("<tr class='Totals'>");
            sb.Append("<td class='title'>");
            sb.Append("<small>Total"/* + (bookingData.AdminFee > 0 && !bookingData.BonusPackSelected ?  "(Includes "+ bookingData.AdminFee.ToString("F") +"% Admin Fee)" : string.Empty) */+"</small>");
            sb.Append("</td>");
            sb.Append("<td class='desc'></td>");
            sb.Append("<td class='cur'><small>"+ currencyStr +"</small></td>");
            sb.Append("<td class='price'><small>$" + totalAmountWithAdditionals.ToString("N2") + "</small></td>");//Total whole for booking
            sb.Append("</tr>");                               
            sb.Append("</tbody>");
            sb.Append("</table>");                    
            sb.Append("<table class='ChargeTable PayementTotal'>");
            sb.Append("<tbody>");
            sb.Append("<tr class='PayDeposit'>");
            sb.Append("<td class='title'>Deposit required to secure your booking </td>");
            sb.Append("<td class='desc' style='" + (availabilityRow.DepositPercentage.Equals(0.0m) ? "visibility:hidden" : "") + "'>" + availabilityRow.DepositPercentage + "% of Vehicle Charge</td>");
            sb.Append("<td class='cur'>"+ currencyStr +"</td>");
            sb.Append("<td class='price'>$" + availabilityRow.DepositAmount + "</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='CCSurchargeRow " + (hasSurcharge ? string.Empty : "NoSurcharge") + "'>");
            sb.Append("<td class='title'>Credit Card Surcharge</td>");
            sb.Append("<td class='desc'><span id='surchargePercentage'>" + (defaultSurcharge * 100).ToString("N2") + "</span> % of <small class='surchargePerDesc'>Deposit</small></td>");
            sb.Append("<td class='cur'>"+ currencyStr +"</td>");
            sb.Append("<td class='price'><span id='surchargeAmount'>$" + surchargeAmount.ToString("N2") + "</span></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='Totals'>");
            sb.Append("<td class='title'><small id='sumlabel'>" + (AddRWCMsg ? "Non refundable deposit total" : "Deposit total") +  "<span class='payPeriod'>"+  "(payable today)" +"<span></small></td>");
            sb.Append("<td class='desc'></td>");
            sb.Append("<td class='cur'><small>"+ currencyStr +"</small></td>");
            sb.Append("<td class='price'><small class='TotalDeposit'>$" + totalDepositWithSCharge.ToString("N2") + "</small></td>");
            sb.Append("</tr>");
            if (finalDuePickUp > 0)
            {
                sb.Append("<tr class='PayDeposit'>");
                sb.Append("<td class='title'>Final balance <span class='payPeriod'></span></td>");
                sb.Append("<td class='desc'></td>");
                sb.Append("<td class='cur'>" + currencyStr + "</td>");
                sb.Append("<td class='price PickUpBalance'>$" + finalDuePickUp.ToString("N2") + "</td>");
                sb.Append("</tr>");
            }
            sb.Append("</tbody>");
            sb.Append("</table>");
            return sb.ToString();
        }

        /// <summary>
        /// Bottom Payment Page Totals Displayer 
        /// </summary>
        /// <param name="avpId"></param>
        /// <returns></returns>
        public string RenderPaymentTotalsPanel(AuroraBooking bookingData, decimal defaultSurcharge)
        {
            string avpId = bookingData.AVPID;
            bool hasInclusivePack = bookingData.BonusPackSelected;
            AvailabilityItem availabilityRow = availabilityResponse.GetAvailabilityItem(avpId);            
            
            decimal surchargeAmount = (availabilityRow.DepositAmount * (defaultSurcharge));
            decimal totaldeposit = (surchargeAmount + availabilityRow.DepositAmount);
                       
            
            decimal balanceOnPickUp = (bookingData != null ? availabilityRow.EstimatedTotal + bookingData.GetTotalNonVehicleCharges(ProductType.All) : availabilityRow.EstimatedTotal);
            if (bookingData.BonusPackSelected && bookingData.InclusivePack != null)
                balanceOnPickUp += bookingData.InclusivePack.GrossAmount;

            //-----------------LOCFE 031109
            if (bookingData.WaiveOneWayFee && bookingData.OneWayFeeComponent > 0 && bookingData.BonusPackSelected)
                balanceOnPickUp -= bookingData.OneWayFeeComponent;
            //----------------------

            if (bookingData.AdminFee > 0 && !bookingData.BonusPackSelected)//Admin Fee added 02/10/09
                balanceOnPickUp = balanceOnPickUp + (balanceOnPickUp * (bookingData.AdminFee / 100));
                        
            balanceOnPickUp -= availabilityRow.DepositAmount;
            
            StringBuilder sb = new StringBuilder();
            sb.Append("<span id='totalPricesPanel' class='right'>");
            sb.Append("<input type='hidden' id='totalDepositValue' name='totalDepositValue' value='" + totaldeposit.ToString("F") + "' />");
            sb.Append("<big class='Price TotalDeposit'>" + currencyStr + " $" + totaldeposit.ToString("N2") + "</big>");
            sb.Append("<small class='Price PickUpBalance PayDeposit' id='pickUpTotal'>" + currencyStr + " $" + balanceOnPickUp.ToString("N2") + "</small>");
            sb.Append("</span>");

            if (bookingData.AdminFee > 0 && !bookingData.BonusPackSelected)
                sb.Append("<em>Total includes " + bookingData.AdminFee.ToString("F") + "% admin fee</em>");

            return sb.ToString();
        }

        /// <summary>
        /// Used By Email Form Page
        /// </summary>
        /// <returns></returns>
        public string RenderRequestSummary(AvailabilityRequest availabilityRequest, string vehicleCode)
        {
            AvailabilityItem availabilityRow = availabilityResponse.GetAvailabilityItemForVehicleCode(vehicleCode);
      
            char brandChar = THLBrand.GetBrandChar(availabilityRow.Brand);
            string vehicleName = availabilityRow.VehicleName;            
            string packageName = availabilityRow.PackageCode;
            string passengersStr = (availabilityRequest.NumberOfAdults > 0 ? availabilityRequest.NumberOfAdults + " adult(s) " : string.Empty) + (availabilityRequest.NumberOfChildren > 0 ? availabilityRequest.NumberOfChildren + " children " : string.Empty);

            ApplicationData appData = ApplicationManager.GetApplicationData();
            string pickUpLocationStr  = appData.GetLocationStringForCtrlCode(availabilityRequest.PickUpBranch);// appData.GetAddressForLocationCode(availabilityRequest.PickUpBranch);
            string dropOffLocationStr = appData.GetLocationStringForCtrlCode(availabilityRequest.DropOffBranch);//appData.GetAddressForLocationCode(availabilityRequest.DropOffBranch);
            string pickUpDateStr = availabilityRequest.PickUpDate.ToString("F");
            string dropOffDateStr = availabilityRequest.DropOffDate.ToString("F"); 

            StringBuilder sb = new StringBuilder();
            sb.Append("<table><tr>");
            sb.Append("<td><label>Vehicle:</label></td>");
            sb.Append("<td>" + availabilityRow.Brand + " " + availabilityRow.VehicleName + "</td>");
            sb.Append("</tr>");
            if (passengersStr != string.Empty)
            {
                sb.Append("<tr>");
                sb.Append("<td><label>Passengers:</label></td>");
                sb.Append("<td>" + passengersStr + "</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
            }
            sb.Append("<td><label>Pick up location :</label></td>");
            sb.Append("<td>" + pickUpLocationStr  + "</td>");
            sb.Append("</tr>");
            sb.Append("<td><label>Pick up date:</label></td>");
            sb.Append("<td>" + pickUpDateStr + "</td>");
            sb.Append("</tr>");            
            sb.Append("<tr>");
            sb.Append("<td><label>Drop off location:</label></td>");
            sb.Append("<td>"+ dropOffLocationStr +"</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td><label>Drop off date:</label></td>");
            sb.Append("<td>" + dropOffDateStr + ", AKL</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            return sb.ToString();
        }

        /// <summary>
        /// Render Membership Data Control (NRMA Requirement)
        /// </summary>
        /// <param name="display"></param>
        /// <returns></returns>
        public string RenderLoyaltyField(bool display) {
            StringBuilder sb = new StringBuilder();
            if (display)
            {
                sb.Append(@"
                <li>                    
                    <p>
                        <label for='loyaltyCartTxt'>Membership number:*</label>
                        <input id='loyaltyCartTxt' name='loyaltyCartTxt' />
                    </p>                        
                </li>            
            ");
            }
            else
            {
                sb.Append("<input id='loyaltyCartTxt' type='hidden' name='loyaltyCartTxt' value='0' />");           
            }
            return sb.ToString();
        }
    }
}