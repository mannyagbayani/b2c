﻿using System;
using System.Collections.Generic;
using System.Text;
namespace THL.Booking
{


    public enum MarinStep { SELECTION, CONFIGURE, PAYMENT, SAVEQUOTE, ALTERNATE, BOOKINGCONF, QUOTECONF };

    /// <summary>
    /// Analytics tracking Event Data Structure
    /// </summary>
    public class AnalyticsEventTrackingItem
    {
        public AnalyticsEventTrackingItem(string category, string action, string label, int value)
        {
            Category = category;
            Action = action;
            Label = label;
            Value = value;
            //TODO: proper encapsulation once finalised
        }

        public string Category;//TODO: proper encapsulation once finalised
        public string Action;//TODO: proper encapsulation once finalised
        public string Label;//TODO: proper encapsulation once finalised
        public int Value;//TODO: proper encapsulation once finalised
    }

    /// <summary>
    /// Tracking Codes Displayer Functionality
    /// </summary>
    public class TrackingDisplayer
    {

        THLDomain _siteDomain;
        bool _secureMode;
        string _dnaClientId;
        string _dnaBaseDomain;

        public TrackingDisplayer(THLDomain siteDomain, bool secureMode)
        {
            _siteDomain = siteDomain;
            _secureMode = secureMode;
            _dnaClientId = System.Web.Configuration.WebConfigurationManager.AppSettings["DNAClientCode"];
            _dnaBaseDomain = System.Web.Configuration.WebConfigurationManager.AppSettings["DNABaseURL"];
        }

        /// <summary>
        /// Generate Decide DNA Tracking code for a provided event
        /// </summary>
        /// <param name="eventTypeCode">DNA Event</param>
        /// <returns></returns>
        public string RenderDNATrackingImg(string eventTypeCode)
        {
            TrackingData tData = ApplicationManager.GetTrackingElementForDomainEvent(_siteDomain.Name, eventTypeCode);
            if (tData == null)
                return string.Empty;
            else
            {
                string HTTPPrefix = ApplicationManager.HTTPPrefix;
                string imageHTML = "<img src='" + HTTPPrefix + _dnaBaseDomain + "/n/" + _dnaClientId + "/" + tData.ID + "/" + tData.Code + "/x/e?domain=au.track.decideinteractive.com' width='1' height='1' border='0' />";
                return imageHTML;
            }
        }

        public string RenderDNATrackingImg(string eventTypeCode, string queryParams)
        {
            TrackingData tData = ApplicationManager.GetTrackingElementForDomainEvent(_siteDomain.Name, eventTypeCode);

            if (tData == null)
                return string.Empty;
            else
            {
                string HTTPPrefix = ApplicationManager.HTTPPrefix;
                string imageHTML = "<img src='" + HTTPPrefix + _dnaBaseDomain + "/n/" + _dnaClientId + "/" + tData.ID + "/" + tData.Code + "/x/e?domain=au.track.decideinteractive.com&" + queryParams + "' width='1' height='1' border='0' />";
                return imageHTML;
            }

        }

        /// <summary>
        /// Added to support Everest Tech Tracking 
        /// 
        /// </summary>
        /// <returns></returns>
        public string RenderEverestTechTracking(string analyticsCode, string auroraBookingId, string vehicleName, AuroraBooking bookingItem, AvailabilityRequest requestItem, decimal totalPayment, string pickUpBranchStr, CountryCode country)
        {
            // added as an AirNZ requirement 29-03-2012
            StringBuilder sb = new StringBuilder();
            if (_siteDomain.Name.ToLower().Contains("airnzcampervans.com"))
            {
                sb.Append(@"
                        <!-- AirNZ tracking for " + _siteDomain.Name + @" only  -->
                        <img src='https://pixel.everesttech.net" + @"/1582/t?ev_AirNZ_" + (country.Equals(CountryCode.AU) ? "AUS" : "NZ") + "_Campervans=1&ev_Revenue=" + totalPayment.ToString("F") + @"' width='1' height='1'/>");
            }
            return sb.ToString();
        }

        public string RenderDNATrackingImgSource(string eventTypeCode, string queryParams)
        {
            TrackingData tData = ApplicationManager.GetTrackingElementForDomainEvent(_siteDomain.Name, eventTypeCode);
            if (tData == null)
                return string.Empty;
            else
            {
                string HTTPPrefix = ApplicationManager.HTTPPrefix;
                string imageHTML = HTTPPrefix + _dnaBaseDomain + "/n/" + _dnaClientId + "/" + tData.ID + "/" + tData.Code + "/x/e?domain=au.track.decideinteractive.com&" + queryParams;
                return imageHTML;
            }
        }

        /// <summary>
        /// Render Analytics Code Segment for the specified brand
        /// </summary>
        /// <returns>JavaScript Code Segment</returns>
        public string RenderAnalsCode()
        {
            string[] siteDomainParams = (_siteDomain.Name != null ? _siteDomain.Name.Split('.') : null);
            if (siteDomainParams == null || siteDomainParams.Length < 3)
                return string.Empty;
            else
            {
                string topTLD = (siteDomainParams[siteDomainParams.Length - 1] == "com" ? string.Empty : siteDomainParams[siteDomainParams.Length - 3] + ".") + siteDomainParams[siteDomainParams.Length - 2] + "." + siteDomainParams[siteDomainParams.Length - 1];

                StringBuilder sb = new StringBuilder();
                /*
                sb.Append("<script type=\"text/javascript\">");
                sb.Append("    var gaJsHost = ((\"https:\" == document.location.protocol) ? \"https://ssl.\" : \"http://www.\");");
                sb.Append("    document.write(unescape(\"%3Cscript src='\" + gaJsHost + \"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E\"));");
                sb.Append("</script>");                
                sb.Append("<script type='text/javascript'>\n");
                sb.Append("    try{\n");
                sb.Append("        var pageTracker = _gat._getTracker('" + _siteDomain.UACode + "');\n");
                sb.Append("        pageTracker._setDomainName('." + topTLD + "');\n");
                sb.Append("        pageTracker._setAllowLinker(true);\n");
                sb.Append("        pageTracker._initData();\n");
                sb.Append("        pageTracker._trackPageview();\n");
                sb.Append("    } catch(err) {}\n");
                sb.Append("</script>\n");
                */

                // Code below swapped for Async Analytics tracking 15/11/2010
                sb.Append(@" 
                    <script type='text/javascript'>
                        //async
                        var _gaq = _gaq || [];
                        _gaq.push(['_setAccount', '" + _siteDomain.UACode + @"']);
                        _gaq.push(['_setDomainName', '." + topTLD + @"']);
                        //_gaq.push(['_setAllowHash', false]);
                        _gaq.push(['_trackPageview']);

                        (function() {
                        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                        })();
                    </script>
                ");

                return sb.ToString();
            }
        }


        /// <summary>
        /// Render Analytics Code Segment for the specified brand with Event Tracking Elements
        /// </summary>
        /// <param name="trackingItems">Array of Items to track</param>
        /// <returns></returns>
        public string RenderAnalsCode(AnalyticsEventTrackingItem[] trackingItems)
        {
            string[] siteDomainParams = (_siteDomain.Name != null ? _siteDomain.Name.Split('.') : null);
            if (siteDomainParams == null || siteDomainParams.Length < 3)
                return string.Empty;
            else
            {
                string topTLD = (siteDomainParams[siteDomainParams.Length - 1] == "com" ? string.Empty : siteDomainParams[siteDomainParams.Length - 3] + ".") + siteDomainParams[siteDomainParams.Length - 2] + "." + siteDomainParams[siteDomainParams.Length - 1];

                StringBuilder sb = new StringBuilder();
                // Code below swapped for Async Analytics tracking 15/11/2010
                sb.Append(@" 
                    <script type='text/javascript'>
                        //async
                        var _gaq = _gaq || [];
                        _gaq.push(['_setAccount', '" + _siteDomain.UACode + @"']);
                        _gaq.push(['_setDomainName', '." + topTLD + @"']);
                        //_gaq.push(['_setAllowHash', false]);
                        _gaq.push(['_trackPageview']);
                ");

                foreach (AnalyticsEventTrackingItem tItem in trackingItems)
                    sb.Append(renderAsyncEventLine(tItem));

                sb.Append(@"
                        (function() {
                        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                        })();
                    </script>
                ");

                return sb.ToString();
            }
        }

        /// <summary>
        /// internal helper for rending a single event line
        /// </summary>
        /// <param name="eventItem"></param>
        /// <returns></returns>
        string renderEventLine(AnalyticsEventTrackingItem eventItem)
        {
            //TODO: for future use detect item param length and construct accordingly            
            string jsEventLine = "pageTracker._trackEvent('" + eventItem.Category + "','" + eventItem.Action + "','" + eventItem.Label + "'," + eventItem.Value + ");\n";
            return jsEventLine;
        }

        /// <summary>
        /// internal helper for asynchronious tracking
        /// </summary>
        /// <param name="eventItem"></param>
        /// <returns></returns>
        string renderAsyncEventLine(AnalyticsEventTrackingItem eventItem)
        {
            //TODO: for future use detect item param length and construct accordingly            
            string jsEventLine = "      _gaq.push(['_trackEvent','" + eventItem.Category + "','" + eventItem.Action + "','" + eventItem.Label + "'," + eventItem.Value + "]);\n";
            return jsEventLine;
        }



        public string RenderAnalsConfirmCode(string analyticsCode, string auroraBookingId, string vehicleName, AuroraBooking bookingItem, AvailabilityRequest requestItem, decimal totalPayment, string pickUpBranchStr)
        {


            string[] siteDomainParams = (_siteDomain.Name != null ? _siteDomain.Name.Split('.') : null);
            int leadTime = requestItem.PickUpDate.Subtract(DateTime.Now).Days;
            if (analyticsCode == null || analyticsCode == string.Empty || siteDomainParams == null || siteDomainParams.Length < 3)
                return string.Empty;
            else
            {
                string topTLD = (siteDomainParams[siteDomainParams.Length - 1] == "com" ? string.Empty : siteDomainParams[siteDomainParams.Length - 3] + ".") + siteDomainParams[siteDomainParams.Length - 2] + "." + siteDomainParams[siteDomainParams.Length - 1];
                StringBuilder sb = new StringBuilder();
                sb.Append(@"
                    <script type='text/javascript'>
                          var _gaq = _gaq || [];
                          _gaq.push(['_setAccount', '" + _siteDomain.UACode + @"']);
                          _gaq.push(['_setDomainName', '." + topTLD + @"']);
                          //_gaq.push(['_setAllowHash', false]);
                          _gaq.push(['_trackPageview']);
                          _gaq.push(['_addTrans',
                            '" + auroraBookingId + @"','', // Booking ID - required                            
                            '" + totalPayment.ToString("F") + @"','' // total - required
                            ]);                           
                          _gaq.push(['_addItem',
                            '" + auroraBookingId + @"', // Aur Bid - required
                            '" + pickUpBranchStr + @"', // PickUp Location - required\n
                            '" + vehicleName + @"',        // vehicle name
                            '" + /*leadTime swapped to driver's license: 13/09/2013 :*/ requestItem.CountryOfResidence.ToUpper() + @"',   // drivers license
                            '" + totalPayment.ToString("F") + @"', // unit price - required
                            '1' // quantity - required
                          ]);
                          _gaq.push(['_trackTrans']); //submits transaction to the Analytics servers

                          (function() {
                            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                          })();
                        </script>
                ");


                return sb.ToString();
            }
        }

        private StringBuilder UniversalVariableScript { get; set; }

        /// <summary>
        /// Pivotal Tracker 72227522: Phoenix Confirmation - Implement Universal Variable 
        /// NOT QuoteConfirmationMode
        /// </summary>
        public string UniversalVariableCode(
            Customer customer,
            string bookingId,
            AvailabilityItem availItem,
            AuroraBooking bookingItem,
            AvailabilityRequest availabilityRequest,
            string passengersStr,
            string currencyStr,
            decimal totalPayment,
            string pickUpBranchStr)
        {
            UniversalVariableScript = new StringBuilder();

            //"bookingItem.PaymentInformation.PaymentAmount " include CC surcharge.
            UniversalVariableScript.Append(@"<script> try{  window.universal_variable =
                   {'page':{'category':'confirmation', 'type':'confirmation'},
                    'version':'1.2.0',
                    'user':{'user_id':'Not Available', 'name':'Not Available', 'has_transacted': false, 'twitter_id':'Not Available', 'facebook_id':0, 'returning':false, 'language':'Not Available', 'email':'Not Available', 'username':'Not Available'},
                    'transaction': {
                      'order_id': '" + bookingId + @"',
                      'currency': '" + currencyStr + @"',
                      'payment_type': 'Credit Card',
                      'subtotal': " + totalPayment + @",
                      'subtotal_include_tax': true,
                      'voucher': 'Not Available',
                      'vouchers': ['Not Available'],
                      'voucher_discount': 0,
                      'promotions':  ['Not Available']  ,
                      'promotion_discount': 0,
                      'tax': 0,
                      'shipping_cost': 0,
                      'shipping_method': 'Not Available',
                      'total': " + totalPayment + @",
                      'delivery': {
                        'name': '" + customer.FirstName + " " + customer.LastName + @"',
                        'address': 'Not Available',
                        'city': 'Not Available',
                        'state': 'Not Available',
                        'postcode': 'Not Available',
                        'country': '" + ((availabilityRequest.CountryOfResidence.Equals(string.Empty)) ? "Not Available" : availabilityRequest.CountryOfResidence.ToUpper()) + @"'
                      },
                      'billing': {
                        'name': '" + customer.FirstName + " " + customer.LastName + @"',
                        'address': 'Not Available',
                        'city': 'Not Available',
                        'state': 'Not Available',
                        'postcode': 'Not Available',
                        'country': '" + ((availabilityRequest.CountryOfResidence.Equals(string.Empty)) ? "Not Available" : availabilityRequest.CountryOfResidence.ToUpper()) + @"'
                      },
                      'line_items': [");

            //all the line items in the confirmation 
            //Vehicle Charge
            AvailabilityItemChargeRateBand[] availabilityRateBands = availItem.GetRateBands();

            if (availabilityRateBands != null)//has several rate bands
            {
                decimal totalVehicleCharge = 0;

                foreach (AvailabilityItemChargeRateBand chargeRateRow in availabilityRateBands)
                {
                    totalVehicleCharge += chargeRateRow.GrossAmount;
                }

                //ID: availItem.VehicleCode
                UniversalVariableScript.Append(@" 
                      {'product': {
                        'id': '" + bookingId + @"',
                        'url': 'Not Available',
                        'name': '" + availItem.Brand + " " + availItem.VehicleName + @"',
                        'category': '" + ((availabilityRequest.CountryOfResidence.Equals(string.Empty)) ? "Not Available" : availItem.Brand + " " + availabilityRequest.CountryOfResidence.ToUpper()) + @"',
                        'currency': '" + currencyStr + @"',
                        'unit_price': " + totalVehicleCharge + @",
                        'unit_sale_price': " + totalVehicleCharge + @",
                        'sku_code': '" + pickUpBranchStr + @"',
                        'stock': 0,
                        'size':0,
                        'subcategory':'Not Available',
                        'voucher':'Not Available',
                        'color':'Not Available',
                        'description':'" + availItem.VehicleCode + @"',
                        'manufacturer':'Not Available'
                      },
                      'quantity': 1,
                      'total_discount':0,
                      'subtotal': " + totalVehicleCharge + @"
                    }");
            }
            else //single rate band, TODO: revisit formating
            {
                AvailabilityItemChargeRow vehicleRow = availItem.GetVehicleChargeRow();

                //ID: availItem.Brand + " " + vehicleRow.ProductCode
                UniversalVariableScript.Append(@" 
                      {'product': {
                        'id': '" + bookingId + @"',
                        'url': 'Not Available',
                        'name': '" + vehicleRow.ProductName + @"',
                        'category': '" + ((availabilityRequest.CountryOfResidence.Equals(string.Empty)) ? "Not Available" : availItem.Brand + " " + availabilityRequest.CountryOfResidence.ToUpper()) + @"',
                        'currency': '" + currencyStr + @"',
                        'unit_price': " + vehicleRow.ProductPrice + @",
                        'unit_sale_price': " + vehicleRow.ProductPrice + @",
                        'sku_code': '" + pickUpBranchStr + @"',
                        'stock': 0,
                        'size':0,
                        'subcategory':'Not Available',
                        'voucher':'Not Available',
                        'color':'Not Available',
                        'description':'Not Available',
                        'manufacturer':'Not Available'
                      },
                      'quantity': 1,
                      'total_discount':0,
                      'subtotal': " + vehicleRow.ProductPrice + @"
                    }");
            }

            UniversalVariableScript.Append(",");

            //Waitomo Glowworm Caves driver goes free	
            //Complimentary Tourism Radio Guide	
            AvailabilityItemChargeRow[] aCharges = availItem.GetNonVehicleCharges();

            decimal totalAmountWithAdditionals = (availItem.EstimatedTotal + bookingItem.GetTotalNonVehicleCharges(ProductType.All));

            if (bookingItem.BonusPackSelected && bookingItem.InclusivePack != null)
                totalAmountWithAdditionals += bookingItem.InclusivePack.GrossAmount;

            decimal oneWayFeeWaiver = (bookingItem.WaiveOneWayFee && bookingItem.OneWayFeeComponent > 0 ? bookingItem.OneWayFeeComponent : 0.0m);
            
            if (bookingItem.BonusPackSelected)
                totalAmountWithAdditionals -= oneWayFeeWaiver;

            decimal adminFeeComponent = (bookingItem.AdminFee / 100) * totalAmountWithAdditionals;


            if (aCharges != null && aCharges.Length > 0)
            {
                int i = 0;

                foreach (AvailabilityItemChargeRow charge in aCharges)
                {
                    if (charge.IsBonusPack)
                        continue;

                    if (charge.AdminFee > 0)
                    {
                        if (!bookingItem.BonusPackSelected)
                        {

                            UniversalVariableScript.Append(@" 
                              {'product': {
                                'id': '" + bookingId + @"',
                                'url': 'Not Available',
                                'name': 'BonusPack',
                                'category': '" + GenerateProductCategory(charge.ProductType) + @"',
                                'currency': '" + currencyStr + @"',
                                'unit_price': " + adminFeeComponent + @",
                                'unit_sale_price': " + adminFeeComponent + @",
                                'sku_code': 'BonusPack_sku'"+ i.ToString() +@",
                                'stock': 0,
                                'size':0,
                                'subcategory':'Not Available',
                                'voucher':'Not Available',
                                'color':'Not Available',
                                'description':'Not Available',
                                'manufacturer':'Not Available'
                              },
                              'quantity': 1,
                              'total_discount':0,
                              'subtotal': " + adminFeeComponent + @"
                            },");

                        }
                        continue;
                    }

                    if (charge.HirePeriod > 1)
                    {
                        //ID: ((charge.ProductCode.Equals(string.Empty)) ? "Not Available" : charge.ProductCode)
                        UniversalVariableScript.Append(@" 
                              {'product': {
                                'id': '" + bookingId + @"',
                                'url': 'Not Available',
                                'name': '" + charge.ProductName + @"',
                                'category': 'Vehicle charges',
                                'currency': '" + currencyStr + @"',
                                'unit_price': " + charge.ProductPrice + @",
                                'unit_sale_price': " + charge.ProductPrice + @",
                                'sku_code': '" + ((charge.ProductCode.Equals(string.Empty)) ? "Not Available HirePeriod larger than 1" : charge.ProductCode) + "_sku" + i.ToString() + @"',
                                'stock': 0,
                                'size':0,
                                'subcategory':'Not Available',
                                'voucher':'Not Available',
                                'color':'Not Available',
                                'description':'Not Available',
                                'manufacturer':'Not Available'
                              },
                              'quantity': 1,
                              'total_discount':0,
                              'subtotal': " + charge.ProductPrice + @"
                            },");
                    }
                    else
                    {
                        //ID: ((charge.ProductCode.Equals(string.Empty)) ? "Not Available" : charge.ProductCode)
                        UniversalVariableScript.Append(@" 
                              {'product': {
                                'id': '" + bookingId + @"',
                                'url': 'Not Available',
                                'name': '" + charge.ProductName + @"',
                                'category': 'Vehicle charges',
                                'currency': '" + currencyStr + @"',
                                'unit_price': " + charge.ProductPrice + @",
                                'unit_sale_price': " + charge.ProductPrice + @",
                                'sku_code': '" + ((charge.ProductCode.Equals(string.Empty)) ? "Not Available HirePeriod less than 1" : charge.ProductCode) + "_sku" + i.ToString() + @"',
                                'stock': 0,
                                'size':0,
                                'subcategory':'Not Available',
                                'voucher':'Not Available',
                                'color':'Not Available',
                                'description':'Not Available',
                                'manufacturer':'Not Available'
                              },
                              'quantity': 1,
                              'total_discount':0,
                              'subtotal': " + charge.ProductPrice + @"
                            },");
                    }

                    i++;
                }
            }

            //Accident Liability
            //insurance
            //Add Additional Hire Items
            AuroraProduct[] additionalProducts = bookingItem.GetBookingProducts(ProductType.All);
            if (additionalProducts != null && additionalProducts.Length > 0)
            {
                int i = 0; //sku_code must be unique

                foreach (AuroraProduct product in additionalProducts)
                {
                    //ID: ((product.Code == null || product.Code.Equals(string.Empty)) ? "Not Available" : product.Code)
                    UniversalVariableScript.Append(@" 
                              {'product': {
                                'id': '" + bookingId + @"',
                                'url': 'Not Available',
                                'name': '" + ((product.Name == null) ? "Not Available" : product.Name) + @"',
                                'category': '" + GenerateProductCategory(product.Type.ToString()) + @"',
                                'currency': '" + currencyStr + @"',
                                'unit_price': " + product.GrossAmount + @",
                                'unit_sale_price': " + product.GrossAmount + @",
                                'sku_code': '" + ((product.Code == null || product.Code.Equals(string.Empty)) ? product.Name : product.Code) + "_sku" + i.ToString() + @"',
                                'stock': 0,
                                'size':0,
                                'subcategory':'Not Available',
                                'voucher':'Not Available',
                                'color':'Not Available',
                                'description':'Not Available',
                                'manufacturer':'Not Available'
                              },
                              'quantity': " + product.SelectedAmount + @",
                              'total_discount':0,
                              'subtotal': " + (product.SelectedAmount * product.GrossAmount) + @"
                            },");

                    i++;
                }

            }

            //-----[END] Non Vehicle Charge-----
            UniversalVariableScript.Remove(UniversalVariableScript.Length - 1, 1);

            UniversalVariableScript.Append(@"
                  ]
                }
             };}catch(exception){}
            </script>");

            return UniversalVariableScript.ToString();
        }

        /// <summary>
        /// Insurance = 1,
        /// ExtraHireItem = 2,
        /// FerryCrossing = 3,
        /// All = 4
        /// </summary>
        /// <param name="productType"></param>
        /// <returns></returns>
        private string GenerateProductCategory(string productType)
        {
            if (productType.Contains("Insurance") || productType.Contains("Bonus"))
            {
                return "Accident Liability";
            }
            else if (productType.Contains("Ferry"))
            {
                return "Ferry";
            }
            else
            {
                return "Additional products and services";
            }
        }

        /// <summary>
        /// Pivotal Tracker 72227522: Phoenix Confirmation - Implement Universal Variable 
        /// QuoteConfirmationMode
        /// </summary>
        public string UniversalVariableCode(
            Customer customer,
            string bookingId,
            AuroraQuote quote,
            AuroraBooking bookingItem,
            AvailabilityRequest availabilityRequest,
             string currencyStr,
            decimal totalPayment,
            string pickUpBranchStr)
        {
            UniversalVariableScript = new StringBuilder();

            //"bookingItem.PaymentInformation.PaymentAmount " include CC surcharge.
            UniversalVariableScript.Append(@"<script> try{  window.universal_variable =
                   {'page':{'category':'confirmation', 'type':'confirmation'},
                    'version':'1.2.0',
                    'user':{'user_id':'Not Available', 'name':'Not Available', 'has_transacted': false, 'twitter_id':'Not Available', 'facebook_id':0, 'returning':false, 'language':'Not Available', 'email':'Not Available', 'username':'Not Available'},
                    'transaction': {
                      'order_id': '" + bookingId + @"',
                      'currency': '" + currencyStr + @"',
                      'payment_type': 'Credit Card',
                      'subtotal': " + totalPayment + @",
                      'subtotal_include_tax': true,
                      'voucher': 'Not Available',
                      'vouchers': ['Not Available'],
                      'voucher_discount': 0,
                      'promotions':  ['Not Available']  ,
                      'promotion_discount': 0,
                      'tax': 0,
                      'shipping_cost': 0,
                      'shipping_method': 'Not Available',
                      'total': " + totalPayment + @",
                      'delivery': {
                        'name': '" + customer.FirstName + " " + customer.LastName + @"',
                        'address': 'Not Available',
                        'city': 'Not Available',
                        'state': 'Not Available',
                        'postcode': 'Not Available',
                        'country': '" + ((availabilityRequest.CountryOfResidence.Equals(string.Empty)) ? "Not Available" : availabilityRequest.CountryOfResidence.ToUpper()) + @"'
                      },
                      'billing': {
                        'name': '" + customer.FirstName + " " + customer.LastName + @"',
                        'address': 'Not Available',
                        'city': 'Not Available',
                        'state': 'Not Available',
                        'postcode': 'Not Available',
                        'country': '" + ((availabilityRequest.CountryOfResidence.Equals(string.Empty)) ? "Not Available" : availabilityRequest.CountryOfResidence.ToUpper()) + @"'
                      },
                      'line_items': [");

            //all the line items in the confirmation 
            //-----Vehicle Charge-----
            AvailabilityItemChargeRateBand[] availabilityRateBands = quote.GetRateBands();


            decimal totalVehicleCharge = 0;

            foreach (AvailabilityItemChargeRateBand chargeRateRow in availabilityRateBands)
            {
                totalVehicleCharge += chargeRateRow.GrossAmount;
            }

            UniversalVariableScript.Append(@" 
                      {'product': {
                        'id': '" + bookingId + @"',
                        'url': 'Not Available',
                        'name': '" + quote.VehicleName + @"',
                        'category': '" + ((availabilityRequest.CountryOfResidence.Equals(string.Empty)) ? "Not Available" : quote.VehicleName.Split(' ')[0] + " " + availabilityRequest.CountryOfResidence.ToUpper()) + @"',
                        'currency': '" + currencyStr + @"',
                        'unit_price': " + totalVehicleCharge + @",
                        'unit_sale_price': " + totalVehicleCharge + @",
                        'sku_code': '" + pickUpBranchStr + @"',
                        'stock': 0,
                        'size':0,
                        'subcategory':'Not Available',
                        'voucher':'Not Available',
                        'color':'Not Available',
                        'description':'" + quote.VehicleModel + @"',
                        'manufacturer':'Not Available'
                      },
                      'quantity': 1,
                      'total_discount':0,
                      'subtotal': " + totalVehicleCharge + @"
                    },");


            //-----Non Vehicle Charge-----
            //Extra Hires
            int i = 0;//sku_code must be unique

            foreach (ExtraHireItem product in quote.AddedExtraHireItems)
            {
                UniversalVariableScript.Append(@" 
                              {'product': {
                                'id': '" + bookingId + @"',
                                'url': 'Not Available',
                                'name': '" + product.Name + @"',
                                'category': '" + GenerateProductCategory(product.Type.ToString()) + @"',
                                'currency': '" + currencyStr + @"',
                                'unit_price': " + product.DailyRate + @",
                                'unit_sale_price': " + product.DailyRate + @",
                                'sku_code': '" + product.Name + "_sku" + i.ToString() + @"',
                                'stock': 0,
                                'size':0,
                                'subcategory':'Not Available',
                                'voucher':'Not Available',
                                'color':'Not Available',
                                'description':'Not Available',
                                'manufacturer':'Not Available'
                              },
                              'quantity': " + product.Quantity + @",
                              'total_discount':0,
                              'subtotal': " + product.GrossAmount + @"
                            },");

                i++;
            }

            //Insurance
            foreach (InsuranceProduct product in quote.AddedInsurance)
            {
                UniversalVariableScript.Append(@" 
                              {'product': {
                                'id': '" + bookingId + @"',
                                'url': 'Not Available',
                                'name': '" + product.Name + @"',
                                'category': '" + GenerateProductCategory(product.Type.ToString()) + @"',
                                'currency': '" + currencyStr + @"',
                                'unit_price': " + product.GrossAmount + @",
                                'unit_sale_price': " + product.GrossAmount + @",
                                'sku_code': '" + product.Name + "_sku" + i.ToString() + @"',
                                'stock': 0,
                                'size':0,
                                'subcategory':'Not Available',
                                'voucher':'Not Available',
                                'color':'Not Available',
                                'description':'Not Available',
                                'manufacturer':'Not Available'
                              },
                              'quantity': 1,
                              'total_discount': 0,
                              'subtotal': " + product.GrossAmount + @"
                            },");

                i++;
            }

            //Ferry
            if (quote.FerryProducts.Length == 2 && quote.FerryProducts[0].Quantity > 0)
            {
                foreach (PersonRateInfo person in quote.FerryProducts[1].FerryData.PersonRateCollection)
                {
                    if (person.NumPassengers > 0)
                    {
                        UniversalVariableScript.Append(@" 
                              {'product': {
                                'id': '" + bookingId + @"',
                                'url': 'Not Available',
                                'name': '" + person.PersonInfoType + @"',
                                'category': '" + GenerateProductCategory("Ferry") + @"',
                                'currency': '" + currencyStr + @"',
                                'unit_price': " + person.Rate + @",
                                'unit_sale_price': " + person.Rate + @",
                                'sku_code': '" + person.PersonInfoType + "_sku" + i.ToString() + @"',
                                'stock': 0,
                                'size':0,
                                'subcategory':'Not Available',
                                'voucher':'Not Available',
                                'color':'Not Available',
                                'description':'Not Available',
                                'manufacturer':'Not Available'
                              },
                              'quantity': " + person.NumPassengers + @",
                              'total_discount': 0,
                              'subtotal': " + person.Rate * person.NumPassengers + @"
                            },");
                    }

                    i++;
                }
            }

            UniversalVariableScript.Remove(UniversalVariableScript.Length - 1, 1);

            //-----[END] Non Vehicle Charge-----

            UniversalVariableScript.Append(@"
                  ]
                }
             };}catch(exception){}
            </script>");

            return UniversalVariableScript.ToString();
        }

        public TrackingData GetAdWordsElement()
        {
            TrackingData tData = ApplicationManager.GetTrackingElementForDomainEvent(_siteDomain.Name, "adwordsPageload");//TODO:
            return tData;
        }

        public string GetSealID()
        {
            TrackingData tData = ApplicationManager.GetTrackingElementForDomainEvent(_siteDomain.Name, "sealLoad");//TODO:
            return (tData != null ? tData.ID : "missingSealID");
        }


        /// <summary>
        /// Generate Marin Tracking image pixel tag for supplied parameters and booking step
        /// </summary>
        /// <param name="marinQueryString">passed marine query params</param>
        /// <param name="step">Current Booking Step</param>
        /// <param name="total">Total Booking, if applicable</param>
        /// <param name="bookingId">Aurora ID</param>
        /// <param name="currency">current booking currency</param>
        /// <param name="guid">Unique GUID for this session</param>
        /// <returns></returns>
        public string RenderMarinTrackingPixelIMG(string marinQueryString, MarinStep step, decimal total, string bookingId, string currencyStr)
        {

            string imgElmTag = string.Empty;
            string guid = string.Empty;
            string refPage = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(marinQueryString))
                {
                    string[] queryParams = marinQueryString.Split(',');
                    foreach (string qParam in queryParams)
                    {
                        string[] paramdata = qParam.Split('|');
                        switch (paramdata[0])
                        {
                            case "ref-page":
                                break;
                                refPage = paramdata[1];
                            case "uuid":
                                guid = paramdata[1];
                                break;
                        }
                    }
                }
                else
                {
                    //Session has no marin context to initialise, TODO: consider defaults
                }
                if (string.IsNullOrEmpty(currencyStr))
                    currencyStr = _siteDomain.Country == CountryCode.NZ ? "NZD" : "AUD";//TODO: add USA if needed
                string cid = "1624rrj16003";//TODO: consider moving into config, THL specific?
                string trackingBase = "tracker.marinsm.com";
                string conversionTypeId = getConversionTypeId(step);
                if (total == 0.0m)//step not goal
                {
                    imgElmTag = @"
                    <!-- BEGIN: Marin Software Tracking Script --> 
                    <script type=""text/javascript""> 
                        var _mTrack = _mTrack || []; 
                        _mTrack.push(['addTrans', { items : [{ convType : '" + conversionTypeId + @"' }] }]); 
                        _mTrack.push(['processOrders']); 
                        (function() { 
                            var mClientId = '" + cid + @"'; 
                            var mProto = ('https:' == document.location.protocol ? 'https://' : 'http://'); 
                            var mHost = '" + trackingBase + @"'; 
                            var mt = document.createElement('script'); 
                            mt.type = 'text/javascript'; 
                            mt.async = true; 
                            mt.src = mProto + mHost + '/tracker/async/' + mClientId + '.js'; 
                            var fscr = document.getElementsByTagName('script')[0]; 
                            fscr.parentNode.insertBefore(mt, fscr); 
                        })(); 
                    </script> 
                    <noscript><img src=""https://" + trackingBase + @"/tp?act=2&cid=" + cid + @"&script=no"" ></noscript> 
                    <!-- END: Copyright Marin Software -->
                    ";
                }
                else
                {//transaction goal
                    imgElmTag = @"
                    <!-- BEGIN: Marin Software Tracking Script --> 
                    <script type=""text/javascript""> 
                        var _mTrack = _mTrack || []; 
                        _mTrack.push(['addTrans', { orderId : '" + bookingId + @"', total : '" + total + @"', currency : '" + currencyStr + @"', items : [ { convType : '" + conversionTypeId + @"', orderId : '" + bookingId + @"', product : '', price : '" + total + @"', category : '', quantity : '1' } ] }]); 
                        _mTrack.push(['processOrders']); 
                        (function() { 
                            var mClientId = '" + cid + @"'; 
                            var mProto = ('https:' == document.location.protocol ? 'https://' : 'http://'); 
                            var mHost = '" + trackingBase + @"'; 
                            var mt = document.createElement('script'); 
                            mt.type = 'text/javascript'; 
                            mt.async = true; 
                            mt.src = mProto + mHost + '/tracker/async/' + mClientId + '.js'; 
                            var fscr = document.getElementsByTagName('script')[0]; 
                            fscr.parentNode.insertBefore(mt, fscr); 
                        })(); 
                    </script>
                ";
                }
                return imgElmTag;
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Application, "TrackingDisplayer.RenderMarinTrackingPixelIMG", ex.Message, "{'query':'" + marinQueryString + "'}");
                return "<span style='display:none;'>" + ex.Message + "</span>";
            }
        }

        /// <summary>
        /// Get the marin conversion (state) identifier
        /// </summary>
        /// <returns></returns>
        string getConversionTypeId(MarinStep conversionStep)
        {
            string contypeId = string.Empty;

            switch (conversionStep)
            {
                case MarinStep.SELECTION:
                    contypeId = "m1";
                    break;
                case MarinStep.ALTERNATE:
                    contypeId = "alternate";
                    break;
                case MarinStep.CONFIGURE:
                    contypeId = "m2";
                    break;
                case MarinStep.PAYMENT:
                    contypeId = "m3";
                    break;
                case MarinStep.SAVEQUOTE:
                    contypeId = "m4";
                    break;
                case MarinStep.BOOKINGCONF:
                    contypeId = "booking";
                    break;
                case MarinStep.QUOTECONF:
                    contypeId = "confirmation";
                    break;
            }

            contypeId = _siteDomain.Brand.ToString().ToLower() + "-" + contypeId + (_siteDomain.IsAggregator ? "" : "-" + _siteDomain.Country.ToString().ToLower());

            return contypeId;
        }

        /// <summary>
        /// Renders a ClickTale tracking snippet
        /// </summary>
        /// <param name="documentPosition">Top or Bottom </param>
        /// <returns></returns>
        public string RenderClickTaleTracking(string documentPosition)
        {
            StringBuilder sb = new StringBuilder();
            documentPosition = documentPosition.ToLower();
            if (documentPosition.Equals("top"))
            {
                sb.Append(@" 
    <!-- ClickTale Top part -->
    <script type='text/javascript'>
    var WRInitTime=(new Date()).getTime();
    </script>
    <!-- ClickTale end of Top part -->
                ");
            }
            else
            {
                sb.Append(@"
    <div id='ClickTaleDiv' style='display: none;'></div>
    <script type='text/javascript'>
    document.write(unescape(""%3Cscript%20src='""+ (document.location.protocol=='https:'?
      'https://clicktale.pantherssl.com/':
      'http://s.clicktale.net/')+
     ""WRb6.js'%20type='text/javascript'%3E%3C/script%3E""));
    </script>
    <script type=""text/javascript"">
    var ClickTaleSSL=1;
    if(typeof ClickTale=='function') ClickTale(6300,0.80,""www07"");
    </script>
                ");
            }
            return sb.ToString();
        }

        /// <summary>
        /// render remarketing tag snippet across all pages.
        /// TODO: consider refactoring the remarketing list ID into config (domains.xml) for future 
        /// support of multiple remarketing channels.
        /// </summary>
        /// <returns></returns>
        public string RenderRemarketingSnippets()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"
    <!-- Google Code for Remarketing Tag -->
    <script type=""text/javascript"">
    /* <![CDATA[ */
    var google_conversion_id = 982569359;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
    </script>
    <script type=""text/javascript"" src=""//www.googleadservices.com/pagead/conversion.js"">
    </script>
    <noscript>
    <div style=""display:inline;"">
    <img height=""1"" width=""1"" style=""border-style:none;"" alt="""" src=""//googleads.g.doubleclick.net/pagead/viewthroughconversion/982569359/?value=0&amp;guid=ON&amp;script=0""/>
    </div>
    </noscript>
            ");
            return sb.ToString();
        }
    }
}