﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Xml;

namespace THL.Booking
{
    /// <summary>
    /// Vehicle functionality Displayer
    /// </summary>
    public class VehicleDisplayer
    {
        private Vehicle vehicle;
        private Vehicle[] vehicles;
        
        public VehicleDisplayer(){
            
        }
        
        public VehicleDisplayer(Vehicle _vehicle)
        {
            vehicle = _vehicle;
        }

        public VehicleDisplayer(Vehicle[] _vehicles)
        {
            vehicles = _vehicles;
        }

        public string Render(string defaultVehicleCode, string defaultVehicleName)
        {
            if (defaultVehicleCode == null)
                defaultVehicleCode = string.Empty;
            StringBuilder sb = new StringBuilder("<select class='SearchBox_FullListBox' id='ctrlSearchBox_dropVehicleType' name='ctrlSearchBox_dropVehicleType'>");
            sb.Append("<option value='-1'>Search all</option>");
            bool hasSelected = false;
            if (vehicles != null)
            {
                foreach (Vehicle vcl in vehicles)
                {
                    if (defaultVehicleCode.Equals(vcl.Code))
                    {
                        hasSelected = true;
                        sb.Append("<option selected='selected' value='" + vcl.Code + "'>" + vcl.Name + "</option>");
                    }
                    else
                    {
                        sb.Append("<option value='" + vcl.Code + "'>" + vcl.Name + "</option>");
                    }
                }
            }
            if (hasSelected == false && defaultVehicleCode != string.Empty)
                sb.Append("<option selected='selected' value='" + defaultVehicleCode + "'>" + defaultVehicleName + "</option>");
            sb.Append("</select>");
            return sb.ToString();            
        }
        
        public string VehicleChargesJSON(decimal basePrice, decimal bonusPackPrice, bool bonusPackSelected) {
            StringBuilder sb = new StringBuilder();
            sb.Append("{");
            sb.Append("vehicleBase:" + basePrice + ",");
            sb.Append("bonusPack:" + bonusPackPrice + ",");
            sb.Append("bonusPackSelected:" + (bonusPackSelected ? "1" : "0"));
            sb.Append("}");
            return sb.ToString();
        }    
    
        /// <summary>
        /// Render rental Charges for vehicle quote page
        /// </summary>
        /// <param name="vehiclCharges"></param>
        /// <param name="vehiclChargeLink"></param>
        /// <param name="RentalInfoNode">TODO: this needs to be replaced by RateBands Collection</param>
        /// <returns></returns>
        public static string RenderQuoteVehicleCharges(BookingFee[] vehiclCharges, string vehiclChargeLink, XmlNode RentalInfoNode, bool hasDiscountDayPrice, AuroraQuote quote)
        {
            decimal adminFee = 0.0m;
            //--TODO: swap this section with rateband implementaion, bizlogic in view..
            decimal vehicleTotalCharge = 0.0m, vehicleRateBandsCharge= 0.0m, hirePeriod = 0.0m, percentageComponent = 0.0m;
            decimal.TryParse(RentalInfoNode.Attributes["VehicleCharge"].Value, out vehicleRateBandsCharge);
            string pickUpDateStr = RentalInfoNode.Attributes["PickupDateForDisp"].Value;
            string currencyCode = RentalInfoNode.Attributes["CurrencyCode"].Value;
            string dropOffDateStr = RentalInfoNode.Attributes["DropOffDateForDisp"].Value;
            decimal perDayPrice = 0.0m;
            decimal.TryParse(RentalInfoNode.Attributes["HirePeriod"].Value, out hirePeriod);
            perDayPrice = vehicleRateBandsCharge / hirePeriod;
            //--TODO: end of swap this section with ratebands implementaion 
            
            vehicleTotalCharge = vehicleRateBandsCharge;
            foreach (BookingFee fee in vehiclCharges) {
                percentageComponent += fee.IsPercentageFee ? fee.GrossCalcPercentage : 0.0m;
                vehicleTotalCharge += fee.GrossAmount;            
            }
            StringBuilder feeBuilder = new StringBuilder();
            
            //feeBuilder.Append("<tr><td class='rt'>" + pickUpDateStr + @" to " + dropOffDateStr + @"</td><td class='fd'></td><td class='hp'>" + hirePeriod + @" day(s)</td><td class='pdp'>$" + perDayPrice.ToString("F") + @"</td><td class='dpd'>$" + perDayPrice.ToString("F") + @"</td><td>$" + vehicleRateBandsCharge.ToString("F") + @"</td></tr>");
            foreach (AvailabilityItemChargeRateBand rb in quote.GetRateBands())
                feeBuilder.Append("<tr><td class='rt'>" + rb.FromDate.ToString("dd-MMM-yyyy") + @" to " + rb.ToDate.ToString("dd-MMM-yyyy") + @"</td><td class='fd'></td><td class='hp'>" + rb.HirePeriod + @" day(s)</td><td class='pdp'>$" + rb.OriginalRate.ToString("F") + @"</td><td class='dpd'>$" + rb.DiscountedRate.ToString("F") + @"</td><td>$" +  rb.GrossAmount +@"</td></tr>");
                        
            foreach (BookingFee fee in vehiclCharges)
            {
                if (fee.ShortName.Equals("STADMIN"))
                {
                    adminFee = fee.GrossAmount;
                    continue; // do not show admin component 150211
                }   
                feeBuilder.Append(@"
                                <tr rel=" + fee.ShortName + @" class=" + fee.ShortName + @" ><td colspan='4'>" + fee.Name + @"</td><td class='pr' rel='" + fee.GrossAmount.ToString("F") + @"'>" + "$" + (fee.GrossAmount > 0.0m ? fee.GrossAmount.ToString("F") : "included")  + @"</td></tr>       
                ");                  
            }

            vehicleTotalCharge = vehicleTotalCharge - adminFee;//To not include admin fee component in total as its has to recalc on client totals

            StringBuilder sb = new StringBuilder();
            sb.Append(@"
                    <script>
                        var vCharges = {'cur':'" + currencyCode + @"',vBase:'" + vehicleTotalCharge.ToString("F") + @"', 'vTotal': " + vehicleTotalCharge.ToString("F") + @" ,'admin':" + percentageComponent + @", 'loadedAdmin':" + percentageComponent + @", 'fees':" + RenderEnforcedFeesJSON(vehiclCharges) + @"};                 
                    </script>
                    <div id='vehicleChargeDetails' class='sections'>
                    <div class='sectionHeader'>
                        <h4>Vehicle charges:&nbsp;</h4>
                        <a href='http://" + vehiclChargeLink + @"' class='Quest PopUp'>?</a>
                        <span class='openclose'><i class='icon-plus'></i></span>
                    </div>
                    <div class='sectionContent'>
                        <span id='ChargeHeader' >
                            <span class='Title'>                            
                                <small>Vehicle Charge</small>
                                <a href='#details' class='ShowPriceList'>Close Details</a>
                            </span>
                            <span class='Total'>
                                <small>" + currencyCode + @" $ "+ vehicleTotalCharge.ToString("F") + @"</small>
                            </span>
                        </span>                    
                        <div class='PriceDetailsList NoFreeDays'>
                            <table class='chargesTable'>
                                <thead>
                                    <tr>
                                        <th class='RateTH'>Product Description</th>
                                        <th class='FreeTH'><span>Free Days</span><span rel='freeDays' class='popup'>[?]</span></th>
                                        <th class='HireTH'>Hire Days</th>
                                        <th class='PerTH'>Per Day Price</th>
                                        <th class='DiscTH'><span>Discounted Day Price</span><!--<span rel='DiscountedDaysPopUp' class='popup' onmouseover='initPopUp(this)'>[?]</span>--></th><th class='TotalTH'>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    " + feeBuilder.ToString() + @"
                                </tbody>
                            </table>
                            <div class='DiscountedDaysPopUp bubbleBody'>
                                <h3>Discounted Day Price</h3>
                                <table>
                                    <tbody>
                                        <tr><td>Long Hire Discount</td><td>5.00%</td></tr><tr class='total' rel='5.00'><td>Total Discount</td><td>5.00%</td></tr>
                                    </tbody>
                                </table>
                                <span class='note'>(off standard per day rates)</span>
                            </div>
                        </div>
                    </div>                   
                </div>
            ");            
            return sb.ToString();
        }

        /// <summary>
        /// Format Enforced Fees JSON
        /// </summary>
        /// <param name="fees">Fees Collection</param>
        /// <returns>Fees Array JSON</returns>
        public static string RenderEnforcedFeesJSON(BookingFee[] fees)
        {
            StringBuilder sb = new StringBuilder();
            foreach (BookingFee fee in fees) {
                sb.Append("{'name':'" + fee.ShortName + "','isPer':" + (fee.IsPercentageFee ? "1" : "0") + ",'amount':" + fee.GrossAmount + ",'waive':0},");
            }
            return "[" + sb.ToString().TrimEnd(',') + "]";
        }

    }//VehicleDisplayer   
}