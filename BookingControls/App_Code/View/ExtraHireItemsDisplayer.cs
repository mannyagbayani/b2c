﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace THL.Booking
{
    /// <summary>
    /// Displayer Functionality for Extra Hire Items 
    /// </summary>
    public class ExtraHireItemsDisplayer
    {
        ExtraHireItem[] items;
        int hirePeriod;
        AvailabilityRequest _availabilityRequest;
        AvailabilityItem _availabilityItem;

        public ExtraHireItemsDisplayer(ExtraHireItem[] _items, int _hirePeriod, AvailabilityRequest availabilityrequest, AvailabilityItem availabilityItem)
        {
            items = _items;
            hirePeriod = _hirePeriod;
            _availabilityItem = availabilityItem;
            _availabilityRequest = availabilityrequest;             
        }

        public string RenderExtraHirePanel()
        {

            VehicleType contentResourceVehicleType = _availabilityRequest.VehicleType;
            if (_availabilityItem.VehicleCode.ToLower().Contains("pfmr")) contentResourceVehicleType = VehicleType.Campervan;//Added exception 08/01/10 for PFMR

            string extraHireContentURL = ContentManager.GetLinkElementForBrand(_availabilityItem.Brand, contentResourceVehicleType, _availabilityRequest.CountryCode, "ExtraHireItems", _availabilityRequest.PickUpDate, THLBrands.AIRNZ).Content;
            if (extraHireContentURL != null && extraHireContentURL.Length > 0) extraHireContentURL = extraHireContentURL.Replace("&amp;", "&");
      
            int maxAmount = 0; 
            int.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["MaxHireItems"], out maxAmount);
            maxAmount = (maxAmount>0 ? maxAmount : 5);
            
            StringBuilder sb = new StringBuilder();
                        
            sb.Append("<h4>Additional products and services:&nbsp;</h4>");
            sb.Append("<a href='http://" + extraHireContentURL + "' class='Quest PopUp'>?</a>");
            
            sb.Append("<table>");

            var sortedItems =
            from i in items
            orderby i.DisplayOrder
            select i;

            foreach (ExtraHireItem item in sortedItems)
            {               
                if(item.PromptQuantity)
                {
                    sb.Append("<tr>");
                    sb.Append("<td class='cb'>");
                    sb.Append(getItemDropDown(item.SapId, maxAmount));
                    sb.Append("</td>");
                    sb.Append("<td class='desc'>");
                    sb.Append("<label>" + item.Name + (item.ProductInfo.Length > 0 ? " ("+ item.ProductInfo +")" : string.Empty)  + "</label>");                    
                    sb.Append("</td>");
                    if (item.UOM == UOMType.None)
                    {
                        sb.Append("<td class='days'>" + "</td>");
                        sb.Append("<td class='price'><span class='rate'>$ " + item.DailyRate + " each " + (item.MaxCharge > 0 ? "</span><span class='max'>Max $" + item.MaxCharge.ToString("F") + " per hire</span>" : string.Empty) + "</td>");
                    }
                    else if (item.UOM == UOMType.Day)
                    {
                        sb.Append("<td class='days'>" + hirePeriod + " day(s)</td>");
                        sb.Append("<td class='price'><span class='rate'>$ " + item.DailyRate + " per day " + (item.MaxCharge > 0 ? "</span><span class='max'> Max $" + item.MaxCharge.ToString("F") + " per hire" : string.Empty) + "</td>");
                    }                   
                    sb.Append("<td class='total'></td>");//express checkout population here
                    sb.Append("</tr>");
                } 
                else
                {
                    sb.Append("<tr>");
                    sb.Append("<td class='cb'>");
                    sb.Append("<input type='checkbox' id='ehi_"+ item.SapId  +"' name='ehi_"+ item.SapId +"' />");
                    sb.Append("</td>");
                    sb.Append("<td class='desc'>");
                    sb.Append("<label>" + item.Name + (item.ProductInfo.Length > 0 ? " (" + item.ProductInfo + ")" : string.Empty) + "</label>");
                    sb.Append("</td>");
                    sb.Append("<td class='days'>" + (item.UOM == UOMType.Day || item.UOM == UOMType.H24 ? hirePeriod + " day(s)" : "&nbsp;") + "</td>");
                    sb.Append("<td class='price'><span class='rate'>$ " + item.DailyRate + (item.UOM == UOMType.Day ? " per day" : " each") + "</span>" + (item.MaxCharge > 0 ? "<span class='max'> Max $" + item.MaxCharge.ToString("F") + " per hire</span>" : string.Empty) + "</td>");
                    sb.Append("<td class='total'>&nbsp;<td>");
                    sb.Append("</tr>");
                }
            }
            sb.Append("</table>");
            sb.Append("<input type='hidden' id='ehi_total' name='ehi_total' />");
            return sb.ToString();
        }


        public string RenderExtraHirePanelResponsive()        {

            VehicleType contentResourceVehicleType = _availabilityRequest.VehicleType;
            if (_availabilityItem.VehicleCode.ToLower().Contains("pfmr")) contentResourceVehicleType = VehicleType.Campervan;//Added exception 08/01/10 for PFMR

            string extraHireContentURL = ContentManager.GetLinkElementForBrand(_availabilityItem.Brand, contentResourceVehicleType, _availabilityRequest.CountryCode, "ExtraHireItems", _availabilityRequest.PickUpDate, THLBrands.AIRNZ).Content;
            if (extraHireContentURL != null && extraHireContentURL.Length > 0) extraHireContentURL = extraHireContentURL.Replace("&amp;", "&");

            int maxAmount = 0;
            int.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["MaxHireItems"], out maxAmount);
            maxAmount = (maxAmount > 0 ? maxAmount : 5);

            StringBuilder sb = new StringBuilder();
            sb.Append(@"
        <div class=""sectionHeader"">
            <h4>Additional products and services:&nbsp;</h4>
            <a href=""http://" + extraHireContentURL + @""" class=""Quest PopUp"">?</a>
            <span class=""openclose""><i class=""icon-plus""></i></span>
        </div>
        <div class=""sectionContent"">
            <table>");
            var sortedItems =
            from i in items
            orderby i.DisplayOrder
            select i;

            foreach (ExtraHireItem item in sortedItems)
            {
                if (item.PromptQuantity)
                {
                    sb.Append(@"
                <tr>
                    <td class=""cb"">");
                    sb.Append(getItemDropDown(item.SapId, maxAmount));
                    sb.Append("</td>");
                    sb.Append("<td class='desc'>");
                    sb.Append("<label>" + item.Name + (item.ProductInfo.Length > 0 ? " (" + item.ProductInfo + ")" : string.Empty) + "</label>");
                    sb.Append("</td>");
                    if (item.UOM == UOMType.None)
                    {
                        sb.Append("<td class='days'>" + "</td>");
                        sb.Append("<td class='price'><span class='rate'>$ " + item.DailyRate + " each " + (item.MaxCharge > 0 ? "</span><span class='max'>Max $" + item.MaxCharge.ToString("F") + " per hire</span>" : string.Empty) + "</td>");
                    }
                    else if (item.UOM == UOMType.Day)
                    {
                        sb.Append("<td class='days'>" + hirePeriod + " day(s)</td>");
                        sb.Append("<td class='price'><span class='rate'>$ " + item.DailyRate + " per day " + (item.MaxCharge > 0 ? "</span><span class='max'> Max $" + item.MaxCharge.ToString("F") + " per hire" : string.Empty) + "</td>");
                    }
                    sb.Append("<td class='total'></td>");//express checkout population here
                    sb.Append("</tr>");
                }
                else
                {
                    sb.Append(@"
                <tr>");
                    sb.Append("<td class='cb'>");
                    sb.Append("<input type='checkbox' id='ehi_" + item.SapId + "' name='ehi_" + item.SapId + "' />");
                    sb.Append("</td>");
                    sb.Append("<td class='desc'>");
                    sb.Append("<label>" + item.Name + (item.ProductInfo.Length > 0 ? " (" + item.ProductInfo + ")" : string.Empty) + "</label>");
                    sb.Append("</td>");
                    sb.Append("<td class='days'>" + (item.UOM == UOMType.Day || item.UOM == UOMType.H24 ? hirePeriod + " day(s)" : "&nbsp;") + "</td>");
                    sb.Append("<td class='price'><span class='rate'>$ " + item.DailyRate + (item.UOM == UOMType.Day ? " per day" : " each") + "</span>" + (item.MaxCharge > 0 ? "<span class='max'> Max $" + item.MaxCharge.ToString("F") + " per hire</span>" : string.Empty) + "</td>");
                    sb.Append("<td class='total'>&nbsp;<td>");
                    sb.Append(@"
                </tr>");
                }
            }
            sb.Append(@"
            </table>
            <input type='hidden' id='ehi_total' name='ehi_total' />
        </div>");
            return sb.ToString();
        }


        public string RenderExtraHireJSON() {
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            foreach (ExtraHireItem item in items)
            {
                sb.Append("{");
                sb.Append("type:'ins',id:'"+ item.SapId + "',name:'"+ item.Name +"',maxCharge:"+ item.MaxCharge + ", dailyRate :"+ item.DailyRate + ",minAge:"+ item.MinAge +",maxAge:"+ item.MaxAge + ", promptQuant: '"+ item.PromptQuantity + "', uom:'"+ item.UOM  +"',numItems:0, grossAmt:"+ item.GrossAmount +", charge: 0");
                sb.Append("},");
            }
            string result = sb.ToString().TrimEnd(',') + "]";
            return result;
        }
        
        string getItemDropDown(string sapId, int maxAmount)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<select id='ehi_" + sapId + "' name='ehi_" + sapId + "'>");
            for (int i = 0; i <= maxAmount; i++)
            {
                sb.Append("<option value='"+ i+"'>"+ i +"</option>");
            }
            sb.Append("</select>");
            return sb.ToString();
        }

        /// <summary>
        /// Added for Save Quote
        /// </summary>
        /// <param name="sapId"></param>
        /// <param name="maxAmount"></param>
        /// <returns></returns>
        static string renderItemDropDown(string sapId, int maxAmount, int defaultValue)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<select id='ehi_" + sapId + "' name='ehi_" + sapId + "'>");
            for (int i = 0; i <= maxAmount; i++)
            {
                sb.Append("<option value='" + i + "' "+ (i==defaultValue ? " selected='selected'" : string.Empty) +">" + i + "</option>");
            }
            sb.Append("</select>");
            return sb.ToString();
        }


        public static string SerializeExtraHireProduct(ExtraHireItem ehProduct)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{product:" + ehProduct.ToString());
            sb.Append(",quantity:" + ehProduct.Quantity + "}");
            return sb.ToString();
        }

        public static string RenderExtraHireQuotePanel(ExtraHireItem[] hireProductOptions, ExtraHireItem[] selectedHires, AvailabilityRequest availabilityRequest)
        {

            decimal totalExtraHireAmount = 0;
            Dictionary<string, ExtraHireItem> selectedItemsDic = null;
            string selectedHiresJSON = "[]", availableHiresJSON = string.Empty;
            if (selectedHires != null && selectedHires.Length > 0)
            {
                selectedHiresJSON = "[";
                selectedItemsDic = new List<ExtraHireItem>(selectedHires).ToDictionary(p => p.SapId);
                foreach (ExtraHireItem item in selectedHires)
                {
                    selectedHiresJSON += "{'id':'" + item.SapId + "','qty':" + item.Quantity + ",'max':" + item.MaxCharge.ToString("F") + ",'total':" + item.GrossAmount + "},";
                    totalExtraHireAmount += item.GrossAmount;
                }
                selectedHiresJSON = selectedHiresJSON.TrimEnd(',');           
                selectedHiresJSON +=  "]";
            }
            if (hireProductOptions != null && hireProductOptions.Length > 0)
            {
                availableHiresJSON = "[";
                foreach (ExtraHireItem item in hireProductOptions)
                    availableHiresJSON += "{'id':'" + item.SapId + "','qty':" + item.Quantity + ",'max':" + item.MaxCharge.ToString("F") + ",'prc':"+ item.DailyRate +",'uom':'" + item.UOM + @"'},";
                availableHiresJSON = availableHiresJSON.TrimEnd(',');
                availableHiresJSON += "]";
                
            }

            THLBrands currentBrand = THLBrand.GetBrandForString(availabilityRequest.Brand);            
            VehicleType contentResourceVehicleType = availabilityRequest.VehicleType;
            if (availabilityRequest.VehicleModel.ToLower().Contains("pfmr")) contentResourceVehicleType = VehicleType.Campervan;//Added exception 08/01/10 for PFMR

            string extraHireContentURL = ContentManager.GetLinkElementForBrand(currentBrand, contentResourceVehicleType, availabilityRequest.CountryCode, "ExtraHireItems", availabilityRequest.PickUpDate, THLBrands.AIRNZ).Content;
            if (extraHireContentURL != null && extraHireContentURL.Length > 0) extraHireContentURL = extraHireContentURL.Replace("&amp;", "&");

            int maxAmount = 0;
            int.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["MaxHireItems"], out maxAmount);
            maxAmount = (maxAmount > 0 ? maxAmount : 5);
            int hirePeriod = availabilityRequest.GetHirePeriod();
             
            StringBuilder sb = new StringBuilder();
            
            sb.Append(@"
            <script>
                var selectedHires = {'hirePeriod':"+ hirePeriod +@",'cur':'"+ CurrencyManager.GetCurrencyForCountryCode(availabilityRequest.CountryCode) +  @"','total':"+ totalExtraHireAmount + @" ,'selected':" + selectedHiresJSON + @"};
                var availableHires = "+ availableHiresJSON + @";
            </script>
            ");

            sb.Append("<div id='hireItemsPanel'  class='sections'>");
            sb.Append("<div class='sectionHeader'><h4>Additional products and services:&nbsp;</h4>");
            sb.Append("<a href='http://" + extraHireContentURL + "' class='Quest PopUp'>?</a><span class='openclose'><i class='icon-plus'></i></span></div>");
            sb.Append("<div class='sectionContent'><table>");
            foreach (ExtraHireItem item in hireProductOptions)
            {
                string productInfo = (item.ProductInfo != null ? item.ProductInfo : string.Empty);
                
                if (item.PromptQuantity)
                {
                    sb.Append("<tr>");
                    sb.Append("<td class='cb'>");
                    sb.Append(renderItemDropDown(item.SapId, maxAmount, selectedItemsDic != null && selectedItemsDic.ContainsKey(item.SapId) ?  selectedItemsDic[item.SapId].Quantity : 0 ));
                    sb.Append("</td>");
                    sb.Append("<td class='desc'>");
                    sb.Append("<label>" + item.Name + (productInfo != string.Empty ? " (" + productInfo + ")" : string.Empty) + "</label>");
                    sb.Append("</td>");
                    if (item.UOM == UOMType.None)
                    {
                        sb.Append("<td class='days'>" + "</td>");
                        sb.Append("<td>$ " + item.DailyRate + " each</td>");
                    }
                    else if (item.UOM == UOMType.Day)
                    {
                        sb.Append("<td class='days'>" + hirePeriod + " day(s)</td>");
                        sb.Append("<td>$ " + item.DailyRate + " per day</td>");
                    }
                    sb.Append("<td class='total'></td>");//express checkout population here
                    sb.Append("</tr>");
                }
                else
                {
                    sb.Append("<tr>");
                    sb.Append("<td class='cb'>");
                    sb.Append("<input type='checkbox' id='ehi_" + item.SapId + "' name='ehi_" + item.SapId + "' " + (selectedItemsDic != null && selectedItemsDic.ContainsKey(item.SapId) /*returns 0 from ws && selectedItemsDic[item.SapId].Quantity > 0 */ ? "checked='checked'" : string.Empty) + " />");
                    sb.Append("</td>");
                    sb.Append("<td class='desc'>");
                    sb.Append("<label>" + item.Name + (productInfo != string.Empty ? " (" + productInfo + ")" : string.Empty) + "</label>");
                    sb.Append("</td>");
                    sb.Append("<td class='days'>" + (item.UOM == UOMType.Day || item.UOM == UOMType.H24 ? hirePeriod + " day(s)" : "&nbsp;") + "</td>");
                    sb.Append("<td class='price'>$ " + item.DailyRate + (item.UOM == UOMType.Day ? " per day" : " each") + "</td>");
                    sb.Append("<td class='total'>&nbsp;<td>");
                    sb.Append("</tr>");
                }
            }
            sb.Append("</table></div>");
            sb.Append("<input type='hidden' id='ehi_total' name='ehi_total' />");
            sb.Append("</div>");
            return sb.ToString();
        }
    }
}