﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Collections.Specialized;

/// <summary>
/// Displayer functionality Quote Summary totals 
/// </summary>

namespace THL.Booking
{
    public class QuoteDisplayer
    {

        AuroraQuote _quote;
        
        public QuoteDisplayer(AuroraQuote quote)
        {
            _quote = quote;
        }

        public string RenderChargeTable()
        {
            decimal totalQuote = _quote.VehicleCharge;
            string currencyStr = _quote.CurrencyCode;
            int hirePeriod = _quote.Request.GetHirePeriod();
            StringBuilder sb = new StringBuilder();
            sb.Append("<table class='ChargeTable'>");
            sb.Append("<tbody>");
            
            //sb.Append("<tr><td class='title'>Vehicle - Rate Period " + _quote.Request.PickUpDate.ToString("dd-MMM-yyyy") + " to " + _quote.Request.DropOffDate.ToString("dd-MMM-yyyy") + "</td><td class='desc'>" + hirePeriod + " Day(s) x " + currencyStr + " " + (_quote.VehicleCharge / hirePeriod).ToString("N2")/* TODO: rate bands */  + "</td><td class='cur'>" + currencyStr + "</td><td class='price'> $" + _quote.VehicleCharge.ToString("F") + "</td></tr>");
            foreach(AvailabilityItemChargeRateBand rb in _quote.GetRateBands())
                sb.Append("<tr><td class='title'>Vehicle - Rate Period " + rb.FromDate.ToString("dd-MMM-yyyy") + " to " + rb.ToDate.ToString("dd-MMM-yyyy") + "</td><td class='desc'>" + rb.HirePeriod + " Day(s) x " + currencyStr + " " + rb.DiscountedRate.ToString("F") /*(_quote.VehicleCharge / hirePeriod).ToString("N2") replaced by rate bands */  + "</td><td class='cur'>" + currencyStr + "</td><td class='price'> $" + rb.GrossAmount.ToString("F") + "</td></tr>");
            
            //Extra Hires
            foreach (ExtraHireItem product in _quote.AddedExtraHireItems)
            {                
                sb.Append("<tr>");
                sb.Append("<td class='title'>"+ product.Quantity + " x " + product.Name + "</td>");
                sb.Append("<td class='desc'>" + product.Quantity + " @ " + currencyStr + " $ " + product.DailyRate.ToString("N2") + " each</td>");
                sb.Append("<td class='cur'>" + currencyStr + "</td>");
                sb.Append("<td class='price'>$" + (product.GrossAmount).ToString("N2") + "</td>");
                sb.Append("</tr>");
                totalQuote += product.GrossAmount;
            }

            //Insurance
            foreach (InsuranceProduct product in _quote.AddedInsurance)
            {
                sb.Append("<tr>");
                sb.Append("<td class='title'>" + product.Name + "</td>");
                sb.Append("<td class='desc'>1 " +  " @ " + currencyStr + " $ " + product.GrossAmount.ToString("N2") + " each</td>");
                sb.Append("<td class='cur'>" + currencyStr + "</td>");
                sb.Append("<td class='price'>" +  (product.GrossAmount > 0 ? ("$" + (product.GrossAmount).ToString("N2")) : "included") + "</td>");
                sb.Append("</tr>");
                totalQuote += product.GrossAmount;
            }

            //Ferry
            if (_quote.FerryProducts.Length == 2 && _quote.FerryProducts[0].Quantity > 0)
            {
                for (int i = 0; i < _quote.FerryProducts[0].Quantity; i++)
                {
                    sb.Append("<tr><td class='title'>1 x Ferry - Vehicle Charge</td><td class='desc'>1 @ NZD $ " + _quote.FerryProducts[0].GrossAmount.ToString("F") + " each</td><td class='cur'>" + currencyStr + "</td><td class='price'>$" + _quote.FerryProducts[0].GrossAmount.ToString("F") + "</td></tr>");
                    totalQuote += _quote.FerryProducts[0].GrossAmount;
                }
                
                foreach(PersonRateInfo person in _quote.FerryProducts[1].FerryData.PersonRateCollection)
                    if (person.NumPassengers > 0)
                    {
                        sb.Append("<tr><td class='title'>" + person.NumPassengers + " x " + person.PersonInfoType + "</td><td class='desc'>" + person.NumPassengers + " @ " + currencyStr + " $ " + person.Rate.ToString("F") + " each</td><td class='cur'>" + currencyStr + "</td><td class='price'>$" + (person.Rate * person.NumPassengers).ToString("F") + "</td></tr>");
                        totalQuote += (person.Rate * person.NumPassengers);
                    }
                }

            //Fees
            List<string> waivedFees = _quote.getWaivedProducts();           
            
            foreach (BookingFee fee in _quote.EnforcedFees)
            {
                
                if (waivedFees != null && waivedFees.Count > 0 && waivedFees.Contains(fee.ShortName)) continue;

                string totalStr = ((fee.IsPercentageFee ? (totalQuote * fee.GrossCalcPercentage / 100) : fee.GrossAmount)).ToString("N2");
                totalStr = (totalStr == "0.00" ? "included" : "$" + totalStr);


                sb.Append("<tr>");
                sb.Append("<td class='title'>" + fee.Name + (fee.IsPercentageFee ? " ("+ fee.GrossCalcPercentage + "%) of $" + totalQuote.ToString("F")  : string.Empty) + "</td>");
                sb.Append("<td class='desc'>1  @ " + currencyStr + " $ " + fee.GrossAmount.ToString("N2") + " </td>");
                sb.Append("<td class='cur'>" + currencyStr + "</td>");
                sb.Append("<td class='price'>" + totalStr + "</td>");
                sb.Append("</tr>");
                totalQuote += (fee.IsPercentageFee ? (totalQuote * fee.GrossCalcPercentage / 100) : fee.GrossAmount);
            }

            sb.Append("<tr class='Totals'><td class='title'><small>Total</small></td><td class='desc'></td><td class='cur'><small>"+ currencyStr +"</small></td><td class='price'><small>$"+ totalQuote.ToString("F") +"</small></td></tr>");

            sb.Append("</tbody></table>");
            sb.Append("<script type=\"text/javascript\">var calculatedTotal = " + totalQuote.ToString("F") + ";</script>");
            return sb.ToString();        
        }

        /// <summary>
        /// Render the Payment Total for this quote
        /// </summary>
        /// <returns></returns>
        public string RenderPaymentTotalPanel(decimal surCharge)
        {                        
            string currencyStr = _quote.CurrencyCode.ToString();//TODO: refactor if needed
            decimal adminFee =  _quote.AdminFee;
            decimal deposit = _quote.GetRequiredDeposit();
            decimal pickUpBalance = _quote.CurrentTotal() - deposit;

            deposit = (surCharge > 0 ? deposit + surCharge : deposit);
            
            StringBuilder sb = new StringBuilder();
            sb.Append("<span id='totalPricesPanel' class='right'>");
            sb.Append("<input type='hidden' id='totalDepositValue' name='totalDepositValue' value='" + deposit.ToString("F") + "' />");
            sb.Append("<big class='Price TotalDeposit'>" + currencyStr + " $" +  deposit.ToString("F") + "</big>");
            sb.Append("<small class='Price PickUpBalance PayDeposit' id='pickUpTotal'>" + currencyStr + " $" + pickUpBalance.ToString("N2") + "</small>");
            sb.Append("</span>");
            if (adminFee > 0 /* validate fee waived if needed */)
                sb.Append("<em>Total includes " + adminFee.ToString("F") + "% admin fee</em>");
            return sb.ToString();
        } 
    }
}