﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace THL.Booking
{
    /// <summary>
    /// Company Funcionality Displayer
    /// </summary>
    public class CompanyDisplayer
    {
        Company company;
        Company[] companies;
        
        public CompanyDisplayer(Company _company)
        {
            company = _company;
        }

        public CompanyDisplayer(Company[] _companies)
        {
            companies = _companies;
        }

        public string RenderCompaniesDropDown(string selectedCompanyCode, VehicleType vehicleType)
        {             
            StringBuilder sb = new StringBuilder("<select class='ListBox' id='ctrlSearchBox_dropVendor' name='ctrlSearchBox_dropVendor'>");
            if (selectedCompanyCode == null)
            {
                sb.Append("<option value=''>Search all companies</option>");
                selectedCompanyCode = string.Empty;    
            }
            foreach(Company cmp in companies) 
            {
                if (cmp.CompanyType == VehicleType.Both || cmp.CompanyType == vehicleType)
                    sb.Append("<option "+ (selectedCompanyCode.Equals(cmp.CompanyCode) ? "selected='selected' " : string.Empty ) +"value='"+ cmp.CompanyCode +"'>"+ cmp.CompanyName +"</option>");
            }
            sb.Append("</select>");
            return sb.ToString();
        }

    }//CompanyDisplayer
}