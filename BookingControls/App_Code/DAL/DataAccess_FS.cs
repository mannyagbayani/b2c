﻿using System;
using System.Xml;

namespace THL.Booking
{

    /// <summary>
    /// DataAccess Layer Filesystem functionality 
    /// </summary>
    public partial class DataAccess
    {
        public XmlDocument GetXMLFromFile(string filename)
        { 
            XmlDocument xmlDoc = new XmlDocument();
            try 
            {
                xmlDoc.Load(filename);
                return xmlDoc;
            } 
            catch(Exception ex)
            {
                string errMsg = ex.Message;
                //TODO: optional log point..
                return null;
            }        
        }
    }
}