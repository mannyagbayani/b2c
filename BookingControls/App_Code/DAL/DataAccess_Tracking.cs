﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace THL.Booking
{
    /// <summary>
    /// Summary description for DataAccess_Tracking
    /// </summary>
    public partial class DataAccess
    {
         
        /// <summary>
        /// Return the available tracking elements from local XML files
        /// </summary>
        /// <param name="domains"></param>
        /// <returns></returns>
        public TrackingData[] GetTrackingDataElements(THLDomain[] domains)
        {
            string trackingBaseDataPath = string.Empty;
            string currentDomain = string.Empty;
            try
            {

                List<TrackingData> trackingElmList = new List<TrackingData>();
                trackingBaseDataPath = System.Web.Configuration.WebConfigurationManager.AppSettings["LocalDataPath"];
                foreach (THLDomain domain in domains)
                {
                    currentDomain = domain.Name;
                    char brandChar = domain.BrandChar;
                    string countryCode = domain.Country.ToString();

                    if (brandChar == 'z' || brandChar == 'c') countryCode = "NZ";//airnz is always NZ                

                    string filename = System.Web.Configuration.WebConfigurationManager.AppSettings["LocalDataPath"] + "\\" + brandChar + ".xml";
                    XmlDocument xmlDoc = GetXMLFromFile(filename);
                    XmlNodeList trackinElementNodes = xmlDoc.SelectNodes("//country[@code='" + countryCode + "']/contentResources/trackings/tracking");
                    foreach (XmlNode trackingNode in trackinElementNodes)
                    {//<tracking name="DecideDNA" event="payment" id="45523" code="BRITZ1" />
                        string tId = trackingNode.Attributes["id"].Value;
                        string tCode = trackingNode.Attributes["code"].Value;
                        string tEvent = trackingNode.Attributes["event"].Value;
                        string tName = trackingNode.Attributes["name"].Value;
                        TrackingProvider tProvider = TrackingProvider.Custom;
                        switch (tName)
                        {
                            case "DecideDNA":
                                tProvider = TrackingProvider.DecideDNA;
                                break;
                            case "GoogleAnalytics":
                                tProvider = TrackingProvider.GoogleAnalytics;
                                break;
                        }
                        TrackingData current = new TrackingData(tProvider, tId, tCode, tEvent, domain.Name);
                        trackingElmList.Add(current);
                    }
                }
                if (trackingElmList.Count > 0)
                    return trackingElmList.ToArray();
                else
                    return null;
            }
            catch (Exception ex) {
                string msg = ex.Message;
                THLDebug.LogError(ErrorTypes.Application, "DataAccess:GetTrackingDataElements",msg, "{trackinLoadFailedOn:'"+ currentDomain+ "'}" );
                return null;
            }
        }
    }
}