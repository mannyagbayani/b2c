﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Net;
using System.Text;
using System.Xml.Linq;
using System.Configuration;

namespace THL.Booking
{
    /// <summary>
    /// Get Availability response Collection from Aurora for a provided AvailabilityRequest
    /// </summary>
    public partial class DataAccess
    {

        /// <summary>
        /// Get AvailabilityResponse Instance for a supplied AvailabilityRequest object
        /// </summary>
        /// <param name="availabilityRequest"></param>
        /// <returns>AvailabilityResponse instance of Aurora</returns>
        public AvailabilityResponse GetAvailability(AvailabilityRequest availabilityRequest)
        {
            DateTime start, end;//Debug Code

            //Normalize the Brand
            availabilityRequest.Brand = AvailabilityHelper.NormaliseBrandString(availabilityRequest.Brand);

            //try {//TODO: implement exception handling
            AvailabilityResponse availabilityResponse = new AvailabilityResponse("m");//TODO: pass site (agentCode) here once MAC/AirNZ defined
            string auroraAvailabilityURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraAvailabilityURL"];

            string auroraAvailabilityParamsStr = availabilityRequest.GenerateAuroraRequestURL();

            start = DateTime.Now;
            string stringXml = GetStringForURL(auroraAvailabilityURL + "?" + auroraAvailabilityParamsStr);
            end = DateTime.Now;

            if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.TimeMeasurement, "{AvailabilityRequest:" + (end - start) + "}");//Debug Line
            try
            {

                XmlDocument responseXML = new XmlDocument();

                responseXML.LoadXml(stringXml);
                THLDebug.LogXML("/availability/availability_" + DateTime.Now.Ticks, responseXML);

                //--debug code
                HttpContext.Current.Session["debugStr"] = responseXML.InnerXml;//debug line
                HttpContext.Current.Session["requestURL"] = auroraAvailabilityURL + "?" + auroraAvailabilityParamsStr;//debug line
                //--debug code

                availabilityResponse.FillFromXML(responseXML, availabilityRequest.Brand);
                //debug line comes here: get serialised version of availabilityRequest + availabilityresponse and an xml dump of responseXML
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                THLDebug.LogError(ErrorTypes.ExternalProvider, "DataAccess.GetAvailability", ex.Message, "{'requestParams':'" + auroraAvailabilityParamsStr + "'}");
                availabilityResponse = null;
            }
            return availabilityResponse;
        }

        /// <summary>
        /// get an Aurora Package for the supplied identifier (AVPID)
        /// TODO: performance test and consider consuming as an XMLDocument
        /// </summary>
        /// <param name="avpId">AVPID</param>
        /// <returns>Aurora Package</returns>
        public AuroraPackage GetPackageForAvpId(string avpId, ApplicationData appData)
        {
            DateTime start, end;

            //try {//TODO: implement exception handling
            AuroraPackage ap = new AuroraPackage(avpId);
            string auroraAvailabilityURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraPackageURL"];

            start = DateTime.Now;
            string stringXml = GetStringForURL(auroraAvailabilityURL + "?avpId=" + avpId + "&IsGross=True&IsTestMode=False");//TODOL: load xml directly 
            end = DateTime.Now;

            XmlDocument responseXML = new XmlDocument();
            responseXML.LoadXml(stringXml);

            if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.TimeMeasurement, "{PackageForAVPRequest:" + (end - start) + "}");//Debug Line
            //--debug code
            THLDebug.LogXML("/package/package_" + avpId, responseXML);
            HttpContext.Current.Session["debugStr"] = responseXML.InnerXml;//debug line
            HttpContext.Current.Session["requestURL"] = auroraAvailabilityURL + "?avpId=" + avpId;//debug line
            //--debug code

            ap.LoadFromXML(responseXML, appData);
            return ap;
        }

        /// <summary>
        /// Submit a booking to Aurora using the WS Proxy Class
        /// TODO: performance test and consider consuming as an XMLDocument 
        /// </summary>
        /// <param name="bookingInfo"></param>
        /// <returns></returns>
        public string SubmitBookingToAurora(ref AuroraBooking bookingInfo)
        {

            DateTime start = DateTime.Now, end;//debug line 

            //try {//TODO: implement exception handling
            XmlDocument bookingXml = bookingInfo.ToXML();
            PhoenixWebService webService = new PhoenixWebService();

            HTTPProxy = System.Web.Configuration.WebConfigurationManager.AppSettings["Proxy"];
            string byPassList = System.Web.Configuration.WebConfigurationManager.AppSettings["ByPassProxy"];

            if (HTTPProxy != null && HTTPProxy != string.Empty)
            {
                WebProxy localProxy = new WebProxy(HTTPProxy, true, new string[] { byPassList });
                webService.Proxy = localProxy;
            }

            XmlNode bookingResponse = null;
            try
            {
                bookingResponse = webService.CreateBookingWithExtraHireItemAndPayment(bookingXml);
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.ExternalProvider, "DA.SubmitBookingToAurora", ex.Message, "{}");
                return null;
            }

            end = DateTime.Now;


            //--------------------added for location retrieval 20/10/09            
            XmlNode bookingPickUpElm = bookingResponse.SelectSingleNode("//BookingDetails/CkoAddress");
            XmlNode bookingDropOffElm = bookingResponse.SelectSingleNode("//BookingDetails/CkiAddress");
            if (bookingPickUpElm != null && bookingDropOffElm != null)
            {
                bookingInfo.PickUpStr = bookingPickUpElm.InnerText;
                bookingInfo.DropOffStr = bookingDropOffElm.InnerText;
            }
            //--------------------added for location retrieval

            if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.TimeMeasurement, "{CreateBookingToAurora:" + (end - start) + "}");//Debug Line

            //--debug code
            string timeStamp = DateTime.Now.ToString("ddMMyyyyHHmmss");
            THLDebug.LogBookingXML("/booking/response_" + timeStamp, bookingResponse);
            THLDebug.LogBookingXML("/booking/request_" + timeStamp, bookingInfo.ToXML());
            //HttpContext.Current.Session["debugStr"] = bookingResponse.OuterXml;//debug line
            //--debug code

            XmlNode bookingReponseIdElm = bookingResponse.SelectSingleNode("Booking/ReturnedBookingNumber");
            string bookingId = (bookingReponseIdElm != null ? bookingReponseIdElm.InnerText : null);
            if (bookingId != null)
                SessionManager.BookingStatus = BookingStatus.BookingSuccessful;
            return bookingId;
        }


        /// <summary>
        /// Submit a Confirmed Quote to Aurora
        /// </summary>
        /// <param name="quote">Quote Information</param>
        /// <param name="bookingInfo">Booking Reference</param>
        /// <returns></returns>
        public string SubmitConfirmedQuoteToAurora(AuroraQuote quote, ref AuroraBooking bookingInfo)
        {

            DateTime start = DateTime.Now, end;//debug line 

            //try {//TODO: implement exception handling
            XmlDocument bookingXml = null;// bookingInfo.ToXML();
            PhoenixWebServiceV2 webService = new PhoenixWebServiceV2();

            HTTPProxy = System.Web.Configuration.WebConfigurationManager.AppSettings["Proxy"];
            string byPassList = System.Web.Configuration.WebConfigurationManager.AppSettings["ByPassProxy"];

            if (HTTPProxy != null && HTTPProxy != string.Empty)
            {
                WebProxy localProxy = new WebProxy(HTTPProxy, true, new string[] { byPassList });
                webService.Proxy = localProxy;
            }

            XmlNode bookingResponse = null;
            try
            {
                //bookingResponse = webService.CreateBookingWithExtraHireItemAndPayment(bookingXml);
                bookingXml = quote.SerializeQuote();

                /*Added for masking creditcard*/
                var cNumberElm = bookingXml.GetXElement().GetXmlNode().SelectSingleNode("//Data/CreditCardDetails/CreditCardNumber");//<CreditCardNumber>1111111111111111</CreditCardNumber>

                if (bookingXml.SelectSingleNode("//CreditCardDetails/CreditCardNumber") != null)
                {
                    string ccNumber = bookingXml.SelectSingleNode("//CreditCardDetails/CreditCardNumber").InnerText;
                    string maskCCNumber = ccNumber.Substring(0, 6) +"xxxxxxxx" + ccNumber.Substring(ccNumber.Length - 2, 2);//mask:
                    bookingXml.OuterXml.Replace(ccNumber, maskCCNumber);
                    bookingXml.SelectSingleNode("//CreditCardDetails/CreditCardNumber").InnerText = maskCCNumber;
                }

                //string maskCCNumber = "xxxx-xxxx-xxxx-" + _paymentInfo.CardNumber.ToString().Substring(_paymentInfo.CardNumber.ToString().Length - 4, 4);//mask:
                //cNumberElm.InnerText = maskCCNumber;
                /*end added for masking creditcard*/
                
                if (bookingXml != null)
                    bookingResponse = webService.ModifyBooking(bookingXml);
                else
                {//cannot serialize the quote
                    THLDebug.LogError(ErrorTypes.Application, "DA.SubmitConfirmedQuoteToAurora", "failed to serialize quote", "{quoteId:'" + quote.BookingID + "'}");
                    return null;
                }

            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.ExternalProvider, "DA.SubmitConfirmedQuoteToAurora", ex.Message, "{}");
                return null;
            }

            end = DateTime.Now;


            //--------------------added for location retrieval 20/10/09            
            XmlNode bookingPickUpElm = bookingResponse.SelectSingleNode("//BookingDetails/CkoAddress");
            XmlNode bookingDropOffElm = bookingResponse.SelectSingleNode("//BookingDetails/CkiAddress");
            if (bookingPickUpElm != null && bookingDropOffElm != null)
            {
                bookingInfo.PickUpStr = bookingPickUpElm.InnerText;
                bookingInfo.DropOffStr = bookingDropOffElm.InnerText;
            }
            //--------------------added for location retrieval

            if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.TimeMeasurement, "{CreateBookingToAurora:" + (end - start) + "}");//Debug Line

            //--debug code
            string timeStamp = DateTime.Now.ToString("ddMMyyyyHHmmss");
            THLDebug.LogBookingXML("/booking/response_" + timeStamp, bookingResponse);
            THLDebug.LogBookingXML("/booking/request_" + timeStamp, bookingXml.FirstChild);
            //HttpContext.Current.Session["debugStr"] = bookingResponse.OuterXml;//debug line
            //--debug code

            XmlNode bookingReponseIdElm = bookingResponse.SelectSingleNode("Booking/ReturnedBookingNumber");
            string bookingId = (bookingReponseIdElm != null ? bookingReponseIdElm.InnerText : null);
            if (bookingId != null)
                SessionManager.BookingStatus = BookingStatus.BookingSuccessful;
            return bookingId;
        }



        /// <summary>
        /// Force a partial bookin into Aurora with No Payment information, used by Email Form functionality
        /// </summary>
        /// <param name="bookingRequestXML">Booking XML Structure</param>
        /// <returns>Generated Booking ID</returns>
        public string ForceBookingToAurora(XmlDocument bookingRequestXML)
        {
            string generatedBookingID = string.Empty;

            DateTime start = DateTime.Now, end;//debug line 

            //try {//TODO: implement exception handling

            PhoenixWebService webService = new PhoenixWebService();

            HTTPProxy = System.Web.Configuration.WebConfigurationManager.AppSettings["Proxy"];
            string byPassList = System.Web.Configuration.WebConfigurationManager.AppSettings["ByPassProxy"];

            if (HTTPProxy != null && HTTPProxy != string.Empty)
            {
                WebProxy localProxy = new WebProxy(HTTPProxy, true, new string[] { byPassList });
                webService.Proxy = localProxy;
            }
            XmlNode bookingResponse = webService.ForceCreateBooking(bookingRequestXML);
            end = DateTime.Now;
            if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.TimeMeasurement, "{ForceCreateBookingToAurora:" + (end - start) + "}");//Debug Line

            //--debug code
            string timeStamp = DateTime.Now.ToString("ddMMyyyyHHmmss");
            THLDebug.LogBookingXML("/booking/wl_response_" + timeStamp, bookingResponse);
            THLDebug.LogBookingXML("/booking/wl_request_" + timeStamp, bookingRequestXML.FirstChild);

            HttpContext.Current.Session["debugStr"] = bookingResponse.OuterXml;//debug line
            //--debug code

            XmlNode bookingReponseIdElm = bookingResponse.SelectSingleNode("Booking/ReturnedBookingNumber");
            string bookingId = (bookingReponseIdElm != null ? bookingReponseIdElm.InnerText : null);
            return bookingId;
        }

        /// <summary>
        /// Get a Quoted Booking for a provided Aurora Rental number
        /// </summary>
        /// <param name="rentalId">Aurora Generated Rental Number</param>
        /// <returns>AuroraBooking Instance representing the Quote (no payment info)</returns>
        public AuroraQuote GetQuoteForRentalId(string rentalNumber)
        {
            string auroraBaseURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraServiceBaseURL"];
            //Use Rest compatible instead of SOAP
            string quoteData = GetStringForURL(auroraBaseURL + "/RetrieveBooking?RentalNumber=" + rentalNumber);//TODO: consider loading an XML structure once service sealed
            string quotePackageData = GetStringForURL(auroraBaseURL + "/GetInsuranceAndExtraHireItemsForRentalQuote?BookingReference=" + rentalNumber + "&ReferenceId=" + rentalNumber);
            //--

            if (quoteData != null && quoteData != string.Empty && quotePackageData != null && quotePackageData != string.Empty && quoteData.Contains("RentalInfo"))
            {
                XmlDocument quoteXml = new XmlDocument();
                quoteXml.LoadXml(quoteData);
                XmlDocument quotePackageXml = new XmlDocument();
                quotePackageXml.LoadXml(quotePackageData);

                THLDebug.LogQuoteInfo(rentalNumber.Replace('/', '-'), quoteXml, quotePackageXml);//Log for debug
                AuroraQuote quote = new AuroraQuote(quoteXml, quotePackageXml);//init quote with package and selected products info           
                //TODO: log
                return quote;
            }
            else
            {
                THLDebug.LogEvent(EventTypes.DBRequest, "{DataAccess.GetQuoteForRentalId:'no data for " + rentalNumber + "}");
                return null;
            }
        }

        /// <summary>
        /// Query Aurora WS for Alternate Availability of a given Availability Request
        /// TODO: move sorting and required types (DisplayMode) into config if the y will change frequently 
        /// </summary>
        /// <returns>AlternativeAvailabilityItem collection representation of the optional results</returns>
        public AlternativeAvailabilityItem[] GetAlternateAvailability(AvailabilityRequest availabilityRequest)
        {
            //add cross sell multiple brands support
            string vehicleBrand = AvailabilityHelper.NormaliseBrandString(availabilityRequest.Brand);


            List<AlternativeAvailabilityItem> altItemsList = new List<AlternativeAvailabilityItem>();
            DateTime start, end;//Debug Code
            //try {//TODO: implement exception handling
            AvailabilityResponse availabilityResponse = new AvailabilityResponse("m");//TODO: pass site (agentCode) here once MAC/AirNZ defined
            string auroraAvailabilityURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraServiceBaseURL"] + "/GetAlternateAvailability";
            string agentCode = availabilityRequest.AgentCode;
            string auroraAvailabilityParamsStr = string.Empty;
            StringBuilder sb = new StringBuilder();//Construct Request Params
            sb.Append("Brand=" + /* swapped for CSell: availabilityRequest.Brand */ vehicleBrand + "&");
            sb.Append("VehicleCode=" + availabilityRequest.VehicleModel + "&");
            sb.Append("CountryCode=" + availabilityRequest.CountryCode + "&");
            sb.Append("CheckOutZoneCode=" + availabilityRequest.PickUpBranch + "&");
            sb.Append("CheckOutDateTime=" + availabilityRequest.PickUpDate.ToString("dd-MMM-yyyy HH:mm") + "&");
            sb.Append("CheckInZoneCode=" + availabilityRequest.DropOffBranch + "&");
            sb.Append("CheckInDateTime=" + availabilityRequest.DropOffDate.ToString("dd-MMM-yyyy HH:mm") + "&");
            sb.Append("AgentCode=" + agentCode + "&");
            sb.Append("PackageCode=" + availabilityRequest.PackageCode + "&");
            sb.Append("IsVan=" + (availabilityRequest.VehicleType == VehicleType.Car ? "False" : "True") + "&");
            sb.Append("NumberOfAdults=" + availabilityRequest.NumberOfAdults + "&");
            sb.Append("NoOfChildren=" + availabilityRequest.NumberOfChildren + "&");
            sb.Append("isBestBuy=True&");
            sb.Append("countryOfResidence=" + availabilityRequest.CountryOfResidence + "&");
            //if pick & drop are same no 3,6 (switch loc)
            string orderStr = (System.Web.Configuration.WebConfigurationManager.AppSettings["AltAvailOrder"] != null ? System.Web.Configuration.WebConfigurationManager.AppSettings["AltAvailOrder"] : "1,2,4,5");

            sb.Append("DisplayMode=" + (!availabilityRequest.PickUpBranch.Equals(availabilityRequest.DropOffBranch) ? "3,6," : string.Empty) + orderStr + "&");//TODO: move to config
            sb.Append("Istestmode=False&IsGross=True");//TODO: read from config, added IsGross true support for B2B
            auroraAvailabilityParamsStr = sb.ToString();

            string auroraBaseURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraServiceBaseURL"];
            string requestURL = auroraAvailabilityURL + "?" + auroraAvailabilityParamsStr;  //"http://pap-app-002/PhoenixWS%20Alternate/service.asmx/GetAlternateAvailability";//?VehicleCode=bp6b&CountryCode=NZ&Brand=P&CheckOutZoneCode=chc&CheckOutDateTime=22-Aug-2011 11:00am&CheckInZoneCode=chc&CheckInDateTime=19-Sep-2011 11:00am&AgentCode=B2CNZ&PackageCode=&IsVan=True&NumberOfAdults=1&NoOfChildren=0&isBestBuy=True&countryOfResidence=nz&DisplayMode=1,2,3,4,5&Istestmode=False";

            try
            {
                start = DateTime.Now;
                string stringXml = GetStringForURL(requestURL);
                end = DateTime.Now;
                XmlDocument responseXML = new XmlDocument();

                responseXML.LoadXml(stringXml);
                XmlNodeList altAvailNodes = responseXML.SelectNodes("data/AvailableRate");
                if (altAvailNodes != null && altAvailNodes.Count > 0)
                {
                    int index = 0;
                    foreach (XmlNode altNode in altAvailNodes)
                    {
                        AlternativeAvailabilityItem aaItem = new AlternativeAvailabilityItem(altNode, availabilityRequest.VehicleModel);
                        aaItem.SortOrder = index++;
                        altItemsList.Add(aaItem);
                    }
                }
                else
                {
                    THLDebug.LogError(ErrorTypes.ExternalProvider, "DataAccess.GetAlternateAvailability", "Alternate Availability Results is empty", "{'requestURL':'" + requestURL + "'}");
                    return null;
                }
                THLDebug.LogXML("/availability/availability_" + DateTime.Now.Ticks, responseXML);
                List<AlternativeAvailabilityItem> noAvailItems = new List<AlternativeAvailabilityItem>();
                foreach (AlternativeAvailabilityItem ai in altItemsList)
                    if (ai.AvailabilityType != AvailabilityItemType.Available)
                        noAvailItems.Add(ai);
                foreach (AlternativeAvailabilityItem ai in noAvailItems)
                    altItemsList.Remove(ai);
                AlternativeAvailabilityItem[] aaItems = altItemsList.Concat(noAvailItems).ToArray();
                if (THLDebug.EventLogOn) THLDebug.LogEvent(EventTypes.TimeMeasurement, "{Alternate Availability Request:" + (end - start) + "}");//Debug Line
                return aaItems;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                THLDebug.LogError(ErrorTypes.ExternalProvider, "DataAccess.GetAlternateAvailability", msg, "{'requestURL':'" + requestURL + "'}");
                return null;
            }
        }

        string PipelineOrigionalRequest(string oRequest)
        {
            return oRequest.Replace('&', '~').Replace('=', '|');
        }
        public string GetAvailableSlots(string avpID, string bookingId)
        {
            StringBuilder sb = new StringBuilder();
            bool isDevEnv = bool.Parse(ConfigurationManager.AppSettings["isDevEnv"] == null ? "false" : ConfigurationManager.AppSettings["isDevEnv"]);
            string auroraBaseURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AuroraServiceBaseURL"];
            //Use Rest compatible instead of SOAP
            string slotData = string.Empty;
            if (string.IsNullOrEmpty(bookingId))

                slotData = GetStringForURL(auroraBaseURL + "/GetAvailableSlot?sRntId=&sAvpId=" + avpID + "&sBpdId=");//TODO: consider loading an XML structure once service sealed
            else
                slotData = GetStringForURL(auroraBaseURL + "/GetAvailableSlot?sRntId=" + bookingId + "&sAvpId=&sBpdId=");//TODO: consider loading an XML structure once service sealed

            //string quotePackageData = GetStringForURL(auroraBaseURL + "/GetInsuranceAndExtraHireItemsForRentalQuote?BookingReference=" + rentalNumber + "&ReferenceId=" + rentalNumber);
            //--

            string retString = "Success";
            //slotData = string.Empty;
            if ((slotData != null && slotData != string.Empty) && slotData.IndexOf("AvailableSlots") >= 0)
            {

                XmlDocument slotXml = new XmlDocument();
                slotXml.LoadXml(slotData);
                //slotXml.GetXElement();
                var result = (from c in slotXml.GetXElement().Descendants("Slot")
                              select c
                         ).ToList();
                int rowCount = result.Count();
                //var widthPercentage = Math.Round(Decimal.Parse(100.ToString()) / rowCount, 0);
                int widthPercentage = 100 / rowCount;
                string fontSize = "18px";
                if (rowCount > 6)
                    fontSize = "16px";
                foreach (XElement slot in result)
                {
                    string slotDesc = slot.Element("SlotDesc").Value;
                    string slotId = slot.Element("SlotId").Value;
                    string allocatedNumbers = slot.Element("AllocatedNumbers") == null? "0" : slot.Element("AllocatedNumbers").Value;
                    string bookedNumbers = slot.Element("Bookednumbers") == null ? "0" : slot.Element("Bookednumbers").Value;
                    string availableNumbers = slot.Element("AvailableNumbers") == null ? "0" : slot.Element("AvailableNumbers").Value;

                    string isSlotAvailable = slot.Element("IsSlotAvailable") == null ? "0" : slot.Element("IsSlotAvailable").Value;
                    
                    string isAllDaySlot = slot.Element("IsAllDaySlot") == null ? "0" : slot.Element("IsAllDaySlot").Value;
                    string isSelected = slot.Element("IsSelected").Value;

                    string[] slotTimes = slotDesc.Split('-');
                    string[] timeSplit1 = slotTimes[0].Split(' ');
                    string[] timeSplit2 = slotTimes[1].Trim().Split(' ');

                    string availUnavail = isSlotAvailable.Equals("1") ? "available" : "unavailable";
                    bool selected = isSelected.Equals("1") ? true : false;
                    string availUnavailText = availUnavail.ToUpper();
                    string ava = " (" + (int.Parse(allocatedNumbers) - int.Parse(bookedNumbers)).ToString() + ")";
                    if (int.Parse(availableNumbers) <= 0)
                    {
                        isSlotAvailable = "0";
                        isSelected = "0";
                        availUnavail = "unavailable";
                        selected = false;
                        availUnavailText = "UNAVAILABLE";
                    }
                    if (selected)
                    {
                        availUnavail += " selected";
                        availUnavailText = "SELECTED";
                    }
                    //else 
                    if (!availUnavail.Equals("unavailable") && isDevEnv)
                        availUnavailText += ava;

                    sb.Append("<div class='timeslots " + availUnavail + "' id='slot" + slotId + "' style='width:" + widthPercentage.ToString() + "%'>");
                    sb.Append("<div class='slots'>");
                    sb.Append("<div class='time left' style='font-size:" + fontSize + "'>" + timeSplit1[0] + "<span class='ampm'>" + timeSplit1[1] + "</span></div>");
                    sb.Append("<div class='arrow'><span class='arrowhover'>&nbsp;</span></div>");
                    sb.Append("<div class='time right' style='font-size:" + fontSize + "'>" + timeSplit2[0] + "<span class='ampm'>" + timeSplit2[1] + "</span></div>");
                    sb.Append("<div class='availability'>" + availUnavailText + "</div>");
                    sb.Append("</div>");
                    sb.Append("</div>");
                }

            }
            else
            {
                ////THLDebug.LogEvent(EventTypes.DBRequest, "{DataAccess.GetQuoteForRentalId:'no data for " + rentalNumber + "}");
                //SessionData sessionData = SessionManager.GetSessionData();
                //string vehicleCode = sessionData.BookingData.VehicleCode;
                //string brand = sessionData.BookingData.Brand.ToString();

                ////PassedParams = (Request.RawUrl.Contains("?") ? Request.RawUrl.Split('?')[1] : string.Empty);
                //var sid = HttpContext.Current.Session.SessionID;
                ////string passedParams = "altavailability.aspx?"+ "&param=" + PipelineOrigionalRequest(("altavailability.aspx?sid=" + sid + "&vCode=" + vehicleCode + "&rType=GetAlternateOptions&Brand=" + brand).Split('?')[1]);
                //sb.Append("<div class='timeslots unavailable' id='slot0' style='width:100%'>");
                //sb.Append("<div class='slots' style='width:100%'>");
                ////sb.Append("<div class='time' style='font-size:14; aligh:center; width:100%; padding:0'><a href='"+passedParams+"'>alternate availability</a></div>");
                ////+"&rType=GetAlternateOptions&Brand=" + brand
                ////sb.Append("<div class='time' style='font-size:14; aligh:center; width:100%; padding:0'><a href='preloader.aspx?vCode=" + vehicleCode + "&rType=GetAlternateOptions&Brand=" + brand + "'>alternate availability</a></div>");
                //sb.Append("<div class='time' style='font-size:14; aligh:center; width:100%; padding:0'><a href='altavailability.aspx?sid=" + sid + "&vCode=" + vehicleCode + "&rType=GetAlternateOptions&Brand=" + brand + "'>Alternate Availability</a></div>");

                //sb.Append("<div class='availability'>unavailable</div>");
                //sb.Append("</div>");
                //sb.Append("</div>");
                return null;
            }
            return sb.ToString();
        }

    }
}
