﻿using System;
using System.Configuration;
using System.Web;
using System.IO;
using System.Net;
using System.Text;

namespace THL.Booking
{
    /// <summary>
    /// HTTP Data Access Layer Functionality 
    /// </summary>
    public partial class DataAccess
    {
        /// <summary>
        /// Return a string context of an HTTP URL Request
        /// </summary>
        /// <param name="url">the URI for the connection</param>
        /// <returns></returns>
        public string GetStringForURL(string url)
        {
            string result = null;
            try
            {
                //if (url.IndexOf("http://test-app-001") != -1) 
                //    System.Diagnostics.Debug.WriteLine(url);
                
                HttpWebRequest request = getHTTPRequest(url);
                HttpWebResponse myHttpWebResponse1 = (HttpWebResponse)request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader loResponseStream = new StreamReader(myHttpWebResponse1.GetResponseStream(), enc);
                result = loResponseStream.ReadToEnd();
                loResponseStream.Close();
                myHttpWebResponse1.Close();
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Database, "DataAccess.GetStringForURL", ex.Message, "{url:'" + url + "'}");
                result = null;      
            }
            return result;
        }

        public string GetStringFromFile(string filePath)
        {
            string result = "";
            try
            {
                StreamReader stream = new StreamReader(filePath);
                result = stream.ReadToEnd();
                stream.Close();
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Database, "DataAccess.GetStringForURL", ex.Message, ""); 
            }

            return result;
        }
        /// <summary>
        /// Support HTTP POST param passing for an HTTP Request 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postParams"></param>
        /// <returns></returns>
        public string GetStringForPostRequest(string url, string postParams)
        {
            string result = null;
            try 
            {
                HttpWebRequest request = getHTTPRequest(url);
                request.ContentType = "application/x-www-form-urlencoded";//--
                request.Method = "POST"; 
                byte[] byteData = UTF8Encoding.UTF8.GetBytes(postParams);// Create a byte array of the data we want to send   
                request.ContentLength = byteData.Length;
                using (Stream postStream = request.GetRequestStream())
                {
                    postStream.Write(byteData, 0, byteData.Length);
                }
                HttpWebResponse myHttpWebResponse1 = (HttpWebResponse)request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader loResponseStream = new StreamReader(myHttpWebResponse1.GetResponseStream(), enc);
                result = loResponseStream.ReadToEnd();
                loResponseStream.Close();
                myHttpWebResponse1.Close();
                THLDebug.LogEvent(EventTypes.HTTPRequest, "{ url:'" + url + "'}");
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Database, "DataAccess.GetStringForPostRequest", ex.Message, "{url:'" + url + "'}");//cleanup if needed            
                result = null;
            }
            return result;
        }

        public string HTTPProxy = string.Empty;

        /// <summary>
        /// Generate a simple HTTP Request Instance For a given URL
        /// </summary>
        /// <param name="url">required resource URL</param>
        /// <returns></returns>
        HttpWebRequest getHTTPRequest(string url)
        {
            HTTPProxy = System.Web.Configuration.WebConfigurationManager.AppSettings["Proxy"];
            string byPassList = System.Web.Configuration.WebConfigurationManager.AppSettings["ByPassProxy"];
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            if (HTTPProxy != null && HTTPProxy != string.Empty)
            {
                WebProxy localProxy = new WebProxy(HTTPProxy, true, new string[] { byPassList });               
                request.Proxy = localProxy;
            }
            THLDebug.LogEvent(EventTypes.HTTPRequest, "{ url:'" + url + "'}");
            return request;
        }
    }
}