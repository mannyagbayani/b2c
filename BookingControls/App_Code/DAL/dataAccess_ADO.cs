﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.Common;
using System.Text;
namespace THL.Booking {


/// <summary>
/// Summary description for dataAccess_ADO
/// </summary>
public partial class DataAccess
{
	protected string insertedRowIdentity; //Current Inserted Key

        /// <summary>
        /// Connects to a SQL BackUp database, TODO: implement 
        /// </summary>
        /// <param name="bookingInfo">Current Booking Information Instance</param>
        /// <returns>Identity Generated From BackUp DB</returns>
        public string SubmitToDRDB(AuroraBooking bookingInfo)
        {
            //TODO: ADO.NET Call with INSERT on a DB at the same Session DB cluster.
            //BEGIN TRAN
            //DECLARE @sRetBooNum VARCHAR(64)
            //Exec [dbo].[WEBP_CreateFailedInfo]
            //@sRequestXML      VARCHAR(MAX),
            //@sErrorMsg        VARCHAR(MAX),     
            //@sAddPrgmName     VARCHAR(128),
            //@sTransStatus     VARCHAR(128),     -- Added RKS:16SEP09 to handle DPS timeout issues //TODO: add support for failed force booking (diff types) DPSFAIL for failed trans
            //@sRetBooNum       VARCHAR(64) OUTPUT
            //  SELECT @sRetBooNum
            //COMMIT TRAN                        
            //-----------Insert/Update Starts
            try
            {
                string failSafeConStr = ConfigurationManager.ConnectionStrings["failsafedbB"].ConnectionString;                
                insertedRowIdentity = "0";
                SqlDataSource mySource = new SqlDataSource();
                mySource.ConnectionString = failSafeConStr;
                mySource.InsertCommand = "WEBP_CreateFailedInfo";
                mySource.InsertCommandType = SqlDataSourceCommandType.StoredProcedure;
                
                mySource.InsertParameters.Add("sRequestXML", bookingInfo.ToXML().OuterXml);
                mySource.InsertParameters.Add("sErrorMsg","connection");
                mySource.InsertParameters.Add("sAddPrgmName", "WebFrontEnd");
                mySource.InsertParameters.Add("sTransStatus", null );                
                
                mySource.Inserted += new SqlDataSourceStatusEventHandler(mySource_Inserted);
                mySource.Inserting += new SqlDataSourceCommandEventHandler(On_Inserting);

                THLDebug.LogEvent(EventTypes.DBRequest, "Failsafe call on: sRequestXML:" + bookingInfo.ToXML().OuterXml);
                
                try
                {
                    mySource.Insert();
                }
                catch (SqlException ex)
                {
                    string errorMsg = ex.Message;
                    THLDebug.LogError(ErrorTypes.Database, "DAL:SubmitFalsafe", "{err:" + errorMsg + "}", "{err:" + errorMsg + "}");
                    return "0";
                }                
                return insertedRowIdentity;//effected rows should be 1                
            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;
                //log here
                return "0";
            }            
        }

        private void On_Inserting(Object sender, SqlDataSourceCommandEventArgs e)
        {
            SqlParameter insertedKey = new SqlParameter("@sRetBooNum", SqlDbType.VarChar, 64);
            insertedKey.Direction = ParameterDirection.Output;
            e.Command.Parameters.Add(insertedKey);
        }

        void mySource_Inserted(object sender, SqlDataSourceStatusEventArgs e)
        {
            try
            {
                DbCommand command = e.Command;
                insertedRowIdentity = command.Parameters["@sRetBooNum"].Value.ToString();
            }
            catch (SqlException ex)
            {
                string errorMsg = ex.Message;
                THLDebug.LogError(ErrorTypes.Database, "DataAccess.Source_Inserted", ex.Message, string.Empty);
                insertedRowIdentity = "0";
            }
        }

        public string GetSlots(string avpID)
        {
            DataSet ds = new DataSet("Slots");
            StringBuilder sb = new StringBuilder();
            bool isDevEnv = bool.Parse(ConfigurationManager.AppSettings["isDevEnv"] == null ? "false" : ConfigurationManager.AppSettings["isDevEnv"]);
            string connStr = ConfigurationManager.ConnectionStrings["slotSQLConn"].ToString();
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                SqlCommand sqlComm = new SqlCommand("getAvailableSlotForRntIdOrAvp", conn);
                sqlComm.Parameters.AddWithValue("@sAvpId", avpID);
                
                sqlComm.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlComm;

                da.Fill(ds);
            }
            // fill the data now and return
            int rowCount = ds.Tables[0].Rows.Count;
            //var widthPercentage = Math.Round(Decimal.Parse(100.ToString()) / rowCount, 0);
            int widthPercentage = 100 / rowCount;
            string fontSize = "18px";
            if(rowCount > 6)
                fontSize = "16px";
            if ( rowCount > 0)
            {
                int ctr = 0;
                foreach (DataRow currentRow in ds.Tables[0].Rows)
                {
                    ctr++;
                    string[] slotTimes = currentRow["SlotDesc"].ToString().Split('-');
                    string[] timeSplit1 = slotTimes[0].Split(' ');
                    string[] timeSplit2 = slotTimes[1].Trim().Split(' ');
                    string availUnavail = currentRow["IsSlotAvailable"].ToString().Equals("1") ? "available" : "unavailable";
                    bool selected = currentRow["IsSelected"].ToString().Equals("1") ? true : false;
                    string availUnavailText = availUnavail.ToUpper();
                    string  ava = " ("+ (int.Parse(currentRow["AllocatedNumbers"].ToString()) - int.Parse(currentRow["BookedNumbers"].ToString())).ToString() + ")";
                    if (selected)
                    {
                        availUnavail += " selected";
                        availUnavailText = "SELECTED";
                    }
                    //else 
                    if(!availUnavail.Equals("unavailable") && isDevEnv)
                    availUnavailText += ava;

                    sb.Append("<div class='timeslots " + availUnavail + "' id='slot" + ctr.ToString() + "' style='width:" + widthPercentage.ToString() + "%'>");
                    sb.Append("<div class='slots'>");
                    sb.Append("<div class='time left' style='font-size:"+fontSize+"'>" + timeSplit1[0] + "<span class='ampm'>" + timeSplit1[1] + "</span></div>");
                    sb.Append("<div class='arrow'><span class='arrowhover'>&nbsp;</span></div>");
                    sb.Append("<div class='time right' style='font-size:" + fontSize + "'>" + timeSplit2[0] + "<span class='ampm'>" + timeSplit2[1] + "</span></div>");
                    sb.Append("<div class='availability'>" + availUnavailText + "</div>");
                    sb.Append("</div>");
                    sb.Append("</div>");
                }



                //sb.Append("</div>");
                //sb.Append("</div>");
            }
            return sb.ToString();
        }
        public string GetSlotsOld(string avpID)
        {
            StringBuilder sb = new StringBuilder();
            string cnStr = "Data Source=test-sql-011;Initial Catalog=Aurora;Persist Security Info=True;User ID=AuroraWeb;Password=@dmin$123";

            var con = new SqlConnection();
            con.ConnectionString = cnStr;
            var com = new SqlCommand();
            com.Connection = con;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = "getAvailableSlotForRntIdOrAvp";
            com.Parameters.AddWithValue("@sAvpId", "BCF77BAC743B4D77B8C7475FDAE6A751");
            var adapt = new SqlDataAdapter();
            adapt.SelectCommand = com;
            var dataset = new DataSet();
            adapt.Fill(dataset);

            if(dataset.Tables[0].Rows.Count >0)
            {
                //sb.Append("<div id='pickuptimes'>");
                //sb.Append("<div id='currentday'>");
                //sb.Append("<span class='date'><%= PickUpDayMonthStr %></span>");
                //sb.Append("<span class='location'><%= PickUpLocationStr %></span>");
                //sb.Append("</div>");
                //sb.Append("<div id='times'>");
                int ctr = 0;
                foreach (DataRow currentRow in dataset.Tables[0].Rows)
                {
                    ctr++;
                    string[] slotTimes = currentRow["SlotDesc"].ToString().Split('-');
                    string[] timeSplit1 = slotTimes[0].Split(' ');
                    string[] timeSplit2 = slotTimes[1].Trim().Split(' ');
                    string availUnavail = currentRow["IsSlotAvailable"].ToString().Equals("1") ? "available" : "unavailable";

	            	sb.Append("<div class='timeslots " + availUnavail + "' id='slot" + ctr.ToString() + "'>");
                    sb.Append("<div class='slots'>");
                    sb.Append("<div class='time left'>" + timeSplit1[0] + "<span class='ampm'>" + timeSplit1[1] + "</span></div>");
                    sb.Append("<div class='arrow'><span class='arrowhover'>&nbsp;</span></div>");
                    sb.Append("<div class='time right'>" + timeSplit2[0] + "<span class='ampm'>" + timeSplit2[1] + "</span></div>");
                    sb.Append("<div class='availability'>" + availUnavail.ToUpper() + "</div>");
                    sb.Append("</div>");
                    sb.Append("</div>");
                }
	
                

                //sb.Append("</div>");
                //sb.Append("</div>");
            }
            

            return sb.ToString();
            ////string connString = "Data Source=test-sql-011\\staging;Initial Catalog=Aurora;Persist Security Info=True;User ID=AuroraWeb;Password=@dmin$123";
            ////string sql = "getAvailableSlotForRntIdOrAvp";

            ////SqlConnection conn = new SqlConnection(connString);

            ////try
            ////{
            ////    SqlDataAdapter da = new SqlDataAdapter();
            ////    da.SelectCommand = new SqlCommand(sql, conn);
            ////    da.SelectCommand.CommandType = CommandType.StoredProcedure;
            ////    da.SelectCommand.Parameters.AddWithValue("@sAvpId", avpID);

            ////    DataSet ds = new DataSet();
            ////    da.Fill(ds, "result_name");

            ////    DataTable dt = ds.Tables["result_name"];

            ////    foreach (DataRow row in dt.Rows)
            ////    {
            ////        //manipulate your data
            ////    }

            ////}
            ////catch (Exception e)
            ////{
            ////    Console.WriteLine("Error: " + e);
            ////}
            ////finally
            ////{
            ////    conn.Close();
            ////}

            //using (SqlConnection cn = new SqlConnection(cnStr))
            //{
            //    cn.Open();
            //    using (SqlCommand cmd = new SqlCommand("getAvailableSlotForRntIdOrAvp", cn))
            //    {
            //        cmd.CommandType = CommandType.StoredProcedure;
            //        cmd.Parameters.AddWithValue("@sAvpId", avpID);
                    
            //        //SqlParameter returnValue = new SqlParameter();
            //        //returnValue.Direction = ParameterDirection.ReturnValue;
            //        //cmd.Parameters.Add(returnValue);

            //        var retValue = cmd.ExecuteNonQuery();
            //        string Nimesh="testing";
            //    }
            //}
        }
    }
}
