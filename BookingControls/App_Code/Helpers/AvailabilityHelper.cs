﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;
using System.Text;


namespace THL.Booking
{

    /// <summary>
    /// Summary description for AvailabilityHelper
    /// </summary>
    public class AvailabilityHelper
    {

        public AvailabilityHelper()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static NameValueCollection GetCollectionForQueryString(string queryString)
        {
            NameValueCollection nvc = new NameValueCollection();
            string[] queryStringParams = queryString.Split('&');
            foreach (string paramStr in queryStringParams)
            {
                string[] pair = paramStr.Split('=');
                nvc.Add(pair[0], pair[1]);
            }
            return nvc;
        }

        public static string NormalizeParamsString(string rawString)
        {
            return rawString.Replace("%20", " ");
        }

        /// <summary>
        /// Test Collection for required incoming Aurora params
        /// </summary>
        /// <param name="paramsCollection"></param>
        /// <returns>Required Check Status</returns>
        public static bool HasRequiredParams(NameValueCollection paramsCollection)
        {//"Brand=X&CountryCode=NZ&VehicleCode=&CheckOutZoneCode=AK2&CheckOutDateTime=15-JUN-2009%2010:00&CheckInZoneCode=Ak2&CheckInDateTime=15-JUL-2009%2010:00&NoOfAdults=2&NoOfChildren=0&AgentCode=B2CNZ&PackageCode=&IsVan=True&IsBestBuy=True&CountryOfResidence=NZ&IsTestMode=false";
            DateTime parseTester;
            if (paramsCollection["Brand"] != null &&
                paramsCollection["CountryCode"] != null /* && (paramsCollection["CountryCode"].ToUpper().Equals("NZ") || paramsCollection["CountryCode"].ToUpper().Equals("AU"))*/ &&
                paramsCollection["CheckOutZoneCode"] != null &&//TODO: validate
                paramsCollection["CheckInZoneCode"] != null && //TODO: validate
                paramsCollection["NoOfAdults"] != null &&
                paramsCollection["IsVan"] != null &&//TODO: validate
                paramsCollection["IsBestBuy"] != null &&
                DateTime.TryParse(paramsCollection["CheckOutDateTime"], out parseTester) &&
                DateTime.TryParse(paramsCollection["CheckInDateTime"], out parseTester)
                )
                return true;
            else
                return false;
        }

        /// <summary>
        /// Mapper from ctrl params to Aurora collection, TODO: Fork by Ctrl Type, current horizontal
        /// </summary>
        /// <param name="BPAECollection"></param>
        /// <returns></returns>
        public static NameValueCollection CtrlToAuroraCollectionMapper(NameValueCollection CtrlParamsCollection)
        {
            NameValueCollection nvc = new NameValueCollection();
            foreach (string key in CtrlParamsCollection.AllKeys)
            {
                switch (key)
                {
                    case "controlBrand":
                        nvc.Add("Brand", CtrlParamsCollection[key]);
                        break;
                    case "vt":
                        nvc.Add("IsVan", (CtrlParamsCollection[key].ToUpper().Contains("AV") || CtrlParamsCollection[key].ToUpper().Contains("RV") ? "True" : "False"));
                        break;
                    case "ctrlSearchBox_dropVehicleType":
                        //TODO: check
                        break;
                    case "countryCode":
                        nvc.Add("CountryCode", CtrlParamsCollection[key]);
                        break;
                    case "ctrlSearchBox_calPickUp_DateText":
                        nvc.Add("CheckOutDateTime", DateTimeHelper.ConvertCtrlDateTimeToAurora(CtrlParamsCollection["ctrlSearchBox_calPickUp_DateText"], CtrlParamsCollection["ctrlSearchBox_dropPickUpTime"]));
                        break;
                    case "ctrlSearchBox_calDropOff_DateText":
                        nvc.Add("CheckInDateTime", DateTimeHelper.ConvertCtrlDateTimeToAurora(CtrlParamsCollection["ctrlSearchBox_calDropOff_DateText"], CtrlParamsCollection["ctrlSearchBox_dropDropOffTime"]));
                        break;
                    case "ctrlSearchBox_dropChildren":
                        nvc.Add("NoOfChildren", CtrlParamsCollection[key]);
                        break;
                    case "ctrlSearchBox_dropAdults":
                        nvc.Add("NoOfAdults", CtrlParamsCollection[key]);
                        break;
                    case "ctrlSearchBox_dropDropOffLocation":
                        nvc.Add("CheckInZoneCode", CtrlParamsCollection[key]);
                        break;
                    case "ctrlSearchBox_dropPickUpLocation":
                        nvc.Add("CheckOutZoneCode", CtrlParamsCollection[key]);
                        break;
                    case "countryOfResidence":
                        nvc.Add("CountryOfResidence", CtrlParamsCollection[key]);
                        break;
                    case "vehicleCode":
                        nvc.Add("VehicleCode", CtrlParamsCollection[key]);
                        break;
                    case "ctrlSearchBox_dropVendor":
                        nvc.Add("CompanyCode", CtrlParamsCollection[key]);
                        break;

                    case "pc":
                        nvc.Add("PackageCode", CtrlParamsCollection[key]);
                        break;


                }
            }
            nvc.Add("IsTestMode", "False");//TODO: from config
            nvc.Add("IsBestBuy", "True");//TODO: from config
            return nvc;
        }

        /// <summary>
        /// Old Control Params Mapper
        /// </summary>
        /// <param name="CtrlParamsCollection"></param>
        /// <returns></returns>
        public static NameValueCollection OldControlCollectionMapper(NameValueCollection CtrlParamsCollection)
        {
            string brandStr = string.Empty;
            NameValueCollection nvc = new NameValueCollection();
            
            foreach (string key in CtrlParamsCollection.AllKeys)
            {
                switch (key)
                {
                    case "vc":
                        if (CtrlParamsCollection["vc"] != string.Empty)
                        {
                            brandStr = THLBrand.GetBrandCharForBPAECode(CtrlParamsCollection[key]);
                            nvc.Add("Brand", brandStr);
                        }
                        break;
                    case "brand":
                        nvc.Add("Brand", CtrlParamsCollection[key].ToUpper());
                        break;
                    case "vtype":
                        nvc.Add("IsVan", (CtrlParamsCollection[key].ToUpper() == "AV" || CtrlParamsCollection[key].ToUpper() == "RV" ? "True" : "False"));
                        break;

                    case "vt"://test
                        nvc.Add("IsVan", (CtrlParamsCollection[key].ToUpper() == "AV" || CtrlParamsCollection[key].ToUpper() == "RV" ? "True" : "False"));
                        break;                 
                        
                    case "ctrlSearchBox_dropVehicleType":
                        //TODO: check
                        break;
                    case "cc":
                        nvc.Add("CountryCode", CtrlParamsCollection[key]);
                        break;
                    case "pd":
                        //pd=23&pm=8&py=2009&pt=09:00&                    
                        nvc.Add("CheckOutDateTime", DateTimeHelper.ConvertCtrlDateTimeToAurora(DateTimeHelper.LeadingZerosPadding(CtrlParamsCollection["pd"], 2) + "-" + DateTimeHelper.NumberToMonth(CtrlParamsCollection["pm"]) + "-" + CtrlParamsCollection["py"] , CtrlParamsCollection["pt"]));
                        break;
                    case "dd":
                        //dd=18&dm=9&dy=2009&dt=09:00&
                        nvc.Add("CheckInDateTime", DateTimeHelper.ConvertCtrlDateTimeToAurora(DateTimeHelper.LeadingZerosPadding(CtrlParamsCollection["dd"], 2) + "-" + DateTimeHelper.NumberToMonth(CtrlParamsCollection["dm"]) + "-" + CtrlParamsCollection["dy"], CtrlParamsCollection["dt"]));
                        break;
                    case "nc":
                        nvc.Add("NoOfChildren", CtrlParamsCollection[key]);
                        break;
                    case "na":
                        nvc.Add("NoOfAdults", CtrlParamsCollection[key]);
                        break;
                    case "pb":
                        nvc.Add("CheckOutZoneCode", ConvertBPAEToAuroraLocationCode(CtrlParamsCollection[key]));
                        break;
                    case "db":
                        nvc.Add("CheckInZoneCode", ConvertBPAEToAuroraLocationCode(CtrlParamsCollection[key]));
                        break;
                    case "cr":
                        nvc.Add("CountryOfResidence", CtrlParamsCollection[key]);
                        break;
                    case "vh":
                        nvc.Add("VehicleCode", ConvertBPAEToAuroraVehicleCode(CtrlParamsCollection[key]));
                        break;
                    case "ch":
                        nvc.Add("ChannelCode", CtrlParamsCollection[key]);
                        break;
                    case "pc":
                        nvc.Add("PackageCode", CtrlParamsCollection[key]);
                        break;                    
                    //TODO:
                    //vc=ma&
                    //dd=18&
                    //dm=9&
                    //dy=2009&
                    //dt=09:00&
                    //vt=rv&
                    //vh=               
                }
            }
            nvc.Add("IsTestMode", "False");//TODO: from config
            nvc.Add("IsBestBuy", "True");//TODO: from config
            return nvc;
        }
        
        public static CountryCode GetCountryCodeForString(string countryCodeString)
        {
            CountryCode code = CountryCode.NONE;
            if (!string.IsNullOrEmpty(countryCodeString))
            {
                switch (countryCodeString.ToLower())
                {
                    case "nz":
                        code = CountryCode.NZ;
                        break;
                    case "au":
                        code = CountryCode.AU;
                        break;
                    case "us":
                        code = CountryCode.US;
                        break;
                }
            }
            else 
            {
                code = CountryCode.NONE;
            }
            return code;
        }

        public static string GetCountryName(CountryCode code)
        {
            string countryName = string.Empty;
            switch (code)
            {
                case CountryCode.NZ:
                    countryName = "New Zealand";
                    break;
                case CountryCode.AU:
                    countryName = "Australia";
                    break;
                //TODO: Add MAC Countries
            }
            return countryName;
        }

        /// <summary>
        /// remove any prefix from incoming URLs
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string NormalizeURL(string url)
        {
            url = url.Replace("http://", string.Empty);
            url = url.Replace("https://", string.Empty);
            return url;
        }

        public static string NVCToQueryString(NameValueCollection nvc)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in nvc.AllKeys)
                sb.Append(key + "=" + nvc[key] + "&");
            return sb.ToString().TrimEnd('&');
        }

        public static bool HasDiscountedDays(AvailabilityItemChargeRateBand[] rateBands)
        {
            foreach (AvailabilityItemChargeRateBand rateBand in rateBands)
            {
                if (rateBand.OriginalRate > rateBand.DiscountedRate)
                    return true;
            }
            return false;
        }

        public static bool HasFreeDays(AvailabilityItemChargeRateBand[] rateBands)
        {
            foreach (AvailabilityItemChargeRateBand rateBand in rateBands)
            {
                if (rateBand.IncludesFreeDay > 0)
                    return true;
            }
            return false;
        }
        /// <summary>
        /// Map Aurora Type to Booking Type
        /// </summary>
        /// <param name="vehicleTypeStr"></param>
        /// <returns></returns>
        public static VehicleType GetVehicleTypeForAuroraString(string vehicleTypeStr)
        {
            VehicleType vehicleType = VehicleType.Both;
            switch (vehicleTypeStr)
            { 
                case "av":
                    vehicleType = VehicleType.Campervan;
                    break;
                case "ac":
                    vehicleType = VehicleType.Car;
                    break;
            }
            return vehicleType;        
        }


        /// <summary>
        /// Map VehicleType to Aurora vehicle StrAurora Type to Booking Type
        /// </summary>
        /// <param name="vehicleTypeStr"></param>
        /// <returns></returns>
        public static string GetAuroraVehicleStrForVehicleType(VehicleType vehicleType)
        {
            string vehicleTypeStr = "both";
            switch (vehicleType)
            {
                case VehicleType.Campervan:
                    vehicleTypeStr = "av";
                    break;
                case VehicleType.Car:
                    vehicleTypeStr = "ac";
                    break;
            }
            return vehicleTypeStr;
        }
        
        public static string ConvertBPAEToAuroraVehicleCode(string bpaeVehicleCode)//aucvbz.2BB -> auavb.2BB
        {
                if (!bpaeVehicleCode.Contains("cv") && !bpaeVehicleCode.Contains("rv")) return bpaeVehicleCode;//not bpae
                
                string countryStr = bpaeVehicleCode.Substring(0, 2);
                string vehicleTypeStr = bpaeVehicleCode.Substring(2, 2);
                string brandStr = bpaeVehicleCode.Substring(4, 2);
                string vehicleAuroraCodeStr = bpaeVehicleCode.Split('.')[1];
                return countryStr + (vehicleTypeStr == "cv" ? "av" : "ac") + THLBrand.GetBrandCharForBPAECode(brandStr) + "." + vehicleAuroraCodeStr;
        }

        public static string ConvertBPAEToAuroraLocationCode(string bpaeLocationCode)
        {
            NameValueCollection bpaeMapper = ApplicationManager.GetApplicationData().BPAEMapper;
            if (bpaeMapper[bpaeLocationCode] != null)
                return bpaeMapper[bpaeLocationCode].ToUpper();
            return bpaeLocationCode;
        }

        public static String ConstructQueryString(NameValueCollection parameters)
        {
            List<String> items = new List<String>();
            foreach (String name in parameters)
                items.Add(String.Concat(name, "=", parameters[name]));
            return String.Join("&", items.ToArray());
        }

        
        /// <summary>
        /// Generate an Availability Ration string for Reporting purposes
        /// </summary>
        /// <param name="availabilityResponse"></param>
        /// <returns></returns>
        static string availabilityRate(AvailabilityResponse availabilityResponse, AvailabilityRequest availabilityRequest)
        {
            string rateStr = string.Empty;
            try
            {
                if (availabilityResponse.GetAvailabilityItems(string.Empty) != null)
                {
                    int availCounter = 0, nonAvailCounter = 0;
                    AvailabilityItem[] items = availabilityResponse.GetAvailabilityItems(string.Empty);
                    string statusCodes = string.Empty;
                    foreach (AvailabilityItem aItem in items)
                    {
                        if (aItem.AvailabilityType == AvailabilityItemType.Available)
                        {
                            statusCodes += aItem.VehicleCode + "=1:";
                            availCounter++;
                        }
                        else
                        {
                            statusCodes += aItem.VehicleCode + "=0:";
                            nonAvailCounter++;                        
                        }
                    }
                    if ((availCounter + nonAvailCounter) == 0)
                        return string.Empty;//this is a case where no data exists, i.e. no resposne.

                    //-- Enhance reporting here 12/08/10 (add FinWeek and locations on top of availability
                    //  00/10/42/SYD/SYD                    
                    string resultStr = availCounter.ToString().PadLeft(2, '0') + "," //available vehicle rows
                        + (availCounter + nonAvailCounter).ToString().PadLeft(2, '0') + "," //total vehicle option rows
                        + availabilityRequest.CountryOfResidence.ToUpper() + "," //Driver's license                         
                        + availabilityRequest.PickUpBranch.ToUpper() + "," // pickup location
                        + availabilityRequest.PickUpDate.ToString("dd/MM/yyyy") + "," // pickup date
                        + availabilityRequest.DropOffBranch.ToUpper() + "," //dropoff location
                        + availabilityRequest.DropOffDate.ToString("dd/MM/yyyy") + "," //dropoff date
                        + availabilityRequest.NumberOfAdults + "," //adults
                        + availabilityRequest.NumberOfChildren + ","  //children
                        + statusCodes;                                               
                        //+ DateTimeHelper.FinWeekForDate(availabilityRequest.PickUpDate) + ","; //financial week pickup
                    return resultStr;                    
                    //--------------------------- Enhancment ends 
                    //return availCounter + "/" + (availCounter + nonAvailCounter);
                }
                else {
                    return "n/a";                
                }
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Presentation, "AvailabilityHelper.AvailabilityRate", ex.Message, "{error:'failed to generating availability ratio'}");
                return "n/a";
            }            
        }

        public static AnalyticsEventTrackingItem[] GetTrackingEventsForAvailResponse(AvailabilityResponse availabilityResponse, AvailabilityRequest availabilityRequest)
        {
            try
            {
                
                string availRateStr = availabilityRate(availabilityResponse, availabilityRequest);
                int availCounter = 0;
                string[] availParams = availRateStr.Split('/');
                if(availParams != null && availParams.Length > 0 && availParams[0] != "n")
                {
                    int.TryParse(availParams[0], out  availCounter);
                }
                AnalyticsEventTrackingItem aet = new AnalyticsEventTrackingItem(availabilityRequest.VehicleType.ToString(), "Availability", availRateStr, availCounter);
                //AnalyticsEventTrackingItem leadTimeTrack = new AnalyticsEventTrackingItem(availabilityRequest.VehicleType.ToString(), "Lead",
                               
                return new AnalyticsEventTrackingItem[] { aet };            
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Presentation, "AvailabilityHelper.GetTrackingEventsForAvailResponse", ex.Message, "{error:'failed to generating availability tracking item'}");
                return null;            
            }            
        }


        public static string GetTrackingImgForAvailResponse(AvailabilityResponse availabilityResponse, AvailabilityRequest availabilityRequest)
        {
            try
            {
                string availRateStr = availabilityRate(availabilityResponse, availabilityRequest);
                if (string.IsNullOrEmpty(availRateStr))
                    return string.Empty;//no valid data.
                
                int availCounter = 0;
                string[] availParams = availRateStr.Split('/');
                if (availParams != null && availParams.Length > 0 && availParams[0] != "n")
                {
                    int.TryParse(availParams[0], out  availCounter);
                }
                string availParamsStr = availRateStr;
                //Added fields
                string domain = ApplicationManager.GetDomainForURL(HttpContext.Current.Request.Url.Host).Name;

                //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
                if (domain.Contains("motorhomesandcars") && HttpContext.Current.Request.Params["country"]!=null){
                    //add country code for motorhomesandcars for reporting purpose.
                    domain = domain + "/" + HttpContext.Current.Request.Params["country"];
                }

                availParamsStr = availParamsStr + "," + domain + "," + HttpContext.Current.Session.SessionID;
                //End Added Fields, TODO: refactor once requirements have been sealed
                string trackingBase = !string.IsNullOrEmpty(System.Web.Configuration.WebConfigurationManager.AppSettings["CrossSellURL"]) ? System.Web.Configuration.WebConfigurationManager.AppSettings["CrossSellURL"] : "/";
                string imgTag = @"<img id=""trkImg"" height=""1"" width=""1"" style=""border-style:none;"" alt="""" src=""" +  trackingBase + "/rpt/t.png?ev=" + availParamsStr + @""" />";
                return imgTag;
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Presentation, "AvailabilityHelper.GetTrackingImgForAvailResponse", ex.Message, "{error:'failed to generating availability tracking image item'}");
                return null;
            }
        }


        public static AnalyticsEventTrackingItem[] GetTrackingEventsForAltAvailResponse(AvailabilityResponse availabilityResponse, AvailabilityRequest availabilityRequest)
        {
            try
            {
                List<AnalyticsEventTrackingItem> aetList = new List<AnalyticsEventTrackingItem>();
                string availRateStr = availabilityRate(availabilityResponse, availabilityRequest);
                int availCounter = 0;
                //string[] availParams = availRateStr.Split('/');
                //if (availParams != null && availParams.Length > 0 && availParams[0] != "n")
                //{
                //    int.TryParse(availParams[0], out  availCounter);
                //}
                foreach (AvailabilityItem item in availabilityResponse.GetAvailabilityItems(string.Empty))
                {
                    AlternativeAvailabilityItem aaItem = (AlternativeAvailabilityItem)(item);                    
                    availRateStr += "/" + aaItem.Type.ToString() ;
                    AnalyticsEventTrackingItem aet = new AnalyticsEventTrackingItem(availabilityRequest.VehicleType.ToString(), "AltAvailability", availRateStr, availCounter);
                    aetList.Add(aet);
                } 
                //AnalyticsEventTrackingItem aet = new AnalyticsEventTrackingItem(availabilityRequest.VehicleType.ToString(), "AltAvailability", availRateStr, availCounter);
                //AnalyticsEventTrackingItem leadTimeTrack = new AnalyticsEventTrackingItem(availabilityRequest.VehicleType.ToString(), "Lead",
                return aetList.ToArray();
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Presentation, "AvailabilityHelper.GetTrackingEventsForAltAvailResponse", ex.Message, "{error:'failed to generating availability tracking item'}");
                return null;
            }
        }




        /// <summary>
        /// Get UOM Enum object for Aurora attribute string
        /// </summary>
        /// <param name="uomString"></param>
        /// <returns></returns>
        public static UOMType UOMFromString(string uomString)
        {
            UOMType oumType = UOMType.None;
            switch(uomString.ToUpper())
            {
                case "DAY":
                    oumType = UOMType.Day;
                    break;
                case "24HR":
                    oumType = UOMType.H24;
                    break;
            }
            return oumType;
        }

        /// <summary>
        /// Return a central Library reference for the provider request and Brand/Vehicle Context
        /// </summary>
        /// <param name="request">The Users Request</param>
        /// <param name="appDomain">The Domain to reference this path to</param>
        /// <param name="assetsURL">base TLD of the resources site</param>
        /// <returns></returns>
        public static string GetCentralLibPath(AvailabilityRequest request, THLDomain appDomain, string assetsURL)
        {
            try
            {
                string BrandChar = appDomain.BrandChar.ToString();
                string BrandCountryAssetPath = THLBrand.GetNameForBrand(appDomain.Brand) + "/" + AvailabilityHelper.GetCountryName(appDomain.Country).Replace(" ", "-"); //"Britz/Australia"
                
                if(appDomain.IsAggregator)//added for AltAvail Aggregator support 25/05/2011,TODO: define Aggregator assets default currenttly NZ
                    BrandCountryAssetPath = "MAC" + "/" + (request.CountryCode == CountryCode.NONE ? "New-Zealand" : AvailabilityHelper.GetCountryName(request.CountryCode).Replace(" ", "-"));
                if (appDomain.Brand == THLBrands.AIRNZ)//TODO: define convention, and default state when bo country defined now on: "New-Zealand"
                    BrandCountryAssetPath = "AirNZ" + "/" + (request.CountryCode == CountryCode.NONE ? "New-Zealand" : AvailabilityHelper.GetCountryName(request.CountryCode).Replace(" ", "-"));

                string VehicleTypeStr = (request.VehicleType == VehicleType.Car ? "Cars" : "Campervans");
                string VehicleImagePath = assetsURL + "/CentralLibraryImages/" + BrandCountryAssetPath + "/" + VehicleTypeStr;
                VehicleImagePath = VehicleImagePath.Replace(' ', '-');
                return VehicleImagePath;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                //TODO: define logging requirements and implement
                return string.Empty;//TODO: defualt path?
            }
        }

        public static string GetVehicleImageBasePath(CountryCode countryCode, THLBrands brand, VehicleType vType, string vCode, string CDNBase, bool domainVariation)
        {
            if (domainVariation)
            {
                THLDomain appDomain = ApplicationManager.GetDomainForURL(HttpContext.Current.Request.Url.Host);
                if (appDomain.Country == CountryCode.NONE || appDomain.IsAggregator)
                {
                    return GetAggregatorImageBasePath(countryCode, brand, vType, vCode, CDNBase);
                }
            }
            return GetBrandImageBasePath(countryCode, brand, vType, vCode, CDNBase);
        }

        public static string GetBrandImageBasePath(CountryCode countryCode,  THLBrands brand, VehicleType vType, string vCode, string CDNBase) {
            string brandCountryAssetPath = String.Format("{0}/{1}", THLBrand.GetNameForBrand(brand).Replace(" ", "-"), GetCountryName(countryCode).Replace(" ", "-"));
            string vehicleTypeInString = (vType == VehicleType.Car ? "Cars" : "Campervans");
            string vehicleImageNameConvension = countryCode + GetAuroraVehicleStrForVehicleType(vType) + THLBrand.GetBrandChar(brand) + "." + vCode;
            return String.Format("{0}/CentralLibraryImages/{1}/{2}/{3}/{3}", CDNBase, brandCountryAssetPath, vehicleTypeInString, vehicleImageNameConvension, vehicleImageNameConvension);
        }

        public static string GetAggregatorImageBasePath(CountryCode countryCode, THLBrands brand, VehicleType vType, string vCode, string CDNBase)
        {
            string aggregatorCountryAssetPath = String.Format("MAC/{0}", GetCountryName(countryCode).Replace(" ", "-"));
            string vehicleTypeInString = (vType == VehicleType.Car ? "Cars" : "Campervans");
            string vehicleImageNameConvension = countryCode + GetAuroraVehicleStrForVehicleType(vType) + THLBrand.GetBrandChar(brand) + "." + vCode;
            return String.Format("{0}/CentralLibraryImages/{1}/{2}/{3}/{3}", CDNBase, aggregatorCountryAssetPath, vehicleTypeInString, vehicleImageNameConvension, vehicleImageNameConvension);
        }

        /// <summary>
        /// Get Alternate Availability Request URL parameters
        /// </summary>
        /// <param name="availabilityRequest"></param>
        /// <returns></returns>
        public static string GetAlternateAvailabilityRequestURL(AvailabilityRequest availabilityRequest)
        {

            //add cross sell multiple brands support
            string vehicleBrand = availabilityRequest.Brand.ToUpper();

            StringBuilder sb = new StringBuilder();
            sb.Append("Brand=" +  /* swapped for CSell: availabilityRequest.Brand */ vehicleBrand + "&");
            sb.Append("VehicleCode=" + availabilityRequest.VehicleModel + "&");
            sb.Append("CountryCode=" + availabilityRequest.CountryCode + "&");
            sb.Append("CheckOutZoneCode=" + availabilityRequest.PickUpBranch + "&");
            sb.Append("CheckOutDateTime=" + availabilityRequest.PickUpDate.ToString("dd-MMM-yyyy HH:mm") + "&");
            sb.Append("CheckInZoneCode=" + availabilityRequest.DropOffBranch + "&");
            sb.Append("CheckInDateTime=" + availabilityRequest.DropOffDate.ToString("dd-MMM-yyyy HH:mm") + "&");
            sb.Append("AgentCode=" + availabilityRequest.AgentCode + "&");
            sb.Append("PackageCode=" + availabilityRequest.PackageCode + "&");
            sb.Append("IsVan=" + (availabilityRequest.VehicleType == VehicleType.Car ? "False" : "True") + "&");
            sb.Append("NumberOfAdults=" + availabilityRequest.NumberOfAdults + "&");
            sb.Append("NoOfChildren=" + availabilityRequest.NumberOfChildren + "&");
            sb.Append("isBestBuy=True&");
            sb.Append("countryOfResidence=" + availabilityRequest.CountryOfResidence + "&");
            //if pick & drop are same no 3,6 (switch loc)
            sb.Append("DisplayMode=1,2,4,5" + (!availabilityRequest.PickUpBranch.Equals(availabilityRequest.DropOffBranch) ? ",3,6" : string.Empty) + "&");//TODO: move to config
            sb.Append("Istestmode=False&");//TODO: read from config
            return sb.ToString();            
        }

        /// <summary>
        /// Normalise brand input to Aurora Format
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public static string NormaliseBrandString(string brand)
        {

            string brandsStr = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(brand))
                {
                    brandsStr = brand.Length > 1 ? brand.Substring(0, 1) : brand;
                    brandsStr = brandsStr.ToUpper();
                }
                if (brandsStr.Equals("C") || brandsStr.Equals("Z"))
                    brandsStr = string.Empty;
                //THLDebug.LogEvent(EventTypes.MTierStatus, "Normalized Brand from " + brand + " -> " + brandsStr);  
            }
            catch (Exception ex)
            {
                THLDebug.LogError(ErrorTypes.Application, "AvailabilityHelper.NormaliseBrandString", ex.Message, "{brand:\"" + brand +"\"");
            }
            return brandsStr;
        }

        /// <summary>
        /// Convert widget supplied params for Availability Request  
        /// </summary>
        /// <param name="requestParams">HTTP Request params</param>
        /// <returns>Availability Request Objects</returns>
        public static AvailabilityRequest GetAvailabilityRequestForWidgetParams(NameValueCollection requestParams)
        {
            AvailabilityRequest request = new AvailabilityRequest();

            request.Brand = !string.IsNullOrEmpty(requestParams["brand"]) ? requestParams["brand"].ToString() : string.Empty;

            switch (requestParams["vt"])
            { 
                case "campers":
                    request.VehicleType = VehicleType.Campervan;
                    break;
                case "cars":
                    request.VehicleType = VehicleType.Car;
                    break;
                default:
                    request.VehicleType = VehicleType.Both;
                    break;
            }
            request.VehicleModel = requestParams["vc"];
            request.CountryOfResidence = requestParams["cr"];
            request.CountryCode = AvailabilityHelper.GetCountryCodeForString(requestParams["cc"]);
            
            request.AgentCode = !string.IsNullOrEmpty(requestParams["ac"]) ? requestParams["ac"] : string.Empty;
            
            return request;
        }

        

        /// <summary>
        /// Convert Availability Request to JSON Object, TODO: upgrade to Object Contract with 4.0 once avilable
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetWidgetJSONForRequest(AvailabilityRequest request, Branch[] branches, Vehicle[] vehicles)
        {
            StringBuilder branchSb = new StringBuilder(), vehicleSb = new StringBuilder();
            foreach(Branch branch in branches)
            {
                string brandsStr = string.Empty;
                foreach (THLBrands brand in branch.brands)
                    brandsStr += THLBrand.GetBrandChar(brand);
                branchSb.Append(@"{""name"":""" + branch.Name + @""",""code"":""" + branch.BranchCode + @""",""brands"":""" + brandsStr + @""",""vt"":""" + branch.VehicleTypeAvailability.ToString().ToLower() + @""",""cc"":""" + branch.country.ToString().ToLower() + @"""},");
            }
            foreach (Vehicle vehicle in vehicles)
                vehicleSb.Append(@"{""name"":""" + vehicle.Name + @""",""code"":""" + vehicle.Code + @""",""brand"":""" + THLBrand.GetBrandChar(vehicle.Brand) + @""",""cc"":""" + vehicle.Country.ToLower() + @""",""vt"":""" + vehicle.Type.ToString().ToLower() + @"""},");
            return @"{""state"":{""brand"":""" + request.Brand.ToLower() +  @""",""cc"":""" + request.CountryCode.ToString().ToLower() + @""",""type"":""" + request.VehicleType.ToString().ToLower() + @""",""cr"":""" + request.CountryOfResidence + @""",""model"":"""+ request.VehicleModel +@""",""agent"":""" +  request.AgentCode + @"""},""branches"":[" + branchSb.ToString().TrimEnd(',') + @"],""vehicles"":[" + vehicleSb.ToString().TrimEnd(',') + @"]}";
        }


        /// <summary>
        /// Convert Availability Request to JSON Object, TODO: upgrade to Object Contract with 4.0 once avilable
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string GetWidgetXMLForRequest(AvailabilityRequest request, Branch[] branches, Vehicle[] vehicles)
        {
            StringBuilder branchSb = new StringBuilder(), vehicleSb = new StringBuilder();
            foreach (Branch branch in branches)
            {
                string brandsStr = string.Empty;
                foreach (THLBrands brand in branch.brands)
                    brandsStr += THLBrand.GetBrandChar(brand);
                branchSb.Append(@"<branch name=""" + branch.Name + @""" code=""" + branch.BranchCode + @""" brandsString=""" + brandsStr + @""" type=""" + branch.VehicleTypeAvailability.ToString().ToLower() + @""" country=""" + branch.country.ToString().ToLower() + @"""/>");
            }
            foreach (Vehicle vehicle in vehicles)
                vehicleSb.Append(@"<vehicle name=""" + vehicle.Name + @""" code=""" + vehicle.Code + @""" brand=""" + THLBrand.GetBrandChar(vehicle.Brand) + @""" country=""" + vehicle.Country.ToLower() + @""" type=""" + vehicle.Type.ToString().ToLower() + @"""/>");
            return @"<?xml version=""1.0"" encoding=""utf-8""?><thlcontroldata><branches>" + branchSb.ToString() + @"</branches><vehicles>" + vehicleSb.ToString() + @"</vehicles></thlcontroldata>";
        }

        public static string LoadOpenTag(THLDomain currentDomain)
        {
            return "<script src='//d3c3cq33003psk.cloudfront.net/opentag-97168-" + currentDomain.OpenTag + ".js' async defer></script>";
        }
    }
}