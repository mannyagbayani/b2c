﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace THL.Booking
{

    public enum Month
    {
        Jan = 1,
        Feb = 2,
        Mar = 3,
        Apr = 4,
        May = 5,
        Jun = 6,
        Jul = 7,
        Aug = 8,
        Sep = 9,
        Oct = 10,
        Nov = 11,
        Dec = 12
    }

    /// <summary>
    /// DateTime Functionality and Logic Helper
    /// </summary>
    /// 
    public class DateTimeHelper
    {
        public static DateTime GetDateTimeForQueryParams(int day, int month, int year, string timeStr)
        {
            string[] timeParams = new string[2];
            if (timeStr.IndexOf(':') > 0)
                timeParams = timeStr.Split(':');
            int hour = 0, minutes = 0;
            int.TryParse(timeParams[0], out hour);
            int.TryParse(timeParams[1], out minutes);
            DateTime dt;
            if (year > 2008 && month > 1 && day > 0)
                dt = new DateTime(year, month, day, hour, minutes, 0);
            else
                dt = new DateTime();
            return dt;
        }


        public static string ConvertCtrlDateTimeToAurora(string ctrlDate, string ctrlTime)
        {
            //TODO: construct an aurora request date time str from ctrl date/time strs    
            return ctrlDate + " " + ctrlTime;
        }

        /// <summary>
        /// Converts integer month number(passed as string from the booking control) to three char Aurora Month convention
        /// </summary>
        /// <param name="numberStr"></param>
        /// <returns>Three Char Month Name</returns>
        public static string NumberToMonth(string numberStr)
        {
            int numberInt = 0;
            string monthStr = string.Empty;
            int.TryParse(numberStr, out numberInt);
            if(numberInt > 0 && numberInt <13)
                monthStr = ((Month)numberInt).ToString();
            return monthStr;
        }


        public static string LeadingZerosPadding(string incomingIntStr, int length)
        {
            StringBuilder paddingStr = new StringBuilder();
            for (int i = 0; i < (length - incomingIntStr.Length ); i++)
            {
                paddingStr.Append("0");
            }
            return (paddingStr.ToString() + incomingIntStr);
        }

        /// <summary>
        /// Get the financial week for the provided Date
        /// </summary>
        /// <param name="pickUpDate"></param>
        /// <returns></returns>
        public static string FinWeekForDate(DateTime pickUpDate)
        {
            try
            {
                int finWeek = 0;
                int zeroLeftPadding = 2;
                //get the previous 1st of July, TODO: cosider passing financial year start as a parameter
                DateTime yearsFirstMonday = pickUpDate;
                //Get the First Day of July
                if (yearsFirstMonday.Month != 7)
                    while (yearsFirstMonday.Month != 7)
                        yearsFirstMonday = yearsFirstMonday.AddMonths(-1);
                //now find the first monday:
                yearsFirstMonday = new DateTime(yearsFirstMonday.Year, yearsFirstMonday.Month, 1);
                //and get the first monday
                while (yearsFirstMonday.DayOfWeek != DayOfWeek.Monday)
                    yearsFirstMonday = yearsFirstMonday.AddDays(1);
                if (pickUpDate < yearsFirstMonday)//this is the early july day exception
                {
                    yearsFirstMonday = new DateTime(yearsFirstMonday.Year - 1, 7, 1);
                    while (yearsFirstMonday.DayOfWeek != DayOfWeek.Monday)
                        yearsFirstMonday = yearsFirstMonday.AddDays(1);
                }
                finWeek = (pickUpDate - yearsFirstMonday).Days / 7 + 1;
                int pickUpFinYear = (yearsFirstMonday.Year);
                return finWeek.ToString().PadLeft(zeroLeftPadding, '0') + "-" + pickUpFinYear.ToString();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                //TODO: log point here
                return "n/a";
            }
        
        }
        
        public DateTimeHelper()
        {

        }
    }
}