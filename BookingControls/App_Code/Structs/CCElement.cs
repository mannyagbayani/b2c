﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace THL.Booking
{
    /// <summary>
    /// Credit Card Information from Aurora
    /// </summary>
    public class CreditCardElement
    {
        public string Name;
        public decimal Surcharge;
        public string Code;

        public CreditCardElement(string code, string name, decimal surcharge)
        {
            Code = code;
            Name = name;
            Surcharge = surcharge;
        }
    }
}