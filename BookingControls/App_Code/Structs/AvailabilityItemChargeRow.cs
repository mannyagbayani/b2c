﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;


namespace THL.Booking
{
    /// <summary>
    /// Availability Item Charge Element
    /// </summary>
    public class AvailabilityItemChargeRow
    {
        public string ProductCode;

        public string ProductName;

        public decimal ProductPrice;
        public decimal AverageDailyRate;

        //TODO: consider Enums
        public string ProductType;
        public string ProductClass;
        public string UnitOfMeasurement;

        public bool IsVehicle;
        public bool TaxIncluded;

        public string ProductInfo;
        public string CurrencyCode;

        public string FlexNumber;
        public int HirePeriod;

        public decimal AdminFee;       

        private bool isBonusPack;
        public bool IsBonusPack
        {
            get { return isBonusPack; }
        }

        private decimal totalSavings;
        public decimal TotalSavings {
            get { return (totalSavings != null && totalSavings>0 ? totalSavings : 0); }
        }
        
        public AvailabilityItemChargeRateBand[] RateBandCollection;
        public AvailabilityItemChargeDiscount[] DiscountCollection;

        public bool LoadFromXml(XmlNode xmlNode)
        {
            bool success = true;
            
            success = decimal.TryParse(xmlNode.Attributes["AvgRate"].Value, out AverageDailyRate);
            success = decimal.TryParse(xmlNode.Attributes["Price"].Value, out ProductPrice);
            if (xmlNode.Attributes["HirePeriod"].Value != "0")
                success = int.TryParse(xmlNode.Attributes["HirePeriod"].Value.Substring(0, xmlNode.Attributes["HirePeriod"].Value.IndexOf(".")), out HirePeriod);
            else
                HirePeriod = 0;
 
            //try {           
            ProductCode = xmlNode.Attributes["PrdCode"].Value;
            ProductName = xmlNode.Attributes["PrdName"].Value;
            UnitOfMeasurement = xmlNode.Attributes["UOM"].Value;
            ProductType = xmlNode.Attributes["Type"].Value;
            ProductClass = xmlNode.Attributes["Class"].Value;
            IsVehicle = (xmlNode.Attributes["IsVeh"].Value == "1" ? true : false);
            ProductInfo = xmlNode.Attributes["ProductInfo"].Value;
            TaxIncluded = (xmlNode.Attributes["TaxInclude"].Value == "1" ? true : false);
            CurrencyCode = xmlNode.Attributes["CurrCode"].Value;
            // } catch (log and set success = false)                                   

            AdminFee = getAdminFee(xmlNode);


            //process <RateBands/Bands> into RateBandCollection
            XmlNode RateBandsElm = xmlNode.SelectSingleNode("RateBands/Bands");
            
            //if(RateBandsElm.Attributes["TotalSaving"] != null)
                           
            List<AvailabilityItemChargeRateBand> airbList = new List<AvailabilityItemChargeRateBand>();
            if (RateBandsElm != null) {
                 decimal.TryParse(RateBandsElm.Attributes["TotalSaving"].Value, out totalSavings);
                foreach (XmlNode rateBandNode in RateBandsElm) {//foreach <Band> node
                    AvailabilityItemChargeRateBand aiRateBand = new AvailabilityItemChargeRateBand();
                    aiRateBand.LoadFromXML(rateBandNode);
                    airbList.Add(aiRateBand);
                }
                RateBandCollection = airbList.ToArray();
            }
            
            //Process <Discounts/Discount>
            XmlNode DiscountsElm = xmlNode.SelectSingleNode("RateBands/Discounts");
            List<AvailabilityItemChargeDiscount> aidList = new List<AvailabilityItemChargeDiscount>();
            if (DiscountsElm != null)
            {
                foreach (XmlNode discountNode in DiscountsElm)
                {//foreach <Discounts> node
                    AvailabilityItemChargeDiscount aiDiscount = new AvailabilityItemChargeDiscount();
                    aiDiscount.LoadFromXML(discountNode);
                    aidList.Add(aiDiscount);
                }
                DiscountCollection = aidList.ToArray();
            }

            //if (ProductCode == "PREMPAC")
            if (xmlNode.Attributes["IsInclusive"] != null && xmlNode.Attributes["IsInclusive"].Value == "Yes")
                isBonusPack = true;

            return true;//TODO: false on failure
        }

        decimal getAdminFee(XmlNode xmlNode)
        {
            decimal adminFeePercentage = 0.0m;
            if (xmlNode.Attributes["GrossCalcPercentage"] != null)
            { 
                string percentageStr = xmlNode.Attributes["GrossCalcPercentage"].Value;
                decimal.TryParse(percentageStr.Replace("%", ""), out adminFeePercentage);
            }
            return adminFeePercentage;
        }
    }
}