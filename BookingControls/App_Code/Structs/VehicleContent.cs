﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Xml;
using System.Text.RegularExpressions;

namespace THL.Booking
{
    /// <summary>
    /// Structure for holding vehicle content recieved from the central repository
    /// </summary>
    public class VehicleContent
    {
        public VehicleContent()
        {
            Description = string.Empty;//default?
        }

        /// <summary>
        /// Load content element from the vehicle central feed item
        /// </summary>
        /// <param name="xnode"></param>
        public VehicleContent(XmlNode xnode)
        {  
            Description = Regex.Replace(xnode.SelectSingleNode("desc").InnerText, "<.*?>", string.Empty);
            HasToilet = (xnode.SelectSingleNode("toilet").InnerText == "true");
            if (xnode.SelectSingleNode("wheelDrive") != null && !string.IsNullOrEmpty(xnode.SelectSingleNode("wheelDrive").InnerText))//TODO: normalize at source
                DriveMode =  xnode.SelectSingleNode("wheelDrive").InnerText.ToLower().Contains("4wd") ? "4wd" : "2wd";
            decimal length = 0.0m; 
            decimal.TryParse(xnode.SelectSingleNode("length").InnerText.ToLower().TrimEnd('m'), out length);
            Length = length;
            if (xnode.SelectSingleNode("shower") != null && !string.IsNullOrEmpty(xnode.SelectSingleNode("shower").InnerText))//TODO: normalize at source
                HasShower = xnode.SelectSingleNode("shower").InnerText.ToLower().Equals("true");
            int fridgeCap = 0;
            if (xnode.SelectSingleNode("fridgeCap") != null && !string.IsNullOrEmpty(xnode.SelectSingleNode("fridgeCap").InnerText))//TODO: normalize at source
                int.TryParse(xnode.SelectSingleNode("fridgeCap").InnerText, out fridgeCap);
            FridgeSize = fridgeCap;

            int sleeps = 0;
            if (xnode.Attributes["sleeps"] != null && !string.IsNullOrEmpty(xnode.Attributes["sleeps"].Value))//TODO: normalize at source
                int.TryParse(xnode.Attributes["sleeps"].Value, out sleeps);
            Sleeps = sleeps;
            
            if (xnode.SelectSingleNode("microwave") != null && !string.IsNullOrEmpty(xnode.SelectSingleNode("microwave").InnerText))//TODO: normalize at source
                HasMicrowave = xnode.SelectSingleNode("microwave").InnerText.ToLower().Contains("microwave");
            if (xnode.SelectSingleNode("dvd") != null && !string.IsNullOrEmpty(xnode.SelectSingleNode("dvd").InnerText))//TODO: normalize at source
                HasDVD = xnode.SelectSingleNode("dvd").InnerText.ToLower().Equals("true");
            if (xnode.SelectSingleNode("gasStove") != null && !string.IsNullOrEmpty(xnode.SelectSingleNode("gasStove").InnerText))//TODO: normalize at source
                HasGasStove = xnode.SelectSingleNode("gasStove").InnerText.Contains("gas");
            //if (xnode.SelectSingleNode("butan") != null && !string.IsNullOrEmpty(xnode.SelectSingleNode("butan").InnerText))//TODO: normalize at source
            //    HasButanStove = xnode.SelectSingleNode("butan").InnerText.ToLower().Equals("true");
            /*
            if (xnode.SelectSingleNode("transmission") != null && !string.IsNullOrEmpty(xnode.SelectSingleNode("transmission").InnerText)) {//TODO: normalize at source
                string transStr = xnode.SelectSingleNode("transmission").InnerText;
                transStr = transStr.ToLower().Contains("or") ? string.Empty : xnode.SelectSingleNode("transmission").InnerText;
            }
            */ 
            if (xnode.SelectSingleNode("popupUrl") != null && !string.IsNullOrEmpty(xnode.SelectSingleNode("popupUrl").InnerText))
                PopupUrl = xnode.SelectSingleNode("popupUrl").InnerText;
        } 

        /// <summary>
        /// Vehicle description HTML
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Has Toilet and Shower
        /// </summary>
        public bool HasToilet { get; set; }

        /// <summary>
        /// 2WD or 4WD
        /// </summary>
        public string DriveMode { get; set; }

        /// <summary>
        /// Vehicle Total Length
        /// </summary>
        public decimal Length { get; set; }

        
        public bool HasShower { get; set; }
        public int FridgeSize { get; set; }
        public bool HasMicrowave { get; set; }
        public bool HasDVD { get; set; }
        public bool HasGasStove { get; set; }
        public bool HasButanStove { get; set; }
        public int Sleeps { get; set; }
        public string Transmission { get; set; }
        public string PopupUrl { get; set; }
    }
}