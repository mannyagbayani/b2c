﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace THL.Booking
{
    /// <summary>
    /// Represents an Extra Hire Item Element selected by the customer
    /// </summary>
    public class ExtraHireItem : AuroraProduct
    {

        private int quantity;
        public int Quantity
        {
            get { return quantity;  }
            set { quantity = value; }
        }

        private decimal totalCharge;
        public decimal TotalCharge
        {
            get { return totalCharge;  }
            set { totalCharge = value; }
        }
        
        public ExtraHireItem(string _code, string _name, decimal _dailyRate, UOMType _uom, decimal _grossAmt, decimal _maxCharge, string _sapId, int _displayOrder, bool promptForQty, int _minAge, int _maxAge) : base(_code, _name, _dailyRate, _uom, _grossAmt, _maxCharge, _sapId, _displayOrder, promptForQty, _minAge, _maxAge)
        {
            productType = ProductType.ExtraHireItem;            
        }

        public ExtraHireItem(string _sapId)
        {
            sapId = _sapId;
            productType = ProductType.ExtraHireItem;
        }
    }
}