﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace THL.Booking
{
    /// <summary>
    /// FerryProduct Objetc Inherits AuroraProduct
    /// </summary>
    public class FerryProduct : AuroraProduct
    {


        public int Quantity { get; set; }


        FerryCrossingData ferryCrossingData;
        public FerryCrossingData FerryData 
        {
            get { return ferryCrossingData; } 
        }
        
        public FerryProduct(string _code, string _name, decimal _dailyRate, UOMType _uom, decimal _grossAmt, decimal _maxCharge, string _sapId, int _displayOrder, bool promptForQty, int _minAge, int _maxAge)
            : base(_code, _name, _dailyRate, _uom, _grossAmt, _maxCharge, _sapId, _displayOrder, promptForQty, _minAge, _maxAge)
        {
            productType = ProductType.FerryCrossing;            
        }

        public FerryProduct(string _code, string _name, FerryCrossingData _ferryCrossingData)
        {
            base.code = _code;
            base.name = _name;
            ferryCrossingData = _ferryCrossingData;
        }

        public FerryProduct(string _sapId)
        {
            sapId = _sapId;
            productType = ProductType.FerryCrossing;
        }
    }
}