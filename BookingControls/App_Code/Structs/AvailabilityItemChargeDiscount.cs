﻿using System;
using System.Xml;


namespace THL.Booking
{
    /// <summary>
    /// Discount Item for Item Charge Record
    /// </summary>
    public struct AvailabilityItemChargeDiscount
    {

        public string CodCode;

        public string CodDesc;

        public decimal DisPercentage;

        public bool IsPercentage;
               
        public bool LoadFromXML(XmlNode xmlNode)
        {
            bool success = true;
            decimal disPercent = 0;
            //try {
            CodCode = xmlNode.Attributes["CodCode"].Value;
            CodDesc = xmlNode.Attributes["CodDesc"].Value;
            IsPercentage = (xmlNode.Attributes["IsPercentage"] != null && xmlNode.Attributes["IsPercentage"].Value == "Yes");            
            if (xmlNode.Attributes["Discount"] != null)
            {
                success = decimal.TryParse(xmlNode.Attributes["Discount"].Value, out disPercent);
                DisPercentage = disPercent; 
            }//catch () { log/set success to false here
            return true;
        }
    }
}