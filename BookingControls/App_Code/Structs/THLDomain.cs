﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace THL.Booking
{
    /// <summary>
    /// Booking Domain Representation
    /// </summary>
    public class THLDomain
    {

        THLBrands _brand;
        public THLBrands Brand
        {
            get {return _brand; }        
        }

        public char BrandChar
        {
            get { return THLBrand.GetBrandChar(_brand); }
        }
        
        bool _isAggregator;
        public bool IsAggregator
        {
            get { return _isAggregator; }
        }

        string _name;
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// The B2C Rendering Engine
        /// </summary>
        public decimal EngineVersion { get; set; }

        public string ParentSite
        {
            get {
                string parent = _name;
                try
                {
                    parent = "http://www" + _name.Substring(_name.IndexOf('.'));
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    THLDebug.LogError(ErrorTypes.Application, "THLDomain.ParentSite", msg, "{'_name':'" + _name +"'}");
                } 
                return parent ;
            }         
        }

        CountryCode _country;
        public CountryCode Country
        {
            get { return _country; }
            set { _country = value; }
        }

        string _agentCode;
        public string AgentCode
        {
            get { return _agentCode;  }
        }


        string _uaCode;
        public string UACode
        { 
            get { return _uaCode; } 
        }

        string _openTag;
        public string OpenTag
        {
            get { return _openTag; }
        }

        public THLDomain(string name, THLBrands brand, CountryCode country, string agentCode, bool isAggregator, string uaCode)
        {
            _name = name;
            _brand = brand;
            _country = (isAggregator ? CountryCode.NONE : country);
            _agentCode = agentCode;
            _isAggregator = isAggregator;
            _uaCode = uaCode;
            _openTag = string.Empty;
        }

        /// <summary>
        /// Overload THLDomain method to include openTag.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="brand"></param>
        /// <param name="country"></param>
        /// <param name="agentCode"></param>
        /// <param name="isAggregator"></param>
        /// <param name="uaCode"></param>
        /// <param name="openTag"></param>
        public THLDomain(string name, THLBrands brand, CountryCode country, string agentCode, bool isAggregator, string uaCode, string openTag)
        {
            _name  = name;
            _brand = brand;
            _country = (isAggregator ? CountryCode.NONE : country);
            _agentCode = agentCode;
            _isAggregator = isAggregator;
            _uaCode = uaCode;
            _openTag = openTag;
        }

        public override string ToString()
        {
            return "<domain><name>"+ _name +"</name><brand>"+ _brand +"</brand><country>"+ _country +"</country><agentcode>"+ _agentCode +"</agentcode><isaggregator>"+ _isAggregator +"</isaggregator></domain>";        
        }

        public string ToJSON()
        {
            return "{\"name\":\"" + _name + "\",\"brand\":\"" + BrandChar + "\",\"ctry\":\"" + _country + "\",\"agg\":" + _isAggregator.ToString().ToLower() + ",\"eng\":" + EngineVersion + "}";
        }

    }
}