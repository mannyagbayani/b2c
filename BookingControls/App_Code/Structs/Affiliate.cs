﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace THL.Booking
{

    /// <summary>
    /// Define a structure for affeliate salable products 
    /// </summary>
    public struct AffelieteBrandDef 
    {
        public THLBrands Brand;
        public CountryCode CountryCode;
        public VehicleType VehicleType;       
    }
    
    /// <summary>
    /// THL Affiliate Structure
    /// </summary>
    public class Affiliate
    {
        string _code;
        AffelieteBrandDef[] _brandDefinitions;
        
        public string Code
        {
            get { return _code; }
        }
       
        public Affiliate(XmlNode affeliateNode)
        { 
            List<AffelieteBrandDef> affBrandList = new List<AffelieteBrandDef>();
            _code = affeliateNode.Attributes["code"].Value;
            XmlNodeList brandNodes = affeliateNode.SelectNodes("country/brand");
            foreach (XmlNode brandNode in brandNodes)  
            {
                AffelieteBrandDef currentAffBrand = new AffelieteBrandDef();
                string brandStr = brandNode.Attributes["code"].Value;
                string vehicleStr = brandNode.Attributes["disp"].Value;
                THLBrands currentBrand = THLBrand.GetBrandForString(brandStr);
                string brandCountryStr = brandNode.ParentNode.Attributes["code"].Value;                
                currentAffBrand.Brand = currentBrand;
                currentAffBrand.CountryCode = AvailabilityHelper.GetCountryCodeForString(brandCountryStr);
                currentAffBrand.VehicleType = AvailabilityHelper.GetVehicleTypeForAuroraString(vehicleStr);
                affBrandList.Add(currentAffBrand);
            }
            _brandDefinitions = affBrandList.ToArray();
        }

        public VehicleType GetVehicleTypeForCountryBrand(CountryCode countryCode, THLBrands brand)
        {
            foreach (AffelieteBrandDef brandDef in _brandDefinitions)
            {
                if (brandDef.Brand == brand && brandDef.CountryCode == countryCode)
                {
                    return brandDef.VehicleType;
                }
            }
            return VehicleType.Both;            
        }
    }
}