﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Preloader.aspx.cs" Inherits="Preloader"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" >
    <title>Retrieving Your Quote</title>    
    <meta http-Equiv="Cache-Control" Content="no-cache">
    <meta http-Equiv="Pragma" Content="no-cache">
    <meta http-Equiv="Expires" Content="0">
    <link href="<%= AssetsURL %>/css/Base.css?v=1.5" type="text/css" rel="stylesheet" />     
    <link href="<%= AssetsURL %>/css/<%= brandStr %>/selection.css?v=1.8" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery-1.3.1.min.js"></script>    
    <script src="js/ui.core.js" type="text/javascript"></script>
    <script src="js/ui.carousel.js" type="text/javascript"></script>
</head>
<body>    
    <form id="form1" runat="server">
        <div style="display:none;">
            <%= Status %>
        </div>
        <div id="vehicleConfigureContainer" class="preloader">       
            <div id="catalogHeader">
                <span class="Image">
                    <img src="<%= AssetsURL %>/images/<%= brandStr %>/logo.jpg" alt="logo" />
                </span>
                <a class="priceMatch" href="javascript:openPriceMatchWindow();">Price match guarantee</a>
                <span class="ContactInfo">
                    <%= ContactHeaderText %> or <a href="<%= ParentSiteURL %>/contactus" target="_blank">contact us</a>
                </span>
            </div>
            <div id="breadCrumbs">
                <span class="Crumbs">
                </span>
            </div>
            <div id="vehicleCatalogContainer">           
                <h2>Alternative Availability</h2>
                <div id="configurePanel">
                    <div class="progressIndicator">
                        <span>Loading Alternative Availability</span>
                        <br />
                        <span>Thank you for your patience</span>
                        <br />
                        <br />
                        <ul id="carousel">
                            <li><img src="<%= AdsPath %>1.png" alt="" /></li>
                            <li><img src="<%= AdsPath %>2.png" alt="" /></li>
                            <li><img src="<%= AdsPath %>3.png" alt="" /></li>
                            <li><img src="<%= AdsPath %>4.png" alt="" /></li>                            
                        </ul>   
                    </div>
                </div>
            </div>
            <div id="disclaimer">
                <span>Please note: Rental prices quoted above are only valid at the time of booking.</span>
            </div>
        </div>    
    </form>
    <script type="text/javascript">
        //process id
        var sid = '<%= Guid.NewGuid().ToString() %>';
        var loadAttempts = 30; //define number of trys
        //var providedParams = "<%= PassedParams %>";
        var progressStarted = false;
        var warmUpPeriod = 2000;
        var pingFreq = 3000;
        var pingServiceUrl = "dispatcher/loadprogress.asmx/LoadSlowFunction";
        var url = pingServiceUrl + '?sid=' + sid + '&<%= PingServiceParam %>';
        var toutUrl = 'selection.aspx?<%= RequestParams %>';
        var redirectURL = "AltAvailability.aspx?sid=" + sid + "&<%= PassedParams %>";
        function getStatus() {
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'text',
                success: function (data) {
                    if (data.indexOf("initialised") > -1) {
                        progressStarted = true;
                        $('#status').html("Initialised");
                        $('#statusFill').width(data);
                        window.setTimeout("getStatus()", warmUpPeriod);
                    }
                    if (data.indexOf("pending") > -1 && --loadAttempts > 0) {
                        $('#status').html(data);
                        $('#statusFill').width(data);
                        window.setTimeout("getStatus()", pingFreq);
                    }
                    else if (loadAttempts == 0)  {
                        $('#carousel').html("<div>Sorry this request has timed out please retry <a href='" + toutUrl + "'>here</a>.</div>");
                    }
                    if (data.indexOf("failed") > -1) {
                        //TODO: redirect user to selection page;
                        window.top.location = toutUrl;
                    }
                    if (data.indexOf("timedout") > -1) {
                        //TODO: action when time out
                        $('#carousel').html("<div>Sorry this request has timed out please retry <a href='"+ toutUrl+ "'>here</a>.</div>");
                    }
                    else if (data.indexOf("success") > -1) {
                        window.top.location = redirectURL;
                    };
                }
            });
        }
        $(document).ready(function(event) {
           getStatus();
        });    

        window.setTimeout(function() {
            $("#carousel").carousel({
                animateByMouse: false,
                pausable: true,
                radius: 250,
                animate:0.004,
                pauseSpeed:0
            });
        }, $.browser.safari ? 100 : 0); 
     </script>    
</body>
</html>