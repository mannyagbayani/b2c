﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CrossSellHeader.ascx.cs" Inherits="THL.Booking.CrossSellHeader" %>
<!-- Start CrossSell Header, TODO: swap to Web Control -->	            
<div id="crossSell">     			
    <ul class="crossSellNav">            					
        <li><span class="logo" id="Span1"><%= CurrentCountry %></span></li>
        <li class="maui"><a href="http://www.maui.<%= CrossSuffix %>" <%= IsCurrentBrand("m") ? "class='current'" : "" %>>Maui</a></li>
        <li class="britz"><a href="http://www.britz.<%= CrossSuffix %>" <%= IsCurrentBrand("b") ? "class='current'" : "" %>>Britz</a></li>
        <li class="BP"><a href="http://www.backpackercampervans.<%= CrossSuffix %>" <%= IsCurrentBrand("p") ? "class='current'"  : "" %>>Backpacker</a></li>						            
        <li class="EX"><a href="http://www.exploremore.<%= CrossSuffix %>" <%= IsCurrentBrand("p") ? "class='current'"  : "" %>>Explore More</a></li>						            
    </ul>
    <div id="crossSellCountry">
        <ul class="crossSellNav">
            <li>
	            <span class="country"><%= ReverseCountryName %> </span>
	            <ul class="crossSellNavDD">	                
		            <li class="maui"><a href="http://www.maui.<%= ReverseSuffix %>">Maui</a></li>
		            <li class="britz"><a href="http://www.britz.<%= ReverseSuffix %>">Britz</a></li>
		            <li class="BP"><a href="http://www.backpackercampervans.<%= ReverseSuffix %>">Backpacker</a></li>
		            <li class="EX"><a href="http://www.exploremore.<%= ReverseSuffix %>">Explore More</a></li>
	            </ul>
            </li>
        </ul>
        <ul class="crossSellNav">
				<li>
					<span class="country">USA</span>
					<ul class="crossSellNavDD">
						<li class="RBC"><a href="http://www.roadbearrv.com">Road Bear RV</a></li>

					</ul>
				</li>
		</ul>
    </div>            	
</div>
<!-- End CrossSell Header -->
