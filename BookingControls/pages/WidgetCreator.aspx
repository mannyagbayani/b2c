﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WidgetCreator.aspx.cs" Inherits="pages_WidgetCreator" %>
<!DOCTYPE html>
<html>
<head>
    <title>THL Widget Creator</title>
    <link href="/css/pagewidget.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/js/json2.js"></script>
    <script type="text/javascript" src="/js/zclip.js"></script>
    <script type="text/javascript" src="/js/genericwidget.js"></script>    
    <script type="text/javascript">
        var env = "<%= System.Web.Configuration.WebConfigurationManager.AppSettings["WidgetEnv"] %>";
        $(document).ready(function () { initWidgetCreator(env); });               
    </script>
    <link href="/css/pagewidget.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" >
        <div id="container">
 	        <div class="content">    
                <!-- Logo -->
                <div class="thllogo"><img src="/images/thllogo.png" width="191" height="137" alt="Tourism Holdings Limited" /></div>
                <!-- start Content -->
    	            <!-- Title -->
                    <div class="title">Booking Widget Creator</div>
                        <div id="leftPanel">
                            <div id="configureForm">
                                <p>
                                    <label for="countrySelector">COUNTRY</label>
                                    <span>Which country would you like the widget to search</span>
                                    <select id="countrySelector">
                                        <option value="AU">Australia</option>
                                        <option value="NZ">New Zealand</option>
                                        <option value="">Both</option>                                       
                                    </select>
                                </p>
                                <p>
                                    <label for="brandSelector">Brand</label>
                                    <span>Which <strong>thl</strong> Rentals brand would you like the widget to search</span>
                                    <select id="brandSelector">
                                        <option value="">All</option>
                                        <option value="y">Mighty</option>
                                        <option value="m">Maui</option>
                                        <option value="q">Kea</option>
                                        <option value="u">United</option>
                                        <option value="a">Alpha</option>
                                        <option value="e">Econo</option>
                                        <option value="b" selected="selected">Britz</option>                    
                                    </select>
                                </p>
                                <p>
                                    <label for="vehicleSelector">Campervan or Car Hire</label>
                                    <span>Would you like the widget to enable customers to search cars, campers or both</span>
                                    <select id="vehicleSelector">
                                        <option value="campers">Campervans</option>
                                        <option value="cars">Cars</option>
                                        <option value="both">Campervans and Cars</option>
                                    </select>
                                </p> 
                                <p> 
                                    <label for="affId">Your Affiliate Id</label>
                                    <span>Please enter the Agent ID your <strong>thl</strong> sales representative has provided to ensure sales originating from your site are tracked correctly</span>
                                    <input type="text" id="affId" value="<%= AgentCode %>"/>
                                </p>
                                <p> 
                                    <label for="promoCode">Promo Code</label>
                                    <span>Please enter the promo code </span>
                                    <input type="text" id="promoCode" value=""/>
                                </p>
                                <p> 
                                    <label for="campaignId">Your Google Campaign Code</label>
                                    <span>Please enter GA Campaign Code</span>
                                    <input type="text" id="utm_campaign" value=""/>
                                    <br />
                                    <br />
                                    <input type="checkbox" id="RemoveCrossLink" style="width:10px;float:left;" disabled checked/>
                                    <Label style="width:150px;float:left;padding-left:10px;">Remove Crosslink </Label>
                                    
                                </p>  
                                <p class="noborder">
                                    <a id="testCode" class="widgetbtn" href="#">Update Widget</a>
                                    <a id="copyToCB" class="widgetbtn" href="#">Copy to clipboard</a> 
                                </p>        
                            </div>
                            <div id="controlCodeArea" style="">       
                                <textarea id="widgetSnippet" cols="50" rows="12" onclick="this.select()">  
<div id="bookingWidget"></div>
<script type="text/javascript">
    //widget starts
function loadwgt() {
    var options = {
    "brand": "b",
    "vehicleType": "campers",
    "country": "AU",
    "serviceUrl": "<%= WidgetBase %>",
    "cssBase": "<%= cssPath %>",
    "expandUI": true,
    "months": 1,
    "startExpanded": true,
    "horizontal": false,
    "width": 240,
    "height": 500,
    "affId": "<%= AgentCode %>"
    };//options
    initTHLWidget(options);
}
var head= document.getElementsByTagName('head')[0];
   var script= document.createElement('script');
   script.type= 'text/javascript';
   script.onreadystatechange= function () {
       if (this.readyState == 'complete' || this.readyState == 'loaded'/*IE8*/) loadwgt();
   }
   script.onload= loadwgt;
   script.src = '<%= WidgetBase %>/js/wdgt.js';
   head.appendChild(script);
//widget ends
</script>    
                            </textarea>        	  
                        </div>
                    </div>
                    <div id="rightPanel">
                        <div id="bookingWidget"></div>
                    </div>   
                <!-- /END content -->
            </div> 
        </div>     
    </form>
</body>
</html>