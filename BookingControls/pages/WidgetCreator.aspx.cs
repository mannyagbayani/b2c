﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_WidgetCreator : System.Web.UI.Page
{

    public string WidgetBase { get; set; }
    public string AgentCode { get; set; }
    public string cssPath { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        WidgetBase = System.Web.Configuration.WebConfigurationManager.AppSettings["WidgetBase"];
        AgentCode = !string.IsNullOrEmpty(Request.Params["ac"]) ? Request.Params["ac"] : string.Empty;
        cssPath = WidgetBase + "/css/GenericWidget.css";
        if (!string.IsNullOrEmpty(Request.Params["cssPath"]))
            cssPath = Request.Params["cssPath"];
    }
}