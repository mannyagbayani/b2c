﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using THL.Booking;

public partial class pages_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        
        
        NameValueCollection passedParamsCollection = new NameValueCollection();
        string passedParams = (Request.RawUrl.Contains("?") ? "?" + Request.RawUrl.Split('?')[1] : string.Empty);
        foreach (string key in Request.Params.Keys)
        {
            switch (key)
            {
                case "vh":
                    passedParamsCollection.Add("vh", AvailabilityHelper.ConvertBPAEToAuroraVehicleCode(Request.Params[key]));
                    break;
                case "cc"://cc=AU
                    passedParamsCollection.Add("cc", Request.Params[key].ToUpper());
                    break;
                case "vc"://vc=bz
                    passedParamsCollection.Add("brand", THLBrand.GetBrandCharForBPAECode(Request.Params[key]));
                    break;
                case "ac": //ac = , ch=
                    passedParamsCollection.Add("ac", Request.Params[key]);
                    break;
                case "ch": //&sc=rv, vt=rv
                    if (passedParamsCollection["ac"] != null) continue;//give ac higher priority
                    passedParamsCollection.Add("ac", Request.Params[key]);
                    break;
                case "sc":
                    passedParamsCollection.Add("sc", (Request.Params[key] == "rv" ? "rv" : "car"));
                    passedParamsCollection.Add("vtype", (Request.Params[key] == "rv" ? "rv" : "car"));
                    break;
                case "na"://na=1
                    passedParamsCollection.Add("na", Request.Params[key]);
                    break;
                case "nc"://nc=1
                    passedParamsCollection.Add("nc", Request.Params[key]);
                    break;
                case "cr"://cr=FR
                    passedParamsCollection.Add("cr", Request.Params[key]);
                    break;
                case "pd":
                    passedParamsCollection.Add("pd", Request.Params[key]);
                    break;
                case "pm":
                    passedParamsCollection.Add("pm", Request.Params[key]);
                    break;
                case "py":
                    passedParamsCollection.Add("py", Request.Params[key]);
                    break;
                case "dd":
                    passedParamsCollection.Add("dd", Request.Params[key]);
                    break;
                case "dm":
                    passedParamsCollection.Add("dm", Request.Params[key]);
                    break;
                case "dy":
                    passedParamsCollection.Add("dy", Request.Params[key]);
                    break;
                case "dt":
                    passedParamsCollection.Add("dt", Request.Params[key]);
                    break;
                case "pt":
                    passedParamsCollection.Add("pt", Request.Params[key]);
                    break;
                case "pb":
                    passedParamsCollection.Add("pb", AvailabilityHelper.ConvertBPAEToAuroraLocationCode(Request.Params[key]));
                    break;
                case "db":
                    passedParamsCollection.Add("db", AvailabilityHelper.ConvertBPAEToAuroraLocationCode(Request.Params[key]));
                    break;

                case "rf":
                    SessionManager.AgentReference = Request.Params[key];
                    break;           
            }
        }

        THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
        string HTTPPrefix = ApplicationManager.HTTPPrefix;

        string baseSelectionURL = HTTPPrefix + appDomain.Name + "/Selection.aspx";
        //passedParamsCollection.Add("url", baseSelectionURL);
        string queryParams = AvailabilityHelper.ConstructQueryString(passedParamsCollection);

        Response.Redirect(baseSelectionURL + "?" + queryParams + "&pv=1.0");
        //?cc=AU&vc=&ac=&sc=rv&pc=&ch=&rf=&na=1&nc=0&cr=AU&pb=ADL-AIR&pd=30&pm=10&py=2009&pt=09:00&db=ADL-AIR&dd=18&dm=11&dy=2009&dt=15:00&vtype=rv&vh=PFMRS&pv=1.0&brand=b
    
    }
}
