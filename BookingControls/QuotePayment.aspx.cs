﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class QuotePayment : System.Web.UI.Page
{
    public string ContentURL, AssetsURL;
    public string CurrencyStr, VehicleImagePath;
    public string CurrencySelector;
    //public string PickUpLocationStr, DropOffLocationStr, PickUpDateStr, DropOffDateStr;
    public string PickUpLocationStr, DropOffLocationStr, PickUpDateStr, DropOffDateStr, PickUpDayMonthStr;
    public string VehicleCode;
    public string RentalSummary;
    public string VehicleChargeJSON;
    public string PaymentTotalPanel;
    public string CCDropDown;
    public string CurrentSurcharge;
    public string SurchargeClass;
    public decimal SecurityDeposit, Surcharge, PickUpDue, TotalDueToday;

    public string TandCLink;
    public decimal AdminFee;

    public string AVPID;
    public string AnalyticsCode, ParentSiteURL, BookingId;

    public Vehicle SelectedVehicle;

    public string VehicleImageConvension;

    public string BrandTitle;

    public string HTTPPrefix;

    public string ContactHeaderText;

    public string MaCDeposit, Lead45, OnRequest; //MaC UIs

    public string SSLSeal;

    ApplicationData appData;

    public string debugStr, debugURL, debugHTML;

    public string SiteInfo;
    public bool AddRWCMsg;
    public int EarlyEventPayment;
    public string LoyaltyCardHTML = string.Empty;//Added for loyalty card support 21/06/10
    public Customer QuoteCustomer;
    public decimal DepositPercentage;
    public string SurStr;

    THLDomain appDomain;
    AuroraQuote quote;
    AvailabilityRequest aReq;

    public string BrandChar { get; set; }
    public string CSSLink { get; set; }
    public string RemarketingCode { get; set; }
    public string OpenTag { get; set; }
    /// <summary>
    /// Marin Tracking Code
    /// </summary>
    public string DNAIMGTag { get; set; }

    public bool PayFullForCar { get; set; }
    //Added to support united/Key advance payment
    public string FinalPayText
    {
        get
        {
            int unitedLead = 28, keaLead = 35;
            string msg = "Final balance ";
            try
            {
                //DateTime pickUpDay = aReq.PickUpDate;
                //int leadTime = (DateTime.Today - pickUpDay).Days;                
                switch (appDomain.Brand)
                {
                    case THLBrands.United:
                    case THLBrands.Alpha:
                    case THLBrands.Econo:
                        if (/*leadTime > unitedLead*/quote.RequiredDeposit == quote.VehicleCharge)
                            msg += string.Empty;
                        else
                            msg += "(Full payment due " + unitedLead + " days before pick-up)";
                        break;
                    case THLBrands.Kea:
                        if (/*leadTime > keaLead*/quote.RequiredDepositPer.Equals(100))
                            msg += string.Empty;
                        else
                            msg += "(Full payment due " + keaLead + " days before pick-up)";
                        break;
                    default:
                        msg += string.Empty;
                        break;
                }
            }
            catch (Exception ex)//default to pickup, or define
            {
                THLDebug.LogError(ErrorTypes.Presentation, "Payment.FinalPayText", "failed to show pickup payment", ex.Message);
            }
            return msg;
        }
    }

    /// <summary>
    /// Support release version from config for forcing static assets cache refresh.
    /// </summary>
    public string Version
    {
        get { return ApplicationManager.ReleaseVersion; }
        set { value = ApplicationManager.ReleaseVersion; }
    }

    public string ContactUsURL
    {
        get
        {
            THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
            string contactUrl = appDomain.ParentSite + "/contactus";
            switch (appDomain.ParentSite)
            {
                case "keacampers.co.nz":
                    contactUrl = "http://nzrentals.keacampers.com/en/contact/contact_us.aspx";
                    break;
                case "keacampers.com.au":
                    contactUrl = "http://nzrentals.keacampers.com/en/contact/contact_us.aspx";
                    break;
            }
           
            return contactUrl;
        }
    }

    public string Currency
    {
        //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
        //get
        //{
        //    THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
        //    return CurrencyManager.GetCurrencyForCountryCode(appDomain.Country.ToString()).ToString();
        //}
        get;
        set;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //try
        //{  
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);

        bool secureMode = false;//TODO: move to config manager
        bool.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["SecureMode"], out secureMode);

        HTTPPrefix = ApplicationManager.HTTPPrefix;   

        appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
        if (appDomain == null)//dubug line
            appDomain = new THLDomain("secure.maui.co.nz", THLBrands.Maui, CountryCode.NZ, "B2CNZ", false, "uahere");//dubug line
        ParentSiteURL = appDomain.ParentSite;

        //DecideDNA Tracking Img
        TrackingDisplayer trd = new TrackingDisplayer(appDomain, secureMode);
                
        //DNAIMGTag = trd.RenderDNATrackingImg("payment");
        DNAIMGTag += trd.RenderMarinTrackingPixelIMG(SessionManager.MarinTrackingParams, MarinStep.SELECTION, 0.0m, string.Empty, string.Empty);

        SSLSeal = trd.GetSealID();        
        //Analytics Tracking Code
        AnalyticsCode = trd.RenderAnalsCode();
        RemarketingCode = trd.RenderRemarketingSnippets();

        BrandChar = appDomain.BrandChar.ToString(); //(Request.Params["brand"] != null ? Request.Params["brand"] : "m");//TODO: this should be swapped by baseURL. 

        //Added by nimesh below on 25/07/2014 by nimesh as it was not picking up agent code
        string agentCode = "mac";

        if (!string.IsNullOrEmpty(SessionManager.AgentCode))
        {
            string acCode = SessionManager.AgentCode.ToLower();
            BrandChar = /*Request.Params["ac"].ToLower();//*/ AgentTheme.CSSPath(acCode, BrandChar);
        }
        // end Added by nimesh on 25/07/2014
        ContentURL = HTTPPrefix + appDomain.Name + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["BookingControlContentPath"];
      
        string countryCode = appDomain.Country.ToString().ToUpper();  //(Request.Params["cc"] != null ? Request.Params["cc"] : "NZ").ToUpper();   
                   
        AssetsURL = HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];
        SessionData sessionData = SessionManager.GetSessionData();
        appData = ApplicationManager.GetApplicationData();

        if(!sessionData.HasBookingData)
            Response.Redirect("../Selection.aspx?msg=sessionExpired");

        if (sessionData.BookingData.Status == BookingStatus.BookingSuccessful) 
            Response.Redirect("../Selection.aspx?msg=bcomp");
        //if (sessionData != null && sessionData.HasRequestData && sessionData.HasResponseData)
        //{
        quote = SessionManager.CurrentQuote;
        VehicleImageConvension = quote.Request.CountryCode.ToString().ToLower() + AvailabilityHelper.GetAuroraVehicleStrForVehicleType(quote.Request.VehicleType) + appDomain.BrandChar.ToString() + "." + quote.Request.VehicleModel;
        SecurityDeposit = quote.GetRequiredDeposit();

        // added by nimesh to hide pay total amount today for car
        PayFullForCar = quote.Request.VehicleType.Equals(VehicleType.Car) ? true : false;
        // end added by nimesh to hide pay total amount today for car

        Surcharge = appDomain.Brand.Equals(THLBrands.Kea) ? 0.0m : SecurityDeposit * 0.02m;//TODO: move to read of the back for kea biz logic
        
        if (appDomain.Brand.Equals(THLBrands.United) || appDomain.Brand.Equals(THLBrands.Alpha) || appDomain.Brand.Equals(THLBrands.Econo))
            Surcharge = 0.0m;//support united, TODO: get from the back.

        SurStr = (appDomain.Brand.Equals(THLBrands.Kea) || appDomain.Brand.Equals(THLBrands.United) || appDomain.Brand.Equals(THLBrands.Alpha) || appDomain.Brand.Equals(THLBrands.Econo)) ? "0.0" : "2.0";

        if (quote.Request.CountryCode.Equals(CountryCode.AU) && quote.Request.VehicleType.Equals(VehicleType.Car))
        {
            Surcharge = 0.0m;//cars au fix
            SurStr = "0.0";
        }
        TotalDueToday = SecurityDeposit + Surcharge;
        PickUpDue = quote.CurrentTotal() - SecurityDeposit;//CurrentTotal()

        aReq = quote.Request;
        THLBrands brand = THLBrand.GetBrandForString(aReq.Brand);
     
        QuoteCustomer = quote.GetCustomerData();
        SelectedVehicle = new Vehicle(quote.VehicleName, VehicleType.Campervan, VehicleCode, 5, "NZ");

        BookingId = quote.BookingID;

        /*Added by Nimesh on 19th Dec 2014 to get available slots*/
        DataAccess da = new DataAccess();
        //da.GetAvailableSlots(AVPID);
        SlotsPanel = da.GetAvailableSlots(string.Empty, BookingId);
        //if (SlotsPanel.Contains("altavailability.aspx"))
        //if (SlotsPanel.Contains("Alternate Availability"))
        if (SlotsPanel == null)
        {
            DisableAll = true;
            string param = string.Empty;// Request.Params;
            string vCode = sessionData.BookingData.VehicleCode;
            //SlotsPanel
            //SlotsPanel.Replace("params", "params=" + param);
        }
        /*end Added by Nimesh on 19th Dec 2014 to get available slots*/
        SiteInfo = "{isAggregator:" + appDomain.IsAggregator.ToString().ToLower() + ", cot:'" + aReq.CountryCode + "'}";//js site level info
                  
        CurrencyManager curMan = new CurrencyManager(CurrencyConversionProvider.BNZ, CurrencyManager.GetCurrencyForCountryCode(appDomain.Country));//from Application Cache                
        CurrencyCode defaultCurrency = (aReq.CountryCode == CountryCode.NZ ? CurrencyCode.NZD : CurrencyCode.AUD);

        if (appDomain.IsAggregator)
            curMan = new CurrencyManager(CurrencyConversionProvider.BNZ, defaultCurrency);
        
        BrandTitle = THLBrand.GetNameForBrand(THLBrand.GetBrandForString(aReq.Brand));

        BrandChar = aReq.Brand;
        CurrencyStr = (aReq.CountryCode == CountryCode.NZ ? "NZD" : "AUD"); //TODO: get from originating site

        //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
        Currency = CurrencyStr;

        VehicleImagePath = AvailabilityHelper.GetVehicleImageBasePath(aReq.CountryCode, THLBrand.GetBrandForString(aReq.Brand), aReq.VehicleType, aReq.VehicleModel, AssetsURL, false);       

        //        bool hasSurcharge = sessionData.HasSurecharge();
        decimal currentSurcharge = (quote.AvailableCards != null && quote.AvailableCards.Length > 0 ? (quote.AvailableCards[0].Surcharge / 100) : 0.0m);
                CurrentSurcharge = currentSurcharge.ToString();                        

        //        //Populate Travel Details
        PickUpLocationStr = quote.PickUpLoc; //appData.GetLocationStringForCtrlCode(aReq.PickUpBranch); 
        DropOffLocationStr = quote.DropOffLoc; //appData.GetLocationStringForCtrlCode(aReq.DropOffBranch); 
        PickUpDateStr = aReq.PickUpDate.ToString("dddd, dd MMMM yyyy HH:mm tt");
        DropOffDateStr = aReq.DropOffDate.ToString("dddd, dd MMMM yyyy HH:mm tt");
        PickUpDayMonthStr = aReq.PickUpDate.ToString("dd MMM");
        VehicleCode =  aReq.VehicleModel;
        DepositPercentage = quote.RequiredDepositPer;

        TandCLink = !String.IsNullOrEmpty(quote.TaCLink) ? quote.TaCLink : string.Empty ;//define default is missing?

        if(quote.AvailableCards != null)
            CCDropDown = new UserDataDisplayer().RenderCCDropDown(quote.AvailableCards, BrandChar);
        //TODO: manage no cards exception

        ContactHeaderText = ContentManager.GetMessageForCode(THLBrand.GetBrandForString(BrandChar), appDomain.Country, "ContactText").Content + "<br />"
            + ContentManager.GetMessageForCode(THLBrand.GetBrandForString(BrandChar), appDomain.Country, "DomesticPhone").Content + " - "
            + ContentManager.GetMessageForCode(THLBrand.GetBrandForString(BrandChar), appDomain.Country, "InternationalPhone").Content;
                
        //Populate Rental UI

        QuoteDisplayer qd = new QuoteDisplayer(quote);            

        RentalSummary =   qd.RenderChargeTable();// ard.RenderRentalSummary(bookingInfo, bookingInfo.AVPID, currentSurcharge, hasSurcharge);
        PaymentTotalPanel = qd.RenderPaymentTotalPanel(Surcharge);// ard.RenderPaymentTotalsPanel(bookingInfo, currentSurcharge);
        VehicleChargeJSON = "{vCode: '" + quote.Request.VehicleModel + "',vName: '" + quote.Request.VehicleName + "',hirePeriod: " + quote.Request.GetHirePeriod() + ",vehicleTotal: " + quote.VehicleCharge + ",depositPercentage : "+ quote.RequiredDepositPer + ",depositAmount : " + quote.GetRequiredDeposit() + ",adminFee : 3.00,calculatedTotal : " + quote.CurrentTotal()  + "}";
        
        //-------------------------------- Debug Zone -------------------------
        debugURL = "unittests/debugStr.aspx?ts=" + DateTime.Now.Millisecond;
        debugHTML = (Request.Params["debugOn"] != null && Request.Params["debugOn"] == "true" ? "<div id='debugSection' style='float: left; margin-top: 20px; width:757px; height: 800px; background-color: #fff;'><div id='debugValue'>" + debugStr + "</div><iframe src='" + debugURL + "' width='757px' height='800px' /></div>" : string.Empty);

        OpenTag = AvailabilityHelper.LoadOpenTag(appDomain);       
        //debugHTML = "<div style='float:left;clear:both;'>" + quote.SerializeQuote().OuterXml + "</div>";

    //}
    //catch (Exception ex)
    //{
    //    string Error = ex.Message;
    //    THLDebug.LogError(ErrorTypes.Application, "Page_Load.Payment", Error, string.Empty);
    //    Response.Redirect("Selection.aspx?msg=sessionExpired");
    //}
                
    }

    public bool DisableAll { get; set; }

    public string SlotsPanel { get; set; }
}
