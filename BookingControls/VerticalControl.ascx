﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VerticalControl.ascx.cs" Inherits="THL.Booking.VerticalControl" %>
<div id="bookingControlContainer" class="<%= SplashMode ? "Splash" : "" %>" >    
    <link rel="stylesheet" type="text/css" media="all" href="<%= ContentURL %>/css/<%= Brand %>/vertical.css" />
    <link rel="stylesheet" type="text/css" media="all" href="<%= ContentURL %>/css/calendar/calendar.css" />
    <script type="text/javascript">
            var cr = null;
            var vt = null;
            var dxr = null;
            var ContentbaseUrl = '<%= ContentURL %>';
            var CallBackURL = '<%= AjaxResourceURL %>';
            var TargetUrl = '<%= Target %>';
            var MAX_PASSENGERS = 4;
            var pudate = 'dd-mm-yyyy', dod = 'dd-mm-yyyy';
            var noa = 0, noc = 0;
            var brandStr = null;
            var $j = jQuery.noConflict();
                        
            jQuery(document).ready(function() {
                dxr = $j('#bookingcontrol').position().left;
                vt = '<%= VehicleTypeStr %>';
                cot = '<%= CountryCode.ToUpper() %>';
                cr = '<%= CountryOfResidence.ToUpper() %>';
                brandStr = '<%= Brand %>'
                initControl();
                bindResize()                
            });
    </script>
    <div id="ctrlSearchBox_up1">
        <div class="SearchBox_Wrapper" id="bookingcontrol">
            <div class="SearchBox_Tabs">
                <ul id="vehicleTabs">
                    <li><a id="campersTab" class="<%= (VehicleTypeStr == "rv" || VehicleTypeStr == "av" ? "on" : "")  %>" href="javascript:swapTabs('rv')">
                    </a></li>
                    <li><a id="carsTab" class="<%= (VehicleTypeStr == "rv" || VehicleTypeStr == "av" ? "" : "on")  %>" href="javascript:swapTabs('ca')">
                    </a></li>
                </ul>
            </div>
            <div class="SearchBox_Content" id="ctrlSearchBox_divContent">
                <ul class="SearchBox_TableWrapper">
                    <li class="Title">
                        <p>
                            <span class="SearchBox_Title" id="ctrlSearchBox_lblTitle">
                                <%= PanelTitle %> hire</span>
                        </p>
                    </li>
                    <li class="Title">
                        <p>
                            <span class="SearchBox_Subtitle" id="ctrlSearchBox_lblSubtitle">Search rates &amp; book</span>
                            <hr class="SearchBox_Line" id="ctrlSearchBox_hrLine1" />
                        </p>
                    </li>
                    <li class="CountryOfTravel">
                        <p>
                            <label>Country of travel:</label>
                            <select class="SearchBox_FullListBox" id="ddCountryOfTravel" onchange="javascript:enablePanel(this);"
                                name="ddCountryOfTravel">
                                <option value="-1" selected="selected">Please select</option>
                                <option value="AU">Australia</option>
                                <option value="NZ">New Zealand</option>
                            </select>
                            <input type="hidden" id="countryOfTravel" value="<%= CountryCode.ToUpper() %>" />
                        </p>
                    </li>
                    <li class="VehicleModel">
                        <p class="<%= ( VehicleCode != "" && VehicleCode != null ? "On" : ""  ) %>"> 
                            <label>Vehicle model:</label>
                            <%= VehicleDD %>
                            <input type="hidden" id="vehicleType" value="<%= VehicleTypeStr %>" />
                            <input type="hidden" id="vehicleCode" value="<%= VehicleCode %>" />
                        </p>
                    </li>
                    <li>
                        <p>
                            <label>Number of travellers:</label>
                            <%= numAdultDD %>
                            <%= numChildrenDD %>
                            <input type="hidden" id="maxTravellers" value="<%= MaxTravellers %>" />
                        </p>
                    </li>
                    <li>
                        <p>
                            <label>
                                Pick up:</label>
                            <%= PickUpDD %>
                        </p>
                    </li>
                    <li>
                        <p>
                            <input <%= IsDisabled %> type="text" onfocus="this.select();" class="SearchBox_CalTextBox"
                                id="ctrlSearchBox_calPickUp_DateText" value="dd/mm/yyyy" name="ctrlSearchBox$calPickUp$DateText" />
                            <%= PickUpTimeDD %>
                        </p>
                    </li>
                    <li>
                        <p>
                            <label>
                                Drop off:</label>
                            <%= DropOfDD %>
                        </p>
                    </li>
                    <li>
                        <p>
                            <input <%= IsDisabled %> type="text" onfocus="this.select();" class="SearchBox_CalTextBox"
                                id="ctrlSearchBox_calDropOff_DateText" value="dd/mm/yyyy" name="ctrlSearchBox$calDropOff$DateText" />
                            <%= DropOffTimeDD %>
                        </p>
                    </li>
                    <li>
                        <p id="ctrlSearchBox_tblrCountryOfResidence">
                            <label class="SearchBoxLabel" id="ctrlSearchBox_lblCountryOfResidence">
                                Country of residence:</label>
                            <select <%= IsDisabled %> class="SearchBox_FullListBox" id="ctrlSearchBox_dropCountryOfResidence"
                                name="ctrlSearchBox$dropCountryOfResidence">
                                <option value="0" selected="selected">Please select</option>
                                <option value="AU">Australia</option>
                                <option value="CA">Canada</option>
                                <option value="DK">Denmark</option>
                                <option value="FR">France</option>
                                <option value="DE">Germany</option>
                                <option value="NL">Netherlands</option>
                                <option value="NZ">New Zealand</option>
                                <option value="SE">Sweden</option>
                                <option value="CH">Switzerland</option>
                                <option value="UK">United Kingdom</option>
                                <option value="US">United States Of America</option>
                                <option value="">--------------------</option>
                                <option value="AF">Afghanistan</option>
                                <option value="AL">Albania</option>
                                <option value="DZ">Algeria</option>
                                <option value="AS">American Samoa</option>
                                <option value="AD">Andora</option>
                                <option value="AO">Angola</option>
                                <option value="AI">Anguilla</option>
                                <option value="AQ">Antarctica</option>
                                <option value="AG">Antigua & Barbados</option>
                                <option value="AR">Argentina</option>
                                <option value="AM">Armenia</option>
                                <option value="AW">Aruba</option>
                                <option value="AT">Austria</option>
                                <option value="AZ">Azerbaijan</option>
                                <option value="BS">Bahamas</option>
                                <option value="BH">Bahrain</option>
                                <option value="BD">Bangladesh</option>
                                <option value="BB">Barbados</option>
                                <option value="BY">Belarus</option>
                                <option value="BE">Belgium</option>
                                <option value="BZ">Belize</option>
                                <option value="BJ">Benin</option>
                                <option value="BM">Bermuda</option>
                                <option value="BO">Bolivia</option>
                                <option value="BA">Bosnia and Herzegovina</option>
                                <option value="BW">Botswana</option>
                                <option value="BV">Bouvet Island</option>
                                <option value="BR">Brazil</option>
                                <option value="IO">British Indian Ocean Territory</option>
                                <option value="BN">Brunei</option>
                                <option value="BG">Bulgaria</option>
                                <option value="BF">Burkina Faso</option>
                                <option value="BI">Burundi</option>
                                <option value="BT">Butan</option>
                                <option value="KH">Cambodia</option>
                                <option value="CM">Cameroon</option>
                                <option value="CV">Cape Verde</option>
                                <option value="KY">Cayman Islands</option>
                                <option value="CF">Cental African Republic</option>
                                <option value="TD">Chad</option>
                                <option value="CL">Chile</option>
                                <option value="CN">China</option>
                                <option value="CX">Christmas Island</option>
                                <option value="CC">Cocos (Keeling) Island</option>
                                <option value="CO">Colombia</option>
                                <option value="KM">Comoros</option>
                                <option value="CG">Congo</option>
                                <option value="CK">Cook Island</option>
                                <option value="CR">Costa Rica</option>
                                <option value="CI">Cote D'ivoire</option>
                                <option value="HR">Croatia</option>
                                <option value="CU">Cuba</option>
                                <option value="CY">Cyprus</option>
                                <option value="CZ">Czech Republic</option>
                                <option value="DJ">DjiboutiI</option>
                                <option value="DM">Dominica</option>
                                <option value="DO">Dominican Republic</option>
                                <option value="TP">East Timor</option>
                                <option value="EC">Ecuador</option>
                                <option value="EG">Egypt</option>
                                <option value="SV">El Salvador</option>
                                <option value="GQ">Equatorial Guinea</option>
                                <option value="ER">Eritrea</option>
                                <option value="EE">Estonia</option>
                                <option value="ET">Ethiopia</option>
                                <option value="FK">Falkland Islands</option>
                                <option value="FO">Faroe Islands</option>
                                <option value="FJ">Fiji</option>
                                <option value="FI">Finland</option>
                                <option value="FX">France, Metropolitan</option>
                                <option value="GF">French Guiana</option>
                                <option value="PF">French Polynesia</option>
                                <option value="TF">French Souther Territories</option>
                                <option value="GA">Gabon</option>
                                <option value="GM">Gambia</option>
                                <option value="GE">Georgia</option>
                                <option value="GH">Ghana</option>
                                <option value="GI">Gibraltar</option>
                                <option value="GR">Greece</option>
                                <option value="GL">Greenland</option>
                                <option value="GD">Grenada</option>
                                <option value="GP">Guadeloupe</option>
                                <option value="GU">Guam</option>
                                <option value="GT">Guatemala</option>
                                <option value="GN">Guinea</option>
                                <option value="GW">Guinea-Bissau</option>
                                <option value="GY">Guyana</option>
                                <option value="HT">Haiti</option>
                                <option value="HM">Heard and McDonald Islands</option>
                                <option value="HN">Honduras</option>
                                <option value="HK">Hong Kong</option>
                                <option value="HU">Hungary</option>
                                <option value="IS">Iceland</option>
                                <option value="IN">India</option>
                                <option value="ID">Indonesia</option>
                                <option value="IR">Iran</option>
                                <option value="IQ">Iraq</option>
                                <option value="IE">Ireland</option>
                                <option value="IL">Israel</option>
                                <option value="IT">Italy</option>
                                <option value="JM">Jamaica</option>
                                <option value="JP">Japan</option>
                                <option value="JO">Jordan</option>
                                <option value="KZ">Kazakhstan</option>
                                <option value="KE">Kenya</option>
                                <option value="KI">Kiribati</option>
                                <option value="KW">Kuwait</option>
                                <option value="KG">Kyrgyzstan</option>
                                <option value="LA">Lao Peoples Democratic Republic</option>
                                <option value="LV">Latvia</option>
                                <option value="LB">Lebanon</option>
                                <option value="LS">Lesotho</option>
                                <option value="LR">Liberia</option>
                                <option value="LY">Libya</option>
                                <option value="LI">Liechtenstein</option>
                                <option value="LT">Lithuania</option>
                                <option value="LU">Luxembourg</option>
                                <option value="MO">Macau</option>
                                <option value="MK">Macedonia</option>
                                <option value="MG">Madagascar</option>
                                <option value="MW">Malawi</option>
                                <option value="MY">Malaysia</option>
                                <option value="MV">Maldives</option>
                                <option value="ML">Mali</option>
                                <option value="MT">Malta</option>
                                <option value="MH">Marshall Islands</option>
                                <option value="MQ">Martinique</option>
                                <option value="MR">Mauritania</option>
                                <option value="MU">Mauritius</option>
                                <option value="YT">Mayotte</option>
                                <option value="MX">Mexico</option>
                                <option value="FM">Micronesia, Federated States of</option>
                                <option value="MD">Moldova</option>
                                <option value="MC">Monaco</option>
                                <option value="MN">Mongolia</option>
                                <option value="MS">Montserrat</option>
                                <option value="MA">Morocco</option>
                                <option value="MZ">Mozambique</option>
                                <option value="MM">Myanamar</option>
                                <option value="NA">Namibia</option>
                                <option value="NR">Nauru</option>
                                <option value="NP">Nepal</option>
                                <option value="AN">Netherlands Antilles</option>
                                <option value="NC">New Caledonia</option>
                                <option value="NI">Nicaragua</option>
                                <option value="NE">Niger</option>
                                <option value="NG">Nigeria</option>
                                <option value="NU">Niue</option>
                                <option value="NF">Norfolk Island</option>
                                <option value="KP">North Korea</option>
                                <option value="MP">Northern Mariana Islands</option>
                                <option value="NO">Norway</option>
                                <option value="OM">Oman</option>
                                <option value="PK">Pakistan</option>
                                <option value="PW">Palau</option>
                                <option value="PA">Panama</option>
                                <option value="PG">Papua New Guinea</option>
                                <option value="PY">Paraguay</option>
                                <option value="PE">Peru</option>
                                <option value="PH">Philippines</option>
                                <option value="PN">Pitcairn Islands</option>
                                <option value="PL">Poland</option>
                                <option value="PT">Portugal</option>
                                <option value="PR">Puerto Rico</option>
                                <option value="QA">Qatar</option>
                                <option value="RE">Reunion</option>
                                <option value="RO">Romania</option>
                                <option value="RU">Russian Federation</option>
                                <option value="RW">Rwanda</option>
                                <option value="SH">Saint Helena</option>
                                <option value="KN">Saint Kitts and Nevis</option>
                                <option value="LC">Saint Lucia</option>
                                <option value="PM">Saint Pierre and Miquelon</option>
                                <option value="VC">Saint Vincent and The Grenadines</option>
                                <option value="WS">Samoa</option>
                                <option value="SM">San Marino</option>
                                <option value="ST">Sao Tome and Principe</option>
                                <option value="SA">Saudi Arabia</option>
                                <option value="SN">Senegal</option>
                                <option value="SC">Seychelles</option>
                                <option value="SG">Singapore</option>
                                <option value="SK">Slovakia</option>
                                <option value="SI">Slovenia</option>
                                <option value="SB">Solomon Islands</option>
                                <option value="SO">Somalia</option>
                                <option value="ZA">South Africa</option>
                                <option value="GS">South Georgia</option>
                                <option value="KR">South Korea</option>
                                <option value="ES">Spain</option>
                                <option value="SL">Sri Lanka</option>
                                <option value="SD">Sudan</option>
                                <option value="SR">Suriname</option>
                                <option value="SJ">Svalbardand Jan Mayen Islands</option>
                                <option value="SY">Syria</option>
                                <option value="TI">Tahiti</option>
                                <option value="TW">Taiwan</option>
                                <option value="TJ">Tajikistan</option>
                                <option value="TZ">Tanzania</option>
                                <option value="TH">Thailand</option>
                                <option value="TG">Togo</option>
                                <option value="TK">Tokelau</option>
                                <option value="TO">Tonga</option>
                                <option value="TT">Trinidad and Tobago</option>
                                <option value="TN">Tunisia</option>
                                <option value="TR">Turkey</option>
                                <option value="TM">Turkmenistan</option>
                                <option value="TC">Turks and Caicos Islands</option>
                                <option value="TV">Tuvalu</option>
                                <option value="UG">Uganda</option>
                                <option value="UA">Ukraine</option>
                                <option value="AE">United Arab Emirates</option>
                                <option value="UY">Uruguay</option>
                                <option value="UM">US Minor Outlying Islands</option>
                                <option value="UZ">Uzbekistan</option>
                                <option value="VU">Vanuatu</option>
                                <option value="VA">Vatican City</option>
                                <option value="VE">Venezuala</option>
                                <option value="VN">Vietnam</option>
                                <option value="VG">Virgin Islands (GB)</option>
                                <option value="VI">Virgin Islands (US)</option>
                                <option value="WF">Wallis and Futuna Islands</option>
                                <option value="EH">Western Sahara</option>
                                <option value="YE">Yemen</option>
                                <option value="YU">Yugoslavia</option>
                                <option value="ZR">Zaire</option>
                                <option value="ZM">Zambia</option>
                                <option value="ZW">Zimbabwe</option>
                            </select>
                        </p>
                    </li>
                    <li>
                        <p>
                            <a href="javascript:submitForm();" id="searchButton">Search Rates &amp; Book</a>
                        </p>
                    </li>
                </ul>
                <div class="SearchBox_BottomDiv">
                    <img src="<%= ContentURL %>/images/<%= Brand %>/bottom_edge.gif" id="ctrlSearchBox_imgBottomEdge" alt="Bottom" />
                </div>
            </div>
        </div>
    </div>
</div>
