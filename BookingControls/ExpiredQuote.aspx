﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExpiredQuote.aspx.cs" Inherits="ExpiredQuote" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>This Quote has Expired</title>    
    <%--<link href="<%= AssetsURL %>/css/Base.css?v=1.7" type="text/css" rel="stylesheet" />     
    <link href="<%= AssetsURL %>/css/<%= brandStr %>/selection.css?v=1.7" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" media="all" href="<%= AssetsURL %>/css/<%= brandStr %>/horizontal.css" />
    <link href="<%= AssetsURL %>/css/jquery.ui.dialog.css" rel="stylesheet" type="text/css"/>      
    <link href="<%= AssetsURL %>/css/<%= brandStr %>/jcal.css" type="text/css" rel="stylesheet" />--%>
    <link href="<%= AssetsURL %>/css/Base.css?v=<%= Version %>" type="text/css" rel="stylesheet" />
    <link href="<%= AssetsURL %>/css/<%= BrandChar %>/configure.css" type="text/css" rel="stylesheet" />    
    <link href="<%= CSSLink %>?v=<%= Version %>" type="text/css" rel="stylesheet" />
    <link href="<%= AssetsURL %>/css/jquery.ui.dialog.css" rel="stylesheet" type="text/css"/>      
    <link href="<%= AssetsURL %>/css/<%= BrandChar %>/jcal.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript" src="<%= ContentURL %>js/jquery-1.3.1.min.js"></script>   
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.dimensions.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.tooltip.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/date.js"></script>     
    <script type="text/javascript" src="<%= ContentURL %>js/hCtrl.js?v=1.7"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/selection.js?v=1.7"></script>   
    <script src="<%= ContentURL %>js/jquery-ui.min.js" type="text/javascript"></script>    

    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <%= AnalyticsCode %>     
    <%= OpenTag %> 
</head>
<body>
    <form id="expiredQuoteForm" runat="server" onsubmit="return serialiseQuote()">
        <%= DNAIMGTag %>
        <script type="text/javascript">            
            var logJS = true;            
            var $j = jQuery.noConflict();
            jQuery(document).ready(function() {               
                $j("a.PopUp").click(function() {
                    NewWindow($j(this).attr('href'), 'vCode', '720', '500', 'yes', 'center'); //mauiv2 wider popup
                   return false;
                });                
            });
        </script>
        <div id="header">
            <div class="headerContent">
                <div id="genericHeader">
                    <a href="/">
                        <img src="<%= AssetsURL %>/images/<%= BrandChar %>/icons/logo.png" alt="logo" />
                    </a>
                    <ul class="steps">
                        <li class="selected">
                            <span class="tab">1.
                                <label>Confirm</label></span>
                        </li>
                        <%--<li>
                            <span class="tab">2.
                                <label>Extras</label></span>
                        </li>--%>
                        <li class="tab">
                            <span class="tab">2.
                                <label>Payment</label></span>
                        </li>
                    </ul>
                    <a class="contactUs" href="<%= ContactUsURL %>" target="_blank"><span>Contact us <i class="icon-chevron-down"></i></span></a>
                    <a class="contactUs contactUsMobile" href="<%= ContactUsURL %>" target="_blank"><span><i class="icon-envelope"></i></span></a>
                </div>
            </div>
        </div>
        <div id="vehicleConfigureContainer" class="quote <%= QuoteStatusClass %>">       
            <%--<div id="catalogHeader">
                <span class="Image">
                    <img src="<%= AssetsURL %>/images/<%= BrandChar %>/logo.jpg" alt="logo" />
                </span>
                <a class="priceMatch" href="javascript:openPriceMatchWindow();">Price match guarantee</a>
                <span class="ContactInfo">
                    <%= ContactHeaderText %> or <a href="<%= ParentSiteURL %>/contactus" target="_blank">contact us</a>
                </span>
            </div>            
            <div id="breadCrumbs">
                <span class="Crumbs">
                    Search Again
                </span>                
            </div>--%>
            <div id="vehicleCatalogContainer">           
                <h2>Confirm your Quote</h2>
                <div id="configurePanel" class="expired">
                    <div id="promptContainer" style="float:left;clear:both;width:100%">
                        <div class="" id="promptTextArea" style="width: 730px; margin-bottom: 10px;">
                            <span><%= QuoteStatusMsg %></span>                       
                        </div>                        
                    </div>                    
                    <div id="topHeader">                                          
                    </div>
                    <div id="vehicleTopPanel">                
                        <div class="VehicleHeader">
                            <h3>
                                <a href="http://<%= VehicleContentLink %>" class="VehicleTitle PopUp">
                                    <%= VehicleName %>
                                </a>
                            </h3>
                            <h4>Travel details:&nbsp;</h4>
                            <div class="Locations">
                                <span><b>Pick up:</b> <%= PickUpLocationStr %>, <%= PickUpDateStr %></span>
                                <span><b>Drop off:</b> <%= DropOffLocationStr%>, <%= DropOffDateStr %></span>                            
                            </div>
                        </div>
                        <div class="VehicleImages">                    
                            <div class="Left">
                                <img src="<%= VehicleInclusionPath %>" alt="inclusions" />                    
                            </div>
                            <div class="Right">
                                <img src="<%= VehicleImagePath %>" alt="vehicle" />
                            </div>                                      
                        </div>                    
                    </div>                               
                    <div id="configureUpdatePanel">                                       
                        <div id="submitExpiredQuote" class="Submit">                                                    
                            <div>What would you like to do?</div>                            
                            <a id="searchVehicle" href="Selection.aspx?<%= SearchAgainURLParams %>" title="Search again">Search All</a>
                            <small id="orLabel">or</small>                                                                       
                            <a id="searchAllVehicles" href="Selection.aspx?<%= SearchAgainURLParamsNoVehicle %>" title="Search again">Search Vehicle</a>
                        </div>
                        <span>
                        
                        </span>
                    </div>                   
                </div>
            </div>            
            <%--<div id="disclaimer">
                <span>Please note: Rental prices quoted above are only valid at the time of booking.</span>
            </div>--%>
            <!-- Debug Code Starts -->
            <%= debugHTML %>            
        </div>
        <!-- footer starts -->
        <div id="footerContainer">
            <div id="footer">
                <div class="top">
                    <div id="disclaimer">
                        <span>
                            <strong>Please note:</strong> Rentals prices quoted above are only vaild at the time of booking.
                        </span>
                        <span>Alternative currency is based on approximate current exchange rates. All payments will be processed in <%= Currency %>.</span>
                    </div>
                    <div id="footerlogo">
                        <a href="#">
                            <img src="<%= AssetsURL %>/images/<%= BrandChar %>/icons/foot_logo.png" alt="Worldwide Rentals - Motorhomes and Cars.com" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div id="footerWrap">
                <div class="thl body">
                    <ul>
                        <li>
                            <a class="thl-logo" href="http://www.thlonline.com">
                                <span class="thl"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.keacampers.com">
                                <span class="kea"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.maui-rentals.com">
                                <span class="maui"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.britz.com">
                                <span class="britz"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.unitedcampervans.com">
                                <span class="united"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.alphacampers.com">
                                <span class="alpha"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.mightycampers.com">
                                <span class="mighty"></span>
                            </a>
                        </li>
                    </ul>
                    <span class="privacy">
                        <b>&copy; Tourism Holidings Limited</b><br />
                        &nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Privacy Policy</a>
                    </span>
                </div>
            </div>
        </div>
        <!-- footer ends -->
    </form>
</body>
</html>