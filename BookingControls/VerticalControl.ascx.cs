﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;


namespace THL.Booking
{

    public partial class VerticalControl : System.Web.UI.UserControl
    {

        public string Brand;
        public bool SplashMode;
        public string Target;
        
        public string CountryCode;
        public string CountryOfResidence;
        public string InitPanelStr;
        public string ContentURL;
        public string VehicleCode;
        public string ClassesStr;
        public string VehicleTypeStr, VCodeStr;
        public VehicleType requiredVType;
        public string PickUpDD, DropOfDD, VehicleDD, PickUpTimeDD, DropOffTimeDD, numAdultDD, numChildrenDD, TravelCountryDD;
        public string BaseURL;
        public string MaxTravellers;
        public string PanelTitle;
        public string IsDisabled = string.Empty;
        private CacheManager cm = null;
        public string AjaxResourceURL;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
            ContentURL = System.Web.Configuration.WebConfigurationManager.AppSettings["BookingControlContentURL"];
            AjaxResourceURL = System.Web.Configuration.WebConfigurationManager.AppSettings["AjaxReourceURL"];

            if (Request.Params["cr"] != null)
            {//TODO: validate input 
                CountryOfResidence = Request.Params["cr"].ToLower();
            }
            if (Request.Params["vt"] != null)
            {//TODO: validate input 
                VehicleTypeStr = Request.Params["vt"].ToLower();
            }
            else if (VehicleTypeStr == null)
            {
                VehicleTypeStr = "rv";//first timer no control settings
            }
            
            if (Request.Params["cc"] != null)
            {//TODO: validate input
                CountryCode = Request.Params["cc"].ToLower();
            }             

            bool disableCache = false;
            bool.TryParse(Request.Params["disableCache"], out disableCache);

            bool useLocalResource = true;//TODO: get This from Controls inputs


            THLBrands brandType = THLBrand.GetBrandForString(Brand);


            if (Application["BookingCacheManager"] == null || (disableCache))
            {
                string CacheDataPath = System.Web.Configuration.WebConfigurationManager.AppSettings["RemoteBookingControlContentURL"] + "/xml/bp" + ".xml";
                string LocalCacheDataPath = System.Web.Configuration.WebConfigurationManager.AppSettings["LocalDataPath"];
                cm = new CacheManager(LocalCacheDataPath, useLocalResource);
                Application.Lock();
                Application["BookingCacheManager"] = cm;
                Application.UnLock();
            }
            else
            {
                cm = (CacheManager)(Application["BookingCacheManager"]);
            }

            requiredVType = (VehicleTypeStr == "rv" || VehicleTypeStr == "av" ? VehicleType.Campervan : VehicleType.Car);//TODO: define default landing..
            PanelTitle = (requiredVType == VehicleType.Campervan ? "Campervan" : "Car");

            if (Brand == "x") CountryCode = "NZ";//Exploremore force
            if (CountryCode != null && CountryCode != string.Empty)
            {
                CountryCode = CountryCode.ToUpper();
                CountryCode = (!CountryCode.Equals("NZ") && !CountryCode.Equals("AU") ? "NZ" : CountryCode);//Force a default
                BranchCountryData bcd = cm.GetCountryData(CountryCode.ToUpper(), THLBrand.GetBrandForString(Brand));
                string defaultTime = (requiredVType == VehicleType.Campervan ? bcd.CampersDefaultHour : bcd.CarsDefaultHour); 
                MaxTravellers = (requiredVType == VehicleType.Campervan ? bcd.MaxPassengersCamper : bcd.MaxPassengersCar).ToString();//TODO: verify
                
                //BranchDisplayer bd = new BranchDisplayer(cm.GetBranchesForBrand("bp"));
                BranchDisplayer bd = new BranchDisplayer(cm.GetBranchesForBrand(brandType));

                PickUpDD = bd.GetBranchDDForBrand(CountryCode, VehicleTypeStr, "ctrlSearchBox_dropPickUpLocation", BranchType.PickUp, string.Empty);
                DropOfDD = bd.GetBranchDDForBrand(CountryCode, VehicleTypeStr, "ctrlSearchBox_dropDropOffLocation", BranchType.DropOff, string.Empty);
                PickUpTimeDD = bd.GetOpeningHoursDD(bcd, requiredVType, defaultTime, "ctrlSearchBox_dropPickUpTime");
                DropOffTimeDD = bd.GetOpeningHoursDD(bcd, requiredVType, defaultTime, "ctrlSearchBox_dropDropOffTime");
                VehicleDD = bd.GetVehiclesDDForBranch(requiredVType, cm.GetBrand(THLBrand.GetBrandForString(Brand)) , VehicleCode, CountryCode);
                numAdultDD = bd.GetPassengerDD(PassengerType.Adult, (requiredVType == VehicleType.Campervan ? bcd.MaxAdultsCamper : bcd.MaxAdultsCar), "ctrlSearchBox_dropAdults");
                numChildrenDD = bd.GetPassengerDD(PassengerType.Child, (requiredVType == VehicleType.Campervan ? bcd.MaxChildrenCamper : bcd.MaxChildrenCar), "ctrlSearchBox_dropChildren");
                TravelCountryDD = UserDataDisplayer.GetCountryOfTravelDD(CountryCode, THLBrands.Maui);//TODO: add brandType detection (as in horizontal)
            }
            else
            {
                PickUpDD = "<select disabled='disabled' class='SearchBox_FullListBox' id='ctrlSearchBox_dropPickUpLocation' name='ctrlSearchBox_dropPickUpLocation'><option>Select pick up location</option></select>";
                DropOfDD = "<select disabled='disabled' class='SearchBox_FullListBox' id='ctrlSearchBox_dropPickUpLocation' name='ctrlSearchBox_dropPickUpLocation'><option>Select drop off location</option></select>";
                PickUpTimeDD = "<select disabled='disabled' name='ctrlSearchBox_dropPickUpTime' id='ctrlSearchBox_dropPickUpTime' class='SearchBox_TimeListBox'><option value='10:00'>10:00am</option></select>";
                DropOffTimeDD = "<select disabled='disabled' name='ctrlSearchBox_dropPickUpTime' id='ctrlSearchBox_dropPickUpTime' class='SearchBox_TimeListBox'><option value='10:00'>10:00am</option></select>";
                VehicleDD = "<select disabled='disabled' class='SearchBox_FullListBox' id='ctrlSearchBox_dropVehicleType' name='ctrlSearchBox_dropVehicleType'><option value='-1'>Search all</option></select>";
                numAdultDD = "<select disabled='disabled' class='SearchBox_AdultsListBox' id='ctrlSearchBox_dropAdults' name='ctrlSearchBox_dropAdults'><option value='0' selected='selected'>Adults</option></select>";
                numChildrenDD = "<select disabled='disabled' class='SearchBox_AdultsListBox' id='ctrlSearchBox_dropChildren' name='ctrlSearchBox_dropChildren'><option value='0' selected='selected'>Children</option></select>";
                IsDisabled = "disabled='disabled'";
            }
        }
    }
}