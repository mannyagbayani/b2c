﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SaveQuote.aspx.cs" Inherits="SaveQuote" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Save Your Quote</title>
    <%--<link href="<%= AssetsURL %>/css/Base.css" type="text/css" rel="stylesheet" />    
    <link href="<%= AssetsURL %>/css/<%= brandStr %>/selection.css" type="text/css" rel="stylesheet" />--%>
    <link href="<%= AssetsURL %>/css/Base.css?v=<%= Version %>" type="text/css" rel="stylesheet" />
    <link href="<%= CSSLink %>?v=<%= Version %>" type="text/css" rel="stylesheet" />
    <link href="<%= AssetsURL %>/css/jquery.ui.dialog.css" rel="stylesheet" type="text/css"/>      
    <link href="<%= AssetsURL %>/css/<%= brandStr %>/jcal.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript" src="<%= ContentURL %>js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.validate.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.dimensions.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/jquery.tooltip.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/date.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/hCtrl.js?v=1.5"></script>
    <script type="text/javascript" src="<%= ContentURL %>js/selection.js?v=1.5"></script>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"> 
    <%= AnalyticsCode %>
    <%= OpenTag %>
</head>
<body>
    <form id="paymentForm" onsubmit="return submitQuoteForm()" action="">
        <%= DNAIMGTag %>
        <script type="text/javascript">
            var ContentbaseUrl = '<%= ContentURL %>';
            var SiteInfo = <%= SiteInfo %>;
            var currencyStr = '<%=  CurrencyStr %>';
            var currentSurcharge = <%= CurrentSurcharge %>;
            var vehicleCharges = <%= VehicleChargeJSON %>;
            var $j = jQuery.noConflict();
            jQuery(document).ready(function() { initPaymentPage(); });
        </script>
        <div id="header">
            <div class="headerContent">
                <div id="genericHeader">
                    <%--<a href="/">--%>
                    <a href="#">
                        <img src="<%= AssetsURL %>/images/<%= BrandChar %>/icons/logo.png" alt="logo" />
                    </a>
                    <ul class="steps">
                        <li>
                            <span class="tab">1.
                                <label>Select</label></span>
                        </li>
                        <li>
                            <span class="tab">2.
                                <label>Extras</label></span>
                        </li>
                        <li class="selected">
                            <span class="tab">3.
                                <label>Save Quote</label></span>
                        </li>
                    </ul>
                    <a class="contactUs" href="<%= ContactUsURL %>" target="_blank"><span>Contact us <i class="icon-chevron-down"></i></span></a>
                    <a class="contactUs contactUsMobile" href="<%= ContactUsURL %>" target="_blank"><span><i class="icon-envelope"></i></span></a>
                </div>
            </div>
        </div>
        <div id="paymentContainer" class="quote saveQuote">
            <%--<div id="catalogHeader">
                <span class="Image">
                    <img src="<%= AssetsURL %>/images/<%= brandStr %>/logo.jpg" alt="logo" />
                </span>
                <span class="ContactInfo">
                    <%= ContactHeaderText %> or <a href="<%= ParentSiteURL %>/contactus" target="_blank">contact us</a>
                </span>
            </div>            
            <div id="breadCrumbs">
                <span class="Crumbs">
                    <a href="<%= ParentSiteURL %>" class="HomeLnk">Home</a>&nbsp;&gt;&nbsp;<a  href="javascript:history.go(-2);" class="HomeLnk">Select your vehicle</a>&nbsp;&gt;&nbsp;<a href="javascript:history.back();" class="HomeLnk">Configure your vehicle</a>&nbsp;&gt;&nbsp;Save your quote
                </span>
                <ul id="stepsDisplayer">
                    <li>
                        <span id="PaymentStep">3.Quote</span>
                    </li>
                    <li>
                        <span id="ConfigureStep">2.Configure</span>
                    </li>
                    <li>
                        <span id="SelectStep">1.Select</span>
                    </li>
                </ul>
            </div>--%>
            <div id="vehicleCatalogContainer">
                <h2>3. Save my quote</h2>
                <div id="paymentPanel">
                    <div id="topHeader">
                        <h3>
                            <!--<a class='VehicleTitle' href='http://pathtocontentprovider/vehibclepage?vehicleCode=<%= SelectedVehicle.Code %>'>-->
                            <%= BrandTitle %> <%= SelectedVehicle.Name %>
                            <!--</a>-->
                        </h3>
                        <span class="CurrencyPanel" style="margin-top: 20px;"></span>
                    </div>
                    <div id="quoteDisclaimer">
                        <span>Saved quotes will be e-mailed to you and the price will be valid for 1 week from today.<br />
                            Your quote can be confirmed by phoning reservations or replying to the quote e-mail.<br />
                            Vehicle availability will be re-checked before your booking can be confirmed.<br />
                        </span>
                    </div>
                    <div id="rentalSummary">
                        
                        <div id="vehicleDetails">
                            <div id="travelDetails">
                                <big>Travel details:</big>
                                <table class="TravelTable">
                                    <tbody>
                                        <tr>
                                            <td class="lbl">Pick up:</td>
                                            <td class="date"><%= PickUpDateStr %></td>
                                            <td class="loc"><%= PickUpLocationStr %></td>
                                        </tr>
                                        <tr>
                                            <td class="lbl">Drop off:</td>
                                            <td class="date"><%= DropOffDateStr %></td>
                                            <td class="loc"><%= DropOffLocationStr %></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="vehicleSpecs">
                                <img src="<%= VehicleImagePath %>-ClearCut-144.jpg" title="<%= VehicleCode %>" />
                                <!--<img src="<%= AssetsURL %>/images/<%= brandStr %>/vehicles/inclusions/<%= VehicleImageConvension %>.gif" title="<%= VehicleCode %> inclusions" />-->
                            </div>
                        </div>
                        <big class="ChargeSum">Rental charge summary</big>
                        <%= RentalSummary %>
                        <div class="Disclaimer">
                            <span id="ccDesclaimerTxt">If paying final balance by credit card, the credit card surcharge will apply.</span>
                        </div>
                    </div>
                    <div id="creditCardDetails">
                        <h3>CUSTOMER DETAILS</h3>
                    </div>
                    <div id="personalDetails">
                        <big>Personal details: (required)</big>
                        <ul>
                            <li>
                                <p>
                                    <label for="customerTitleDD">Title:*</label>
                                    <select id="customerTitleDD" name="customerTitleDD">
                                        <option value="Mr">Mr</option>
                                        <option value="Mrs">Mrs</option>
                                        <option value="Ms">Ms</option>
                                        <option value="Miss">Miss</option>
                                        <option value="Master">Master</option>
                                        <option value="Dr">Dr</option>
                                    </select>
                                </p>
                            </li>
                            <li>
                                <p>
                                    <label for="customerFirstName">First Name:*</label>
                                    <input id="customerFirstName" name="customerFirstName" minlength="2" class="required ClickTaleSensitive" />
                                </p>
                            </li>
                            <li>
                                <p>
                                    <label for="customerLastName">Last Name:*</label>
                                    <input id="customerLastName" name="customerLastName" minlength="2" class="required ClickTaleSensitive" />
                                </p>
                            </li>
                            <li>
                                <p>
                                    <span>Note: Please enter your email address carefully as this will be the address we send your confirmation to.</span>
                                </p>
                            </li>
                            <li>
                                <p>
                                    <label for="emailAddressTxt">Email address:*</label>
                                    <input id="emailAddressTxt" name="emailAddressTxt" class="required email ClickTaleSensitive" />
                                </p>
                            </li>
                            <li>
                                <p>
                                    <label for="confirmEmailAddressTxt">Confirm email:*</label>
                                    <input id="confirmEmailAddressTxt" name="confirmEmailAddressTxt" class="required email ClickTaleSensitive" />
                                </p>
                            </li>
                            <li>
                                <p>
                                    <label for="telephoneTxt1">Telephone:*</label>
                                    <span class="PhoneNumber">
                                        <span class="number">
                                            <small>country code</small>
                                            <input id="telephoneTxt1" name="telephoneTxt1" size="10" class="required number" />
                                        </span>
                                        <span class="number">
                                            <small>area code</small>
                                            <input id="telephoneTxt2" name="telephoneTxt2" size="10" class="required number" />
                                        </span>
                                        <span class="number">
                                            <small>number</small>
                                            <input id="telephoneTxt3" name="telephoneTxt3" size="10" class="required number ClickTaleSensitive" />
                                        </span>
                                    </span>
                                </p>
                            </li>
                            <li style="display: none;">
                                <p>
                                    <label for="customerAgeDD">Age:</label>
                                    <select id="customerAgeDD" name="customerAgeDD">
                                        <option selected="selected" value="">Please Select</option>
                                        <option value="18-25">18-25</option>
                                        <option value="26-35">26-35</option>
                                        <option value="36-45">36-45</option>
                                        <option value="46+">46+</option>
                                    </select>
                                </p>
                            </li>
                            <li>
                                <p>
                                    <span>Any special requests or comments relating to your upcoming hire?</span>
                                </p>
                            </li>
                            <li>
                                <p>
                                    <label for="customerNoteTxt">Enter here:</label>
                                    <textarea id="customerNoteTxt" name="customerNoteTxt" class="ClickTaleSensitive" cols="50" rows="5" onblur="this.value = this.value.replace(/[^a-zA-Z 0-9 \n]+/g,'')"></textarea>
                                </p>
                            </li>
                        </ul>
                    </div>
                    <div id="termsAndConditions">
                        <big>Terms & Conditions</big>
                        <input type="checkbox" id="acceptedTerms" name="acceptedTerms" />
                        <span>YES - I have read and accept the rental <a class="PopUp" href="<%= TandCLink %>">terms and conditions</a> and acknowledge that I am responsible for obtaining all required travel documents and visas and for complying with all country entry/transit requirements.</span>
                    </div>
                    <div id="depositPanel" style="display: none;">
                        <span class="left">
                            <big class="PayDeposit"><%= (AddRWCMsg ? "Non refundable deposit" : "Deposit") %> (payable today)</big>
                            <big class="PayFull">Total charge <span class='payPeriod'>(payable today)</span></big>
                            <small class="PayDeposit"><span class='payPeriod'><%= AddRWCMsg ? "Final balance (payable 150 days prior to pickup)" : "Final balance (payable on pick-up)"%></span></small>
                        </span>
                        <%= PaymentTotalPanel %>
                    </div>
                    <div id="footerPanel" class="<%= OnRequest %>">
                        <span>Total includes all taxes (GST)</span>
                        <!--<span id="ccDesclaimerTxtBtm">If paying final balance by credit card, the credit card surcharge will apply.</span>-->
                        <span class="Buttons">
                            <a href="javascript:history.back();">Back</a>
                            <small class="right">
                                <label id="globalFormError">Please try again</label>
                                <input type="submit" class="SubmitBtn" value="Save Quote" id="submitQuoteLnk" />
                                <input type="hidden" id="bookingCompleted" name="bookingCompleted" value="0" />
                            </small>
                        </span>
                    </div>
                </div>
            </div>
            <!-- Debug Code Starts -->
            <%= debugHTML %>
            <!-- Debug Code Ends -->
        </div>

        <!-- footer starts -->
        <div id="footerContainer">
            <div id="footer">
                <div class="top">
                    <div id="disclaimer">
                        <span>
                            <strong>Please note:</strong> Rentals prices quoted above are only vaild at the time of booking.
                        </span>
                        <span>Alternative currency is based on approximate current exchange rates. All payments will be processed in <%= Currency %>.</span>
                    </div>
                    <div id="footerlogo">
                        <a href="#">
                            <img src="<%= AssetsURL %>/images/<%= BrandChar %>/icons/foot_logo.png" alt="Worldwide Rentals - Motorhomes and Cars.com" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div id="footerWrap">
                <div class="thl body">
                    <ul>
                        <li>
                            <a class="thl-logo" href="http://www.thlonline.com">
                                <span class="thl"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.keacampers.com">
                                <span class="kea"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.maui-rentals.com">
                                <span class="maui"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.britz.com">
                                <span class="britz"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.unitedcampervans.com">
                                <span class="united"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.alphacampers.com">
                                <span class="alpha"></span>
                            </a>
                        </li>
                        <li>
                            <a class="thllogos" href="http://www.mightycampers.com">
                                <span class="mighty"></span>
                            </a>
                        </li>
                    </ul>
                    <span class="privacy">
                        <b>&copy; Tourism Holidings Limited</b><br />
                        &nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Privacy Policy</a>
                    </span>
                </div>
            </div>
        </div>
        <!-- footer ends -->
    </form>
    <%= RemarketingCode %>
</body>
</html>
