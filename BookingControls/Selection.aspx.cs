﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;
using System.Collections.Specialized;

public partial class Selection : System.Web.UI.Page
{
    public string brandStr;
    public string ContentURL, AssetsURL;
    public string VehicleCatalog;
    public string CurrencySelector;
    public string DNAIMGTag, AnalyticsCode, ParentSiteURL;

    public string AvailRequestJSON = "null";

    public string HTTPPrefix;

    public string SSLSeal;

    public string ContactHeaderText;

    public string GlobalMeassage = string.Empty;

    public string debugStr, debugURL, debugHTML;

    public CurrencyCode defaultCurrency;

    public string CurrencyStr;

    public string AdsPath;

    public string ClickTaleTop = string.Empty, ClickTaleBottom = string.Empty;

    THLDomain appDomain;

    public string CrossSellURL;

    public string CCode;

    public string AffCode = string.Empty;

    public string RemarketingCode { get; set; }

    /// <summary>
    /// Support release version from config for forcing static assets cache refresh.
    /// </summary>
    public string Version { 
        get { return ApplicationManager.ReleaseVersion;} 
        set { value= ApplicationManager.ReleaseVersion;}  
    }

    public string OpenTag { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {     
        //EnableViewState = false;
        //try  
        //{

        bool isCar = !string.IsNullOrEmpty(Request.Params["vtype"]) && Request.Params["vtype"].Equals("ac");

        bool secureMode = false;
        bool.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["SecureMode"], out secureMode);

        //Cross Sell, TODO: consider refactoring into config manager
        CrossSellURL = String.IsNullOrEmpty(System.Web.Configuration.WebConfigurationManager.AppSettings["CrossSellURL"]) ? "https://secure.motorhomesandcars.com" : System.Web.Configuration.WebConfigurationManager.AppSettings["CrossSellURL"];

        HTTPPrefix = ApplicationManager.HTTPPrefix;

        if (!string.IsNullOrEmpty( Request.Params["ac"]))
        {
            SessionManager.AgentCode = Request.Params["ac"].ToUpper();
            AffCode = Request.Params["ac"].ToUpper();
        }

        appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
        
        if (appDomain == null)//debug line
            appDomain = new THLDomain("secure.maui.co.nz", THLBrands.Maui, CountryCode.NZ, "B2CNZ", false, "uahere");//dubug line             

        //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
        if (appDomain.Country.Equals(CountryCode.NONE))
        {
            if (Request.Params["cc"] != null)
            {
                appDomain.Country = (CountryCode)Enum.Parse(typeof(CountryCode), Request.Params["cc"].ToString().ToUpper()); 
            }
            else
                //default country code
                appDomain.Country = CountryCode.NZ;
        }

        decimal ver = 0;
        decimal.TryParse(Version, out ver);
        if (ver > 2.5m && !isCar)
        {            
            Response.Redirect("/" + appDomain.Country + "/select" + (Request.RawUrl.Contains("?") ? "?" + Request.RawUrl.Split('?')[1] : ""));
        }

        brandStr = appDomain.BrandChar.ToString(); //(Request.Params["brand"] != null ? Request.Params["brand"] : "m");//TODO: this should be swapped by baseURL. 
        ParentSiteURL = appDomain.ParentSite;

        CrossSellHeader1.CrossSellCountry = appDomain.Country.ToString();//added for cross sell banner support

        //DecideDNA Tracking Img
        TrackingDisplayer trd = new TrackingDisplayer(appDomain, secureMode);
        DNAIMGTag = trd.RenderDNATrackingImg("selection");

        //add marin tracking
        DNAIMGTag += trd.RenderMarinTrackingPixelIMG(SessionManager.MarinTrackingParams, MarinStep.SELECTION, 0.0m, string.Empty, string.Empty);

        AnalyticsCode = trd.RenderAnalsCode();//render default Analytics

        if (appDomain.ParentSite == "http://www.britz.com.au")//Tested exception added for ClickTale Trial 03/02/2011
        {//TODO: move into domain level config with account UID and sample ratio 0.0-1.0
            ClickTaleTop = trd.RenderClickTaleTracking("top");
            ClickTaleBottom = trd.RenderClickTaleTracking("bottom");
        }

        SSLSeal = trd.GetSealID();

        

        if (ver > 2.5m && !isCar)
        {
            if (Request.Params["msg"] != null && Request.Params["msg"].Length > 0)
            {
                GlobalMeassage = ContentManager.GetMessageForCode(THLBrand.GetBrandForString(brandStr), appDomain.Country, Request.Params["msg"]).Content;
                Response.Redirect("/" + appDomain.Country + "/select?msg=" + Request.Params["msg"]);//migration point, TODO: refactor. 
            }
            if (!string.IsNullOrEmpty(Request.Params["errMsg"]))
                Response.Redirect("/" + appDomain.Country + "/select?msg=" + Request.Params["errMsg"]);//migration point, TODO: refactor as above..             
        }

        if (Request.Params["rf"] != null && Request.Params["rf"].Length > 0)
        {
            SessionManager.AgentReference = Request.Params["rf"];
        }

        string domesticPhoneText = ContentManager.GetMessageForCode(THLBrand.GetBrandForString(brandStr), appDomain.Country, "DomesticPhone").Content;
        string internationalPhoneText = ContentManager.GetMessageForCode(THLBrand.GetBrandForString(brandStr), appDomain.Country, "InternationalPhone").Content;
        
        ContactHeaderText = ContentManager.GetMessageForCode(THLBrand.GetBrandForString(brandStr), appDomain.Country, "ContactText").Content + "<br />"
                            + domesticPhoneText + " - "
                            +  internationalPhoneText;

        ContentURL = HTTPPrefix + appDomain.Name + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["BookingControlContentPath"];
        string countryCode = appDomain.Country.ToString().ToUpper();  //(Request.Params["cc"] != null ? Request.Params["cc"] : "NZ").ToUpper();   
        if (appDomain.Brand == THLBrands.AIRNZ || appDomain.Brand == THLBrands.MAC)
            countryCode = (Request.Params["cc"] != null ? Request.Params["cc"] : "NZ").ToUpper();
                
        if (SessionManager.BookingStatus == BookingStatus.BookingSuccessful)
        {
            GlobalMeassage = "The quote for this booking has already been completed or is more than a day old. Please search again to start another booking.";//TODO: get this from Content Manager instance
            //Consider reseting the session here..
        }
        SessionManager.BookingStatus = BookingStatus.Created;

        HorizontalControl1.Brand = brandStr;
        HorizontalControl1.CountryCode = countryCode;

        AssetsURL = HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];

        AvailabilityRequest aRequest = new AvailabilityRequest();
        AvailabilityResponse aResponse = null;

        

        bool HasSession = false;//TODO: Query Session State here
        
        string paramsVersion = (Request.Params["pv"] != null && !IsPostBack ? Request.Params["pv"] : "2.0");
        
        if(Request.UrlReferrer != null && !Request.UrlReferrer.ToString().Contains(appDomain.Name)) paramsVersion = "1.0";

        //defaultCurrency = (countryCode == "NZ" ? CurrencyCode.NZD : CurrencyCode.AUD);//TODO: MaC update here
        defaultCurrency = CurrencyManager.GetCurrencyForCountryCode(countryCode);

        string preLoaderTypeStr = "SelectionPreloader";

        CurrencyManager curMan = new CurrencyManager(CurrencyConversionProvider.BNZ, defaultCurrency);//todo: from appManager
        CurrencySelector = new AvailabilityResponseDisplayer(null, aRequest, curMan, AssetsURL, appDomain.Brand).RenderCurrencyDropDown(defaultCurrency);

        if (!HasSession)
        {
            //construct from post params and convert to aurora format
            NameValueCollection auroraParams;
            switch (paramsVersion)
            {
                case "1.0"://old BPAE params
                    auroraParams = AvailabilityHelper.OldControlCollectionMapper(Request.Params);                    
                    break;
                case "2.0":
                    auroraParams = AvailabilityHelper.CtrlToAuroraCollectionMapper(Request.Params);
                    break;
                default:
                    auroraParams = AvailabilityHelper.CtrlToAuroraCollectionMapper(Request.Params);
                    break;
            }
                         
            debugStr = THLDebug.DisplayNameValueParams(auroraParams);//Test Line 

            AvailRequestJSON = (SessionManager.GetSessionData().HasRequestData ? SessionManager.GetSessionData().AvailabilityRequestData.ToJSON() : "null");//load ctrl from session

            if ( appDomain.IsAggregator && string.IsNullOrEmpty(Request.Params["utm_campaign"]))
                Response.Redirect("/"+ (aRequest.CountryCode.Equals(CountryCode.NZ) ? "nz" : "au") +"/select");//added for new cross-sell
            
            if (AvailabilityHelper.HasRequiredParams(auroraParams))
            {
                aRequest.LoadFromAuroraParams(auroraParams);

                AvailRequestJSON = aRequest.ToJSON();//reflect on ctrl

                aResponse = new DataAccess().GetAvailability(aRequest);

                aRequest.AgentCode = appDomain.AgentCode;

                CCode = aRequest.CountryCode.ToString();//added for MaC banner support                 

                SessionData sessionData = SessionManager.GetSessionData();
                sessionData.AvailabilityRequestData = aRequest;
                                
                CurrencyStr = CurrencyManager.GetCurrencyForCountryCode(aRequest.CountryCode).ToString();

                if (aRequest.CountryOfResidence != null && aRequest.CountryOfResidence != string.Empty)
                    sessionData.CountryOfResidence = aRequest.CountryOfResidence;//TODO : test
                
                aResponse.Currency = defaultCurrency;
                sessionData.AvailabilityResponseData = aResponse;

                //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
                //change from confiure.aspx to configurev2.aspx
                if (aResponse.GetAvailabilityItems("") != null && aResponse.GetAvailabilityItems("").Length == 1 && aResponse.GetAvailabilityItems("")[0].AVPID != null)
                    Server.Transfer("Configurev2.aspx?avp=" + aResponse.GetAvailabilityItems("")[0].AVPID);

                AvailabilityResponseDisplayer ard = new AvailabilityResponseDisplayer(aResponse, aRequest, curMan, AssetsURL, appDomain.Brand);
                ard.AppDomain = appDomain;//support convention based 
                
                //Replaced 12/11/2010 ard.ContactPhoneText = domesticPhoneText + "<br />" + internationalPhoneText;
                ard.ContactPhoneText = "Let our 24hr res team find a vehicle for you<br />We can search all the big brands:<br /><b>" + ContentManager.GetMessageForCode(THLBrand.GetBrandForString(brandStr), appDomain.Country, "CrossSale").Content + "</b>";

                string selectedCode = aRequest.VehicleModel;
                
                //TODO: do not render if radio changed
                VehicleCatalog = /* Added for radio swapping functionality 150110 */(Request.Params["radiosSwapped"] != "true")/* end radio swap*/ ? ard.RenderAvailabilityVehicleCatalog(selectedCode) : "";
                
                CurrencySelector = ard.RenderCurrencyDropDown(defaultCurrency);                 
               
                //Render Analytics Segment
                AnalyticsEventTrackingItem[] availTrackingItems = AvailabilityHelper.GetTrackingEventsForAvailResponse(aResponse, aRequest);

                if (!string.IsNullOrEmpty(VehicleCatalog))
                    DNAIMGTag += AvailabilityHelper.GetTrackingImgForAvailResponse(aResponse, aRequest);                
                if (availTrackingItems != null && availTrackingItems.Length > 0 && appDomain.Brand != THLBrands.AIRNZ)
                    AnalyticsCode = trd.RenderAnalsCode(availTrackingItems);
                else
                    AnalyticsCode = trd.RenderAnalsCode();
                
                initControl(aRequest);
            }
            else
            {
                aRequest.Init();//load defaults  
                //initControl(aRequest);
            }
        }
        else
        {
            // construct from session avail request object
        }
               
        //catch (Exception ex)
        //{
        //    string Message = ex.Message;
        //    THLDebug.LogError(ErrorTypes.Application,"Selection.PageLoad", Message,"{t:fend}");
        //    Response.Redirect("Selection.aspx?msg=sessionExpired");
        //}
        AdsPath = AvailabilityHelper.GetCentralLibPath(aRequest, ApplicationManager.GetDomainForURL(Request.Url.Host), AssetsURL) + "/" + preLoaderTypeStr + "/Ad";
        RemarketingCode = trd.RenderRemarketingSnippets();//remarketing.
        //-------------------------------- Debug Zone -------------------------
        debugURL = "unittests/debugStr.aspx?ts=" + DateTime.Now.Millisecond;
        debugHTML = (Request.Params["debugOn"] != null && Request.Params["debugOn"] == "true" ? "<div id='debugSection' style='float: left; margin-top: 20px; width:757px; height: 800px; background-color: #fff;'><div id='debugValue'>" + debugStr + "</div><iframe src='" + debugURL + "' width='757px' height='800px' /></div>" : string.Empty);

        OpenTag = AvailabilityHelper.LoadOpenTag(appDomain); 
    }

    void initControl(AvailabilityRequest aRequest)
    {
        HorizontalControl1.PUDate = aRequest.PickUpDate.ToString("dd-MMM-yyyy");
        HorizontalControl1.DODate = aRequest.DropOffDate.ToString("dd-MMM-yyyy");
        HorizontalControl1.Brand = THLBrand.GetBrandChar(appDomain.Brand).ToString();
        //HorizontalControl1.CountryCode = appDomain.Country.ToString(); //aRequest.CountryCode.ToString();
        HorizontalControl1.CountryOfResidence = (aRequest.CountryOfResidence != null ? aRequest.CountryOfResidence.ToUpper() : string.Empty);
        HorizontalControl1.VehicleTypeStr = (aRequest.VehicleType == VehicleType.Campervan ? "av" : "ac");
        //HorizontalControl1.VCode = aRequest.VehicleModel;
        HorizontalControl1.PULoc = aRequest.PickUpBranch.ToUpper();
        HorizontalControl1.DOLoc = aRequest.DropOffBranch.ToUpper();
    }
}