<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="VerticalDemo.aspx.cs" Inherits="_Bookings" %>
<%@ Register Src="VerticalControl.ascx" TagName="VerticalControl" TagPrefix="thl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>        
    <title>Vertical Booking Control DEV Zone</title>
    <script type="text/javascript" src="<%= ContentURL %>/js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>/js/jquery.dimensions.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>/js/date.js"></script>   
    <script type="text/javascript" src="<%= ContentURL %>/js/calendar/utils.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>/js/calendar/calendar.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>/js/calendar/calendar-setup.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>/js/calendar/lang/calendar-en.js"></script>    
    <script type="text/javascript" src="<%= ContentURL %>/js/vCtrl.js"></script> 
    <link href="css/Base.css" type="text/css" rel="stylesheet" />       
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <thl:VerticalControl ID="VerticalControl1" VehicleTypeStr="av" CountryOfResidence="NZ" CountryCode="AU" VehicleCode="auavp.BP4BS" Brand="p" SplashMode="true" Target="http://secure.backpackercampervans.co.nz/pages" runat="server" />
        </div>
        <div style="float:left;clear: both;margin-top: 30px;">
            <big>Test Vehicle Code Population</big><br /><br />
            <label>Vehicle Code:</label><input type="text" id="vehicleCodeInput" />
            <label>Vehicle Name:</label><input type="text" id="vehicleNameInput" />
            <a href="javascript:setSelectedVehicle($j('#vehicleCodeInput').val(), $j('#vehicleNameInput').val())">Populate Vehilce Code</a>
        </div>
    </form>
</body>
</html>