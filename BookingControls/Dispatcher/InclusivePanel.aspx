﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InclusivePanel.aspx.cs" Inherits="Dispatcher_InclusivePanel" %>
<div>
    <big>All Inclusive package includes -</big>
    <ul>       
        <li>Excess Reduction 2 (nil excess)</li>
        <li> No Worries Cover (covers overhead &amp; underbody vehicle damage)</li>
        <li> Pre-Purchase Gas (gas does not have to be full on vehicles return)</li>
        <li> Diesel Tax Recovery Fee (government levied diesel tax)</li>
        <li> 1x Camping Table</li>
        <li> Camping Chairs (per person)</li>
        <li> 1x Baby or Booster Seat</li>
        <li> Snow Chains (if required)</li>
        <li> Portable Heater (if required)</li>
    </ul>
</div>