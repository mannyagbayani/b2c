﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using THL.Booking;

public partial class SelectionPage : System.Web.UI.Page
{
    public string ctrlParams { get; set; } 

    public string VehicleCatalog { get;set; }
    public string bodyClass { get; set; }
    public string InitJSON { get; set; }
    public string CurrencySelector { get; set; }
    public static Random rnd = new Random();//just for displayer test,remove
    public static string AssetsURL { get; set; }
    public string BrandChar { get; set; }
    public string BrandedHeader { get; set; }
    public string BrandedFooter { get; set; }

    public static string HTTPPrefix { get; set; } 
    public static string AssetsUrl {get; set; }

    public string RemarketingCode { get; set; }
    public string AnalyticsCode { get; set; }
    public string DomainJSON { get; set; }
    public string StyleBase { get; set; }
    public string RequestJSON { get; set; }
    public string CSSLink { get; set; }

    //Open Tag
    public string OpenTag {get;set;}

    public string ImageURL { get; set; }

    public string Currency
    {
        //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
        //get
        //{
        //    appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
        //    return CurrencyManager.GetCurrencyForCountryCode(appDomain.Country.ToString()).ToString();
        //}
        get;
        set;
    }

    public bool IsSecureMode
    {
        get
        {
            bool secureMode = false;
            bool.TryParse(System.Web.Configuration.WebConfigurationManager.AppSettings["SecureMode"], out secureMode);
            return secureMode;
        }
    }

    public string ContactUsURL {
        get {
            THLDomain appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);
            string contactUrl = appDomain.ParentSite + "/contactus";
            switch (appDomain.ParentSite)
            { 
                case "keacampers.co.nz":
                    contactUrl = "http://nzrentals.keacampers.com/en/contact/contact_us.aspx";
                    break;
                case "keacampers.com.au": 
                    contactUrl = "http://nzrentals.keacampers.com/en/contact/contact_us.aspx";
                    break;
            }

            

            return contactUrl;
        }    
    }

    /// <summary>
    /// Marin Tracking Code
    /// </summary>
    public string DNAIMGTag { get; set; }

    THLDomain appDomain;

    protected void Page_Load(object sender, EventArgs e)
    {
        
        HTTPPrefix = ApplicationManager.HTTPPrefix;
        AssetsURL = HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];
        ApplicationData appData = ApplicationManager.GetApplicationData();
        appDomain = ApplicationManager.GetDomainForURL(Request.Url.Host);

        string[] aggBrands = new string[] {"q", "m", "u","b", "a", "y"};//TODO: from config
        //string[] aggBrands = new string[] { "m"};//TODO: from config
        string currentCountry = !string.IsNullOrEmpty(Request.Params["cc"]) && Request.Params["cc"].ToLower().Contains("au") ?  "AU" : "NZ";//based on request

        //PivotalTracker 73171752: MAC - Move server from old code base to new code base. (i.e. THL-MS-IIS-01 to THL-MS-IIS-03)
        Currency = CurrencyManager.GetCurrencyForCountryCode(currentCountry).ToString();

        CountryCode selectedCountry = currentCountry.Equals("AU") ? CountryCode.AU : CountryCode.NZ;
        string agentCode = "mac";
        BrandChar = appDomain.BrandChar.ToString();//TODO: domain object
        //Below IF condition was changed by Nimesh on 25/07/2014 so that agent code is not picked up when brand is other than the ones in aggBrands
        //if (!string.IsNullOrEmpty(Request.Params["ac"]) && !aggBrands.Contains(BrandChar))
        //Reverted Back by Nimesh

        if (!string.IsNullOrEmpty(Request.Params["ac"]))
        {

            string acCode = Request.Params["ac"].ToLower();
            BrandChar = /*Request.Params["ac"].ToLower();//*/ AgentTheme.CSSPath(acCode, BrandChar);
            agentCode = Request.Params["ac"].ToLower();
            ImageURL = "select?ac=" + agentCode;
            
        }
        else 
        {
            ImageURL = "";
            agentCode = appDomain.AgentCode;
        }        

        InitJSON = RenderInitJSON(selectedCountry, agentCode, Request.Params);

        bodyClass = currentCountry.ToLower() + ((!string.IsNullOrEmpty(Request.Params["promo"]) && Request.Params["promo"].Equals("1")) ? " promo" : string.Empty) + " "+ appDomain.Brand.ToString().ToLower(); 
        
        
        DomainJSON = appDomain.ToJSON();
        

        Branch[] branches = appData.GetWidgetBranches();
        ctrlParams = "{branches: " + RenderBranchJSON(branches) + ", openHours : " +
            "[{'vt':'av','cc':'au','pt':7, 'dt':17, 'df': '10-15'},{'vt':'ac','cc':'au','pt':0, 'dt':23, 'df': '10-15'},{'vt':'av','cc':'nz','pt':7, 'dt':17, 'df': '10-15'},{'vt':'ac','cc':'nz', 'pt':0, 'dt':23, 'df': '10-15'}]"
            /* TODO: from config */
            +"}";

        AssetsURL = ApplicationManager.HTTPPrefix + System.Web.Configuration.WebConfigurationManager.AppSettings["AssetsURL"];

        CSSLink = string.IsNullOrEmpty(Request.Params["cssLink"]) ? AssetsURL + "/css/" + BrandChar + "/csell.css" : Request.Params["cssLink"];
        
        Vehicle[] vehicles = appData.Vehicles;
        var sortedVehicles =
           from v in vehicles
           where v.Type == VehicleType.Campervan && v.Country == currentCountry
           orderby v.Brand, v.MaxPassengers
           select v;
        StringBuilder sb = new StringBuilder();
        //Commented by Nimesh on 06th Oct 2014
        if (appDomain.IsAggregator)
        {

            foreach (string brandStr in aggBrands)
            {
                sb.Append(GetBrandVehicleCatalog(sortedVehicles.ToArray(), THLBrand.GetBrandForString(brandStr), AssetsURL));
            }
        }
        else
        {
            sb.Append(GetBrandVehicleCatalog(sortedVehicles.ToArray(), appDomain.Brand, AssetsURL));
        }
        //End Commented by Nimesh on 06th Oct 2014
        //Added by Nimesh on 06th Oct 2014
        //sb.Append(GetBrandVehicleCatalog(sortedVehicles.ToArray(), appDomain.Brand, AssetsURL));
        //End Added by Nimesh on 06th Oct 2014
        VehicleCatalog = sb.ToString();

        //currency
        CurrencyCode defaultCurrency = currentCountry.Equals("NZ") ? CurrencyCode.NZD : CurrencyCode.AUD;
        CurrencyManager curMan = new CurrencyManager(CurrencyConversionProvider.BNZ, defaultCurrency);//todo: from appManager        
        CurrencySelector = new AvailabilityResponseDisplayer(null, null, curMan, string.Empty, THLBrands.MAC).RenderCurrencyDropDown(defaultCurrency);

        TrackingDisplayer trd = new TrackingDisplayer(appDomain, ApplicationManager.HTTPPrefix == "https:");
        AnalyticsCode = trd.RenderAnalsCode();
        
        RemarketingCode = trd.RenderRemarketingSnippets();

        //add marin tracking
        DNAIMGTag += trd.RenderMarinTrackingPixelIMG(SessionManager.MarinTrackingParams, MarinStep.SELECTION, 0.0m, string.Empty, string.Empty);

        AvailabilityRequest request = new AvailabilityRequest();
        request.LoadFromQueryStringParams(Request.Params);
        RequestJSON = request.ToJSON();

        LoadAgentView(agentCode);//load theme for passe agent, if any..
        SessionManager.BookingStatus = BookingStatus.Created;
        //
        OpenTag = AvailabilityHelper.LoadOpenTag(appDomain);
    }
    
    /// <summary>
    /// Render a Brand Column initial view, refactor into Vehicle Displayer
    /// </summary>
    /// <param name="vehicles"></param>
    /// <param name="brand"></param>
    /// <returns></returns>
    public string GetBrandVehicleCatalog(Vehicle[] vehicles, THLBrands brand, string assetsURL)
    {
        int brandVehicleCount = 0;
        StringBuilder sb = new StringBuilder();
        sb.Append(@"
        <div class=""brand"" id=""" + brand.ToString().ToLower()  +@"_vehicle"">");
        foreach (Vehicle vehicle in vehicles)
        {
            if (!brand.Equals(vehicle.Brand)) continue;//only this brand's vehicles
            sb.Append(appDomain.IsAggregator ? RenderVehicleItem(vehicle, assetsURL) : RenderBrandedVehicleItem(vehicle, assetsURL));
            brandVehicleCount++;
        }
        sb.Append("</div>");
        return brandVehicleCount > 0 ? sb.ToString() : string.Empty;    
    }


    public string RenderBrandsHeader(string[] brands)
    {
        StringBuilder sb = new StringBuilder();
        
        return sb.ToString();
    }

    

    /// <summary>
    /// Render a Vehicle Item Tile For Aggregator Mode
    /// </summary>
    /// <param name="vehicle"></param>
    /// <returns></returns>
    public string RenderBrandedVehicleItem(Vehicle vehicle, string assetsURL)
    {
        //Changed by Nimesh on 27th Aug 2014 so that div contains dataprice, datalength and datasleeps so that the vehicles are arranged/sorted by isotope
        //if(vehicle.Content == null || vehicle.Content.Sleeps == 0) return @"<div class=""emptyVehicle"" ></div>";//TODO: biz point, merge this condition once only reading from feed.

        //return @"<div class=""emptyVehicle"" data-price=""999999"" data-sleeps='" + (vehicle.Content != null ? vehicle.Content.Sleeps : 0) + "' data-length='" + (vehicle.Content != null ? vehicle.Content.Length : 0.0m) + "'></div>";//TODO: biz point, merge this condition once only reading from feed.

        if (vehicle.Content == null || vehicle.Content.Sleeps == 0) return @"<div class=""emptyVehicle"" data-price=""999999"" data-sleeps='" + (vehicle.Content != null ? vehicle.Content.Sleeps : 0) + "' data-length='" + (vehicle.Content != null ? vehicle.Content.Length : 0.0m) + "'></div>";//TODO: biz point, merge this condition once only reading from feed.
        
        
        //End Changed by Nimesh on 27th Aug 2014 so that div contains dataprice, datalength and datasleeps so that the vehicles are arranged/sorted by isotope
        string normalizedVehicleCode = vehicle.Code.Split('.')[1];
        string vehicleImagePath = AvailabilityHelper.GetVehicleImageBasePath(vehicle.Country.Equals("NZ") ? CountryCode.NZ : CountryCode.AU, vehicle.Brand, vehicle.Type, normalizedVehicleCode, assetsURL, false);
        string vehicleClearCut72Image = String.Format("<img src='{0}-ClearCut-72.jpg' border='0' title='{1}' />", vehicleImagePath, normalizedVehicleCode);
        string contentUrl = vehicle.Content != null && !string.IsNullOrEmpty(vehicle.Content.PopupUrl) ? vehicle.Content.PopupUrl : "#"; //TODO: define missing url path
        int price = 999999;//TODO: swap with preloader <span>
        int sleeps = vehicle.Content != null ? vehicle.Content.Sleeps : 0;
        string wheelDrive = vehicle.Content != null && !string.IsNullOrEmpty(vehicle.Content.DriveMode) ? vehicle.Content.DriveMode.ToLower() : "na";
        string toilet = vehicle.Content != null && vehicle.Content.HasToilet ? "tlt" : "notlt";
        decimal length = vehicle.Content != null ? vehicle.Content.Length : 0.0m;
        string currency = (vehicle.Country.Equals("NZ") ? "NZD" : "AUD");
        
        StringBuilder sleepsClasses = new StringBuilder(); 
        for(int i=1; i<= sleeps; i++)
            sleepsClasses.Append("s"+ i + " ");
        
        StringBuilder sb = new StringBuilder();
        sb.Append(@"
        <div class=""element " + sleepsClasses.ToString() + " " + "w" + wheelDrive + " " + toilet + @""" id=""v_" + normalizedVehicleCode + 
            @""" data-sleeps=""" + sleeps + 
            @""" data-price=""" + price +
            @""" data-length=""" + length + 
            @""">
            <div class=""vehicle loading"">
                <div class=""vhead row"">
                    <span class=""left"">
                        <h4>" + vehicle.Name + @"</h4>
                        <span class=""slps row"">
                            <small>SLEEPS " + sleeps + @"</small>
                            <span class=""spc"">HOT DEAL</span>    
                        </span>
                    </span>
                    <span class=""right"">
                        <span class=""inf row"" rel=""" + contentUrl + @""">&nbsp;<span>All Vehicle Info</span></span>
                    </span>                    
                </div>
                <div class=""vprice"">
                    <div class=""head"">
                        <span class=""left"">
                            <span class=""prc row"" data-cur=""" + currency.ToLower() + @""">&nbsp;</span>
                            <span class=""cur row"">" + currency + @"</span>
                            <span class=""forex""><span class=""prc""></span><span class=""cur""></span></span>
                        </span>
                        <span class=""right"">
                            <span class=""lbl"">Price Details</span>
                            <span class=""expand""></span>
                        </span>
                    </div>
                    <div class=""det""></div>
                </div>
                <div class=""vstatus"">
                    <span class=""row status prcMsg"" title=""looking for availability"" rel=""" + THLBrand.GetBrandChar(vehicle.Brand) + @""">SEARCHING AVAILABILITY</span>
                </div>
                <div class=""vfeat row"">
                    <span class=""ftrs left"">" + RenderBrandedVehicleFeatureList(vehicle) + @"</span>
                    <span class=""tmb right"">" + vehicleClearCut72Image + @"</span>
                </div>
                <div class=""vfoot row"">
                    <span class=""spcDet""></span>
                    <span class=""row book"">Select</span>
                    <span class=""alt"">
                        <a href=""#"" class=""lnk"">Check Alternative Availability</a>
                        <a href=""#"" class=""btn"">Alternative Availability</a>
                    </span>                    
                </div>
            </div>        
        </div>");
        return sb.ToString();
    }

    /// <summary>
    /// Render a Vehicle Item Tile for Branded Mode
    /// </summary>
    /// <param name="vehicle"></param>
    /// <returns></returns>
    public string RenderVehicleItem(Vehicle vehicle, string assetsURL)
    {
        //return @"<div class=""emptyVehicle"" data-price=""999999"" data-sleeps='" + (vehicle.Content != null ? vehicle.Content.Sleeps : 0) + "' data-length='" + (vehicle.Content != null ? vehicle.Content.Length : 0.0m) + "'></div>";//TODO: biz point, merge this condition once only reading from feed.
        //Changed by Nimesh on 27th Aug 2014 so that div contains dataprice, datalength and datasleeps so that the vehicles are arranged/sorted by isotope
        //if (vehicle.Content == null || vehicle.Content.Sleeps == 0) return @"<div class=""emptyVehicle""></div>";//TODO: biz point, merge this condition once only reading from feed.
     //   if (vehicle.Content == null || vehicle.Content.Sleeps == 0) return @"<div class=""emptyVehicle"" data-price=""999999"" data-sleeps='" + (vehicle.Content != null ? vehicle.Content.Sleeps : 0) + "' data-length='" + (vehicle.Content != null ? vehicle.Content.Length : 0.0m) + "'></div>";//TODO: biz point, merge this condition once only reading from feed.
        //End Changed by Nimesh on 27th Aug 2014 so that div contains dataprice, datalength and datasleeps so that the vehicles are arranged/sorted by isotope
        string normalizedVehicleCode = vehicle.Code.Split('.')[1];
        string vehicleImagePath = AvailabilityHelper.GetVehicleImageBasePath(vehicle.Country.Equals("NZ") ? CountryCode.NZ : CountryCode.AU, vehicle.Brand, vehicle.Type, normalizedVehicleCode, assetsURL, false);
        string vehicleClearCut72Image = String.Format("<img src='{0}-ClearCut-72.jpg' border='0' title='{1}' />", vehicleImagePath, normalizedVehicleCode);
        string contentUrl = vehicle.Content != null && !string.IsNullOrEmpty(vehicle.Content.PopupUrl) ? vehicle.Content.PopupUrl : "#"; //TODO: define missing url path
        int price = 999999;//TODO: swap with preloader <span>
        int sleeps = vehicle.Content != null ? vehicle.Content.Sleeps : 0;
        string wheelDrive = vehicle.Content != null && !string.IsNullOrEmpty(vehicle.Content.DriveMode) ? vehicle.Content.DriveMode.ToLower() : "na";
        string toilet = vehicle.Content != null && vehicle.Content.HasToilet ? "tlt" : "notlt";
        decimal length = vehicle.Content != null ? vehicle.Content.Length : 0.0m;
        string currency = (vehicle.Country.Equals("NZ") ? "NZD" : "AUD");
        StringBuilder sb = new StringBuilder();
        // Added by Nimesh on 06th Oct 2014
        StringBuilder sleepsClasses = new StringBuilder();
        for (int i = 1; i <= sleeps; i++)
            sleepsClasses.Append("s" + i + " ");
        // End Added by Nimesh on 06th Oct 2014
        sb.Append(@"
        <div class=""element s" + sleepsClasses.ToString() + " " + "w" + wheelDrive + " " + toilet + @""" id=""v_" + normalizedVehicleCode +
            @""" data-sleeps=""" + sleeps +
            @""" data-price=""" + price +
            @""" data-length=""" + length +
            @""">
            <div class=""vehicle loading"">
                <h4>" + vehicle.Name + @"</h4>
                <span class=""slps row"">
                    <small>SLEEPS " + sleeps + @"</small>
                    <span class=""spc"">HOT DEAL</span>    
                </span>
                <span class=""tmb row"">
                    " + vehicleClearCut72Image + @"</span>
                <span class=""prc row"" data-cur=""" + currency.ToLower() + @""">&nbsp;</span>
                <span class=""cur row"">" + currency + @"</span>
                <span class=""ftrs"">
                    " + RenderVehicleFeatureList(vehicle) + @"
                </span>           
                <span class=""row book"">Book Vehicle</span>
                <span class=""row status"" title=""looking for availability"" rel=""" + THLBrand.GetBrandChar(vehicle.Brand) + @""">SEARCHING AVAILABILITY</span>
                <span class=""inf row"" rel=""" + contentUrl + @""">&nbsp;<span>more details</span></span> 
            </div>        
        </div>");
        return sb.ToString();
    }

    public string RenderVehicleFeatureList(Vehicle vehicle)
    {
        if (vehicle.Content == null) return string.Empty;
        THL.Booking.VehicleContent content = vehicle.Content;
        StringBuilder sb = new StringBuilder();
        sb.Append(@"
                <ul>
                    " + (content.HasShower  ? "<li>Shower</li>" : string.Empty) + @"
                    " + (content.HasDVD ? "<li>LCD/DVD</li>" : string.Empty) + @"
                    " + (content.HasToilet ? "<li>Toilet</li>" : string.Empty) + @"        
                    " + (content.HasGasStove ? "<li>Gas Stove</li>" : string.Empty) + @"
                    " + (content.HasButanStove ? "<li>Butane Stove</li>" : string.Empty) + @"
                    " + (content.FridgeSize > 0 ? "<li>" + content.FridgeSize + @"L Fridge</li>" : string.Empty) + @"
                    " + (content.HasMicrowave ? "<li>Microwave</li>" : string.Empty) + @"
                    <li>" + content.Transmission + @"</li>                    
                </ul>");
        return sb.ToString();
    }

    /*
    public string RequestJSON()
    { 
        StringBuilder sb = new StringBuilder("{");
        foreach (string key in Request.Params.Keys)
        {
            string val = "";
            switch (key)
            {
                case "vh":
                    val = Request.Params[key];
                    break;
                default:
                    val = string.Empty;
                    break;
            }
            if(!string.IsNullOrEmpty(val))
                sb.Append(@"""" + key + @""":""" +  val + @""",");
        }
        return sb.ToString().TrimEnd(',') + "}";
    } refactored */

    /// <summary>
    /// Add two item grouping
    /// </summary>
    /// <param name="vehicle"></param>
    /// <returns></returns>
    public string RenderBrandedVehicleFeatureList(Vehicle vehicle)
    {
        if (vehicle.Content == null) return string.Empty;
        THL.Booking.VehicleContent content = vehicle.Content;
        StringBuilder sb = new StringBuilder();
        string csvFeatures = (content.HasShower ? "Shower," : string.Empty) +
                                (content.HasDVD ? "LCD/DVD," : string.Empty) +
                                (content.HasToilet ? "Toilet," : string.Empty) +
                                (content.HasGasStove ? "Gas Stove," : string.Empty) +
                                (content.HasButanStove ? "Butane Stove," : string.Empty) +
                                (content.FridgeSize > 0 ? content.FridgeSize + @"L Fridge," : string.Empty) +
                                (content.HasMicrowave ? "Microwave" : string.Empty); 
        string[] features = csvFeatures.TrimEnd(',').Split(',');
        for (int i = 0; i < features.Length; i += 2) {
            sb.Append("<ul><li>" + features[i] + "</li><li>" + (i+1 < features.Length ? features[i+1] : string.Empty) + "</li></ul>");
        }    
        return sb.ToString();
    }


    /// <summary>
    /// TODO: refactor into displayer
    /// </summary>
    /// <param name="branches"></param>
    /// <returns></returns>
    public static string RenderBranchJSON(Branch[] branches)
    {
        StringBuilder branchSb = new StringBuilder(), vehicleSb = new StringBuilder();
        foreach (Branch branch in branches)
        {
            string brandsStr = string.Empty;
            foreach (THLBrands brand in branch.brands)
                brandsStr += THLBrand.GetBrandChar(brand);
            branchSb.Append(@"{""name"":""" + branch.Name + @""",""code"":""" + branch.BranchCode + @""",""brands"":""" + brandsStr + @""",""vt"":""" + branch.VehicleTypeAvailability.ToString().ToLower() + @""",""cc"":""" + branch.country.ToString().ToLower() + @"""},");
        }
        return "[" + branchSb.ToString().TrimEnd(',') + "]";
    }

    /// <summary>
    /// This will come from config and based on requesting agent and country
    /// </summary>
    public static string RenderInitJSON(CountryCode countryCode, string agentCode, NameValueCollection requestParams) {
        string debugJson = "[]";
        string json = string.Empty;
        DateTime defaultPickUp = DateTime.Now.AddDays(10), defaultDropOff = defaultPickUp.AddDays(7);//TODO: from config?
        string format = "dd-MM-yyyy";
        string pickDateStr = defaultPickUp.ToString(format), dropDateStr = defaultDropOff.ToString(format);
        string cur = string.Empty;
        string queryBrands = string.Empty;
        //Added PromoCode by Nimesh on 16th Oct 2014
        string promoCode = string.Empty;
        if (countryCode.Equals(CountryCode.AU))
        {
            cur = "aud";
            queryBrands = "['kea', 'maui', 'britz', 'mighty']";
        }
        else {
            cur = "nzd";
            queryBrands = "['kea','maui', 'britz','united','alpha' , 'mighty']";
        }

        if (!string.IsNullOrEmpty(requestParams["cc"]) && agentCode != "virginb2c")//TODO: refactor for all agents scenario
        {
            try
            {
                int na = 1, nc = 0;
                string packageCode = !string.IsNullOrEmpty(requestParams["pc"]) ? requestParams["pc"].ToLower() : string.Empty;
                string ac = agentCode;
                string cc = requestParams["cc"].ToLower().Contains("au") ? "au" : "nz";
                string cr = !string.IsNullOrEmpty(requestParams["cr"]) ?  requestParams["cr"].ToLower() : string.Empty;     
                string pbLbl = ApplicationManager.GetApplicationData().GetAddressForLocationCode(requestParams["pb"]);
                string dbLbl = ApplicationManager.GetApplicationData().GetAddressForLocationCode(requestParams["db"]);
                string pt = requestParams["pt"].Remove(2, 1), dt = requestParams["dt"].Remove(2, 1);
                string pb = requestParams["pb"].ToLower();
                string db = requestParams["db"].ToLower();
                string promocode = !string.IsNullOrEmpty(requestParams["promoCode"]) ? requestParams["promoCode"].ToLower() : string.Empty;

                int.TryParse(requestParams["na"], out na);
                int.TryParse(requestParams["nc"], out nc); 
                pickDateStr = DateTimeHelper.LeadingZerosPadding(requestParams["pd"], 2) + "-" + DateTimeHelper.LeadingZerosPadding(requestParams["pm"],2) + "-" + requestParams["py"];
                dropDateStr = DateTimeHelper.LeadingZerosPadding(requestParams["dd"], 2) + "-" + DateTimeHelper.LeadingZerosPadding(requestParams["dm"], 2) + "-" + requestParams["dy"];
                //json = "{ brand: '" + /*TODO: request.Brand*/ "mighty" + "', cc: '" + cc + "', cr: '" + cr + "', pb: '" + pb + "', pbLbl: '" + pbLbl + "', db: '" + db + "', dbLbl: '" + dbLbl + "', pd: '" + pickDateStr + "', pt: '" + pt + "', dd: '" + dropDateStr + "', dt: '" + dt + "', na: " + na + ", nc: " + nc + ", queryBrands: " + queryBrands + ", ac : '" + ac + "',pc: '" + packageCode + "',cur:'" + cur + "', pc:'"+ packageCode +"', refresh: true}";
                json = "{ brand: '" + /*TODO: request.Brand*/ "mighty" + "', cc: '" + cc + "', cr: '" + cr + "', pb: '" + pb + "', pbLbl: '" + pbLbl + "', db: '" + db + "', dbLbl: '" + dbLbl + "', pd: '" + pickDateStr + "', pt: '" + pt + "', dd: '" + dropDateStr + "', dt: '" + dt + "', na: " + na + ", nc: " + nc + ", queryBrands: " + queryBrands + ", ac : '" + ac + "',pc: '" + packageCode + "',cur:'" + cur + "', pc:'" + packageCode + "',promocode : '" + promocode + "', refresh: true}";
                
            }
            catch (Exception ex)//failed to parse the incoming aggregator params
            {
                string msg = ex.Message;
                THLDebug.LogError(ErrorTypes.Application, "Async:RendeInitJSON", "Failed to load incoming params", "{'params':'" + requestParams.ToString() + "'}");
                debugJson = "{'msg':'failed to parse query params'}";
            }
        }
        //?cc=AU&vc=&ac=&sc=rv&pc=&ch=&rf=&na=1&nc=0&cr=AU&pb=ADL&pd=13&pm=3&py=2013&pt=10:00&db=ADL&dd=28&dm=3&dy=2013&dt=15:00&vtype=rv&cvh=&pv=1.0&cbrand=b&csell=1&SID=zdelccf0m2x10k55vdb2gwee&
        if (string.IsNullOrEmpty(json))
        {
            string packageCode = !string.IsNullOrEmpty(requestParams["pc"]) ? requestParams["pc"].ToLower() : string.Empty;
            if (countryCode.Equals(CountryCode.AU))
                //Added PromoCode by Nimesh on 16th Oct 2014
                json = "{ brand: 'mighty', cc: 'au', cr: '', pb: 'bne', pbLbl: 'Brisbane', db: 'bne', dbLbl: 'Brisbane', pd: '" + pickDateStr + "', pt: '1000', dd: '" + dropDateStr + "', dt: '1500', na: 1, nc: 0, queryBrands: ['kea', 'maui', 'britz', 'mighty'], ac : '" + agentCode + "', cur:'aud', pc:'" + packageCode + "',promocode : '" + promoCode + "',debug : " + debugJson + "}";
            else
                //Added PromoCode by Nimesh on 16th Oct 2014
                json = "{ brand: 'mighty', cc: 'nz', cr: '', pb: 'akl', pbLbl: 'Auckland Airport', db: 'akl', dbLbl: 'Auckland Airport', pd: '" + pickDateStr + "', pt: '1000', dd: '" + dropDateStr + "', dt: '1500', na: 1, nc: 0, queryBrands: ['kea','maui', 'britz','united','alpha' , 'mighty', 'econo'], ac : '" + agentCode + "', cur:'nzd', pc:'" + packageCode + "',promocode : '" + promoCode + "', debug: " + debugJson + " }";
            /* not use for now
            if (agentCode.ToLower().Equals("virginb2c"))
            {
                if (countryCode.Equals(CountryCode.AU))
                    json = "{ brand: 'mighty', cc: '" + countryCode.ToString().ToLower() + "', cr: '', pb: 'bne', pbLbl: 'Brisbane', db: 'bne', dbLbl: 'Brisbane', pd: '" + pickDateStr + "', pt: '1000', dd: '" + dropDateStr + "', dt: '1500', na: 1, nc: 0, queryBrands: ['maui', 'britz', 'mighty'], ac : 'VIRGINB2C', cur:'aud' }";
                else
                    json = "{ brand: 'mighty', cc: '" + countryCode.ToString().ToLower() + "', cr: '', pb: 'akl', pbLbl: 'Auckland Airport', db: 'akl', dbLbl: 'Auckland Airport' , pd: '" + pickDateStr + "', pt: '1000', dd: '" + dropDateStr + "', dt: '1500', na: 1, nc: 0, queryBrands: ['maui', 'britz', 'mighty'], ac : 'VIRGINB2C', cur:'nzd'}";
            }
             * */
        }
        return json;
    }
    
    public string AgentPromo { get; set; }

    /// <summary>
    /// Load Agents Views if applicable
    /// </summary>
    /// <param name="agentCode"></param>
    /// <returns></returns>
    protected bool LoadAgentView(string agentCode)
    {
        AgentTheme aTheme = new AgentTheme(agentCode);
        AgentPromo = aTheme.PromoHTML;
        BrandedFooter = aTheme.FooterHTML;
        BrandedHeader = aTheme.HeaderHTML;
        return true;//TODO: alert caller if empty..or consider defaults
    }
}