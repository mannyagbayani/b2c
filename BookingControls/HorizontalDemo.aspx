﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HorizontalDemo.aspx.cs" Inherits="HorizontalDemo" %>
<%@ Register Src="HorizontalControl.ascx" TagName="HorizontalControl" TagPrefix="thl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Horizontal Booking Control DEV Zone</title>
    <link rel="stylesheet" type="text/css" media="all" href="<%= ContentURL %>/css/calendar/calendar.css" />
    <link href="css/Base.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="<%= ContentURL %>/js/jquery-1.3.1.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>/js/jquery.dimensions.min.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>/js/date.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>/js/calendar/utils.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>/js/calendar/calendar.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>/js/calendar/calendar-setup.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>/js/calendar/lang/calendar-en.js"></script>
    <script type="text/javascript" src="<%= ContentURL %>/js/hCtrl.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <thl:HorizontalControl ID="HorizontalControl1" VehicleTypeStr="av" CountryOfResidence="" CountryCode="NZ" VehicleCode="nzavm.2BMA" Brand="mac" SplashMode="false" AggregatorMode="true" Target="http://secure.motorhomesandcars.com/pages/Selection.aspx" runat="server" />
        </div>
        <div style="float:left;clear: both;margin-top: 30px;">
            <big>Test Vehicle Code Population</big><br /><br />
            <label>Vehicle Code:</label><input type="text" id="vehicleCodeInput" />
            <label>Vehicle Name:</label><input type="text" id="vehicleNameInput" />
            <script type="text/javascript">
                function testCodepopulation() {
                    setSelectedVehicle($j('#vehicleCodeInput').val(), $j('#vehicleNameInput').val());
                }
            </script>
            <a href="javascript:testCodepopulation();">Populate Vehilce Code</a>           
        </div>                
    </form>
</body>
</html>